module Crosstab

open Ids
open IdsExtensions
open DBAction
open System.Data
open System.Net.WebSockets
// The crosstab query is composed of 4 elements:
// select $selection from crosstab($values_query, details_query) as (id int, field2 type,...)
// 1) selection : what is selected in the table built by the crosstab function
// 2) values_query: this returns the values of the table being built
// 3) details_query: this returns the columns names of the table, in order
// 4) as string: list the data types of the columns

type Info =
    { // body (i.e. `crostab(....) as ...`) of the crosstab query where all details of the entity are returned
      // just need to prepend with select *, select count(*), etc
      crosstabQueryBody: string
      // body of crosstab query used to display lists, so details not displayed in lists are not displayed
      crosstabListQueryBody: string
      // all entityDetails of the entity
      entityDetails: Entity.EntityDetail list
      // entityDetails for details displayed in list
      entityDetailsInList: Entity.EntityDetail list }


let valuesTableQuery (EntityId entityId) (valuesTable:string) (instancesFilter:InstanceId list option)=
    let instanceIdFilter = match instancesFilter with
                             // No filter, so we don't add any WHERE clause
                             | None -> ""
                             // an empty list means there's no existing instance. As the "IN ()" syntax of postgresql
                             // does not allow an empty list, add a filter that is alway false
                             | Some [] -> sprintf "and false"
                             | Some ids -> sprintf "and i.id in (%s)" (ids |> List.map (fun (id:InstanceId) -> string(InstanceId.Get id)) |> String.concat ",")
    match valuesTable with
    | "detail_values"
    | "date_detail_values"
    | "integer_detail_values" ->
        sprintf "(select distinct on (i.id, d.name) i.id, d.name as name, dv.value::text from instances i join %s dv on (dv.instance_id=i.id) join details d on (d.id=dv.detail_id) where i.entity_id=%d %s order by i.id,d.name,dv.id DESC)" valuesTable entityId instanceIdFilter
    | "ddl_detail_values" ->
        sprintf "(select distinct on (i.id, d.name) i.id, d.name as name, pv.value from instances i join %s dv join detail_value_propositions pv on (pv.id=dv.detail_value_proposition_id)  on (dv.instance_id=i.id) join details d on (d.id=dv.detail_id)  where i.entity_id=%d %s order by i.id,d.name,pv.id)" valuesTable entityId instanceIdFilter
    | _ -> failwithf "detailValues table %s unhandled" valuesTable
// ********************************************************************************
// Query that returns values in the crosstab call (first query passed as argument)
// if a new detail type is added, append the 'select distinct' query at the end of the string, and add a `entity_id` parameter
let valuesQuery (entityId) (instancesFilter:InstanceId list option)=
    sprintf "
    %s
      union
    %s
      union
    %s
      union
    %s
   order by id, name" (valuesTableQuery entityId "detail_values" instancesFilter)
                      (valuesTableQuery entityId "date_detail_values" instancesFilter)
                      (valuesTableQuery entityId "integer_detail_values" instancesFilter)
                      (valuesTableQuery entityId "ddl_detail_values" instancesFilter)

// ********************************************************************************
// details displayed , passed as second argument to the crosstab call
let detailsSelect (entityDetails: Entity.EntityDetail list) =
    entityDetails
    // map each detail kept to query with incrementing id values for ordering
    // replace ' by '''' because we are a quoted string 2 levels deep (quoted string in a quoted string)
    |> List.mapi (fun i ed -> sprintf "select %d as id, E''%s'' as name" (i + 1) (ed.name.Replace("'","''''")))
    // group all results with a sql union
    |> String.concat " UNION "
    // put results in a temporary table
    |> sprintf "select name from (%s order by id) as temporary_table"

// ********************************************************************************
// data type mapping of the columns resulting from the crosstab call
// IMPROVE : make the data_type a discriminated union type and make a exaustive match here
let asString (entityDetails: Entity.EntityDetail list) =
    entityDetails
    |> List.map (fun ed ->
        match ed.data_type with
        | "madb_integer" -> sprintf "\"%s\" bigint" (ed.name.Replace("\"","\"\""))
        | _ -> sprintf "\"%s\" text" (ed.name.Replace("\"","\"\"")))
    // add the id field we manually added for ordering
    |> List.append [ "id int" ]
    |> String.concat ","

// Get instances corresponding to the instanceIdsFilter
// Useful for linked instances, and possibly for search results later on
let getFilteredCrosstabInfoAction entityId (instanceIdsFilter:InstanceId list option): DBAction.DBAction<Info> =
    let action (client: DB.DBClient): Async<DBResult.DBResult<Info>> = async {
        let! entityDetailsResult = EntityExtensions.entityDetailsAction entityId |> DBAction.run client
        match entityDetailsResult with
        | Ok entityDetails ->
            let entityDetailsInList = entityDetails |> List.filter (fun e -> e.displayed_in_list_view)
            return DBResult.DBResult.Ok
                [ { entityDetails = entityDetails
                    entityDetailsInList = entityDetailsInList
                    crosstabListQueryBody =
                        sprintf "crosstab(E'%s', E'%s') as (%s)" (valuesQuery entityId instanceIdsFilter)
                            (detailsSelect entityDetailsInList) (asString entityDetailsInList)
                    crosstabQueryBody =
                        sprintf "crosstab(E'%s', E'%s') as (%s)" (valuesQuery entityId instanceIdsFilter) (detailsSelect entityDetails)
                            (asString entityDetails)
                 } ]
        | Error e -> return DBResult.DBResult.Error e
    }
    DBAction<Info>.Query action

// Get all instances list for entityId.
let getCrosstabInfoAction entityId: DBAction.DBAction<Info> =
    getFilteredCrosstabInfoAction entityId None