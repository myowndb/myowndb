// A small lens implementation for records inspired by https://medium.com/@devboy_org/generic-lenses-for-f-record-types-6aea9f4cba40
//requires: nuget FSharp.Quotations.Evaluator
module Lens

open FSharp.Quotations
open FSharp.Quotations.Patterns
open FSharp.Quotations.Evaluator
open Microsoft.FSharp.Reflection
open System.Reflection

let private eval = QuotationEvaluator.EvaluateUntyped

module internal Record =
    // Return an updated list of values to be used in a call to FSharpValue.MakeRecord,
    // in which the value for the field fieldPropertyInfo is newVal, all other fields
    // having the same value as record in object o
    let updatedValues (fieldPropertyInfo: PropertyInfo) newValue o =
        // get type of object o from the PropertyInfo describing its field to be modified
        fieldPropertyInfo.DeclaringType
        // then get all the fields of  o's type
        |> FSharpType.GetRecordFields
        // and map over the fields, only modifying the one corresponding to the PropertyInfo
        // we got
        |> Array.map (fun x -> if x = fieldPropertyInfo then newValue else x.GetValue(o))
    // Problem: the type argument of WithArgs is undefined at compile time, thus is set to obj by compiler
    //          But the list we pass is not a list of obj, and this causes trouble at runtime.
    // Solution: replace the function by a static member on a type, so that we can easily manipulate it
    //           with reclection code. The reflection code will "instanciate" the generic method and get
    //           a method with the type argument set correctly, which can then be invoked.
    // Seem comments of https://stackoverflow.com/a/501356 about overloading choices done by fsharp
    type WithMethods() =
        // This is used to update a list element, of which the index is in args
        static member WithArgs<'a> (fieldPropertyInfo: PropertyInfo) (newVal: 'a) (currentList: 'a list) args =
            if Array.length args <> 1 then
                failwithf
                    "WithArgs only handles list, and hence only supert args of size 1, but here it has size %d: %A"
                    (Array.length args) args
            // get index of element to be modified from args
            let index = args |> Array.head |> unbox
            // map and change only element to be modified
            currentList
            |> List.mapi (fun i (v: 'a) -> if i = index then newVal else v)
            |> box
    // update field corresponding to fieldPropertyInfo of record passed as object o
    // to have value v
    let With (fieldPropertyInfo: PropertyInfo) v o =
        let newRecordValues = updatedValues fieldPropertyInfo v o
        FSharpValue.MakeRecord(fieldPropertyInfo.DeclaringType, newRecordValues)
    // Traverse the tree to change the value referenced by the expression we get.
    let rec updateRecord =
        (fun p ->
            match p with
            // this was present in @devboy_org's blog, but not sure it is needed
            // | PropertyGet(None,_,[]),v -> v
            | PropertyGet (Some (PropertyGet (_) as pg), property, []), newValue ->
                // Update the field `property.Name` of record `eval pg` by `newValue`
                // printfn "Update field %A of record\n%A\nby\n %A" property.Name (eval pg) newValue
                updateRecord (pg, (With property newValue (eval pg)))
            | PropertyGet (Some (PropertyGet (_, pi, _) as pg), property, propertyArgs), newValue when property.Name =
                                                                                                           "Item" ->
                // We have a list as the property.Name is Item, the way to access an item of a list
                // Replace element `propertyArgs` of list `eval pg` by newValue
                // Place value `newValue` in element `propertyArgs` of list `(eval pg)` which is stored under parent's key `pi.Name`
                //printfn "Place value %A \nin element %A \nof list %A\n stored in parent level's key %A" newValue propertyArgs (eval pg) pi.Name
                let evaledPropertyArgs =
                    propertyArgs |> List.map eval |> Array.ofList

                let mt = typeof<WithMethods>
                // GetMethod returned null, don't know why
                // GetDeclaredMethods works fine
                let withArgsMethod =
                    mt.GetTypeInfo().GetDeclaredMethods("WithArgs")
                    |> Seq.item 0
                // We need to handle values of different types, and we use reflection to instanciate
                // a generic method
                // Get the type of the replacement value, which is the same as the elements
                // of the list we're working on
                let typ = (unbox newValue).GetType()
                // instanciate the method with the correct type argument
                let m =
                    withArgsMethod.MakeGenericMethod [| typ |]
                // put arguments of the method call in an array of objects
                // they will be unboxed correctly by Invoke
                let methodArgs =
                    [| box property
                       newValue
                       (eval pg)
                       box evaledPropertyArgs |]
                // invoke the withArgs method
                let realArgs = m.Invoke(null, methodArgs)
                // we can then do the recursive wall
                updateRecord (pg, realArgs)
            | PropertyGet (Some (ValueWithName (_, _, n) as pg), property, []), newValue ->
                // Store newValue under property `property.Name` of `n`
                // For example, a result of
                //  printfn "replace %s.%s by %A" n property.Name newValue
                // is
                //  > replace spec.instanceId by Some (InstanceId 100002)
                updateRecord (pg, (With property newValue (eval pg)))
            // Replace the value by the new one
            | ValueWithName (_) as pg, newValue -> newValue
            | v ->
                // Beware! If you get here, it might be due to editing a record defined in another scope.
                // I had this error when trying to update a record defined at the module level, and updating it
                // in a class in that module (in a test).
                // The solution is simply to assign the value from the module to a local variable:
                //   let myRecord = moduleRecord
                let msg =
                    sprintf "unhandled value in updateRecord. Only a record can be handled! Is it a non-local scope value? In that case simply assign it to a local variable\n: %A" v

                failwith msg)

// Update the field accessed by expression e with value v
// For example:
//   Lens.With <@ r.detailValues.[0].values.[0].detailValueId @> 5
let With (e: Expr<'v>) (v: 'v): 'r = Record.updateRecord (e.Raw, v) :?> 'r
// Allows chaining, the function f receiving as argument the record to update
// A type annotation is currently required on f's argument.
// For example:
//   Lens.With <@ spec.instanceId @> (Some (InstanceId (instanceId+1)))
//   |> Lens.Bind (fun (r:Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>) (Some (DateDetailValueId (dateDetailValueId+1)))
let Bind (f: 'b -> Expr<'a>) (v: 'a) (r: 'b): 'b =
    Record.updateRecord ((f r).Raw, v) :?> 'b
