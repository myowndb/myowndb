module DateAndTime

open Error
open System


type t


val fromString: string -> Result<t, Errors>
val isValid: string -> bool
val toString: t -> string
val get: t -> DateTime
