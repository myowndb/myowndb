module DdlChoice
// This module provides the hidden type ChoiceId, representing the id of the
// detail_value_proposition chosen for a particular detail.
// Having it hidden ensures the chosen id belongs to the propositions for the detail,
// as the queries used below limit the results to the scode of the detail in question.

//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
//              __        ___    ____  _   _ ___ _   _  ____
//              \ \      / / \  |  _ \| \ | |_ _| \ | |/ ___|
//               \ \ /\ / / _ \ | |_) |  \| || ||  \| | |  _
//                \ V  V / ___ \|  _ <| |\  || || |\  | |_| |
//                 \_/\_/_/   \_\_| \_\_| \_|___|_| \_|\____|
//!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// If you modify this file, check that the corresponding file in the WebSharper
// proxies project wsproxies is updated accordingly!
// The fact that we make the Discriminated Union type t's cases unreachable by using
// a .fsi file prevents us from moving the functions like fromStrings to a separate module
// because these functions need to instanciate values of type t. As these functions don't make
// any sense in the WS proxies project, we currently keep a copy of the file with only the
// type and relevant functions.

open Error
open Ids
open FSharpPlus

// A record type to hold information about a ddl value proposition.
type Proposition =
    {
        id: int
        detail_id: int
        value: string
    }
type t = ChoiceId of Proposition

// Get if of proposition from its textual value
let fromString (client: DB.DBClient) (DetailId detail_id) (choice: string) =
    DBAction.querySingleAction "select id,value from detail_value_propositions dvp where dvp.detail_id=%d and value= %s"
        detail_id choice
    |> DBAction.run client
    |> Async.map (DBResult.map (fun (r:{|id:int;value:string|}) -> {id=r.id; detail_id=detail_id; value=r.value}))
    |> Async.map (DBResult.map ChoiceId)

// Get id of proposition from its id, this is just a validation of coherency
let fromIdAction (DetailId detail_id) (DetailValuePropositionId dvpId) =
    // assignation to explicitly specify type
    // We don't do a select * because the DetailValueProposition type is not available here as it is defined in a file included later.
    // Instead, we define an anonymous record.
    // We only retrieve fields we use, we can expand it later if needed.
    let r:DBAction.DBAction<Proposition> =
        DBAction.querySingleAction "select detail_id, id,value from detail_value_propositions dvp where dvp.detail_id=%d and id= %d"
                                    detail_id dvpId
    r
    |> DBAction.map ChoiceId

// Get id of proposition from its id, this is just a validation of coherency
let fromId (client: DB.DBClient) (detailId) (detailValuePropositionId) =
    fromIdAction detailId detailValuePropositionId
    |> DBAction.run client

// Get id of proposition from its value
let fromValueAction (DetailId detail_id) (value:string) =
    // assignation to explicitly specify type
    // We don't do a select * because the DetailValueProposition type is not available here as it is defined in a file included later.
    // Instead, we define an anonymous record.
    // We only retrieve fields we use, we can expand it later if needed.
    DBAction.querySingleAction "select detail_id, id,value from detail_value_propositions dvp where dvp.detail_id=%d and value= %s"
                                    detail_id value
    |> DBAction.map ChoiceId


// Get the textual valu of the proposition
let toString  (ChoiceId {id=id;detail_id=detailId;value=value}): string =
    value

// Extract the id of the proposition from the value of hidden type
let toId (ChoiceId {id=id;detail_id=_;value=_}) = id
let toDetailId (ChoiceId {id=_;detail_id=detail_id;value=_}) = detail_id
