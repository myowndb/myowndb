module DateAndTime

open System.Text.RegularExpressions
open Error
// for DateTime
open System

type t = DateTime of DateTime

let fromString (s: string): Result<t, Errors> =
    match DateTime.TryParse(s) with
    | false, _ -> Error.fromString "Date could not be parsed" |> Error
    | true, dt -> Ok(DateTime dt)

let isValid (s:string) =
    if s = "" then
        true
    else
        match fromString s with
            | Ok _ -> true
            | Error _ -> false

let toString (DateTime dt) =
    if dt.Hour=0 && dt.Minute=0 && dt.Second=0 then
        $"{dt.Year}-%02d{dt.Month}-%02d{dt.Day}"
    else
        $"{dt.Year}-%02d{dt.Month}-%02d{dt.Day} %02d{dt.Hour}:%02d{dt.Minute}:%02d{dt.Second}"
let get (DateTime dt) = dt
