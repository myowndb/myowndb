module DdlChoice

open DBResult
open Error

type Proposition =
    {
        id: int
        detail_id: int
        value: string
    }
type t



val fromString: DB.DBClient -> Ids.DetailId -> string -> Async<DBResult<t>>
val fromId: DB.DBClient -> Ids.DetailId -> Ids.DetailValuePropositionId -> Async<DBResult<t>>
val fromIdAction: Ids.DetailId -> Ids.DetailValuePropositionId -> DBAction.DBAction<t>
val fromValueAction: Ids.DetailId -> string -> DBAction.DBAction<t>
val toString: t -> string
val toId: t -> int
val toDetailId: t -> int
