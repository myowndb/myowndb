module WebURL

open System.Text.RegularExpressions
open Error

type t = WebURL of string

let fromString (s: string): Result<t, Errors> =
    let pattern = "^https?://[^\s]*$"
    let m = Regex(pattern).Match(s)
    match m.Success with
    | true -> Ok(WebURL s)
    | false ->
        $"email %s{s} is invalid"
        // First create our Error.t instance
        |> Error.fromString
        // then wrap it in the Error case of the Result type
        |> Error

let toString (WebURL a) = a

let isValid (s: string): bool =
    match fromString s with
    | Ok _ -> true
    | Error _ -> false

let validate (s: string): Result<Unit, Errors> =
    match fromString s with
    | Ok _ -> Ok()
    | Error e -> Error e
