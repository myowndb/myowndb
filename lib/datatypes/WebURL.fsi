module WebURL

open Error

type t


val fromString: string -> Result<t, Errors>
val toString: t -> string
val isValid: string -> bool
val validate: string -> Result<Unit, Errors>
