module Validations.Base

open Error

/// <summary>returns a validation function</summary>
/// <param name="fv">A bool-returning function indicating if the value passed as argument is valid or not</param>
/// <param name="fe">A translation-returning function indicating the reason why the value passed as argument is invalid</param>
/// <param name="value">The value to be tested</param>
/// <returns>A function, taking as argument the value to validate, that can be used by Validations.Base.run</returns>
let make (fv: 'a -> bool) (fe: 'a -> string): 'a -> Result<Unit, Errors> =
    fun value ->
        match (fv value) with
        | true -> (Ok())
        | false -> fe value
                    // First create our Error.t instance
                    |> Error.fromString
                    // then wrap it in the Error case of the Result type
                    |> Error
/// Will pass the value in the second argument to all validation functions in the list passed as first argument.
/// All functions are run, and if multiple errors occur, those are accumulated.
/// Any function of type ('a -> Result<Unit,Errors>) can be passed, where 'a is the type of the value to be validated.
let run<'a> (validations: ('a -> Result<Unit, Errors>) list) (value: 'a): Result<Unit, Errors> =
    // store intermediate result as foldBack doesn't take the list to fold as last argument
    let results = validations |> List.map (fun f -> f value)
    // Use foldBack to have error messages in the same order as the validations
    List.foldBack (fun r acc ->
        match acc, r with
        | Ok(), Ok() -> Ok()
        | Ok(), Error e -> Error e
        | Error es, Error e -> (Error(Error.join es e))
        | Error es, Ok() -> Error es) results (Ok())
