module Validations.String

open System.Text.RegularExpressions
open Error

let minimumLength l =
    Base.make (fun v -> String.length v >= l)
        (fun v -> sprintf "String is too short, needed length >= %d, got %d" l (String.length v))
let maximumLength l =
    Base.make (fun v -> String.length v <= l)
        (fun v -> sprintf "String is too long, needed length <= %d, got %d" l (String.length v))
let exactLength l =
    Base.make (fun v -> String.length v = l)
        (fun v ->
            sprintf "String length incorrect, needed exact length of %d, got %d" l
                (String.length v))
let regexp pattern =
    Base.make (fun v -> Regex(pattern).Match(v).Success)
        (fun v -> sprintf "String does not match pattern %s" pattern)
