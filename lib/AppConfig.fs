module AppConfig

open FSharp.Data
open System
open System.IO

type Json = JsonProvider<"config.json.sample", EmbeddedResource="lib, lib.embedded.config.json.sample">

type DBConfig =
  {
    host: string
    port: string
    user: string
    password_file: string
    database: string
  }
// Use a single case DU he as F# cannot  hide type abbreviations:
// https://stackoverflow.com/questions/2855735/f-cant-hide-a-type-abbreviation-in-a-signature-why-not
type t =
  | AppConfig of Json.Root
  | EnvConfig of DBConfig

let load (path: string): t =
  if path <>"-" then
    let config = Json.Load(path)
    AppConfig config
  else
    EnvConfig {
      host = Environment.GetEnvironmentVariable("MYOWNDB_database__host")
      port = Environment.GetEnvironmentVariable("MYOWNDB_database__port")
      user = Environment.GetEnvironmentVariable("MYOWNDB_database__user")
      database = Environment.GetEnvironmentVariable("MYOWNDB_database__database")
      password_file = Environment.GetEnvironmentVariable("MYOWNDB_database__password_file")
  }


let host (config:t): string =
  match config with
  | AppConfig json -> json.Db.Host
  | EnvConfig r -> r.host
let port (config:t): int =
  match config with
  | AppConfig json -> json.Db.Port
  | EnvConfig r -> int r.port
let database (config:t): string =
  match config with
  | AppConfig json -> json.Db.Db
  | EnvConfig r -> r.database
let user (config:t): string =
  match config with
  | AppConfig json -> json.Db.User
  | EnvConfig r -> r.user
let pass (config:t): string =
  match config with
  | AppConfig json -> json.Db.Pass
  | EnvConfig r -> File.ReadAllText(r.password_file).Trim()

let pgConnectionString (config:t): string =
  sprintf  "Host=%s;Port=%d;Database=%s;Username=%s;Password=%s;CommandTimeout=600;" (host config) (port config) (database config) (user config)
            (pass config)
