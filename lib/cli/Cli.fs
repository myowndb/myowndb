module Cli

open Ids
open System.Data
open FSharpPlus

// returns a string displaying the table with data from the datatable
let sprintDataTable (table: DataTable) =
    let s = ref [||]
    if table.Columns.Count = 0 then
        s := Array.append !s [| "No columns to display in list view\n" |]
    else
        for c in table.Columns do
            s := Array.append !s [| sprintf "%-8s\t" c.ColumnName |]
        s := Array.append !s [| "\n" |]
        for r in table.Rows do
            for c in table.Columns do
                s := Array.append !s [| sprintf "%-8s\t" (r.Item(c).ToString()) |]
            s := Array.append !s [| "\n" |]
    String.concat "" !s

// If the table could be built, returns success, else return the error(s)
let tableDataTableResult (client: DB.DBClient) (entityId: EntityId) =
    Instance.selectDataTableAction entityId Instance.defaultListClause
    |> DBAction.run client

// If the table could be built, returns success, else return the error(s)
let tableStringResult (client: DB.DBClient) (entityId: EntityId) =
    tableDataTableResult client entityId
    |> Async.map (DBResult.map sprintDataTable)
    |> Async.map DBResult.toSingleResult

let tableJsonResult (client: DB.DBClient) (entityId: EntityId) =
    Instance.selectDataTableAction entityId Instance.defaultListClause
    |> DBAction.mapDataTableToJSON
    |> DBAction.run client
    |> Async.map DBResult.toSingleResult

// Function returning the table or an error as string
let tableString (locale: Locale.t) (client: DB.DBClient) (entityId: EntityId) = async {
    let! v = (tableStringResult client entityId)
    match v with
    | Ok s -> return s
    | Error es -> return Error.toString es
}

let tableJson (locale: Locale.t) (client: DB.DBClient) (entityId: EntityId) = async {
    let! v = (tableJsonResult client entityId)
    match v with
    | Ok s -> return s
    | Error es -> return Error.toString es
}
