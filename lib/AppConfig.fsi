module AppConfig

type t

val load: string-> t

val host:  t -> string
val port:  t -> int
val database:  t -> string
val user:  t -> string
val pass:  t -> string
val pgConnectionString:  t -> string
