module Admin.Entity


// return DBAction yielding updated spec
let createActionFromSpec (spec:Specs.EntitySpec) =
    let (Ids.DatabaseId id) = spec.databaseId
    DBAction.singleInsertAction "insert into entities (database_id,name,has_public_form) VALUES (%d,%s,%b) RETURNING id" id spec.name spec.hasPublicForm
    |> DBAction.map (fun entityId -> {spec with entityId = Some (entityId)})

// return DBACtion yielding spec, or error if update failed
let updateActionFromSpec id (spec:Specs.EntitySpec) =
    DBAction.statementAction "update entities set name=%s,has_public_form=%b where id=%d" spec.name spec.hasPublicForm id
    |> DBAction.bind (fun (DBTypes.ImpactedRows n) -> if n=1 then
                                                        DBAction.retn spec
                                                      else
                                                        DBAction.error "Update failed")
// return DBAction to create ( if entityId is None) or update (if entityId is Some)
let createOrUpdateActionFromSpec (spec:Specs.EntitySpec) =
    match spec.entityId with
    | None -> createActionFromSpec spec
    | Some (Ids.EntityId id) -> updateActionFromSpec id spec

// Create or update the detail if the Entity2DetailSpec includes a DetailSpec for creation. Otherwise it has a detailId, so do nothing
let createDetailIfNeededAction (specIn:Specs.Entity2DetailSpec) =
    match specIn with
    | Specs.Entity2ExistingDetailSpec spec -> DBAction.retn spec
    | Specs.Entity2NewDetailSpec spec -> let (Ids.EntityId eId) = spec.info.entityId
                                         DBAction.querySingleAction "select database_id from entities where id=%d" eId
                                         |> DBAction.map Ids.DatabaseId
                                         |> DBAction.bind (fun databaseId ->  Admin.Detail.createOrUpdateActionFromSpec {  detailId = spec.detailSpec.detailId
                                                                                                                           name = spec.detailSpec.name
                                                                                                                           dataType = spec.detailSpec.dataType
                                                                                                                           propositions = spec.detailSpec.propositions
                                                                                                                           status = spec.detailSpec.status
                                                                                                                           databaseId = databaseId})
                                         |> DBAction.bind (fun dSpec -> match dSpec.detailId with
                                                                         | Some detailId -> DBAction.retn { detailId = detailId
                                                                                                            info = spec.info }
                                                                         | None -> DBAction.error "Detail not created"
                                                       )


// Will create or update the entities2details entry.
// This function handle both as the field spec.info.entities2detailId is an option.
// Extracting each branch of the match in a separate function is not optimal because
// these functions would also take the spec as argument, and for the update case, which
// needs to extract the Entities2detailId under the option, this leads to two suboptimal code
// structure:
// - either we match again, but we know only one branch is relevant as that match was
//   already done in the caller
// - or the caller passes the value it extracted from the option as additional argument, but
//   then this value is duplicated as it is also present in the spec.
let linkDetailAction (spec:Specs.Entity2ExistingDetailSpec) =
    let (Ids.EntityId entityId) = spec.info.entityId
    let (Ids.DetailId detailId) = spec.detailId
    match spec.info.entities2detailId with
    | None -> DBAction.singleInsertAction "insert into entities2details(entity_id,detail_id,displayed_in_list_view,maximum_number_of_values,display_order) VALUES (%d,%d,%b,%d,%d) returning id"
                                          entityId detailId spec.info.displayedInList spec.info.maximumNumberOfValues spec.info.displayOrder
               |> DBAction.map (fun e2dId -> {spec with info= {spec.info with entities2detailId = Some e2dId}})
    | Some (Ids.Entities2detailId e2dId) -> DBAction.statementAction "update entities2details set entity_id=%d,detail_id=%d,displayed_in_list_view=%b,maximum_number_of_values=%d,display_order=%d where id=%d"
                                                                     entityId detailId spec.info.displayedInList spec.info.maximumNumberOfValues spec.info.displayOrder e2dId
                                            |> DBAction.bind (fun (DBTypes.ImpactedRows n) -> match n with
                                                                                              | 1 -> DBAction.retn spec
                                                                                              | _ -> DBAction.error "update failed" )

// Link an existing or new to be created detail
// Can also be used to update the detail itself.
let addDetailActionFromSpec (spec:Specs.Entity2DetailSpec) =
    createDetailIfNeededAction spec
    |> DBAction.bind linkDetailAction

// Update an already linked detail
let updateDetailActionFromSpec (spec:Specs.Entity2ExistingDetailSpec) =
    spec
    |> linkDetailAction

let getAllForAccountAndDatabaseAction (accountId:int)(databaseId:int) : DBAction.DBAction<Specs.EntitySpec>=
    DBAction.queryAction "select e.id as EntityId, e.database_id as DatabaseId,e.name,e.has_public_form as hasPublicForm from entities e join databases db on (db.id=e.database_id) where db.account_id=%d and e.database_id=%d order by e.id" accountId databaseId

let getEntityDetailsAction (accountId) (entityId) : DBAction.DBAction<Entity.EntityDetail>=
    DBAction.queryAction
        """select
                e2d.id,
                e2d.detail_id,
                e2d.entity_id,
                e2d.displayed_in_list_view,
                e2d.maximum_number_of_values,
                e2d.display_order,
                d.name,
                d.lock_version,
                'active' as status,
                dt.name as data_type,
                dt.class_name
            from
                entities2details e2d
                    join entities e on (e.id=e2d.entity_id)
                    join details d on (d.id=e2d.detail_id)
                    join data_types dt on (d.data_type_id=dt.id)

                    join databases db on (e.database_id=db.id)
                    join accounts acc on (db.account_id=acc.id)
                where e.id=%d and acc.id=%d
                order by e2d.display_order
        """
        entityId accountId
// Fetch an individual EntityDetail for an account (which validates access to it)
let getEntityDetailAction (accountId)(entityDetailId) : DBAction.DBAction<Entity.EntityDetail>=
    DBAction.queryAction
        """select
                e2d.id,
                e2d.detail_id,
                e2d.entity_id,
                e2d.displayed_in_list_view,
                e2d.maximum_number_of_values,
                e2d.display_order,
                d.name,
                d.lock_version,
                'active' as status,
                dt.name as data_type,
                dt.class_name
            from
                entities2details e2d
                    join entities e on (e.id=e2d.entity_id)
                    join details d on (d.id=e2d.detail_id)
                    join data_types dt on (d.data_type_id=dt.id)

                    join databases db on (e.database_id=db.id)
                    join accounts acc on (db.account_id=acc.id)
                where e2d.id=%d and acc.id=%d
        """
        entityDetailId accountId