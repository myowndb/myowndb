module Admin.Database

let createActionFromSpec (spec:Specs.DatabaseSpec) =
    let (Ids.AccountId id) = spec.accountId
    DBAction.singleInsertAction "insert into databases (account_id,name) VALUES (%d,%s) RETURNING id" id spec.name
    |> DBAction.map (fun databaseId -> {spec with databaseId = Some (databaseId)})

// return DBACtion yielding spec, or error if update failed
let updateActionFromSpec id (spec:Specs.DatabaseSpec) =
    DBAction.statementAction "update databases set name=%s where id=%d" spec.name id
    |> DBAction.bind (fun (DBTypes.ImpactedRows n) -> if n=1 then
                                                        DBAction.retn spec
                                                      else
                                                        DBAction.error "Update failed")
// return DBAction to create ( if entityId is None) or update (if entityId is Some)
let createOrUpdateActionFromSpec (spec:Specs.DatabaseSpec) =
    match spec.databaseId with
    | None -> createActionFromSpec spec
    | Some (Ids.DatabaseId id) -> updateActionFromSpec id spec

let getAllForAccountAction (accountId:int): DBAction.DBAction<Specs.DatabaseSpec> =
    DBAction.queryAction "select id as databaseId,account_id as accountId,name from databases where account_id=%d order by id" accountId