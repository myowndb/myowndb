module Admin.Relation

open Ids

let validateSpec (spec:Specs.RelationSpec)=
    let (EntityId parentId) = spec.parentId
    let (EntityId childId) = spec.childId
    //DBAction.querySingleAction "select count(*) from (select  distinct database_id from entities where id in (%d,%d)) as temp" parentId childId
    DBAction.querySingleAction "select count(*) from (select distinct database_id from entities where id in (%d,%d)) as temp" parentId childId
    |> DBAction.bind (fun (count) -> if count = 1L then DBAction.retn spec else DBAction.error "inconsistent data")
let createActionFromSpec (spec:Specs.RelationSpec) =
    let (EntityId parentId) = spec.parentId
    let (EntityId childId) = spec.childId
    validateSpec spec
    |> DBAction.bind (fun spec ->  DBAction.singleInsertAction "insert into relations (parent_id,
                                                        child_id,
                                                        from_parent_to_child_name,
                                                        from_child_to_parent_name,
                                                        parent_side_type_id,
                                                        child_side_type_id)
                                                    VALUES
                                                        (%d,%d,%s,%s,%d,%d) returning id
                                    " parentId childId spec.parentToChildName spec.childToParentName (Specs.RelationSideType.toId spec.parentSideType) (Specs.RelationSideType.toId spec.childSideType)
                     )
    |> DBAction.map (fun (Ids.RelationId id) -> {spec with relationId=Some (Ids.RelationId id) })
let updateActionFromSpec (RelationId rId)(spec:Specs.RelationSpec) =
    let (EntityId parentId) = spec.parentId
    let (EntityId childId) = spec.childId
    // Using postgresql function `greatest` to prevent modifying a 'to many' relation to become a `to one` as this raises the questions of
    // what to do with existing links if there are many of them (leaving them as is would create an inconsistency with a 'to one' relation
    // having multiple targets....)
    // We use a query action and not a statement action so we can use `RETURNING *` in the query so we can return the spec corresponding to the
    // db state (we cannot return the spec sent by the client here as the updates of the relation sides are not guaranteed to be applied, see previous comment)
    DBAction.queryAction "update relations set from_parent_to_child_name=%s,
                                                   from_child_to_parent_name=%s,
                                                   parent_side_type_id=greatest(parent_side_type_id,%d),
                                                   child_side_type_id=greatest(child_side_type_id,%d)
                                                where id=%d and parent_id=%d and child_id=%d RETURNING *
                             " spec.parentToChildName spec.childToParentName (Specs.RelationSideType.toId spec.parentSideType)(Specs.RelationSideType.toId spec.childSideType) rId parentId childId
    |> DBAction.map (fun (relation:Relation.Relation) ->
        relation.toSpec()
    )

let createOrUpdateActionFromSpec (spec:Specs.RelationSpec) =
    match spec.relationId with
    | Some rId -> updateActionFromSpec rId spec
    | None -> createActionFromSpec spec
