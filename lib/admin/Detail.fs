module Admin.Detail


// return DBAction yielding updated spec
let createActionFromSpec (spec:Specs.DetailSpec) =
    let (Ids.DatabaseId databaseId) = spec.databaseId
    let (Ids.DataTypeId dataTypeId) = spec.dataType.dataTypeId
    let  statusId = DetailStatus.statusId spec.status

    DBAction.singleInsertAction "insert into details (name,data_type_id,status_id,database_id) VALUES (%s,%d,%d,%d) RETURNING id" spec.name dataTypeId statusId databaseId
    |> DBAction.map (fun detailId -> {spec with detailId = Some (detailId)})

// return DBACtion yielding spec, or error if update failed
let updateActionFromSpec (Ids.DetailId id) (spec:Specs.DetailSpec) =
    let (Ids.DatabaseId databaseId) = spec.databaseId
    let (Ids.DataTypeId dataTypeId) = spec.dataType.dataTypeId
    let  statusId = DetailStatus.statusId spec.status

    DBAction.statementAction "update details set name=%s where id=%d and data_type_id=%d and status_id=%d and database_id=%d" spec.name  id dataTypeId statusId databaseId
    |> DBAction.bind (fun (DBTypes.ImpactedRows n) -> if n=1 then
                                                        DBAction.retn spec
                                                      else
                                                        DBAction.error "Update failed")

// return DBAction to create ( if entityId is None) or update (if entityId is Some)
let createOrUpdateActionFromSpec (spec:Specs.DetailSpec) =
    match spec.detailId with
      | None -> createActionFromSpec spec
      | Some dId -> updateActionFromSpec dId spec
    |> DBAction.bind (fun (spec:Specs.DetailSpec) ->
        match spec.detailId with
        | None -> DBAction.retn spec
        | Some dId ->
            match spec.propositions with
            | None -> DBAction.retn spec
            | Some l -> (DetailValueProposition.createOrUpdateItemsAction dId l)
                        |> DBAction.map (fun propositions -> {spec with propositions=Some propositions})
          )