module Admin.DetailValueProposition


// return DBAction yielding updated spec
let createActionFromSpec (_detailId:Ids.DetailId) (spec:Specs.DetailValuePropositionItemSpec) =
    let (Ids.DetailId detailId) = _detailId

    DBAction.singleInsertAction "insert into detail_value_propositions (detail_id,value) VALUES (%d,%s) RETURNING id" detailId spec.value
    |> DBAction.map (fun detailValuePropositionId -> {spec with detailValuePropositionId = Some (detailValuePropositionId)})

// return DBACtion yielding spec, or error if update failed
let updateActionFromSpec (_id: Ids.DetailValuePropositionId) (_detailId:Ids.DetailId) (spec:Specs.DetailValuePropositionItemSpec) =
    let (Ids.DetailValuePropositionId id) = _id
    let (Ids.DetailId detailId) = _detailId

    DBAction.statementAction "update detail_value_propositions set value=%s where id=%d and detail_id=%d" spec.value id detailId
    |> DBAction.bind (fun (DBTypes.ImpactedRows n) -> if n=1 then
                                                        DBAction.retn spec
                                                      else
                                                        DBAction.error "Update failed")

// dispatch to function to create or update individual proposition value
let createOrUpdatePropositionActionFromSpec (detailId: Ids.DetailId) (spec:Specs.DetailValuePropositionItemSpec) =
    match spec.detailValuePropositionId with
    | Some id -> updateActionFromSpec id detailId spec
    | None -> createActionFromSpec detailId spec

// function to recustively handle items in the list of proposition values in the spec
let rec createOrUpdateItemsAction (detailId:Ids.DetailId) (propositions:Specs.DetailValuePropositionItemSpec list) =
    let cons head tail = head :: tail
    let (<*>) = DBAction.apply
    let retn = DBAction.retn
    let action =
        match propositions with
        | [] -> retn []
        | p::ps -> retn cons
                    <*> createOrUpdatePropositionActionFromSpec detailId p
                    <*> createOrUpdateItemsAction detailId ps
    action

// return DBAction to create ( if entityId is None) or update (if entityId is Some)
let createOrUpdateActionFromSpec (spec:Specs.DetailValuePropositionSpec) =
    createOrUpdateItemsAction spec.detailId spec.propositions
    |> DBAction.map (fun propositions -> { spec with propositions=propositions})