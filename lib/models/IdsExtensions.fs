module IdsExtensions
open Ids

type InstanceId with
    // function that will let the caller build an InstanceId value
    // the validator will be provided by the app. This does not
    // prevent malicious calls with a validator (fun _ -> true) as is done in
    // the tests btw, but it should ensure we work only with valid Ids in our code.
    // It seems hard to predefine validators here in the lib, as the validation will be
    // client code dependent, eg in ASP.Net we will use Policies.
    static member ValidateAsync (id:int, validator: int -> Async<bool>) = async {
        let! isValid = validator id
        if  isValid then
            return Some (InstanceId id)
        else
            return None
    }
    static member Validate(id:int, validator: int -> bool) =
        if  validator id then
            Some (InstanceId id)
        else
            None

type EntityId with
    // see explanation at InstanceId
    static member ValidateAsync (id:int, validator: int -> Async<bool> ) = async {
        let! isValid = validator id
        if isValid then
            return Some (EntityId id)
        else
            return None
    }
    static member Validate(id:int, validator: int -> bool) =
        if  validator id then
            Some (EntityId id)
        else
            None
    static member ValidateAction(id:int, validatorAction: int -> DBAction.DBAction<bool>) =
        validatorAction id
        |> DBAction.bind (fun valid ->
          if valid then
            DBAction.retn (EntityId id)
          else
            DBAction.error ("Entity id access refused")
    )
    member self.toJSON() =
        let id = EntityId.Get self
        sprintf "entityid: %d" id
type RelationId with
    // see explanation at InstanceId
    static member ValidateAsync (id:int, validator: int -> Async<bool> ) = async {
        let! isValid = validator id
        if isValid then
            return Some (RelationId id)
        else
            return None
    }
    static member Validate(id:int, validator: int -> bool) =
        if  validator id then
            Some (RelationId id)
        else
            None

type DatabaseId with
    // see explanation at InstanceId
    static member ValidateAsync (id:int, validator: int -> Async<bool> ) = async {
        let! isValid = validator id
        if isValid then
            return Some (DatabaseId id)
        else
            return None
    }
    static member Validate(id:int, validator: int -> bool) =
        if  validator id then
            Some (DatabaseId id)
        else
            None
