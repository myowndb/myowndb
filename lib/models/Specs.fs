module Specs

open Ids
open System

// Grouping all specs to avoid circular dependencies

// ServerValue is wrapping values that can be trusted as having been validated by the server.
// In an ideal situation, the client should not be able to construct values of type ServerValue.
// However this seems impossible to achieve (because the type Value has to be available to the client,
// and so also the ServerValue type). Once the client has access ot a type, a malicious user could create a value
// of that type.
// The best approach seems to be to publish both types to the client, but sanitize the data in the RPC function called
// by the client to pass the spec back to the server.
// I think the sanitizing can simply be to remove ServerValue items sent by the client (it either got it from the server, and
// hence they didn't change, or it was forged by a malicious user), and then validate the ClientValue items and consequently wrap
// them in ServerValue type.
[<RequireQualifiedAccess>]
type ServerValue =
    | Simple of string option
    | LongText of string option
    | Email of Email.t option
    | WebURL of WebURL.t option
    | DateAndTime of DateAndTime.t option
    // the name of the case is not DdlChoice to avoid a clash
    // with the DdlChoice module. When importing Specs, the module
    // DdlChoice would be shadowed by this union case.
    | DdlValueChoice of DdlChoice.t option
    | Integer of int64 option
    | FileAttachment of ServerFileAttachment.FileAttachment option
    with
    member self.IsSome() =
      self
      |> function
        | Simple v -> v.IsSome
        | LongText v -> v.IsSome
        | Email v -> v.IsSome
        | WebURL v -> v.IsSome
        | DateAndTime v -> v.IsSome
        | DdlValueChoice v -> v.IsSome
        | Integer v -> v.IsSome
        | FileAttachment v -> v.IsSome
    member self.Reset() =
      self
      |> function
        | Simple _ -> Simple None
        | LongText _ -> LongText None
        | Email _ ->Email  None
        | WebURL _ -> WebURL None
        | DateAndTime _ -> DateAndTime None
        | DdlValueChoice _ -> DdlValueChoice None
        | Integer _ -> Integer None
        | FileAttachment _ -> FileAttachment None

// ClientValue is wrapping values proposed by the client
[<RequireQualifiedAccess>]
type ClientValue =
    | Simple of string option
    | LongText of string option
    | Email of string option
    | WebURL of string option
    | DateAndTime of string option
    // the name of the case is not DdlChoice to avoid a clash
    // with the DdlChoice module. When importing Specs, the module
    // DdlChoice would be shadowed by this union case.
    | DdlValueChoice of int option
    | Integer of string option
    | FileAttachment of string option
    with
    member self.IsSome() =
      self
      |> function
        | Simple v -> v.IsSome
        | LongText v -> v.IsSome
        | Email v -> v.IsSome
        | WebURL v -> v.IsSome
        | DateAndTime v -> v.IsSome
        | DdlValueChoice v -> v.IsSome
        | Integer v -> v.IsSome
        | FileAttachment v -> v.IsSome
    override self.ToString() =
      self
      |> function
        | Simple (Some s) -> s
        | Simple None -> ""
        | LongText (Some s) -> s
        | LongText None -> ""
        | Email (Some s) -> s
        | Email None -> ""
        | WebURL (Some s) -> s
        | WebURL None -> ""
        | DateAndTime (Some s) -> s
        | DateAndTime None -> ""
        | DdlValueChoice (Some i) -> string i
        | DdlValueChoice None -> ""
        | Integer (Some s) -> s
        | Integer None -> ""
        | FileAttachment (Some s) -> s
        | FileAttachment None -> ""
    member self.Reset() =
      self
      |> function
        | Simple _ ->Simple None
        | LongText _ ->LongText None
        | Email _ ->Email None
        | WebURL _ ->WebURL None
        | DateAndTime _ ->DateAndTime None
        | DdlValueChoice _ ->DdlValueChoice None
        | Integer _ ->Integer None
        | FileAttachment _ ->FileAttachment None


// A Value in a spec is either a server validated value, which can be trusted,
// or a client proposed value, which cannot be trusted.
// Insertion in the db of values proposed by the client requires to transform them
// into server validated values as this is the only type of values accepted by DB
// accessing functions
type Value =
    | ServerValidated of ServerValue
    | ClientProposed of ClientValue
    with
    member self.IsSome() =
      match self with
      | ServerValidated v -> v.IsSome()
      | ClientProposed v -> v.IsSome()
    override self.ToString() =
      match self with
      | ServerValidated v -> $"ServerValidated {v.ToString()}"
      | ClientProposed v -> v.ToString()
    member self.Reset() =
      match self with
      |ServerValidated v -> ServerValidated (v.Reset())
      | ClientProposed v -> ClientProposed (v.Reset())

// This ValueSpec record type definition allows to define inconsistent data as the
// DetailValueId can be out of sync with the value (eg DateDetailValueId for a Simple v).
// I tried to remediate to it, but without success. The basic problem comes from the fact that
// we store the values in distinct tables but still want to handle them similarly.
// One approach I attempted was to make ServerValue (and ClientValue) wrap a record like this:
//
//     type PlainServerValue = {id: DetailValueId option; value : string option}
//     type EmailServerValue = {id: DetailValueId option; value : Email.t option}
//     type WebURLServerValue = {id: DetailValueId option; value : WebURL.t option}
//     type DateServerValue = {id: DateDetailValueId option; value : DateAndTime.t option}
//     type DdlServerValue = {id: DdlDetailValueId option; value : DdlChoice.t option}
//     type IntegerServerValue = {id: IntegerDetailValueId option; value : int64 option}
//     type ServerValue =
//         | Simple of PlainServerValue
//         | LongText of PlainServerValue
//         | Email of EmailServerValue
//         | WebURL of WebURLServerValue
//         | DateAndTime of DateServerValue
//         | DdlValueChoice of DdlServerValue
//         | Integer of IntegerServerValue
// Doing this, the Id and value are placed in the same record and this prevents inconsistency.
// The problem though is that the function DetailValue.createActionFromValueSpecList
// will handle all of these cases and call createOrUpdateActionwhich returns the id
// of the rown inserted. But the type of this id is different according to the
// table the row was inserted in. And that makes it impossible to use as such.
// A solution would be an additional DU wrapping, but that would lead to complex code
// and it would again allow to have inconsistent data.
//
type ValueSpec =
    { detailValueId: DetailValueIdType option
      value: Value }
    with
    // Return a new record with detailValueId and its value reset
    member self.Reset() =
      { self with
         detailValueId = None
         value = self.value.Reset()
      }

type DetailValueSpec =
    { detailId: DetailId
      values: ValueSpec list }

type InstanceSpec =
    { entityId: EntityId
      instanceId: InstanceId option
      detailValues: DetailValueSpec list }
      with
      static member New(entityId:EntityId) =
        {
          entityId    = entityId
          instanceId  = None
          detailValues= []
        }

type DatabaseSpec =
    { databaseId: DatabaseId option
      accountId: AccountId
      name: string
    }
    with
    static member Init() =
      {
        databaseId = None
        accountId = AccountId 0
        name = ""
      }

type EntitySpec =
    { entityId: EntityId option
      databaseId: DatabaseId
      name: string
      hasPublicForm: bool
    }
    with
      static member Init() =
        { entityId=None
          databaseId= DatabaseId 0
          name= ""
          hasPublicForm = false
        }

// one item in the list of propositions for a detail
// it is extracted so that the detail id is not replicated
// in each item, with risk of incoherence and need for additional
// checks
type DetailValuePropositionItemSpec =
  { detailValuePropositionId: DetailValuePropositionId option
    value: string
  }
  with
  static member Init() =
    {
      detailValuePropositionId = None
      value = ""
    }

// detail value propositions for a detail
type DetailValuePropositionSpec =
  { detailId: DetailId
    propositions: DetailValuePropositionItemSpec list
  }

type DataTypeSpec =
  { dataTypeId: DataTypeId
    name: string
    className: string
  }

// Define an interface to be implemented by both DetailSpec and EntityDetailSpec. Needed so we can
// have a generic function web.admin.EntityDetail.addNewDetailSection working with both these record types.
// That function defines static type constraints so that its generic type argument has the members name,
// propositions and dataType, but this still does not allow to use the record update syntax {r with propositions=...}.
// Instead, we define this interface and its member will be used to update the propositions of the record.
// The other with* members are needed to define the WebSharper lenses setter functions.
// The accessors are needed to define WebSharper lenses reader function as accessing the record field directly raises
// the error WS9001: Using a trait call requires the Inline attribute; types: 'T0
type IDetailSpec<'T> =
  abstract member withPropositions: Option<List<DetailValuePropositionItemSpec>> -> 'T
  abstract member withDataType: DataTypeSpec -> 'T
  abstract member withName: string -> 'T
  abstract member name: string
  abstract member dataType: DataTypeSpec
  abstract member propositions: Option<List<DetailValuePropositionItemSpec>>

type DetailSpec =
    { detailId: DetailId option
      name: string
      dataType: DataTypeSpec
      propositions: DetailValuePropositionItemSpec list option
      status: DetailStatus.Status
      databaseId: DatabaseId
    }
    interface IDetailSpec<DetailSpec> with
      member self.withPropositions(l) =
        {self with propositions=l}
      member self.withDataType(dt) =
        {self with dataType=dt}
      member self.withName(n) =
        {self with name=n}
      member self.name = self.name
      member self.propositions = self.propositions
      member self.dataType = self.dataType

// An EntityDetailSpec is the same as the DetailSpec, except for the fact that it doesn't
// have a databaseId field.
type EntityDetailSpec =
    { detailId: DetailId option
      name: string
      dataType: DataTypeSpec
      propositions: DetailValuePropositionItemSpec list option
      status: DetailStatus.Status
    }
    interface IDetailSpec<EntityDetailSpec> with
      member self.withPropositions(l) =
        {self with propositions=l}
      member self.withDataType(dt) =
        {self with dataType=dt}
      member self.withName(n) =
        {self with name=n}
      member self.name = self.name
      member self.propositions = self.propositions
      member self.dataType = self.dataType

// record type holding all info for a entities2details entry
// EXCEPT for the detail. This is because defining this lets us
// reuse it for both existing and new details to be added to an entity
// This lets us define a DU holding a different record type for new and existing details.
// If we didn't do this, we would have a DU type for the detail field of the record. However
// this is not optimal because wa have the code
//    createDetailIfNeededAction spec
//    |> DBAction.bind linkDetailAction
// The call to createDetailIfNeededAction would ensure the field of the record is of the case
// holding a DetailID. But the function linkDetailAction would take as argument the record with
// the detail field holding a detail spec of id, without knowing which one it is, even though
// the check was done at the previous step of the pipeline. This would require a new, useless match.
// Moving the DU one layer up avoids this, we always pass as argument a record holding a detail id
// to linkDetailAction
type Entity2DetailInfoSpec =
  { entityId: EntityId
    displayedInList: bool
    maximumNumberOfValues: int
    displayOrder: int
    entities2detailId: Entities2detailId option
  }

// spec adding an existing detail to an entity
type Entity2ExistingDetailSpec =
  { detailId: DetailId
    info : Entity2DetailInfoSpec
  }

// spec adding a to be created detail to an entity
type Entity2NewOrUpdatedDetailSpec =
  { detailSpec: EntityDetailSpec
    info : Entity2DetailInfoSpec
  }

// DU to hold Entity2Detail for both existing and new details
type Entity2DetailSpec = Entity2NewDetailSpec of Entity2NewOrUpdatedDetailSpec
                        |Entity2ExistingDetailSpec of Entity2ExistingDetailSpec

type RelationSideType =  One | Many
module RelationSideType =
    let toId (v:RelationSideType)=
        // IMPROVE: don't hard-code this, but extract from DB
        match v with
        | One -> 1
        | Many -> 2
    let fromId (id:int)=
        // IMPROVE: don't hard-code this, but extract from DB
        match id with
        | 1 -> One
        | _ -> Many


type RelationSpec =
    { relationId : RelationId option
      parentSideType: RelationSideType
      childSideType: RelationSideType
      parentToChildName: string
      childToParentName: string
      parentId: EntityId
      childId: EntityId
    }
