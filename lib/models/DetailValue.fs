module DetailValue

open System
open Ids
open IdsExtensions
open Specs
open DBAction.StaticArguments
open ServerFileAttachmentExtensions

// This table stores all text based details values.
// At time of writing, this is:
//  - FileAttachment
//  - EmailDetailValue
//  - SimpleDetailValue
//  - LongTextDetailValue
//  - WebUrlDetailValue
// The kind of detail value stored in the row is specified in the column `type`.

type DetailValue =
    { id: DetailValueIdType
      detail_id: DetailId
      instance_id: InstanceId
      value: string
      // FIXME: type is equal to detail_values->details->datatypes.class_name
      // Let's try to not use it and remove it later
      ``type``: string
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
    with
    static member Table = "detail_values"
    interface IDetailValue.IDetailValue with
      member this.GetId() = this.id
      member this.GetStringValue() = this.value.ToString()
      member this.GetDetailId() = this.detail_id
      member this.GetInstanceId() = this.instance_id

// Ideally these should be type aliases, but at runtime the type resolves to its alias,
// so we can't use such a type alias to distinct between the types stored in table detail_values
// We cannot inherit from DetailValue as record types cannot be inherited.
// So we duplicated the DetailValue definition.
// START OF DETAIL_VALUE TYPES DUPLICATION
type LongTextDetailValue =
    { id: DetailValueIdType
      detail_id: DetailId
      instance_id: InstanceId
      value: string
      // FIXME: type is equal to detail_values->details->datatypes.class_name
      // Let's try to not use it and remove it later
      ``type``: string
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
    with
    static member Table = "detail_values"
    interface IDetailValue.IDetailValue with
      member this.GetId() = this.id
      member this.GetStringValue() = this.value.ToString()
      member this.GetDetailId() = this.detail_id
      member this.GetInstanceId() = this.instance_id
type SimpleDetailValue =
    { id: DetailValueIdType
      detail_id: DetailId
      instance_id: InstanceId
      value: string
      // FIXME: type is equal to detail_values->details->datatypes.class_name
      // Let's try to not use it and remove it later
      ``type``: string
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
    with
    static member Table = "detail_values"
    interface IDetailValue.IDetailValue with
      member this.GetId() = this.id
      member this.GetStringValue() = this.value.ToString()
      member this.GetDetailId() = this.detail_id
      member this.GetInstanceId() = this.instance_id
type WebUrlDetailValue =
    { id: DetailValueIdType
      detail_id: DetailId
      instance_id: InstanceId
      value: string
      // FIXME: type is equal to detail_values->details->datatypes.class_name
      // Let's try to not use it and remove it later
      ``type``: string
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
    with
    static member Table = "detail_values"
    interface IDetailValue.IDetailValue with
      member this.GetId() = this.id
      member this.GetStringValue() = this.value.ToString()
      member this.GetDetailId() = this.detail_id
      member this.GetInstanceId() = this.instance_id
type EmailDetailValue =
    { id: DetailValueIdType
      detail_id: DetailId
      instance_id: InstanceId
      value: string
      // FIXME: type is equal to detail_values->details->datatypes.class_name
      // Let's try to not use it and remove it later
      ``type``: string
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
    with
    static member Table = "detail_values"
    interface IDetailValue.IDetailValue with
      member this.GetId() = this.id
      member this.GetStringValue() = this.value.ToString()
      member this.GetDetailId() = this.detail_id
      member this.GetInstanceId() = this.instance_id

type FileAttachmentDetailValue =
    { id: DetailValueIdType
      detail_id: DetailId
      instance_id: InstanceId
      value: string
      // FIXME: type is equal to detail_values->details->datatypes.class_name
      // Let's try to not use it and remove it later
      ``type``: string
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
    with
    static member Table = "detail_values"

    // This file attachment's info from the yaml in an object
    member this.attachmentInfo (): ServerFileAttachment.FileAttachment =
        ServerFileAttachment.FileAttachment.FromString this.value

    interface IDetailValue.IDetailValue with
      member this.GetId() = this.id
      member this.GetStringValue() = this.value.ToString()
      member this.GetDetailId() = this.detail_id
      member this.GetInstanceId() = this.instance_id
// END OF DETAIL_VALUE TYPES DUPLICATION

// No DU in record for Legivel
type DetailValueFixture =
    { id: int
      detail_id: int
      instance_id: int
      value: string
      // FIXME: type is equal to detail_values->details->datatypes.class_name
      // Let's try to not use it and remove it later
      ``type``: string
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }



// for file attachment handling
open System.IO

// Those helpers need to be public because they are also used in the remoting functions of the web ui
// to handle deletion of file attachments for a deleted instance.
let getPathElementsAction (detailValueId:int64) (instanceId:int):DBAction.DBAction<PathElements>=
    // Extract the attachment's path elements from the database
    DBAction.querySingleAction
        "select %A as detail_value_id,
                e.id as entity_id,
                e.database_id,
                db.account_id
            from
                instances i
                    join entities e on (e.id=i.entity_id)
                    join databases db on (e.database_id=db.id)
            where i.id=%d"
        (Static detailValueId)
        instanceId

// Helper to build the path to an attachment file
let buildAttachmentPath (attachmentsDir:string)(r:PathElements) =
        Path.Combine(attachmentsDir,buildKey r)

module private Helpers =
    // get file attachment path elements from the database
    let prepareDestination (attachmentsDir:string)(r:PathElements)=
        // We first need to create the directory of the attachment
        let destinationDir = fileAttachmentDir attachmentsDir r
        MyOwnDBLib.Logging.Log.Information($"Creating attachment destination directory {destinationDir}")
        let _ = Directory.CreateDirectory(destinationDir)
        // We return the path where the file will be located. buildKey calls fileAttachmentDir so the values
        // are coherent
        buildAttachmentPath attachmentsDir r
    let getSource (waitRoom:string)(info:ServerFileAttachment.FileAttachment)(r:PathElements)=
        // An upload by an authenticated user is stored under the waitRoom with a path containing the account id
        let withAccount = Path.Combine(waitRoom,string r.account_id,info.s3_key|>Option.get,info.filename)
        // For public forms, there is no account id available for the uploader, and it is set to 0 by convention
        let publicUpload = Path.Combine(waitRoom,"0",info.s3_key|>Option.get,info.filename)
        // If a file exists at the location for an upload by an authenticated user, return that
        // else return the path for a public form upload
        if File.Exists(withAccount) then
            withAccount
        else
            publicUpload
    let moveAttachmentToDestination  source destination (overwrite:bool)=
        MyOwnDBLib.Logging.Log.Information($"Moving uploaded attachment from {source} to {destination}")
        File.Move(source,destination,overwrite)
open Helpers

// We define this type becaue the createAction and updateAction functions need to communicate both an updated detailValueId in case of creation, but also
// an updated value for the file attachments (because the value of the key in the json is only known after creation in the database)
// We cannot put the type definition in the private module as it causes this error:
//The type 'UpdatedDetailValueInfo' is less accessible than the value, member or type 'val createAction: ... -> DBAction.DBAction<UpdatedDetailValueInfo option>' it is used in.
type UpdatedDetailValueInfo =
    {detailValueId:DetailValueIdType;
     value:ServerValue}


// Instance Id is last arg for |> combination
// do not specify the datatype here, it's available via the detail linked.
let createAction (DetailId detailId) (value: ServerValue) (instanceIdIn:InstanceId) : DBAction.DBAction<UpdatedDetailValueInfo option>=
    let instanceId = InstanceId.Get(instanceIdIn)
    // printfFormat value partial application with instanceId and detailId alredy set
    let detailValueActionBase value typ : DBAction.DBAction<DetailValueIdType> =
        DBAction.singleInsertAction "insert into detail_values(instance_id,detail_id,value,type)
                                                                      VALUES
                                                                      (%d,%d,%s,%s) RETURNING id" instanceId detailId value typ

    match value with
    | ServerValue.Simple (Some s) ->
        detailValueActionBase s "SimpleDetailValue"
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.LongText (Some s) ->
        detailValueActionBase s "LongTextDetailValue"
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some

    | ServerValue.Simple None
    | ServerValue.LongText None -> DBAction.retn None
    | ServerValue.DateAndTime (Some dt) ->
        DBAction.singleInsertAction "insert into date_detail_values(instance_id,detail_id,value) VALUES (%d,%d,%A) RETURNING id"
            instanceId detailId (DateAndTime.get dt)
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.DateAndTime None -> DBAction.retn None
    | ServerValue.Email (Some email) ->
        detailValueActionBase (Email.toString email) "EmailDetailValue"
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.Email None -> DBAction.retn None
    | ServerValue.WebURL (Some url) ->
        detailValueActionBase (WebURL.toString url) "WebUrlDetailValue"
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.WebURL None -> DBAction.retn None
    | ServerValue.DdlValueChoice None ->
            // At creationg time, and DdlValueChoice of None means no value was chose, so nothing to insert
            DBAction.retn None
    | ServerValue.DdlValueChoice (Some v) ->
        if detailId <> (DdlChoice.toDetailId v) then
            DBAction.error "Incoherency between detailId and value proposition chosen. This should never have happened!"
        else
            DBAction.singleInsertAction
                "insert into ddl_detail_values(instance_id,detail_id,detail_value_proposition_id) VALUES (%d,%d,%d) RETURNING id"
                instanceId (DdlChoice.toDetailId v) (DdlChoice.toId v)
            |> DBAction.map (fun id -> {detailValueId = id; value = value})
            |> DBAction.map Some
    | ServerValue.Integer (Some i) ->
        DBAction.singleInsertAction
            "insert into integer_detail_values(instance_id,detail_id,value) VALUES (%d,%d,%d) RETURNING id" instanceId
            detailId i
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.Integer None -> DBAction.retn None
    | ServerValue.FileAttachment (Some info) ->
        if info.uploaded then
            // We want the detail_value_id to be present in the json we save as the value so that the display of a link
            // to download the file can be based only on the json value. For that we first select the next_val of the sequence used
            // to automatically assign id in this table. This effectively reserves the id for us and we can manually use it for the id
            // column, but also add it in the json.
            DBAction.querySingleAction "select nextval('detail_values_id_seq'::regclass)"
            // FIXME: the id we get from the database is an int64, but all other id types are int. We need to switch to int64 for id types!
            |> DBAction.bind (fun (detailValueId:int64) ->
                getPathElementsAction detailValueId instanceId
            )
            |> DBAction.bind (fun (r:PathElements)->
                ServerFileAttachment.FileAttachment.withDirsFromConfig (fun waitRoom attachmentsDir ->
                    let destination = prepareDestination attachmentsDir r
                    let source = getSource waitRoom info r
                    moveAttachmentToDestination source destination false
                    // update the file attachment info: add the id we reserved in the FileAttachment instance and the attachment's key
                    let key = buildKey r
                    // We also update indb to make it true
                    let infoWithId = { info with detail_value_id = Some r.detail_value_id; s3_key=Some key; indb=true}
                    let newValue = ServerValue.FileAttachment (Some infoWithId)
                    // Issue the same query as for other detail_values, but specify the id explicitly
                    DBAction.singleInsertAction "insert into detail_values(id, instance_id,detail_id,value,type)
                                                                                VALUES
                                                                                (%d, %d,%d,%s,%s) RETURNING id"
                                                r.detail_value_id instanceId detailId
                                                (infoWithId.Serialize())
                                                "FileAttachmentDetailValue"
                    |> DBAction.map (fun id -> {detailValueId = id; value = newValue})

                )
            )
            |> DBAction.map Some
        else
            DBAction.retn None
    | ServerValue.FileAttachment None -> DBAction.retn None

let updateAction (DetailId detailId) (dvid: DetailValueIdType) (value: ServerValue) (instanceIdIn:InstanceId) :DBAction.DBAction<UpdatedDetailValueInfo option>=
    // return Some if a detail value exists after the update, None if no detail value exists after the update
    let instanceId = InstanceId.Get instanceIdIn
    let detailValueActionBase (v:string)(did:int)(id:int):DBAction.DBAction<DetailValueIdType>=
        DBAction.singleInsertAction
            "update detail_values set value=%s where detail_id = %d and id= %d RETURNING id" v did id

    match value, dvid with
    | ServerValue.Simple (Some s), DetailValueId id
    | ServerValue.LongText (Some s), DetailValueId id ->
        detailValueActionBase s detailId id
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.Simple None, DetailValueId id
    | ServerValue.LongText None, DetailValueId id ->
            DBAction.statementAction "delete from detail_values where detail_id=%d and id=%d" detailId id
            // we ignore the number of rows deleted, and return None to signal there's no value here for the spec
            |> DBAction.bind (fun numberOfRows -> DBAction.retn None)
    | ServerValue.DateAndTime None, DateDetailValueId id ->
            DBAction.statementAction "delete from date_detail_values where detail_id=%d and id=%d" detailId id
            // we ignore the number of rows deleted, and return None to signal there's no value here for the spec
            |> DBAction.bind (fun numberOfRows -> DBAction.retn None)
    | ServerValue.DateAndTime (Some dt), DateDetailValueId id ->
        DBAction.singleInsertAction "update date_detail_values set value=%A where detail_id=%d and id=%d RETURNING id"
            (DateAndTime.get dt) detailId id
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.Email (Some email), DetailValueId id ->
        detailValueActionBase (Email.toString email) detailId id
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.Email None, DetailValueId id ->
            DBAction.statementAction "delete from detail_values where detail_id=%d and id=%d" detailId id
            // we ignore the number of rows deleted, and return None to signal there's no value here for the spec
            |> DBAction.bind (fun numberOfRows -> DBAction.retn None)
    | ServerValue.WebURL (Some url), DetailValueId id ->
        detailValueActionBase (WebURL.toString url) detailId id
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.WebURL None, DetailValueId id ->
            DBAction.statementAction "delete from detail_values where detail_id=%d and id=%d" detailId id
            // we ignore the number of rows deleted, and return None to signal there's no value here for the spec
            |> DBAction.bind (fun numberOfRows -> DBAction.retn None)
    | ServerValue.FileAttachment (Some info), DetailValueId id ->
            if not info.indb then
                DBAction.querySingleAction
                    "select value from detail_values where id = %d" (info.detail_value_id |> Option.get)
                |> DBAction.map (fun s ->
                    ServerFileAttachment.FileAttachment.FromString s
                )
                |> DBAction.bind (fun oldInfo ->
                    let attachmentPath = oldInfo.filename
                    ServerFileAttachment.FileAttachment.withDirsFromConfig (fun waitRoom attachmentsDir ->
                        // FIXME: we move the file then update the db, but what if the db update fails? We don't roll back to
                        // the previous file
                        let r = PathElements.FromKey (oldInfo.s3_key |> Option.get)
                        let destination = prepareDestination attachmentsDir r
                        let source = getSource waitRoom info r
                        // We do not overwrite an existing file. Doing so could bring consistency problems, eg if the DB transaction
                        // we are operating in is canceled. The file attachment was overwritten, but all detail values updated in
                        // this transaction were rolled back.
                        moveAttachmentToDestination source destination false

                        let newInfo ={ oldInfo with filename = info.filename;filetype=info.filetype}
                        let updatedJson = newInfo.Serialize()
                        let newValue = ServerValue.FileAttachment (Some newInfo)

                        detailValueActionBase updatedJson detailId (info.detail_value_id |> Option.get)
                        // LEARN: why does calling the following not work and raise the error empty result, but required (failed update? forgot to include 'RETURNING id' in query?)
                        // The query was copied as is from the function called.
                        //DBAction.singleInsertAction
                        //    "update detail_values set value=%s where detail_id = %d and id= %d RETURNING id" updatedJson  (info.detail_value_id |> Option.get) detailId|> DBAction.map Some
                        |> DBAction.map (fun id -> {detailValueId = id; value = newValue})
                        |> DBAction.map Some
                    )
                )

            else
                DBAction.retn None
    | ServerValue.FileAttachment None, DetailValueId id ->
            DBAction.statementAction "delete from detail_values where detail_id=%d and id=%d" detailId id
            // we ignore the number of rows deleted, and return None to signal there's no value here for the spec
            |> DBAction.bind (fun numberOfRows -> DBAction.retn None)
    | ServerValue.DdlValueChoice None, DdlDetailValueId id ->
            // At updating time, and DdlValueChoice of None means no value was chose, so if it was chosen before, we need to delete it
            // I think those two statements are equivalent, but I didn't think much more about it. The commented one is inpired from the createAction.
            //DBAction.statementAction "delete from ddl_detail_values where instance_id=%d and detail_id=%d" instanceId detailId
            DBAction.statementAction "delete from ddl_detail_values where detail_id=%d and id=%d" detailId id
            // we ignore the number of rows deleted, and return None to signal there's no value here for the spec
            |> DBAction.bind (fun numberOfRows -> DBAction.retn None)
    | ServerValue.DdlValueChoice (Some v), DdlDetailValueId id ->
        if detailId <> (DdlChoice.toDetailId v) then
            DBAction.error "Incoherency between detailId and value proposition chosen. This should never have happened!"
        else
            DBAction.singleInsertAction
                "update ddl_detail_values set detail_value_proposition_id=%d where detail_id=%d and id=%d RETURNING id "
                (DdlChoice.toId v) detailId id
            |> DBAction.map (fun id -> {detailValueId = id; value = value})
            |> DBAction.map Some
    | ServerValue.Integer (Some i), IntegerDetailValueId id ->
        DBAction.singleInsertAction "update integer_detail_values set value=%d where detail_id = %d and id=%d RETURNING id" i
            detailId id
        |> DBAction.map (fun id -> {detailValueId = id; value = value})
        |> DBAction.map Some
    | ServerValue.Integer None, IntegerDetailValueId id ->
            DBAction.statementAction "delete from integer_detail_values where detail_id=%d and id=%d" detailId id
            // we ignore the number of rows deleted, and return None to signal there's no value here for the spec
            |> DBAction.bind (fun numberOfRows -> DBAction.retn None)
    | _ -> failwith "unhandled detailvalue type or mispatch between detailvalue type and its id type"

let createOrUpdateAction (detailId: DetailId) (spec: ValueSpec) (instanceId: InstanceId) =
    match spec.value,spec.detailValueId with
    | ClientProposed _ , _ ->
       // FIXME: log error
       DBAction.zero
    | ServerValidated value, None -> createAction detailId value instanceId
    | ServerValidated value, Some id -> updateAction detailId id value instanceId
// traverse values for one detailValue
let rec createActionFromValueSpecList (instanceId: InstanceId) (detailId: DetailId) (specs: ValueSpec list) =
    let cons head tail = head :: tail
    let (<*>) = DBAction.apply
    let retn = DBAction.retn

    match specs with
    | [] -> DBAction.retn []
    | spec :: tail ->
        // see function with similar approach below for explanations (createActionsFromDetailValueSpecList)
        (DBAction.retn cons)
        // create an action with it, that we DBAction.map to returns an updated spec
        <*> (createOrUpdateAction detailId spec instanceId
             |> DBAction.map (fun updateOption ->
                match updateOption with
                |None ->
                 { spec with
                       detailValueId = None }
                |Some r ->
                 { spec with
                       detailValueId = Some r.detailValueId
                       value = ServerValidated r.value
                        }))
        <*> createActionFromValueSpecList instanceId detailId tail

// traverse detailValues
// This function receives a list of DetailValueSpecs, and needs to return a DBAction wrapping
// a list of updated DetailValueSpecs (that now includes ids of rows inserted by the action)
let rec createActionsFromDetailValueSpecList (instanceId: InstanceId) (values: DetailValueSpec list) =
    // We want to return one DBAction holding a list.
    // Just using map on the values would return a list of DBActions
    // See traverse function in F# for fun and profit for details.
    let cons head tail = head :: tail
    let (<*>) = DBAction.apply
    let retn = DBAction.retn

    let action =
        match values with
        | [] -> retn []
        | detailValueSpec :: tail ->
            // We need to use DBAction.apply, which has the signature
            // DBAction<'a -> 'b> -> DBAction<'a> -> DBAction<'b>
            // As the function needs to be wrapped, we wrap it with retn defined above
            // We can then DBAction.apply this wrapped function to DBActions.
            // The first value passed is a DBAction wrapping the value that will be the
            // head of the list inside the returned DBAction.
            // The second value passed is a DBAction wrapping a value that will be the tail
            // of the list inside the returned DBAction. The tail of a list is a list, so that the
            // second value passed wraps a list, which is exactly the type returned by this function,
            // enabling a recursive call.
            (retn cons)
            <*> (createActionFromValueSpecList instanceId detailValueSpec.detailId detailValueSpec.values
                 |> DBAction.map (fun v -> { detailValueSpec with values = v }))
            <*> createActionsFromDetailValueSpecList instanceId tail

    action

let byId (d: DetailValueIdType): DBAction.DBAction<DetailValue> =
    match d with
    | DetailValueId id -> DBAction.querySingleAction "select * from detail_values where id=%d" id
    | _ -> DBAction.error "Can only handle the union case DetailValueId here"

// This function returns the Specs.ServerValue for a detail and a string representation of the value as received from
// the client.
// It retrieves the class_name of the detail's data type, and delegates other work to Specs.ServerValue.GetAction
// if value is None, no value was provided for that detail
open SpecExtensions
let buildServerValueAction (id:DetailId) (value:string option) :DBAction.DBAction<Specs.ServerValue>=
  // get Detail by its id
  DetailExtensions.byIdAction id
  // get its datatype's class_name
  |> DBAction.bind (fun detail ->
    let (DataTypeId dataTypeId) = detail.data_type_id
    // FIXME: we should memoize this
    DBAction.querySingleAction "select class_name from data_types where id = %d" dataTypeId
    |> DBAction.map (fun className -> (detail,className))
  )
  // use the class_name to build a Specs.Value
  // Specs.Value.TryBuild should not fail as values come from the DB, but
  // its possible error is still wrapped in a DBAction if this occurs
  |> DBAction.bind (fun (detail,className) ->
    Specs.ServerValue.GetAction detail.id detail.name className value
  )

let rec toSpecAction (dvs:IDetailValue.IDetailValue list) : DBAction.DBAction<DetailValueSpec> =
    // Internal function
    // Return an action that accumulates detail values passed in dvs in the DetailValueSpec accumulator
    let rec toSpecActionAcc (acc:DBAction.DBAction<DetailValueSpec>) (dvs:IDetailValue.IDetailValue list): DBAction.DBAction<DetailValueSpec>=
        match dvs with
        | [] -> acc
        // FIXME: the tail is never handled. Ok for now that only one value per detail is supported
        | dv::_tail ->
            // As we take the value from the db, we always pass a Some as last argument and never a None
            let valueAction = (buildServerValueAction (dv.GetDetailId()) (Some (dv.GetStringValue())))
            valueAction
            |> DBAction.bind (fun v ->
                acc
                |> DBAction.map (fun accumulator ->
                    {accumulator with values = {detailValueId = Some (dv.GetId()); value= (ServerValidated v)} :: accumulator.values}) )
    // function body
    let expectedDetailId = dvs.Item(0).GetDetailId()
    let expectedInstanceId = dvs.Item(0).GetInstanceId()
    let isInputValid (dvs:IDetailValue.IDetailValue list) =
        // check all dvs are for the same instance and detail
        dvs
        |> List.forall (fun dv ->
                            dv.GetDetailId() = expectedDetailId
                            && dv.GetInstanceId() = expectedInstanceId)

    if isInputValid dvs then
        toSpecActionAcc (DBAction.retn { detailId = expectedDetailId; values=[]}) dvs
    else
        DBAction.error "detail values passed do not all belong to same detail_id and instance_id"
