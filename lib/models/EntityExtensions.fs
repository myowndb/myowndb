module EntityExtensions

open Entity
open DBAction
open Ids

// Function to return the PrintfFormat from the SQL string, that will be passed to DBAction.queryAction.
// The type inference works because of the explicit type annotation at the call site.
// FIXME: details_status seems to be unused and unset actually. that's why we have a full outer join.
// We should either use it or drop it.
let entityDetailsSQL=
    Utils.getPrintfFormat "
select
ed.id  ,ed.entity_id ,ed.detail_id ,ed.displayed_in_list_view ,ed.maximum_number_of_values ,ed.display_order,
d. name,d.lock_version,
ds.name as status,
dt.name as data_type, dt.class_name
from entities2details ed
join details d
    on (ed.detail_id=d.id)
full outer join detail_status ds
    on (d.status_id=ds.id)
join data_types dt
    on (d.data_type_id=dt.id)
where ed.entity_id=%d
order by ed.display_order
"

// Function to return the DBAction<EntityDetail> for entity with id entityId.
// The explicit type annotation for the return value is required.
// Without this type annotation, it is impossible to determine the type of the value
// returned by the PrintfFormat's callback once all arguments have been consumed.
// Said callback is called from Util.formatStringHandler.
let entityDetailsAction (EntityId entityId) :DBAction<EntityDetail>= (DBAction.queryAction entityDetailsSQL) entityId

let byIdAction (id:EntityId ) : DBAction.DBAction<Entity>=
    DBAction.querySingleAction "select * from entities where id = %d" (EntityId.Get id)

let byIdForPublicFormAction (id:int) =
    DBAction.querySingleAction "select * from entities where id=%d and has_public_form=true" id