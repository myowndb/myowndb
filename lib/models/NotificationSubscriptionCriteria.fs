module NotificationSubscriptionCriteria

open Ids

// Form easier to manipulate of event source filters
type SourceFilterRecord =
  {
    field: string
    value: int
  }
// How do we filter source of events?
type SourceFilter = |Entity of EntityId
with
  // Used for inserting
  override self.ToString() =
    match self with
    | Entity (EntityId id) -> $"""{{ "entity_id" : {id} }}"""
// Used for querying
  member self.ToRecord() =
    match self with
    | Entity (EntityId id) -> { field = "entity_id"; value = id }

// What events do trigger notifications?
type Event = |AfterCreate
// What event subjects trigger notifications?
type Source = |Instance
// Over which protocol is notification sent?
type Protocol = |Smtp
// Who gets the notifications?
type DestinationType = |User
