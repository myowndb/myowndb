module SpecExtensions
open Ids
open Specs
open ServerFileAttachmentExtensions
// These SpecsExtensions add methods to our Specs types, so that making the types available client side can be done easily.
// This is needed becaue Value.GetAction, returning a DBAction, does not make sense on the client.
type ServerValue with
    // Returns a DBAction wrapping the ServerValue
    static member GetAction (detailId:DetailId)(detailName:string)(className:string)(value:string option) : DBAction.DBAction<ServerValue>=
      match className with
      |"SimpleDetailValue" -> DBAction.retn (ServerValue.Simple value)
      |"LongTextDetailValue" -> DBAction.retn (ServerValue.LongText value)
      |"EmailDetailValue" ->
        match value with
        | Some s ->
            Email.fromString s
            |> DBAction.fromResult
            |> DBAction.map Some
            |> DBAction.map ServerValue.Email
            |> DBAction.mapError (fun e ->
                // Log as Debug as this is expected
                MyOwnDBLib.Logging.Log.Debug($"Email {s} for {detailId} could not be parsed")
                // Return an explicit error
                Error.fromString $"Email {s} invalid for detail {detailName}"
            )
        | None -> DBAction.retn (ServerValue.Email None)
      |"WebUrlDetailValue" ->
        match value with
        | Some s ->
            WebURL.fromString s
            |> DBAction.fromResult
            |> DBAction.map Some
            |> DBAction.map ServerValue.WebURL
            |> DBAction.mapError (fun e ->
                // Log as Debug as this is expected
                MyOwnDBLib.Logging.Log.Debug($"WebURL {s} for {detailId} could not be parsed")
                // Return an explicit error
                Error.fromString $"WebURL {s} invalid for detail {detailName}"
            )
        | None -> DBAction.retn (ServerValue.WebURL None)
      |"DateDetailValue" ->
        match value with
        | Some s ->
            DateAndTime.fromString s
            |> DBAction.fromResult
            |> DBAction.map Some
            |> DBAction.map ServerValue.DateAndTime
            |> DBAction.mapError (fun e ->
                // Log as Debug as expected
                MyOwnDBLib.Logging.Log.Debug($"DateDetailValue {s} for {detailId} could not be parsed")
                // Return an explicit error
                Error.fromString $"DateDetailValue {s} invalid for detail {detailName}"
            )
        | None -> DBAction.retn (ServerValue.DateAndTime None)
      |"DdlDetailValue" ->
        match value with
        | Some s ->
            s
            |> int
            |> DetailValuePropositionId
            |> DdlChoice.fromIdAction detailId
            |> DBAction.map Some
            |> DBAction.map ServerValue.DdlValueChoice
            |> DBAction.mapError (fun e ->
                // Log as Error as this should not be expected
                MyOwnDBLib.Logging.Log.Error($"DdlDetailValue choice {s} for {detailId} was not found")
                // Return an explicit error
                Error.fromString $"DdlDetailValue {s} invalid for detail {detailName}"
            )
        | None -> DBAction.retn (ServerValue.DdlValueChoice None)
      |"FileAttachmentDetailValue" ->
        match value with
        | Some s ->
            s
            |> ServerFileAttachment.FileAttachment.FromString
            |> DBAction.retn
            |> DBAction.map Some
            |> DBAction.map ServerValue.FileAttachment
            |> DBAction.mapError (fun e ->
                // Log as Error as this should not be expected
                MyOwnDBLib.Logging.Log.Error($"FileAttachment YAML {s} for {detailId} could not be parsed")
                // Return an explicit error
                Error.fromString $"FileAttachment {s} could not be parsed for detail {detailName}"
            )
        | None -> DBAction.retn (ServerValue.FileAttachment None)
      |"IntegerDetailValue" ->
        match value with
        | Some s ->
            match System.Int64.TryParse s with
            | true, i -> DBAction.retn (ServerValue.Integer (Some i))
            | _ -> // Log as Debug here as this could be common user error
                   MyOwnDBLib.Logging.Log.Debug($"Integer detail value {s} for {detailId} could not be parsed")
                   DBAction.error $"integer detail value {s} for {detailName} could not be parsed"
        | None -> DBAction.retn (ServerValue.Integer None)
      | _ -> failwithf "Unknown data type with class name %s" className
