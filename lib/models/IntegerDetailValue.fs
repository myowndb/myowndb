module IntegerDetailValue

open System

type IntegerDetailValue =
    { id:  Ids.DetailValueIdType
      detail_id: Ids.DetailId
      instance_id: Ids.InstanceId
      value: int64
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
    with
    static member Table = "integer_detail_values"
    interface IDetailValue.IDetailValue with
      member this.GetId() = this.id
      member this.GetStringValue() = this.value.ToString()
      member this.GetDetailId() = this.detail_id
      member this.GetInstanceId() = this.instance_id

type IntegerDetailValueFixture =
    { id:  int
      detail_id: int
      instance_id: int
      value: int
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
