module AccountType

type AccountType =
    { id: int
      name: string
      active: bool
      free: bool
      number_of_users: string
      number_of_databases: string
      monthly_fee: float
      maximum_file_size: int
      maximum_monthly_file_transfer: int
      maximum_attachment_number: int }
