module ServerFileAttachment
type FileAttachment =
    // Some fields are Option because not available before saved to the database
    // detail_value_id is int64 as that's the type we get when calling the postgresql function nextval,
    // which we must use to include the detail_value_id in the json
    { detail_value_id: int option
      // S3 key was defined in rails app as
      // %Q{#{account_id}/#{database_id}/#{entity_id}/#{instance_id}/#{detail_value_id}
      s3_key: string option
      filetype: string option
      filename: string
      uploaded: bool
      // Flag needed to distinguish new files that have been uploaded in the waitRoom from
      // file which are also referenced from the database. This allows to display file attachments
      // correctly in the instance form.
      indb: bool
    }

// We introduce this type because some values extracted from the database may miss the indb field.
// A record field that is an option is set to None by Json parsers when that field is not present in
// the string being parsed. We use this behaviour here.
type FileAttachmentWithOptionalIndb =
    { detail_value_id: int option
      // S3 key was defined in rails app as
      // %Q{#{account_id}/#{database_id}/#{entity_id}/#{instance_id}/#{detail_value_id}
      s3_key: string option
      filetype: string option
      filename: string
      uploaded: bool
      // Flag needed to distinguish new files that have been uploaded in the waitRoom from
      // file which are also referenced from the database. This allows to display file attachments
      // correctly in the instance form.
      indb: bool option
    }

// Define this function at module level to make it available to WebSharper, otherwize
// causing "error WS9001: No client-side support for member of F# record type, add JavaScript attribute:"
// even though the file is added to wsproxies.
let New():FileAttachment =
  { detail_value_id= None
    s3_key= None
    filetype= None
    filename= ""
    uploaded= false
    indb= false
  }

let ToYaml (fa:FileAttachment) =
  $"""
  ---
  :uploaded:{fa.uploaded}
  :filename: {fa.filename}
  { if fa.s3_key.IsSome then ":s3_key: "+ fa.s3_key.Value else ""}
  { if fa.filetype.IsSome then ":filetype: "+ fa.filetype.Value else ""}
  """

// Build a FileAttachment from a yaml string
let fromYaml (s:string): FileAttachment =
  let jsonMap =
      s.Split([|'\n'|])
      // Split key from value
      |> Array.map (fun l -> l.Split(':') )
      // Filter out first and last line which don't contain values
      |> Array.filter (fun a -> Array.length a > 1)
      // Build a map corresponding to the yaml, trimming values of possible
      // problematic characters.
      |> Array.fold
          (fun (acc:Map<string,string>) parts ->
              acc.Add(
                  parts[1].Trim([|' ';'"'|]),
                  parts[2].Trim([|' ';'"'|]))
          )
          (Map.empty<string,string>)

  { detail_value_id= jsonMap.TryFind("detail_value_id") |> Option.map int
    s3_key= jsonMap.TryFind("s3_key")
    filetype= jsonMap.TryFind("filetype")
    filename= jsonMap["filename"]
    // If we have a yaml format, it can only come from the previous code base,
    // and so it must have been uploaded if its filename is not empty
    uploaded= (jsonMap.TryFind("s3_key").IsSome)
    // indb is true as a yaml value can only come from the database
    indb = true
  }

// As the indb field was added later, some values in the database don't have it. As a consequence,
// we extract the value from the database as an option field, and set with this function to get the FileAttachment instance
let augmentWithIndb (fromDb:FileAttachmentWithOptionalIndb):FileAttachment =
  { detail_value_id= fromDb.detail_value_id
    s3_key= fromDb.s3_key
    filetype= fromDb.filetype
    filename= fromDb.filename
    uploaded= fromDb.uploaded
    indb = (
        match fromDb.indb with
        // If we have a  value in the database, return it.
        // Otherwise,see if we have a '/' in the key, indicating it was saved on the disk
        // and saved in the database
        | Some b -> b
        | None ->
          match fromDb.s3_key with
          | Some k -> k.IndexOf("/") <> -1
          | None -> false
    )
  }
