module DetailExtensions


open Detail
open Ids



let byIdAction (id:DetailId) : DBAction.DBAction<Detail>=
  DBAction.querySingleAction "select * from details where id = %d" (DetailId.Get id)

let getSpecAction (id:DetailId) : DBAction.DBAction<Specs.DetailSpec> =
  id
  |> byIdAction
  |> DBAction.bind (fun detail ->
     let (DataTypeId dataTypeId) = detail.data_type_id
     DBAction.querySingleAction "select * from data_types where id = %d" dataTypeId
     |> DBAction.map (fun (dataType:DataType.DataType) ->
      (detail,dataType)
     )
  )
  |> DBAction.bind (fun  (detail:Detail,dataType:DataType.DataType) ->
     // Define the base record which does not hold value propositions.
     // For details wtihout propositions, this is what will be returned
     let specWithoutPropositions:Specs.DetailSpec =
      { detailId= Some detail.id
        name = detail.name
        dataType =
          { dataTypeId = dataType.id
            name= dataType.name
            className = dataType.class_name
          }
        propositions = None
        // FIXME: we do not have inactive details. This field should be retired or made relevant
        status = DetailStatus.Status.Active
        databaseId = detail.database_id
      }
     // extract the int value from the id
     let (DataTypeId dataTypeIdInt) = detail.data_type_id
     // build query to determine if we need to include value propositions
     DBAction.queryAction "select count(*) from data_types where id = %d and name='madb_choose_in_list'" dataTypeIdInt
     |> DBAction.bind
      (fun count ->
        match count with
        // No proposition, return the base record we have built
        | 0L -> DBAction.retn specWithoutPropositions
        // Need to get propositions from the database
        | 1L ->
          let (DetailId detailIdInt) = detail.id
          // extract the rows from the database
          DBAction.queryAction "select * from detail_value_propositions where detail_id = %d order by id" detailIdInt
          // map them to theirs specs
          |> DBAction.map
            (fun (proposition:DetailValueProposition) ->
              // assignation to add required type annotation
              let r:Specs.DetailValuePropositionItemSpec =
                      { detailValuePropositionId = (Some proposition.id)
                        value = proposition.value
                      }
              r

          )
          // we need to return the list, so workOnList
          |> DBAction.workOnList
          // update base record with the propositions
          |> DBAction.map (fun propositions -> {specWithoutPropositions with propositions = Some propositions})
        | _ -> failwith "should be impossible to have more than on deta type matchind"
     )
  )


let linkableDetailsForEntityAction (entity:Entity.Entity) : DBAction.DBAction<Detail> =
    DBAction.queryAction
      "select * from details where database_id = %d and id not in (select detail_id from entities2details where entity_id = %d) order by details.id;"
      (entity.database_id)
      (entity.id|>EntityId.Get)

// Join to accounts to ensure access is warranted
let getDetailSpecsForDatabaseAction (accountIdIn:AccountId)(databaseIdIn:DatabaseId) :DBAction.DBAction<Specs.DetailSpec>=
    let (AccountId accountId) = accountIdIn
    let (DatabaseId databaseId) = databaseIdIn
    DBAction.queryAction "select d.id from details d join databases db on (db.id=d.database_id) join accounts acc on (acc.id=db.account_id) where db.id=%d and acc.id=%d order by d.id" databaseId accountId
    |> DBAction.bind (fun (detailId:int) ->
      getSpecAction (DetailId detailId)
    )
