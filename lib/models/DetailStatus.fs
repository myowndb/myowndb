module DetailStatus

type Status = Active | Inactive

let statusId  = function
                | Active -> 1
                | Inactive -> 2