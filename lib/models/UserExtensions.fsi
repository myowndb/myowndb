module UserExtensions
open User

/// get a user by id, returns a DBAction.
val byId: int -> DBAction.DBAction<User>
/// get a user by Email.t. returns a DBAction.
val byEmail: Email.t -> DBAction.DBAction<User>
val getAllForAccountAction: int -> DBAction.DBAction<User>
val setLoggedInAtAction: string -> DBAction.DBAction<DBTypes.ImpactedRows>
val authenticateEmailAction: string -> string -> DBAction.DBAction<User>
val validatePassword: string -> User -> DBResult.DBResult<User>
val updatePasswordAction: string -> User -> DBAction.DBAction<DBTypes.ImpactedRows>
val authenticateUser: string -> User -> DBResult.DBResult<User>
val authenticateEmailAction: string -> string -> DBAction.DBAction<User>
val randomString: int -> string
