module DatabaseExtensions

open Database
open Ids

let byIdAction (databaseId:Ids.DatabaseId): DBAction.DBAction<Database> =
    DBAction.querySingleAction "select * from databases where id = %d" (databaseId|>DatabaseId.Get)
