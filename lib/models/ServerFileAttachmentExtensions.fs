module ServerFileAttachmentExtensions
open FSharp.Json
open ServerFileAttachment
open System
open System.IO


type PathElements = {detail_value_id:int;entity_id:int64;database_id:int64;account_id:int64 }
    with
    static member FromKey(key:string) =
        let elements = key.Split("/")
        if Array.length elements = 4 then
            {
                account_id = int64 elements[0]
                database_id= int64 elements[1]
                entity_id  = int64 elements[2]
                detail_value_id = int elements[3]
            }
        // In rails app we had the instance id, which we now leave out
        else if Array.length elements = 5 then
            {
                account_id = int64 elements[0]
                database_id= int64 elements[1]
                entity_id  = int64 elements[2]
                // leave out instance id
                detail_value_id = int elements[4]
            }
        else
            MyOwnDBLib.Logging.Log.Warning("Failed parsing of file attachment key, wrong number of elements")
            raise (Exception("failed parsing key in json for file attachment"))


type ServerFileAttachment.FileAttachment with

    member this.Serialize():string=
          Json.serialize(this)
    // Function converting the information about a file attachment stored in the database
    // in the yaml format to a FileAttachment instance
    static member FromString (s:string) : ServerFileAttachment.FileAttachment=
        // See DetailValueFormatter.fs in webui for details
        // We get the yaml string
        //  ---
        //  :uploaded: true
        //  :filename: logo-Ubuntu.png
        //  :s3_key: 1/6/101/202/1215
        //  :valueid: 1215
        //  :detail_value_id: 1215
        //  :filetype: image/png
        // which is splitted in lines
        //  line: "---"
        //  line: ":uploaded: true"
        //  line: ":filename: logo-Ubuntu.png"
        //  line: ":s3_key: 1/6/101/202/1215"
        //  line: ":valueid: 1215"
        //  line: ":detail_value_id: 1215"
        //  line: ":filetype: image/png"
        //  line: ""


        if s.StartsWith("---") then
          fromYaml s
        else
          // We deserialise to an anonymous record because the indb is not always present in the
          // database (values from the rails app don't have it)
          let fromDb:FileAttachmentWithOptionalIndb = Json.deserialize(s)
          // After deserialising from the database, we add the indb.
          augmentWithIndb fromDb

    // Helper to use the waitRoom and attachment path config values.
    // It tries to extract config values from the map and then runs the callback with extracted values, but also
    // raises an error if values not found
    static member withDirsFromConfig  (callback: string -> string -> DBAction.DBAction<_>) =
        // Access the config
        DBAction.config
        // Then use it to pass values to the callback. As the callback returns an DBAction, we use DBAction.bind
        |> DBAction.bind  ( fun config ->

            let waitFound,waitRoom= config.TryGetValue("myowndb:fileAttachments:waitRoom")
            let pathFound, attachmentsDir= config.TryGetValue("myowndb:fileAttachments:path")
            if pathFound && waitFound then
                callback waitRoom attachmentsDir
            else
                MyOwnDBLib.Logging.Log.Error("Error getting myowndb:fileAttachments:waitRoom or myowndb:fileAttachments:path from config")
                raise (System.Exception("Problem getting waitRoom and fileAttachment paths in DetailValue.createAction"))
            )

// Functions to compute file attachment paths from theyr PathElements instance.
// These different functions generate values that are all consistent.
let directoryPart (r:PathElements) =
    Path.Combine(string r.account_id,string r.database_id,string r.entity_id)
let buildKey (r:PathElements) =
    Path.Combine(directoryPart r, string r.detail_value_id)
let fileAttachmentDir (attachmentsDir:string)(r:PathElements)=
  System.IO.Path.Combine(attachmentsDir,directoryPart r)