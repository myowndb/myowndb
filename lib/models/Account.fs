module Account

open System

type Account =
    { id: int
      account_type_id: int
      name: string
      street: string
      zip_code: string
      city: string
      country: string
      status: string
      end_date: DateTime option
      subscription_id: string option
      subscription_gateway: string option
      vat_number: string option
      attachment_count: int option
      lock_version: int option
      created_at: DateTime option
      last_login_at: DateTime option }
