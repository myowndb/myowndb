module ImportMapping

open Ids

// A dedicate type to map a column `csvColumn` to detail with `detailId`
type Mapping = {
  csvColumn: string
  detailId: DetailId
}
