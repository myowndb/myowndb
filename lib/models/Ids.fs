module Ids
// All I models' Id types are put in the modules Ids to break a circular dependency as described  here:
// I wanted to use a type InstanceSpec.Spec holding the specification of a database record to create or update.
// If the objects exists, the id if Some value and we need to update. If the id is None, we need
// to create the entry.
// Here's an hypothetical example:
//
// module InstanceSpec
//
// type Spec = { id:    Instance.Id option,
//               name:  Detail.Name,
//               value: Value.T }
//
// The Spec type references multiple other types used in the app, as illustrated here by Detail.Name and Value.T.
// So I thought I'd add InstanceSpec at the end of the fsproj file.
//
// However, the Instance module itself would need to also use InstanceSpec like this:
//
// module Instance
//
// let createOrUpdate (spec:InstanceSpec.Spec) = ...
//
// Leading to a circular dependency...

open SerialisableId

// Make the constructor internal so we can still easily instanciate it in
// the lib, but others needs to use the Validate method
type InstanceId = internal InstanceId of int
    with
        static member Get (InstanceId id) = id
        interface SerialisableId with
            member self.getJSONInfo() =
                let id = InstanceId.Get self
                {| name= "InstanceId"; value= id |}

type EntityId = internal EntityId of int
    with
        static member Get (EntityId id) = id
        interface SerialisableId with
            member self.getJSONInfo() =
                let id = EntityId.Get self
                {| name= "EntityId"; value= id |}

type DetailId = DetailId of int with
    static member Get (DetailId id) = id


type Entities2detailId = Entities2detailId of int

type DataTypeId = DataTypeId of int

type DetailStatusId = DetailStatusId of int

type DetailValuePropositionId = DetailValuePropositionId of int
// FIXME: switch DetailValueId to int64
type DetailValueIdType =
    | DetailValueId of int
    | DateDetailValueId of int
    | DdlDetailValueId of int
    | IntegerDetailValueId of int
module DetailValueIdType =
    let get (v:DetailValueIdType) =
        match v with
        | DetailValueId i -> i
        | DateDetailValueId i -> i
        | DdlDetailValueId i -> i
        | IntegerDetailValueId i -> i

type LinkId = LinkId of int

type RelationId =
    internal RelationId of int
    with
    static member Get(RelationId id) = id
    interface SerialisableId with
        member self.getJSONInfo() =
            let id = RelationId.Get self
            {| name= "RelationId"; value= id |}

type RelationAndDirection = ChildrenVia of RelationId | ParentsVia of RelationId

type AccountId = AccountId of int
type DatabaseId =
  // Constructor is public because the webui needs to instanciate it in admin/DatabaseDisplay
  // when creating a new Detail.
  // Adding a phantom type is not immediate because it bubbles up every record type in which
  // DatabaseId is present.
  // As a consequence: leave it public, but be cautious and don't trust its values
  DatabaseId of int
    with
    static member Get(DatabaseId id) = id
module DatabaseIdHelpers =
    let get (DatabaseId id) = id
type UserId = UserId of int
    with
        static member Get (UserId id) = id
type UserTypeId = UserTypeId of int

type ImportId = ImportId of int
    with
        static member Get (ImportId id) = id

type NotificationSubscriptionId = NotificationSubscriptionId of int
