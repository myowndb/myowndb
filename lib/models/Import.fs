module Import

open Ids

open FSharp.Data

type Import =
  {
    id: ImportId
    entity_id: EntityId
    // Steps:
    // * 0: import initialised by accessing the import page in the app
    // * 1: file has been uploaded, but not imported yet. User is asked for mappings of csv columns to entity details
    // * 2: file has been imported
    // * 3: import has been undone
    step: int
    // We use the email of the user having imported. Do not use the user id
    // as the user might be deleted at some pointa by the admin.
    user_email: string
    // We put the details of the import in a json field in the database
    // Format:
    // {
    //   file_path: "sha256sum_file.csv"
    //   file_type: "text/csv"
    //   file_name: "Sales.csv"
    //   imported_instances: [ 42,43,44 ]
    //   mappings: { csv_column_name1: detail_id_1, ....}
    // }
    import_info: JsonValue
  }

open FSharp.Data.JsonExtensions
let filePath (import:Import) = import.import_info?file_path.AsString()
let fileName (import:Import) = import.import_info?file_name.AsString()
let fileType (import:Import) = import.import_info?file_type.AsString()
