module DataType

open System

(* Currently unused because our DBHelper cannot map the name string to this type as it only handles
   cases wrapping a value
type Name = ShortText |LongText |Date |Integer |Choose |Email |WebURL |FileAttachment

let nameString (n:Name) = match n with
                          | ShortText ->      "madb_short_text"
                          | LongText ->       "madb_long_text"
                          | Date ->           "madb_date"
                          | Integer ->        "madb_integer"
                          | Choose ->         "madb_choose_in_list"
                          | Email ->          "madb_email"
                          | WebURL ->         "madb_web_url"
                          | FileAttachment -> "madb_file_attachment"
*)
// possibly get rid of this record type, as all fields are static and could be hard coded for better type system checks
type DataType =
    { id: Ids.DataTypeId
      name: string
      class_name: string }
  with member self.toSpec():Specs.DataTypeSpec = {
        dataTypeId=self.id;
        name=self.name;
        className=self.class_name
      }

type DataTypeFixture =
    { id: int
      name: string
      class_name: string }
