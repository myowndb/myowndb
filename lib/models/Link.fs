module Link
open System
open Ids
open IdsExtensions

type Link = {
 id          : int
 parent_id   : int
 child_id    : int
 relation_id : int
}

// returns the related instance id according to the RelationAndDirection passed
// we check that we don't have incoherencies, and return a Result accordingly
// Note that this is not a DBResult, and the Ok value only holds one scalar, and not a list
let getLinkedInstanceId (relationAndDirection: RelationAndDirection) (link:Link)=
    match relationAndDirection with
    | ParentsVia (RelationId relationId) ->
        if link.relation_id <> relationId then
            Error (Error.fromString "error getting linked instances, incoherence between relation and the link considered")
        else
            Ok (InstanceId link.parent_id)
    | ChildrenVia (RelationId relationId) ->
        if link.relation_id <> relationId then
            Error (Error.fromString "error getting linked instances, incoherence between relation and the link considered")
        else
            Ok (InstanceId link.child_id)

let linkInstancesAction (RelationId relationId) (parentIdIn:InstanceId ) (childIdIn:InstanceId )=
    let parentId = InstanceId.Get parentIdIn
    let childId = InstanceId.Get childIdIn
    DBAction.queryAction "insert into links (parent_id,child_id,relation_id) VALUES (%d,%d,%d) RETURNING id" parentId childId relationId
    |> DBAction.map LinkId

let deleteAction (LinkId id) =
    DBAction.statementAction "delete from links where id=%d" (id)

let unlinkInstancesAction (relationId:RelationId)(parentId:InstanceId)(childId:InstanceId)=
    DBAction.queryAction
        "select id from links where relation_id=%d and parent_id = %d and child_id=%d"
        (relationId |> RelationId.Get)
        (parentId |> InstanceId.Get)
        (childId |> InstanceId.Get)
    |> DBAction.bind
        (fun linkId ->
            deleteAction (LinkId linkId)
        )
