module ModelUtils

open Humanizer

let insertStatementForType (typ: System.Type) =
    let props = typ.GetProperties() |> Array.map (fun p -> p.Name)
    let columns = props |> String.concat ", "

    let parms =
        props
        |> Array.map (fun p -> "@" + p)
        |> String.concat ", "
    sprintf "INSERT INTO %s (%s) VALUES (%s)" (typ.Name.Pluralize().Underscore()) columns parms

let insertStatement<'a>() =
    let recordType = typeof<'a>
    insertStatementForType recordType

let updateRecordStatement<'a> (updatedFields: string list) =
    let table = typeof<'a>.Name.Pluralize().Underscore()

    let fields =
        updatedFields
        |> List.map (fun f -> sprintf "%s=@%s" f f)
        |> String.concat ","
    sprintf "UPDATE %s set %s where id=@id" table fields
