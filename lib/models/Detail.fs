module Detail

open System
open Ids

type Detail =
    { id: DetailId
      name: string
      data_type_id: DataTypeId
      status_id: int option
      database_id: DatabaseId
      lock_version: int }
and
  DetailValueProposition =
    { id: DetailValuePropositionId
      detail_id: DetailId
      value: string
      lock_version: int }


// no DU in record type for Legivel
type DetailFixture =
    { id: int
      name: string
      data_type_id: int
      status_id: int option
      database_id: int
      lock_version: int }
