module Entities2details

open System

type Entities2details =
    { id: int
      entity_id: int
      detail_id: int
      status_id: int
      displayed_in_list_view: Boolean
      maximum_number_of_values: int
      display_order: int }
