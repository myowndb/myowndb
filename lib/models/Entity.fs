module Entity

open Ids

type Entity =
    { id: EntityId
      database_id: int
      name: string
      has_public_form: bool
      lock_version: int option
      has_public_data: bool option
      public_to_all: bool option }

// Legivel can't map a scalar in YAML to a DU
// Work around by duplicating the record type, removing the DUs...
type EntityFixture =
    { id: int
      database_id: int
      name: string
      has_public_form: bool
      lock_version: int option
      has_public_data: bool option
      public_to_all: bool option }

// See https://www.gresearch.co.uk/article/squeezing-more-out-of-the-f-type-system-introducing-crates/
// for details regarding existential types implementation in F#
// We want the markup generation to be independent of any front-end technology.
// This was done with only the websharper front-end, so it might have to be refined when
// a second front-end is possibly added.
type IMarkupCrate =
    // This is the type that will wrap a value to be displayed, whatever its type
    abstract member Apply: IMarkupEvaluator<'ret> -> 'ret
// information about details linked to an entity
and IMarkupEvaluator<'ret> =
    // 'a is the type of the value to be displayed
    // It is expected to be a string, but could possibly be a specs.Value in the future
    // The IMarkupEvaluator instance knows the value's detail's class_name and will display
    // it accordingly. As such it is probable that the constructor of the type implementing
    // IMarkupEvaluator will take the detail of the detail's class_name as parameter.
    // Also note that due to this, each datatype to be displayed will have a corresponding
    // instance of the IMarkupEvaluator.
    abstract member Eval<'a> : 'a  -> 'ret
and EntityDetail =
    { id: int // id of record in relation table
      entity_id: EntityId
      detail_id: DetailId
      displayed_in_list_view: bool
      maximum_number_of_values: int
      display_order: int
      name: string
      lock_version: int
      status: string
      data_type: string
      class_name: string }
      with
      // I hoped to pack more in the EntityDetail instance, ast he IMarkupCrate will possibly always be the same:
      //        member _.Apply (v:Entity.IMarkupEvaluator<_>) =
      //           v.Eval<string> s
      // However, WebSharper has trouble translating object expresions to javascript, requiring to explicitly
      // define a type implementing the interface. I don't think I want to define those types here, so I settled
      // on this display method taking the crate wrapping the value and its correponding evaluator.
      member self.display(crate:IMarkupCrate) (evaluator:IMarkupEvaluator<'ret>)=
        crate.Apply evaluator

let Init(): Entity =
    { id = Ids.EntityId 0
      database_id =0
      name= ""
      has_public_form= false
      lock_version= None
      has_public_data= Some false
      public_to_all= Some false }
