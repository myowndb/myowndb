module Relation
open Ids
open Specs

type Relation = {
 id                        : RelationId
 parent_id                 : EntityId
 child_id                  : EntityId
 parent_side_type_id       : int
 child_side_type_id        : int
 from_parent_to_child_name : string
 from_child_to_parent_name : string
 lock_version              : int
}
with
  member self.toSpec():RelationSpec =
    {
      relationId = Some self.id
      parentSideType = RelationSideType.fromId self.parent_side_type_id
      childSideType = RelationSideType.fromId self.child_side_type_id
      parentToChildName = self.from_parent_to_child_name
      childToParentName = self.from_child_to_parent_name
      parentId = self.parent_id
      childId = self.child_id
    }

type RelationFixture = {
 id                        : int
 parent_id                 : int
 child_id                  : int
 parent_side_type_id       : int
 child_side_type_id        : int
 from_parent_to_child_name : string
 from_child_to_parent_name : string
 lock_version              : int
}
