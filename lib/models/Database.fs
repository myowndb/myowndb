module Database

open System
open Ids
type Database =
    { id: DatabaseId
      account_id: AccountId
      name: string
      lock_version: int }

type DatabaseFixture =
    { id: int
      account_id: int
      name: string
      lock_version: int }