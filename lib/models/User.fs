module User
// get DateTime
open System
open Ids


type User =
    { id: UserId
      account_id: AccountId
      user_type_id: int
      login: string
      password: string
      aspnet_pass_hash: string option
      email: string
      firstname: string option
      lastname: string option
      uuid: string option
      salt: string
      verified: int
      created_at: DateTime
      updated_at: DateTime option
      logged_in_at: DateTime option
      api_key: string option }

type UserFixture =
    { id: int
      account_id: int
      user_type_id: int
      login: string
      password: string
      aspnet_pass_hash: string option
      email: string
      firstname: string option
      lastname: string option
      uuid: string option
      salt: string
      verified: int
      created_at: DateTime
      updated_at: DateTime option
      logged_in_at: DateTime option
      api_key: string option }

let Init () :User=
    { id= UserId 0
      account_id= AccountId 0
      user_type_id= 2
      login= ""
      password= ""
      aspnet_pass_hash= None
      email= ""
      firstname= None
      lastname= None
      uuid= None
      salt= ""
      verified= 0
      created_at= DateTime.Now
      updated_at= None
      logged_in_at= None
      api_key= None }
