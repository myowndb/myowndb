module DetailValueProposition

open Ids
open System

// the type DetailValueProposition's definition was moved to Detail.fs
// to handle the mutual references. We can define a type alias here if needed later
// type DetailValueProposition = Detail.DetailValueProposition

// No DU for Legivel
type DetailValuePropositionFixture =
    { id: int
      detail_id: int
      value: string
      lock_version: int }
