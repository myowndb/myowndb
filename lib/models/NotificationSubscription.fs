module NotificationSubscription
open Ids
open NotificationSubscriptionCriteria
open FSharp.Data

// Record type used when reading from database
type NotificationSubscription =
  {
    id: NotificationSubscriptionId
    event: Event
    source_type: Source
    source_filter: JsonValue
    destination_type: DestinationType
    destination: string
    protocol: Protocol
  }
// This is the equivalent to the Specs we use for other types, but as it uses
// all these dedicated DUs and is not used in other specs, I call it distinctively and leave its definition here.
// The id fields is absent, and not an option, because subscriptions will not be updatable.
type NewNotificationSubscription =
  {
    event: Event
    source_type: Source
    source_filter: SourceFilter
    destination_type: DestinationType
    destination: string
    protocol: Protocol
  }

////////////////////////////////////////////////////////////////////////////////
// Notifier module
////////////////////////////////////////////////////////////////////////////////
module Notifier =
    // Defines a mutable notifiers map that the application has to fill at startup.
    // There is max one notifier per protocol.
    // The notifier is a function taking as argument the protocol to send the notification
    // over, the subscription and the id of the source that triggered the notification (eg instance id).
    // It returns an DBAction, so that the notifier can do database queries, eg to translate the user id in
    // email address. In the web app, it is Hangfire that will actually run the action so the notification
    // is sent.
    // In production, it is probably better to just register a Hangfire job and limit the number of queries in
    // the notifier, because these are run at the same time as the operation triggering the notification.
    let mutable internal notifiers= Map.empty<Protocol,NotificationSubscription -> int -> unit>
    // Functions to manipulate the notifiers, which is not accessible directly as internal
    let setNotifier (protocol:Protocol) (f) =
      notifiers <- notifiers.Add(protocol,f)
    let getNotifier (protocol) =
      notifiers.TryFind(protocol)
      |> Option.defaultValue (fun _ _ -> MyOwnDBLib.Logging.Log.Error $"{protocol} notifier is not configured, notification skipped")

// Create a notification subscription.
let AddAction (r:NewNotificationSubscription) : DBAction.DBAction<NotificationSubscription>=
  DBAction.queryAction
    "insert into
      notification_subscriptions (event,source_type,source_filter,destination_type,destination,protocol)
      VALUES (%s,%s,%s::jsonb,%s,%s,%s) RETURNING *"
    (r.event.ToString())
    (r.source_type.ToString())
    (r.source_filter.ToString())
    (r.destination_type.ToString())
    (r.destination)
    (r.protocol.ToString().ToLower())

let RemoveAction (Ids.NotificationSubscriptionId id) =
  DBAction.statementAction "delete from notification_subscriptions where id=%d" id

// Shortcut for what is initially the only notification we define:
// a mail notification of new instance creation
let SubscribeInstanceCreationMailAction (entityId:EntityId)(userId:UserId)=
  let spec=
    {
      event = AfterCreate
      source_type = Instance
      source_filter = (Entity entityId)
      destination_type= (User)
      destination = (userId |> UserId.Get) |> string
      protocol = Smtp
    }
  AddAction spec

// Get all notification subscriptions corresponding to a specific event, like creation of an instance with entity id 43.
let GetAll(event:Event)(source:Source)(sourceFilter:SourceFilter)=
  let r = sourceFilter.ToRecord()
  DBAction.queryAction
    "select * from notification_subscriptions where event=%s and source_type=%s and (source_filter->%s)::text::numeric = %d"
    (event.ToString())
    (source.ToString())
    r.field
    r.value

// Retrieve a user's subscriptions for a specific source
let GetUserSubscriptionsFor (userId:UserId)(source:Source)(sourceFilter:SourceFilter):DBAction.DBAction<NotificationSubscription> =
  let r = sourceFilter.ToRecord()
  DBAction.queryAction
    "select * from notification_subscriptions where source_type=%s and (source_filter->%s)::text::numeric = %d and destination=%s"
    (source.ToString())
    r.field
    r.value
    (userId|>UserId.Get|>string)

// Action sending the notification for one subscription. The notification subscription only misses the id
// of the source that triggered the notification, and it is passed as second argument.
let SendNotificationAction (notificationSubscription:NotificationSubscription) (sourceId:int)=
  let notifier = Notifier.getNotifier  notificationSubscription.protocol
  notifier notificationSubscription sourceId

// Action notifying all subscriber to a specific event
let NotifyAction(event:Event)(source:Source)(sourceFilter:SourceFilter)(sourceId:int)=
  GetAll event source sourceFilter
  |> DBAction.map (fun notificationSubscription ->
    SendNotificationAction notificationSubscription sourceId
  )
// Helper to notify of an instance creation
let NotifyInstanceCreationAction (a:DBAction.DBAction<Specs.InstanceSpec>) =
  a
  |> DBAction.ensureSingleRow
  |> DBAction.trigger (fun instanceSpecs ->
    // We can take the Head here as we called DBAction.ensureSingleRow before
    let instanceSpec = instanceSpecs.Head
    match instanceSpec.instanceId with
    | Some instanceId ->
      NotifyAction AfterCreate Instance  (Entity instanceSpec.entityId) (instanceId|> InstanceId.Get)
    // If no InstanceId, nothing to notify, just return a DBAction<unit> as needed
    | None -> DBAction.retn ()
  )
