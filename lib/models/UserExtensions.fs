module UserExtensions

// For SHA1 computation
open System.Security.Cryptography
// get DateTime
open System
open Error
open FSharpPlus
open Ids
open User

let byId (id: int): DBAction.DBAction<User> = DBAction.querySingleAction "select * from users where id=%d" id

let byEmail (e: Email.t): DBAction.DBAction<User> =
    DBAction.querySingleAction "select * from users where email=%s" (Email.toString e)

let getAllForAccountAction (accountId:int): DBAction.DBAction<User> =
    DBAction.queryAction "select * from users where account_id=%d order by id" accountId

// Action to update the logged_in_at field
let setLoggedInAtAction (email:string) =
    DBAction.statementAction
      "update users set logged_in_at=%A where email=%s" (DateTime.Now) email
    |> DBAction.mapError (fun e ->
      MyOwnDBLib.Logging.Log.Error("Updated user#logged_in_at for user {email} failed with {error}", email, e|>Error.toString)
      e
    )
    |> DBAction.map (fun impactedRows ->
      match impactedRows with
      | DBTypes.ImpactedRows 1 ->
          MyOwnDBLib.Logging.Log.Debug("Updated user#logged_in_at for user {email}", email)
          impactedRows
      | DBTypes.ImpactedRows n ->
          MyOwnDBLib.Logging.Log.Error("Updated user#logged_in_at for user {email} updated unexpected number or rows: {rowsUpdated}", email, n)
          impactedRows
    )

/// Encode bytes in hexadecimal
let BytesToHex bytes =
    bytes
    |> Array.map (fun (x: byte) -> System.String.Format("{0:X2}", x))
    |> String.concat System.String.Empty

/// Compute the hexadecimal SHA1 of string
let hexDigest (s: string): string =
    s
    |> System.Text.Encoding.UTF8.GetBytes
    |> SHA1.Create().ComputeHash
    |> BytesToHex
    |> (fun s -> s.ToLower())


/// Compute hash of internal string concatenated with string s
let hashed (s: string) =
    let complete =
        s
        |> sprintf "change-me--%s--"
        |> hexDigest
    complete.[0..31]

/// return a random alphabetic string of length n.
let randomString (n: int): string =
    let r = new System.Random()
    System.String(Array.init n (fun _ -> char (r.Next(97, 123))))

/// Compute saled hash of the password string, and return tuple with salt used and resulting salted hash.
let newPasswordToHash (password: string): string * string =
    let hashedPassword = hashed password
    let newSalt = hashed (randomString 64)
    let saltedHash = sprintf "%s%s" newSalt hashedPassword |> hashed
    (newSalt, saltedHash)

/// Check password is valid for user. Returns a DBResult.
let validatePassword (password: string) (user: User): DBResult.DBResult<User> =
    let hashedPassword = hashed password
    let saltedHash = sprintf "%s%s" user.salt hashedPassword |> hashed
    if saltedHash = user.password
    then Ok [ user ]
    else (sprintf "FAILED invalid password for user %s" user.email |> DBResult.error)

/// Set new password for user, returning a DBAction<int>, indicating number of rows affected.
let updatePasswordAction (password: string) (user: User): DBAction.DBAction<DBTypes.ImpactedRows> =
    let salt, newHashedPassword = newPasswordToHash password

    let newUserRecord: User =
        { user with
              salt = salt
              password = newHashedPassword }

    let updateStatement = ModelUtils.updateRecordStatement<User> [ "salt"; "password" ]
    DBAction.recordStatementAction updateStatement newUserRecord


/// Check user account is verified, and if password is valid for user. Returns a DBResult<User>
let authenticateUser (password: string) (user: User): DBResult.DBResult<User> =
    if user.verified = 1
    then (validatePassword password user)
    else (sprintf "user with email %s is not verified" user.email |> DBResult.error)

/// Authenticate with email and string
let authenticateEmailAction (email: string) (password: string) =
    let action client = async {
        let v = email
                |> Email.fromString
                |> Result.map byEmail
                |> (function
                    | Ok v -> async { let! res = DBAction.run<User> client v
                                      return (res |>  DBResult.bind (authenticateUser password))
                              }
                    | Error e -> async { return DBResult.DBResult.Error e}
                )
        return! v
    }
    DBAction.Query action
