module RelationExtensions

open DBAction
open Ids
open DBAction.StaticArguments
open Relation

let entityIdForRelationAction (relationAndDirection:RelationAndDirection) =
    let selectedColumn,relationId = match relationAndDirection with
                                    | ChildrenVia (RelationId relationId) -> "child_id",relationId
                                    | ParentsVia (RelationId relationId) ->  "parent_id",relationId
    DBAction.queryAction "select %A from relations where id=%d" (Static selectedColumn) relationId
    |>DBAction.ensureSingleRow
    |>DBAction.map EntityId

// functions to get relations for an entity are placed here because we need the type
// annotation DBAction<Relation>, and Relation is not available in the file Entity.fs due to
// file ordering in the fsproj file.
let toChildrenForEntityAction (entityId:EntityId):DBAction<Relation> =
    DBAction.queryAction "select * from relations where parent_id = %d" (EntityId.Get entityId)

let toParentsForEntityAction (entityId:EntityId):DBAction<Relation> =
    DBAction.queryAction "select * from relations where child_id = %d" (EntityId.Get entityId)

let byIdAction (RelationId id) : DBAction.DBAction<Relation>=
    DBAction.querySingleAction "select * from relations where id = %d" id
