module DdlDetailValue

open System

type DdlDetailValue =
    { id:  Ids.DetailValueIdType
      detail_id: Ids.DetailId
      instance_id: Ids.InstanceId
      detail_value_proposition_id: int
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
    static member Table = "ddl_detail_values"
    interface IDetailValue.IDetailValue with
      member this.GetId() = this.id
      member this.GetStringValue() = this.detail_value_proposition_id.ToString()
      member this.GetDetailId() = this.detail_id
      member this.GetInstanceId() = this.instance_id

type DdlDetailValueFixture =
    { id: int
      detail_id: int
      instance_id: int
      detail_value_proposition_id: int
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }