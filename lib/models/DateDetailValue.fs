module DateDetailValue

open System

type DateDetailValue =
    { id: Ids.DetailValueIdType
      detail_id: Ids.DetailId
      instance_id: Ids.InstanceId
      value: DateTime
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
    interface IDetailValue.IDetailValue with
      member this.GetId() = this.id
      member this.GetStringValue() = this.value.ToString()
      member this.GetDetailId() = this.detail_id
      member this.GetInstanceId() = this.instance_id
    static member Table = "date_detail_values"


type DateDetailValueFixture =
    { id: int
      detail_id: int
      instance_id: int
      value: DateTime
      lock_version: int
      created_at: DateTime option
      updated_at: DateTime option }
