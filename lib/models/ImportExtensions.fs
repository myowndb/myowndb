module ImportExtensions


open Import
open Ids
open Specs
open DBAction
open ImportMapping


let byIdAction (ImportId importId):DBAction<Import> =
  DBAction.queryAction "select * from imports where id = %d" importId

// Find the immport entry that has been uploaded but not imported yet
// We use the account id to ensure the access it warranted
// and the userEmail as additional safe-guard to avoid other users messing
// with another one's import
let byIdForImportAction (ImportId importId)(AccountId accountId)(userEmail:string):DBAction<Import> =
  DBAction.queryAction "select * from imports i join entities e on (e.id=i.entity_id) join databases db on (db.id=e.database_id) where i.id = %d and i.step=1 and lower(i.user_email)=lower(%s) and db.account_id=%d" importId userEmail accountId
  |> DBAction.ensureSingleRow

// Only return import if is at step 2
let byIdForUndoAction (ImportId importId)(AccountId accountId)(userEmail:string):DBAction<Import> =
  DBAction.queryAction "select * from imports i join entities e on (e.id=i.entity_id) join databases db on (db.id=e.database_id) where i.id = %d and i.step=2 and lower(i.user_email)=lower(%s) and db.account_id=%d" importId userEmail accountId
  |> DBAction.ensureSingleRow

// Creates the import entry in the database, and returns its id
let initialiseAction (EntityId entityId) (userEmail: string):DBAction<ImportId>=
  // Ensure consitency of entity id and userEmail (part of the same account)
  // Lowercase email comparison because Identity Core may give it all uppercase
  DBAction.queryAction
    "select u.email from users u
      join accounts acc on (acc.id=u.account_id)
      join databases db on (db.account_id=acc.id)
      join entities e on (e.database_id=db.id)
    where lower(u.email)=lower(%s) and e.id=%d"
    userEmail
    entityId
  |> DBAction.ensureSingleRow
  |> DBAction.bind (fun email ->
    DBAction.queryAction "insert into imports (entity_id,step,user_email,import_info) VALUES (%d,0,%s,'{}') RETURNING id" entityId email
    |> DBAction.map ImportId
  )

open System.IO
open System.Globalization
open CsvHelper

// Updates the import entry in the database, and returns the columns available in the csv
let afterFileUploadAction (import:Import) (fileOnDisk:string)(originalFileName:string)(fileType:string) =
  if not (System.IO.File.Exists fileOnDisk) then
    raise (System.Exception("csv file not found"))
  let reader = new StreamReader(fileOnDisk)
  let csv = new CsvReader(reader, CultureInfo.InvariantCulture)

  DBAction.statementAction
    """update imports set step=1, import_info = CAST (import_info::jsonb || jsonb_build_object('file_type',%s, 'file_name', %s, 'file_path', %s ) AS json) where id=%d""" fileType originalFileName fileOnDisk (import.id|> ImportId.Get)
  |> DBAction.bind ( fun impactedRows ->
    if impactedRows = DBTypes.ImpactedRows 1 then
      // Read the first line to headers can be read
      if csv.Read() && csv.ReadHeader()then
        DBAction.retn csv.Context.Reader.HeaderRecord
      else
        DBAction.error "Could not read headers of csv file?"
    else
        DBAction.error (sprintf "Updating import entry returned unexpected impacted rows {impactedRows}. Working on Import : %A" import)
    )

open System.Security.Cryptography

// Function to import a csv file according to the mapping list
let importFileAction (import:Import) (mappings:Mapping list) =
  let bytes = File.ReadAllBytes(Import.filePath import)
  let sha256 = HashAlgorithm.Create("SHA256").ComputeHash bytes

  let rec mappingsListToMap (mappings: Mapping list) (acc:Map<string,DetailId>)=
    match mappings with
    | [] -> acc
    | h :: t ->
      mappingsListToMap t (acc.Add(h.csvColumn,h.detailId))
      //
  // Helpers to build a DBAction<List> from a List
  let cons h t = h :: t
  let (<*>) = DBAction.apply
  let retn = DBAction.retn


  // Helper function to build a DBAction<DetailValueSpec list>  from the mappings list
  let rec mappingsToValueSpecsListAction (mappings:Mapping list)(csv:CsvReader) =

    // We will catch the exception raised by csv.GetField to report an error
    try
      match mappings with
      | [] -> retn []
      | mapping :: t ->
        // Get the value from the file. The line was already read by our caller.
        // A string badly quoted (eg no closing quote) generates an error on the next line
        let csvValue = string (csv.GetField(typeof<string>,mapping.csvColumn))
        // We ignore the empty string values
        // In CSV files, distinguishing no value from an empty string is not always easy.
        // See https://github.com/JoshClose/CsvHelper/issues/252
        // CsvHelper seems to return an empty string for empty fields.
        // We decide to ignore empty strings in imports.
        if csvValue = "" then
          mappingsToValueSpecsListAction t csv
        // If we get a non empty value, we construct the rest of the list
        else
          // Build a DBAction<DetailValueSpec list> using DBAction.apply
          retn cons
          <*>
            (
              // Get the DetailSpec of the destination detail
              DetailExtensions.getSpecAction mapping.detailId
              // We now have all information to map the value from the csv to a detail value.
              |> DBAction.bind (fun detailSpec ->
                // If the detail spec has value propositions, we cannot use the value from the
                // CSV as is, we need to translate it to the proposition id
                // An Ok valueResult means we either have a value matching a value propositions,
                // or we have a value for a detail without proposition.
                let valueResult =
                  match detailSpec.propositions with
                  // If the detail spec has propositions, validate the value against it
                  | Some l ->
                    // Filter propositions against the value from the csv
                    let matchingPropositions =
                      l
                      |> List.filter (fun prop -> prop.value = csvValue)
                      |> List.map (fun prop -> prop.detailValuePropositionId |> Option.get)
                    // If we have one match, use it, otherwise report an error
                    match matchingPropositions with
                    | [] -> Error $"No proposition found for value {csvValue} to be mapped to {detailSpec.name}"
                    | [ DetailValuePropositionId dvpid ] -> Ok (string dvpid)
                    | l -> Error (sprintf "Multiple matching propositions: %A" l)
                  // If the detail value has no proposition, we use the value from the csv as is
                  | None -> Ok csvValue
                match valueResult with
                // The value from the csv is usable, use it
                | Ok value ->
                  // Build a ServerValidated value. Data validation happens here!
                  DetailValue.buildServerValueAction (mapping.detailId) (Some value)
                  |> DBAction.map (fun serverValue ->
                    let serverValidatedValue = (ServerValidated serverValue )
                    // detailValueId = None indicates this is an insertion
                    let valueSpec = { detailValueId = None; value = serverValidatedValue}
                    {
                      detailId = mapping.detailId
                      values = [ valueSpec ]
                    }
                  )
                // The validation against value propositions failed, report it.
                | Error e -> DBAction.error e
              )
            )
          <*>
            mappingsToValueSpecsListAction t csv




        // If no exception, we read the value, so wrap it in a DBAction
      with
      | e -> DBAction.error (e.Message)
    // An entry in mappings indicates a column of the CSV to be imported, so we go through
    // the whole list

  // Go throught the CsvReader and for each row generate an InstanceSpec
  let rec generateInstanceSpecsAction (entityId:EntityId)(csv:CsvReader)(mappings:Mapping list) =
    // csv.Read() = true means an additional line vould be read from the file
    if csv.Read() then
        // We want to return a DBAction<_ list>, se we use DBAction.apply (i.e. <*> ) here
        retn cons
        <*>
        // Map the csv row we just read to an InstanceSpec
        (
          // Map each column to a ValueSpec list
          mappingsToValueSpecsListAction mappings csv
          // then insert the ValueSpec list in the InstanceSpec
          |> DBAction.map (fun detailValuesSpecs ->
              { entityId= entityId
                // InstanceId None means this is an insert, not an update
                instanceId= None
                detailValues= detailValuesSpecs }
          )
        )
        <*>
        // Build the tail of the list similarly
        generateInstanceSpecsAction entityId csv mappings
    else
      retn []

  // Initialise the csv reading
  let reader = new StreamReader(Import.filePath import)
  let csv = new CsvReader(reader, CultureInfo.InvariantCulture)
  // Check we can read the csv headers, and if not, raise an exception
  match csv.Read(), csv.ReadHeader() with
  | true, true -> ()
  | _, _ ->
    raise (System.Exception("Problem reading CSV headers"))

  // Generate a list of InstanceSpec from the CSV according to the mappings
  generateInstanceSpecsAction (import.entity_id) csv mappings
  // As we get a DBAction<InstanceSpec list>, we transform it in a DBAction.<InstanceSpec>
  |> DBAction.workOnItems
  // Create the instances in the database
  |> DBAction.bind Instance.createFromSpecForImportAction
  // We want to work in the list of the updated InstanceSpec again
  |> DBAction.workOnList
  // Update the import entry with the imported instances
  |> DBAction.bind (fun instanceSpecs ->
    let instanceIdsJson =
      instanceSpecs
      // Get the instance ids. All InstanceSpecs have an instance id as they have been inserted in the db
      |> List.map (fun spec -> spec.instanceId |> Option.get |> InstanceId.Get)
      |> box
      // Map it to json so it can be used in the json column in postgres
      |> Utils.toJson
    // Issue the update
    DBAction.statementAction "update imports set step=2, import_info = CAST (import_info::jsonb || jsonb_build_object('imported_instances',%s::json ) AS json) where id=%d" instanceIdsJson (import.id |> ImportId.Get)
    // FIXME: check update was successful
    // We want to return the InstanceSpecs to the caller
    |> DBAction.map (fun _ -> instanceSpecs)
  )
  // In case of error, set the import entry step to -1
  |> DBAction.bindError (fun e ->
      DBAction.statementAction "update imports set step=-1 where id=%d" (import.id|>ImportId.Get)
      |> DBAction.bind (fun _ -> DBAction.wrapError e)
  )


let undoImportAction (import:Import)(EntityId entityId)=
  DBAction.statementAction
    "delete from instances
      where
        id in
          (select json_array_elements(import_info->'imported_instances')::text::int as instance_id from imports where id=%d)
        and entity_id=%d"
    (import.id|> ImportId.Get)
    entityId
  |> DBAction.bind (fun (deletedInstances) ->
      DBAction.statementAction "update imports set step=3 where id=%d" (import.id |> ImportId.Get)
      |> DBAction.bind(fun (DBTypes.ImpactedRows updatedImport) ->
        match updatedImport with
        | 1 ->
          DBAction.retn deletedInstances
        | n ->
          MyOwnDBLib.Logging.Log.Error($"""Import {import.id} update after undo updated {n} rows, not 1 as expected.""")
          DBAction.error "Error in undo"
      )
  )
  |> DBAction.wrapWithSavePointAndRollBackForError
  |> DBAction.wrapInTransactionIfNeeded
