module Instance

open System
open Ids
open IdsExtensions
open Specs

open System.Data
open Detail
open DBAction.StaticArguments

type Instance =
    { id: InstanceId
      entity_id: EntityId
      created_at: DateTime option
      lock_version: int
      updated_at: DateTime option }
// No DU in record for Legivel
type InstanceFixture =
    { id: int
      entity_id: int
      created_at: DateTime option
      lock_version: int
      updated_at: DateTime option }

type DetailWhereFilter = {
    column: Detail
    value: string
}

type WhereFilter =
    | DetailFilter of DetailWhereFilter
    | False

type CountClauses =
    {
      where:  WhereFilter option
    }
type SortDirection = |Asc |Desc
type OrderClause =
    {
        column: string
        direction: SortDirection
    }
type ListClauses =
    {
      select:  string
      where:  WhereFilter option
      order: OrderClause option
      perPage:  int
      page:  int
    }
let maxPerPage=500
let defaultListClause = { select = "select *"
                          where = None
                          order = None
                          perPage= maxPerPage
                          page=1

}
type Clauses = | Count of CountClauses
               | List of ListClauses

let countFor (filterClause:ListClauses)=
    {where = filterClause.where}
// Basic pagination is done on results of crosstab queries
// In lots of cases can probably be greatly optimised by working on individual tables
// i.e. first select instance ids to be displayed with a query on table instances or detail_values
// and then pass these ids to the crosstab query.
// Will be a later improvement
// When the select is a count, there's no point in pagination, we want to know the number of rows
// corresponding to the clause, so return an empty string
let limitOffset ({select=s;page=p; perPage=pp}) =
    if s="select count(*)" || p = -1 then
        ""
    else
        $"LIMIT {pp} OFFSET {pp*(p-1)}"

// FIXME: ensure no injection attack is possible
// match the order field of the ListClauses value passed
let orderBy ({order=o}) =
    match o with
    | Some {column=c; direction=d} -> $"order by {c} {string d}"
    | None -> ""

let byIdAction (id:InstanceId ) : DBAction.DBAction<Instance>=
    DBAction.querySingleAction "select * from instances where id = %d" (InstanceId.Get id)

let createAction (EntityId entityId) : DBAction.DBAction<Ids.InstanceId>=
    DBAction.singleInsertAction "insert into instances(entity_id) VALUES (%d) RETURNING id" entityId

// FIXME: check if this code is really used. It was added when I thought I'd need to extract a spec from the
// database, but in the end I will probably use the crosstab functions to display an instance. So there's a good change
// this code will have to be removed, though I leave it in until instance edition is done.
let createActionSpec (spec: InstanceSpec) =
    // First, initialise the DBAction, which returns the id of the row it inserts.
    // However, we want to return an updated spec, which includes that very id.
    // The way to achieve this is to DBAction.map it to an updated spec that includes the id
    // of the row that was just inserted by the action.
    // The function passed to DBAction.map is plain: it doesn't work on nor return wrapped values.
    // It gets an id, and returns a spec.
    // It is DBAction.map that will wrap the value returned by the function in a new DBAction.
    // As we pass DBAction.map a function InstanceId -> InstanceSpec, we end up with a
    // DBAction<InstanceSpec>.
    // This DBAction<InstanceSpec> can then be used further with a DBAction.bind.
    createAction spec.entityId
    |> DBAction.map (fun instanceId ->
        { spec with
              instanceId = Some instanceId })

// A pipeline taking a DBACtion<InstanceSpec> and will create or update  all details values in a transaction.
let createOrUpdateInstanceDetailsAction (specAction:DBAction.DBAction<InstanceSpec>)=
    specAction
    |> DBAction.bind (fun instanceSpec ->
        match instanceSpec.instanceId with
        // if no instance id is received, the insertion of the instance failed, don't work further
        | None -> DBAction.error "no instance id available in spec for creation or update"
        // when the instance was inserted and we get its id, we can use it to create the DBAction
        // for DetailValues.
        // Similarly as above, we use DBAction.map to return a DBAction<InstanceSpec>.
        | Some instanceId ->
            DetailValue.createActionsFromDetailValueSpecList instanceId instanceSpec.detailValues
            |> DBAction.map (fun detailValueSpecs ->
                { instanceSpec with
                      detailValues = detailValueSpecs }))
    // This sets a savepoint and rolls back to it in case of error
    |> DBAction.wrapWithSavePointAndRollBackForError
    // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
    |> DBAction.wrapInTransactionIfNeeded

let createOrUpdateAction (spec: InstanceSpec) =
    match spec.instanceId with
    | None ->
      createActionSpec spec
      |> createOrUpdateInstanceDetailsAction
      // For a creation, we send a notification to subscribers
      |> NotificationSubscription.NotifyInstanceCreationAction
    | Some _id ->
      DBAction.retn spec
      |> createOrUpdateInstanceDetailsAction

// Specific function for instance creation in case of an import.
// This allows us to behave accordingly, eg for an import, no notification is sent
let createFromSpecForImportAction (spec: InstanceSpec) =
    match spec.instanceId with
    | None ->
      createActionSpec spec
      |> createOrUpdateInstanceDetailsAction
    | Some id ->
      // For an import, we do not support updates, and we should never get here. Raise an error
      DBAction.error (sprintf "Updates are not supported for imports. Got instance id %d in spec" (id|>InstanceId.Get))

// Function to retrieve detail values
// the type argument 'T must implement interface IDetailValue
let getDetailValueSpecs<'T when 'T :> IDetailValue.IDetailValue> (instanceId:InstanceId) =
        let t = typeof<'T>
        let tableName = (t.GetProperty("Table")).GetValue(null).ToString()
        (
            // if we work on values stored in the table of type DetailValue, it means we need an additional filter on the
            // type column
            if tableName = DetailValue.DetailValue.Table then
                let typeName = t.Name
                DBAction.queryAction "select * from %A where instance_id = %d and type = %s order by id" (Static tableName) (InstanceId.Get instanceId) typeName
            else
                DBAction.queryAction "select * from %A where instance_id = %d order by id" (Static tableName) (InstanceId.Get instanceId)
        )
        // we will work on the whole list and not individual entries, so switch wit workOnList
        |> DBAction.workOnList
        // group by detail_id
        |> DBAction.map (List.groupBy (fun (i:'T) -> (i:>IDetailValue.IDetailValue).GetDetailId()))
        |> DBAction.workOnItems
        |> DBAction.map (fun (detailId, dvs) -> (detailId, dvs |> List.map (fun dv -> (dv :> IDetailValue.IDetailValue))))
        |> DBAction.bind (fun (_detailId, dvs) ->
                            DetailValue.toSpecAction dvs
        )

let detailValuesSpecListForInstanceAction (instanceId:InstanceId) =
    let simpleDetailValuesSpecs = getDetailValueSpecs<DetailValue.SimpleDetailValue> instanceId
    let longTextDetailValuesSpecs = getDetailValueSpecs<DetailValue.LongTextDetailValue> instanceId
    let emailDetailValuesSpecs = getDetailValueSpecs<DetailValue.EmailDetailValue> instanceId
    let webUrlDetailValuesSpecs = getDetailValueSpecs<DetailValue.WebUrlDetailValue> instanceId
    let integerDetailValuesSpecs =  getDetailValueSpecs<IntegerDetailValue.IntegerDetailValue> instanceId
    let dateDetailValuesSpecs =  getDetailValueSpecs<DateDetailValue.DateDetailValue> instanceId
    let ddlDetailValuesSpecs =  getDetailValueSpecs<DdlDetailValue.DdlDetailValue> instanceId
    let fileAttachmentDetailValuesSpecs =  getDetailValueSpecs<DetailValue.FileAttachmentDetailValue> instanceId

    simpleDetailValuesSpecs
    |>DBAction.merge longTextDetailValuesSpecs
    |>DBAction.merge emailDetailValuesSpecs
    |>DBAction.merge webUrlDetailValuesSpecs
    |>DBAction.merge integerDetailValuesSpecs
    |>DBAction.merge dateDetailValuesSpecs
    |>DBAction.merge ddlDetailValuesSpecs
    |>DBAction.merge fileAttachmentDetailValuesSpecs

let getSpecAction (id:InstanceId)=
    byIdAction id
    |> DBAction.ensureSingleRow
    |> DBAction.bind (fun instance ->
        detailValuesSpecListForInstanceAction id
        |> DBAction.workOnList
        |> DBAction.map (fun dvs ->
            {
                entityId = instance.entity_id
                instanceId = Some instance.id
                detailValues = dvs
            }
        )
    )

// Easily get a datatable with instances for entityId, limited to instanceIdFilter
// The instanceIdFilter is an option so that an empty list makes it return an empty instances list.
// An earlier version interpreted the empty list as "no filtering to be done", but this caused trouble for linked instances
// (if no instance was linked, the instance id filters list was empty and all instances were displaed as linked, whereas it should
// have displayed an empty list )
let selectFilteredDataTableAction (entityId: EntityId) (instanceIdFilter:InstanceId list option) (clauses: ListClauses) =
    Crosstab.getFilteredCrosstabInfoAction entityId instanceIdFilter
    |> DBAction.bind (fun info ->
        if List.length info.entityDetailsInList = 0 then
            DBAction.retn (new DataTable())
        else
            match clauses.where with
            | None ->
                let query =
                    sprintf "%s from %s %s %s" clauses.select info.crosstabListQueryBody (orderBy clauses)(limitOffset clauses)
                DBAction.staticDataTableAction query
            | Some (DetailFilter {column=d; value=v}) ->
                // we mark 3 arguments as static, those are values built by the code.
                // The last argument, a string provided by the user, is then correctly escaped as dynamic argument
                // v is not passed as static, and so is correctly escaped when inserted in the query.
                // We cast the column search to text because we get a text from the search form. Without this cast, the integer_detail_values wouldn't be
                // searchable.
                DBAction.dataTableAction "%A from %A where \"%A\"::text ~* %s %A %A" (Static clauses.select) (Static info.crosstabListQueryBody) (Static (d.name.Replace("\"","\"\""))) (v) (Static (orderBy clauses)) (Static (limitOffset clauses))
            | Some (False) -> DBAction.retn (new DataTable())
        )
// Easily get in a datatable all instances for an entity
let selectDataTableAction (entityId: EntityId) (clauses:ListClauses) =
    selectFilteredDataTableAction entityId None clauses

// Get one instance information in a datatable
let getEntryDataTableAction (instanceId: InstanceId) =
    byIdAction instanceId
    |> DBAction.bind (fun instance ->
        // get crosstab info limiting to one instance
        Crosstab.getFilteredCrosstabInfoAction instance.entity_id (Some [instance.id])
        |> DBAction.bind (fun info ->
            if List.length info.entityDetailsInList = 0 then
                DBAction.retn (new DataTable())
            else
                let query =
                    // CHECKME: is filtering here superfluous? Should not cause trouble anyway.
                    sprintf "select * from %s where id=%d" info.crosstabQueryBody (InstanceId.Get instanceId)
                DBAction.staticDataTableAction query
                |> DBAction.ensureSingleRow
        )
    )

let selectFilteredCountAction (entityId: EntityId) (instanceIdFilter:InstanceId list option)(clauses:CountClauses)=
    Crosstab.getFilteredCrosstabInfoAction entityId instanceIdFilter
    |> DBAction.bind (fun info ->
        if List.length info.entityDetailsInList = 0 then
            DBAction.retn 0L
        else
            match clauses.where with
            | None ->
                let query =
                    sprintf "select count(*) from %s" info.crosstabListQueryBody
                DBAction.staticQueryAction query
            | Some (DetailFilter{column=d; value=v}) ->
                // we mark 2 arguments as static, those are values built by the code.
                // The last argument, a string provided by the user, is then correctly escaped
                DBAction.queryAction "select count(*) from %A where \"%A\"::text ~* %s" (Static info.crosstabListQueryBody) (Static (d.name.Replace("\"","\"\""))) (v)
            | Some (False) -> DBAction.retn 0
    )

let selectCountAction (entityId: EntityId) (clauses:CountClauses)=
    selectFilteredCountAction entityId None clauses

// return links for the instance and relation in the specified direction
let linksAction (instanceIdIn:InstanceId) (relationAndDirection: RelationAndDirection):DBAction.DBAction<Link.Link> =
    let instanceId = InstanceId.Get instanceIdIn
    match relationAndDirection with
    | ParentsVia (RelationId relationId) -> DBAction.queryAction "select * from links where child_id=%d and relation_id=%d" instanceId relationId
    | ChildrenVia (RelationId relationId) -> DBAction.queryAction "select * from links where parent_id=%d and relation_id=%d" instanceId relationId

// Returns the list of instances that can be linked to instanceIdIn via the relationAndDirection
let linkableInstanceIdsAction (instanceIdIn:InstanceId) (relationAndDirection: RelationAndDirection):DBAction.DBAction<InstanceId>=
    byIdAction instanceIdIn
    |> DBAction.bind (fun instance ->
        match relationAndDirection with
        | ParentsVia relationIdIn ->
            RelationExtensions.byIdAction relationIdIn
            |> DBAction.bind (fun relation ->
                match relation.child_side_type_id,relation.parent_side_type_id with
                |1,1 ->
                    // We use pgsql Common Table Expressions here:
                    // - existing: counts existing linked instances through this relation
                    // - available: the instances we can link of no instance is already linked. This queury is only
                    //   used when no instance is already linked, so there's no need for a where clause
                    // The trick here , found at [1], is to join existing with available and select the rows where count is 0.
                    // If existing has 1 in its value count, no row of this query will be selected, achieving the required result.
                    // 1: https://stackoverflow.com/questions/33146305/firing-different-query-in-postgres-based-on-condition-returned-by-another-query
                    DBAction.queryAction
                        """
                        WITH existing AS (select count(i.*) from instances i join links l on (i.id=l.parent_id and l.relation_id=%d) where l.child_id=%d),
                        available AS (select * from instances where entity_id = %d and id not in (select parent_id from links where relation_id=%d))
                        select available.* from available,existing where count=0
                        """
                        (relation.id|> RelationId.Get)
                        (instance.id|>InstanceId.Get)
                        (relation.parent_id|> EntityId.Get)
                        (relation.id|> RelationId.Get)
                |1,2 ->
                    DBAction.queryAction
                        """
                            select * from instances where entity_id = %d and id not in (select parent_id from links where relation_id=%d)
                        """
                        (relation.parent_id|> EntityId.Get)
                        (relation.id|> RelationId.Get)

                |2,1 ->
                    DBAction.queryAction
                        """
                        WITH existing AS (select count(i.*) from instances i join links l on (i.id=l.parent_id and l.relation_id=%d) where l.child_id=%d),
                        available AS (select * from instances where entity_id = %d)
                        select available.* from available,existing where count=0
                        """
                        (relation.id|> RelationId.Get)
                        (instance.id|>InstanceId.Get)
                        (relation.parent_id|> EntityId.Get)
                // to many, no need to check existing links
                | _ ->
                    DBAction.queryAction
                        "select * from instances where entity_id=%d and id not in (select parent_id from links where child_id=%d and relation_id=%d)"
                            (relation.parent_id|> EntityId.Get)
                            (instance.id|>InstanceId.Get)
                            (relation.id|> RelationId.Get)
            )
        | ChildrenVia relationIdIn ->
            RelationExtensions.byIdAction relationIdIn
            |> DBAction.bind (fun relation ->
                match relation.parent_side_type_id, relation.child_side_type_id with
                // One to one, only display if we have no one yet, and only those not yet involved in this relation id
                |1,1 ->
                    // See above for explanation of this query
                    DBAction.queryAction
                        """
                        WITH existing AS (select count(i.*) from instances i join links l on (i.id=l.child_id and l.relation_id=%d) where l.parent_id=%d),
                        available AS (select * from instances where entity_id = %d and id not in (select child_id from links where relation_id=%d))
                        select available.* from available,existing where count=0
                        """
                        (relation.id|> RelationId.Get)
                        (instance.id|>InstanceId.Get)
                        (relation.child_id|> EntityId.Get)
                        (relation.id|> RelationId.Get)
                // One to many, only display if we have no one yet, and all those possible
                |1,2 ->
                    // See above for explanation of this query
                    DBAction.queryAction
                        """
                            select * from instances where entity_id = %d and id not in (select child_id from links where relation_id=%d)
                        """
                        (relation.child_id|> EntityId.Get)
                        (relation.id|> RelationId.Get)
                // Many to one: display all those not yet involved in this relation id
                |2,1 ->
                // to many, no need to check existing links, but exclude those already linked to us with this relation id
                    DBAction.queryAction
                        """
                        WITH existing AS (select count(i.*) from instances i join links l on (i.id=l.child_id and l.relation_id=%d) where l.parent_id=%d),
                        available AS (select * from instances where entity_id = %d and id not in (select child_id from links where parent_id=%d and relation_id=%d))
                        select available.* from available,existing where count=0
                        """
                        (relation.id|> RelationId.Get)
                        (instance.id|>InstanceId.Get)
                        (relation.child_id|> EntityId.Get)
                        (instance.id|>InstanceId.Get)
                        (relation.child_id|> EntityId.Get)
                | _ ->
                    DBAction.queryAction
                        "select * from instances where entity_id=%d and id not in (select child_id from links where parent_id=%d and relation_id=%d)"
                            (relation.child_id|> EntityId.Get)
                            (instance.id|>InstanceId.Get)
                            (relation.id|> RelationId.Get)
            )
    )
    |> DBAction.map (fun (instance:Instance) -> instance.id )

// return a datatable holding the data for linked instance returned by the crosstab query
let linkedInstanceIdsAction (instanceId:InstanceId) (relationAndDirection: RelationAndDirection)=
    linksAction instanceId relationAndDirection
    // from the links list, get the related instances ids
    |> DBAction.bind (fun link -> Link.getLinkedInstanceId relationAndDirection link
                                 |> function
                                    | Ok id -> DBAction.retn id
                                    | Error e -> DBAction.error (Error.toString e)
                                 )

// Helper to turn DBAction with a list of instanceIds to filter with into a DBAction with the result of the
// selectFilteredFunction, which can be a DataTable if displaying a list, or a int64, if counting rows.
let instanceIdsFilteredSelect relationAndDirection clauses selectFilteredFunction instanceIdsAction =
    instanceIdsAction
    |> DBAction.workOnList
    |> DBAction.bind (fun instanceIds ->
        RelationExtensions.entityIdForRelationAction relationAndDirection
        |> DBAction.bind (fun entityId ->
            selectFilteredFunction entityId (Some instanceIds) clauses
        )
    )
// return a datatable holding the data for linkable instance returned by the crosstab query
let linkableInstancesAction (instanceId:InstanceId) (relationAndDirection: RelationAndDirection) (clauses:ListClauses):DBAction.DBAction<DataTable> =
    linkableInstanceIdsAction instanceId relationAndDirection
    |> instanceIdsFilteredSelect relationAndDirection clauses selectFilteredDataTableAction
// return a datatable holding the data for linked instance returned by the crosstab query
let linkedInstancesAction (instanceId:InstanceId) (relationAndDirection: RelationAndDirection) (clauses:ListClauses)=
    linkedInstanceIdsAction instanceId relationAndDirection
    |> instanceIdsFilteredSelect relationAndDirection clauses selectFilteredDataTableAction
// counting linked instances
let countLinkedInstancesAction (instanceId:InstanceId) (relationAndDirection: RelationAndDirection) (clauses:ListClauses):DBAction.DBAction<int64> =
    linkedInstanceIdsAction instanceId relationAndDirection
    |> instanceIdsFilteredSelect relationAndDirection (countFor clauses) selectFilteredCountAction
// counting linkable instances
let countLinkableInstancesAction (instanceId:InstanceId) (relationAndDirection: RelationAndDirection) (clauses:ListClauses):DBAction.DBAction<int64> =
    linkableInstanceIdsAction instanceId relationAndDirection
    |> instanceIdsFilteredSelect relationAndDirection (countFor clauses) selectFilteredCountAction

let deleteAction (id:InstanceId ) =
    DBAction.statementAction "delete from instances where id=%d" (InstanceId.Get id)
(*
let createOrUpdateFromSpec spec =
    let instanceAction=createOrUpdateAction spec
    // CONTINUE HERE
    let detailValuesAction = DetailValue.createOrUpdate instanceAction spec.detailValues
    instanceAction
*)
//let handleDetailValues (detailValues:DetailValue.Spec array) (instanceId:Id)=
//    let action= (client:DB.DBClient):DBResult.DBResult

let getEntityAction (instance:Instance) =
    EntityExtensions.byIdAction (instance.entity_id)
