module DataTypeExtensions

open DataType
open DBAction

let GetAllAction () : DBAction.DBAction<DataType>=
  DBAction.queryAction "select * from data_types order by id"
