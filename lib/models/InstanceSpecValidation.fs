namespace MyOwnDBLib

// Functions cleaning and validating the InstanceSpc received from the client.
open Ids
open Specs
open MyOwnDBLib.Logging

module Instance =
    // helper function used to check the count query returned 1
    let checkCountIsOne (errorMessage:string)(count:int64) =
        if count <>1L then
            MyOwnDBLib.Logging.Log.Error($"InstanceSpec inconsistency: {errorMessage}")
            DBAction.error $"Ids inconsistency count is {count}"
        else
            DBAction.retn count

    let ddlValuesUpdateAdditionalJoin (value:ValueSpec) (entityId:EntityId) (instanceId:InstanceId) (detailId:DetailId)=
        // for a ddl_detail_value, we define an additional join to the detail_value_propositions table to validate the proposition id received
        // We want all checks to be done thanks to the additional join so that the same base query can be used and only the additional join can be
        // added without additional arguments to be passed to the query
        match value.value with
        | ClientProposed (ClientValue.DdlValueChoice (Some choiceId)) ->
            // The join is not "on (dvp.id=dv.detail_value_proposition_id)" because dv has still the old proposition id as value!
            // We just want to validate that the dvp can be linked to the proposition, hence this join
            sprintf
                "join detail_value_propositions dvp on (dvp.id=%d and dvp.detail_id=%d)"
                choiceId
                (detailId |> DetailId.Get)
        | ClientProposed (ClientValue.DdlValueChoice None) ->
            // No id for a proposition given when deleting the value, so we can't and don't need to check it here.
            ""
        | _ ->
            // Receiving a ServerValidated value should never happen and we shouldn't get any other ClientProposed value here
            // We fail here to avoid errors.
            Log.Error( "Wrong DU wrapping in validation of ddl value choice {value} for entity {entityId} instance {instanceId}, detail {detailId} detail value {detailValue}",
                                    value,
                                    (entityId|>EntityId.Get),
                                    (instanceId|>InstanceId.Get),
                                    (detailId|>DetailId.Get),
                                    id
                                    )
            failwithf "should never happen to have this value here %A" value


    // Returns a tuple made of
    // - the table in which to look for the detail value
    // - the id of the row in that table identifying the detail value
    // - additional joins to use to validate ids are coherent
    let getUpdateQueryDetails (value:ValueSpec) (entityId:EntityId) (instanceId:InstanceId) (detailId:DetailId)  =
        match value.detailValueId with
        | Some (DetailValueId id) -> "detail_values", id, ""
        | Some (DateDetailValueId id) -> "date_detail_values", id,""
        | Some (DdlDetailValueId id) ->
            "ddl_detail_values", id, (ddlValuesUpdateAdditionalJoin value entityId instanceId detailId)
        | Some (IntegerDetailValueId id) -> "integer_detail_values", id,""
        // None values should never get here as they follow another path code
        | None -> failwith "should never have gotten a None value here"

    let getNewDetailValueQueryDetails (value:Value) (detailId:DetailId) =
        match value with
        | ClientProposed (ClientValue.DdlValueChoice (Some id)) ->
            sprintf
                "join detail_value_propositions dvp on (dvp.id=%d and dvp.detail_id=%d)"
                id
                (detailId |> DetailId.Get)
        | _ -> ""
    // ** Query to validate the update of an existing detail_values entry (hence of an existing instance).
    // It validates consistency of 2 chains:
    // - detail_values.instance_id -> instance.entity_id -> entities.database_id
    // - detail_values.detail_id -> details.database_id
    // We known the instance id, entity id, detail id  and detail_value id which are all 4 used in the query.
    // The query should return exactly 1 row for consistent data.
    // select count(*) from detail_values dv
    //      join details d on (dv.detail_id=d.id)
    //      join instances i on (dv.instance_id=i.id)
    //      join entities e on (i.entity_id=e.id)
    // where dv.id=2136101 and e.database_id = d.database_id and e.id=608 and d.id=1611 and i.id=51256;

    let isDetailValueIdDataTypeConsistent (valueSpec:ValueSpec) =
        match valueSpec.detailValueId, valueSpec.value with
        // if no detail value id is passed, inconsistency is not possible
        | None,_ -> true
        // check data types stored in table detail_values, which hence have the
        // id of type DetailValueId
        | Some (DetailValueId _), ClientProposed (ClientValue.Simple _ )
        | Some (DetailValueId _), ServerValidated (ServerValue.Simple _ )
        | Some (DetailValueId _), ClientProposed (ClientValue.LongText _ )
        | Some (DetailValueId _), ServerValidated (ServerValue.LongText _ )
        | Some (DetailValueId _), ClientProposed (ClientValue.Email _ )
        | Some (DetailValueId _), ServerValidated (ServerValue.Email _ )
        | Some (DetailValueId _), ClientProposed (ClientValue.WebURL _ )
        | Some (DetailValueId _), ServerValidated (ServerValue.WebURL _ )
        | Some (DetailValueId _), ClientProposed (ClientValue.FileAttachment _ )
        | Some (DetailValueId _), ServerValidated (ServerValue.FileAttachment _ )
            -> true
        // Ddl detail values stored in ddl_detail_values
        | Some (DdlDetailValueId _), ClientProposed (ClientValue.DdlValueChoice _ )
        | Some (DdlDetailValueId _), ServerValidated (ServerValue.DdlValueChoice _ )
            -> true
        // dates, stored in date_detail_values
        | Some (DateDetailValueId _), ClientProposed (ClientValue.DateAndTime _ )
        | Some (DateDetailValueId _), ServerValidated (ServerValue.DateAndTime _ )
            -> true
        // integers, stored in integer_detail_values
        | Some (IntegerDetailValueId _), ClientProposed (ClientValue.Integer _ )
        | Some (IntegerDetailValueId _), ServerValidated (ServerValue.Integer _ )
            -> true
        // all other combinations are inconsistent
        | _,_ -> false

    let checkDetailValueIdDataTypeConsistency (spec:InstanceSpec) =
        let inconsistenciesList =
          spec.detailValues
          |> List.filter (fun detailValueSpec ->
              detailValueSpec.values
              |> List.filter (
                        // we want to check if an inconsistent value is in the list, hence we
                        // want to return true for an inconsistent value
                        isDetailValueIdDataTypeConsistent >> not
              )
              |> (fun l -> List.length l > 0)
          )
        if List.length inconsistenciesList > 0 then
            MyOwnDBLib.Logging.Log.Error("detail value id and datatype inconsistency detected in {inconsistenciesList}", inconsistenciesList)
            let errorMsg = sprintf "detail value id type and value inconsistency detected in %A" inconsistenciesList
            DBAction.error errorMsg
        else
            DBAction.retn spec

    let checkDetailValueIdsConsistencyUpdateAction (value:ValueSpec) (entityId:EntityId) (instanceId:InstanceId) (detailId:DetailId) =
        let tableName, dvId, specificJoins =
            getUpdateQueryDetails value entityId instanceId detailId

        let entityIdInt =  entityId|>EntityId.Get
        let instanceIdInt = instanceId|>InstanceId.Get
        let detailIdInt =  detailId|>DetailId.Get
        DBAction.queryAction
            "
                select count(*) from %A dv
                    join details d on (dv.detail_id=d.id)
                    join instances i on (dv.instance_id=i.id)
                    join entities e on (i.entity_id=e.id)
                    join entities2details e2d on (e2d.detail_id=d.id and e2d.entity_id=e.id)
                    %A
                where e.database_id = d.database_id and e.id=%d and i.id=%d and d.id=%d and dv.id=%d
            "
            (DBAction.StaticArguments.Static tableName)
            (DBAction.StaticArguments.Static specificJoins)
            entityIdInt
            instanceIdInt
            detailIdInt
            (dvId)
        // map to an error if count is not 1
        |> DBAction.bind  (checkCountIsOne $"detailValue ids inconsitency in update. entity_id = {entityIdInt}, instanceId = {instanceIdInt}, detailId = {detailIdInt}, detail value id = {dvId}")
    //
    // ** Query to validate a new detail_value for an existing instance
    // It validates the consistency of the chain
    // detail.database_id -> database <- entity.database_id <- instance.entity_id
    // This ensures the detail linked from the detail_value is in the correct database
    // We know the entity, instance and detail ids, which are used in the query that should return exactly one row:
    // select count(*) from details d join databases db on (d.database_id=db.id) join entities e on (e.database_id=db.id) join instances i on (i.entity_id=e.id) where e.id=608 and i.id=51256 and d.id=1606
    //

    let checkDetailValueIdsConsistencyNewDetailValueAction (value:ValueSpec) (entityId:EntityId) (instanceId:InstanceId) (detailId:DetailId) =
        let specificJoins = getNewDetailValueQueryDetails value.value detailId
        let entityIdInt =  entityId|>EntityId.Get
        let instanceIdInt = instanceId|>InstanceId.Get
        let detailIdInt =  detailId|>DetailId.Get
        DBAction.queryAction
            "
                select count(*) from details d
                    join databases db on (d.database_id=db.id)
                    join entities e on (e.database_id=db.id)
                    join instances i on (i.entity_id=e.id)
                    join entities2details e2d on (e2d.detail_id=d.id and e2d.entity_id=e.id)
                    %A
                    where e.id=%d and i.id=%d and d.id=%d

            "
            (DBAction.StaticArguments.Static specificJoins)
            entityIdInt
            instanceIdInt
            detailIdInt
        // map to an error if count is not 1
        |> DBAction.bind (checkCountIsOne $"detail value id inconsistency in creation entity_id = {entityIdInt}, instanceId = {instanceIdInt}, detailId = {detailIdInt}")



    // ** Query to validate a new detail_value for a new instance
    // It validates the consistency of the chain
    // detail.database_id -> database <- entity.database_id
    // select d.* from details d join databases db on (d.database_id=db.id) join entities e on (e.database_id=db.id) where  e.id=608 and d.id=1606;
    let checkDetailValueIdsConsistencyNewInstanceAction (value:ValueSpec) (entityId:EntityId) (detailId:DetailId) =
        let specificJoins = getNewDetailValueQueryDetails value.value detailId
        let entityIdInt =  entityId|>EntityId.Get
        let detailIdInt =  detailId|>DetailId.Get
        DBAction.queryAction
            "
                select count(*) from details d
                    join databases db on (d.database_id=db.id)
                    join entities e on (e.database_id=db.id)
                    join entities2details e2d on (e2d.detail_id=d.id and e2d.entity_id=e.id)
                    %A
                where  e.id=%d and d.id=%d;
            "
            (DBAction.StaticArguments.Static specificJoins)
            entityIdInt
            detailIdInt
        // map to an error if count is not 1
        |> DBAction.bind (checkCountIsOne $"detail value id inconsistency for new instance entity_id = {entityIdInt}, detailId = {detailIdInt}")


    // retains only detailValues for which at least one value is proposed by the client
    // (removes all detailValues with only ServerValidated values)
    let filterDetailValuesWithClientProposed (spec:InstanceSpec) =
        {spec with
            detailValues =
                spec.detailValues
                // Keep only detailValues for which there is a ClientProposed value
                |> List.filter (fun dv ->
                    dv.values
                    |> List.exists (fun v ->
                            match v.value with
                            | ClientProposed _ -> true
                            | _ -> false)
                    )
        }
    // retains only client proposed values for the detailsValues
    // (does not remove any detailValue, only ServerValidated values inside a detailValue)
    // only useful when we have multiple values for a detailValue.
    let filterKeepOnlyClientProposedValues (spec:InstanceSpec) =
        { spec with
            detailValues =
                spec.detailValues
                |> List.map (fun dv ->
                    { dv with
                        values =
                            dv.values
                            |> List.filter (fun v ->
                                match v.value with
                                | ClientProposed _ -> true
                                | _  -> false
                            )
                    }
                )
                // Remove DetaliValueSpec with an empty list of values.
                // This would probably not happen, but cleaner and to be sure.
                |> List.filter (fun dv ->
                    (List.length dv.values) > 0
                )
        }

    // Validate the ClientProposed ValuesSpecs passed as argument and returns the corresponding ServerValidated ValueSpecs
    // if the value is valid
    let rec validateValuesinDetailValueAction (detailId:Ids.DetailId)(values:ValueSpec list) =
        let cons h t = h::t
        let (<*>) = DBAction.apply
        let retn = DBAction.retn

        match values with
        |[] -> retn []
        |valueSpec :: t ->
            retn cons
            <*>
            (
                match valueSpec.value, valueSpec.value.IsSome() with
                | ClientProposed v, true ->
                    DetailValue.buildServerValueAction detailId (Some (valueSpec.value.ToString()))
                    |> DBAction.map (fun v -> { valueSpec with value = ServerValidated v })
                | ClientProposed v, false ->
                    DetailValue.buildServerValueAction detailId None
                    |> DBAction.map (fun v -> { valueSpec with value = ServerValidated v })
                | ServerValidated _, _  ->
                    MyOwnDBLib.Logging.Log.Error("ServerValidated value {value} found when only ClientProposed should be seen", valueSpec.value)
                    DBAction.error "Inconsistent data"
            )
            <*>
            validateValuesinDetailValueAction detailId t

    // Go through DetailValueSpecs and validate their values.
    let rec getValidatedDetailValuesAction (dvs:Specs.DetailValueSpec list) =
        let cons h t = h::t
        let (<*>) = DBAction.apply
        let retn = DBAction.retn

        match dvs with
        | [] -> retn []
        | dv::tail ->
            (retn cons)
            <*> (
                // As a DetailValueSpec wraps a list of values, we need to dive a level deeper
                validateValuesinDetailValueAction (dv.detailId) (dv.values)
                // We get back a list of ValueSpecs we need to place inside dv to update it
                |> DBAction.map (fun values ->
                    { dv with values = values}
                )
            )
            <*> (getValidatedDetailValuesAction tail)

    // Check that detail_id, detail_value_id, instance_id, possibly ddl_detail_value_id and
    let checkIdsConsistency (spec:InstanceSpec) =
        let cons h t = h::t
        let (<*>) = DBAction.apply
        let retn = DBAction.retn

        let checkOneValueIdsConsistency (detailId:DetailId) (value:ValueSpec) =
            match spec.instanceId, value.detailValueId with
            | Some instanceId, Some detailValueId -> checkDetailValueIdsConsistencyUpdateAction value spec.entityId instanceId detailId
            | Some instanceId, None -> checkDetailValueIdsConsistencyNewDetailValueAction  value spec.entityId instanceId detailId
            | None, _ -> checkDetailValueIdsConsistencyNewInstanceAction value spec.entityId detailId

        let rec checkOneDetailValueIdsConsistency (detailId:DetailId) (values:ValueSpec list) =
            match values with
            |[] -> retn []
            |valueSpec :: t ->
                retn cons
                <*>
                (checkOneValueIdsConsistency detailId valueSpec)
                <*>
                (checkOneDetailValueIdsConsistency detailId t)


        // Check Ids consistency of on DetailValueSpec
        let rec checkDetailValuesIdsConsistency (dvs:Specs.DetailValueSpec list) =
            match dvs with
            | [] -> retn []
            | dv::tail ->
                (retn cons)
                <*>
                (checkOneDetailValueIdsConsistency dv.detailId dv.values)
                <*>
                (checkDetailValuesIdsConsistency tail)

        checkDetailValuesIdsConsistency (spec.detailValues)
        // if the action is a success, we don't care about its value
        // IMPROVE: we have nested lists here, clean up the values returned in nested function calls
        // to avoid those nested lists
        |> DBAction.map (fun _ -> spec)




    let sanitiseSpecAction (spec:InstanceSpec):DBAction.DBAction<InstanceSpec> =
        DBAction.retn spec
        |> DBAction.map filterDetailValuesWithClientProposed
        |> DBAction.map filterKeepOnlyClientProposedValues
        |> DBAction.bind checkDetailValueIdDataTypeConsistency
        |> DBAction.bind checkIdsConsistency
        |> DBAction.bind (fun spec ->
            getValidatedDetailValuesAction (spec.detailValues)
            |> DBAction.map (fun validDetailValues ->
                { spec with
                    detailValues = validDetailValues
                }
            )
        )