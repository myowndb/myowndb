module IDetailValue

type  IDetailValue =
    abstract GetStringValue: unit -> string
    abstract GetDetailId: unit -> Ids.DetailId
    abstract GetId: unit -> Ids.DetailValueIdType
    abstract GetInstanceId: unit -> Ids.InstanceId