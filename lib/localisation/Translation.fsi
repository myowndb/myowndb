module Translation
/// The Translation module include facilities to localise text generate by the
/// application.
/// Localisation is managed with NGettext. Translations are kept in `.pot` files.
/// The code is inspired by the reader monad. The goal was to be able to generated
/// strings to be localised by low level code, without the need to know in which
/// locale that string should be displayed.
/// As such, a translation is create by calling a function (<c>create</c> or <c>plural</c>)
/// taking as argument the  id of the string to be translated (in printf format string)
/// and its arguments. To generate the localised string, this translation is passed
/// to the function <c>/tr</c> with also the locale to translate to.
/// The id of the string to localise always starts with "madb_tr", and it can also include
/// variable place holders, like for printf format strings. Here is an example:
/// <c>"madb_tr  the email %s is invalid"</c>
/// This translation has one place holder for a string. The value to be placed at this
/// place holder must be provided at the time of creation of the translation, for example:
/// <c>let myTranslation = Translations.create "madb_tr  the email %s is invalid" "#example.com"</c>
/// Code needing to display the translated string then has to use `tr`:
/// <c>
/// let myLocale = Locale.fromString "en_US"
/// let localisedString = Translation.tr myLocale myTranslation
/// </c>
/// For the localisation to be successful, the id to be translated must be present in the `pot` file
/// for the locale in `po/$locale/$file.pot` (file name might be dependent of software/library component).
/// As NGettexct uses C# <c>String.format</c>, the printf like place holders have to be replaced by index
/// based place holders. For example:
/// <c>
/// msgid "madb_tr email %s is invalid"
/// msgstr "Email {0} is invalid"
///</c>
/// Singular/plural translations are also supported. They are created with the function <c>Translations.plural</c>
/// which also takes as first argument the int64 number determining which translation (singular or plural) must be displayed.
/// Example:
///<c>
/// let pluralT1 = Translation.plural (int64 1) "madb_tr there are %d errors" 1
///</c>
/// The first argument is the number determining if the singular or plural translation is to be chosed, and after
/// that comes the format string and its arguments (that's why the number, 1 in the example, is passed 2 times
/// as argument: first for singular form choixe, than as format string argument).
/// Generating the localised string is done identically:
/// <c>let pluralEn1 = Translation.tr (Locale.fromString "en_US") pluralT1</c>
/// In the po file however, the pluralisable translation need additional info:
/// <c>
/// msgid "madb_tr there are %d errors"
/// msgid_plural "madb_tr there are %d errors"
/// msgstr[0] "There is {0} error"
/// msgstr[1] "There are {0} errors"
/// </c>
/// For english, `msgstr[0]` is the singular form, `msgstr[1]` is plural.
/// Note that msgid and msgid_plural are identical (this is be cause the use translation ids
/// and is required in this project, though this is just a convetion and other projects might
/// do otherwise).
/// See README for information on how to extract message ids from the code.

/// The translation is kept in an opaque format, and is only manipulated by function of the module.
type t


/// This is a configuration record used by the translation engine.
/// directory is the path of the base dir for translations, and
/// file is the filename to use.
/// The default value set is
///     {directory= "./po"; file="lib"}
/// Which will make translations for the en_US locale searched in
/// the file ./po/en_US/LC_MESSAGES/library.mo
type TranslationConfig =
    { directory: string
      file: string }


/// <summary>Translate the translation passed using the catalog. Should mostly be used inside the module.</summary>
val translateWithCatalog: NGettext.Catalog -> t -> string
/// <summary>Translate the translation passed using the config and locale to initialise the catalog to be used. Should mostly be used inside the module.</summary>
val translateWithConfig: TranslationConfig -> Locale.t -> t -> string
/// <summary>Returned the localised string corresponding to the translation in the locale passed as argument</summary>
/// <param name="locale">The locale to translate to</param>
/// <param name="translation">A printf format string, with possible place holders, which is the translation id and starts with "madb_tr"</param>
/// <returns>The localised string</returns>
val tr: locale:Locale.t -> translation:t -> string
/// <summary>Create a simple translation. It has no single/plural forms to choose from, but
/// accepts arguments.
/// This function returns a function that will consume arguments to be place in the format string
/// in place of place holders. Usually, these values are passed directly as additional arguments to
/// this call, eg <c>Translation.create "madb_tr the email %s is invalid" "blurps" </c></summary>
/// <param name="format">A printf format string, with possible place holders</param>
/// <returns>A function that will consume the values to replace place holders with. Is usually called immediately by passing the values as additional arguments</returns>
val create: format:PrintfFormat<'a, _, _, t> -> 'a
/// <summary>Create a translation supporting single/plural forms to choose from.
/// It also accepts arguments.
/// This function returns a function that will consume arguments to be place in the format string
/// in place of place holders. Usually, these values are passed directly as additional arguments to
/// this call, eg <c>Translation.create "madb_tr the email %s is invalid" "blurps" </c></summary>
/// <param name="number">The value that will determine if the single or plural form is chosen for
/// the translation</param>
/// <param name="format">A printf format string, with possible place holders, which is the translation id and starts with "madb_tr"</param>
/// <returns>A function that will consume the values to replace place holders with. Is usually called immediately by passing the values as additional arguments</returns>
val plural: number:int64 -> format:PrintfFormat<'a, _, _, t> -> 'a
/// <summary>Funtion that will return a function generating the localised string rathen than a translation.
/// As for other functions, the format string arguments are usually passed directly to the returned function. This uses the default translation config and will
/// use the default po file in the default location.</summary>
/// <param name="locale">The locale name to translate to, as string. Having the locale as first argument might be handy for partial application.</param>
/// <param name="id">A printf format string, with possible place holders, which is the translation id and starts with "madb_tr"</param>
/// <returns>A function that will consume the values to replace place holders with. Is usually called immediately by passing the values as additional arguments</returns>
val immediate: locale:string -> id:PrintfFormat<'a, _, _, string> -> 'a
/// <summary>Funtion that will return a function generating the localised single or plural string rathen than a translation.
/// As for other functions, the format string arguments are usually passed directly to the returned function</summary>
/// <param name="locale">The locale name to translate to, as string. Having the locale as first argument might be handy for partial application.</param>
/// <param name="number">Number determining if the singular or plural form is needed</param>
/// <param name="id">A printf format string, with possible place holders, which is the translation id and starts with "madb_tr"</param>
/// <returns>A function that will consume the values to replace place holders with. Is usually called immediately by passing the values as additional arguments</returns>
val immediateP: locale:string -> number:int -> id:PrintfFormat<'a, _, _, string> -> 'a
/// <summary>Apply a function to the string that will eventually be generated from the translation.</summary>
/// <param name="mapping">Function taking as input a string (the translated string) and returning a string</param>
/// <param name="translation">The translation to which the mapping is to be applied</param>
/// <returns>A modified translation</returns>
val map: mapping:(string -> string) -> translation:t -> t
/// Like map, but the mapping returns a translation, not a string.
val bind: (string -> t) -> t -> t
