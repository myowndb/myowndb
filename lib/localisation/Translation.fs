module Translation

open NGettext
open System.Globalization
open System.IO


type t = Translation of (NGettext.Catalog -> string)

// see .fsi for doc
type TranslationConfig =
    { directory: string
      file: string }

/// Configuration for default translation files location and name.
let defaultConfig =
    { directory = "./po"
      file = "lib" }

// see .fsi for doc
let translateWithCatalog (catalog: Catalog) (Translation f) = f catalog

// see .fsi for doc
let translateWithConfig (config: TranslationConfig) (locale: Locale.t) (translation) =
    let catalog = Catalog(config.file, config.directory, CultureInfo(Locale.toString locale))
    translateWithCatalog catalog translation

// see .fsi for doc
let tr (locale: Locale.t) (translation) = translateWithConfig defaultConfig locale translation

let create format =
    // We need to pass a callback to the function handling the PrintfFormat to get
    // the format string and all info about the values passed to it
    // This callback will return a Translation.t value holing a function calling
    // NGettext's GetString function to get the translated string.
    let translationCallback (format: string) (types: System.Type list) (values: obj list) =
        let newT (catalog: Catalog) = catalog.GetString(format, values |> Seq.toArray)
        Translation newT
    Utils.formatStringHandler translationCallback format

let plural number format =
    // We need to pass a callback to the function handling the PrintfFormat to get
    // the format string and all info about the values passed to it
    // This callback will return a Translation.t value holing a function calling
    // NGettext's GetPluralString function to get the translated string.
    // GetPluralString takes as argument:
    // - singular string
    // - plural string. As we work with translation ids, this is equal to the singular identical
    // - number as int64 determining if we need the singular or plural form
    // - the translation string arguments.
    let translationCallback (format: string) (types: System.Type list) (values: obj list) =
        let newT (catalog: Catalog) =
            // the id for our single and plural formas are identical, that's why we pass format
            // as the 2 first arguments.
            // After that comes the number determining if we have single or plural form,
            // then the format string arguments.
            catalog.GetPluralString(format, format, number, values |> Seq.toArray)
        Translation newT
    Utils.formatStringHandler translationCallback format

let immediate (locale: string) id =
    // We need to pass a callback to the function handling the PrintfFormat to get
    // the format string and all info about the values passed to it
    // This callback returns the translated string.
    let immediateTranslationCallback (format: string) (types: System.Type list) (values: obj list): string =
        let catalog = Catalog(defaultConfig.file, defaultConfig.directory, CultureInfo(locale))
        catalog.GetString(format, values |> Seq.toArray)
    Utils.formatStringHandler immediateTranslationCallback id

let immediateP (locale: string) number id =
    // We need to pass a callback to the function handling the PrintfFormat to get
    // the format string and all info about the values passed to it
    // This callback returns the translated string.
    let immediateTranslationCallback (format: string) (types: System.Type list) (values: obj list): string =
        let catalog = Catalog(defaultConfig.file, defaultConfig.directory, CultureInfo(locale))
        catalog.GetPluralString(format, format, (int64 (number)), values |> Seq.toArray)
    Utils.formatStringHandler immediateTranslationCallback id


let map mapping translation =
    let newTf (catalog: Catalog) =
        let s = translateWithCatalog catalog translation
        mapping s
    Translation newTf

let bind f xtr =
    let newTf (catalog: Catalog) =
        let x = translateWithCatalog catalog xtr
        translateWithCatalog catalog (f x)
    Translation newTf
