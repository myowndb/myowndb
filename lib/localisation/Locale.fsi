module Locale

/// The Locale type is opaque, and can only be manipulated with the module's functions.
type t


/// <summary>Generate a locale from a locale name, such as "en_US"</summary>
val fromString: string -> t
/// Returns the local name
val toString: t -> string
/// True if the local name is the empty string.
val isEmpty: t -> bool
/// <summary>True if the locale corresponds to the locale name</summary>
/// <param name="locale">Locale to test</param>
/// <param name="name">A string to test the locale against, for example "en_US"</param>
/// <returns>True if the locale name corresponds to the string, false ottherwise.</returns>
val isString: t -> string -> bool
val is: t -> t -> bool
