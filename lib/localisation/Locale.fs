module Locale

type t = S of string

let id = S "id-id"
let fromString (s) = S s

let toString (S s) = s

let isEmpty (S s) = s = ""

let isString (S s) (test: string) = s = test

let is (S s) (S s2) = s = s2
