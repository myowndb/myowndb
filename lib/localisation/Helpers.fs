namespace Myowndb.I18n
open NGettext
open System
// NGettext translation helpers.
// Instanciate with the right catalog, and use the methods to get translations from it.
type Helpers(catalog:Catalog) =
    // t: translate
    // p: with parameters
    // n: with number for plural, implies with parameters so never combined with p
    // c: with context
    member _.t (s,[<ParamArray>] values:obj array) =
        catalog.GetString(s,values)
    member _.p (s:string ,[<ParamArray>]values:obj array)=
        catalog.GetString(s,values)
    member _.n (singular, plural, selector,[<ParamArray>]values:obj array)=
        catalog.GetPluralString(singular,plural,selector,values)
    member _.c (context, s)=
        catalog.GetParticularString(context,s)
    member _.cp (context, s, [<ParamArray>]values:obj array)=
        catalog.GetParticularString(context,s,values)
    member _.cn (context, singular, plural, selector,[<ParamArray>]values:obj array)=
        catalog.GetParticularPluralString(context,singular,plural,selector,values)