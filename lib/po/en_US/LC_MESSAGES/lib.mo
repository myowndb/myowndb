��          t      �         -     (   ?  7   h  8   �  B   �       $   8     ]  7   }  *   �  4  �  &     !   <  E   ^  V   �  S   �     O     d     �  '   �  #   �                   	          
                               madb_tr One row expected in result but got %d madb_tr String does not match pattern %s madb_tr String is too long, needed length <= %d, got %d madb_tr String is too short, needed length >= %d, got %d madb_tr String length incorrect, needed exact length of %d, got %d madb_tr email %s is invalid madb_tr single row query got %d rows madb_tr the email %s is invalid madb_tr there are %d errors madb_tr there are %d errors madb_tr user with email %s is not verified Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-03-07 09:16+0100
Last-Translator: rb <rb@myowndb.com>
Language-Team: English
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=ASCII
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 One row expected in result but got {0} String does not match pattern {0} String is too long. Maximum accepted is {0} but string has length {1} String is too short. Minimum required length is {0} but string is {1} characters long. String length incorrect. Required length is {0}, but string is {1} characters long. Email {0} is invalid Single row query got {0} rows The email {0} is invalid There is {0} error There are {0} errors User with email {0} is not verified 