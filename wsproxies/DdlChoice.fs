module DdlChoice
// This module provides the hidden type ChoiceId, representing the id of the
// detail_value_proposition chosen for a particular detail.
// Having it hidden ensures the chosen id belongs to the propositions for the detail,
// as the queries used below limit the results to the scode of the detail in question.
open Error
open Ids

// A record type to hold information about a ddl value proposition.
type Proposition =
    {
        id: int
        detail_id: int
        value: string
    }
type t = ChoiceId of Proposition

// Get the textual valu of the proposition
let toString  (ChoiceId {id=id;detail_id=detailId;value=value}): string =
    value

// Extract the id of the proposition from the value of hidden type
let toId (ChoiceId {id=id;detail_id=_;value=_}) = id
let toDetailId (ChoiceId {id=_;detail_id=detail_id;value=_}) = detail_id
