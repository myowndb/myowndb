module DBResult

open System.Xml
open System.Xml

open Error

type DBResult<'a> = Result<'a list, Error.Errors>

// function to easily define the List functions to be applied on the DBREsult like List.map.
let buildListFunction listFunction =
    let res f (x: DBResult<'a>): DBResult<'b> =
        match x with
        | Ok s -> DBResult.Ok(listFunction f s)
        | Error e -> DBResult.Error e

    res

let mapi f x = (buildListFunction List.mapi) f x
let map f x = (buildListFunction List.map) f x
let filter f x = (buildListFunction List.filter) f x

let mapError (f:Error.Errors->Error.Errors) (r:DBResult<'a>) =
    match r with
    | Ok _ -> r
    | Error e -> Error (f e)

// build an Ok DBResult with the list passed as argument
let wrapList (l: 'a list): DBResult<'a> = Ok(l)
// Useful if we have a DBResult<list>, this removes one level of list
let flatten (r:DBResult<'a list>): DBResult<'a> =
    match r with
    | Ok(l) -> wrapList (l |> List.concat)
    | Error e -> Error e


// Define fold manually as it takes an additional arg compared to functions defined by buildFunction
let fold (f: 's list -> 'a -> 's list) (init: 's list) (x: DBResult<'a>): DBResult<'s> =
    match x with
    | Ok s -> wrapList (List.fold f init s)
    | Error e -> Error e
// same for zip as for fold
let zip (a1:DBResult<'a>)(a2:DBResult<'b>):DBResult<'a*'b>=
        match a1,a2 with
        | (Error e1, Error e2) -> Error(Error.join e1 e2)
        | (Error e1, _) -> Error e1
        | (_, Error e2) -> Error e2
        | (Ok l1, Ok l2) -> Ok (List.zip l1 l2)




let bind (f: 'a -> DBResult<'b>) (x: DBResult<'a>): DBResult<'b> =
    let folder (state: DBResult<'c>) (i: DBResult<'c>) =
        match state, i with
        | Ok s, Ok v -> (Ok(List.append s v))
        | Error se, Error ve -> Error(Error.join se ve)
        | _, Error ve -> Error ve
        | Error se, _ -> Error se

    match x with
    | Ok s -> s |> (List.map f) |> List.fold folder (Ok([]))
    | Error e -> Error e

let apply (f: DBResult<'a -> 'b>) (x: DBResult<'a>): DBResult<'b> =
    match f, x with
    | Ok flist, Ok vlist -> Ok(List.collect (fun f -> List.map f vlist) flist)
    (* Alternate implementation possible, but in this case, for the functions sequence and the value
sequence need to have the same length.
                            let flist = fseq|>Seq.toList
                            let vallist = valseq |> Seq.toList
                            List.zip flist vallist |> List.map (fun (f,v) -> (f v) ) |> List.toSeq
*)
    | Error fe, Error ve -> Error(Error.join fe ve)
    | Error fe, _ -> Error fe
    | _, Error ve -> Error ve

// build an Ok DBResult wrapping a single value
let retn (x: 'a): DBResult<'a> = Ok([ x ])

// return an error DBResult
let error (msg:string): DBResult<'a> = Error (Error.fromString msg)
let errorFromExn (e:exn): DBResult<'a> = Error (Error.fromExn e)

// "neutral" value, DBResult with empty list. This was needed because the ret takes a parameter
let zero: DBResult<'a> = Ok([])

// Merges the lists held by the 2 db results
let merge (a1:DBResult<'a>)(a2:DBResult<'a>)=
        match a1,a2 with
        | (Error e1, Error e2) -> Error(Error.join e1 e2)
        | (Error e1, _) -> Error e1
        | (_, Error e2) -> Error e2
        | (Ok l1, Ok l2) -> Ok (List.append l1 l2)

let ensureSingleRow (i: DBResult<'a>) =
    match i with
    | Ok rows ->
        match List.length rows with
        | 1 -> (Ok rows)
        | 0 ->
            "Not found"
            |> error
        | _ ->
            $"One row expected in result but got %d{Seq.length rows}"
            |> error
    | Error e -> Error e

let get (r: DBResult<'a>) =
    match r with
    | Ok v -> v
    | Error e ->
        let msg =
            sprintf "Unexpected error when calling DBResult.get: %s" (e.ToString())

        failwith msg

let getFirst (r: DBResult<'a>) = r |> ensureSingleRow |> get |> List.head

let forall (f: 'a -> bool) (r: DBResult<'a>) =
    match r with
    | Ok i -> List.forall f i
    | Error e ->
        printfn "%s" (Error.toString e)
        false

// Let's see if this function is useful. Transforms the DBResult in a Result.
// If the DBResult holds only on row, do not return a list in the result, but
// the unique item of the list
let toSingleResult (i: DBResult<'a>) =
    i
    |> ensureSingleRow
    |> (fun i ->
        match i with
        | Ok rows -> (Result.Ok(List.head rows))
        | Error e -> Result.Error e)

let toResult (i: DBResult<'a>) =
    i
    |> (fun i ->
        match i with
        | Ok rows -> (Result.Ok rows)
        | Error e -> Result.Error (e |> Error.toStringList ))

let fromSingleResult (i: Result<'a, exn>) =
    match i with
    | Ok v -> Ok [ v ]
    | Error e -> error (e.ToString())

let workOnList (r:DBResult<'T>) : DBResult<'T list> =
    match r with
    | Ok l -> DBResult.Ok [ l ]
    | Error e -> DBResult.Error e

let workOnItems (r:DBResult<'T list>) : DBResult<'T> =
    match r with
    | Ok [l] -> DBResult.Ok l
    | Error e -> DBResult.Error e
    | _ -> failwith "workOnItem only handles one element lists"
