SHELL := /bin/bash
TESTS_DIR := tests
# gitlab-ci.yaml sets DOCKER_HOST=tcp://docker:2753/ to pass on gitlab shared runners, but this does not work on gitlab runner I deploy myself
# Here we create a makefile variable to be used in the place of the docker command. It sets the DOCKER_HOST value to the value of ${RUNNER_DOCKER_HOST} if it is defined
# otherwise it uses the value of DOCKER_HOST. In short, RUNNER_DOCKER_HOST overrides DOCKER_HOST
DOCKER := DOCKER_HOST=$${RUNNER_DOCKER_HOST:-$${DOCKER_HOST:-}} docker

test-lib:
	cd $(TESTS_DIR)/lib && $(MAKE) test

test-webui:
	$(MAKE) -C webui test

# Set the warnaserror flag to fail tests in CI in case of warning
# Note that dotnet test won't immediately fail on warning as errors, and will run all tests, possibly successfully, but still have exit status 1.
# To avoid misunderstandings, I also added a dotnet build with the flag before the tests run so that it immediately fails.
test: export DOTNET_FLAGS=--warnaserror
test: test-lib test-webui test-renovate-config

test-renovate-config:
	$(DOCKER) run --rm renovate/renovate renovate-config-validator < renovate.json

test-in-docker:
	# we mount the pojsonvolume as a docker volume, so that it can be shared with a container run inside this docker container.
	# See https://stackoverflow.com/questions/31381322/docker-in-docker-cannot-mount-volume
	image=$$(docker build -q --build-arg build_env=copy .) && \
		  docker run -v /var/run/docker.sock:/var/run/docker.sock -v pojsonvolume:/app/webui/locale/clientside/json --rm $${image} bash -c "./deployment/prepare_container_for_tests.sh && make test"

clean:
	dirs=( "lib" "tests/lib" "tests/webui/playwright" "tests/webui/lib" "tests/webui/IdentityTests"); \
	for dir in "$${dirs[@]}"; do \
		$(MAKE) -C "$${dir}" clean;  \
	done

setup-gitlab-runner:
	# Check the required token is passed by a environment variable
	echo $${GITLAB_RUNNER_TOKEN:?You need to provide GITLAB_RUNNER_TOKEN}
	# start the runner
	docker run -d --name gitlab-runner --restart always \
		-v $$PWD/private/gitlab-runner/config:/etc/gitlab-runner \
		-v /var/run/docker.sock:/var/run/docker.sock \
		gitlab/gitlab-runner:latest
	# register the runner
	docker run --rm -it \
		-v $$PWD/private/gitlab-runner/config:/etc/gitlab-runner \
		gitlab/gitlab-runner \
		register \
			--non-interactive \
			--executor $${GITLAB_RUNNER_EXECUTOR:-docker} \
			--docker-image $${GITLAB_RINNER_IMAGE:-docker} \
			--name $${GITLAB_RUNNER_NAME:-$$(hostname)} \
			--url $${GITLAB_RUNNER_SERVER:-https://gitlab.com/} \
			--registration-token $${GITLAB_RUNNER_TOKEN}
	# Set variable RUNNER_DOCKER_HOST so that the scripts it runs can detect it needs to update the DOCKER_HOST with this value
	# Thisis needed because the DOCKER_HOST value needs to be a different value on the shared gitlab runners than on dedicated runners
	# (dedicated runners use /var/run/docker.sock and shared runner use dind)
	docker run --rm -it \
		-v $$PWD/private/gitlab-runner/config:/etc/gitlab-runner \
		ubuntu \
		sed -i -e '/\[\[runners\]\]/a \ \ environment = \["RUNNER_DOCKER_HOST=unix:///var/run/docker.sock"\]' /etc/gitlab-runner/config.toml
	# configure the runner to mount the host's docker.sock to run docker commands
	docker run --rm -it \
		-v $$PWD/private/gitlab-runner/config:/etc/gitlab-runner \
		ubuntu \
		sed -i -e 's+volumes = \["/cache"\]+volumes = ["/var/run/docker.sock:/var/run/docker.sock","/cache"]+' /etc/gitlab-runner/config.toml
stop-gitlab-runner:
	docker stop gitlab-runner
start-gitlab-runner:
	docker start gitlab-runner
restart-gitlab-runner: stop-gitlab-runner start-gitlab-runner
clean-gitlab-runner:
	docker stop gitlab-runner || true
	docker rm gitlab-runner || true
purge-gitlab-runner: clean-gitlab-runner
	sudo rm -rf private/gitlab-runner

# Pass additional docker flags to this target in variable DOCKER_FLAGS
build-docker-prod:
	@docker build -q -f Dockerfile.prod --build-arg branch=$$(git rev-parse --abbrev-ref HEAD) $(DOCKER_FLAGS) .

# Set the DOCKER_FLAGS to tag the image as staging in the dependency build-docker-prod
push-staging-image: DOCKER_FLAGS=-t registry.gitlab.com/myowndb/myowndb/staging:latest
push-staging-image: build-docker-prod
	# This requires to have previously logged in to the registry with docker --config private/docker login
	docker --config private/docker push registry.gitlab.com/myowndb/myowndb/staging:latest

# Run the webui in a docker container
run-in-docker:
	# Generate image and call run with the imgae's id's prefix "sha256:" removed
	@image_id=$$($(MAKE) --no-print-directory build-docker-prod) && \
			 docker run -it -v $${PWD}/webui/conf:/app/conf -p8080:80 $${image_id#*:}
