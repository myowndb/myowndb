#!/bin/bash
# run database migrations on local postgres

script_dir=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

if [[ -f /ide/bin/gitpod-code ]]; then
    user=gitpod
    options="?sslmode=disable"

    createuser madb
    createuser --superuser postgres
    for db in myowndb myowndbtest; do
        createdb $db -O madb
        (unset PGHOSTADDR; migrate -verbose -path migrations -database "postgres://gitpod@localhost:5432/$db?sslmode=disable" up)
    done

else
    user=postgres
    options=""
    # all tables have owner madb
    PGPASSFILE=private/pgpass migrate -verbose -path migrations -database "postgres://${user}@localhost:5432/myowndbtest${options}" up
fi
