#!/bin/bash
set -eux

script_dir="$( cd -- "$( dirname -- "${BASH_SOURCE[0]:-$0}"; )" &> /dev/null && pwd 2> /dev/null; )";
repo_root="$script_dir/.."
service postgresql start
# ensure we are at the root of the repo
cd "$repo_root"
cd deployment
./prepare_tests.sh
cd ..
cd webui
# add source so the dotnet restore can access the prerelease
dotnet nuget add source -u rbauduin -p  "$WS5TOKEN" --store-password-in-clear-text https://nuget.pkg.github.com/dotnet-websharper/index.json
dotnet restore
cd ..
cd tests/webui/playwright
dotnet build
/root/.dotnet/tools/playwright install
cd ../../..
