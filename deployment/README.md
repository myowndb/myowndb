# MyOwnDB deployment

## Ansible
This directory holds all files needed to deploy MyOwnDB with ansible.
The cookbook is `main.yml` using basically 2 roles:
* for postgres: https://github.com/geerlingguy/ansible-role-postgresql
* for dotnet: https://github.com/ocha/ansible-role-dotnet-core

Inventory file `ansible_hosts.database` is used to deploy only the database part for running tests,
(the dotnet role is not used as we use an image from ms with dotnet core available).


## Scripts

Shell scripts in this directory are used to build test containers to run with Gitlab CI/CD.
They are run in this order to build the image:
* `setup_env.sh`: use ansible to install postgresql locally
* `migrate.sh`: apply migrations to local postgres instance
* `clean_env.sh`: remove packages that were needed to setup the test environment, but not needed to run tests

Before running the test:
* `prepare_tests.sh`: generate config for tests to use local database
