#!/bin/bash
# remove packages added to deploy myowndb with ansible
pip3 uninstall -y ansible
apt-get remove -y --purge python3 python3-pip python3-setuptools python3-apt aptitude
apt-get -y autoremove
