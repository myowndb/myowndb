#!/bin/bash

# download migration tool
curl -O -L "https://github.com/golang-migrate/migrate/releases/download/v4.16.2/migrate.linux-amd64.tar.gz"
if sha256sum -c <(echo "457c5ac80c85c2d7c06d3859706c9664e8ccd294e0469d6adddf6d7265237fcc migrate.linux-amd64.tar.gz") ; then
    tar xvf migrate.linux-amd64.tar.gz
    if ! sha256sum -c <(echo "9e139470f1247f99566fb3449f345e2c027fa03fa813275e80e311bd1691bec4  migrate") ; then
        >&1 echo "checksum of migrate didn't validate"
    fi
else
    >&1 echo "checksum of migrate archive didn't validate"
fi

mv migrate /usr/bin/migrate
