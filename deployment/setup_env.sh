#!/bin/bash -eux
# scripts that sets up myowndb locally (database part for running test with Gitlab CI/CD)

# https://www.nubera.eu/blog/building-docker-images-with-gitlab-ci/cd
# https://medium.com/faun/building-a-docker-image-with-gitlab-ci-and-net-core-8f59681a86c4

apt-get update
apt-get upgrade -y
apt-get install -y python3 python3-pip python3-setuptools make gettext wget
# setup database server with ansible

# do not upgrade system pip with pip
#pip3 install --upgrade pip
pip3 install ansible==2.9.27

mkdir private

cat >private/postgresql.yml <<EOF
postgresql_users:
  - name: myowndb
    password: myowndb
  - name: myowndbtest
    password: myowndbtest
  - name: postgres
    password: postgres
  - name: madb
    password: madb
postgresql_databases:
  - name: myowndbtest
    owner: myowndbtest
postgresql_hba_entries:
  - { type: local, database: myowndbtest, user: postgres, auth_method: peer }
  - { type: local, database: all, user: postgres, auth_method: md5 }
  - { type: local, database: myowndbtest, user: myowndbtest, auth_method: md5 }
  - { type: local, database: all, user: all, auth_method: peer }
  - { type: host, database: all, user: all, address: '127.0.0.1/32', auth_method: md5 }
  - { type: host, database: all, user: all, address: '::1/128', auth_method: md5 }
postgresql_daemon: postgresql
postgresql_python_library: python3-psycopg2
EOF

# pgpass file used for migration
cat >private/pgpass <<EOF
*:5432:*:postgres:postgres
*:5432:*:myowndbtest:myowndbtest
EOF
chmod 600 private/pgpass

apt-get install -y python3-apt aptitude sudo

# Install postgresql from their repo, giving access to latest version
apt-get update
apt-get install -y lsb-release wget gnupg;
sh -c 'echo "deb https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
apt-get update
DEBIAN_FRONTEND=noninteractive apt-get -y install postgresql-16

#ansible-galaxy install geerlingguy.postgresql
#mv roles/geerlingguy.postgresql ansible-role-postgresql
# We still call the playbook and use the ansible-role-postgresql to configure the server, even though we install
# postgresql ourselves. We just need to ensure that the variable postgresql-version in roles/ansible-role-postgresql/vars/Ubuntu-${version}.yml
# is set to the version we install above. ${version} here is the ubuntu version number, which is the version used by the playwright image we reference in Dockerfile
ansible-playbook --inventory ansible_hosts.database main.yml --limit "database"

./install_migrate.sh
