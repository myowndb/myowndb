--
-- Data for Name: account_types; Type: TABLE DATA; Schema: public; Owner: myowndbtest
--

INSERT INTO public.account_types VALUES (3, 'madb_account_type_association', false, false, 'madb_unlimited', '1', 99.9899999999999949, 51200, 10485760, 50);
INSERT INTO public.account_types VALUES (4, 'madb_account_type_enterprise', false, false, 'madb_unlimited', '1', 99.9899999999999949, 51200, 10485760, 50);
INSERT INTO public.account_types VALUES (5, 'madb_account_type_multinational', false, false, 'madb_unlimited', '1', 99.9899999999999949, 51200, 10485760, 50);
INSERT INTO public.account_types VALUES (1, 'madb_account_type_free', true, true, 'madb_unlimited', '1', 0, 51200, 10485760, 50);
INSERT INTO public.account_types VALUES (2, 'madb_account_type_personal', false, false, 'madb_unlimited', '3', 9.90000000000000036, 10485760, 8589934592, 200);


--
-- Data for Name: account_type_values; Type: TABLE DATA; Schema: public; Owner: myowndbtest
--

INSERT INTO public.account_type_values VALUES (1, 1, 'number_of_databases', '1');
INSERT INTO public.account_type_values VALUES (2, 5, 'number_of_databases', '500');


--
-- Data for Name: data_types; Type: TABLE DATA; Schema: public; Owner: myowndbtest
--

INSERT INTO public.data_types VALUES (1, 'madb_short_text', 'SimpleDetailValue');
INSERT INTO public.data_types VALUES (2, 'madb_long_text', 'LongTextDetailValue');
INSERT INTO public.data_types VALUES (3, 'madb_date', 'DateDetailValue');
INSERT INTO public.data_types VALUES (4, 'madb_integer', 'IntegerDetailValue');
INSERT INTO public.data_types VALUES (5, 'madb_choose_in_list', 'DdlDetailValue');
INSERT INTO public.data_types VALUES (6, 'madb_email', 'EmailDetailValue');
INSERT INTO public.data_types VALUES (7, 'madb_web_url', 'WebUrlDetailValue');
INSERT INTO public.data_types VALUES (8, 'madb_file_attachment', 'FileAttachment');


--
-- Data for Name: relation_side_types; Type: TABLE DATA; Schema: public; Owner: myowndbtest
--

INSERT INTO public.relation_side_types VALUES (1, 'one');
INSERT INTO public.relation_side_types VALUES (2, 'many');


--
-- Data for Name: user_types; Type: TABLE DATA; Schema: public; Owner: myowndbtest
--

INSERT INTO public.user_types VALUES (1, 'primary_user');
INSERT INTO public.user_types VALUES (2, 'normal_user');


--
-- Data for Name: detail_status; Type: TABLE DATA; Schema: public; Owner: myowndbtest
--

INSERT INTO public.detail_status VALUES (1, 'active');
INSERT INTO public.detail_status VALUES (2, 'inactive');

