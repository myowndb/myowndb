-- the record definition requires the created_at to be set. Do it with a trigger
ALTER TABLE users ALTER COLUMN created_at SET DEFAULT now();
