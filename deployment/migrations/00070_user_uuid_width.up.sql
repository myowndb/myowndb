-- increase width to handle uuid including '-' as generateb by dotnet
alter table users alter column uuid TYPE char(36);
