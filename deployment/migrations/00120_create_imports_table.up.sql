CREATE TABLE imports (
    id SERIAL,
    entity_id integer REFERENCES entities(id) ON DELETE CASCADE,
    step integer,
    user_email text,
    import_info json
    )
