
UPDATE entities2details set maximum_number_of_values = 1 where maximum_number_of_values is null;
ALTER table entities2details  ALTER COLUMN maximum_number_of_values set not null;
