-- Convert the source_filter column to json type.
-- We alter the column type to jsonb, using a transformation to get the value of the new type.
-- These are ad-hoc transformations, targetting the values found in that table.
-- Replacements are executed from right to left for calls, and from top to botton for arguments
-- Once we have the right string value, we convert to jsonb with `::jsonb`
alter table notification_subscriptions
  alter column source_filter type jsonb using
    (REPLACE(REGEXP_REPLACE(REGEXP_REPLACE(REPLACE(REPLACE(source_filter,
        -- first remove preamble
        '---',''),
        -- then remove leading : of ruby symbols
        ':',''),
        -- remove new lines
        E'[\\n\\r]+','','g'),
        -- add curly brace to open object
        '^ ','{"'),
        -- a space separates key from value, replace it by a ':', and close the key's quoted string at the same time
        ' ','":')
        -- add the closing curly brace to mark en of object
        || '}')::jsonb;

-- Change name of event to match our F# type
update notification_subscriptions set event='AfterCreate' where event = 'after_create';
