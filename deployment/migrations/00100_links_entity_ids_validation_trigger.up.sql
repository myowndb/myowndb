-- For each side (child_id,parent_id), check the relation to
-- see the side type (one or many)
-- if one, check the child_id/parent_id inserted is not already present
CREATE OR REPLACE FUNCTION links_entity_ids_validation()
RETURNS TRIGGER AS
$$
DECLARE
child_entity_id integer;
parent_entity_id integer;
count integer;
msg text;
has_error bool;
BEGIN
select '' into msg;
select false into has_error;
select relations.child_id into child_entity_id from relations where id = NEW.relation_id;
select parent_id into parent_entity_id from relations where id = NEW.relation_id;

-- validate parent instance's entity_id
select count(*) into count from entities e join instances i on (i.entity_id=parent_entity_id and i.id=NEW.parent_id);
IF count=0
THEN
    select 'parent entity_id is not correct according to relation;' into msg;
    select true into has_error;
END IF;

-- validate child instance's entity_id
select count(*) into count from instances i where (i.entity_id=child_entity_id and i.id=NEW.child_id);
IF count=0
THEN
    select msg||'child entity_id is not correct according to relation;' into msg;
    select true into has_error;
END IF;

IF has_error
THEN
    Raise Exception '%', msg;
END IF;
RETURN NEW;
END;
$$ LANGUAGE PLpgSQL;


CREATE TRIGGER links_entity_ids_validation BEFORE INSERT OR UPDATE ON links
FOR EACH ROW EXECUTE PROCEDURE links_entity_ids_validation();
