--
-- PostgreSQL database dump
--

-- Dumped from database version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.6 (Ubuntu 10.6-0ubuntu0.18.04.1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
--SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- Name: tablefunc_crosstab_2; Type: TYPE; Schema: public; Owner: madb
--

DO $$ BEGIN
CREATE TYPE public.tablefunc_crosstab_2 AS (
	row_name text,
	category_1 text,
	category_2 text
);
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;


ALTER TYPE public.tablefunc_crosstab_2 OWNER TO madb;

--
-- Name: tablefunc_crosstab_3; Type: TYPE; Schema: public; Owner: madb
--

DO $$ BEGIN
CREATE TYPE public.tablefunc_crosstab_3 AS (
	row_name text,
	category_1 text,
	category_2 text,
	category_3 text
);
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;


ALTER TYPE public.tablefunc_crosstab_3 OWNER TO madb;

--
-- Name: tablefunc_crosstab_4; Type: TYPE; Schema: public; Owner: madb
--

DO $$ BEGIN
CREATE TYPE public.tablefunc_crosstab_4 AS (
	row_name text,
	category_1 text,
	category_2 text,
	category_3 text,
	category_4 text
);
EXCEPTION
    WHEN duplicate_object THEN null;
END $$;


ALTER TYPE public.tablefunc_crosstab_4 OWNER TO madb;

--
-- Name: connectby(text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.connectby(text, text, text, text, integer) RETURNS SETOF record
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'connectby_text';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.connectby(text, text, text, text, integer) OWNER TO postgres;

--
-- Name: connectby(text, text, text, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.connectby(text, text, text, text, integer, text) RETURNS SETOF record
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'connectby_text';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.connectby(text, text, text, text, integer, text) OWNER TO postgres;

--
-- Name: connectby(text, text, text, text, text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.connectby(text, text, text, text, text, integer) RETURNS SETOF record
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'connectby_text_serial';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.connectby(text, text, text, text, text, integer) OWNER TO postgres;

--
-- Name: connectby(text, text, text, text, text, integer, text); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.connectby(text, text, text, text, text, integer, text) RETURNS SETOF record
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'connectby_text_serial';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.connectby(text, text, text, text, text, integer, text) OWNER TO postgres;

--
-- Name: crosstab(text); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.crosstab(text) RETURNS SETOF record
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'crosstab';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.crosstab(text) OWNER TO postgres;

--
-- Name: crosstab(text, integer); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.crosstab(text, integer) RETURNS SETOF record
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'crosstab';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.crosstab(text, integer) OWNER TO postgres;

--
-- Name: crosstab(text, text); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.crosstab(text, text) RETURNS SETOF record
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'crosstab_hash';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.crosstab(text, text) OWNER TO postgres;

--
-- Name: crosstab2(text); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.crosstab2(text) RETURNS SETOF public.tablefunc_crosstab_2
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'crosstab';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.crosstab2(text) OWNER TO postgres;

--
-- Name: crosstab3(text); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.crosstab3(text) RETURNS SETOF public.tablefunc_crosstab_3
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'crosstab';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.crosstab3(text) OWNER TO postgres;

--
-- Name: crosstab4(text); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.crosstab4(text) RETURNS SETOF public.tablefunc_crosstab_4
    LANGUAGE c STABLE STRICT
    AS '$libdir/tablefunc', 'crosstab';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.crosstab4(text) OWNER TO postgres;

--
-- Name: normal_rand(integer, double precision, double precision); Type: FUNCTION; Schema: public; Owner: postgres
--

DO $$ BEGIN
CREATE FUNCTION public.normal_rand(integer, double precision, double precision) RETURNS SETOF double precision
    LANGUAGE c STRICT
    AS '$libdir/tablefunc', 'normal_rand';
EXCEPTION
    WHEN duplicate_function THEN null;
END $$;


ALTER FUNCTION public.normal_rand(integer, double precision, double precision) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: account_type_values; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.account_type_values (
    id integer NOT NULL,
    account_type_id integer,
    detail text,
    value text
);


ALTER TABLE public.account_type_values OWNER TO madb;

--
-- Name: account_type_values_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.account_type_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_type_values_id_seq OWNER TO madb;

--
-- Name: account_type_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.account_type_values_id_seq OWNED BY public.account_type_values.id;


--
-- Name: account_types; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.account_types (
    id integer NOT NULL,
    name text,
    active boolean DEFAULT false,
    free boolean DEFAULT false,
    number_of_users text DEFAULT 'madb_unlimited'::text,
    number_of_databases text DEFAULT '1'::text,
    monthly_fee double precision DEFAULT 99.99,
    maximum_file_size integer DEFAULT 51200,
    maximum_monthly_file_transfer bigint DEFAULT 10485760,
    maximum_attachment_number integer DEFAULT 50
);


ALTER TABLE public.account_types OWNER TO madb;

--
-- Name: account_types_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.account_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.account_types_id_seq OWNER TO madb;

--
-- Name: account_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.account_types_id_seq OWNED BY public.account_types.id;


--
-- Name: accounts; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.accounts (
    id integer NOT NULL,
    account_type_id integer,
    name text,
    street text,
    zip_code text,
    city text,
    country text,
    status text DEFAULT 'inactive'::text,
    end_date date DEFAULT now(),
    subscription_id text,
    subscription_gateway text,
    vat_number text,
    attachment_count integer DEFAULT 0,
    lock_version integer DEFAULT 0,
    created_at timestamp without time zone,
    last_login_at timestamp without time zone
);


ALTER TABLE public.accounts OWNER TO madb;

--
-- Name: accounts_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.accounts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.accounts_id_seq OWNER TO madb;

--
-- Name: accounts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.accounts_id_seq OWNED BY public.accounts.id;


--
-- Name: data_types; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.data_types (
    id integer NOT NULL,
    name text,
    class_name text
);


ALTER TABLE public.data_types OWNER TO madb;

--
-- Name: data_types_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.data_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.data_types_id_seq OWNER TO madb;

--
-- Name: data_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.data_types_id_seq OWNED BY public.data_types.id;


--
-- Name: databases; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.databases (
    id integer NOT NULL,
    account_id integer NOT NULL,
    name text,
    lock_version integer DEFAULT 0
);


ALTER TABLE public.databases OWNER TO madb;

--
-- Name: databases_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.databases_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.databases_id_seq OWNER TO madb;

--
-- Name: databases_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.databases_id_seq OWNED BY public.databases.id;


--
-- Name: date_detail_values; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.date_detail_values (
    id integer NOT NULL,
    detail_id integer,
    instance_id integer,
    value timestamp without time zone,
    lock_version integer DEFAULT 0,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.date_detail_values OWNER TO madb;

--
-- Name: date_detail_values_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.date_detail_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.date_detail_values_id_seq OWNER TO madb;

--
-- Name: date_detail_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.date_detail_values_id_seq OWNED BY public.date_detail_values.id;


--
-- Name: ddl_detail_values; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.ddl_detail_values (
    id integer NOT NULL,
    detail_id integer,
    instance_id integer,
    detail_value_proposition_id integer,
    lock_version integer DEFAULT 0,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.ddl_detail_values OWNER TO madb;

--
-- Name: ddl_detail_values_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.ddl_detail_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.ddl_detail_values_id_seq OWNER TO madb;

--
-- Name: ddl_detail_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.ddl_detail_values_id_seq OWNED BY public.ddl_detail_values.id;


--
-- Name: delayed_jobs; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.delayed_jobs (
    id integer NOT NULL,
    priority integer DEFAULT 0,
    attempts integer DEFAULT 0,
    handler text,
    last_error character varying(255),
    run_at timestamp without time zone,
    locked_at timestamp without time zone,
    failed_at timestamp without time zone,
    locked_by character varying(255),
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.delayed_jobs OWNER TO madb;

--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.delayed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.delayed_jobs_id_seq OWNER TO madb;

--
-- Name: delayed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.delayed_jobs_id_seq OWNED BY public.delayed_jobs.id;


--
-- Name: detail_status; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.detail_status (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.detail_status OWNER TO madb;

--
-- Name: detail_status_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.detail_status_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detail_status_id_seq OWNER TO madb;

--
-- Name: detail_status_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.detail_status_id_seq OWNED BY public.detail_status.id;


--
-- Name: detail_value_propositions; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.detail_value_propositions (
    id integer NOT NULL,
    detail_id integer,
    value text,
    lock_version integer DEFAULT 0
);


ALTER TABLE public.detail_value_propositions OWNER TO madb;

--
-- Name: detail_value_propositions_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.detail_value_propositions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detail_value_propositions_id_seq OWNER TO madb;

--
-- Name: detail_value_propositions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.detail_value_propositions_id_seq OWNED BY public.detail_value_propositions.id;


--
-- Name: detail_values; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.detail_values (
    id integer NOT NULL,
    detail_id integer,
    instance_id integer,
    value text,
    type text,
    lock_version integer DEFAULT 0,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.detail_values OWNER TO madb;

--
-- Name: detail_values_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.detail_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.detail_values_id_seq OWNER TO madb;

--
-- Name: detail_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.detail_values_id_seq OWNED BY public.detail_values.id;


--
-- Name: details; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.details (
    id integer NOT NULL,
    name text,
    data_type_id integer,
    status_id integer,
    database_id integer,
    lock_version integer DEFAULT 0
);


ALTER TABLE public.details OWNER TO madb;

--
-- Name: details_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.details_id_seq OWNER TO madb;

--
-- Name: details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.details_id_seq OWNED BY public.details.id;


--
-- Name: entities; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.entities (
    id integer NOT NULL,
    database_id integer NOT NULL,
    name text,
    has_public_form boolean DEFAULT false,
    lock_version integer DEFAULT 0,
    has_public_data boolean DEFAULT false,
    public_to_all boolean DEFAULT false
);


ALTER TABLE public.entities OWNER TO madb;

--
-- Name: entities2details; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.entities2details (
    id integer NOT NULL,
    entity_id integer,
    detail_id integer,
    status_id integer,
    displayed_in_list_view boolean DEFAULT true,
    maximum_number_of_values integer,
    display_order integer DEFAULT 100
);


ALTER TABLE public.entities2details OWNER TO madb;

--
-- Name: entities2details_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.entities2details_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entities2details_id_seq OWNER TO madb;

--
-- Name: entities2details_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.entities2details_id_seq OWNED BY public.entities2details.id;


--
-- Name: entities_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.entities_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.entities_id_seq OWNER TO madb;

--
-- Name: entities_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.entities_id_seq OWNED BY public.entities.id;


--
-- Name: instances; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.instances (
    id integer NOT NULL,
    entity_id integer,
    created_at timestamp with time zone,
    lock_version integer DEFAULT 0,
    updated_at timestamp without time zone
);


ALTER TABLE public.instances OWNER TO madb;

--
-- Name: instances_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.instances_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.instances_id_seq OWNER TO madb;

--
-- Name: instances_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.instances_id_seq OWNED BY public.instances.id;


--
-- Name: integer_detail_values; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.integer_detail_values (
    id integer NOT NULL,
    detail_id integer,
    instance_id integer,
    value bigint,
    lock_version integer DEFAULT 0,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.integer_detail_values OWNER TO madb;

--
-- Name: integer_detail_values_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.integer_detail_values_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.integer_detail_values_id_seq OWNER TO madb;

--
-- Name: integer_detail_values_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.integer_detail_values_id_seq OWNED BY public.integer_detail_values.id;


--
--
-- Name: links; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.links (
    id integer NOT NULL,
    parent_id integer,
    child_id integer,
    relation_id integer
);


ALTER TABLE public.links OWNER TO madb;

--
-- Name: links_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.links_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.links_id_seq OWNER TO madb;

--
-- Name: links_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.links_id_seq OWNED BY public.links.id;


--
-- Name: notification_subscriptions; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.notification_subscriptions (
    id integer NOT NULL,
    event text,
    source_type text,
    source_filter text,
    destination_type text,
    destination text,
    protocol text
);


ALTER TABLE public.notification_subscriptions OWNER TO madb;

--
-- Name: notification_subscriptions_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.notification_subscriptions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.notification_subscriptions_id_seq OWNER TO madb;

--
-- Name: notification_subscriptions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.notification_subscriptions_id_seq OWNED BY public.notification_subscriptions.id;


--
-- Name: paypal_communications; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.paypal_communications (
    id integer NOT NULL,
    t timestamp without time zone DEFAULT now(),
    account_id integer,
    txn_type text,
    communication_type text,
    direction text,
    content text
);


ALTER TABLE public.paypal_communications OWNER TO postgres;

--
-- Name: paypal_communications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.paypal_communications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.paypal_communications_id_seq OWNER TO postgres;

--
-- Name: paypal_communications_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.paypal_communications_id_seq OWNED BY public.paypal_communications.id;


--
-- Name: plugin_schema_info; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.plugin_schema_info (
    plugin_name character varying(255),
    version integer
);


ALTER TABLE public.plugin_schema_info OWNER TO madb;

--
-- Name: preferences; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.preferences (
    id integer NOT NULL,
    user_id integer,
    display_help boolean
);


ALTER TABLE public.preferences OWNER TO madb;

--
-- Name: preferences_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.preferences_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.preferences_id_seq OWNER TO madb;

--
-- Name: preferences_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.preferences_id_seq OWNED BY public.preferences.id;


--
-- Name: relation_side_types; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.relation_side_types (
    id integer NOT NULL,
    name text
);


ALTER TABLE public.relation_side_types OWNER TO madb;

--
-- Name: relation_side_types_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.relation_side_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relation_side_types_id_seq OWNER TO madb;

--
-- Name: relation_side_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.relation_side_types_id_seq OWNED BY public.relation_side_types.id;


--
-- Name: relations; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.relations (
    id integer NOT NULL,
    parent_id integer NOT NULL,
    child_id integer NOT NULL,
    parent_side_type_id integer NOT NULL,
    child_side_type_id integer NOT NULL,
    from_parent_to_child_name text NOT NULL,
    from_child_to_parent_name text,
    lock_version integer DEFAULT 0
);


ALTER TABLE public.relations OWNER TO madb;

--
-- Name: relations_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.relations_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.relations_id_seq OWNER TO madb;

--
-- Name: relations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.relations_id_seq OWNED BY public.relations.id;


--
-- Name: sessions; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.sessions (
    id integer NOT NULL,
    session_id character varying(255) NOT NULL,
    data text,
    created_at timestamp without time zone,
    updated_at timestamp without time zone
);


ALTER TABLE public.sessions OWNER TO madb;

--
-- Name: sessions_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.sessions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sessions_id_seq OWNER TO madb;

--
-- Name: sessions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.sessions_id_seq OWNED BY public.sessions.id;


--
-- Name: transfers; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.transfers (
    id integer NOT NULL,
    created_at timestamp without time zone,
    account_id integer,
    user_id integer,
    detail_value_id integer,
    instance_id integer,
    entity_id integer,
    size integer NOT NULL,
    file text,
    direction text
);


ALTER TABLE public.transfers OWNER TO madb;

--
-- Name: transfers_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.transfers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.transfers_id_seq OWNER TO madb;

--
-- Name: transfers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.transfers_id_seq OWNED BY public.transfers.id;


--
-- Name: user_types; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.user_types (
    id integer NOT NULL,
    name text NOT NULL
);


ALTER TABLE public.user_types OWNER TO madb;

--
-- Name: user_types_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.user_types_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_types_id_seq OWNER TO madb;

--
-- Name: user_types_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.user_types_id_seq OWNED BY public.user_types.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: madb
--

CREATE TABLE public.users (
    id integer NOT NULL,
    account_id integer NOT NULL,
    user_type_id integer DEFAULT 2,
    login character varying(80),
    password character varying,
    email character varying(40),
    firstname character varying(80),
    lastname character varying(80),
    uuid character(32),
    salt character(32),
    verified integer DEFAULT 0,
    created_at timestamp without time zone,
    updated_at timestamp without time zone,
    logged_in_at timestamp without time zone,
    api_key text
);


ALTER TABLE public.users OWNER TO madb;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: madb
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO madb;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: madb
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: account_type_values id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.account_type_values ALTER COLUMN id SET DEFAULT nextval('public.account_type_values_id_seq'::regclass);


--
-- Name: account_types id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.account_types ALTER COLUMN id SET DEFAULT nextval('public.account_types_id_seq'::regclass);


--
-- Name: accounts id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.accounts ALTER COLUMN id SET DEFAULT nextval('public.accounts_id_seq'::regclass);


--
-- Name: data_types id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.data_types ALTER COLUMN id SET DEFAULT nextval('public.data_types_id_seq'::regclass);


--
-- Name: databases id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.databases ALTER COLUMN id SET DEFAULT nextval('public.databases_id_seq'::regclass);


--
-- Name: date_detail_values id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.date_detail_values ALTER COLUMN id SET DEFAULT nextval('public.date_detail_values_id_seq'::regclass);


--
-- Name: ddl_detail_values id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.ddl_detail_values ALTER COLUMN id SET DEFAULT nextval('public.ddl_detail_values_id_seq'::regclass);


--
-- Name: delayed_jobs id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.delayed_jobs ALTER COLUMN id SET DEFAULT nextval('public.delayed_jobs_id_seq'::regclass);


--
-- Name: detail_status id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_status ALTER COLUMN id SET DEFAULT nextval('public.detail_status_id_seq'::regclass);


--
-- Name: detail_value_propositions id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_value_propositions ALTER COLUMN id SET DEFAULT nextval('public.detail_value_propositions_id_seq'::regclass);


--
-- Name: detail_values id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_values ALTER COLUMN id SET DEFAULT nextval('public.detail_values_id_seq'::regclass);


--
-- Name: details id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.details ALTER COLUMN id SET DEFAULT nextval('public.details_id_seq'::regclass);


--
-- Name: entities id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities ALTER COLUMN id SET DEFAULT nextval('public.entities_id_seq'::regclass);


--
-- Name: entities2details id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities2details ALTER COLUMN id SET DEFAULT nextval('public.entities2details_id_seq'::regclass);


--
-- Name: instances id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.instances ALTER COLUMN id SET DEFAULT nextval('public.instances_id_seq'::regclass);


--
-- Name: integer_detail_values id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.integer_detail_values ALTER COLUMN id SET DEFAULT nextval('public.integer_detail_values_id_seq'::regclass);


--
-- Name: links id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.links ALTER COLUMN id SET DEFAULT nextval('public.links_id_seq'::regclass);


--
-- Name: notification_subscriptions id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.notification_subscriptions ALTER COLUMN id SET DEFAULT nextval('public.notification_subscriptions_id_seq'::regclass);


--
-- Name: paypal_communications id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.paypal_communications ALTER COLUMN id SET DEFAULT nextval('public.paypal_communications_id_seq'::regclass);


--
-- Name: preferences id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.preferences ALTER COLUMN id SET DEFAULT nextval('public.preferences_id_seq'::regclass);


--
-- Name: relation_side_types id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relation_side_types ALTER COLUMN id SET DEFAULT nextval('public.relation_side_types_id_seq'::regclass);


--
-- Name: relations id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations ALTER COLUMN id SET DEFAULT nextval('public.relations_id_seq'::regclass);


--
-- Name: sessions id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.sessions ALTER COLUMN id SET DEFAULT nextval('public.sessions_id_seq'::regclass);


--
-- Name: transfers id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.transfers ALTER COLUMN id SET DEFAULT nextval('public.transfers_id_seq'::regclass);


--
-- Name: user_types id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.user_types ALTER COLUMN id SET DEFAULT nextval('public.user_types_id_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Name: account_type_values account_type_values_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.account_type_values
    ADD CONSTRAINT account_type_values_pkey PRIMARY KEY (id);


--
-- Name: account_types account_types_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.account_types
    ADD CONSTRAINT account_types_pkey PRIMARY KEY (id);


--
-- Name: accounts accounts_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_pkey PRIMARY KEY (id);


--
-- Name: data_types data_types_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.data_types
    ADD CONSTRAINT data_types_pkey PRIMARY KEY (id);


--
-- Name: databases databases_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.databases
    ADD CONSTRAINT databases_pkey PRIMARY KEY (id);


--
-- Name: date_detail_values date_detail_values_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.date_detail_values
    ADD CONSTRAINT date_detail_values_pkey PRIMARY KEY (id);


--
-- Name: ddl_detail_values ddl_detail_values_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.ddl_detail_values
    ADD CONSTRAINT ddl_detail_values_pkey PRIMARY KEY (id);


--
-- Name: delayed_jobs delayed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.delayed_jobs
    ADD CONSTRAINT delayed_jobs_pkey PRIMARY KEY (id);


--
-- Name: detail_status detail_status_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_status
    ADD CONSTRAINT detail_status_pkey PRIMARY KEY (id);


--
-- Name: detail_value_propositions detail_value_propositions_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_value_propositions
    ADD CONSTRAINT detail_value_propositions_pkey PRIMARY KEY (id);


--
-- Name: detail_values detail_values_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_values
    ADD CONSTRAINT detail_values_pkey PRIMARY KEY (id);


--
-- Name: details details_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.details
    ADD CONSTRAINT details_pkey PRIMARY KEY (id);


--
-- Name: entities2details entities2details_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities2details
    ADD CONSTRAINT entities2details_pkey PRIMARY KEY (id);


--
-- Name: entities entities_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_pkey PRIMARY KEY (id);


--
-- Name: instances instances_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.instances
    ADD CONSTRAINT instances_pkey PRIMARY KEY (id);


--
-- Name: integer_detail_values integer_detail_values_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.integer_detail_values
    ADD CONSTRAINT integer_detail_values_pkey PRIMARY KEY (id);


--
-- Name: links links_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_pkey PRIMARY KEY (id);


--
-- Name: notification_subscriptions notification_subscriptions_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.notification_subscriptions
    ADD CONSTRAINT notification_subscriptions_pkey PRIMARY KEY (id);


--
-- Name: paypal_communications paypal_communications_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.paypal_communications
    ADD CONSTRAINT paypal_communications_pkey PRIMARY KEY (id);


--
-- Name: preferences preferences_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.preferences
    ADD CONSTRAINT preferences_pkey PRIMARY KEY (id);


--
-- Name: relation_side_types relation_side_types_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relation_side_types
    ADD CONSTRAINT relation_side_types_pkey PRIMARY KEY (id);


--
-- Name: relations relations_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT relations_pkey PRIMARY KEY (id);


--
-- Name: sessions sessions_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.sessions
    ADD CONSTRAINT sessions_pkey PRIMARY KEY (id);


--
-- Name: transfers transfers_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT transfers_pkey PRIMARY KEY (id);


--
-- Name: links u_parent_child_relation; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT u_parent_child_relation UNIQUE (parent_id, child_id, relation_id);


--
-- Name: user_types user_types_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.user_types
    ADD CONSTRAINT user_types_pkey PRIMARY KEY (id);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: entities2details__detail_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX entities2details__detail_id ON public.entities2details USING btree (detail_id);


--
-- Name: entities2details__entity_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX entities2details__entity_id ON public.entities2details USING btree (entity_id);


--
-- Name: i_date_detail_value__detail_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_date_detail_value__detail_id ON public.date_detail_values USING btree (detail_id);


--
-- Name: i_date_detail_value__instance_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_date_detail_value__instance_id ON public.date_detail_values USING btree (instance_id);


--
-- Name: i_ddl_detail_value__detail_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_ddl_detail_value__detail_id ON public.ddl_detail_values USING btree (detail_id);


--
-- Name: i_ddl_detail_value__instance_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_ddl_detail_value__instance_id ON public.ddl_detail_values USING btree (instance_id);


--
-- Name: i_detail_value__detail_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_detail_value__detail_id ON public.detail_values USING btree (detail_id);


--
-- Name: i_detail_value__instance_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_detail_value__instance_id ON public.detail_values USING btree (instance_id);


--
-- Name: i_entities2details__detail_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_entities2details__detail_id ON public.entities2details USING btree (detail_id);


--
-- Name: i_entities2details__entity_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_entities2details__entity_id ON public.entities2details USING btree (entity_id);


--
-- Name: i_instances__entity_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_instances__entity_id ON public.instances USING btree (entity_id);


--
-- Name: i_links__child_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_links__child_id ON public.links USING btree (child_id);


--
-- Name: i_links__parent_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_links__parent_id ON public.links USING btree (parent_id);


--
-- Name: i_relations__child_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_relations__child_id ON public.relations USING btree (child_id);


--
-- Name: i_relations__parent_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX i_relations__parent_id ON public.relations USING btree (parent_id);


--
-- Name: index_sessions_on_session_id; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX index_sessions_on_session_id ON public.sessions USING btree (session_id);


--
-- Name: index_sessions_on_updated_at; Type: INDEX; Schema: public; Owner: madb
--

CREATE INDEX index_sessions_on_updated_at ON public.sessions USING btree (updated_at);


--
-- Name: account_type_values account_type_values_account_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.account_type_values
    ADD CONSTRAINT account_type_values_account_type_id_fkey FOREIGN KEY (account_type_id) REFERENCES public.account_types(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: accounts accounts_account_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT accounts_account_type_id_fkey FOREIGN KEY (account_type_id) REFERENCES public.account_types(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: databases databases_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.databases
    ADD CONSTRAINT databases_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.accounts(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: date_detail_values date_detail_values_detail_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.date_detail_values
    ADD CONSTRAINT date_detail_values_detail_id_fkey FOREIGN KEY (detail_id) REFERENCES public.details(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: date_detail_values date_detail_values_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.date_detail_values
    ADD CONSTRAINT date_detail_values_instance_id_fkey FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: ddl_detail_values ddl_detail_values_detail_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.ddl_detail_values
    ADD CONSTRAINT ddl_detail_values_detail_id_fkey FOREIGN KEY (detail_id) REFERENCES public.details(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: ddl_detail_values ddl_detail_values_detail_value_proposition_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.ddl_detail_values
    ADD CONSTRAINT ddl_detail_values_detail_value_proposition_id_fkey FOREIGN KEY (detail_value_proposition_id) REFERENCES public.detail_value_propositions(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: ddl_detail_values ddl_detail_values_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.ddl_detail_values
    ADD CONSTRAINT ddl_detail_values_instance_id_fkey FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: detail_value_propositions detail_value_propositions_detail_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_value_propositions
    ADD CONSTRAINT detail_value_propositions_detail_id_fkey FOREIGN KEY (detail_id) REFERENCES public.details(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: detail_values detail_values_detail_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_values
    ADD CONSTRAINT detail_values_detail_id_fkey FOREIGN KEY (detail_id) REFERENCES public.details(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: detail_values detail_values_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_values
    ADD CONSTRAINT detail_values_instance_id_fkey FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: details details_data_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.details
    ADD CONSTRAINT details_data_type_id_fkey FOREIGN KEY (data_type_id) REFERENCES public.data_types(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: details details_database_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.details
    ADD CONSTRAINT details_database_id_fkey FOREIGN KEY (database_id) REFERENCES public.databases(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: details details_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.details
    ADD CONSTRAINT details_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.detail_status(id) ON UPDATE SET DEFAULT ON DELETE SET DEFAULT;


--
-- Name: entities2details entities2details_detail_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities2details
    ADD CONSTRAINT entities2details_detail_id_fkey FOREIGN KEY (detail_id) REFERENCES public.details(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: entities2details entities2details_entity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities2details
    ADD CONSTRAINT entities2details_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: entities2details entities2details_status_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities2details
    ADD CONSTRAINT entities2details_status_id_fkey FOREIGN KEY (status_id) REFERENCES public.detail_status(id) ON UPDATE SET DEFAULT ON DELETE SET DEFAULT;


--
-- Name: entities entities_database_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities
    ADD CONSTRAINT entities_database_id_fkey FOREIGN KEY (database_id) REFERENCES public.databases(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: accounts fk_accounts_account_type_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.accounts
    ADD CONSTRAINT fk_accounts_account_type_id FOREIGN KEY (account_type_id) REFERENCES public.account_types(id) ON DELETE SET NULL;


--
-- Name: databases fk_databases_account_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.databases
    ADD CONSTRAINT fk_databases_account_id FOREIGN KEY (account_id) REFERENCES public.accounts(id) ON DELETE CASCADE;


--
-- Name: date_detail_values fk_date_detail_values__instance_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.date_detail_values
    ADD CONSTRAINT fk_date_detail_values__instance_id FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON DELETE CASCADE;


--
-- Name: ddl_detail_values fk_ddl_detail_values__instance_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.ddl_detail_values
    ADD CONSTRAINT fk_ddl_detail_values__instance_id FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON DELETE CASCADE;


--
-- Name: ddl_detail_values fk_ddl_detail_values__proposition_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.ddl_detail_values
    ADD CONSTRAINT fk_ddl_detail_values__proposition_id FOREIGN KEY (detail_value_proposition_id) REFERENCES public.detail_value_propositions(id) ON DELETE CASCADE;


--
-- Name: detail_values fk_detail_values__detail_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_values
    ADD CONSTRAINT fk_detail_values__detail_id FOREIGN KEY (detail_id) REFERENCES public.details(id) ON DELETE CASCADE;


--
-- Name: detail_values fk_detail_values__instance_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.detail_values
    ADD CONSTRAINT fk_detail_values__instance_id FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON DELETE CASCADE;


--
-- Name: details fk_details_data_type_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.details
    ADD CONSTRAINT fk_details_data_type_id FOREIGN KEY (data_type_id) REFERENCES public.data_types(id) ON DELETE CASCADE;


--
-- Name: details fk_details_database_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.details
    ADD CONSTRAINT fk_details_database_id FOREIGN KEY (database_id) REFERENCES public.databases(id) ON DELETE CASCADE;


--
-- Name: details fk_details_status_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.details
    ADD CONSTRAINT fk_details_status_id FOREIGN KEY (status_id) REFERENCES public.detail_status(id) ON DELETE CASCADE;


--
-- Name: entities2details fk_entities2details_detail_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities2details
    ADD CONSTRAINT fk_entities2details_detail_id FOREIGN KEY (detail_id) REFERENCES public.details(id) ON DELETE CASCADE;


--
-- Name: entities2details fk_entities2details_entity_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities2details
    ADD CONSTRAINT fk_entities2details_entity_id FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: entities fk_entities_database_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.entities
    ADD CONSTRAINT fk_entities_database_id FOREIGN KEY (database_id) REFERENCES public.databases(id) ON DELETE CASCADE;


--
-- Name: instances fk_entity_type; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.instances
    ADD CONSTRAINT fk_entity_type FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: links fk_from_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT fk_from_id FOREIGN KEY (parent_id) REFERENCES public.instances(id) ON DELETE CASCADE;


--
-- Name: relations fk_from_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT fk_from_id FOREIGN KEY (parent_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: integer_detail_values fk_integer_detail_values__instance_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.integer_detail_values
    ADD CONSTRAINT fk_integer_detail_values__instance_id FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON DELETE CASCADE;


--
-- Name: links fk_relation_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT fk_relation_id FOREIGN KEY (relation_id) REFERENCES public.relations(id) ON DELETE CASCADE;


--
-- Name: relations fk_relations__child_side_type_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT fk_relations__child_side_type_id FOREIGN KEY (child_side_type_id) REFERENCES public.relation_side_types(id) ON DELETE CASCADE;


--
-- Name: relations fk_relations__parent_side_type_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT fk_relations__parent_side_type_id FOREIGN KEY (parent_side_type_id) REFERENCES public.relation_side_types(id) ON DELETE CASCADE;


--
-- Name: links fk_to_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT fk_to_id FOREIGN KEY (child_id) REFERENCES public.instances(id) ON DELETE CASCADE;


--
-- Name: relations fk_to_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT fk_to_id FOREIGN KEY (child_id) REFERENCES public.entities(id) ON DELETE CASCADE;


--
-- Name: transfers fk_transfers_account_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT fk_transfers_account_id FOREIGN KEY (account_id) REFERENCES public.accounts(id) ON DELETE CASCADE;


--
-- Name: transfers fk_transfers_detail_value_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT fk_transfers_detail_value_id FOREIGN KEY (detail_value_id) REFERENCES public.detail_values(id) ON DELETE SET NULL;


--
-- Name: transfers fk_transfers_instance_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT fk_transfers_instance_id FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON DELETE SET NULL;


--
-- Name: transfers fk_transfers_user_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.transfers
    ADD CONSTRAINT fk_transfers_user_id FOREIGN KEY (user_id) REFERENCES public.users(id) ON DELETE CASCADE;


--
-- Name: users fk_users_account_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_users_account_id FOREIGN KEY (account_id) REFERENCES public.accounts(id) ON DELETE CASCADE;


--
-- Name: users fk_users_user_type_id; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT fk_users_user_type_id FOREIGN KEY (user_type_id) REFERENCES public.user_types(id) ON DELETE CASCADE;


--
-- Name: instances instances_entity_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.instances
    ADD CONSTRAINT instances_entity_id_fkey FOREIGN KEY (entity_id) REFERENCES public.entities(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: integer_detail_values integer_detail_values_detail_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.integer_detail_values
    ADD CONSTRAINT integer_detail_values_detail_id_fkey FOREIGN KEY (detail_id) REFERENCES public.details(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: integer_detail_values integer_detail_values_instance_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.integer_detail_values
    ADD CONSTRAINT integer_detail_values_instance_id_fkey FOREIGN KEY (instance_id) REFERENCES public.instances(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: links links_child_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_child_id_fkey FOREIGN KEY (child_id) REFERENCES public.instances(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: links links_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.instances(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: links links_relation_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.links
    ADD CONSTRAINT links_relation_id_fkey FOREIGN KEY (relation_id) REFERENCES public.relations(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: preferences preferences_user_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.preferences
    ADD CONSTRAINT preferences_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: relations relations_child_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT relations_child_id_fkey FOREIGN KEY (child_id) REFERENCES public.entities(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: relations relations_child_side_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT relations_child_side_type_id_fkey FOREIGN KEY (child_side_type_id) REFERENCES public.relation_side_types(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: relations relations_parent_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT relations_parent_id_fkey FOREIGN KEY (parent_id) REFERENCES public.entities(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: relations relations_parent_side_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.relations
    ADD CONSTRAINT relations_parent_side_type_id_fkey FOREIGN KEY (parent_side_type_id) REFERENCES public.relation_side_types(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: users users_account_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_account_id_fkey FOREIGN KEY (account_id) REFERENCES public.accounts(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: users users_user_type_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: madb
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_user_type_id_fkey FOREIGN KEY (user_type_id) REFERENCES public.user_types(id) ON UPDATE SET DEFAULT ON DELETE CASCADE;


--
-- Name: TABLE paypal_communications; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.paypal_communications TO madb;


--
-- Name: SEQUENCE paypal_communications_id_seq; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON SEQUENCE public.paypal_communications_id_seq TO madb;


--
-- PostgreSQL database dump complete
--

