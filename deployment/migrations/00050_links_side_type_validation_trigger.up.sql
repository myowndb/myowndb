-- For each side (child_id,parent_id), check the relation to
-- see the side type (one or many)
-- if one, check the child_id/parent_id inserted is not already present
CREATE OR REPLACE FUNCTION links_side_type_validation()
RETURNS TRIGGER AS
$$
DECLARE
child_side_type integer;
parent_side_type integer;
count integer;
child_msg text;
parent_msg text;
msg text;
has_error bool;
BEGIN
select '' into child_msg;
select '' into parent_msg;
select '' into msg;
select false into has_error;
select relations.child_side_type_id into child_side_type from relations where id = NEW.relation_id;
select parent_side_type_id into parent_side_type from relations where id = NEW.relation_id;
IF child_side_type=1
THEN
    -- if only one child is accepted, check that the parent is not already in the links, as it would mean
    -- it already has a child
    select count(*) into count from links where relation_id=NEW.relation_id and parent_id=NEW.parent_id;
    IF count>0
    THEN
        select 'child_side_type (one) not respected, parent has already a child);' into msg;
        select true into has_error;
    END IF;
END IF;
IF parent_side_type=1
THEN
    -- if only one child is accepted, check that the parent is not already in the links, as it would mean
    -- it already has a child
    select count(*) into count from links where relation_id=NEW.relation_id and child_id=NEW.child_id;
    IF count>0
    THEN
        select msg||'parent_side_type (one) not respected, child has already a parent);' into msg;
        select true into has_error;
    END IF;
END IF;

IF has_error
THEN
    Raise Exception '%', msg;
END IF;
RETURN NEW;
END;
$$ LANGUAGE PLpgSQL;


CREATE TRIGGER links_side_type_validation BEFORE INSERT OR UPDATE ON links
FOR EACH ROW EXECUTE PROCEDURE links_side_type_validation();
