-- Ensure that details cannot be linked to entities in another database
CREATE OR REPLACE FUNCTION conceal_entities2details()
RETURNS TRIGGER AS
$$
DECLARE
entity_database_id integer;
detail_database_id integer;
msg text;
BEGIN
select entities.database_id into entity_database_id from entities where id = NEW.entity_id;
select details.database_id into detail_database_id from details where id = NEW.detail_id;
IF entity_database_id != detail_database_id
THEN
    select 'entities2details cannot link across databases, '||entity_database_id||' for entity '|| NEW.entity_id ||', '||detail_database_id||' for detail '||NEW.detail_id into msg;
    Raise Exception '%', msg;
END IF;
RETURN NEW;
END;
$$ LANGUAGE PLpgSQL;


CREATE TRIGGER entities2details_concealed_to_database BEFORE INSERT OR UPDATE ON entities2details
FOR EACH ROW EXECUTE PROCEDURE conceal_entities2details();
