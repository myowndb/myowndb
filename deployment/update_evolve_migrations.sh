#!/bin/bash -eu

# retrieve the absolute path of the destination directory before we change directory
# requires bash -e flag to stop script if the argument is not passed
destination_dir="$(readlink -f "${1:?destination directory required as first argument}")"

# change to our script directory
script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
cd "$script_dir"

# cleanup
rm -rf "${destination_dir}"
mkdir -p "${destination_dir}"
# link to rhe absolute path so this scrip can be called from any location
# add a 1 because migration 0 is not used by evolve
for p in migrations/*up.sql; do m=$(basename $p); ln -s "$(readlink -f "$p")" "${destination_dir}/V${m%%_*}1__${m#*_}"; done
