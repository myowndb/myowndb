#!/bin/bash
# creates config files for database test to use local database

set -x

if [[ -n "$SUDO_USER" ]] && [[ "$SUDO_USER" == "gitpod" ]]; then
    user="gitpod"
else
    user="postgres"
fi

pwd

(
cd ../tests/lib
cat >conf/database.json <<EOF
{ "db": {
    "host": "127.0.0.1",
    "port": 5432,
    "user": "$user",
    "pass": "postgres",
    "db":   "myowndbtest"
    }
}
EOF
)

(
cd ../webui
cat >conf/database.test.json <<EOF
{ "db": {
    "host": "127.0.0.1",
    "port": 5432,
    "user": "$user",
    "pass": "postgres",
    "db":   "myowndbuitest"
    }
}
EOF
cd ../tests
cat >webui/IdentityTests/conf/database.test.json <<EOF
{ "db": {
    "host": "127.0.0.1",
    "port": 5432,
    "user": "$user",
    "pass": "postgres",
    "db":   "myowndbtest"
    }
}
EOF
cat >webui/playwright/conf/database.json <<EOF
{ "db": {
    "host": "127.0.0.1",
    "port": 5432,
    "user": "$user",
    "pass": "postgres",
    "db":   "myowndb"
    }
}
EOF
cat >webui/playwright/conf/myowndb.test.json <<EOF
{
    "myowndb": {
        "baseUrl": "http://localhost:5000",
        "urls" : {
            "publicFormDocumentation": ""
        },
        "database": "conf/database.test.json",
        "auth": {
            "minimumPasswordLength":10 },
        "fileAttachments": {
            "path": "run/attachments",
            "waitRoom": "run/waitRoom",
            "maxBytes": 524289000
        },
        "requests": {
            "maxBytes": 524789000
        },
        "saas" : {
            // is this a saas hosted version. False for self-hosted
            "is_saas": true,
            // URLs to TOS and privacy policy
            "tos" : "https://www.myowndb.com/tos.html",
            "privacy": "https://www.myowndb.com/privacy.html"
        }
    }
}
EOF
)
