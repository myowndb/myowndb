//********************************************************************************
// This generates a 5000 record csv file named file.csv in the current directory,
// that can be imported in the test database "DemoForTests", entity "Form tests"
// You need to ensure the link Program.fs points to this file and then call
// dotnet run
//********************************************************************************

open System
open Bogus
open MyOwnDB.Tests.IdsHelpers
open FSharp.Json
open CsvHelper
open CsvHelper.TypeConversion
open System.IO
open System.Globalization

// We define a type so we can define a CustomInstantiator for ti
type CsvRow ={
  name: string
  notes: string
  birthdate: DateTime
  count: int
  approved: string
  email: string
  website: string
}

// We will be able to choose randomly a value in the list
let approvedValues = ["Yes";"No"]



[<EntryPoint>]
let main argv =
    let gen =
      // Define the custom instanciator, using a faker for each field
      Faker<CsvRow>().CustomInstantiator(fun f ->
        {
          name = f.Person.FirstName
          notes = f.Lorem.Sentence(5)
          birthdate = DateTime.Parse(f.Date.Past().ToString("yyyy.MM.dd"));
          count = f.Random.Number(1,1000)
          approved = f.PickRandom(approvedValues)
          email = f.Internet.Email()
          website = f.Internet.Url()
}
      )
    // We can then generated the records
    let records = gen.Generate(5000)
    // We will write these to a csv file
    use writer = new StreamWriter("./file.csv")
    // By default CsvWriter does not quote values, we want it
    let config = new Configuration.CsvConfiguration(CultureInfo.InvariantCulture, ShouldQuote = (fun _ -> true ))
    // CsvWriter can handle the list of records directly to generate the csv with a header line
    let options = new TypeConverterOptions ( Formats = [|"yyyy-MM-dd"|] );
    use csv = new CsvWriter(writer, config)
    csv.Context.TypeConverterOptionsCache.AddOptions<DateTime>(options)
    csv.WriteRecords(records)
    1
