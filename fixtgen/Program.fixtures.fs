// Learn more about F# at http://docs.microsoft.com/dotnet/fsharp

open System
open Bogus
open MyOwnDB.Tests.IdsHelpers
open FSharp.Json

// Define a function to construct a message to print
let from whom =
    sprintf "from %s" whom

[<EntryPoint>]
let main argv =
    // highest existing ids
    let instanceId = ref 100100
    let detailValueId = ref 100100

    // entity for which we generate instances
    let entityId = 12
    let numberOfInstances = 100
    // details information for entity 12
    // this is a function so a new person is created and reused across detail values of one instance
    let details12 () =
        let person = Faker().Person
        [
        {| id= 48; kind= "SimpleDetailValue" ; gen = (fun () -> person.LastName )|}  // name
        {| id= 56; kind= "SimpleDetailValue" ; gen = (fun () -> person.FirstName)|} // first name
        {| id= 57; kind= "SimpleDetailValue" ; gen = (fun () -> Faker().Name.JobTitle() ) |} // function
        {| id= 58; kind= "SimpleDetailValue" ; gen = (fun () -> Faker().Commerce.Department() ) |} // service
        {| id= 59; kind= "SimpleDetailValue" ; gen = (fun () -> Faker().Address.BuildingNumber().ToString()  ) |} // location
        {| id= 63; kind= "EmailDetailValue"  ; gen = (fun () -> Faker().Internet.Email(person.FirstName, person.LastName) ) |}  // email
    ]


    let instanceGenerator =
        Faker<Instance.InstanceFixture>().CustomInstantiator(fun f ->
            incr instanceId
            {
                id = !instanceId
                entity_id = entityId
                // Legivel complains that datetime in yaml are strings, so set None
                created_at = None
                updated_at = None
                lock_version = 0
            }
         )
    let instances : List<Instance.InstanceFixture>= instanceGenerator.Generate(numberOfInstances) |> List.ofSeq


    let detailValues =
        instances
        |> List.map (fun (i:Instance.InstanceFixture) ->
            let instanceId = i.id
            details12()
            |> List.map (fun d ->
                incr detailValueId
                let detailId = d.id
                let r:DetailValue.DetailValueFixture = {
                    id = !detailValueId
                    detail_id = detailId
                    instance_id = instanceId
                    value = d.gen()
                    ``type`` = d.kind
                    lock_version = 0
                    // Legivel complains that datetime in yaml are strings, so set None
                    created_at = None
                    updated_at = None
                }
                r
            )
        )

    let instanceMapInfo =
        instances
        |> List.map (fun i -> $"instance_{i.id}" , i )
    let instanceMap = new Map<string,Instance.InstanceFixture>(instanceMapInfo)

    let detailValueMapInfo =
        detailValues
        |> List.map (fun dl ->
            dl
            |> List.map (fun (d:DetailValue.DetailValueFixture) -> $"detail_value_{d.id}", d)  )
        |> Seq.concat
    let detailValueMap = new Map<string,DetailValue.DetailValueFixture>(detailValueMapInfo)


    instances
    |> List.iter (fun i ->
        printfn "%A" i
    )

    printfn "********************************************************************************"
    detailValues
    |> List.iter (fun l ->
        l
        |> List.iter (fun dv -> printfn "%A" dv)
    )

    let config = JsonConfig.create(serializeNone = SerializeNone.Omit)

    // json is valid yaml, so write to file with .yml extension so it can be used as is as a fixture
    System.IO.File.WriteAllText("/tmp/instances.yml", Json.serializeEx config instanceMap)
    System.IO.File.WriteAllText("/tmp/detail_values.yml", Json.serializeEx config detailValueMap)

    printfn "Data written in /tmp/instances.yml and /tmp/detail_values.yml"
    0 // return an integer exit code