namespace web
open Hangfire
open Serilog
open MBrace.FsPickler
open MBrace.FsPickler.Json
open Microsoft.Extensions.DependencyInjection

// This is a Hangfire job runner executing a DBAction.
// It should be used only for simple DBActions, as it needs to serialise it
// to persist it in database. For example it fails to handle a DBAction wrapped
// in a transaction because the wrapping function uses a ref cell to track if a new
// transaction was opened (see DBAction.wrapInTransactionIfNeeded).

// This is the interface type that will be injected as dependency
type IDBActionRunner =
    abstract Send: string * DBAction.DBAction<_> -> unit

// We need a class here so that Hangfire can serialize it.
// From an error message I had:
//   A class should either have a default constructor,
//   one constructor with arguments or a constructor marked
//   with the JsonConstructor attribute
type DBActionWorker(container:System.IServiceProvider) =
    member _.Send(title:string,serialisedAction:string) =
      async {
        // We got an error because DB.DBClient only implements IAsyncDisposable: type only implements IAsyncDisposable. Use DisposeAsync to dispose the container.
        // It seems this cryptic error requires to create an async scope, get the DBClient from that scope, and call DisposeAsync ourself on that scope when
        // we have finished with DBClient. There's no good doc about this, and I got to this working solution after trials and errors :-(
        // The other dependencies can be had via the normal dependency injection.
        let scope = container.CreateAsyncScope()
        let dbClient = scope.ServiceProvider.GetRequiredService<DB.DBClient>();
        try
          // If this fails, we go to the exception handling and don't execute the finally.
          // This is fine as if it raises an exception, we will not have an open connection that we should close.
          do! dbClient.Open()
          try
            // Deserialise the DBAction Hangfire retrieves from database, and run it
            let jsonSerializer = FsPickler.CreateJsonSerializer(indent = false)
            let action = jsonSerializer.UnPickleOfString<DBAction.DBAction<unit>>(serialisedAction)
            // The serialisation screws up the transaction wrapping (the wrapping uses a ref cell to keep
            // track of the start of a new transaction, but after serialisation the cell doesn't get updated
            // and the transaction is not committed.)
            // That's why we wrap it with a transaction here, after deserialising the original DBAction.
            // As this is the outer transaction, it is the one that will be active. As it doesn't go
            // through serialisation, it will be committed. Inner transactions
            // will not be activated, but that's fine. These don't seem to have to set savepoints
            // either, see https://gitlab.com/monazita/monazita/-/issues/1
            let wrappedAction =
              action
              |> DBAction.wrapInTransactionIfNeeded
            let! result = DBAction.run dbClient wrappedAction
            match result with
            | Ok _ ->
              Log.Information("DBAction job with title {title} ran successfully", title)
            | Error es ->
              Log.Error("DBAction job with title {title} encountered errors: {errors}", title, (es |> Error.toString))
          // If the connection was opened, ensure we close it here
          finally
            dbClient.Close() |> Async.Start
        with
        | e ->
          Log.Error("DBAction job with title {title} encountered exception: {exception}", title, sprintf "%A" e)

        // We're done with the DBClient, so we can dispose the scope we got it from, which will also dispose of the DBClient
        do! (scope :> System.IAsyncDisposable).DisposeAsync().AsTask() |> Async.AwaitTask
        }
      |> Async.StartAsTask :> System.Threading.Tasks.Task

// We need a class here to it can be injected as a dependency.
type DBActionRunner() =
    interface IDBActionRunner with
        member _.Send(title:string,action:DBAction.DBAction<_>) =
            // For serialisation, we need to have the type wrapped by the DBAction determined. We decided to only handle DBAction<unit>.
            // Start by ensuring we will serialise a DBAction<unit> without bothering the caller
            // We also wrap it in a transaction to ensure consistency of the database, but only if the caller didn't already do it.
            let unitAction =
              action
              |> DBAction.map (fun _ -> ())
            // We need to serialise the DBAction for Hangfire to store it before it runs
            let jsonSerializer = FsPickler.CreateJsonSerializer(indent = false)
            let json = jsonSerializer.PickleToString(unitAction)

            let id = BackgroundJob.Enqueue<DBActionWorker>( fun (worker:DBActionWorker) -> worker.Send(title,json) )
            Log.Debug("Created DBActionRunner job with id {id} for jobs with title {title}", id, title)
