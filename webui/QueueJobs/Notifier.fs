namespace web
open Hangfire
open Serilog

open FSharp.Data
// For jsonvalue?field syntax
open FSharp.Data.JsonExtensions

open NotificationSubscription
open NotificationSubscriptionCriteria

open Microsoft.Extensions.DependencyInjection
// This is the interface type that will be injected as dependency
// The string is the url to link to in the notification.
type INotifier =
    abstract Send: NotificationSubscription.NotificationSubscription * string -> unit

// We inject dependencies in the constructor. This means the worker needs to be enqueued with
//  BackgroundJob.Enqueue<NotificationWorker>( fun (worker:NotificationWorker) -> ...)
type NotificationWorker(container:System.IServiceProvider, mailer:IMailer) =
  member _.Send(sub:NotificationSubscription,url) =
    async {
      // We got an error because DB.DBClient only implements IAsyncDisposable: type only implements IAsyncDisposable. Use DisposeAsync to dispose the container.
      // It seems this cryptic error requires to create an async scope, get the DBClient from that scope, and call DisposeAsync ourself on that scope when
      // we have finished with DBClient. There's no good doc about this, and I got to this working solution after trials and errors :-(
      // The other dependencies can be had via the normal dependency injection.
      let scope = container.CreateAsyncScope()
      let dbClient = scope.ServiceProvider.GetRequiredService<DB.DBClient>();
      //let t = scope.ServiceProvider.GetRequiredService<web.I18n>();
      let! res =
        match sub.protocol with
        | Smtp ->
          DBAction.queryAction "select email from users where id = %d" (int sub.destination)
          |> DBAction.bind
            (fun email ->
                match sub.source_type with
                | Instance ->
                  let entityIdInt = sub.source_filter?entity_id.AsInteger()
                  DBAction.queryAction "select name from entities where id = %d" entityIdInt
                  |> DBAction.map (fun (entityName:string) ->
                    // We currently send notifications in english. To send localised emails, we first need
                    // to store the language preference if the users in the database.
                    let locale = "en"
                    let cultureInfo = new System.Globalization.CultureInfo(locale);
                    System.Threading.Thread.CurrentThread.CurrentUICulture <- cultureInfo
                    // Now that the CurrentUICulture is set, we can load the catalog.
                    let t = web.I18n(NGettext.Catalog("myowndb", "./locale"))
                    // Do the translations and send the mail
                    let subject = t.t("New {0}",entityName)
                    let body = t.t("Dear user,\nA new {0} entry was created, you can view it by following this link: {1}",entityName,url)
                    mailer.Send(email,subject, body )
                    MyOwnDBLib.Logging.Log.Information("Sent notification to {user} for creation of instance at {url} of entity {entityId}",email,url,entityIdInt)
                    // Map to unit as this is expected
                    ()
                  )
            )
          |> DBAction.ensureSingleRow
          |> DBAction.mapError (fun es ->
            MyOwnDBLib.Logging.Log.Error("Error sending notification with URL {url} for subscription {subscription}.\n{error}", url, sub, es|>Error.toString)
            es
          )
          |> DBAction.run dbClient
      // We're done with the DBClient, so we can dispose the scope we got it from, which will also dispose of the DBClient
      do! (scope :> System.IAsyncDisposable).DisposeAsync().AsTask() |> Async.AwaitTask

      // Log results
      match res with
      | Ok _ ->
        MyOwnDBLib.Logging.Log.Debug("Handled Notification job for recipient {subscription}", sub)
      | Error es ->
        MyOwnDBLib.Logging.Log.Error("Error sending notification for subscription {subscription}: {error} ", sub,es|>Error.toString)

    }
    // The cast generates a non-generic Task, see https://github.com/fsharp/fslang-design/blob/main/FSharp-6.0/FS-1097-task-builder.md#producing-non-generic-task-values
    // This is required to make it accepted by Hangfire as a System.Linq.Expressions.Expression<Func<System.Threading.Tasks.Task>> )
    |> Async.StartAsTask :> System.Threading.Tasks.Task

// We need a class here to it can be injected as a dependency.
type InstanceNotifier() =
    interface INotifier with
        member _.Send(sub:NotificationSubscription, url:string) =
            let id = BackgroundJob.Enqueue<NotificationWorker>( fun (worker:NotificationWorker) -> worker.Send(sub,url))
            MyOwnDBLib.Logging.Log.Debug("Created Instance notifier job with id {id} for {destination_type} {recipient} regarding instance  at {url}", id, sub.destination_type,sub.destination, url)
