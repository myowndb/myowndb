namespace web
open Hangfire
open Serilog
open System.IO

// This is the interface type that will be injected as dependency
type IAttachmentsCleaner =
    abstract Clean: string -> unit

// We need a class here so that Hangfire can serialize it.
// From an error message I had:
//   A class should either have a default constructor,
//   one constructor with arguments or a constructor marked
//   with the JsonConstructor attribute
// This worker simply prints on stdout the mail recipient, subject and body.
type AttachmentsCleanerWorker() =
    static member Clean(path:string) =
        if File.Exists(path) then
            printfn "deleting %s" path
            File.Delete(path)
            Log.Debug("Handled AttachmentsCleaner job for {path}", path)
        else
            Log.Error("AttachmentsCleaner didn't find the file {path}", path)

// We need a class here to it can be injected as a dependency.
type AttachmentsCleaner() =
    interface IAttachmentsCleaner with
        member _.Clean(path) =
            let id = BackgroundJob.Enqueue( fun () -> AttachmentsCleanerWorker.Clean(path) )
            Log.Debug("Created AttachmentCleaner job with id {id} for path {path}", path)

type NoOpCleaner()=
    interface IAttachmentsCleaner with
        member _.Clean(_path) = ()