namespace web
open Hangfire
open Serilog

// This is the interface type that will be injected as dependency
type IMailer =
    abstract Send: string * string * string -> unit

// We need a class here so that Hangfire can serialize it.
// From an error message I had:
//   A class should either have a default constructor,
//   one constructor with arguments or a constructor marked
//   with the JsonConstructor attribute
// This worker simply prints on stdout the mail recipient, subject and body.
type ConsoleMailerWorker() =
    static member Send(recipient,subject,body) =
        printfn "mailing to %s [%s] %s" recipient subject body
        Log.Debug("Handled Mailer job for recipient {recipient}", recipient)

// We need a class here to it can be injected as a dependency.
type ConsoleMailer() =
    interface IMailer with
        member _.Send(recipient, subject, body) =
            let id = BackgroundJob.Enqueue( fun () -> ConsoleMailerWorker.Send(recipient,subject,body) )
            Log.Debug("Created Mailer job with id {id} for recipient {recipient}", id, recipient)

type NoOpMailer()=
    interface IMailer with
        member _.Send(r,s,b) = ()

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// System.Net.Mail based Mailer
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// I didn't manage to make it work with OVH smtp relay.
open System.Net
open System.Net.Mail
open Microsoft.Extensions.Configuration
type SystemMailerWorker() =
  static member Send(config:{|host:string;port:string;user:string;password:string;enableSsl:bool|}, recipient,subject,body) =
        let toAddress = new MailAddress(recipient)
        let from  = new MailAddress("info@myowndb.com")

        let email = new MailMessage(from, toAddress);
        email.Subject <- subject
        email.Body <- body

        let smtp = new SmtpClient();
        smtp.Host <- config.host
        smtp.Port <- int (config.port);
        smtp.Credentials <- new NetworkCredential(config.user, config.password);
        smtp.DeliveryMethod <- SmtpDeliveryMethod.Network;
        smtp.EnableSsl <- config.enableSsl;

        smtp.Send email

        Log.Debug("Handled SMTP Mailer job for recipient {recipient}", recipient)

// We need a class here to it can be injected as a dependency.
type SystemMailer(config:IConfiguration) =
    interface IMailer with
        member _.Send(recipient, subject, body) =
            let workerConfig = {|host=config.Item("myowndb:smtp:host");port = config.Item("myowndb:smtp:port"); user=config.Item("myowndb:smtp:user"); password=config.Item("myowndb:smtp:password");enableSsl=config.Item("myowndb:smtp:enableSsl")="true"|}
            let id = BackgroundJob.Enqueue( fun () -> SystemMailerWorker.Send(workerConfig,recipient,subject,body) )
            Log.Debug("Created SMTP Mailer job with id {id} for recipient {recipient}", id, recipient)

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// MailKit based Mailer
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
open MailKit.Net.Smtp
open MailKit
open MimeKit
type MailkitMailerWorker() =
  static member Send(config:{|host:string;port:string;user:string;password:string;enableSsl:bool;sender:string|}, recipient,subject,body) =
        let message = new MimeMessage()

        message.From.Add (new MailboxAddress ("MyOwndb", config.sender))
        message.To.Add (new MailboxAddress ("", recipient));
        message.Subject <- subject
        message.Body <- new TextPart("plain",Text=body)

        let client = new SmtpClient()
        client.Connect(config.host,int config.port,true)
        client.Authenticate(config.user,config.password)
        client.Send(message)|> ignore
        client.Disconnect(true)

        Log.Debug("Handled Mailkit Mailer job for recipient {recipient}", recipient)

// We need a class here to it can be injected as a dependency.
type MailkitMailer(config:IConfiguration) =
    interface IMailer with
        member _.Send(recipient, subject, body) =
            let workerConfig = {|host=config.Item("myowndb:smtp:host");port = config.Item("myowndb:smtp:port"); user=config.Item("myowndb:smtp:user"); password=config.Item("myowndb:smtp:password");enableSsl=config.Item("myowndb:smtp:enableSsl")="true"; sender=config.Item("myowndb:smtp:fromAddress")|}
            let id = BackgroundJob.Enqueue( fun () -> MailkitMailerWorker.Send(workerConfig,recipient,subject,body) )
            Log.Debug("Created Mailkit Mailer job with id {id} for recipient {recipient}", id, recipient)
