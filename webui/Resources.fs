namespace web

open System
open WebSharper
open WebSharper.Resources
open Zanaptak.TypedCssClasses

module Css=
    type BS=CssClasses<"wwwroot/vendor/bootstrap/css/bootstrap.min.css">
    type SBA=CssClasses<"wwwroot/vendor/sbadmin/css/styles.css">
    type AppCss=CssClasses<"wwwroot/app/css/common.css">

module AppResources =

    module Bootstrap =
        type Js() =
            inherit BaseResource("/vendor/bootstrap/js/bootstrap.bundle.min.js")
        type Css() =
            inherit BaseResource("/vendor/bootstrap/css/bootstrap.min.css")

    module SBAdmin =
        type Js() =
            inherit BaseResource("/vendor/sbadmin/js/scripts.js")
        type Css() =
            inherit BaseResource("/vendor/sbadmin/css/styles.css")

    module FrontEndApp =
        type Css() =
            inherit BaseResource("/app/css/common.css")

        type Js() =
            inherit BaseResource("/app/js/common.js")

    module Gettext =
        type Js() =
            inherit BaseResource("/vendor/gettext.min.js")

    // FIXME: this is a 1.2MB file, we should improve things and only load
    // required icons...
    module FontAwesome =
        type Js() =
            inherit BaseResource("/vendor/font-awesome/all.min.js")

    module DOMPurify =
        type Js() =
            inherit BaseResource("/vendor/DOMPurify/purify.min.js")

    module Marked =
        type Js() =
            inherit BaseResource("/vendor/marked/marked.min.js")

    module Driver =
        type Css() =
            inherit BaseResource("/vendor/driver/css/driver.css")

        type Js() =
            inherit BaseResource("/vendor/driver/js/driver.iife.js")

    // Adding your resource here will make it available to all pages, meaning it is also loaded
    // for every page.
    // For loading resources only on pages using it, add the Require annotation to the module or
    // function needing it. See eg in the MyOwnDBHelp module in WSHelper.fs
    [<assembly:Require(typeof<Bootstrap.Js>);
      assembly:Require(typeof<Bootstrap.Css>);
      assembly:Require(typeof<SBAdmin.Css>);
      assembly:Require(typeof<SBAdmin.Js>);
      assembly:Require(typeof<FrontEndApp.Css>);
      assembly:Require(typeof<FrontEndApp.Js>);
      assembly:Require(typeof<FontAwesome.Js>);
      assembly: WebResource("/vendor/gettext_spread.js", "text/javascript")
      >]
    do()
