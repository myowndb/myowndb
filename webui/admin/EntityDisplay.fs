namespace web.admin

open WebSharper.UI.Html
open WebSharper
open WebSharper.UI
open WebSharper.UI.Client
open web.ClientI18n
open web.HtmlHelpers
open web.WSExtensions
open web.Css

[<JavaScript>]
module EntityDisplay =
    open web

    // Distinguish first render from subsequent updates to avoid flashing all elements at first render
    type RenderCycle = |First |Update
    // This is called by the ClientContent.clientBody() to render the body of the page
    // according to the client-side router state.
    let pageBody (entityId:int) (pageInfo:web.PageInfo.Admin.Entity.PageInfo)= async {
        let t = JStr()

        //********************************************************************************
        // Setup data used in multiple sections below
        //********************************************************************************
        let renderCycleVar = Var.Create First
        // Feedback Var
        let resultVar=Var.Create Doc.Empty
        // We initialise the entity details list and the linkable details list at the top level
        // because those have interactions: and a linked detail is unlinked it should appear in
        // the linkable list, and vice-versa.
        // Initialise an empty ListModel that we will populate with the list above
        let collection =
            ListModel.Create
                (fun (ed:Entity.EntityDetail) -> ed.id)
                []
        // Initialise empty linkable details list
        let linkableDetailsListModel:ListModel<int,Detail.Detail> =
          ListModel.Create
            (fun d -> let (Ids.DetailId id) = d.id in id)
            []
        let refreshLinkableDetailsList () = async {
            let! linkableDetailsResult = Server.Entity.linkableDetails entityId
            match linkableDetailsResult with
              | Error es ->
                resultVar.Set(dangerBox (es|>Error.toString|>text))
                linkableDetailsListModel.Set []
              | Ok details ->
                linkableDetailsListModel.Set details
          }
        ////////////////////////////////////////////           _      _        _ _
        //        __| | ___| |_ __ _(_) |___
        //       / _` |/ _ \ __/ _` | | / __|
        //      | (_| |  __/ || (_| | | \__ \
        //       \__,_|\___|\__\__,_|_|_|___/
        /////////////////////////////////////////////


        //********************************************************************************
        // Display currently linked details
        //********************************************************************************
        // Define the <li> list from the collection based on the collection's view
        // We do it separately from the <ul> so we don't recall DocSeqCached when we will
        // update the lu, as it would prevent the render of only updated entries
        let lis :Doc=
            collection.View.DocSeqCached
              // The collection holds a sequence of which we map individual elements to an <li>
              (fun (entityDetail:Entity.EntityDetail) ->

                  let deleteElement (entity2detailId:string)(refreshFunction:unit->unit)=
                      DeleteConfirmation.deleteConfirmation "detail" (
                          (fun () -> async {
                              match! (Server.Entity.UnlinkDetail (int entity2detailId)) with
                              | Ok _ ->
                                  refreshFunction()
                                  do! refreshLinkableDetailsList()
                                  // signal success
                                  return true
                              | Error _ ->
                                  // signal failure
                                  return false
                          })
                      )




                  li
                    // Set attribute according to render cycle. At first render, do not flash element.
                    // But for update, flash the rendered element.
                    (
                      match renderCycleVar.Value with
                      | First -> []
                      | Update -> flashExclusiveAttributes()
                    )
                    [
                        // Detail name in the first row with an Edit icon
                        div
                            [classes [BS.``d-flex``; BS.``flex-row``]]
                            [
                              div [classes [BS.``p-2``;"myowndbdetailname"]] [text (entityDetail.name); fontAwesomeIconWithClasses [BS.``ms-1``] "edit" ]
                            ]

                        // Actions row
                        div
                            [classes [BS.``d-flex``; BS.``flex-row``]]
                            [

                                div
                                    [classes [BS.``d-flex``; BS.``flex-row``]]
                                    [
                                        // Visibility in list
                                        div [classes [BS.``p-2``;"myowndbdisplayinlistview"]]
                                            [
                                                (entityDetail.displayed_in_list_view
                                                 |> (fun b ->
                                                    let setVisibility (b:bool)=
                                                        async {
                                                            let! r = Server.Entity.setListVisibility entityDetail.id b
                                                            match r with
                                                            | Ok [ed] ->
                                                                collection.Add ed
                                                                resultVar.Set(successBox (t.tt("Saved successfully.")))
                                                            | _ ->
                                                                resultVar.Set(dangerBox (t.tt("An error occured")))
                                                            ()
                                                        }|> Async.Start
                                                    if b then
                                                        div
                                                            [on.click (fun el ev -> setVisibility false)
                                                             attr.``data-`` "myowndb-action" "hide-in-list";
                                                            ]
                                                            [
                                                                t.tt("Displayed in list view")
                                                                fontAwesomeIconWithClasses [BS.``ms-1``] "toggle-on"
                                                            ]
                                                    else
                                                        div
                                                            [on.click (fun el ev -> setVisibility true)
                                                             attr.``data-`` "myowndb-action" "show-in-list";
                                                            ]
                                                            [
                                                                t.tt("Not displayed in list view")
                                                                fontAwesomeIconWithClasses [BS.``ms-1``] "toggle-off"
                                                            ]
                                                 ))
                                            ]
                                        // Remove detail from entity
                                        div [classes [BS.``p-2``;"myowndbentitydetailremove"]
                                             attr.``data-`` "myowndb-action" "delete";
                                            ]
                                            [
                                                a [] [(deleteElement(string entityDetail.id) (fun () -> collection.Remove(entityDetail))).V]
                                            ]
                                        // Move up in display order
                                        div [classes [BS.``p-2``];
                                             attr.``data-`` "myowndb-action" "up";
                                             on.click (fun _el _ev ->
                                                  let rec up (l:List<Entity.EntityDetail>)(i:Entity.EntityDetail) = async {
                                                      match l with
                                                      | h::snd::t ->
                                                          if snd = i then
                                                            let newFirst = {snd with display_order = h.display_order}
                                                            let newSecond = {h with display_order = snd.display_order}
                                                            let! res = Server.Entity.updateEntityDetailsDisplayOrder [newFirst;newSecond]
                                                            match res with
                                                            | Ok _ ->
                                                              return newFirst::newSecond::t
                                                            | Error es ->
                                                              resultVar.Set(dangerBox (es |> Error.toString|>text))
                                                              return l
                                                          else
                                                            // This is not tail recursive, but ok as 2 elements list
                                                            let! newTail = (up (snd::t) i)
                                                            return h :: newTail
                                                      | _  ->  return l
                                                  }

                                                  async {
                                                    let! updatedList = up (collection.Value|>List.ofSeq) entityDetail
                                                    collection.Set (updatedList|>Seq.ofList)
                                                  } |> Async.Start

                                                )
                                            ]
                                            [
                                                fontAwesomeIcon "arrow-up"
                                            ]
                                        // Move down in display order
                                        div [classes [BS.``p-2``];
                                             attr.``data-`` "myowndb-action" "down";
                                             on.click (fun _el _ev ->
                                                  let rec down (l:List<Entity.EntityDetail>)(i:Entity.EntityDetail) = async {
                                                      match l with
                                                      | h::snd::t ->
                                                          if h = i then
                                                            let newFirst = {snd with display_order = h.display_order}
                                                            let newSecond = {h with display_order = snd.display_order}
                                                            let! res = Server.Entity.updateEntityDetailsDisplayOrder [newFirst;newSecond]
                                                            match res with
                                                            | Ok _ ->
                                                              return newFirst::newSecond::t
                                                            | Error es ->
                                                              resultVar.Set(dangerBox (es |> Error.toString|>text))
                                                              return l
                                                          else
                                                            // This is not tail recursive, but ok as 2 elements list
                                                            let! newTail = (down (snd::t) i)
                                                            return h :: newTail
                                                      | _  ->  return l
                                                  }

                                                  async {
                                                    let! updatedList = down (collection.Value|>List.ofSeq) entityDetail
                                                    collection.Set (updatedList|>Seq.ofList)
                                                  } |> Async.Start
                                                )
                                            ]
                                            [
                                                fontAwesomeIcon "arrow-down"
                                            ]
                                    ]
                            ]
                    ]
              )

        // Var holding the <ul> with the list of details of this entity
        let ulVar = Var.Create Doc.Empty
        // Define the function retrieving entity details from the backend and updating the collection
        // used by DocSeqCached
        let refreshEntityDetails (cycle:RenderCycle) = async {
          // Set the var used by the DocSeqCached mapping to <li>s so it know if it needs to flash elements
          renderCycleVar.Set cycle
          // Get list from the backend
          let! entityDetailsResult = Server.Entity.getEntityDetails entityId
          match entityDetailsResult with
          | Error es-> resultVar.Set (infobox InfoBoxKind.Danger (es|>Error.toString|>text))
          | Ok entityDetails ->
              collection.Set(entityDetails|>Seq.ofList)
              // Build the <li>s to put inside the <ul>
              // put the <li>s in the <ul>
              ulVar.Set(
                  ul
                      [attr.id "entityDetails"]
                      [lis]
              )
        }
        // Call the function immediately for initial display
        do! refreshEntityDetails(First)

        //********************************************************************************
        // Link an existing details
        //********************************************************************************
        let buildLinkExistingDetailSection() = async {
            do! refreshLinkableDetailsList()
            let linkThisDetail (detail:Detail.Detail) = async {
              let! res = Server.Entity.linkExistingDetail entityId detail
              match res with
              | Ok l ->
                collection.Set l
                linkableDetailsListModel.Remove detail
              | Error es ->
                    resultVar.Set(dangerBox (es|>Error.toString|>text))

            }
            let rows =
              linkableDetailsListModel.View.DocSeqCached (fun (d:Detail.Detail) ->
                tr
                  []
                  [
                    td
                      []
                      [ text d.name]
                    td
                      [on.click (fun _el _ev ->
                        async {
                          do! linkThisDetail d
                        } |> Async.Start);
                       attr.``data-`` "myowndb-action" "link"
                      ]
                      [
                        li [ classes ["fas"; "fa-link"; BS.``me-5``;"myowndb-action-cell"]] []
                      ]
                  ]
            )
            return
                  buttonRevealingContent
                    {|
                      buttonTextWhenVisible = t.tt("Hide existing details")
                      buttonTextWhenHidden = t.tt("Define field with existing detail")
                    |}
                    (
                      table
                        [ classes [BS.table; BS.``table-bordered``; BS.``table-striped``; BS.``table-hover``; BS.``table-sm``; ];attr.id "adminUsableDetails" ]
                        [
                            thead []
                                  [
                                    tr []
                                       [
                                         th [classes [ BS.``w-25``]]
                                               [
                                                 div [classes [AppCss.``th-inner``]]  [t.tt("Name")]
                                               ]

                                       ]

                                    ]

                            tbody []
                                  [
                                    rows
                                  ]
                        ]
                    )
        }
        let! linkExistingDetailSection =
          buildLinkExistingDetailSection()

        /////////////////////////////////////////////////             _       _   _
        //    _ __ ___| | __ _| |_(_) ___  _ __  ___
        //   | '__/ _ \ |/ _` | __| |/ _ \| '_ \/ __|
        //   | | |  __/ | (_| | |_| | (_) | | | \__ \
        //   |_|  \___|_|\__,_|\__|_|\___/|_| |_|___/
        /////////////////////////////////////////////////             _       _   _

        //********************************************************************************
        // Existin relations
        //********************************************************************************
        let buildRelationsTable (relationDirection:Server.Relation.RelationDirection) =
          let targetString = sprintf "%A" relationDirection
          let renderCycleVar = Var.Create First
          let relationsCollection =
              ListModel.Create
                  (fun (r:Specs.RelationSpec,_targetName:string) -> r.relationId)
                  []
          // We create a Var holding an Option of Var
          // The outer Var is so that we can updated to a None or Some _ to toggle form display
          // The inner Var holds the RelationSpec from which we derive lenses that we linke to the form fields.
          // Define it here so it can be called from the table rows for relation edition
          let editedRelationVar:Var<Option<Var<Specs.RelationSpec>>> = Var.Create None
          // Define Var for rows so on ly rows are updated by DocSeqCached
          let tableRows =
            relationsCollection.View.DocSeqCached (fun (r:Specs.RelationSpec,targetName:string) ->
              tr
                []
                [
                  td
                    [classes [BS.``table-sm``]]
                    [ text targetName ]
                  td
                    [classes [BS.``table-sm``]]
                    [ text r.childToParentName ]
                  td
                    [classes [BS.``table-sm``]]
                    [ text r.parentToChildName ]
                  td
                    [classes [BS.``table-sm``]]
                    // Translate strings "One" and "Many"
                    [ text (r.childSideType|> sprintf "%A" |> t.t)]
                  td
                    [classes [BS.``table-sm``]]
                    // Translate strings "One" and "Many"
                    [ text (r.parentSideType|>  sprintf "%A" |> t.t) ]
                  td
                    [
                      classes [BS.``table-sm``]
                      attr.``data-`` "myowndb-action" "edit-relation";
                      on.click
                        (fun _e _ev ->
                          editedRelationVar.Set (Some (Var.Create r))
                        )
                    ]
                    // Translate strings "One" and "Many"
                    [ fontAwesomeIcon "edit"]
                ]

            )
          let relationForm =
            let rowClasses = classes [ BS.row;BS.``mb-3``;BS.``align-items-center``]
            let labelClasses = classes [BS.``col-sm-4``;BS.``col-form-label``]
            let fieldClasses = classes [BS.``form-control``]
            let wrapLabel l = div [classes [BS.``col-sm-4``]] [l]
            let wrapField l = div [classes [BS.``col-sm-8``]] [l]

            // Function returning the form for the RelationsSpec wrapped in the Var passed as argument.
            let formForRelation (r:Var<Specs.RelationSpec>) =
              // Define lenses on the records fields so these can be used in the form
              let targetVar =
                r.Lens
                  (fun rel ->
                    match relationDirection with
                    // If working on relations to parents, we are the child and it is the entity with the parentId that is the target
                    | Server.Relation.ToParents -> pageInfo.entitiesInDB |> List.filter (fun i -> i.id = rel.parentId) |> List.head
                    // If working on relations to children, we are the parent and it is the entity with the childId that is the target
                    | Server.Relation.ToChildren -> pageInfo.entitiesInDB|> List.filter (fun i -> i.id = rel.childId) |> List.head
                  )
                  (fun rel newFieldValue ->
                    match relationDirection with
                    // If working on relations to parents, update the parent id when the targetVar is updated
                    | Server.Relation.ToParents -> { rel with parentId = newFieldValue.id }
                    // If working on relations to children, update the child id when the targetVar is updated
                    | Server.Relation.ToChildren -> { rel with childId = newFieldValue.id }
                  )
              let childToParentNameVar= r.LensAuto (fun rel -> rel.childToParentName)
              let parentToChildNameVar = r.LensAuto (fun rel -> rel.parentToChildName)
              let parentSideTypeVar =
                r.LensAuto
                  (fun rel -> rel.parentSideType)
              let childSideTypeVar =
                r.LensAuto
                  (fun rel -> rel.childSideType)
              // The error Var specific to this relation form
              let relationErrorVar = Var.Create Doc.Empty
              //Helper to disable relation side type changes when we edit a relation that is "To many" as switching it to "To one"
              // requires special care (it should be impossible to switch to "To One" if there are instances that are "To many")
              let disableArityChangeIfNeeded (relVar:Var<Specs.RelationSpec>)(sideTypeVar:Var<Specs.RelationSideType>)=
                if (relVar.Value.relationId.IsSome && sideTypeVar.Value = Specs.RelationSideType.Many)
                then
                  attr.disabled "disabled"
                else
                  Attr.Empty
              // Cannot change target of an existing relation
              let disableTargetChangeIfEditing (relVar:Var<Specs.RelationSpec>)=
                if (relVar.Value.relationId.IsSome)
                then
                  attr.disabled "disabled"
                else
                  Attr.Empty
              let appendHelpForDisabledSidetypeChangeIfNeeded (relVar:Var<Specs.RelationSpec>)(sideTypeVar:Var<Specs.RelationSideType>)(doc:Doc)=
                if (relVar.Value.relationId.IsSome && sideTypeVar.Value = Specs.RelationSideType.Many) then
                  Doc.Append
                    doc
                    (helpIcon (t.t("It is not possible to edit the arity of a relation when it is accepting multiple targets.")))
                else
                  doc
              let appendHelpForDisabledTargetChangeInEdition (relVar:Var<Specs.RelationSpec>)(doc:Doc)=
                if (relVar.Value.relationId.IsSome) then
                  Doc.Append
                    doc
                    (helpIcon (t.t("It is not possible to edit the target of an existing relation.")))
                else
                  doc

              // Return the form Doc
              div
                [classes [BS.card];attr.id (sprintf "relationForm%s" targetString)]
                [
                  div
                    [classes [BS.``card-body``]]
                    [
                      // Target entity
                      div
                        [rowClasses]
                        [
                          label
                            [labelClasses;attr.``for`` "target"]
                            [
                              match relationDirection with
                              | Server.Relation.ToParents ->
                                t.tt("parent")
                              | Server.Relation.ToChildren ->
                                t.tt("child")
                            ]
                          |> wrapLabel
                          Doc.InputType.Select [attr.id "target";fieldClasses; disableTargetChangeIfEditing r] (fun (e:Entity.Entity) -> e.name) pageInfo.entitiesInDB targetVar
                          |> appendHelpForDisabledTargetChangeInEdition r
                          |> wrapField
                        ]
                      // Relation name from parent to child
                      div
                        [rowClasses]
                        [
                          label
                            [labelClasses;attr.``for`` "parentToChildName"]
                            [
                              (
                                match relationDirection with
                                | Server.Relation.ToParents ->
                                  // We are the child
                                  targetVar.View
                                  |> View.Map  (fun target ->
                                    t.tt("Name from %1 (parent) to %2", target.name, pageInfo.entity.name)
                                  )
                                | Server.Relation.ToChildren ->
                                  targetVar.View
                                  |> View.Map  (fun target ->
                                    t.tt("Name from %1 (parent) to %2", pageInfo.entity.name, target.name)
                                  )
                              ).V
                            ]
                          |> wrapLabel
                          Doc.InputType.Text [fieldClasses; attr.id "parentToChildName"] parentToChildNameVar
                          |> wrapField
                        ]
                      // Relation name from child to parent
                      div
                        [rowClasses]
                        [
                          label
                            [labelClasses;attr.``for`` "childToParentName"]
                            [
                              (
                                match relationDirection with
                                | Server.Relation.ToParents ->
                                  targetVar.View
                                  |> View.Map  (fun target ->
                                    t.tt("Name from %1 (child) to %2", pageInfo.entity.name, target.name)
                                  )
                                | Server.Relation.ToChildren ->
                                  targetVar.View
                                  |> View.Map  (fun target ->
                                    t.tt("Name from %1 (child) to %2", target.name, pageInfo.entity.name)
                                  )
                              ).V
                            ]
                          |> wrapLabel
                          Doc.InputType.Text [attr.id "childToParentName"; fieldClasses] childToParentNameVar
                          |> wrapField
                        ]
                      // As determining the labels for the relation side types can be cumbersome, here's a helper schema:
                      //
                      //
                      //                        parent side type: Can one employee (child,current) have multiple companies (parent, target)
                      //                      /
                      //                     /
                      //                    /
                      //         to parents
                      //        / (eg employee to company: employee is child and currently displayed, company is parent and target)
                      //       /            \
                      //      /              \
                      //     /                \
                      //    /                  \
                      //   .                    \ child side type: Can one company (parent, target) have multiple employees (child, current)
                      //    \
                      //     \
                      //      \
                      //       \                     parent side type: Can one employee (child, target) have multiple companies (parent,current)
                      //        \                  /
                      //         \                /
                      //          \              /
                      //           \ to children
                      //              (eg company to employee, company is parent and currently displayed, employee is child and target)
                      //                         \
                      //                          \
                      //                           \
                      //                             child side type: Can one company (parent, current) have multiple employees (child, target)
                      //
                      //

                      // Parent side type
                      div
                        [rowClasses]
                        [
                          label
                            [labelClasses;attr.``for`` "parentSideType"]
                            [
                               (match relationDirection with
                                | Server.Relation.ToParents ->
                                  targetVar.View
                                  |> View.Map  (fun target ->
                                    t.tt("Can one %1 (child) be linked to multiple %2 (parent)?", pageInfo.entity.name, target.name)
                                  )
                                | Server.Relation.ToChildren ->
                                  targetVar.View
                                  |> View.Map  (fun target ->
                                    t.tt("Can one %1 (child) be linked to multiple %2 (parent)?", target.name, pageInfo.entity.name)
                                  )
                               ).V
                            ]
                          |> wrapLabel

                          Doc.InputType.Select [attr.id "parentSideType"; fieldClasses; disableArityChangeIfNeeded r parentSideTypeVar;] (function |Specs.RelationSideType.Many -> t.t("Yes");|Specs.RelationSideType.One -> t.t("No")) [Specs.RelationSideType.Many;Specs.RelationSideType.One] parentSideTypeVar
                          |> appendHelpForDisabledSidetypeChangeIfNeeded r parentSideTypeVar
                          |> wrapField
                        ]
                      // Child side type
                      div
                        [rowClasses]
                        [
                          label
                            [labelClasses;attr.``for`` "childSideType"]
                            [
                               (match relationDirection with
                                | Server.Relation.ToParents ->
                                  targetVar.View
                                  |> View.Map  (fun target ->
                                    t.tt("Can one %1 (parent) be linked to multiple %2 (child)?", target.name, pageInfo.entity.name)
                                  )
                                | Server.Relation.ToChildren ->
                                  targetVar.View
                                  |> View.Map  (fun target ->
                                    t.tt("Can one %1 (parent) be linked to multiple %2 (child)?", pageInfo.entity.name, target.name)
                                  )
                               ).V
                            ]
                          |> wrapLabel

                          Doc.InputType.Select [attr.id "childSideType";fieldClasses; disableArityChangeIfNeeded r childSideTypeVar] (function |Specs.RelationSideType.Many -> t.t("Yes");|Specs.RelationSideType.One -> t.t("No")) [Specs.RelationSideType.Many;Specs.RelationSideType.One] childSideTypeVar
                          |>appendHelpForDisabledSidetypeChangeIfNeeded r childSideTypeVar
                          |> wrapField
                        ]
                      button
                        [
                          classes [BS.btn; BS.``btn-primary``]
                          on.click
                            (fun _el _ev ->
                              async {
                                  let! specResult =Server.Relation.updateRelation r.Value
                                  match specResult with
                                  | Ok [spec] ->
                                    relationErrorVar.Set Doc.Empty
                                    // Extract the target entity name as it is needed for the display of the list of relations
                                    let targetName =
                                      match relationDirection with
                                      | Server.Relation.ToParents ->
                                        pageInfo.entitiesInDB|> List.filter (fun i -> i.id = spec.parentId) |> List.map (fun i -> i.name) |> List.head
                                      | Server.Relation.ToChildren ->
                                        pageInfo.entitiesInDB|> List.filter (fun i -> i.id = spec.childId) |> List.map (fun i -> i.name) |> List.head
                                    // Add the pair to the collection, which will update the list
                                    relationsCollection.Add (spec,targetName)
                                    // Hide form
                                    editedRelationVar.Set None
                                  | Ok _ ->  relationErrorVar.Set (dangerBox (t.tt("An error occured")))
                                  | Error es -> relationErrorVar.Set (dangerBox (text (es |> Error.toString)))
                                } |> Async.Start
                          )
                        ]
                        [t.tt("Submit")]
                      relationErrorVar.V
                    ]



                ]
              // This is the View allowing us to hide the form simply by setting the editedRelationVar to None
            let relationFormView =
              editedRelationVar.View
              |> View.Map(fun specVarOption ->
                  match specVarOption with
                  | None -> Doc.Empty
                  | Some specVar -> formForRelation specVar

            )
            let shouldDisplayRelationFormView =
              editedRelationVar.View
              |> View.Map (fun o -> o.IsSome)
            let displayFormButtonText =
              shouldDisplayRelationFormView
              |> View.Map (fun b -> if b then t.tt("Hide form") else t.tt("Add relation") )
            let blankRelationSpec:Specs.RelationSpec =
              {
                relationId = None
                parentSideType = Specs.RelationSideType.Many
                childSideType =  Specs.RelationSideType.Many
                parentToChildName = ""
                childToParentName = ""
                parentId =
                  match relationDirection with
                  // We are the child so the target's id comes here. As initial target selection we take the first one
                  | Server.Relation.ToParents -> pageInfo.entitiesInDB[0].id
                  // We are the parent so our id comes here
                  | Server.Relation.ToChildren -> pageInfo.entity.id
                childId =
                  match relationDirection with
                  // We are the child so our id comes here
                  | Server.Relation.ToParents -> pageInfo.entity.id
                  // We are the parent so the target's id comes here. As initial target selection we take the first one
                  | Server.Relation.ToChildren -> pageInfo.entitiesInDB[0].id
              }

            div
              []
              [
                // Form
                button
                  [
                    classes [BS.btn;BS.``btn-primary``; ]
                    attr.id (sprintf "toggleRelationForm%s" targetString)
                    // Toggle the display of the form by setting the editedRelationVar to Some or None.
                    on.click (fun _el _ev -> if editedRelationVar.Value.IsSome then editedRelationVar.Set None else editedRelationVar.Set (Some(Var.Create blankRelationSpec)))
                  ]
                  [displayFormButtonText.V]
                relationFormView.V
              ]
          // Var containing Doc displaying the relations. It brings together the form and the table listing existing relations
          let tableVar = Var.Create(
            div
              []
              [
                relationForm
                table
                    [ classes [BS.table; BS.``table-bordered``; BS.``table-striped``; BS.``table-hover``; BS.``table-sm``; ];attr.id $"{targetString}Relations"]
                    [
                      thead
                        []
                        [
                          tr
                            []
                            [
                              // Related entity name
                              th
                                []
                                [
                                  match relationDirection with
                                  | Server.Relation.ToParents ->
                                    t.tt("parent name")
                                  | Server.Relation.ToChildren ->
                                    t.tt("child name")
                                ]
                              // Relation name from child to parent
                              th
                                []
                                [
                                  match relationDirection with
                                  | Server.Relation.ToParents ->
                                    // We are the child
                                    t.tt("name from %1 to parent",pageInfo.entity.name)
                                  | Server.Relation.ToChildren ->
                                    // We are the parent
                                    t.tt("name from child to %1",pageInfo.entity.name)
                                ]
                              // Relation name from parent to child
                              th
                                []
                                [
                                  match relationDirection with
                                  | Server.Relation.ToParents ->
                                    // We are the child
                                    t.tt("name from parent to %1",pageInfo.entity.name)
                                  | Server.Relation.ToChildren ->
                                    // We are the parent
                                    t.tt("name from %1 to child",pageInfo.entity.name)
                                ]
                              // Child side type
                              th
                                []
                                [
                                  match relationDirection with
                                  | Server.Relation.ToParents ->
                                    // We are the child
                                    t.tt("%1 side type",pageInfo.entity.name)
                                  | Server.Relation.ToChildren ->
                                    // We are the parent
                                    t.tt "child side type"
                                ]
                              // Parent side type
                              th
                                []
                                [
                                  match relationDirection with
                                  | Server.Relation.ToParents ->
                                    // We are the child
                                    t.tt "parent side type"
                                  | Server.Relation.ToChildren ->
                                    // We are the parent
                                    t.tt("%1 side type",pageInfo.entity.name)
                                ]
                            ]
                        ]
                      tbody
                        []
                        [
                          tableRows
                        ]
                    ]
              ]
          )

          // Function to be called to refresh relations list by calling RPC
          let refreshParentRelations (cycle:RenderCycle) = async {
            // Set the var used by the DocSeqCached mapping to <li>s so it know if it needs to flash elements
            renderCycleVar.Set cycle
            // Get list from the backend
            let! rpcResult = Server.Relation.getRelations entityId relationDirection
            match rpcResult with
            | Error es-> resultVar.Set (infobox InfoBoxKind.Danger (es|>Error.toString|>text))
            | Ok relationsSpecs ->
                relationsCollection.Set(relationsSpecs|>Seq.ofList)
                // Build the <li>s to put inside the <ul>
                // put the <li>s in the <ul>
          }

          tableVar,refreshParentRelations

        // Call function creating relations management elements
        let toParentsVar,refreshToParentRelations = buildRelationsTable Server.Relation.ToParents
        let toChildrenVar,refreshToChildrenRelations = buildRelationsTable Server.Relation.ToChildren
        // And trigger the RPC call for first display
        do! refreshToParentRelations First
        do! refreshToChildrenRelations First
        let datatypeSpecs:Specs.DataTypeSpec list = web.ClientHelpers.dataTypes |> List.map(fun dt -> dt.toSpec() )
        let initialiseDetailSpec() :Specs.EntityDetailSpec=
            {
              detailId = None
              name = ""
              dataType = datatypeSpecs[0]
              propositions = None
              status = DetailStatus.Active
            }
        // return the content of the page
        return
            div
                []
                [
                    h1
                      []
                      [text pageInfo.entity.name]
                    resultVar.V
                    ulVar.V
                  // ServerCall : Server.Entity.linkNewDetail entityId
                  // refreshList: refreshEntityDetails(Update)
                    web.admin.EntityDetail.addNewDetailSection initialiseDetailSpec (Server.Entity.linkNewDetail entityId ) (fun () -> async { do! refreshEntityDetails Update}) (t.tt("Add field based on new detail"))
                    linkExistingDetailSection
                    // Relations
                    h1 [] [t.tt("Relations to parents")]
                    toParentsVar.V
                    h1 [] [t.tt("Relations to children")]
                    toChildrenVar.V
                ]
    }
