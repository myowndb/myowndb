namespace web.admin
open web
open WebSharper
open WebSharper.Sitelets
open WebSharper.Sitelets.Router
open WebSharper.Sitelets.RouterOperators

[<JavaScript>]
module AdminRouting=
    // Admin pages endpoints. Those are defined at the root, but are embedded in the app's main Sitelet
    // under the prefix "/admin"
    type AdminEndPoint =
        |[<EndPoint "/users">] Users
        |[<EndPoint "/databases">] Databases
        |[<EndPoint "/database">] Database of databaseId:int
        |[<EndPoint "/entity">] Entity of entityId:int
        |[<EndPoint "/user">]User of id:int
    // Router inferred from endpoints. It is used to define the Sitelet for the admin pages
    let router : WebSharper.Sitelets.Router<AdminEndPoint> =
        WebSharper.Sitelets.InferRouter.Router.Infer ()
    // This router can be used to generate admin routes including the "/admin" prefix
    let adminRouter: WebSharper.Sitelets.Router<AdminEndPoint>  =
        r "admin" / WebSharper.Sitelets.InferRouter.Router.Infer ()
