namespace web.admin

open WebSharper.UI.Html
open WebSharper
open WebSharper.UI
open WebSharper.UI.Client
open web.ClientI18n
open web.HtmlHelpers
open web.WSExtensions
open web.Css

[<JavaScript>]
module DatabaseDisplay =

    // This is called by the ClientContent.clientBody() to render the body of the page
    // according to the client-side router state.
    let pageBody (databaseId:int) (pageInfo:web.PageInfo.Admin.Database.PageInfo)= async {
        let t = JStr()


//     _____       _   _ _   _
//    | ____|_ __ | |_(_) |_(_) ___  ___
//    |  _| | '_ \| __| | __| |/ _ \/ __|
//    | |___| | | | |_| | |_| |  __/\__ \
//    |_____|_| |_|\__|_|\__|_|\___||___/



        let! entities = Server.Database.getEntities databaseId
        let entitiesCollection =
            ListModel.Create
                (fun (e:Specs.EntitySpec) -> e.entityId)
                []
        let entitiesResultVar = Var.Create Doc.Empty
        match entities with
        | Ok l ->
            entitiesCollection.Set (l|> Seq.ofList)
            entitiesResultVar.Set Doc.Empty
        | Error es ->
            entitiesCollection.Set [||]
            entitiesResultVar.Set(
                infobox
                    (InfoBoxKind.Danger)
                    (t.tt("An error occured"))
            )
        // If the editedEntity Var holds Some entity, we will display the edition for for that entity
        let editedEntity:Var<Option<Specs.EntitySpec>> = Var.Create None

        let editionFormForEntity (entity:Specs.EntitySpec) =
            // Put the user in a var and create lenses to easily link it to form fields
            let e = Var.Create entity
            let name =
                e.LensAuto
                    (fun u -> u.name)
            let hasPublicForm =
                e.LensAuto
                    (fun u -> u.hasPublicForm)
            form
                [ attr.id "admin_entities_form"]
                [
                    div
                      []
                      [
                        label [attr.``for`` "entityEditionName"] [t.tt("Name")]
                        Doc.InputType.Text [attr.id "entityEditionName"] name
                      ]
                    div
                      []
                      [
                        label [ attr.``for`` "entityEditionPublicForm";  classes [] ] [t.tt("Has public form")]
                        Doc.InputType.CheckBox [attr.id "entityEditionPublicForm"] hasPublicForm
                      ]

                    Doc.Button
                        (t.t("Save"))
                        [classes [BS.btn; BS.``btn-primary``]]
                        (fun () ->
                           async {
                                    let! r =
                                        Server.Database.createOrUpdateEntity databaseId e.Value
                                    match r with
                                    | Ok [e] ->
                                        // We get the updated user back, inject it in the users collection used to display the table
                                        // Add updates an existing entry
                                        entitiesCollection.Add e
                                        // Notify of the success
                                        entitiesResultVar.Set (infobox InfoBoxKind.SuccessBox (t.tt "Entity saved successfully"))
                                        // Get out of user edition, hiding the form
                                        editedEntity.Set None
                                    // This Ok l should not happen as single row  result is ensured server side
                                    | Ok _ ->
                                        entitiesResultVar.Set (infobox InfoBoxKind.Danger (t.tt("An error occured")))
                                    | Error es ->
                                        entitiesResultVar.Set (infobox InfoBoxKind.Danger (text (es|>Error.toString)))
                            }
                            |> Async.Start
                        )
                    Doc.Button
                        (t.t("Cancel"))
                        [classes [BS.btn; BS.``btn-secondary``]]
                        // Cancelling is simply going to a state where no user edition is done
                        (fun () -> editedEntity.Set None)
                ]


        let editionformView () =
            editedEntity.View
            |> View.Map (fun eo ->
                match eo with
                | None -> Doc.Empty
                | Some e -> editionFormForEntity e
            )

        let setPublicForm(spec:Specs.EntitySpec) =
          async {
            let! r = Server.Database.createOrUpdateEntity databaseId spec
            match r with
            | Ok [e] ->
              entitiesCollection.Add e
              entitiesResultVar.Set(successBox (t.tt("Saved successfully")))
            | _ ->
              entitiesResultVar.Set(dangerBox (t.tt("An error occured")))
          } |> Async.Start

        let entitiesTable =
            entitiesCollection.View
            |> View.Map
                (fun l ->
                    table
                        [ classes [BS.table; BS.``table-bordered``; BS.``table-striped``; BS.``table-hover``; BS.``table-sm``; ];attr.id "admin_entities_table" ]
                        [
                            thead []
                                  [
                                    tr []
                                       [
                                            th []
                                               [
                                                    div [classes [AppCss.``th-inner``]]  [t.tt("Name")]
                                               ]

                                       ]

                                    ]

                            tbody []
                                  [
                                        l
                                        |> Seq.map (fun e ->
                                            tr
                                                []
                                                [
                                                    td [classes [BS.``table-sm``]] [text e.name]
                                                    td
                                                      [classes [BS.``table-sm``;"myowndbactioncell";]]
                                                      [
                                                        a
                                                          [
                                                            attr.``data-`` "myowndb-action" "togglePublicForm";
                                                            on.click  (fun _el ev -> ev.PreventDefault(); setPublicForm {e with hasPublicForm = not e.hasPublicForm})]
                                                          [
                                                            fontAwesomeIconWithClasses [BS.``ms-1``] $"""toggle-{if e.hasPublicForm then "on" else "off"}"""
                                                          ]
                                                        if e.hasPublicForm then
                                                          let publicFormRelativeLink = web.Routing.router.Link (web.EndPoint.PublicForm  ((e.entityId |> Option.get |> Ids.EntityId.Get),None))
                                                          let publicFormLink = JavaScript.URL(publicFormRelativeLink, JavaScript.JS.Document.BaseURI).Href
                                                          let documentationUrlOption= pageInfo.publicFormDocumentationUrl
                                                          span
                                                            []
                                                            [
                                                              t.tt("Has public form")
                                                              helpIcon
                                                                  (t.t("The public form can be embedded in you website using an IFRAME. Add this to your html: <iframe src=\"%1\"></iframe>. To make it resize to fit your page automatically, follow the documention link.", publicFormLink))
                                                              text "("
                                                              a [ attr.href publicFormLink; attr.target "_blank"] [text publicFormLink]
                                                              // If a documentation URL was set in the config, display it here
                                                              documentationUrlOption
                                                              |> Option.map (fun url ->

                                                                [
                                                                  text " - "
                                                                  a [ attr.href url; attr.target "_blank" ] [t.tt("Documentation")]
                                                                ]
                                                                |> Doc.Concat
                                                              )
                                                              |> Option.defaultValue (Doc.Empty)
                                                              text ")"

                                                            ]
                                                        else
                                                          t.tt("Has no public form");
                                                      ]
                                                    td [classes [BS.``table-sm``;"myowndbactioncell";]
                                                        on.click (fun ev el ->
                                                            // Remove previous feedback
                                                            entitiesResultVar.Set Doc.Empty
                                                            // Trigger form display
                                                            editedEntity.Set (Some e)
                                                            // The form is at the top of the page, scroll to it
                                                            JavaScript.JS.Window.ScrollTo(0,0)
                                                            )
                                                        ] [ a [attr.``data-`` "myowndb-action" "edit";] [li [ classes ["fas"; "fa-edit"; BS.``me-5``;"myowndb-action-cell"]] [] ]]
                                                    td [classes [BS.``table-sm``;"myowndbactioncell";] ]
                                                       [ a [attr.``data-`` "myowndb-action" "view"; attr.href (AdminRouting.adminRouter.Link (AdminRouting.AdminEndPoint.Entity (e.entityId|> fun (eid) -> eid |> Option.map Ids.EntityId.Get|> Option.get)))] [li [ classes ["fas"; "fa-search"; BS.``me-5``;"myowndb-action-cell"]] [] ]]
                                                ]
                                        )
                                        |> Doc.Concat

                                  ]
                        ]
            )


//       ____       _        _ _
//      |  _ \  ___| |_ __ _(_) |___
//      | | | |/ _ \ __/ _` | | / __|
//      | |_| |  __/ || (_| | | \__ \
//      |____/ \___|\__\__,_|_|_|___/


        let detailsCollection =
            ListModel.Create
                // The entitiesNames is a string listing entities using the detail of which the spec is
                // the second element of the pair
                (fun (_entitiesNames:string,d:Specs.DetailSpec) -> d.detailId)
                []
        let detailsResultVar = Var.Create Doc.Empty
        let refreshDetailsList () = async {
          let! details = Server.Database.getDetailSpecs databaseId
          match details with
          | Ok l ->
              detailsCollection.Set (l|> Seq.ofList)
              detailsResultVar.Set Doc.Empty
          | Error _es ->
              detailsCollection.Set [||]
              detailsResultVar.Set(
                  infobox
                      (InfoBoxKind.Danger)
                      (t.tt("An error occured"))
              )
        }
        do! refreshDetailsList()

        // Function displaying the table cell content for the name column.
        let displayEditableName (entitiesNames:string)(d:Specs.DetailSpec) =
          let step = Var.Create 0
          step.View
          |> View.Map (fun n ->
            match n with
            // Step 0: display the name and an edit icon
            | 0 ->
                span
                  []
                  [
                    text d.name
                    span
                      [
                        on.click (fun _el _ev -> step.Set 1)
                        attr.``data-`` "myowndb-action" "edit"
                      ]
                      [
                        fontAwesomeIcon "edit"
                      ]
                  ]
            // Step 1: when the edit icon is clicked, we get to step 1 for edition.
            | 1 ->
              let nameVar = Var.Create d.name
              Doc.InputType.Text
                [
                  classes ["myowndb-detail-name-edition"]
                  focusAfterRender()
                  on.keyUp
                    (fun el kev ->
                      async {
                        match kev.KeyCode with
                        // Enter: submit
                        | 13 ->
                          let newSpec = {d with name = nameVar.Value}
                          let! res = Server.Database.updateDetail newSpec
                          match res with
                          | Ok s -> detailsCollection.Add (entitiesNames,s|>List.head)
                          | Error es -> detailsResultVar.Set (dangerBox (es|>Error.toString|>text))
                        // Escape
                        | 27 ->
                          step.Set 0
                        | _ -> ()
                      } |> Async.Start
                      )
                  ]
                nameVar
              |> Doc.Prepend (helpIcon (t.t("Enter to submit, Escape to cancel")))
            | _ ->
              step.Set 0
              Doc.Empty
          )
        let displayEditablePropositions (entitiesNames:string)(detail:Specs.DetailSpec) =
          let step = Var.Create 0
          step.View
          |> View.Map (fun n ->
            match n with
            // Step 0: display propositions
            | 0 ->
              span
                [
                ]
                [
                  text
                      (detail.propositions
                        |> Option.map (List.map (fun (p:Specs.DetailValuePropositionItemSpec) -> p.value))
                        |> Option.map (String.concat ",")
                        |> Option.defaultValue "" )
                  (
                    if detail.dataType.name = "choose in list" then
                      span
                        [
                          attr.``data-`` "myowndb-action" "edit"
                          on.click (fun _el _ev -> step.Set 1)
                        ]
                        [
                          fontAwesomeIcon "edit"
                        ]
                      else
                        Doc.Empty
                  )
                ]
             // Step 1: edition of the propositions
             | 1 ->
               // Define the model which will hold the propositions
               let propositionsModel =
                  ListModel.Create<Ids.DetailValuePropositionId option,Specs.DetailValuePropositionItemSpec>
                    (fun p -> p.detailValuePropositionId )
                    []
               // Initialise the model with the propositions from the spec
               propositionsModel.Set (detail.propositions|> Option.get )
               let lis =
                 // Using DocLens gives us a Var we can use to modify its value with a form field eg
                 propositionsModel.DocLens
                   (fun _k vp ->
                     li
                      []
                      [
                        Doc.InputType.TextV [] vp.V.value
                      ]
                   )
               // Content of the table cell
               span
                []
                [
                   ul
                     []
                     [ lis ]
                   span
                    [
                      on.click
                        (fun _el _ev ->
                          async {
                            // Filter out values that are empty
                            let newSpec = {detail with propositions = Some (propositionsModel.Value|>List.ofSeq|> List.filter(fun p -> p.value<>""))}
                            let! res = Server.Database.updateDetail newSpec
                            match res with
                            | Ok s ->
                              detailsCollection.Add (entitiesNames,s|>List.head)
                              step.Set 0
                            | Error es ->
                              detailsResultVar.Set (dangerBox (es|>Error.toString|>text))
                          } |> Async.Start
                        )
                      attr.``data-`` "myowndb-action" "apply-propositions-changes"
                    ]
                    [
                      fontAwesomeIcon "check"
                    ]
                   span
                    [ on.click (fun _el _ev -> step.Set 0)
                      attr.``data-`` "myowndb-action" "cancel-propositions-changes"
                    ]
                    [
                      fontAwesomeIcon "times"
                    ]
                   span
                    [ on.click (fun _el _ev ->
                      match propositionsModel.Value |> Seq.tryFind (fun p -> p.detailValuePropositionId.IsNone && p.value="") with
                      // No new empty proposition, add a new one
                      | None ->
                        let newProposition = Specs.DetailValuePropositionItemSpec.Init()
                        propositionsModel.Add newProposition
                      // There is already a new empty proposition, do not add another one
                      | Some _ -> ()
                      )
                      attr.``data-`` "myowndb-action" "add-proposition"
                    ]
                    [
                      fontAwesomeIcon "plus"
                    ]
                ]
             | _ -> Doc.Empty
          )


        let detailsTableRows =
          detailsCollection.View.DocSeqCached (fun (entitiesNames:string,d:Specs.DetailSpec) ->
            tr
              []
              [
                td
                  [classes [BS.``table-sm``]]
                  [
                    (displayEditableName entitiesNames d).V
                  ]
                td
                  [classes [BS.``table-sm``; "myowndb-detail-datatype"]]
                  [ t.tt(d.dataType.name) ]
                td
                  [classes [BS.``table-sm``;"myowndb-detail-propositions"]]
                  [
                    (displayEditablePropositions entitiesNames d).V
                  ]
                td
                  [classes [BS.``table-sm``; "myowndb-detail-entities"]]
                  [ text entitiesNames ]
                td
                  [classes [BS.``table-sm``]]
                  // Translate strings "Active" and "Inactive"
                  [ text (d.status|> sprintf "%A" |> t.t)]
              ]

          )

        let detailsTableVar = Var.Create(
          div
            []
            [
              table
                  [ classes [BS.table; BS.``table-bordered``; BS.``table-striped``; BS.``table-hover``; BS.``table-sm``; ];attr.id "DetailsTable"]
                  [
                    thead
                      []
                      [
                        tr
                          []
                          [
                            th
                              []
                              [
                                  t.tt("Name")
                              ]
                            th
                              []
                              [
                                  t.tt("Datatype")
                              ]
                            th
                              []
                              [
                                t.tt("Value propositions")
                              ]
                            th
                              []
                              [
                                t.tt("Used in entities")
                              ]
                            th
                              []
                              [
                                t.tt("Status")
                              ]
                          ]
                      ]
                    tbody
                      []
                      [
                        detailsTableRows
                      ]
                  ]
            ]
          )

        let datatypeSpecs:Specs.DataTypeSpec list = web.ClientHelpers.dataTypes |> List.map(fun dt -> dt.toSpec() )
        let initialiseDetailSpec() :Specs.DetailSpec=
            {
              detailId = None
              name = ""
              dataType = datatypeSpecs[0]
              propositions = None
              status = DetailStatus.Active
              databaseId = Ids.DatabaseId databaseId
            }


        return div
                []
                [
                    div
                      [ classes ["myowndb-entities-section"]]
                      [
                        h1 [] [t.tt "Entities administration"]
                        Doc.Button
                            (t.t("Add new"))
                            [classes [BS.btn; BS.``btn-primary``]]
                            (fun () ->
                                entitiesResultVar.Set Doc.Empty
                                editedEntity.Set (Some(Specs.EntitySpec.Init())))
                        editionformView().V
                        entitiesResultVar.V
                        entitiesTable.V
                      ]
                    div
                      [ classes ["myowndb-details-section"]]
                      [
                        h1 [] [t.tt("Details")]
                        detailsResultVar.V
                        web.admin.EntityDetail.addNewDetailSection (initialiseDetailSpec)(Server.Database.createNewDetail) (refreshDetailsList) (t.tt("Add new detail"))
                        detailsTableVar.V
                      ]
                ]
    }
