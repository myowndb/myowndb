namespace web.admin

open WebSharper
open Microsoft.AspNetCore.Localization
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Identity;
open Microsoft.AspNetCore.Authorization;
open WebSharper.Sitelets
open WebSharper.Sitelets.InferRouter
open FSharpPlus
open Helpers
open FsToolkit.ErrorHandling
open web.ServerHelpers
open web.ServerHelpers.Remoting
open ServerFileAttachmentExtensions
open Ids

module Server =
    open web
    // This helper will check the user has admin rights, and in that case run the DBAction passed in,
    // logging possible errors
    let inline adminActionBase<'T>(dbaction:DBAction.DBAction<'T>) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let client:DB.DBClient = getService<DB.DBClient>()
        let t:web.I18n = getService<web.I18n>()
        let accountId = Auth.getCurrentUserAccountId httpContext
        // Update the account id of the user so it is set when insertion sql query is executed
        if web.Authorization.isAdmin httpContext then
            // open connection if needed as we want to wrap in a transaction
            let! closeIfNeeded = client.OpenIfNeeded()
            let! res =
                dbaction
                // Wrap in a transaction because the update of display_order is done with 2 queries that should be in a tx
                |> DBAction.wrapInTransactionIfNeeded
                |> DBAction.run client
                |> Async.map (DBResult.mapError(fun e ->
                        MyOwnDBLib.Logging.Log.Error($"""Error working on {typeof<'T>} for a account {accountId} by admin: {e |> Error.toString}""")
                        Error.fromString (t.t("An error occured"))
                ))
            // Close the connection if we opened it earlier
            do! closeIfNeeded()
            return res
        else
            MyOwnDBLib.Logging.Log.Warning($"problem creating {typeof<'T>} for admin for account id {accountId} by non-admin user")
            return DBResult.error "not admin"
    }
    module Users =
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let getUsers () : Async<DBResult.DBResult<User.User>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let accountId = Auth.getCurrentUserAccountId httpContext
            return!
                UserExtensions.getAllForAccountAction accountId
                |> adminActionBase
        }

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let updateUser (u:User.User) : Async<DBResult.DBResult<User.User>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let t:web.I18n = getService<web.I18n>()
            let client:DB.DBClient = getService<DB.DBClient>()
            let accountId = Auth.getCurrentUserAccountId httpContext
            if web.Authorization.isAdmin httpContext then
                let currentUserId =Auth.getCurrentUserId httpContext
                // Current admin is trying to transition to standard user. We don't allow that, another admin should do it.
                // This protects to accidental removal of admins for self, and also ensures at least one admin exists.
                let checkSelfAdminRemoval =
                    if u.id = (UserId currentUserId) && u.user_type_id <> 1 then
                        // Update self removing admin, we don't allow that
                        DBAction.error (t.t("You cannot remove the admin right for your own profile. Ask another admin to do it if that's really what you want."))
                    else
                        // Update of self but without removing admin profile
                        DBAction.retn true
                let! r =
                    checkSelfAdminRemoval
                    |> DBAction.bind (fun _b ->
                        DBAction.queryAction
                            "update users set firstname=%s, lastname=%s, email=%s,user_type_id=%d where id=%d and account_id=%d returning *"
                            (u.firstname|> Option.defaultValue "")
                            (u.lastname|> Option.defaultValue "")
                            u.email
                            u.user_type_id
                            // I can't call UserId.Get here, don't know why
                            (u.id|> UserId.Get)
                            accountId
                    )
                    |> DBAction.ensureSingleRow
                    |> DBAction.run client
                    |> Async.map (DBResult.mapError (fun e ->
                        MyOwnDBLib.Logging.Log.Error($"problem updating user {u} by admin of account {accountId}: {e|>Error.toString}")
                        e
                    ))
                return r
            else
                MyOwnDBLib.Logging.Log.Warning($"problem updating user {u} for admin for account id {accountId} by non-admin user")
                return DBResult.error "not admin"
        }

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let createUser (uIn:User.User) : Async<DBResult.DBResult<User.User>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let client:DB.DBClient = getService<DB.DBClient>()
            let t:web.I18n = getService<web.I18n>()
            let userManager = getService<UserManager<ApplicationUser>>()
            let accountId = Auth.getCurrentUserAccountId httpContext
            // Update the account id of the user so it is set when insertion sql query is executed
            let u = {uIn with account_id = AccountId accountId}
            if web.Authorization.isAdmin httpContext then
                // Generate long random email for user creation, but trigger a password change immediately after
                let pass = $"{UserExtensions.randomString 50}1A#"
                let appUser = ApplicationUser.FromUser(u)

                try
                    let! creationResult = userManager.CreateAsync(appUser, pass) |> Async.AwaitTask
                    if creationResult.Succeeded then
                        let! token = (userManager.GenerateEmailConfirmationTokenAsync(appUser)|> Async.AwaitTask)
                        let! _ = userManager.ConfirmEmailAsync(appUser,token) |> Async.AwaitTask
                        do! Server.resetPassword u.email
                        return DBResult.retn {u with id = (UserId (appUser.Id))}
                    else
                        MyOwnDBLib.Logging.Log.Error($"""Error creating user by admin: {creationResult.Errors|> Seq.map (fun e -> e.Code)|> String.concat " "}""")
                        let errorCodes = creationResult.Errors |> Seq.map (fun e -> e.Code)
                        if Seq.contains "DuplicateUserName" errorCodes then
                            return DBResult.error (t.t("The email {0} is already registered.",u.email))
                        else
                            return DBResult.error (t.t("An error occured"))

                with
                | e ->
                    MyOwnDBLib.Logging.Log.Error($"Error creating user by admin: {e}")
                    return DBResult.error (t.t("An error occured"))
            else
                MyOwnDBLib.Logging.Log.Warning($"problem creating user {u} for admin for account id {accountId} by non-admin user")
                return DBResult.error "not admin"
        }

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let deleteUser (u:User.User) : Async<DBResult.DBResult<unit>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let client:DB.DBClient = getService<DB.DBClient>()
            let t:web.I18n = getService<web.I18n>()
            let userManager = getService<UserManager<ApplicationUser>>()
            let accountId = Auth.getCurrentUserAccountId httpContext
            if web.Authorization.isAdmin httpContext then
                let currentUserId =Auth.getCurrentUserId httpContext
                if u.id = (UserId currentUserId) then
                    // We don't allow an admin users to delete their own user. This also avoids the risk to have an account without
                    // admin users
                    return DBResult.DBResult.Error (t.t("You cannot delete your own account as an admin.")|>Error.fromString)
                else
                    let appUser= ApplicationUser.FromUser u
                    let! result = userManager.DeleteAsync(appUser) |> Async.AwaitTask
                    if result.Succeeded then
                        return DBResult.retn ()
                    else
                        MyOwnDBLib.Logging.Log.Error($"problem deleting user {u} by admin of account {accountId}: {result.Errors}")
                        return DBResult.DBResult.Error (t.t("An error occured")|>Error.fromString)
            else
                MyOwnDBLib.Logging.Log.Warning($"problem deleting user {u} for admin for account id {accountId} by non-admin user")
                return DBResult.error "not admin"
        }

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let resetPassword (u:User.User) : Async<DBResult.DBResult<unit>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let t:web.I18n = getService<web.I18n>()
            let userManager = getService<UserManager<ApplicationUser>>()
            let accountId = Auth.getCurrentUserAccountId httpContext
            // Update the account id of the user so it is set when insertion sql query is executed
            if web.Authorization.isAdmin httpContext then
                let appUser= ApplicationUser.FromUser u
                try
                    // As we force a password reset, we change the current password to an long random string
                    let! result = userManager.RemovePasswordAsync(appUser) |> Async.AwaitTask
                    if result.Succeeded then
                        // Then we trigger the password reset procedure
                        do! Server.resetPassword u.email
                        return DBResult.retn ()
                    else
                        MyOwnDBLib.Logging.Log.Error("Error resetting password for user by admin: {result.Errors}")
                        return DBResult.error (t.t("An error occured"))

                with
                | e ->
                    MyOwnDBLib.Logging.Log.Error("Error resetting password for user by admin: {e}")
                    return DBResult.error (t.t("An error occured"))
            else
                MyOwnDBLib.Logging.Log.Warning($"problem resetting password for user {u} for admin for account id {accountId} by non-admin user")
                return DBResult.error "not admin"
        }
    module Databases =
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let getDatabases () : Async<DBResult.DBResult<Specs.DatabaseSpec>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let accountId = Auth.getCurrentUserAccountId httpContext
            return!
                Admin.Database.getAllForAccountAction accountId
                |> adminActionBase
        }

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let createOrUpdateDatabase (dbIn:Specs.DatabaseSpec) : Async<DBResult.DBResult<Specs.DatabaseSpec>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let accountId = Auth.getCurrentUserAccountId httpContext
            // Update the account id of the user so it is set when insertion sql query is executed
            let db = {dbIn with accountId = AccountId accountId}
            return!
                Admin.Database.createOrUpdateActionFromSpec db
                |> adminActionBase
        }

    module Database =
        let validateDatabaseIdAction (e:Specs.EntitySpec) (accountId:int) =
            DBAction.queryAction "select count(*) from databases db join accounts acc on (db.account_id=acc.id) where db.id=%d and acc.id=%d" (e.databaseId|>DatabaseId.Get) accountId
            |> DBAction.bind (fun count ->
                if count > 0L then
                    //DBResult.DBResult.Ok [()]
                    DBAction.retn e
                else
                    MyOwnDBLib.Logging.Log.Warning($"Inconsistent database id {e.databaseId} and account id (got {e.databaseId} but should be {accountId}) for entity creation by admin")
                    //DBResult.DBResult.Error (Error.fromString "Access denied")
                    DBAction.error "Access denied"
            )
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let getEntities (databaseId:int) : Async<DBResult.DBResult<Specs.EntitySpec>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let accountId = Auth.getCurrentUserAccountId httpContext
            return!
                (Admin.Entity.getAllForAccountAndDatabaseAction accountId databaseId)
                |> adminActionBase<Specs.EntitySpec>

        }

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let createOrUpdateEntity (databaseIdInt:int)(e:Specs.EntitySpec) : Async<DBResult.DBResult<Specs.EntitySpec>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let client:DB.DBClient = getService<DB.DBClient>()
            let t:web.I18n = getService<web.I18n>()
            let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
            let accountId = Auth.getCurrentUserAccountId httpContext
            // When the record is initialised, it has the database id 0. We update it here.
            let! databaseIdOption = (GetId(signInManager).DatabaseId client (httpContext.User) databaseIdInt)
            // If we have access to the database id, wrap it in a DBAction, otherwise return a DBAction error skipping all subsequent actions
            return!
              databaseIdOption
              // If we get a None we transform it in an error, skipping all subsequent operations
              |> DBAction.fromOption
              // Log the error and replace it with a translated error for the user
              |> DBAction.mapError (fun _e ->
                  MyOwnDBLib.Logging.Log.Error($"Problem accessing Database id {databaseIdInt} for account {accountId} in createOrUpdateEntity")
                  t.t("An error occured") |> Error.fromString
              )
              // Now we can use the databaseId if it was retrieved
              |> DBAction.map (fun databaseId -> {e with databaseId= databaseId})
              // Check consistency of database_id and account_id (check user adds an entity to a database it has access to)
              |> DBAction.bind (fun e ->
                validateDatabaseIdAction e accountId
              )
              // If working on correct database, do the insert
              |> DBAction.bind (fun e ->
                  Admin.Entity.createOrUpdateActionFromSpec e
              )
              // Check admin access and run the action if all is ok
              |> adminActionBase<Specs.EntitySpec>
        }

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        // Returns a list of pairs (entities names using the detail, detail spec)
        let getDetailSpecs (databaseIdInt:int) : Async<DBResult.DBResult<string*Specs.DetailSpec>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let accountId = Auth.getCurrentUserAccountId httpContext
            let client:DB.DBClient = getService<DB.DBClient>()
            let t:web.I18n = getService<web.I18n>()
            let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
            let! databaseIdOption = (GetId(signInManager).DatabaseId client (httpContext.User) databaseIdInt)
            return!
                databaseIdOption
                // If we get a None we transform it in an error, skipping all subsequent operations
                |> DBAction.fromOption
                // Log the error and replace it with a translated error for the user
                |> DBAction.mapError (fun _e ->
                    MyOwnDBLib.Logging.Log.Error($"Problem accessing Database id {databaseIdInt} for account {accountId} in getDetailSpecs")
                    t.t("An error occured") |> Error.fromString
                )
                // Now we can use the databaseId if it was retrieved
                |> DBAction.bind (fun databaseId ->
                    DetailExtensions.getDetailSpecsForDatabaseAction (AccountId accountId) databaseId
                    |> DBAction.map (fun spec -> databaseId,spec)
                )
                |> DBAction.map (fun (databaseId, spec) ->
                    (databaseId, {spec with dataType = { spec.dataType with name = (spec.dataType.name |> ServerHelpers.Utils.humaniseDataTypeName) }})
                )

                |> DBAction.bind (fun (databaseId,spec) ->
                  let dbId = (databaseId |> DatabaseId.Get)
                  // Get list of entities using this detail
                  DBAction.queryAction "select e.name from entities e join entities2details ed on (ed.entity_id=e.id) join details d on (d.id=ed.detail_id) where d.id=%d and d.database_id=%d and e.database_id=%d order by name" (spec.detailId |> Option.get |> DetailId.Get) dbId dbId
                  |> DBAction.workOnList
                  |> DBAction.map (String.concat ",")
                  |> DBAction.map (fun entitiesNames -> entitiesNames,spec)
                )
                |> adminActionBase<string*Specs.DetailSpec>

        }

        // DBAction to validate access to the database of the detail by the account with accountId
        let checkDatabaseAccessAction (accountId:int) (detail:Specs.DetailSpec) =
            let t:web.I18n = getService<web.I18n>()
            DBAction.querySingleAction "select count(*) from databases where id=%d and account_id=%d" (detail.databaseId|> DatabaseId.Get) accountId
            |> DBAction.bind (fun l ->
              if l <> 1L then
                DBAction.error (t.t("An error occured"))
              else
                DBAction.retn ()
            )

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let createNewDetail (detail:Specs.DetailSpec) = async {
          let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
          let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
          let t:web.I18n = getService<web.I18n>()
          let accountId = Auth.getCurrentUserAccountId httpContext
          return!
            checkDatabaseAccessAction accountId detail
            |> DBAction.bind (fun _ ->
              Admin.Detail.createOrUpdateActionFromSpec ({detail with propositions = detail.propositions|>Option.map List.rev})
              // Needed to be able to use the generic function web.admin.EntityDetail.addNewDetailSection
              |> DBAction.map (fun _ -> ())
            )
            |> adminActionBase
        }

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let updateDetail (detail:Specs.DetailSpec) = async {
          let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
          let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
          let t:web.I18n = getService<web.I18n>()
          let accountId = Auth.getCurrentUserAccountId httpContext
          return!
            checkDatabaseAccessAction accountId detail
            |> DBAction.bind (fun _ ->
              Admin.Detail.createOrUpdateActionFromSpec detail
              // Needed to be able to use the generic function web.admin.EntityDetail.addNewDetailSection
            )
            |> adminActionBase
        }

    module Entity =
        // Check account id of user is ok regarding entities2details id we get
        let validateAccountAccessToEntity2Detail (accountId:int)(entity2DetailId:int)=
          let t:web.I18n = getService<web.I18n>()
          DBAction.queryAction "select count(*) from entities2details e2d join details d on (d.id=e2d.detail_id) join databases db on (db.id=d.database_id) where db.account_id=%d and e2d.id=%d" accountId entity2DetailId
          |> DBAction.bind (fun count ->
              if count=0L then
                  MyOwnDBLib.Logging.Log.Warning($"Error accessing entities2details with id {entity2DetailId} by admin for account {accountId}")
                  DBAction.error (t.t("An error occured"))
              else
                  DBAction.retn ()
          )
        let lastDisplayOrderaction (entityId:EntityId)=
            DBAction.querySingleAction "select coalesce(max(display_order),10) from entities2details where entity_id=%d" (entityId|>Ids.EntityId.Get)
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let getEntityDetails (entityId:int) : Async<DBResult.DBResult<Entity.EntityDetail>>=
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let accountId = Auth.getCurrentUserAccountId httpContext
            Admin.Entity.getEntityDetailsAction accountId entityId
            |>adminActionBase<Entity.EntityDetail>
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let setListVisibility (entity2DetailId:int) (visible:bool): Async<DBResult.DBResult<Entity.EntityDetail>>=
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let t:web.I18n = getService<web.I18n>()
            let accountId = Auth.getCurrentUserAccountId httpContext
            // Check account id of user is ok regarding entities2details id we get
            validateAccountAccessToEntity2Detail accountId entity2DetailId
            // If ok, update data
            |> DBAction.bind (fun () ->
                DBAction.statementAction "update entities2details set displayed_in_list_view=%b where entities2details.id=%d" visible entity2DetailId
            )
            |> DBAction.bind (fun impactedRows ->
                match impactedRows with
                | DBTypes.ImpactedRows 1 ->
                    Admin.Entity.getEntityDetailAction accountId entity2DetailId
                | DBTypes.ImpactedRows n ->
                    MyOwnDBLib.Logging.Log.Error($"Update of individual entities2details resulted in {n} rows update! For account {accountId} and entities2details {entity2DetailId}")
                    DBAction.error (t.t("An error occured"))
            )
            // wrap this is admin access check
            |>adminActionBase<Entity.EntityDetail>
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let UnlinkDetail (entity2DetailId:int) : Async<DBResult.DBResult<Entity.EntityDetail>>=
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let t:web.I18n = getService<web.I18n>()
            let accountId = Auth.getCurrentUserAccountId httpContext
            // Check account id of user is ok regarding entities2details id we get
            validateAccountAccessToEntity2Detail accountId entity2DetailId
            // If ok, update data
            |> DBAction.bind (fun () ->
                DBAction.statementAction "delete from entities2details where entities2details.id=%d" entity2DetailId
            )
            |> DBAction.bind (fun impactedRows ->
                match impactedRows with
                | DBTypes.ImpactedRows 1 ->
                    DBAction.zero
                | DBTypes.ImpactedRows n ->
                    MyOwnDBLib.Logging.Log.Error($"Deletion of individual entities2details resulted in {n} rows update! For account {accountId} and entities2details {entity2DetailId}")
                    DBAction.error (t.t("An error occured"))
            )
            // wrap this is admin access check
            |>adminActionBase<Entity.EntityDetail>
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let linkNewDetail (entityIdInt:int)(entityDetail:Specs.EntityDetailSpec) = async {
          let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
          let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
          let client:DB.DBClient = getService<DB.DBClient>()
          let t:web.I18n = getService<web.I18n>()
          let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
          let! entityIdOption = (GetId(signInManager).EntityId client (httpContext.User) entityIdInt)
          match entityIdOption with
          | None ->
              return DBResult.error (t.t("An error occured"))
          | Some entityId ->
            return!
              // Determine highest display order for this entity
              lastDisplayOrderaction entityId
              |> DBAction.bind
                (fun lastDisplayOrder ->
                  let e2dInfo:Specs.Entity2DetailInfoSpec =
                      { entities2detailId = None
                        entityId= entityId
                        displayedInList = true
                        maximumNumberOfValues = 1
                        displayOrder = lastDisplayOrder + 10
                      }

                  let spec: Specs.Entity2DetailSpec =
                      Specs.Entity2NewDetailSpec {   info = e2dInfo
                                                     // Reverse the propositions list because as we handle it the first proposition is inserted last. Doing this
                                                     // preserves the display order of the propositions in the form
                                                     detailSpec= { entityDetail with propositions = entityDetail.propositions|> Option.map List.rev}
                                                 }


                  Admin.Entity.addDetailActionFromSpec spec
                  // The caller doesn't use the result, which is of type Entity2ExistingDetailSpec and does not include
                  // all data required to update the UI.
                  // Mapping to unit also helps in making the EntityDetail.addNewDetailSection function generic.
                  |> DBAction.map (fun _ -> ())
                )
              |> adminActionBase
        }
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let linkableDetails (entityIdInt:int) = async {
          let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
          let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
          let client:DB.DBClient = getService<DB.DBClient>()
          let t:web.I18n = getService<web.I18n>()
          let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
          let! entityIdOption = (GetId(signInManager).EntityId client (httpContext.User) entityIdInt)
          match entityIdOption with
          | None ->
              let accountId = Auth.getCurrentUserAccountId httpContext
              MyOwnDBLib.Logging.Log.Error("Problem accessing entity id {entityIdInt} for account {accountId} in linkableDetails")
              return DBResult.error (t.t("An error occured"))
          | Some entityId ->
            return!
              EntityExtensions.byIdAction entityId
              |> DBAction.bind DetailExtensions.linkableDetailsForEntityAction
              |> adminActionBase
        }
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let linkExistingDetail (entityIdInt:int) (detail:Detail.Detail)= async {
          let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
          let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
          let client:DB.DBClient = getService<DB.DBClient>()
          let t:web.I18n = getService<web.I18n>()
          let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
          let! entityIdOption = (GetId(signInManager).EntityId client (httpContext.User) entityIdInt)
          let accountId = Auth.getCurrentUserAccountId httpContext
          match entityIdOption with
          | None ->
              MyOwnDBLib.Logging.Log.Error("Problem accessing entity id {entityIdInt} for account {accountId} in linkExistingDetail")
              return DBResult.error (t.t("An error occured"))
          | Some entityId ->
            return!
              lastDisplayOrderaction entityId
              |> DBAction.map (fun lastDisplayOrder ->
                let e2dInfo: Specs.Entity2DetailInfoSpec =
                   { entities2detailId = None
                     entityId= entityId
                     displayedInList = true
                     maximumNumberOfValues = 1
                     displayOrder = lastDisplayOrder + 10
                   }
                let spec: Specs.Entity2DetailSpec =
                    Specs.Entity2ExistingDetailSpec {   info = e2dInfo
                                                        // This details is in database with id 3, not 6 as the entity
                                                        detailId= detail.id
                                                    }

                spec
              )
              |> DBAction.bind Admin.Entity.addDetailActionFromSpec
              |> DBAction.bind (fun _ -> Admin.Entity.getEntityDetailsAction accountId (entityId|>EntityId.Get))
              |> adminActionBase
        }
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let updateEntityDetailsDisplayOrder (entityDetails:List<Entity.EntityDetail>) = async {
          let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
          let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
          let client:DB.DBClient = getService<DB.DBClient>()
          let t:web.I18n = getService<web.I18n>()
          let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
          let accountId = Auth.getCurrentUserAccountId httpContext
          // Check all EntityDetails to update belong to the same entity, otherwise is not possible
          let entityIdFromClientOption =
            entityDetails
                |> List.distinctBy (fun ed -> ed.entity_id)
                |> List.tryExactlyOne
                |> Option.map (fun ed -> ed.entity_id)
          let! entityIdOption =
            match entityIdFromClientOption with
            | None ->
              // If none here, we have an error above
              MyOwnDBLib.Logging.Log.Error($"""Not all entity details to be updated had the same entityid? account {accountId}, entity details list = {sprintf "%A" entityDetails}""")
              async { return None }
            | Some entityIdToValidate ->
              // If only one entity id, we can go on and validate it
              (GetId(signInManager).EntityId client (httpContext.User) (entityIdToValidate|> EntityId.Get))
          match entityIdOption with
          // We don't have access to the entityId, stop here
          | None ->
              MyOwnDBLib.Logging.Log.Error("Problem accessing entity id {entityIdInt} for account {accountId} in updateEntityDetails")
              return DBResult.error (t.t("An error occured"))
          // We have access to the entityId, but we don't use it below, it was just to validate access to the entiies2details we work on
          | Some _entityId ->
            // Define operators as we will traverse the list with DBAction.Apply so we get a DBAction<EntityDetail list>
            let (<*>) = DBAction.apply
            let cons a b = a::b
            // Update one individual EntityDetail in the database
            let updateEntityDetail (entityDetail:Entity.EntityDetail) =
                validateAccountAccessToEntity2Detail accountId (entityDetail.id)
                // If ok, update data
                |> DBAction.bind (fun () ->
                DBAction.statementAction "update entities2details set display_order=%d where entities2details.id=%d" entityDetail.display_order entityDetail.id
                )
                |> DBAction.bind (fun impactedRows ->
                    match impactedRows with
                    | DBTypes.ImpactedRows 1 ->
                        Admin.Entity.getEntityDetailAction accountId entityDetail.id
                    | DBTypes.ImpactedRows n ->
                        MyOwnDBLib.Logging.Log.Error($"Update of individual entities2details resulted in {n} rows update! For account {accountId} and entitiesdetail {entityDetail.id} updating display order")
                        DBAction.error (t.t("An error occured"))
                )
            // Recursive function to work on list of EntityDetails.
            let rec updateEntityDetails (entityDetailsList:List<Entity.EntityDetail>)=
                match entityDetailsList with
                | ed :: tail ->
                  (DBAction.retn cons)<*>(updateEntityDetail ed) <*> (updateEntityDetails tail)
                | [] ->
                  DBAction.zero
            return!
                updateEntityDetails entityDetails
                // wrap this is admin access check
                |>adminActionBase<Entity.EntityDetail list>
        }
    module Relation =
        type RelationDirection = |ToParents |ToChildren
        // Check account id of user is ok regarding entities2details id we get
        let validateAccountAccessToRelation (accountId:int)(relationId:int)=
          let t:web.I18n = getService<web.I18n>()
          DBAction.queryAction "select count(*) from relations r join  entities e on (e.id=r.parent_id) join databases db on (db.id=e.database_id) where db.account_id=%d and r.id=%d" accountId relationId
          |> DBAction.bind (fun count ->
              if count=0L then
                  MyOwnDBLib.Logging.Log.Warning($"Error accessing relations with id {relationId} by admin for account {accountId}")
                  DBAction.error (t.t("An error occured"))
              else
                  DBAction.retn ()
          )
        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let getRelations (entityIdInt:int) (target:RelationDirection) : Async<DBResult.DBResult<Specs.RelationSpec*string>>= async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let client:DB.DBClient = getService<DB.DBClient>()
            let t:web.I18n = getService<web.I18n>()
            let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
            let accountId = Auth.getCurrentUserAccountId httpContext
            let! entityIdOption = GetId(signInManager).EntityId client (httpContext.User) entityIdInt
            match entityIdOption with
            // We don't have access to the entityId, stop here
            | None ->
                MyOwnDBLib.Logging.Log.Error($"Problem accessing entity id {entityIdInt} for account {accountId} in getToParentsRelations")
                return DBResult.error (t.t("An error occured"))
            // We have access to the entityId, but we don't use it below, it was just to validate access to the entiies2details we work on
            | Some entityId ->
              return!
                (
                  match target with
                  | ToParents ->
                    RelationExtensions.toParentsForEntityAction entityId
                    |> DBAction.bind (fun r ->
                        DBAction.querySingleAction "select name from entities where id = %d" (r.parent_id|>EntityId.Get)
                        |> DBAction.map (fun name -> r,name)
                    )
                  | ToChildren ->
                    RelationExtensions.toChildrenForEntityAction entityId
                    |> DBAction.bind (fun r ->
                        DBAction.querySingleAction "select name from entities where id = %d" (r.child_id|>EntityId.Get)
                        |> DBAction.map (fun name -> r,name)
                    )
                )
                |>DBAction.map (fun (r,name) ->
                  (r.toSpec(),name)
                )
                |>adminActionBase<Specs.RelationSpec*string>
        }

        [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
        let updateRelation (relSpec:Specs.RelationSpec) = async {
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let client:DB.DBClient = getService<DB.DBClient>()
            let t:web.I18n = getService<web.I18n>()
            let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
            let accountId = Auth.getCurrentUserAccountId httpContext
            // Do not trust the EntityId we get from the client, validate the user has access to it
            let! parentEntityIdOption = GetId(signInManager).EntityId client (httpContext.User) (relSpec.parentId|>EntityId.Get)
            let! childEntityIdOption = GetId(signInManager).EntityId client (httpContext.User) (relSpec.childId |> EntityId.Get)

            match (parentEntityIdOption,childEntityIdOption) with
            | (Some _,Some _) ->
              return!
                Admin.Relation.createOrUpdateActionFromSpec relSpec
                |>adminActionBase<Specs.RelationSpec>
            | _,_ ->
              MyOwnDBLib.Logging.Log.Error($"""Problem updating relation {relSpec |> sprintf "%A"}. Entity id could not be retrieved. Parent:{sprintf "%A" parentEntityIdOption}, child: {sprintf "%A" childEntityIdOption}""")
              return DBResult.error (t.t("An error occured"))


        }
