namespace web.admin

open web
open WebSharper
open WebSharper.Sitelets
open WebSharper.Sitelets.Router
open WebSharper.Sitelets.RouterOperators
open WebSharper.Sitelets.UrlHelpers

open web.admin.AdminRouting
module Sitelet =
    // Function to embed admin Sitelet into the app's main Sitelet
    // Maps our endpoints to the endpoints we are embedded into.
    let embedFunction adminEndPoint =
        match adminEndPoint with
        | Users -> EndPoint.Admin(["users"])
        | Databases-> EndPoint.Admin(["databases"])
        | Database databaseId-> EndPoint.Admin(["database";string databaseId])
        | Entity entityId-> EndPoint.Admin(["entity";string entityId])
        | User id-> EndPoint.Admin(["user";string id])
    // Maps the app's endpoints under which the admin sitelet is embedded
    // to our endpoints
    let unEmbedFunction endPoint =
        match endPoint with
        | Admin ["users"] ->
            Some Users
        | Admin ["databases"] ->
            Some Databases
        | Admin["database";INT databaseId] ->
            Some (Database databaseId)
        | Admin["entity";INT entityId] ->
            Some (Entity entityId)
        | Admin ["user";INT userId]->
            Some (User userId)
        | _ -> None

    let adminSitelet =
        Sitelet.InferPartial
            embedFunction
            unEmbedFunction
            (fun ctx endpoint->
                match endpoint with
                | Users -> Users.content ctx
                | Databases -> Databases.content ctx
                | Database databaseId -> Database.content ctx databaseId
                | Entity entityId -> Entity.content ctx entityId
                | User id -> User.content ctx id
            )
        |> Sitelet.Shift "admin"
