namespace web.admin

open web
open web.admin.AdminRouting
open WebSharper.Sitelets
open WebSharper.AspNetCore
open WebSharper.UI
open WebSharper.UI.Html

// Functions rendering the pages accessed by its url in the browser.
// It usually renders the body with javascript. This allows client-side navigation (without
// full page load)

module StandardPage=
    let content (ctx:Context<EndPoint>) (endpoint:EndPoint)= async {
        let body = ClientServer.client ( ClientContent.clientBody ())
        return! Templating.Main ctx endpoint
            [
                div [] [body]
            ]
    }


module Users=
    let content (ctx:Context<EndPoint>) =
      StandardPage.content ctx (EndPoint.Admin ["users"])

module Databases=
    let content (ctx:Context<EndPoint>) =
      StandardPage.content ctx (EndPoint.Admin ["databases"])

module Database=
    let content (ctx:Context<EndPoint>) (databaseId:int)=
      StandardPage.content ctx (EndPoint.Admin ["database";string databaseId])

module Entity=
    let content (ctx:Context<EndPoint>) (entityId:int)=
      StandardPage.content ctx (EndPoint.Admin ["entity";string entityId])

module User=
    let content (_ctx:Context<EndPoint>) (id:int)=
      Content.Text $"welcome on the admin page for modifying user with id {id}"
