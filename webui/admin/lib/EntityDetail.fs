namespace web.admin

open WebSharper.UI.Html
open WebSharper
open WebSharper.UI
open WebSharper.UI.Client
open web.ClientI18n
open web.HtmlHelpers
open web.WSExtensions
open web.Css

[<JavaScript>]
module EntityDetail =

        //********************************************************************************
        // Elements to add a new detail
        //********************************************************************************
        // With type constraints making the function can be required, but I think it works here without inlining
        // because we use an interface as constraint.
        let  addNewDetailSection<'T when 'T:>Specs.IDetailSpec<'T>>
            (initialiseSpec:unit -> 'T)
            (serverCall:'T->Async<DBResult.DBResult<unit>>)
            (refreshList:unit->Async<unit>)
            (buttonText:Doc)=

          let datatypeSpecs:Specs.DataTypeSpec list = web.ClientHelpers.dataTypes |> List.map(fun dt -> dt.toSpec() )
          let formDisplayedVar = Var.Create false
          let entityDetailSpecVar:Var<'T> =
            Var.Create
              (initialiseSpec())
              // lenses on DetailSpec's fields
          let dataTypeVar =
              entityDetailSpecVar.Lens
                (fun r -> (r:>Specs.IDetailSpec<'T>).dataType)
                (fun r v -> (r:>Specs.IDetailSpec<'T>).withDataType v)
          // A lens to the propositions field, which is an option
          let entityDetailPropositionsVar =
              entityDetailSpecVar.Lens
                (fun r -> (r:>Specs.IDetailSpec<'T>).propositions)
                (fun r v -> (r:>Specs.IDetailSpec<'T>).withPropositions v)
          let newDetailNameVar =
              entityDetailSpecVar.Lens
                (fun r -> (r:>Specs.IDetailSpec<'T>).name)
                (fun r v -> (r:>Specs.IDetailSpec<'T>).withName v )
          // The list of propositions entered by the user is lensed into the spec too. But there's some more transformation
          // as the code below uses a list of strings
          // Manipulating a list of string is easier below as it allows the use of the DocSeqCached function
          let propositionsVar:Var<List<string>> =
              entityDetailSpecVar.Lens
                (fun (r:'T) ->
                  match (r:>Specs.IDetailSpec<'T>).propositions with
                  | Some l -> l |> List.map (fun i -> i.value )
                  | None -> []
                )
                (fun (r:'T) (strList) ->
                  if List.length strList = 0 && (r:>Specs.IDetailSpec<'T>).dataType.name <> "choose in list" then
                    (r:>Specs.IDetailSpec<'T>).withPropositions None
                  else
                    Some (strList |> List.map (fun s -> {detailValuePropositionId = None; value = s}:Specs.DetailValuePropositionItemSpec))
                    |> (r:>Specs.IDetailSpec<'T>).withPropositions
                )
          // When we select a data type with propositions, set the entityDetailPropositionsVar to Some []
          // We set an empty list
          let propositionsBackup:Var<Specs.DetailValuePropositionItemSpec list option> = Var.Create None
          dataTypeVar.View
            |> View.Sink
                (fun selectedDataType  ->
                  let chooseInList = (datatypeSpecs |> List.find(fun dt -> dt.name = "choose in list"))

                  // As updating the lens updates the base object it seems we got in a loop here.
                  // Those extensive conditions break the loop
                  if (selectedDataType = chooseInList) && entityDetailPropositionsVar.Value.IsNone then
                    match propositionsBackup.Value with
                    | Some _ ->
                      // If propositions were backed up, restore these
                      entityDetailPropositionsVar.Set (propositionsBackup.Value)
                    | None ->
                      // If no proposition is backed up, initialise with an empty list
                      entityDetailPropositionsVar.Set (Some [])
                  else if (selectedDataType <> chooseInList) && entityDetailPropositionsVar.Value.IsSome then
                    // BAckup current propositions to restore in other branch of the if
                    propositionsBackup.Set entityDetailPropositionsVar.Value
                    entityDetailPropositionsVar.Set None
                )

          // A bool view indicating if the submit button is disabled, so true = button is disabled
          let submitDisabledView =
              View.Map3
                (fun name propositions (dataType:Specs.DataTypeSpec)->
                  // Disable if name is empty or when the data type is choose in list and there is no proposition
                  name = "" || ( dataType.name = "choose in list" && propositions = [])
                )
                newDetailNameVar.View
                propositionsVar.View
                dataTypeVar.View

          // Build the list of propositions entered by the user.
          // We do it outside of the function generating the div view below so that only
          // the new proposition is rendered
          let propositionsListDoc =
            propositionsVar.View.DocSeqCached (fun p ->
              // Return the li element, with the flash class set
              li
                [classes ["flash"]; flashAttribute()]
                [
                  // The value of the proposition
                  text p
                  // The trash icon to delete this proposition from the list
                  span
                    [ on.click (fun _el _ev  ->
                      // Filtering propositions different from this one will remove it.
                      propositionsVar.Set (propositionsVar.Value |> List.filter (fun v -> v<>p))
                    )]
                    [
                      fontAwesomeIconWithClasses [BS.``ms-1``] "trash"
                    ]

                ]

                    )
          // This is the view displaying elements to manage value propositions
          // It is based on the entityDetailPropositionsVar indicating if propositions should be managed
          // with a Some value
          let propositionsDocView =
            entityDetailPropositionsVar.View
            // Use View.MapCached here as all record field lenes are updated as soon as one field is updated.
            // Not using the cache here rerenders the propositions input field and focuses it as soon as one
            // character is typed in the detail name.
            |> View.MapCached (fun propositions  ->
                match propositions with
                // If the propositions in the spec is None, nothing to show
                | None  ->
                    Doc.Empty
                // If the propositions in the space is Some _, then show the management of propositions.
                // We don't use the value though as we manipulate the string list and not the DetailValuePropositionItemSpec list.
                | Some _ ->
                  let newPropositionVar = Var.Create ""
                  // Add proposition to list when enter is pressed
                  let addOnEnter () =
                    on.keyUp
                      (fun el ev ->
                        if ev.KeyCode = 13 then
                          propositionsVar.Set (List.append propositionsVar.Value [newPropositionVar.Value]|> List.distinct)
                          newPropositionVar.Set ""
                      )

                  // This is the UI to manage propositions
                  div
                     []
                     [
                      // Name of the detail
                      label [attr.``for`` "newProposition"] [t.tt("Add proposition")]
                      // We need to focus it after render because it is re-rendered every time we add a proposition
                      // because it is locate in the View.Map. Without this, it loses focus after a proposition is added
                      // by pressing enter.
                      Doc.InputType.Text [attr.id "newProposition";focusAfterRender();addOnEnter(); attr.placeholder(t.t("Press Enter to add"))] newPropositionVar
                      // Followed by value propositions for this detail
                      ul
                        [ attr.id "newPropositionsList"]
                        [
                          // We build the list of lis outside of this view so that only the new entry is rendered
                          propositionsListDoc
                        ]

                     ]
                  )

          let newDetailForm =
            let errorVar = Var.Create (Doc.Empty)
            div
              []
              [
                errorVar.V
                label [attr.``for`` "newDetailName"] [t.tt("Detail name")]
                Doc.InputType.Text [attr.``id`` "newDetailName"] newDetailNameVar
                Doc.InputType.Select [attr.``id`` "newDetailDataType";] (fun (dt:Specs.DataTypeSpec) -> t.t(dt.name))  datatypeSpecs dataTypeVar
                propositionsDocView.V

                button
                  [
                    attr.id "submitNewEntityDetail"
                    classes [BS.btn; BS.``btn-primary``];
                    UI.Html.attr.disabledDynPred (View.Const "disabled") submitDisabledView;
                    on.click (fun _el _ev ->
                      async {
                        let! res = serverCall (entityDetailSpecVar.Value)
                        match res with
                        | Error es ->
                          errorVar.Set (dangerBox (text (es |> Error.toString)))
                        | Ok _ ->
                          // Call the function updating the entity details list, signaling it this is
                          // and update so updated <li> is flashed.
                          do! refreshList()
                          // Clear the new detail form fields
                          entityDetailSpecVar.Set (initialiseSpec())
                      }|>Async.Start
                    )
                  ]
                  [
                    t.tt("Submit")
                  ];
              ]
          buttonRevealingContent
            {|
              buttonTextWhenVisible = t.tt("Hide new detail form")
              buttonTextWhenHidden  = buttonText
            |}
            newDetailForm
