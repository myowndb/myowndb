namespace web.admin

open WebSharper.UI.Html
open WebSharper
open WebSharper.UI
open WebSharper.UI.Client
open web.ClientI18n
open web.HtmlHelpers
open web.WSExtensions
open web.Css

[<JavaScript>]
module UsersDisplay =

    // This is called by the ClientContent.clientBody() to render the body of the page
    // according to the client-side router state.
    let pageBody () = async {
        let t = JStr()
        let! users = Server.Users.getUsers()
        let usersCollection =
            ListModel.Create
                (fun (u:User.User) -> u.id)
                []
        let resultVar = Var.Create Doc.Empty
        let res =
            match users with
            | Ok l ->
                usersCollection.Set (l|> Seq.ofList)
                resultVar.Set Doc.Empty
            | Error es ->
                usersCollection.Set [||]
                resultVar.Set(
                    infobox
                        (InfoBoxKind.Danger)
                        (t.tt("An error occured"))
                )

        // If the editedUser Var holds Some user, we will display the edition for for that user
        let editedUser:Var<Option<User.User>> = Var.Create None
        // Function generating the form to edit the user passed as
        let editionFormForUser (user:User.User) =
            // Put the user in a var and create lenses to easily link it to form fields
            let u = Var.Create user
            let firstname =
                u.Lens
                    (fun u -> u.firstname|> Option.defaultValue "")
                    (fun u v ->
                        if v = "" then
                            {u with firstname=None}
                        else
                            {u with firstname=Some v}
                    )
            let lastname =
                u.Lens
                    (fun u -> u.lastname|> Option.defaultValue "")
                    (fun u v ->
                        if v = "" then
                            {u with lastname=None}
                        else
                            {u with lastname=Some v}
                    )
            let email =
                u.LensAuto (fun u -> u.email)
            let userTypeId =
                u.LensAuto (fun u -> u.user_type_id)
            form
                [ attr.id "admin_users_form"]
                [
                    label [attr.``for`` "userEditionFirstname"] [t.tt("Firstname")]
                    Doc.InputType.Text [attr.id "userEditionFirstname"] firstname

                    label [attr.``for`` "userEditionLastname"] [t.tt("Lastname")]
                    Doc.InputType.Text [attr.id "userEditionLastname"] lastname

                    label [attr.``for`` "userEditionEmail"] [t.tt("Email")]
                    Doc.InputType.Email [attr.id "email"; attr.id "userEditionEmail"; ] email

                    label [attr.``for`` "userEditionUsertype"] [t.tt("User type")]
                    Doc.InputType.Select
                        [attr.id "userEditionUsertype";]
                        (fun id -> if id=1 then (t.t("Administrator")) else t.t("Standard"))
                        [1;2]
                        userTypeId

                    Doc.Button
                        (t.t("Save"))
                        [classes [BS.btn; BS.``btn-primary``]]
                        (fun () ->
                           async {
                                let emailElt = (downcast JavaScript.JS.Document.GetElementById("userEditionEmail"): WebSharper.JavaScript.HTMLInputElement)
                                if emailElt.CheckValidity() then
                                    let! r =
                                        if u.Value.id = Ids.UserId 0 then
                                            Server.Users.createUser u.Value
                                        else
                                            Server.Users.updateUser u.Value
                                    match r with
                                    | Ok [u] ->
                                        // We get the updated user back, inject it in the users collection used to display the table
                                        // Add updates an existing entry
                                        usersCollection.Add u
                                        // Notify of the success
                                        resultVar.Set (infobox InfoBoxKind.SuccessBox (t.tt "User saved successfully"))
                                        // Get out of user edition, hiding the form
                                        editedUser.Set None
                                    // This Ok l should not happen as single row  result is ensured server side
                                    | Ok _ ->
                                        resultVar.Set (infobox InfoBoxKind.Danger (t.tt("An error occured")))
                                    | Error es ->
                                        resultVar.Set (infobox InfoBoxKind.Danger (text (es|>Error.toString)))
                            }
                            |> Async.Start
                        )
                    Doc.Button
                        (t.t("Cancel"))
                        [classes [BS.btn; BS.``btn-secondary``]]
                        // Cancelling is simply going to a state where no user edition is done
                        (fun () -> editedUser.Set None)
                ]
        // The view displaying the for or an empty doc according to the editedUser Var value.
        let editionformView () =
            editedUser.View
            |> View.Map (fun uo ->
                match uo with
                | None -> Doc.Empty
                | Some u -> editionFormForUser u
            )

        let usersTable =
            usersCollection.View
            |> View.Map
                (fun l ->
                    let deleteElement (u:User.User)=
                        web.DeleteConfirmation.deleteConfirmation "user" (
                            (fun () -> async {
                                match! Server.Users.deleteUser u with
                                | Ok _ ->
                                    usersCollection.Remove u
                                    resultVar.Set Doc.Empty
                                    // signal success
                                    return true
                                | Error es ->
                                    resultVar.Set(
                                        infobox
                                            (InfoBoxKind.Danger)
                                            (text (es|>Error.toString))
                                    )
                                    // signal failure
                                    return false
                            })
                        )
                    let resetPasswordElement (u:User.User)=
                        button
                            [ classes [BS.btn; BS.``btn-secondary``]
                              on.click
                                (fun ev el ->
                                  async {
                                    let! res = Server.Users.resetPassword u
                                    match res with
                                    | Ok _ ->
                                        resultVar.Set (infobox InfoBoxKind.SuccessBox (t.tt "The user has received a mail to reset the password. The old password was invalidated."))
                                    | Error _ ->
                                        resultVar.Set( infobox (InfoBoxKind.Danger) (t.tt("An error occured")))
                                    return ()
                                  }
                                |> Async.Start
                              )
                            ]
                            [
                                t.tt("Force password reset")
                            ]
                    table
                        [ classes [BS.table; BS.``table-bordered``; BS.``table-striped``; BS.``table-hover``; BS.``table-sm``; ];attr.id "admin_users_table" ]
                        [
                            thead []
                                  [
                                    tr []
                                       [
                                            th []
                                               [
                                                    div [classes [AppCss.``th-inner``]]  [t.tt("Firstname")]
                                               ]
                                            th []
                                               [
                                                    div [classes [AppCss.``th-inner``]]  [t.tt("Lastname")]
                                               ]
                                            th []
                                               [
                                                    div [classes [AppCss.``th-inner``]]  [t.tt("Email")]
                                               ]
                                            th []
                                               [
                                                    div [classes [AppCss.``th-inner``]]  [t.tt("User type")]
                                               ]

                                       ]

                                    ]

                            tbody []
                                  [
                                        l
                                        |> Seq.map (fun u ->
                                            tr
                                                []
                                                [
                                                    td [classes [BS.``table-sm``]] [text (u.firstname|> Option.defaultValue "")]
                                                    td [classes [BS.``table-sm``]] [text (u.lastname|> Option.defaultValue "")]
                                                    td [classes [BS.``table-sm``]] [text u.email]
                                                    td [classes [BS.``table-sm``]] [if u.user_type_id = 1 then t.tt("Administrator") else t.tt("Standard user") ]
                                                    td [classes [BS.``table-sm``;"myowndbactioncell";]
                                                        on.click (fun ev el ->
                                                            // Remove previous feedback
                                                            resultVar.Set Doc.Empty
                                                            // Trigger form display
                                                            editedUser.Set (Some u)
                                                            // The form is at the top of the page, scroll to it
                                                            JavaScript.JS.Window.ScrollTo(0,0)
                                                            )
                                                        ] [ a [attr.``data-`` "myowndb-action" "edit";] [li [ classes ["fas"; "fa-edit"; BS.``me-5``;"myowndb-action-cell"]] [] ]]
                                                    td [classes [BS.``table-sm``;"myowndbactioncell"];
                                                       ] [ (deleteElement u).V ]
                                                    td [classes [BS.``table-sm``;"myowndbactioncell"];
                                                       ] [ a [attr.``data-`` "myowndb-action" "resetpw"] [(resetPasswordElement u)]]
                                                ]
                                        )
                                        |> Doc.Concat

                                  ]
                        ]
            )

        return div
                []
                [
                    Doc.Button
                        (t.t("Add new"))
                        [classes [BS.btn; BS.``btn-primary``]]
                        (fun () ->
                            resultVar.Set Doc.Empty
                            editedUser.Set (Some(User.Init())))
                    editionformView().V
                    resultVar.V
                    usersTable.V
                ]
    }
