namespace web.admin

open WebSharper.UI.Html
open WebSharper
open WebSharper.UI
open WebSharper.UI.Client
open web.ClientI18n
open web.HtmlHelpers
open web.WSExtensions
open web.Css
open Ids

// Renders /admin/databases
[<JavaScript>]
module DatabasesDisplay =

    // This is called by the ClientContent.clientBody() to render the body of the page
    // according to the client-side router state.
    let pageBody () = async {
        let t = JStr()
        let! dbs = Server.Databases.getDatabases()
        let dbsCollection =
            ListModel.Create
                (fun (db:Specs.DatabaseSpec) -> db.databaseId )
                []
        let resultVar = Var.Create Doc.Empty
        match dbs with
        | Ok l ->
            dbsCollection.Set (l|> Seq.ofList)
            resultVar.Set Doc.Empty
        | Error es ->
            dbsCollection.Set [||]
            resultVar.Set(
                infobox
                    (InfoBoxKind.Danger)
                    (t.tt("An error occured"))
            )
        // If the editedDb Var holds Some db, we will display the edition for for that db
        let editedDb:Var<Option<Specs.DatabaseSpec>> = Var.Create None

        let editionFormForDb (database:Specs.DatabaseSpec) =
            // Put the user in a var and create lenses to easily link it to form fields
            let db = Var.Create database
            let name =
                db.LensAuto
                    (fun u -> u.name)
            let buttonLabel=
              match database.databaseId with
              | Some _ -> t.t("Update")
              | None -> t.t("Create")
            form
                [ attr.id "admin_databases_form"]
                [
                    label [attr.``for`` "dbEditionName"] [t.tt("Name")]
                    Doc.InputType.Text [attr.id "dbEditionName"] name

                    Doc.Button
                        buttonLabel
                        [classes [BS.btn; BS.``btn-primary``]]
                        (fun () ->
                           async {
                                    let! r = Server.Databases.createOrUpdateDatabase db.Value
                                    match r with
                                    | Ok [db] ->
                                        // We get the updated user back, inject it in the users collection used to display the table
                                        // Add updates an existing entry
                                        dbsCollection.Add db
                                        // Notify of the success
                                        resultVar.Set (infobox InfoBoxKind.SuccessBox (t.tt "Database saved successfully"))
                                        // Get out of user edition, hiding the form
                                        editedDb.Set None
                                    // This Ok l should not happen as single row  result is ensured server side
                                    | Ok _ ->
                                        resultVar.Set (infobox InfoBoxKind.Danger (t.tt("An error occured")))
                                    | Error es ->
                                        resultVar.Set (infobox InfoBoxKind.Danger (text (es|>Error.toString)))
                            }
                            |> Async.Start
                        )
                    Doc.Button
                        (t.t("Cancel"))
                        [classes [BS.btn; BS.``btn-secondary``]]
                        // Cancelling is simply going to a state where no user edition is done
                        (fun () -> editedDb.Set None)
                ]


        let editionformView () =
            editedDb.View
            |> View.Map (fun dbo ->
                match dbo with
                | None -> Doc.Empty
                | Some db -> editionFormForDb db
            )

        let dbsTable =
            dbsCollection.View
            |> View.Map
                (fun l ->
                    table
                        [ classes [BS.table; BS.``table-bordered``; BS.``table-striped``; BS.``table-hover``; BS.``table-sm``; ];attr.id "admin_databases_table" ]
                        [
                            thead []
                                  [
                                    tr []
                                       [
                                            th []
                                               [
                                                    div [classes [AppCss.``th-inner``]]  [t.tt("Name")]
                                               ]

                                       ]

                                    ]

                            tbody []
                                  [
                                        l
                                        |> Seq.map (fun db ->
                                            tr
                                                []
                                                [
                                                    td [classes [BS.``table-sm``]] [text db.name]
                                                    td [classes [BS.``table-sm``;"myowndbactioncell";]
                                                        on.click (fun ev el ->
                                                            // Remove previous feedback
                                                            resultVar.Set Doc.Empty
                                                            // Trigger form display
                                                            editedDb.Set (Some db)
                                                            // The form is at the top of the page, scroll to it
                                                            JavaScript.JS.Window.ScrollTo(0,0)
                                                            )
                                                        ] [ a [attr.``data-`` "myowndb-action" "edit";] [li [ classes ["fas"; "fa-edit"; BS.``me-5``;"myowndb-action-cell"]] [] ]]
                                                    td [classes [BS.``table-sm``;"myowndbactioncell";] ]
                                                       [ a [attr.``data-`` "myowndb-action" "view";attr.href (AdminRouting.adminRouter.Link (AdminRouting.AdminEndPoint.Database (db.databaseId|> fun idIn -> idIn|>Option.get |> DatabaseId.Get)))] [li [ classes ["fas"; "fa-search"; BS.``me-5``;"myowndb-action-cell"]] [] ]]
                                                ]
                                        )
                                        |> Doc.Concat

                                  ]
                        ]
            )
        return div
                []
                [
                    Doc.Button
                        (t.t("Add new"))
                        [classes [BS.btn; BS.``btn-primary``; "myowndb-btn-add-db"]]
                        (fun () ->
                            resultVar.Set Doc.Empty
                            editedDb.Set (Some(Specs.DatabaseSpec.Init())))
                    editionformView().V
                    resultVar.V
                    dbsTable.V
                ]
    }
