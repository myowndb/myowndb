namespace web

open WebSharper
open WebSharper.AspNetCore
open WebSharper.UI
open WebSharper.UI.Server
open WebSharper.Sitelets
open Microsoft.Extensions.DependencyInjection
open Microsoft.AspNetCore.Identity;
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Localization
open Microsoft.AspNetCore.Http
open Microsoft.Extensions.Options
open HtmlHelpers
open Microsoft.Extensions.Configuration
open WebSharper.JavaScript
open ClientI18n
// for Async.map
open FsToolkit.ErrorHandling


module Site =
    open WebSharper.UI.Html
    open Css

    let loginErrorBox  (ctx:Context<EndPoint>) (t:I18n) (errorCode:LoginError option) =
            match errorCode with
            | Some AuthenticationError -> div [classes [BS.alert; BS.``alert-danger``]]
                                              [t.tt("Your credentials are not valid. Please check that you entered the correct email and password.")
                                               t.tt("Otherwise the next link lets you change your password: ")
                                               // FIXME: set correct url to reset password
                                               a [attr.href (ctx.Link Home)] [t.tt("click here")]
                                              ]
            | None -> Doc.Empty

    let loginForm(ctx:Context<EndPoint>)(t:web.I18n)(returnUrl:string option)(errorCode: LoginError option) =
        let effectiveReturnUrl = (returnUrl |> Option.defaultValue "/")
        let forgotPasswordLink = ctx.Link ForgotPassword
        div [] [ ClientServer.client (Client.loginForm  forgotPasswordLink effectiveReturnUrl ) ]

    let LoginPage (ctx:Context<EndPoint>) (returnUrl:string option) (errorCode: LoginError option)=
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let config:IConfiguration = unbox (serviceProvider.GetService(typeof<IConfiguration>))
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        // The login page is served from the server without client side routing, so we can include
        // its banners here. We look for banners in the subdirectory "login" in the directory
        // configured in config (see implementation of BuildBanners for exact key)
        let banners = ServerHelpers.Utils.BuildBanners config (Some "login")
        Templating.Anon ctx (EndPoint.Login(returnUrl,errorCode)) (t.t("MyOwnDB Login")) [
            h1 [] [t.tt("Login")]
            banners
            loginForm ctx t returnUrl errorCode
        ]


    let LogoutPage (ctx:Context<EndPoint>) = async {
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let signInManager:SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<SignInManager<ApplicationUser>>))
        signInManager.SignOutAsync()|> Async.AwaitTask |> ignore
        return! Content.RedirectTemporary EndPoint.Home
    }

    let AboutPage (ctx:Context<EndPoint>) = async {
        let httpContext = ctx.HttpContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<I18n>))
        let client = httpContext.RequestServices.GetRequiredService<DB.DBClient>()
        return! Templating.Anon ctx EndPoint.About (t.t("About MyOwnDB")) [
            h1 [] [t.tt("About MyOwnDB")]
            p [] [t.tt("MyOwnDB is your database on the web")]

        ]
    }

    let StandardAuthenticatedPage (ctx:Context<EndPoint>) (endpoint:EndPoint) =
        async {
            let body = ClientServer.client (ClientContent.clientBody () )
            return!
              Templating.Main ctx endpoint
                [
                    div [] [body]
                ]
        }

    let ForgotPasswordPage (ctx:Context<EndPoint>) = async {
        let httpContext = ctx.HttpContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<I18n>))
        return! Templating.Anon ctx EndPoint.ForgotPassword (t.t("Reset Password")) [
            h1 [] [t.tt("Reset Password")]
            div [] [ClientServer.client ( Client.forgotPasswordForm () ) ]
        ]
    }

    let ResetPasswordPage (ctx:Context<EndPoint>) (token:string) (email:string)= async {
        let httpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<I18n>))
        let userManager:UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<UserManager<ApplicationUser>>))

        return! Templating.Anon ctx (EndPoint.ResetPassword(token,email)) (t.t("Reset Password")) [
            h1 [] [t.tt("Set a New Password")]
            div [] [ ClientServer.client ( Client.setNewPasswordForm email token )]
        ]
    }

    let ConfirmEmailPage (ctx:Context<EndPoint>) (token:string) (email:string)= async {
        let httpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<I18n>))
        let userManager:UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<UserManager<ApplicationUser>>))
        let! user = userManager.FindByEmailAsync(email) |> Async.AwaitTask
        let! content = async {
                      if isNull user then
                          MyOwnDBLib.Logging.Log.Information("Could not find user {0} for email confirmation", email)
                          return [h1 [] [t.tt("Confirmation failed...")]
                                  div [attr.id "errorBox"; classes [BS.alert; BS.``alert-danger``]] [t.tt("We could not confirm your email address.")]
                          ]
                      else
                          let! result = userManager.ConfirmEmailAsync(user,token) |> Async.AwaitTask
                          if result.Succeeded then
                              return [h1 [] [t.tt("Confirmation succeeded!")]
                                      div [] [ div [attr.id "infoBox"; classes [BS.alert; BS.``alert-info``]] [t.tt("Your email was successfully confirmed. You can now log in.")];
                                              loginForm ctx t None None ]
                              ]
                          else
                              MyOwnDBLib.Logging.Log.Error("Failed to validate email for user {0} with token {1} . Error was:\n{2}", email, token, (result.Errors|>Seq.map(fun e -> e.Code + ":" + e.Description)))
                              return [ h1 [] [t.tt("Confirmation failed...")]
                                       div [attr.id "errorBox"; classes [BS.alert; BS.``alert-danger``]] [ result.Errors |>Seq.map(fun e -> text e.Description) |> ul [] ]
                              ]
        }

        return! Templating.Anon ctx (EndPoint.ResetPassword(token,email)) (t.t("Confirm your email address")) content
    }

    let SignupPage (ctx:Context<EndPoint>) = async {
        let httpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let config:IConfiguration = unbox (serviceProvider.GetService(typeof<IConfiguration>))
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<I18n>))
        let isSaas = config.GetValue("myowndb:saas:is_saas")
        let tosUrl = config.GetValue("myowndb:saas:tos")
        let privacyUrl = config.GetValue("myowndb:saas:privacy")
        return! Templating.Anon ctx (EndPoint.Signup) (t.t("Signup")) [
            h1 [] [t.tt("Create your account")]
            div [] [ ClientServer.client ( Client.registerForm({| isSaas= isSaas; tosUrl=tosUrl; privacyUrl = privacyUrl|}) )]
        ]
    }

    // file attachment information is stored as yaml, which we will handle with Legivel
    open Legivel.Serialization
    open ServerFileAttachmentExtensions
    // endpoint to serve the file stored in the detail valu with said id
    let GetAttachment (ctx:Context<EndPoint>) (detailValueId:int64)= async {
        let httpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<I18n>))
        let config:IConfiguration = unbox (serviceProvider.GetService(typeof<IConfiguration>))
        let client = httpContext.RequestServices.GetRequiredService<DB.DBClient>()
        let claimsPrincipal = httpContext.User

        // retrieve information from the database
        do! client.Open()
        let! (detailValueResult:DBResult.DBResult<DetailValue.DetailValue>) = DBAction.immediate client "select * from detail_values where id=%d and type='FileAttachmentDetailValue'" detailValueId
        client.Close() |> Async.Start

        match detailValueResult with
        // success if one value is returned
        | Ok [detailValue] -> // first make the yaml convertible to a F# Record by
                            try
                              // removing leading ':' from field names
                              let info = ServerFileAttachment.FileAttachment.FromString detailValue.value
                              // The account id is the first part of the s3 key stored
                              let accountId = info.s3_key |> Option.map (fun k -> k.Split("/").[0])
                              // we control the access to the file based on the account of the user. vvvvvvvvvvv This is ok as first condition validated we have a Some
                              if accountId.IsSome && claimsPrincipal.HasClaim("AccountId", accountId|>Option.get) then
                                // the s3key is some here as validated by accountId.IsSome which is derivated from the s3key
                                let s3Key = info.s3_key |> Option.get
                                // build the path to find the file stored locally
                                let constructedPath = System.IO.Path.Combine(config.GetValue("myowndb:fileAttachments:path"), s3Key)
                                let path = System.IO.Path.GetFullPath(constructedPath)
                                // and serve it
                                if System.IO.File.Exists(path) then
                                    return! (Content.File(path,AllowOutsideRootFolder=true,ContentType=(info.filetype|>Option.defaultValue("application/unknown")))
                                            |> Content.WithHeader "Content-Disposition" $"attachment; filename={info.filename}"
                                    )
                                else
                                    MyOwnDBLib.Logging.Log.Error("File {0} not found for download", path)
                                    return! Content.NotFound
                              else
                                return! Content.Unauthorized
                            with
                                | e ->
                                    MyOwnDBLib.Logging.Log.Error("Deserialise error: {0}",e)
                                    return! Content.NotFound
        // other OK results are not correct
        | Ok [] ->
            MyOwnDBLib.Logging.Log.Error("Detail value with id {0} for file download not found", detailValueId)
            return! Content.NotFound
        | Ok l ->
            MyOwnDBLib.Logging.Log.Error("Unexpected number of detail values returned for file download: {0}",l)
            return! Content.NotFound
        | DBResult.DBResult.Error es ->
            MyOwnDBLib.Logging.Log.Error("Detail value retrieval for file download error: {0}",es)
            return! Content.NotFound

    }
    let HandleUpload (ctx:Context<EndPoint>) = async {
        let id = ServerHelpers.Uploads.copyXhrFirstFileToWaitRoom ctx
        return! Content.Json {| id = id |}
    }

    open DatatableTypes
    let GetTableCsv (ctx:Context<EndPoint>)
                    (tableType:TableType)
                    (id:int)
                    (paginationState:PaginationState)
                    (searchCriteria:SearchCriteria)
                    (sortCriteria:SortCriteria) = async {
        let httpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<I18n>))
        let config:IConfiguration = unbox (serviceProvider.GetService(typeof<IConfiguration>))
        let client = httpContext.RequestServices.GetRequiredService<DB.DBClient>()
        let claimsPrincipal = httpContext.User

        let! serverResult = Server.InstancesDataTableCSV httpContext tableType id paginationState searchCriteria sortCriteria
        match serverResult with
        | Result.Error _ -> return! Content.ServerError
        | Ok (csv,count) ->
            return! Content.Text csv
                    |> Content.WithHeader "Content-Disposition" $"attachment; filename=entity_{id}.csv"
                    |> Content.WithHeader "Content-Type" "text/csv"
    }


    let PublicForm (ctx:Context<EndPoint>) (entityId:int) (responsive:bool): Async<Content<_>>= async {
        let httpContext = ctx.HttpContext()
        let serviceProvider=httpContext.RequestServices
        let dbClient = httpContext.RequestServices.GetRequiredService<DB.DBClient>()
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let body = ClientServer.client ( ClientContent.clientBody ())
        let! hasPublicForm =
            DBAction.queryAction "select has_public_form from entities where id=%d" entityId
            |> DBAction.run dbClient
        match hasPublicForm with
        | DBResult.DBResult.Error es ->
            MyOwnDBLib.Logging.Log.Error($"Error getting has_public_form for entity {entityId}")
            return! Content.NotFound
        | Ok [ b ] ->
            if b then
                // It is the template that will decide if the iframeresizer code needs to be loaded.
                // We couldn't do it with a resource annotation, because the same page is displayed
                // with and without the resource loaded and it is decided dynamically according to the
                // presence or not of the GET parameter "responsive". The resource annotation is for
                // statically deciding if a page needs a resource to be loaded. (See Resource.fs)
                return! Templating.PublicForm ctx responsive (t.t("Public Form"))
                            [
                                div [] [body]
                            ]
            else
                return! Content.NotFound
        | Ok l ->
            MyOwnDBLib.Logging.Log.Error($"Error getting has_public_form for entity {entityId}, unexpected number of rows: {l}")
            return! Content.NotFound
    }
    let PublicFormSubmittedPage (ctx:Context<EndPoint>) (entityIdInt:int)=
        // When accessing the page directly (not via client side rendering), serve an empty page
        Content.Ok


    // The sitelet for the user pages (i.e. non-admin)
    let UserSitelet =
        Sitelet.New Routing.router (fun ctx endpoint ->

            match endpoint with
            | EndPoint.Home -> StandardAuthenticatedPage ctx endpoint
            | EndPoint.About -> AboutPage ctx
            | EndPoint.Login(returnUrl,errorCode) -> LoginPage ctx returnUrl errorCode
            | EndPoint.Logout -> LogoutPage ctx
            | EndPoint.ForgotPassword -> ForgotPasswordPage ctx
            | EndPoint.ResetPassword(token, email) -> ResetPasswordPage ctx token email
            | EndPoint.Signup -> SignupPage ctx
            | EndPoint.ConfirmEmail(token,email) -> ConfirmEmailPage ctx token email
            | EndPoint.Instances(_) -> StandardAuthenticatedPage ctx endpoint
            | EndPoint.Instance(_) -> StandardAuthenticatedPage ctx endpoint
            | EndPoint.EditInstance(_instanceId) -> StandardAuthenticatedPage ctx endpoint
            | EndPoint.CreateInstance(_entityId) -> StandardAuthenticatedPage ctx endpoint
            | EndPoint.GetAttachment(detailValueId) -> GetAttachment ctx detailValueId
            | EndPoint.GetTableCsv(tableType,id,_page,perPage,searchColumn,searchValue,sortColumn,sortDirection) ->
                GetTableCsv ctx tableType id {currentPage= -1; perPage=perPage} {column=searchColumn;value=searchValue} {column= sortColumn; direction=sortDirection}
            | EndPoint.HandleUpload -> HandleUpload ctx
            // At this routing stage, both responsive and non-responsive public forms are handled identically.
            // We just pass the info is it needs to be responsive
            // The form is responsive by default. Only when responsive=false or responsive=0 is passed will it not be responsive.
            | EndPoint.PublicForm(entityId,responsive)  -> PublicForm ctx entityId (responsive.IsNone ||  ( responsive <> (Some "false")) && (responsive <> (Some "0")))
            | EndPoint.PublicFormSubmitted(entityId) -> PublicFormSubmittedPage ctx entityId
            | EndPoint.Import(_entityId) -> StandardAuthenticatedPage ctx endpoint
            // For the endpoint handlet by the embedded Sitelet, we return a ServerError response
            | EndPoint.Admin(_) -> Content.ServerError
        )

    // Embed the sitelet for admin pages in the main sitelet, under prefix "/admin"

    // Main is used in Startup.fs. It is the combination of normal and admin sitelets.
    [<Website>]
    let Main =
        Sitelet.Sum [
            web.admin.Sitelet.adminSitelet
            UserSitelet
        ]
