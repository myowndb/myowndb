namespace web

open WebSharper
open WebSharper.AspNetCore
open WebSharper.UI
open WebSharper.UI.Server
open WebSharper.Sitelets
open Microsoft.Extensions.DependencyInjection
open Microsoft.AspNetCore.Identity;
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Localization
open Microsoft.Extensions.Options
open HtmlHelpers
open Microsoft.Extensions.Configuration
open WebSharper.JavaScript

open System.IO

module Templating =
    open WebSharper.UI.Html
    open Css
    open web.admin
    open web.admin.AdminRouting

    type MainTemplate = Templating.Template<"templates/Main.html">
    type PublicFormTemplate = Templating.Template<"templates/PublicForm.html">
    type AnonTemplate = Templating.Template<"templates/Anon.html">
    type SideBarSection    = Templating.Template<"templates/SideBarSection.html">
    type SideBarItem=Templating.Template<"templates/SideBarItem.html">

    // helper function to read from file json translations to be injected in the template
    // App Ui strings are identified by their english text.
    // Help messages are identified by an id which is also translated to english, and are located
    // in the gettext domain "myowndbhelp". That's why we have 2 files loaded.
    let readLocaleJson (locale:string) =
        let i18nFile = $"wwwroot/i18n/myowndb.{locale}.po.json"
        let i18nJson =
            if System.IO.File.Exists(i18nFile) then
                MyOwnDBLib.Logging.Log.Debug($"Locale file {i18nFile} was found")
                // we have a translations file, read it to pass it to the javascript code below
                System.IO.File.ReadAllText(i18nFile)
            else
                MyOwnDBLib.Logging.Log.Debug($"Locale file {i18nFile} was NOT found, using empty file instead")
                // the keys are in english, for which we load an empty translations list
                """{"":{"language":"en","plural-forms":"nplurals=2; plural=(n > 1);"}}"""
        let i18nHelpFile = $"wwwroot/i18n/myowndbhelp.{locale}.po.json"
        let i18nHelpJson =
            if System.IO.File.Exists(i18nHelpFile) then
                MyOwnDBLib.Logging.Log.Debug($"Locale help file {i18nHelpFile} was found")
                // we have a translations file, read it to pass it to the javascript code below
                System.IO.File.ReadAllText(i18nHelpFile)
            else
                MyOwnDBLib.Logging.Log.Debug($"Locale help file {i18nHelpFile} was NOT found, using empty file instead")
                // the keys are in english, for which we load an empty translations list
                """{"":{"language":"en","plural-forms":"nplurals=2; plural=(n > 1);"}}"""
        i18nJson,i18nHelpJson

    // helper function to inject javascript code in the template to initialise client side i18n
    // This is done in 2 templates, but as the JS.Inline function only takes a
    // static string argument, it is not possible to put the javascript in a variable.
    // To avoid code duplication, we wrap the JSInline call itself in a function.
    [<JavaScript>]
    let clientI18nInit (i18nJson:string) (i18nHelpJson:string) =JS.Inline("""
                                    if (window.hasOwnProperty("i18n")) {
                                        // work with a local variable of WS translation to JS
                                        // causes trouble (using new variables at each use?)
                                        var tmp_myowndb_i18n=window.i18n({});
                                        tmp_myowndb_i18n.loadJSON( $0)
                                        // load help messages in myowndbhelp context
                                        tmp_myowndb_i18n.loadJSON( $1, "myowndbhelp")
                                        // make it available globally
                                        window.myowndb_i18n = tmp_myowndb_i18n;
                                    }
                                    """, i18nJson , i18nHelpJson) |> ignore

    // As we don't have a lot of information to place in global js variables, we define a record type
    // to hold those
    [<JavaScript>]
    type ClientGlobals =
        {
            myowndb_fileAttachments_maxBytes: int64
            dataTypes: DataType.DataType list
        }
    // Reminder: JS.Inline takes static strings with place holders, so we can't build a string in a variable
    [<JavaScript>]
    // We make data in the ClientGlobals instance available to client code
    // You can configure javascript objects here, but be sure these are loaded (according to the Require
    // annotation loading resources)
    let clientGlobalsInit (r:ClientGlobals) =
        JS.Inline("""
            // Configure marked to disable deprecated options
            marked.use({mangle:false, headerIds: false});
            // Initialise the driverObj so page tours can be displayed
            window.driverObj = window.driver.js.driver();
            window.myowndb_fileAttachments_maxBytes = $0;
            window.myowndb_dataTypes = $1;
        """, r.myowndb_fileAttachments_maxBytes, r.dataTypes)

    // Compute a menubar where the menu item for the given endpoint is active
    let MenuBar (ctx: Context<EndPoint>) endpoint : Doc list =
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let locOptions:IOptions<RequestLocalizationOptions> =  unbox (serviceProvider.GetService(typeof<IOptions<RequestLocalizationOptions>>))
        let supportedCultures = locOptions.Value.SupportedUICultures
        let requestCulture = httpContext.Features.Get<IRequestCultureFeature>().RequestCulture.UICulture
        let requestCultureName = (requestCulture.TextInfo.ToTitleCase requestCulture.NativeName, requestCulture.Name)
        let culturesItems = supportedCultures
                            |> Seq.filter (fun c -> c <> requestCulture)
                            |> Seq.map (fun c ->c.TextInfo.ToTitleCase c.NativeName, c.Name)
                            |> Seq.toList
        let ( => ) txt act =
             // attr.``class`` takes as argument the string listing all classes to be applied to element
             li  [ attr.``class`` (BS.``nav-item`` + (if endpoint = act then  " " + BS.active else "")) ]
                       [
                           a [ attr.id($"menu_entry_{act.ToString()}")
                               attr.href (ctx.Link act)
                               attr.``class`` BS.``nav-link``]
                               [text txt]
                       ]
        // Only display logout link if the user is authenticated
        let loginEntry = if httpContext.User.Identity.IsAuthenticated then  Doc.Empty else t.t("Login") => EndPoint.Login (None,None)
        let signupEntry = if httpContext.User.Identity.IsAuthenticated then  Doc.Empty else t.t("Signup") => EndPoint.Signup
        let logoutEntry = if httpContext.User.Identity.IsAuthenticated then  t.t("Logout") => EndPoint.Logout else Doc.Empty
        let tourEntry =
          if httpContext.User.Identity.IsAuthenticated then
             li
              [ classes [BS.``nav-item``] ]
              [
                a [ classes [BS.``nav-link``]; attr.``id`` "pageTourIcon"; on.click (fun _el _ev -> MyOwnDBHelp.showTour() )] [ClientServer.client (MyOwnDBHelp.tourMenuIcon() )]
              ]
          else Doc.Empty
        let adminUsersEntry =
            if Authorization.isAdmin httpContext then
                t.t("Manage users") => EndPoint.Admin (["users"])
            else
                Doc.Empty
        let aboutEntry =   if httpContext.User.Identity.IsAuthenticated then  Doc.Empty else t.t("About") => EndPoint.About

        // Build and return list of link to display in the menu bar
        [
            ul [classes [BS.``navbar-nav``; BS.``me-auto``; BS.``mb-2``; BS.``mb-lg-0``]] [
                t.t("Home") => EndPoint.Home
                loginEntry
                signupEntry
                adminUsersEntry
                aboutEntry
                logoutEntry
                tourEntry
            ]
            ClientServer.client (Client.languageDropdown requestCultureName culturesItems)
        ]

    let SideBarAsync (ctx:Context<EndPoint>) action =

        let httpContext = ctx.HttpContext()
        let serviceProvider=httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = httpContext.RequestServices.GetService<SignInManager<ApplicationUser>>()
        let client = httpContext.RequestServices.GetRequiredService<DB.DBClient>()
        async {
            let! closeDBIfNeeded = client.OpenIfNeeded()
            let! user = signInManager.UserManager.GetUserAsync(httpContext.User)|> Async.AwaitTask
            let! databasesResult = DBAction.queryAction "select db.id,db.name from databases db join accounts a on (a.id=db.account_id) join users u on (u.account_id=a.id) where u.id=%d" user.Id
                                     |> DBAction.map (fun (r:{| id:int; name:string  |}) ->
                                          (r.id, SideBarItem().Database(r.name).DatabaseId(string r.id))
                                        )
                                     |> DBAction.bind (fun (id,sideBarItem ) ->
                                             DBAction.queryAction "select e.id as entity_id, e.name as entity_name from entities e join databases db on (e.database_id=db.id) where db.id=%d" id
                                             |> DBAction.map (fun (r:{|entity_id: int; entity_name:string|}) ->
                                                    a [attr.``class`` "nav-link"; attr.href (Routing.router.Link (Instances (r.entity_id,None)))] [text r.entity_name]
                                             )
                                             |> DBAction.workOnList
                                             |> DBAction.map (fun links -> sideBarItem.Entities(links).Doc())
                                     )
                                     |> DBAction.run client
            let adminSidebarSectionIfNeeded =
                if Authorization.isAdmin httpContext then
                    let items =
                        nav [classes [SBA.``sb-sidenav-menu-nested``; SBA.nav]] [
                            a [attr.``class`` "nav-link"; attr.href (adminRouter.Link (Users))] [text (t.t("Users"))]
                            a [attr.``class`` "nav-link"; attr.href (adminRouter.Link (Databases))] [text (t.t("Databases"))]
                        ]
                    [|
                      div [classes ["sb-sidenav-menu-heading"]] [t.tt("Admin")]

                      SideBarSection().Title(t.t("Admin")).Id("Admin").Icon("wrench").SectionItems(items).Doc()
                    |]
                else
                    [||]

            do! closeDBIfNeeded()

            match databasesResult with
            | Ok l ->
                let databasesSection = [| SideBarSection().Title(t.t("Databases")).Id("Databases").Icon("database").SectionItems(l).Doc() |]
                return
                    div
                        []
                        (Array.concat [| databasesSection;adminSidebarSectionIfNeeded|])
            | Error e ->
                        MyOwnDBLib.Logging.Log.Error("Error retrieving databases for sidebar: {0}", (e |> Error.toString))
                        return Doc.Empty
        }

    let BuildBusyIndicator (t:web.I18n) =
        let busyLabel =  t.t("Working...")
        ClientServer.client
          (
            WSExtensions.Remoting.activeRpcs.View
            |> View.Map (fun currentActiveRpcs ->
                if currentActiveRpcs > 0 then
                  div [classes [BS.``spinner-border``; BS.``text-primary``]; Attr.Create "role" "status"]
                      [
                        span
                          [classes [BS.``visually-hidden``] ]
                          // -- TRANSLATORS: Busy indicator alternate text.
                          [ text busyLabel ]
                      ]
                else
                  Doc.Empty
              )
            |> Client.Doc.EmbedView
          )

    let Main (ctx:Context<EndPoint>) action (body: Doc list) =
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let dbClient = unbox(serviceProvider.GetService(typeof<DB.DBClient>))
        let config:IConfiguration = unbox (serviceProvider.GetService(typeof<IConfiguration>))
        let userName = httpContext.User.Identity.Name.ToLower()
        // we use the locale multiple times, so store it in a variable
        let locale = t.locale()
        let i18nJson,i18nHelpJson = readLocaleJson locale
        async {
            // We set the datatypes value for every authenticated page even though it is only used on admin pages.
            // This is because this code is only run when the server renders the page, and not when we have a client side
            // navigation. If we only set the datatypes on admin pages, and the user first goes to a non-admin page then
            // navigates client-side to an admin page, the datatypes would not be available on that admin page.
            let! dataTypes = ServerHelpers.MemoizedData.getDataTypesAsync dbClient
            // Put values to be globally available client side in a record
            let clientGlobals:ClientGlobals =
                { myowndb_fileAttachments_maxBytes= config.GetValue("myowndb:fileAttachments:maxBytes")
                  dataTypes = dataTypes
                }
            let busyIndicator = BuildBusyIndicator (t)

            let! sidebar = SideBarAsync ctx action
            // Display global banners, ie not looking in subdirectories of the path in config
            let banners = ServerHelpers.Utils.BuildBanners config None
            return! Content.Page(
                        MainTemplate()
                            .Locale(locale)
                            .InitGlobals(fun (_t:Dom.Element) ->
                                clientI18nInit i18nJson i18nHelpJson
                                clientGlobalsInit clientGlobals
                            )
                            .DataTitle(t.t("Data"))
                            .BusyIndicator(busyIndicator)
                            .MenuBar(MenuBar ctx action)
                            .SideBarDatabases(sidebar)
                            .Body(body)
                            .MyOwnDB(t.t("MyOwnDB"))
                            .Navigation(t.t("Navigation"))
                            .CurrentUser(userName)
                            .Banners(banners)
                            .Doc()
                    )
        }
    let Anon (ctx:Context<EndPoint>) action (title: string) (body: Doc list) =
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let config:IConfiguration = unbox (serviceProvider.GetService(typeof<IConfiguration>))
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        // we use the locale multiple times, so store it in a variable
        let locale = t.locale()
        // identify the lacalisation file
        let i18nJson,i18nHelpJson = readLocaleJson locale
        // Display global banners, ie not looking in subdirectories of the path in config
        let banners = ServerHelpers.Utils.BuildBanners config None
        let busyIndicator = BuildBusyIndicator (t)
        Content.Page(
            AnonTemplate()
                .Locale(t.locale())
                .InitGlobals(fun (t:Dom.Element) -> clientI18nInit i18nJson i18nHelpJson)
                .Title(title)
                .MenuBar(MenuBar ctx action)
                .BusyIndicator(busyIndicator)
                .Body(body)
                .MyOwnDB(t.t("MyOwnDB"))
                .Navigation(t.t("Navigation"))
                .Banners(banners)
                .Doc()
        )

    let PublicForm (ctx:Context<EndPoint>) (responsive:bool) (title: string) (body: Doc list) =
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = ctx.HttpContext()
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let config:IConfiguration = unbox (serviceProvider.GetService(typeof<IConfiguration>))
        let dbClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))
        // we use the locale multiple times, so store it in a variable
        let locale = t.locale()
        let i18nJson,i18nHelpJson = readLocaleJson locale
        // Put values to be globally available client side in a record
        async {
            let clientGlobals:ClientGlobals =
                { myowndb_fileAttachments_maxBytes= config.GetValue("myowndb:fileAttachments:maxBytes")
                  // In the public form we do not need to have the list of data types
                  dataTypes = []
                }
            return! Content.Page(
                        PublicFormTemplate()
                            .Locale(locale)
                            .InitGlobals(fun (t:Dom.Element) ->
                                clientI18nInit i18nJson i18nHelpJson
                                clientGlobalsInit clientGlobals
                            )
                            .Body(body)
                            // Include the iframeresizer script of the form needs to be responsive.
                            .IframeResizer(if responsive then script [attr.src "/vendor/iframeresizer/iframeResizer.contentWindow.min.js"] [] else Doc.Empty)
                            .Doc()
                    )
        }
