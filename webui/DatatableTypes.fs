namespace web

open WebSharper
open WebSharper.UI

[<JavaScript>]
module DatatableTypes =

    type PaginationState = {
        currentPage: int
        perPage: int
    }

    type SearchCriteria = {
        column: string;
        value: string
    }

    type SortDirection = |Asc |Desc
    type SortCriteria = {
        column: string option;
        direction: SortDirection
    }

    // types to support display of different types of instances lists:
    // - main list
    // - linked list (both to parents and to children)
    type LinkedTableInfo =
        {
          sourceInstanceId: Ids.InstanceId
          direction: Ids.RelationAndDirection
        }
        with
        override self.ToString() =
            let sourceId = Ids.InstanceId.Get self.sourceInstanceId
            match self.direction with
            | Ids.ChildrenVia (relationId) -> $"{sourceId}_children_relation_{relationId|>Ids.RelationId.Get}"
            | Ids.ParentsVia (relationId) ->  $"{sourceId}_parents_relation_{relationId|>Ids.RelationId.Get}"
    type TableType =
        | Main
        | Linked of LinkedTableInfo
        | Linkable of LinkedTableInfo
        with
        override self.ToString() =
            match self with
            | Main -> "main"
            | Linked l -> "linked_" + l.ToString()
            | Linkable l -> "linkable_" + l.ToString()

    type TableDocInfo =
        {
            doc: WebSharper.UI.Doc
            refresh: unit -> unit
            numberOfRows: View<int64>

        }
    with
        static member fromDoc (doc:WebSharper.UI.Doc) =
            {
                doc = doc
                refresh = fun () -> ()
                numberOfRows = View.Const 0
            }
[<JavaScript>]
module LinkedInstancesTypes =
    type RelatedInDirectionForIntInstanceId = |ParentsFor of int | ChildrenFor of int
