﻿namespace web

open WebSharper
open Microsoft.AspNetCore.Localization
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Identity;
open Microsoft.AspNetCore.Authorization;
open WebSharper.Sitelets
open WebSharper.Sitelets.InferRouter
open FSharpPlus
open Helpers
open FsToolkit.ErrorHandling
open ServerHelpers
open ServerFileAttachmentExtensions
open Microsoft.Extensions.Configuration
open ServerHelpers.Remoting

module Server =
    open LinkedInstancesTypes
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let setCultureCookie (culture:string) =
        let ctx = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        async {
            httpContext.Response.Cookies.Append(CookieRequestCultureProvider.DefaultCookieName,
                                                CookieRequestCultureProvider.MakeCookieValue(RequestCulture(culture)))
        }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let authenticate (email:string) (pass:string) =
        let ctx = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let signInManager:SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<SignInManager<ApplicationUser>>))
        let dbActionRunner:IDBActionRunner = getService<IDBActionRunner>()
        async {
            let! result = signInManager.PasswordSignInAsync(email, pass, false, false) |> Async.AwaitTask
            if result.Succeeded then
                MyOwnDBLib.Logging.Log.Information("User {Email} authenticated successfully", email)
                dbActionRunner.Send("user#logged_in_at update", UserExtensions.setLoggedInAtAction(email))
                return true
            else
                MyOwnDBLib.Logging.Log.Warning("User {Email} failed to authenticate: {Errors}", email,result)
                // If login is unsuccessul, redirect with a Status 303, which always causes the next
                // page to be fetched with a GET, which is the only method handled by Enpoint.Login
                // See https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/303
                return false
        }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let resetPassword (email:string) =
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let mailer:IMailer = unbox (serviceProvider.GetService(typeof<IMailer>))
        let userManager:UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<UserManager<ApplicationUser>>))
        let router = Router.Infer<EndPoint>()

        async {
            let! user = userManager.FindByEmailAsync(email) |> Async.AwaitTask
            if not (isNull user) then
                let! token = userManager.GeneratePasswordResetTokenAsync(user) |> Async.AwaitTask
                let resetLink = sprintf "%s://%s%s" (httpContext.Request.Scheme.ToString()) (httpContext.Request.Host.ToString()) (router.Link (ResetPassword (token,email)))
                let subject = t.t("Reset your password at MyOwnDB")
                let body = t.t("Dear user,") + "\n\n" +
                           t.t("Someone asked a reset of your password at MyOwnDB. If you didn't initiate this request, please ignore this mail.") + "\n\n" +
                           t.t("You can set a new password for your account by following this link: {0}", resetLink) + "\n\n" +
                           t.t("Thanks for using MyOwnDB!")
                mailer.Send(email,subject,body)
            return ()
        }
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let changePassword (email:string) (pass:string) (token:string)=
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let mailer:IMailer = unbox (serviceProvider.GetService(typeof<IMailer>))
        let userManager:UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<UserManager<ApplicationUser>>))
        async {
            let! user = userManager.FindByEmailAsync(email) |> Async.AwaitTask
            let! result = userManager.ResetPasswordAsync(user,token,pass) |> Async.AwaitTask
            if result.Succeeded then
                return Ok ()
            else
                return Error (result.Errors |>  Seq.map(fun e -> (e.Code, e.Description)) |> Seq.toList)
        }
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let registerAccount (email:string) (pass:string) (tos:bool)=
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let mailer:IMailer = unbox (serviceProvider.GetService(typeof<IMailer>))
        let userManager:UserManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<UserManager<ApplicationUser>>))
        let client:DB.DBClient = unbox (httpContext.RequestServices.GetService(typeof<DB.DBClient>))
        let router = Router.Infer<EndPoint>()
        async {
            do! client.Open()
            do! client.BeginTransaction()
            // create account to which the user account will belong
            let! (accountIdResult:DBResult.DBResult<Ids.AccountId>) = DBAction.singleInsertAction "insert into accounts(account_type_id,status) VALUES(1,'active') RETURNING id"
                                                                        |>DBAction.run client
            match accountIdResult with
            | Ok([Ids.AccountId id]) ->
                // create ApplicationUser from ApplicationUserSpec
                let userSpec = {
                    accountId = id
                    // The fisrt user created for a new account is admin
                    userTypeId = 1
                    email = email
                    firstname = None
                    lastname = None
                }
                let appUser = ApplicationUser.FromSpec(userSpec)
                // Pass the ApplicationUser to the UserManager for creation
                let! result = userManager.CreateAsync(appUser, pass) |> Async.AwaitTask
                if result.Succeeded then
                    // commit database changes and send email
                    do! client.Commit()
                    let! token = (userManager.GenerateEmailConfirmationTokenAsync(appUser)|> Async.AwaitTask)
                    let confirmLink = sprintf "%s://%s%s" (httpContext.Request.Scheme.ToString()) (httpContext.Request.Host.ToString()) (router.Link (ConfirmEmail(token,email)))
                    let subject = t.t("Confirm your account at MyOwnDB")
                    let body = t.t("Dear user,") + "\n\n" +
                               t.t("You have successfully created your account at MyOwnDB, but you still need ot confirm your email by following this link: {0}", confirmLink) + "\n\n" +
                               t.t("Thanks for using MyOwnDB!")
                    mailer.Send(email,subject,body)
                    return Ok ()
                else
                    let! r =  client.TryRollback()
                    match r with
                    |Ok _ -> ()
                    |Error _ -> MyOwnDBLib.Logging.Log.Debug("Error trying to rollback transaction in account creation")
                    MyOwnDBLib.Logging.Log.Information("Error creating user entry for new account: {0}", result.Errors)
                    return Error (result.Errors |>  Seq.map(fun e -> (e.Code, e.Description)) |> Seq.toList)
            | Error es ->
                    MyOwnDBLib.Logging.Log.Debug("Error creating new account for user registration: {0}", es)
                    return Error (["AccountCreationError", "The account could not be created."])
            | Ok ids ->
                    MyOwnDBLib.Logging.Log.Debug("Error creating new account for user registration: multiple ids returned?? ({0})", ids)
                    return Error [ ("AccountCreationIncoherence", "Creating the account resulted in an incoherence aborting.")]
        }

    // helper to be used in RPC functions
    let getDBClient (ctx:Web.Context) =
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))
        client

    // helper to be used in RPC functions
    let getEntityId (httpContext:Microsoft.AspNetCore.Http.HttpContext) (id:int) = async {
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let! user = signInManager.UserManager.GetUserAsync(httpContext.User)|> Async.AwaitTask
        let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))
        // logging function of access to the EntityId is denied
        let accessDeniedLogger () = MyOwnDBLib.Logging.Log.Error("No access granted to entity {0} for user {email} with claims {claims}:", id,user.Email, (httpContext.User.Claims))
        return! (GetId(signInManager).EntityId client (httpContext.User) id)
                |> Async.map (resultOfOption accessDeniedLogger (t.t "Access denied"))
                // log the error and map it to the user message
                |> AsyncResult.mapError ( fun e ->
                        MyOwnDBLib.Logging.Log.Error("Error retrievind entity {0} for user {email} ", id,user.Email)
                        t.t "An error was encountered retrieving database information. It has been logged for administrator to analyse"
                )
    }

    let getEntityIdFromWebContext (ctx:Web.Context) (id:int) =
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        getEntityId httpContext id

    // helper to be used in RPC functions
    let getInstanceId (httpContext:Microsoft.AspNetCore.Http.HttpContext) (id:int) = async {
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let! user = signInManager.UserManager.GetUserAsync(httpContext.User)|> Async.AwaitTask
        let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))
        // logging function of access to the EntityId is denied
        let accessDeniedLogger () = MyOwnDBLib.Logging.Log.Error("No access granted to entity {0} for user {email} with claims {claims}:", id,user.Email, (httpContext.User.Claims))
        return! (GetId(signInManager).InstanceId client (httpContext.User) id)
                |> Async.map (resultOfOption accessDeniedLogger (t.t "Access denied"))
                // log the error and map it to the user message
                |> AsyncResult.mapError ( fun e ->
                        MyOwnDBLib.Logging.Log.Error("Error retrievind entity {0} for user {email} ", id,user.Email)
                        t.t "An error was encountered retrieving database information. It has been logged for administrator to analyse"
                )
    }

    let getEntityIdActionAsync  (ctx:Web.Context) (id:int) = async {
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        return! getEntityId httpContext id
                |> Async.map DBAction.fromResult
    }

    let getPublicEntityIdActionAsync  (ctx:Web.Context) (id:int) = async {
        let client = getDBClient ctx
        return EntityExtensions.byIdForPublicFormAction id
                |> DBAction.map (fun (e:Entity.Entity) -> e.id )
    }

    let getInstanceIdActionAsync  (ctx:Web.Context) (id:int) = async {
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        return! getInstanceId httpContext id
                |> Async.map DBAction.fromResult
    }


    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let EntityDetails (id:int) (listView:bool) :Async<Result<list<Entity.EntityDetail>,string>> = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let client = getDBClient ctx
        // retrieve the EntityId, the value is None if no access is granted
        return! getEntityId httpContext id
                // map option to an Async<Result<>>
                // retrieve details from database
                |> AsyncResult.bind (fun entityId -> async {
                    return! EntityExtensions.entityDetailsAction entityId
                            |> DBAction.run client
                            |> Async.map DBResult.toResult
                            |> AsyncResult.mapError (fun l -> String.concat ", " l)
                })
                // filter details according to list view
                |> AsyncResult.map (fun l ->
                    (l |> List.filter (fun i -> i.displayed_in_list_view=listView)))
    }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let InstanceDetails (id:int) :Async<Result<list<Entity.EntityDetail>,string>> = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let client = getDBClient ctx
        // retrieve the EntityId, the value is None if no access is granted
        return! getInstanceId httpContext id
                // map option to an Async<Result<>>
                // retrieve details from database
                |> AsyncResult.bind (fun instanceId -> async {
                    return! Instance.byIdAction instanceId
                            |> DBAction.bind (fun instance -> EntityExtensions.entityDetailsAction instance.entity_id)
                            |> DBAction.run client
                            |> Async.map DBResult.toResult
                            |> AsyncResult.mapError (fun l -> String.concat ", " l)
                })
    }

    let InstancesDataTable (httpContext:Microsoft.AspNetCore.Http.HttpContext)(tableType:DatatableTypes.TableType) (format:ServerHelpers.DataTable.DataTableFormat)(id:int) (paginationState:DatatableTypes.PaginationState) (searchCriteria:DatatableTypes.SearchCriteria)(sortCriteria:DatatableTypes.SortCriteria):Async<Result<string * int64,string>> =
        async {
            let serviceProvider= httpContext.RequestServices
            let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))
            let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
            // open (and close below) the connection manually so both queries use the same connection
            do! client.Open()
            // authorize by trying to get the EntityId
            // it will be reused for 2 queries below
            let! entityIdResult = getEntityId httpContext id

            // FIXME: after making getFilterClause return a DBAction, do all work at DBAction level and only issue query as last step
            //  build the filter for the subsequent queries
            let! filterClause = ServerHelpers.DataTable.getFilterClause client entityIdResult searchCriteria sortCriteria paginationState
            // retrieve the table's data and map it to a json array of objects
            let! r = ServerHelpers.DataTable.getData client tableType format entityIdResult filterClause
                     |> Async.map (I18n.setResultError (t.t "Database error"))
            // retrieve the table count of rows
            let! c = ServerHelpers.DataTable.getCount client tableType entityIdResult filterClause
                     |> Async.map (I18n.setResultError (t.t "Database error"))
            do! client.Close()

            // change the pair of results in a result of a pair, which is what we need to return
            let ret = (r,c) |> tupleSequenceToResult

            return ret
        }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let InstancesDataTableJSON (tableType:DatatableTypes.TableType) (id:int) (paginationState:DatatableTypes.PaginationState) (searchCriteria:DatatableTypes.SearchCriteria)(sortCriteria:DatatableTypes.SortCriteria):Async<Result<string * int64,string>> =
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        InstancesDataTable httpContext tableType ServerHelpers.DataTable.DataTableFormat.JSON id paginationState searchCriteria sortCriteria

    let InstancesDataTableCSV (httpContext)(tableType:DatatableTypes.TableType) (id:int) (paginationState:DatatableTypes.PaginationState) (searchCriteria:DatatableTypes.SearchCriteria)(sortCriteria:DatatableTypes.SortCriteria):Async<Result<string * int64,string>> =
        InstancesDataTable httpContext tableType ServerHelpers.DataTable.DataTableFormat.CSV id paginationState searchCriteria sortCriteria

    // FIXME: unused?
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let getInstanceSpec (instanceId:int) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let! user = signInManager.UserManager.GetUserAsync(httpContext.User)|> Async.AwaitTask
        let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))
        let! instanceIdResult = getInstanceId httpContext instanceId

        match instanceIdResult with
        | Ok instanceId -> let! specResult =
                            Instance.getSpecAction instanceId
                            |> DBAction.run client
                           match specResult with
                           | Ok [spec] -> return (Some spec)
                           | Ok _ ->
                                MyOwnDBLib.Logging.Log.Error("Error getting instance spec: multiple specs returned ??") |> ignore
                                return None
                           | Error e ->
                                MyOwnDBLib.Logging.Log.Error("Error getting instance spec: {0}", e) |> ignore
                                return None
        | Error e -> MyOwnDBLib.Logging.Log.Error("Error getting instance id to get instance spec: {0}", e) |> ignore
                     return None
    }

    // returns all detail specs of an entity
    let detailSpecOfEntity (entityId:Ids.EntityId) =
        entityId
        |> DBAction.retn
        |> DBAction.bind EntityExtensions.entityDetailsAction
        // extract the detail id for each detail
        // note that it is under key detail_value because the EntityDetail's id is for the record in
        // the table entities2details
        |> DBAction.map (fun (d:Entity.EntityDetail) -> d.detail_id)
        // map the detail id to the detail's spec action
        |> DBAction.bind (fun detailId -> DetailExtensions.getSpecAction detailId)
        // We need to handle the list to return it
        |> DBAction.workOnList

    // Type used to identify which instance form information is requested for:
    // - edition, so we have an instance id which is passed
    // - creation, but there is no instance id and the form must be built based on the entity id passed
    [<JavaScript>]
    type LinkFormInfo ={
        relationId: int
        entityId: int
        dirAndId: RelatedInDirectionForIntInstanceId
    }
    type FormKind = EditionForm of instanceId:int | CreationForm of entityId:int |PublicForm of entityId:int |LinkForm of LinkFormInfo

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let getInstanceAndDetailsSpec (formKind:FormKind) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let! user = signInManager.UserManager.GetUserAsync(httpContext.User)|> Async.AwaitTask
        let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))

        let displayCreationFormForValidEntityId (entityId:Ids.EntityId) = async {
            let! specsTupleResult =
                detailSpecOfEntity entityId
                    |> DBAction.map (fun detailsSpecs ->
                        None,detailsSpecs
                    )
                |> DBAction.run client
            match specsTupleResult with
            | Ok [_,detailsSpecs] ->
                 return (Some ( Specs.InstanceSpec.New(entityId), detailsSpecs))
            | Ok _ ->
                   MyOwnDBLib.Logging.Log.Error("Error getting entity spec: multiple specs returned ??") |> ignore
                   return None
            | Error e ->
                 MyOwnDBLib.Logging.Log.Error("Error getting entity spec: {0}", e) |> ignore
                 return None
        }
        match formKind with
        | EditionForm instanceId ->
            let! instanceIdResult = getInstanceId httpContext instanceId
            match instanceIdResult with
            | Ok instanceId -> let! specsTupleResult =
                                // get the instance spec
                                Instance.getSpecAction instanceId
                                // the function will return a DBAction, so use bind
                                |> DBAction.bind (fun spec ->
                                    // We need to look at the entity details, and there could be some details
                                    // for which the instance hs no detail_value
                                    detailSpecOfEntity spec.entityId
                                    // map to a tuple holding the instance's spec and its details' specs
                                    |> DBAction.map (fun detailsSpecs ->
                                        spec,detailsSpecs
                                    )
                                )
                                |> DBAction.run client
                               match specsTupleResult with
                               | Ok [spec,detailsSpecs] ->
                                    return (Some (spec, detailsSpecs))
                               | Ok _ ->
                                    MyOwnDBLib.Logging.Log.Error("Error getting instance spec: multiple specs returned ??") |> ignore
                                    return None
                               | Error e ->
                                    MyOwnDBLib.Logging.Log.Error("Error getting instance spec: {0}", e) |> ignore
                                    return None
            | Error e -> MyOwnDBLib.Logging.Log.Error("Error getting instance id to get instance spec: {0}", e) |> ignore
                         return None
        | CreationForm entityIdInt ->
            let! entityIdResult = getEntityId httpContext entityIdInt
            match entityIdResult with
            | Ok entityId ->
                return! displayCreationFormForValidEntityId entityId
            | Error e -> MyOwnDBLib.Logging.Log.Error("Error getting entity id to get entity spec: {0}", e) |> ignore
                         return None
        | LinkForm {entityId=entityIdInt} ->
                let! entityIdResult = getEntityId httpContext entityIdInt
                match entityIdResult with
                | Ok entityId ->
                    return! displayCreationFormForValidEntityId entityId
                | Error e ->
                    MyOwnDBLib.Logging.Log.Error("Error getting entity id to get entity spec: {0}", e) |> ignore
                    return None
        | PublicForm entityIdInt ->
            let! (entityIdResult:DBResult.DBResult<Entity.Entity>) =
                EntityExtensions.byIdForPublicFormAction entityIdInt
                |> DBAction.run client
            match entityIdResult with
            // Found entity with public form
            | (Ok [r]) ->
                return! displayCreationFormForValidEntityId (r.id)
            // Entity not found or does not have public form
            | (Ok []) ->
                MyOwnDBLib.Logging.Log.Information($"Public form requested where entity {entityIdInt} doe snot exist or does not accept it")
                return None
            | Error e -> MyOwnDBLib.Logging.Log.Error("Error getting entity id to get entity spec: {0}", e) |> ignore
                         return None
            | Ok r ->
                MyOwnDBLib.Logging.Log.Error($"Error checking has_public_form to display public form, got result {r}")
                return None
    }
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let getInstanceDatatable (instanceId:int) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let! user = signInManager.UserManager.GetUserAsync(httpContext.User)|> Async.AwaitTask
        let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))
        let! instanceIdResult = getInstanceId httpContext instanceId

        match instanceIdResult with
        | Ok instanceId -> let! jsonResult =
                            Instance.getEntryDataTableAction instanceId
                                        |> DBAction.mapDataTableToJSON
                                        |> DBAction.run client
                                        |> Async.map DBResult.toResult
                           match jsonResult with
                           | Ok [dt] -> return (Some dt)
                           | Ok _ ->
                                MyOwnDBLib.Logging.Log.Error("Error getting instance spec: multiple specs returned ??") |> ignore
                                return None
                           | Error e ->
                                MyOwnDBLib.Logging.Log.Error("Error getting instance spec: {0}", e) |> ignore
                                return None
        | Error e -> MyOwnDBLib.Logging.Log.Error("Error getting instance id to get instance spec: {0}", e) |> ignore
                     return None
    }

    open WebSharper.UI.Html
    // This function returns the list of relations in the direction corresponding to the action-returning function passed
    // as first argument. It is not a RPC function, but is called from RPC functions
    let getRelations (directionInfo:RelatedInDirectionForIntInstanceId) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>>))
        let! user = signInManager.UserManager.GetUserAsync(httpContext.User)|> Async.AwaitTask
        let dbClient:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))
        let instanceId,getRelationsAction, directionDU =
            match directionInfo with
            | ChildrenFor instanceId -> instanceId, RelationExtensions.toChildrenForEntityAction , Ids.ChildrenVia
            | ParentsFor instanceId -> instanceId, RelationExtensions.toParentsForEntityAction , Ids.ParentsVia

        let! instanceResult = getInstanceId httpContext instanceId
                                |> AsyncResult.map Instance.byIdAction
                                |> Async.bind (function
                                        | Ok a -> DBAction.run dbClient a
                                        | Error e ->
                                            Async.singleton (DBResult.DBResult.Error (e|>Error.fromString))
                                        )


        let! toChildrenRelations =
            match instanceResult with
            | Error e ->
                MyOwnDBLib.Logging.Log.Error("Error getting Instance with id {id}: {error}", instanceId, e |> Error.toString )
                // FIXME: translated error
                Async.singleton (DBResult.error "an error occured")
            | Ok [ instance ] -> async {
                // get relations
                let! relationsToChildrenResult =
                    getRelationsAction instance.entity_id
                    // get linked instances through each relation
                    |> DBAction.run dbClient
                    |> Async.map (DBResult.map (fun relation ->
                                  (relation, DatatableTypes.Linked {sourceInstanceId=instance.id; direction= directionDU relation.id }))
                    )
                return relationsToChildrenResult
              }
            | _ ->
                MyOwnDBLib.Logging.Log.Error("Unexpected result length when retrieving single instance")
                // FIXME: translated error
                Async.singleton (DBResult.error "an error occured")

        return toChildrenRelations
    }
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let getChildrenRelations (instanceIdIn:int) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let t:web.I18n = getService<web.I18n>()
        let! instanceIdResult = getInstanceId httpContext instanceIdIn

        //(RelationExtensions.toChildrenForEntityAction)
        match instanceIdResult with
        | Ok instanceId ->
            return! getRelations  (ChildrenFor (instanceId|> Ids.InstanceId.Get))
        | Error es ->
            MyOwnDBLib.Logging.Log.Error($"Failed validating instance id to retrieve children relations: {es}")
            return DBResult.error (t.t("An error occured"))
    }
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let getParentsRelations (instanceIdIn:int) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let t:web.I18n = getService<web.I18n>()
        let! instanceIdResult = getInstanceId httpContext instanceIdIn

        // (RelationExtensions.toParentsForEntityAction)
        match instanceIdResult with
        | Ok instanceId ->
            return! getRelations  (ParentsFor (instanceId|>Ids.InstanceId.Get))
        | Error es ->
            MyOwnDBLib.Logging.Log.Error($"Failed validating instance id to retrieve parent relations: {es}")
            return DBResult.error (t.t("An error occured"))
    }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let getEntityOfInstance (id:int)  :Async<Result<list<Entity.Entity>,string>> = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let client = getDBClient ctx
        // retrieve the EntityId, the value is None if no access is granted
        return! getInstanceId httpContext id
                // map option to an Async<Result<>>
                // retrieve details from database
                |> AsyncResult.bind (fun instanceId ->
                    Instance.byIdAction instanceId
                    |> DBAction.bind (fun instance -> Instance.getEntityAction instance)
                    |> DBAction.run client
                    |> Async.map DBResult.toResult
                    |> AsyncResult.mapError (fun l -> String.concat ", " l)
                )
    }

    open Specs
    open Ids



    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let createOrUdpdateInstance (formKind:FormKind)(specFromClient:InstanceSpec) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let client = getDBClient ctx


        // As we get an Async<DBAction<_>>, we assign it with let! to get rid of Async for later pipes
        let! (getEntityIdAction: DBAction.DBAction<EntityId>) =
            match formKind with
            | EditionForm _
            | LinkForm _
            | CreationForm _ ->
                getEntityIdActionAsync ctx (Ids.EntityId.Get specFromClient.entityId)
            | PublicForm _ ->
                getPublicEntityIdActionAsync ctx (Ids.EntityId.Get specFromClient.entityId)

        let! (getInstanceIdAction: DBAction.DBAction<InstanceId option>) =
            match formKind with
            | EditionForm _
            | LinkForm _
            | CreationForm _ ->
                async {return specFromClient.instanceId}
                |> Async.bind (fun idOption ->
                    // we get an option because the instanceId can be None in the case we need to create a new instance.
                    match idOption with
                    // We are in a Async.bind, so we need to return Async<DBAction<_>>
                    | Some id -> async {
                        // This gives us a <Async<DBAction<_>> as we want
                        let! instanceIdAction = getInstanceIdActionAsync ctx (Ids.InstanceId.Get id)
                        // the action returns an instanceId, and an error if it was not found.
                        // we map it to an option in case of success to that downstream we can handle it similarly to
                        // the instance creation where instanceId is None
                        return (instanceIdAction |> DBAction.map (fun id -> Some id))
                        }
                    // if no instanceId is present in the spec, ne check to be done,
                    // we can just return None for downstream to handle it
                    | None -> async {return DBAction.retn None}
                )
            | PublicForm _ ->
                    async {return DBAction.retn None}


        let updateAction =
            getEntityIdAction
            // replace entityId with the one retrieved in the database
            |> DBAction.map (fun entityId -> {specFromClient with entityId=entityId})
            // replace instanceId with the one retrieved in the database
            |> DBAction.bind2 (fun instanceId spec -> DBAction.retn {spec with instanceId=instanceId}) getInstanceIdAction
            // sanitise the rest of the spec
            |> DBAction.bind (fun spec -> MyOwnDBLib.Instance.sanitiseSpecAction spec)
            // then apply it
            |> DBAction.bind (fun sanitisedSpec -> Instance.createOrUpdateAction sanitisedSpec)
            |> DBAction.bind (fun instanceSpec ->
                match formKind with
                | CreationForm _ |EditionForm _ |PublicForm _-> DBAction.retn instanceSpec
                | LinkForm {entityId=entityId;relationId=relationId; dirAndId=dirAndId} ->
                    match dirAndId with
                      | ParentsFor childId ->
                          DBAction.queryAction "insert into links(relation_id,parent_id,child_id) values (%d,%d,%d) returning id" relationId  (instanceSpec.instanceId|> Option.get |> InstanceId.Get) childId
                      | ChildrenFor parentId ->
                          DBAction.queryAction "insert into links(relation_id,parent_id,child_id) values (%d,%d,%d) returning id" relationId  parentId (instanceSpec.instanceId|> Option.get |> InstanceId.Get)
                    // map to return the instanceSpec as before
                    |> DBAction.map (fun _ -> instanceSpec )
            )
        let! closeIfNeeded = client.OpenIfNeeded()
        let! (result:DBResult.DBResult<InstanceSpec>) =
            updateAction
            // This sets a savepoint and rolls back to it in case of error
            |> DBAction.wrapWithSavePointAndRollBackForError
            // This sets up the transaction and rollsback the whole tx in case of error **only if it started the tx**
            |> DBAction.wrapInTransactionIfNeeded
            |> DBAction.run client
        do! closeIfNeeded()
        match result with
        |Ok [spec] ->
          MyOwnDBLib.Logging.Log.Debug("Instance created or updated {instanceId} from {specFromClient}", spec.instanceId, specFromClient)
        |Ok l ->
          MyOwnDBLib.Logging.Log.Error("Got wrong number of instance spec back after an insert or update: {specs}. Spec from client:{specFromClient}", l, specFromClient)
        |Error es ->
          MyOwnDBLib.Logging.Log.Error("Error creating or updating instance {instanceId}: {error}. Spec from client: {specFromClient}", specFromClient.instanceId, es|>Error.toString, specFromClient)
        return result
    }

    open ServerFileAttachment
    open ServerFileAttachmentExtensions
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let deleteAttachment (fileAttachment:ServerFileAttachment.FileAttachment) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let client = getDBClient ctx

        // This will be used to validate access rights to the file attachment
        let accountId:int64 =  Auth.getCurrentUserAccountId httpContext
        let userId = Auth.getCurrentUserId httpContext

        // Raises exception if failing
        let r = PathElements.FromKey (fileAttachment.s3_key|>Option.defaultValue "")
        return!
            (if accountId <> r.account_id then
                MyOwnDBLib.Logging.Log.Warning($"Inconsistent ids when deleting attachment for detail value {r.detail_value_id}. Account id from request's key {r.account_id} is different from the value in the user session {accountId}")
                DBAction.error "Inconsistency detected in request"
             else
                DBAction.retn ()
            )
            |> DBAction.bind (fun _ ->
                // This sql statement validates the id received as argument by joining with tables identifying the current user and its account
                DBAction.statementAction
                    """delete from detail_values where id in (
                                select dv.id from detail_values dv
                                    join instances i on (dv.instance_id=i.id)
                                    join entities e on (i.entity_id = e.id)
                                    join databases db on (e.database_id=db.id)
                                    join accounts acc on (acc.id=%d)
                                    join users u on (u.id=%d)
                                    where dv.id = %d
                                )
                    """ accountId userId (fileAttachment.detail_value_id|>Option.defaultValue -1)
            )
            |> DBAction.bind
                (fun (DBTypes.ImpactedRows n) ->
                    if n = 1 then
                        // delete file on storage
                        try
                            ServerFileAttachment.FileAttachment.withDirsFromConfig (fun _ attachmentsDir ->
                                let filePath = System.IO.Path.Combine(attachmentsDir,buildKey r)
                                System.IO.File.Delete(filePath)
                                DBAction.retn ()
                            )
                        with
                        |e ->
                            MyOwnDBLib.Logging.Log.Error($"Failed deletion of file attachment on storage: {e}")
                            DBAction.error "Deletion failed"
                    else
                        DBAction.error "Deletion failed"
            )
            |> DBAction.run client
            |> Async.map (function
                | Ok _ -> Ok ()
                | Error es -> Error ()

            )
    }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let isEmailValid (s:string) = async{ return s= "" || Email.isValid s }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let isWebURLValid (s:string) = async{ return s= "" || WebURL.isValid s }

    let validateLinkManipulationArguments(relationId:RelationId)(parentIdIn)(childIdIn) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let signInManager:SignInManager<ApplicationUser> = unbox (serviceProvider.GetService(typeof<SignInManager<ApplicationUser>>))
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let client = getDBClient ctx

        let! parentIdResult =  getInstanceId httpContext parentIdIn
        let! childIdResult =   getInstanceId httpContext childIdIn
        let! relationIdOption =
            (GetId(signInManager).RelationId client (httpContext.User) (relationId|> RelationId.Get))
        let relationIdResult =
            relationIdOption
            |> function
                | Some v -> Ok v
                | None -> Error "Error getting RelationId"
        return relationIdResult,parentIdResult,childIdResult

    }
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let linkInstances (relationId:RelationId)(parentIdIn)(childIdIn) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))

        try
          // We authorise access to the data here:
          let! (relationIdResult,parentIdResult,childIdResult) = validateLinkManipulationArguments relationId parentIdIn childIdIn
          let actionResult=
              Result.map3
                  (fun relationId parentId childId ->
                      Link.linkInstancesAction relationId parentId childId
                  )
                  relationIdResult
                  parentIdResult
                  childIdResult

          match actionResult with
          | Error es ->
              MyOwnDBLib.Logging.Log.Error($"Error getting ids to link: {es}")
              return (DBResult.error (t.t "An error occured"))
          | Ok a ->
              let! (r:DBResult.DBResult<LinkId>) =  DBAction.run client a
              return r
        with
        e ->
          raise e
          return DBResult.error (e.ToString())
    }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let unlinkInstances (relationId:RelationId)(parentIdIn)(childIdIn) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))

        // We authorise access to the data here:
        let! (relationIdResult,parentIdResult,childIdResult) = validateLinkManipulationArguments relationId parentIdIn childIdIn
        let actionResult=
            Result.map3
                (fun relationId parentId childId ->
                    Link.unlinkInstancesAction relationId parentId childId
                )
                relationIdResult
                parentIdResult
                childIdResult

        match actionResult with
        | Error es ->
            MyOwnDBLib.Logging.Log.Error($"Error getting ids to link: {es}")
            return (DBResult.error (t.t "An error occured"))
        | Ok a ->
            let! (r:DBResult.DBResult<DBTypes.ImpactedRows>) =  DBAction.run client a
            match r with
            | Error es ->
                MyOwnDBLib.Logging.Log.Error($"Error unlinking instances: {es}")
                return (DBResult.error (t.t "An error occured"))
            | Ok [DBTypes.ImpactedRows 1] ->
                return (DBResult.retn ())
            | Ok [] ->
                return (DBResult.error (t.t "Link not found"))
            | Ok n ->
                MyOwnDBLib.Logging.Log.Error($"Unexpected impacted rows unlinking instances: {n}")
                return (DBResult.error (t.t "An error occured"))
    }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let deleteInstance (id:int) :Async<DBResult.DBResult<unit>>= async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let serviceProvider= httpContext.RequestServices
        let t:web.I18n = unbox (serviceProvider.GetService(typeof<web.I18n>))
        let client:DB.DBClient = unbox (serviceProvider.GetService(typeof<DB.DBClient>))
        let attachmentsCleaner:IAttachmentsCleaner = unbox (serviceProvider.GetService(typeof<IAttachmentsCleaner>))

        // We authorise access to the data here:
        let! instanceIdResult =   getInstanceId httpContext id
        match instanceIdResult with
        | Error e ->
            MyOwnDBLib.Logging.Log.Warning($"Error getting instance id {id} for user {Auth.getCurrentUserId httpContext}")
            return (DBResult.error (t.t "An error occured"))
        | Ok instanceId ->
            return!
                // First retrieve info from the database to be able to delete existing attachments
                // Get the id of the detail values
                DBAction.queryAction "select id from detail_values where instance_id=%d and type='FileAttachmentDetailValue'" (instanceId|>InstanceId.Get)
                // With the ids of the detail values, retrieve the info from the database to be able to build the path to the file stored on disk
                |> DBAction.bind (fun (intDetailValueId:int32) ->
                    DetailValue.getPathElementsAction intDetailValueId (instanceId|>InstanceId.Get)
                )
                // With the info retrieved from disk, build the paths of the file to be deleted if the instance is deleted successfully
                |> DBAction.bind (fun pathElements ->
                    FileAttachment.withDirsFromConfig
                        (fun _waitRoom attachmentsDir ->
                            DetailValue.buildAttachmentPath attachmentsDir pathElements
                            |> DBAction.retn
                    )
                )
                // Work on list so the next bind function gets the list of paths
                |> DBAction.workOnList
                |> DBAction.bind (fun pathsToDelete ->
                    // Now we have the paths of the file attachments, we can delete the instance from the database
                    Instance.deleteAction instanceId
                    // We get back the number of rows impacted
                    |> DBAction.bind (fun impactedRows ->
                        match impactedRows with
                        // We should get 1, and in this case we can delete the file attachments
                        | (DBTypes.ImpactedRows 1) ->
                            MyOwnDBLib.Logging.Log.Debug("need to clean up file attachments {pathsToDelete}", pathsToDelete)
                            pathsToDelete
                            |> List.iter(fun path ->
                                // Send the request to delete the file attachment at path
                                attachmentsCleaner.Clean(path)
                            )
                            // We return unit as we don't have anything meaningfull to return here
                            DBAction.retn ()
                        // Other results are suspicious. Return an error which will be logged below
                        | (DBTypes.ImpactedRows n)->
                            DBAction.error ($"Inconsistent impacted rows when deleting the instance with id {id}: {n} for user {Auth.getCurrentUserId httpContext}"))
                )

                // Now issued the queries
                |> DBAction.run client
                // And map the possible error to something cleaner for the user, and log it too for us to debug.
                |> Async.map (DBResult.mapError (fun es ->
                        MyOwnDBLib.Logging.Log.Error($"Error deleting instance {id}: {es |> Error.toString}")
                        Error.fromString (t.t "An error occured")
                ))

    }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let handleImportUpload (entityIdInt:int)(fileName:string) (fileContent:byte array) =
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let userName = httpContext.User.Identity.Name
        let accountId = ServerHelpers.Auth.getCurrentUserAccountId httpContext
        let config= getService<IConfiguration>()
        let dbClient = getDBClient ctx
        let baseDir = config.GetValue("myowndb:imports:path")
        let uploadId = string (System.Random.Shared.NextInt64())
        let uploadDir = System.IO.Path.Combine(baseDir, string accountId, string entityIdInt, uploadId)
        let _dirInfo = System.IO.Directory.CreateDirectory(uploadDir)
        let uploadPath = System.IO.Path.Combine(uploadDir,fileName)
        async {
            do! System.IO.File.WriteAllBytesAsync(uploadPath, fileContent) |> Async.AwaitTask
            // Validate access to the entity
            let! entityIdAction = getEntityIdActionAsync ctx entityIdInt
            return!
              entityIdAction
              |> DBAction.bind (fun entityId ->
                  // Initialise the import and load it from database, also keeping entityId available to next steps
                  ImportExtensions.initialiseAction entityId userName
                  |> DBAction.bind (fun importId -> ImportExtensions.byIdAction importId)
                  |> DBAction.map (fun import -> entityId,import)
              )
              // Handle uploaded file, extracting its columns
              |> DBAction.bind (fun (entityId,import) ->
                  ImportExtensions.afterFileUploadAction import uploadPath fileName "text/csv"
                  |> DBAction.map (fun columns -> columns,entityId,import)
              )
              // Return columns from the csv and the details of the specs for form display
              |> DBAction.bind (fun (columns,entityId,import) ->
                  detailSpecOfEntity entityId
                  |> DBAction.map (fun detailsSpecs -> (import.id,detailsSpecs, columns))
              )
              |> DBAction.run dbClient
        }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let importFileWithMappings (importId:Ids.ImportId) (mappings:ImportMapping.Mapping list) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let userName = httpContext.User.Identity.Name
        let accountId = ServerHelpers.Auth.getCurrentUserAccountId httpContext
        let config= getService<IConfiguration>()
        let dbClient = getDBClient ctx
        let! closeIfNeeded = dbClient.OpenIfNeeded()
        let! r=
            // Find Import entry in database
            ImportExtensions.byIdForImportAction importId (AccountId accountId) userName
            |> DBAction.mapError( fun e ->
              MyOwnDBLib.Logging.Log.Error($"""Problem finding import with id {sprintf "%A" importId} for account {accountId} and user {userName}: {e|>Error.toString}""")
              e
            )
            // Execute the import, returning just the instance ids created
            |> DBAction.bind (fun import ->
              ImportExtensions.importFileAction import mappings
              |> DBAction.map (fun instanceSpecs ->
                instanceSpecs |> List.map (fun spec -> spec.instanceId)
              )
            )
            |> DBAction.run dbClient
        do! closeIfNeeded()
        return r

    }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let undoImport (importId:Ids.ImportId)  = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let userName = httpContext.User.Identity.Name
        let accountId = ServerHelpers.Auth.getCurrentUserAccountId httpContext
        let signInManager= getService<SignInManager<ApplicationUser>>()
        let dbClient = getDBClient ctx
        let! closeIfNeeded = dbClient.OpenIfNeeded()
        let! r=
            // Find Import entry in database
            ImportExtensions.byIdForUndoAction importId (AccountId accountId) userName
            |> DBAction.mapError( fun e ->
              MyOwnDBLib.Logging.Log.Error($"""Problem finding import with id {sprintf "%A" importId} for account {accountId} and user {userName}: {e|>Error.toString}""")
              e
            )
            // Execute the import, returning just the instance ids created
            |> DBAction.bind (fun import ->
                GetId(signInManager).EntityIdAction (httpContext.User) (import.entity_id|>EntityId.Get)
                |> DBAction.bind (fun entityId ->
                  ImportExtensions.undoImportAction import entityId
                  |> DBAction.map (fun (DBTypes.ImpactedRows c) -> c)
                )
            )
            |> DBAction.run dbClient
        do! closeIfNeeded()
        return r

    }

    open NotificationSubscriptionCriteria
    [<JavaScript>]
    type NotificationSubscriptionSpec =
      {
        id: Ids.NotificationSubscriptionId
        protocol: Protocol
        event: Event
        destination: string
    }
    let toSpec userName (subs:NotificationSubscription.NotificationSubscription) =
        {
          id= subs.id
          event= subs.event
          protocol= subs.protocol
          destination=
            (match subs.destination_type with
              // Currently only notification to self can be set. This will need more
              // work if we expand the possible recipients.
              | User -> userName
            )
        }
    // Returns the list of notification subscriptions for the current user. Thus each user has a different result here.
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let getInstanceNotifications (entityId:int)  = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let userName = httpContext.User.Identity.Name
        let t:web.I18n = getService<web.I18n>()
        let dbClient = getDBClient ctx
        let! closeIfNeeded = dbClient.OpenIfNeeded()
        let userId = Auth.getCurrentUserId httpContext

        let! entityIdAction =
            getEntityIdActionAsync ctx entityId
        let! r=
            entityIdAction
            |> DBAction.bind (fun entityId ->
                NotificationSubscription.GetUserSubscriptionsFor (Ids.UserId userId) (NotificationSubscriptionCriteria.Source.Instance) (NotificationSubscriptionCriteria.SourceFilter.Entity entityId)
            )
            |> DBAction.map (toSpec userName)
            |> DBAction.mapError (fun err ->
                MyOwnDBLib.Logging.Log.Error("Problem getting user instance notifications for user {user}: {error}", userName,(err|>Error.toString))
                Error.fromString (t.t("An error occurred"))
            )
            |> DBAction.run dbClient
        do! closeIfNeeded()
        return r

    }

    // Create a notification subscription for the current user.
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let createInstanceNotification (entityId:int) (event:Event) (protocol:Protocol) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let userName = httpContext.User.Identity.Name
        let t:web.I18n = getService<web.I18n>()
        let dbClient = getDBClient ctx
        let! closeIfNeeded = dbClient.OpenIfNeeded()
        let userId = Auth.getCurrentUserId httpContext

        let! entityIdAction =
            getEntityIdActionAsync ctx entityId
        let! r=
            entityIdAction
            |> DBAction.bind (fun entityId ->
                NotificationSubscription.SubscribeInstanceCreationMailAction entityId (Ids.UserId userId)
            )
            |> DBAction.map (toSpec userName)
            |> DBAction.mapError (fun err ->
                // We retrieve the exceptions to check if it is due to a duplicated subscription creation attemps, which
                // we want to report clearly to the user
                let exceptions = err |> Error.getExceptions
                match exceptions with
                // The postgresql code is stored under Data["SqlState"]. 23505 is a uniqueness violation
                | [e] when (e.GetBaseException().Data.Contains("SqlState") && e.GetBaseException().Data["SqlState"]="23505") ->
                  // When we have a duplicate notification error, report it but don't log it
                  Error.fromString (t.t("This subscription already exists"))
                | _ ->

                  // This is an undetermined exception, log it
                  MyOwnDBLib.Logging.Log.Error("Problem creating user instance notifications for user {user}: {error}", userName,(err|>Error.toString))
                  Error.fromString (t.t("An error occurred"))
            )
            |> DBAction.run dbClient
        do! closeIfNeeded()
        return r

    }
    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let deleteInstanceNotification (notificationId:Ids.NotificationSubscriptionId) = async {
        let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let userName = httpContext.User.Identity.Name
        let t:web.I18n = getService<web.I18n>()
        let dbClient = getDBClient ctx
        let! closeIfNeeded = dbClient.OpenIfNeeded()
        let userId = Auth.getCurrentUserId httpContext

        let! r=
            NotificationSubscription.RemoveAction notificationId
            // Map to unit, as we have no info to convey and DBTypes.ImpactedRows is not known client side
            |> DBAction.bind (fun (DBTypes.ImpactedRows n) ->
                match n with
                | 1 -> DBAction.retn ()
                | 0 -> DBAction.error (t.t("Not found"))
                | i ->
                  MyOwnDBLib.Logging.Log.Error("Error deleting notification subscription for {user}, {impactedRows} rows impacted where 1 expected", userName, i)
                  DBAction.error (t.t("An error occurred"))
            )
            |> DBAction.mapError (fun err ->
                MyOwnDBLib.Logging.Log.Error("Problem deleting user instance notifications for user {user}: {error}", userName,(err|>Error.toString))
                Error.fromString (t.t("An error occurred"))
            )
            |> DBAction.run dbClient
        do! closeIfNeeded()
        return r

    }
