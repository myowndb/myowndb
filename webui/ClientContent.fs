namespace web
open WebSharper
open WebSharper.UI
open WebSharper.UI.Html
// needed for UI.Html.attr.disableDynPred
open WebSharper.UI.Client
open WebSharper.JavaScript
open ClientI18n
open WSExtensions
open DatatableTypes

open MyOwnDBHelp
[<JavaScript>]
module ClientContent =
    // As we update the page's body, the title is not updated. This functions updates it with javascript
    let setPageTitleAsync (title:string) (docAsync:Async<Doc>)= async {
      let! doc = docAsync
      return
        doc
        |> Doc.Prepend (
            span
              [on.afterRender(fun _ ->JavaScript.JS.Inline("""var title = $0; document.title = title;""", title))]
              []
        )
    }

    // This function add an h1 with the title, and sets the page's HTML title too
    let setTitleHeaderAndBodyAsync (title:string)(docAsync:Async<Doc>)=
      docAsync
      |> Doc.PrependPipedAsync (h1 [HtmlHelpers.classes ["myowndb-page-title"]] [text title])
      |> setPageTitleAsync title


    // This renders the body of the page with Javascript, allowing client-side navigation.
    // I chooses the page to render according to the installed router's state.
    let clientBody () =
        async {
            let view = ClientRouting.installedRouter.View
                        |> View.MapAsync
                            (fun e ->   async {
                            try
                              MyOwnDBHelp.resetTour()
                              match e with
                                |Home ->
                                  [|
                                    "#menu_entry_Home", th.tHelp("Home"), th.tHelp("madb_home_menu_help")
                                    "#Databases", th.tHelp("Databases list"), th.tHelp("This is the list of the existing databases")
                                    "#Admin", th.tHelp("Admin actions"), th.tHelp("This gives you access to admin sections of the app")
                                    "#sidebarToggle", th.tHelp("Sidebar toggle"), th.tHelp("Toggle the sidebar display by clicking this icon. A spinner displays besides this icon when the app is waiting for the response to a request.")
                                  |]
                                  |>Tour.setPageTourFromTuples
                                  return!
                                    // -- TRANSLATORS: Global help displayed on the homepage. Markdown formatted, separate paragraphs with "\n\n"
                                    async { return div [] [th.helpBlock("home_page_help")] }
                                    |> setPageTitleAsync (t.t "Home")
                                    // Disabled as it made some tests fail when trying to log out from the homepage.
                                    // Leaving it here as I might want to re-enable it, and it's a good reference if it is needed somewhere else
                                    // searching the git history to find it back is more cumbersome in my experience.
                                    //|> Doc.PrependPipedAsync
                                    //  (
                                    //    span
                                    //      [ on.afterRender
                                    //          (fun _el ->
                                    //            WebSharper.JavaScript.JS.Inline ("""
                                    //              // We need to assign it to a variable as WS further replaces window.driverObj
                                    //              // by driverObj, which is undefined.
                                    //              var d = window.driverObj
                                    //              d.highlight($0);
                                    //            """,
                                    //            { element = "#pageTourIcon";
                                    //              popover={
                                    //                title=th.tHelp("Getting help");
                                    //                description=th.tHelp("madb_help_icon_highlight")
                                    //              }})
                                    //          )
                                    //      ]
                                    //      []
                                    //  )
                                |Instances (id,state) ->
                                    // retrieve the page info
                                    let! pageInfoResult = web.PageInfo.Instances.pageInfo (int id)
                                    // If the PageInfo could not be retrieved, it raises an error catched right below to display an error message
                                    // A proper error is logged at the server side
                                    let pageInfo = pageInfoResult|> DBResult.getFirst
                                    [|
                                      "#add_instance_section", th.tHelp("Add instance"), th.tHelp("madb_add_instance_section")
                                      ".myowndbimportcsv", th.tHelp("Import a CSV file"), th.tHelp("madb_import_csv")
                                      ".myowndbmanagenotifications", th.tHelp("Manage your notifications"), th.tHelp("madb_manage_notifications")
                                      ".myowndb-table-rows-count", th.tHelp("Number of rows"), th.tHelp("madb_table_number_of_rows")
                                      ".myowndbpaginationfirst", th.tHelp("To first page"), th.tHelp("madb_table_pagination_first")
                                      ".myowndbpaginationprevious", th.tHelp("To previous page"), th.tHelp("madb_table_pagination_previous")
                                      ".myowndbpaginationnumber.active", th.tHelp("Current page"), th.tHelp("madb_table_pagination_current")
                                      ".myowndbpaginationnext", th.tHelp("To next page"), th.tHelp("madb_table_pagination_next")
                                      ".myowndbpaginationlast", th.tHelp("To last page"), th.tHelp("madb_table_pagination_last")
                                      ".myowndbperpageddl", th.tHelp("Rows per page"), th.tHelp("madb_table_rows_per_page")
                                      ".myowndbsearchddl", th.tHelp("Filter on this detail"), th.tHelp("madb_table_search_column")
                                      ".myowndbsearchvalue", th.tHelp("Search for this value"), th.tHelp("madb_table_search_value")
                                      ".myowndbclearsearch", th.tHelp("Clear search"), th.tHelp("madb_table_clear_search")
                                      ".myowndb_csv_download_link", th.tHelp("Download CSV"), th.tHelp("madb_table_download_csv")
                                      ".myowndbrefreshtable", th.tHelp("Reload data"), th.tHelp("madb_table_refresh")
                                      "div.th-inner.sortable", th.tHelp("Sort rows"), th.tHelp("madb_table_header_sort_rows")
                                      """[data-myowndb-action="view"]""",  th.tHelp("View instance"), th.tHelp("madb_view_instance")
                                      """[data-myowndb-action="edit"]""",  th.tHelp("Edit instance"), th.tHelp("madb_edit_instance")
                                      """[data-myowndb-action="delete"]""",  th.tHelp("Delete instance"), th.tHelp("madb_delete_instance")
                                    |]
                                    |>Tour.setPageTourFromTuples
                                    // Setup state var and history recording
                                    let stateVar:Var<PageState.Datatable.State option>= PageState.setupPageState state (fun stringOption -> Instances(id,stringOption))
                                    return!
                                      Datatable.displayMainListTable id pageInfo stateVar
                                      |> setTitleHeaderAndBodyAsync pageInfo.entity.name
                                |Instance (id,state) ->
                                    // Setup state var and history recording
                                    let stateVar:Var<PageState.InstanceDisplay.State option>= PageState.setupPageState state (fun stringOption -> Instance(id,stringOption))
                                    // retrieve the page info
                                    let! pageInfoResult = web.PageInfo.Instance.pageInfo (int id)
                                    // If the PageInfo could not be retrieved, it raises an error catched right below to display an error message
                                    // A proper error is logged at the server side
                                    let pageInfo = pageInfoResult|> DBResult.getFirst
                                    // -- translator: args are entity name and the instance id
                                    let title = (t.t("%1 details (%2)",pageInfo.entity.name,string id))
                                    return!
                                      InstanceDisplay.displayInstance id pageInfo stateVar
                                      |> setTitleHeaderAndBodyAsync title
                                |EditInstance id ->
                                    return!
                                      InstanceForm.displayEdit id FormHelpers.displaySavedMessageInGrid
                                      |> setTitleHeaderAndBodyAsync  (t.t "Record edition")
                                |CreateInstance id ->
                                    return!
                                      InstanceForm.displayCreate id FormHelpers.displaySavedMessageInGrid
                                      |> setTitleHeaderAndBodyAsync (t.t "Record creation")
                                // The responsive argument is used by the page template, not here.
                                |PublicForm (entityId,_responsive) ->
                                    return!
                                      InstanceForm.displayPublic entityId FormHelpers.displaySavedMessageInGrid
                                |PublicFormSubmitted _entityId ->
                                        return
                                            div []
                                                [
                                                    h1 [] [t.tt("Information registered")]
                                                    div [] [t.tt("Your form has been successfully registered")]
                                                ]
                                |Import entityId ->
                                    // retrieve the page info
                                    let! pageInfoResult = web.PageInfo.Import.pageInfo entityId
                                    // If the PageInfo could not be retrieved, it raises an error catched right below to display an error message
                                    // A proper error is logged at the server side
                                    let pageInfo = pageInfoResult|> DBResult.getFirst
                                    [|
                                      ".myowndbfileselection", th.tHelp("Select the file"), th.tHelp("madb_select_file_to_import")
                                      ".myowndbuploadfile", th.tHelp("Upload the file"), th.tHelp("madb_upload_file_to_import")
                                      ".myowndbcsvmappingrow", th.tHelp("Mapping to a detail"), th.tHelp("madb_one_row_maps_one_detail")
                                      ".myowndbdestinationdetail", th.tHelp("Destination"), th.tHelp("madb_csv_detail_destination")
                                      ".myowndbcsvfieldchoice", th.tHelp("Make your choice"), th.tHelp("madb_choose_csv_column_to_map")
                                      ".myowndbdoimport", th.tHelp("Start the import"), th.tHelp("madb_pressing_this_button_will_start_the_import")
                                    |]
                                    |>Tour.setPageTourFromTuples
                                    return!
                                      ImportPage.display entityId pageInfo
                                |Admin ["users"] ->
                                    return!
                                      web.admin.UsersDisplay.pageBody()
                                      |> setTitleHeaderAndBodyAsync (t.t "Users administration")
                                |Admin ["databases"] ->
                                    [|
                                      ".myowndb-btn-add-db", th.tHelp("Add a database"), th.tHelp("madb_add_database_button")
                                      "#admin_databases_form", th.tHelp("Database form"), th.tHelp("madb_database_form")
                                      "#admin_databases_table", th.tHelp("Databases list"), th.tHelp("madb_database_table")
                                      """[data-myowndb-action="edit"]""",  th.tHelp("Rename database"), th.tHelp("madb_rename_database")
                                      """[data-myowndb-action="view"]""",  th.tHelp("Manage database"), th.tHelp("madb_manage_database")
                                    |]
                                    |>Tour.setPageTourFromTuples
                                    return!
                                      web.admin.DatabasesDisplay.pageBody()
                                      |> setTitleHeaderAndBodyAsync (t.t "Databases administration")
                                |Admin ["database";databaseId] ->
                                    [|
                                      "h1.myowndb-page-title", th.tHelp("Current database"), th.tHelp("madb_database_admin_title")
                                      ".myowndb-entities-section", th.tHelp("Entities of the database"), th.tHelp("madb_database_admin_entities_section")
                                      """[data-myowndb-action="togglePublicForm"]""",  th.tHelp("Public form"), th.tHelp("madb_admin_entities_toggle_public_form")
                                      """[data-myowndb-action="edit"]""",  th.tHelp("Edit entity"), th.tHelp("madb_admin_entities_edit")
                                      """[data-myowndb-action="view"]""",  th.tHelp("Manage entity"), th.tHelp("madb_admin_entities_view")
                                      ".myowndb-details-section", th.tHelp("Details of the database"), th.tHelp("madb_database_admin_details_section")
                                      """.myowndb-details-section span[data-myowndb-action="edit"]""", th.tHelp("Rename detail"), th.tHelp("madb_database_admin_details_rename")
                                      ".myowndb-detail-datatype", th.tHelp("Datatype of the detail"), th.tHelp("madb_database_admin_details_datatype")
                                      ".myowndb-detail-propositions", th.tHelp("Propositions for the detail"), th.tHelp("madb_database_admin_details_propositions")
                                      ".myowndb-detail-entities", th.tHelp("Entities using the detail"), th.tHelp("madb_database_admin_details_entities")
                                    |]
                                    |>Tour.setPageTourFromTuples
                                    // retrieve the page info
                                    let! pageInfoResult = web.PageInfo.Admin.Database.pageInfo (int databaseId)
                                    // If the PageInfo could not be retrieved, it raises an error catched right below to display an error message
                                    // A proper error is logged at the server side
                                    let pageInfo:PageInfo.Admin.Database.PageInfo = pageInfoResult|> DBResult.getFirst
                                    // -- translator: args are entity name and the instance id
                                    let title = pageInfo.database.name
                                    return!
                                      web.admin.DatabaseDisplay.pageBody(int databaseId)(pageInfo)
                                      |> setTitleHeaderAndBodyAsync title
                                |Admin ["entity";entityId] ->
                                    [|
                                      ".myowndbdetailname", th.tHelp("Entity detail"), th.tHelp("madb_entity_detail_name")
                                      ".myowndbdisplayinlistview", th.tHelp("Display in list"), th.tHelp("madb_entity_detail_in_list_view")
                                      ".myowndbentitydetailremove", th.tHelp("Detach from entity"), th.tHelp("madb_entity_detail_detach")
                                      """[data-myowndb-action="up"]""",  th.tHelp("Move up"), th.tHelp("madb_entity_detail_move_down")
                                      """[data-myowndb-action="down"]""",  th.tHelp("Move down"), th.tHelp("madb_entity_detail_move_up")
                                    |]
                                    |>Tour.setPageTourFromTuples
                                    // retrieve the page info
                                    let! pageInfoResult = web.PageInfo.Admin.Entity.pageInfo (int entityId)
                                    // If the PageInfo could not be retrieved, it raises an error catched right below to display an error message
                                    // A proper error is logged at the server side
                                    let pageInfo = pageInfoResult|> DBResult.getFirst
                                    return!
                                      web.admin.EntityDisplay.pageBody(int entityId) pageInfo
                                      // -- translator: arg is entity name. Title of the page displayed in browser tab for entity administration page
                                      |> setTitleHeaderAndBodyAsync (t.t("Admin %1",pageInfo.entity.name))
                                | _  ->  return div [] [t.tt "not implemented"]
                            with ex ->
                                JavaScript.Console.Log($"""Exception client side: {sprintf "%A" ex}""")
                                return  (HtmlHelpers.dangerBox (t.tt("An error occured")))
                            })
            return view |> Client.Doc.EmbedView
        } |> WebSharper.UI.Client.Doc.Async
