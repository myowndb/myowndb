function spread_ngettext(i18n,singular,plural,count,args) {
    return i18n.ngettext(singular,plural,count,...args)
}

function spread_gettext(i18n,string,args) {
    return i18n.gettext(string,...args)
}

// Return translation of an msgid in a particular domain.
// Currently does not support plurals or context as it is mailnly used for MyOwnDB help messages
// in the domain "myowndbhelp".
function spread_dgettext(i18n,domain,msgid,args) {
    // parameters are domain, context, msig, msid_plural, number, translation arguments,...
    return i18n.dcnpgettext(domain, undefined, msgid, undefined, undefined,...args)
}
