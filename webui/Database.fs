namespace MyOwnDB.WS
open AppConfig

type Database(path:string, connectionStringAppend:string)  =
    let config = (AppConfig.load path |> AppConfig.pgConnectionString ) + connectionStringAppend
    let client =
        DB.DBClient(config)
    new(path:string) = Database(path,"")
    member _.Get() = client
    member _.ConnectionString() =  client.ConnectionString()
