﻿namespace web

open WebSharper
open WebSharper.JavaScript
open WebSharper.UI
open WebSharper.UI.Client
open WebSharper.UI.Html
open WebSharper.UI.Html.Tags
open WebSharper.Forms
open Microsoft.FSharp.Collections
open Css
open HtmlHelpers
open ClientI18n

[<JavaScript>]
module Client =

    let languageDropdown (requestCulture:string*string) (culturesNames:(string*string) list) =
        let culturesItems = requestCulture::culturesNames
                            |> List.mapi (fun i (c,v) ->
                                option [ attr.value v; if i=0 then attr.selected "selected"] [ text c ] )
        select [attr.id "languageSelect"; on.change (fun el _ev->
            async {
                let newCulture = (el.GetJS [|"value"|]).ToString() ;
                do! Server.setCultureCookie newCulture
                JS.Window.Location.Reload()
            }|> Async.Start
        )
        ] culturesItems

    let getT (m:Map<string,string>) =
        fun (s:string) ->
            if m.ContainsKey s then
                m.Item s
            else
                s
    let loginForm (forgotPasswordLink:string) (returnUrl: string) =
        let email = Var.Create ""
        let password = Var.Create ""
        Form.Return( fun email pass -> email,pass)
        <*> (Form.YieldVar email
             |> Validation.IsNotEmpty (t.t("Please enter your email address.")))
        <*> (Form.YieldVar password
             |> Validation.IsNotEmpty (t.t("Please enter your password.")))
        |> Form.WithSubmit
        |> Form.MapToAsyncResult (fun (email,pass) ->
            async {
                let! authenticated = Server.authenticate email pass
                if authenticated  then
                    JS.Window.Location.Assign returnUrl
                    return (Success (email,pass))
                else
                    return  (Failure [ErrorMessage.Create(id="failed",text=t.t("Authentication failed. Check your credentials and try again or reset your password."))])
            }
        )
        |> Form.TransmitView
        |>Form.Render (fun email pass submit authView ->
            let submitOnEnter () = on.keyUp (fun el ev -> if ev.KeyCode = 13 then submit.Trigger())
            div [] [
                authView.ShowErrors (fun errors ->
                    match errors with
                    | [] -> Doc.Empty
                    | _ -> errors
                            |> Seq.map (fun m -> p [] [text m.Text])
                            |> Seq.cast
                            |> Doc.Concat
                            |> Seq.singleton
                            |> div [ attr.id "errorBox"; classes [BS.alert; BS.``alert-danger``] ] )

                div [classes [BS.``mb-3``]] [label [] [text (t.t("Email"))]; Doc.InputType.Text [submitOnEnter(); attr.id "emailInput"; classes [BS.``form-control``]; attr.``type`` "email"] email]
                div [classes [BS.``mb-3``]] [label [] [text (t.t("Password"))]; Doc.InputType.Password [submitOnEnter(); attr.id "passwordInput"; classes [BS.``form-control``]] pass]
                Doc.Button (t.t("Send")) [attr.id "submitLogin"; attr.``class`` (BS.btn + " " + BS.``btn-primary`` + " " + BS.``w-100``)] submit.Trigger
                a [attr.href forgotPasswordLink] [text (t.t("Forgot Password"))]
            ]
        )

    open PasswordValidators
    let emailField emailVar=
        let emailValidationMessage = t.t("Please enter your email address. The current value is not recognised as a valid address.")
        Form.YieldVar emailVar
            |> Validation.IsNotEmpty (emailValidationMessage)
            |> Validation.IsMatch @"^.+@.+\..+$" (emailValidationMessage)
    let passwordAndConfirmationFields(passwordValidationMsg:string, confirmationValidationMsg:string)=
        let pass = Var.Create ""
        let conf = Var.Create ""
        Form.Return (fun password confirmation -> password,confirmation)
        <*> (Form.YieldVar pass
             |> Validation.IsNotEmpty passwordValidationMsg)
        <*> (Form.YieldVar conf
             |> Validation.IsNotEmpty confirmationValidationMsg)

    let isPasswordUnconfirmed = View.Map2 (fun pass conf -> pass<>conf || conf="")
    let passwordConfirmationClasses = View.Map (fun unconfirmed -> if unconfirmed then
                                                                     BS.``is-invalid`` + " " +
                                                                     BS.``form-control``
                                                                   else
                                                                     BS.``is-valid`` + " " +
                                                                     BS.``form-control``)
    let checkboxValidClasses = View.Map (fun isChecked -> if isChecked then
                                                                     BS.``is-valid``
                                                                   else
                                                                     BS.``is-invalid``)
    let passwordFormButtonClasses = View.Map (fun unsubmittable -> if unsubmittable then [BS.btn; BS.``btn-danger``;BS.``w-100``] else [BS.btn; BS.``btn-primary``;BS.``w-100``]
                                                                    |> String.concat " ")
    let passwordFormButtonText (v:View<bool>)=  v|> View.Map (fun unsubmittable -> if unsubmittable then (t.t("The form has errors")) else (t.t("Send")))

    let passwordCriteriaDiv () =
                div [attr.id "passwordRequirements"] [
                        p [] [t.tt("Your password must have the following characteristics:")]
                        ul [] [
                            li [] [
                                t.tt("Be at least 8 characters long")]
                            li [] [
                                t.tt("Have at least one lowercase letter")]
                            li [] [
                                t.tt("Have at least one uppercase letter")]
                            li [] [
                                t.tt("Have at least one digit")]
                            li [] [
                                t.tt("Include at least one special character")]
                            li [] [
                                t.tt("The confirmation must be equal to the password")]
                        ]
                ]
    let passwordFormErrorOrCriteria (resultView:View<Result<string * string>>) (passwordCriteria:Doc) =
        resultView.ShowErrors (fun errors ->
                                        match errors with
                                        | [] -> passwordCriteria
                                        | _ -> errors
                                                |> Seq.map (fun m -> li [] [text m.Text])
                                                |> ul []
                                                |> Seq.singleton
                                                |> div [ attr.id "errorBox"; classes [BS.alert; BS.``alert-danger``] ] )

    let passwordFormOrNotice (resultView:View<Result<string * string>>) form notice=
        resultView.Map (fun v ->
                            match v with
                            | Success (email,submitted) -> notice
                            | _ -> form
                        ) |> Doc.EmbedView
    let setNewPasswordForm (email:string) (token:string) =
        let confirmationMessage = t.t("The confirmation must be equal to the password")
        passwordAndConfirmationFields(t.t("Password is invalid"), confirmationMessage)
        |> Form.WithSubmit
        |> Form.MapToAsyncResult (fun (p,c) ->
            async {
                if p<>c then
                    return  (Failure [ErrorMessage.Create(id="different",text=confirmationMessage)])
                else
                    let! result = Server.changePassword email p token
                    match result with
                    | Ok() -> return Success(p,c)
                    | Error s ->
                        let errors = s |> List.map (fun (code,msg) -> ErrorMessage.Create(id=code,text=msg))
                        return  (Failure errors)
            }
        )
        |> Form.TransmitView
        |> Form.Render (fun pass conf submit resultView ->
            // attribute to submit form on enter press
            let submitOnEnter () = on.keyUp (fun el ev -> if ev.KeyCode = 13 then submit.Trigger())
            // view for reporting invalid confirmation of password
            // ---------------------------------------------------
            // consider confirmation invalid if it is empty or different to password
            let passwordUnconfirmed = isPasswordUnconfirmed pass.View conf.View
            // mark the confirmation field as invalid accordingly
            let confirmationClasses = passwordConfirmationClasses passwordUnconfirmed
            // display red button if confirmation is invalid
            let buttonClasses = passwordFormButtonClasses passwordUnconfirmed // Display error message as button text if confirmation invalid
            let buttonText = passwordFormButtonText passwordUnconfirmed
            // Also disable the button if the confirmation is invalid
            let button = View.Map (fun t ->
                                    Doc.Button (t) [  attr.id "submitNewPassword"; attr.disabledDynPred (View.Const "") (passwordUnconfirmed)  ; attr.``class`` (buttonClasses.V) ] submit.Trigger
                                    )
                                   buttonText
                         |> Doc.EmbedView
            // defining the password criteria list
            let passwordCriteria = passwordCriteriaDiv ()
            // handle errors reported by server
            // if no error is reported, display the password criteria (done at first page load)
            let errorsOrCriteria = passwordFormErrorOrCriteria resultView passwordCriteria
            // submission form
            let form = div [] [
                        div [classes [BS.``mb-3``]] [
                            label [] [t.tt("New Password")]
                            Doc.InputType.Password [submitOnEnter(); attr.id "passwordInput"; classes [BS.``form-control``]; attr.``type`` "password"] pass]
                        div [attr.``class`` BS.``mb-3``] [
                            label [] [t.tt("Password Confirmation")]
                            Doc.InputType.Password [submitOnEnter(); attr.id "confirmationInput"; attr.``class`` confirmationClasses.V; attr.``type`` "password"] conf]
                        button
            ]
            // The notice will replace the form after successful submission
            // It includes a login form so the user can immediately log in.
            // The login for displayed does not include a forgot password link.
            let notice =
              [
                div
                  [ attr.id "infoBox"; classes [BS.alert; BS.``alert-info``] ]
                  [
                    t.tt("Your password was updated, you can now try to log in!")
                  ]
                loginForm (Routing.router.Link ForgotPassword) "/"
              ]
              |> Doc.Concat

            // all the above is inserted in the page by this:
            div [] [
                errorsOrCriteria
                passwordFormOrNotice resultView form notice
            ]
        )

    let forgotPasswordForm  () =
        let email = Var.Create ""
        let displayErrors errors=
                    match errors with
                    | [] -> Doc.Empty
                    | _ -> errors
                            |> Seq.map (fun (m:ErrorMessage) -> p [] [text m.Text])
                            |> Seq.cast
                            |> Doc.Concat
                            |> Seq.singleton
                            |> div [ attr.id "errorBox"; classes [BS.alert; BS.``alert-danger``] ]
        // assign to var as used multiple times
        Form.Return (fun email -> email)
        <*> (emailField email)
        |> Form.WithSubmit
        |> Form.MapToAsyncResult (fun email ->
            async {
                do! Server.resetPassword email
                return Success(email,true)
            }
        )
        |> Form.TransmitView
        |> Form.Render (fun email submit resetView ->
            let submitOnEnter () = on.keyUp (fun el ev -> if ev.KeyCode = 13 then submit.Trigger())
            div [] [
                Doc.EmbedView (resetView.Map (fun v ->
                                        match v with
                                        | Success (email,submitted) -> div [ classes [BS.alert; BS.``alert-info``] ] [t.tt("You will receive a mail shortly with reset instructions if the email address you entered belongs to a MyOwnDB account. If you don't receive a mail, check your spam folder, and check you entered the right email address in this form.")]
                                        // at first display, we are in the failure case but the errors list is empty
                                        // so it just displays the form.
                                        | Failure errors ->
                                            div [] [p [] [t.tt("Fill and submit the form to reset your password.")]
                                                    displayErrors errors
                                                    div [classes [BS.``mb-3``]] [label [] [text (t.t("Email"))]; Doc.InputType.Text [submitOnEnter(); attr.id "emailInput"; classes [BS.``form-control``]; attr.``type`` "email"] email]
                                                    Doc.Button (t.t("Send")) [attr.id "submitForgotEmail";attr.``class`` (BS.btn + " " + BS.``btn-primary`` + " " + BS.``w-100``)] submit.Trigger]
                    ))
            ]
        )

    let registerForm (r:{|isSaas:bool;tosUrl:string;privacyUrl:string|}) =
        let email = Var.Create ""
        // in Saas, the tos is initially not checked. For self hosted, it is checked and not visible.
        let tos = Var.Create (not r.isSaas)
        // used in multiple places
        let confirmationMessage = t.t("The confirmation must be equal to the password")
        let emailValidationMessage = t.t("Please enter your email address. The current value is not recognised as a valid address.")
        Form.Return (fun email (p,c) tos -> email,p,c,tos )
        <*> (emailField email)
        <*> passwordAndConfirmationFields(t.t("Password is invalid"), confirmationMessage)
        <*> (Form.YieldVar tos
             |> Validation.Is id (t.t("Please accept our Terms of Service and Privacy Policy.")))
        |> Form.WithSubmit
        |> Form.MapToAsyncResult (fun (email,p,c,tos) ->
            async {
                if p<>c then
                    return  (Failure [ErrorMessage.Create(id="different",text=confirmationMessage)])
                else
                    let! result = Server.registerAccount email p tos
                    match result with
                    | Ok() -> return Success(p,c)
                    | Error s ->
                        let errors = s |> List.map (fun (code,msg) -> ErrorMessage.Create(id=code,text=msg))
                        return  (Failure errors)
            }
        )
        |> Form.TransmitView
        |> Form.Render (fun email pass conf tosAccepted submit resultView ->
            // attribute to submit form on enter press
            let submitOnEnter () = on.keyUp (fun el ev -> if ev.KeyCode = 13 then submit.Trigger())

            let emailInvalid =
                (email.View |> View.Map (fun s -> RegExp(@"^.+@.+\..+$").Test s |> not ))
            // view for reporting invalid confirmation of password or no acceptance of tos
            // ---------------------------------------------------------------------------
            // Cannot submit form if
            // - password is unconfirmed
            // - tos is not accepted
            // - email is not recognised
            let cannotSubmitForm =
              View.Map3
                (fun f s t -> f || s || t)
                (isPasswordUnconfirmed pass.View conf.View)
                (tosAccepted.View |> View.Map not)
                emailInvalid
            let confirmationClasses = passwordConfirmationClasses (isPasswordUnconfirmed pass.View conf.View)
            let tosClasses =
                checkboxValidClasses tosAccepted.View
                |> View.Map (fun validityClasses ->
                  String.concat  " " [BS.``form-check-input``; validityClasses]
                )
            let emailClasses =
                emailInvalid
                |> View.Map (fun invalid ->
                  if invalid then
                    String.concat " " [BS.``form-control``; BS.``is-invalid``]
                  else
                    String.concat " " [BS.``form-control``; BS.``is-valid``]
                )
            // display red button if confirmation is invalid
            let buttonClasses = passwordFormButtonClasses cannotSubmitForm // Display error message as button text if confirmation invalid
            let buttonText = passwordFormButtonText cannotSubmitForm
            // Also disable the button if the confirmation is invalid
            let button = View.Map (fun t ->
                                    Doc.Button (t) [  attr.id "submitSignup"; attr.disabledDynPred (View.Const "") (cannotSubmitForm)  ; attr.``class`` (buttonClasses.V) ] submit.Trigger
                                    )
                                   buttonText
                         |> Doc.EmbedView
            // defining the password criteria list
            let passwordCriteria = passwordCriteriaDiv ()
            // handle errors reported by server
            // if no error is reported, display the password criteria (done at first page load)
            let errorsOrCriteria = passwordFormErrorOrCriteria resultView passwordCriteria
            let tosFields =
              if r.isSaas then
                div [attr.``class`` BS.``mb-3``] [
                  Doc.InputType.CheckBox [attr.id "tosInput"; attr.``class`` tosClasses.V] tosAccepted
                  label [classes [BS.``form-check-label``]; attr.``for`` "tosInput"] [t.tt("I have read and accepted the Terms of Service and Privacy Policy: ")]
                  a [attr.href r.tosUrl] [ t.tt("TOS")]
                  text " - "
                  a [attr.href r.privacyUrl] [ t.tt("Privacy Policy")]
                ]
              else
                  Doc.InputType.CheckBox [attr.id "tosInput"; attr.``class`` BS.hide ] tosAccepted

            // submission form
            let form = div [] [
                        div [classes [BS.``mb-3``]] [
                            label [] [t.tt("Email")]
                            Doc.InputType.Text [submitOnEnter(); attr.id "emailInput"; attr.``class`` emailClasses.V;] email
                            div [attr.``class`` BS.``form-text``] [t.tt("Only used for application notifications, never shared with third parties.")]
                            ]
                        div [classes [BS.``mb-3``]] [
                            label [] [t.tt("New Password")]
                            Doc.InputType.Password [submitOnEnter(); attr.id "passwordInput"; classes [BS.``form-control``]; attr.``type`` "password"] pass]
                        div [attr.``class`` BS.``mb-3``] [
                            label [] [t.tt("Password Confirmation")]
                            Doc.InputType.Password [submitOnEnter(); attr.id "confirmationInput"; attr.``class`` confirmationClasses.V; attr.``type`` "password"] conf]
                        tosFields
                        button
            ]
            // The notice will replace the form after successful submission
            let notice = div [ attr.id "infoBox"; classes [BS.alert; BS.``alert-info``] ] [t.tt("Your account has been registered, but you need to confirm it by clicking the link we just sent you.")]

            // all the above is inserted in the page by this:
            div [] [
                errorsOrCriteria
                passwordFormOrNotice resultView form notice
            ]
        )
