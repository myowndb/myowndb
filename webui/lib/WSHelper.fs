namespace web
open Microsoft.AspNetCore.Identity
open System.Security.Claims
open System.Reflection
open System
// for IEndpointRouteBuilder
open Microsoft.AspNetCore.Routing
// for endpoints.Map
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Authorization
open WebSharper.AspNetCore
open FSharp.Reflection
open FSharp.Core

open System.Collections.ObjectModel


open Microsoft.Extensions.DependencyInjection

// For [Extension]
open System.Runtime.CompilerServices
open System.Runtime.InteropServices

open WebSharper.UI.Html
open WebSharper
open NGettext
open WebSharper.UI
open WebSharper.UI.Client

type I18n(catalog:Catalog) =
    inherit Myowndb.I18n.Helpers(catalog)
    // Helpers to get a Html.Text wrapped translated string
    member self.tt (s ,[<ParamArray>]values:obj array)=
        text (self.p(s,values) )
    member self.tn (singular, plural, selector,[<ParamArray>]values:obj array)=
        text (self.n(singular, plural, selector, values))
    member self.tc(context, s)=
        text (self.c(context,s))
    member self.tcp (context, s, [<ParamArray>]values:obj array)=
        text (self.cp(context,s,values) )
    member self.tcn (context, singular, plural, selector,[<ParamArray>]values:obj array)=
        text (self.cn(context,singular, plural,selector,values))
    member self.locale() =
        catalog.CultureInfo.TwoLetterISOLanguageName
module I18n=
    let setResultError (msg:string) (result:Result<_,_>) =
        result
        |> Result.mapError (fun _ -> msg)

[<JavaScript>]
module ClientHelpers =
    let debounce f t =
        let mutable timer:JavaScript.JS.Handle option = None
        fun _ ->
            Option.iter JavaScript.JS.ClearTimeout timer
            timer <- Some(JavaScript.JS.SetTimeout (fun() -> timer <- None; f()) t)
    let randomIntString factor =
        JavaScript.Math.Floor(JavaScript.Math.Random()*factor).ToString()
    // Make data types list easily available on the client
    let dataTypes:DataType.DataType list = JavaScript.JS.Inline("window.myowndb_dataTypes")

[<JavaScript>]
module WSExtensions =
    module Var =
        let setIfChanged x v = if x <> Var.Get v then Var.Set v x
        let setAndCallIfChanged x v f=
            if x <> Var.Get v then
                Var.Set v x; f
    module Doc =
        // This returns a tuple of
        // - the Doc for the input field
        // - a bool view indicating if the user has changed the value but the debounced value was not yet updated
        let debouncedInput attr delay (debouncedVar:Var<string>) =
            // If the delay is 0 we don't debounce
            if delay = 0 then
                Doc.InputType.Text attr debouncedVar
                ,
                View.Const false
                ,
                // In this case the reset function does nothing. See below for more explanations.
                (fun () -> () )
            else
                // This implementation had a problem with client side DateTime validation:
                // A date is accepted in the format `2020-01-01` but is updated ot include the time an becomes `2020-01-01 10:32:59`.
                // The problem comes from the fact that updates only flow from the internalVar to the debouncedVar: the debouncedVar is not
                // supposed to be updated externally. If the debouncedVar is modified externally as for date values, it will be considered
                // as awaiting validation and will prevent the form to be submitted.
                let (internalVar:Var<string>) = Var.Create (debouncedVar.Value)
                // We return a reset function that simply set our internal var to an empty string. This is needed due to the on-way flow
                // from internal Var to debouncedVar, and we need to give a way to reset the internal var when the debouncedVar is reset
                // externally, eg when the form is reset.
                let resetFunction() = internalVar.Set ""
                let awaitingValidation = View.Map2 (fun intern debounced -> intern <> debounced) internalVar.View debouncedVar.View
                let update = ClientHelpers.debounce (fun _-> Var.setIfChanged internalVar.Value debouncedVar) delay
                do internalVar.View |> View.Sink update

                Doc.InputType.Text attr internalVar
                ,
                awaitingValidation
                ,
                resetFunction
        let Prepend d1 d2 = Doc.Append d2 d1
        // To be used in a pipe. doc2Async |> PrependPipedAsync docFirst results in [docFirst doc2]
        let PrependPipedAsync d1 d2async = async {
          let! d2=d2async
          return Doc.Append d1 d2
        }

    module Remoting =
        let activeRpcs = Var.Create 0
        type RemotingWithActiveCounter() =
              inherit Remoting.AjaxRemotingProvider()

              override _.AsyncBase(handle, data) =
                  // Increase the number of active Rpcs
                  activeRpcs.Update (fun n -> n+1)
                  // Start the call
                  let resAsync = base.AsyncBase(handle, data)
                  async {
                      let! res = resAsync
                      activeRpcs.Update (fun n -> n-1)
                      return res
                  }

    module View =
        // transform a list of views in a view of a list
        // this form is not tail recursive
        let rec sequence (l:View<'a> list) =
            let retn = View.Const
            let (<*>) = View.Apply
            let cons a l = a::l
            match l with
            | [] -> retn []
            | h :: t ->
                (retn cons)
                <*>
                h
                <*>
                sequence t
[<JavaScript>]
module HtmlHelpers =
    open Css
    type InfoBoxKind = |Primary |Secondary |SuccessBox |Danger |Warning |Info |Light |Dark
    type InfoBoxTemplate = Templating.Template<"templates/InfoBox.html">
    let classes (l:string list) = attr.``class`` (String.concat " " l )
    type WebSharper.UI.Html.attr with
        static member classDynPredBoth (trueClass:string) (falseClass:string) (boolView:View<bool>)=
            boolView
            |> View.Map (fun b -> if b then trueClass else falseClass) |> attr.classDyn
    let wrapInCard (id:string) (title:string)(body:Doc) =
        div [classes ["card";"mb-4"]; attr.id id ] [
            div [classes ["card-header"]] [
                i [classes ["fas";"fa-table";"me-1"]] []
                text title
            ]
            div [classes ["card-body"]] [
                body
            ]
        ]
    let infobox (kind:InfoBoxKind) (content:Doc) =
        InfoBoxTemplate()
            .Kind((sprintf "%A" kind).ToLower().Replace("box",""))
            .Content(content)
            .Doc()
    let dangerBox (content:Doc) =
        infobox InfoBoxKind.Danger content
    let successBox (content:Doc) =
        infobox InfoBoxKind.SuccessBox content
    let primaryBox (content:Doc) =
        infobox InfoBoxKind.Primary content

    let fontAwesomeIconWithClasses (cssClasses:string list)(name:string) =
        // @ = List.append
        li [classes (["fas"; $"fa-{name}"] @ cssClasses)] []
    let fontAwesomeIcon  = fontAwesomeIconWithClasses []
    let flashDuration = 1000
    let setupFlashClassRemover (docId:string) =
         // Define a javascript to be called after a timeout that will remove the flash class to
         // avoid repeated flashing when new .flash elements are added
         let classRemover =
                         JavaScript.JS.Inline("""
                             function() {
                             var id = $0
                             var selector = "[data-flash-id='"+id+"']"
                             var el = document.querySelector(selector)
                             el.classList.remove('flash')
                             }
                         """, docId)
         // The the timer to remove the flash class
         JavaScript.JS.SetTimeout (fun() -> classRemover() ) flashDuration |> ignore
    // This function returns an attribute to assign to the element having the "flash" CSS class.
    // It handles the removal of the "flash" CSS class to avoid repetition of the flash when other elements
    // with the flash class are added.
    let flashAttribute () =
        // Generate an id to assign to the li element, so that we can remove the flash class
        // when the animation is done (otherwise subsequent .flash element renderings will also
        // highlight this one)
        let elementId = ClientHelpers.randomIntString 1000
        setupFlashClassRemover elementId
        attr.``data-`` "flash-id" elementId
    // Helper to set attributes of a flashed element when the caller wants to set CSS classes
    let flashAttributesWithClasses (cssClasses:string list) =
        [
          classes ( List.append cssClasses ["flash"])
          flashAttribute()
        ]
    // Helper to set flash attributes when no class is set by the caller
    // Beware: you cannot Attr.concat your own classes attribute to the return value of this function.
    // Use flashAttributesWithClasses instead if you need to set your own classes.
    let flashExclusiveAttributes () =
        flashAttributesWithClasses []

    // Call this function in the attrs list of an element to have it focused after it was rendered
    let focusAfterRender()=
        on.afterRender(fun el ->
            JavaScript.JS.Inline("""var e = $0; e.focus();""", el)
    )

    // Returns a button that will show `content` when clicked. The button of the text is updated
    // according to the visibility of the content.
    let buttonRevealingContent (cfg:{|buttonTextWhenVisible:Doc;buttonTextWhenHidden:Doc;|}) (content:Doc) =
      let contentDisplayed = Var.Create false
      let buttonText =
        contentDisplayed.View
        |> View.Map (fun b -> if b then cfg.buttonTextWhenVisible else cfg.buttonTextWhenHidden)
      [
        button
          [
            classes [BS.btn;BS.``btn-primary``]
            on.click (fun _el _ev -> contentDisplayed.Set (not contentDisplayed.Value); )
          ]
          [ buttonText.V]
        div
          [attr.classDynPredBoth "d-block" "d-none" contentDisplayed.View]
          [content]
      ]
      |> Doc.Concat
    // Displays a Doc with a tooltip on hover. Configured in app's common.css data-tooltip settings
    let withTooltip (tooltip:string) (content:Doc) =
      span [ attr.``data-`` "tooltip" tooltip ] [content]
    // Displays a question mark with a tooltip on hover.
    let helpIcon (tooltip:string) =
      withTooltip tooltip (fontAwesomeIcon "question-circle")

    // Returns if the element identified by selector s is present in the document
    // Runs as javascript in the document, but is a F# function usable in F# code.
    let selectorPresent(s:string) =
       JavaScript.JS.Inline("""
           var selector = $0
           var el = document.querySelector(selector)
           return el != undefined
       """, s)

open HtmlHelpers


module EndpointRouting =
    type EndpointProp ={ method: string * Http.RequestDelegate -> IEndpointConventionBuilder; path: string; policies: string list; isWildcard: bool }


    let private getEndpointProps (aspnetEndpoints:IEndpointRouteBuilder) (case:UnionCaseInfo) =
        let firstArg (att:CustomAttributeData) =
            att.ConstructorArguments |> Seq.head
        // we get a CustomAttributeTypedArgument that holds a readonly collection of CustomAttributeTypedArgument whose value is a boxed string
        let getMethod  (att) : (string*Microsoft.AspNetCore.Http.RequestDelegate)-> Microsoft.AspNetCore.Builder.IEndpointConventionBuilder  =
            match (firstArg att |> fun v -> v.Value |> unbox |> (fun  (v:ReadOnlyCollection<CustomAttributeTypedArgument>) -> unbox (v.Item(0).Value)) ) with
            | "GET" -> aspnetEndpoints.MapGet;
            | "POST" -> aspnetEndpoints.MapPost;
            | "PUT" -> aspnetEndpoints.MapPut;
            | "DELETE" -> aspnetEndpoints.MapDelete;
            | _ -> aspnetEndpoints.Map
        let getConstructorArguments (att:CustomAttributeData) =
            att.ConstructorArguments
            |> Seq.collect (fun a ->
                a.Value
                |> unbox
                |> Seq.map (fun (el:CustomAttributeTypedArgument)-> unbox (el.Value)))
        // Extracts endpoint arguments that are not put in the URL's path, ie they are posted form data or
        // placed in the query string.
        // These elements are specified in the endpoint's attribute
        let nonPathElements (case: UnionCaseInfo) =
            let attributes = case.GetCustomAttributesData()
            attributes
            |> Seq.fold (fun acc att ->
                            match att.AttributeType.Name with
                            |"FormDataAttribute"|"QueryAttribute" ->
                                let args = getConstructorArguments att
                                Seq.concat (seq [acc;args])
                            | _ ->
                                //printfn "uknown attribute %s" att.AttributeType.Name
                                acc
                        )
                        Seq.empty

        // returns elements that are place in the url's PATH by Websharper.
        let getPathElements (case:UnionCaseInfo) =
            let ignored = nonPathElements case
            let fields = case.GetFields()|> Array.map (fun e -> e.Name)
            fields
            |> Seq.fold (fun acc n ->
                    if Seq.contains n ignored then
                        acc
                    else
                        Array.concat (seq [acc;[|n|]])
                    )
                    [||]
        // returns a string specifying the matched elements in the aspnet endpoint's path
        // format: /{argname1}/{argname2}...
        // empty if none
        // this is meant to be concatenated to the websharper endpoint's path present in the attribute
        let dynamicPathElements (case:UnionCaseInfo)=
            let elements = getPathElements case
            let suffix= elements
                        |> Array.fold (fun acc e ->
                               acc+"/{"+e+"}"
                            )
                            ""
            suffix
        // See https://learn.microsoft.com/en-us/aspnet/core/fundamentals/routing?view=aspnetcore-7.0#route-templates
        // A wildcard is indicated by a star before the path component.
        let makeEndpointPropsWildcardIfNeeded (r:EndpointProp) =
            if r.isWildcard then
                let lastCurlyOpen = r.path.LastIndexOf("{")
                let newPath = r.path.Remove(lastCurlyOpen,1).Insert(lastCurlyOpen,"{*")
                { r with path = newPath}
            else
                r
        // iterates over all CustomAttributeData and accumulates info in the record r
        let rec getEndpointPropsInternal (atts:CustomAttributeData seq) (r:EndpointProp)=
            match atts|>List.ofSeq with
                | [] -> r
                | att::t ->
                    match att.AttributeType.Name with
                    // The EndPointAttribute case will handle the arguments passed to the EndPoint annotation
                    | "EndPointAttribute" ->
                        // this holds the path and possibly the method
                        let annotation:string = string (att|> firstArg).Value
                        let suffix = (dynamicPathElements case)
                        // helper function to get the path from an annotation's string
                        let buildPath ann = (sprintf "%s%s" ((ann|> string |> fun s -> s.Trim('"')).Trim('/')) suffix )
                        // Inspired from WebSharper.Core/src/sitelets/WebSharper.Sitelets/RouterAttributeReader.fs:176
                        // The EndPoint annotation is either  only a path, or a method followed by a path (separated by a space)
                        match annotation.IndexOf(" ") with
                            // No method was specified
                            | -1 ->
                                // Recurse without changing the method
                                getEndpointPropsInternal t {r with path = buildPath (firstArg att)}
                            // A method was specified, extract it and include it in the recursive call
                            | i ->
                                // The part before the space is the method
                                let annotatedMethod = annotation.Substring(0, i)
                                // After the space is the path
                                let annotatedPath = annotation.Substring(i + 1)

                                // Get the method's function to be passed in the recursive call
                                let method : (string*Microsoft.AspNetCore.Http.RequestDelegate)-> Microsoft.AspNetCore.Builder.IEndpointConventionBuilder =
                                    match annotatedMethod with
                                    | "GET" -> aspnetEndpoints.MapGet;
                                    | "POST" -> aspnetEndpoints.MapPost;
                                    | "PUT" -> aspnetEndpoints.MapPut;
                                    | "DELETE" -> aspnetEndpoints.MapDelete;
                                    | _ -> aspnetEndpoints.Map

                                // Do recursive call with data extracted from annotation
                                getEndpointPropsInternal t {r with path = buildPath annotatedPath ; method = method}
                    | "MethodAttribute" ->
                        // this holds the HTTP method for the endpoint
                        // in the record we set the corresponding endpoint.Map${Method}
                        getEndpointPropsInternal t {r with method = (getMethod att) }
                    | "FormDataAttribute" -> r
                    | "WildcardAttribute" ->
                        // We just mark the record as wildcard so it can be handled after all attributes have been seen.
                        getEndpointPropsInternal t {r with isWildcard = true}
                    | "AuthorizeAttribute" ->
                        // here we handle the policies
                        // a.MemberName = "Policy"
                        // a.MemberInfo = "Policy"
                        // a.TypedValue = "Admin"
                        let args = att.NamedArguments
                        match args|>Seq.length with
                        // Authorize without arguments is requiring authentication
                        | 0 -> getEndpointPropsInternal t { r with policies="Authenticate"::r.policies}
                        // policies listed are all required, there's no further flexibility here
                        | _ -> getEndpointPropsInternal t { r with policies=(List.concat [| r.policies;
                                                                 (args
                                                                 |> Seq.toList
                                                                 |> List.map (fun a -> a.TypedValue.ToString().Trim('"'))
                                                                 )
                                                              |])}
                    | _ ->
                        //printfn "Unknown Attribute: %s with args %A" att.AttributeType.Name att.ConstructorArguments
                        getEndpointPropsInternal t r
        let attributes = case.GetCustomAttributesData()
        let r = getEndpointPropsInternal attributes {method= aspnetEndpoints.MapGet; path=""; policies=[]; isWildcard=false }
        makeEndpointPropsWildcardIfNeeded r



    type EndpointKind = |Remoting |Page
    let MapWebSharperEndpointsInternal<'T>(aspnetEndpoints:IEndpointRouteBuilder) (kind:EndpointKind)=

        let ws = aspnetEndpoints.CreateApplicationBuilder()
                                    .UseWebSharper( fun builder -> builder.SiteletAssembly(Assembly.GetExecutingAssembly()) |> ignore)
                                    .Build()
        let endpointsType = typeof<'T>
        let duCases =
            Reflection.FSharpType.GetUnionCases endpointsType
        duCases |>  Array.iter (fun c ->
                                    let props = getEndpointProps aspnetEndpoints c
                                    let builder = match kind with
                                                  | Page -> props.method(props.path,ws)
                                                  | Remoting ->
                                                    aspnetEndpoints.MapPost(props.path,ws)
                                    // Only call RequireAuthorization if some policies are present, otherwise
                                    // even endpoint without authorize annotation will require an authentication...
                                    if List.length props.policies > 0 then
                                            builder.RequireAuthorization(props.policies|>List.toArray) |> ignore
        )

    let MapWebSharperEndpoints<'T>(aspnetEndpoints:IEndpointRouteBuilder) =
        MapWebSharperEndpointsInternal<'T> aspnetEndpoints Page
    let MapWebSharperRemotingEndpoints<'T>(aspnetEndpoints:IEndpointRouteBuilder) =
        MapWebSharperEndpointsInternal<'T> aspnetEndpoints Remoting

[<Extension>]
type ApplicationBuilderExtensions =
    [<Extension>]
    static member UseWebSharperEndpointRouting<'T>
        (
            this: IApplicationBuilder
        ) =
        // POST requests with x-websharper-rpc are handled here
        // this means that non-POST requests with that header are not sent to websharper
        this.MapWhen( (fun httpContext -> httpContext.Request.Headers.ContainsKey("x-websharper-rpc") && httpContext.Request.Method="POST") ,
                      (fun (app:IApplicationBuilder)-> app
                                                        .UseRouting()
                                                        .UseAuthentication()
                                                        .UseAuthorization()
                                                        .UseEndpoints(fun endpoints ->
                                                                            EndpointRouting.MapWebSharperRemotingEndpoints<'T>(endpoints))
                                                                            |> ignore
                      )
            )
        // requests without the x-websharper-rpc header are handled here.
            .MapWhen( (fun httpContext -> not (httpContext.Request.Headers.ContainsKey("x-websharper-rpc"))) ,
                      (fun (app:IApplicationBuilder)-> app
                                                        .UseRouting()
                                                        .UseAuthentication()
                                                        .UseAuthorization()
                                                        .UseEndpoints(fun endpoints ->
                                                                            EndpointRouting.MapWebSharperEndpoints<'T>(endpoints))
                                                                            |> ignore
                      )
            )
[<JavaScript>]
module ClientI18n =
    open WebSharper.JavaScript

    [<Require(typeof<AppResources.Gettext.Js>)>]
    type JStr() =
     [<Require(typeof<Resources.BaseResource>, "/vendor/gettext_spread.js")>]
     member _.t (s:string,[<ParamArray>] args:obj array) : string=
        JS.Inline ("""
        return spread_gettext(window.myowndb_i18n,$0,$1)
        """, s, args)

     member self.tt(s:string,[<ParamArray>] args:obj array) =
        text (self.t(s,args))

     [<Require(typeof<Resources.BaseResource>, "/vendor/gettext_spread.js")>]
     member _.n (s:string,plural:string,n:int64,[<ParamArray>]o:obj array)=
        JS.Inline ("""
        return spread_ngettext(window.myowndb_i18n,$0,$1,$2,$3)
        """, s, plural, n, o)

     member self.tn(s:string,plural:string,n:int64,[<ParamArray>] args:obj array) =
        text (self.n(s,plural, n,args))

     // Translate msgid in domain. Doesn't handle plurals or context.
     [<Require(typeof<Resources.BaseResource>, "/vendor/gettext_spread.js")>]
     member _.td (domain:string,s:string,[<ParamArray>]o:obj array)=
        JS.Inline ("""
        return spread_dgettext(window.myowndb_i18n,$0,$1,$2)
        """, domain, s, o)

    let t = JStr()

    // Make this module auto open, so that code opening ClientI18n also get access to
    // its functions.
    [<AutoOpen>]
    module NotificationSubscriptionTranslations =
      open NotificationSubscriptionCriteria
      let translateNotificationEvent(e:Event) =
        match e with
        | AfterCreate -> t.t("Creation")
      let translateNotificationProtocol(e:Protocol) =
        match e with
        | Smtp -> t.t("Email")


open ClientI18n

// Code handling MyOwnDB help display
[<JavaScript>]
module MyOwnDBHelp=
    open WebSharper.JavaScript

    // Function parsinga markdown string to HTML, client-side
    [<Require(typeof<AppResources.Marked.Js>)>]
    let parseMarkdown (s:string) =
      JS.Inline ("""
        return marked.parse($0);
      """, s)

    // Function sanitising the HTML it receives as argument.
    [<Require(typeof<AppResources.DOMPurify.Js>)>]
    let purifyHTML (s:string)=
      JS.Inline ("""
        return DOMPurify.sanitize($0)
      """, s)


    type HelpTranslation()=
      // Helper to translate the help message id to its string found in the myowndbhelp.po file, hence the
      // gettext domain "myowndbhelp" passed as first argument.
      member _.tHelp(msgid:string,[<ParamArray>] args:obj array)=
        t.td("myowndbhelp",msgid,args)
    // Helper to display the help frame for the msgid.
      member self.helpBlock(msgid:string,[<ParamArray>] args:obj array)=
        self.tHelp(msgid,args)
        // Help translations are in the markdown format, parse it
        |> parseMarkdown
        // Just to be sure, sanitise it
        |> purifyHTML
        // Create a Doc instance from the string
        |> Doc.Verbatim
        // Wrap it in a box that can be identified as being help
        |> primaryBox

    let th= HelpTranslation()

    // Types that when translated to javascript give driver config objects
    [<Require(typeof<AppResources.Driver.Js>)>]
    [<Require(typeof<AppResources.Driver.Css>)>]
    type DriverPopover = {title: string; description: string}
    type DriverStep = {element: string; popover: DriverPopover}
    type DriverTour =
      { steps: DriverStep array}
    // Helper function to set the tour steps on a page
    let setPageTour (tour: DriverTour ) =
      JS.Inline ("""
        var tour = $0;
        window.tour = tour
        // Set the window.tour.
        // We set it on window to make it global, so that it is accessible from the
        // menu icon, which is only generated at the first page render, not when
        // the body of the page is update by ClientContent.fs
        // We set the tour object globally so that the driver object is only constructed when
        // the use clicks on the help icon. Constructing the driver object here is problematic with
        // client side generated content as it is possibly not yet available here.
      """, tour)

    // Trigger showing the tour
    let showTour() =
      JS.Inline ("""
        // Assign to local variable. I think it prevents some quirks?
        var fullTour = window.tour
        // Filtering function, to keep only elements found
        // We do it this way to be able to show tours for client side generated content.
        // We filter out inexisting elements so we can be generic in the tour definition, and
        // still be sure only existing elements will be covered.
        function elementPresent(step) {
          return document.querySelector(step.element) != null
        }
        // Filter the steps of the full tour
        var filteredsteps = fullTour.steps.filter(elementPresent)
        // Clone the full tour
        var realTour = structuredClone(fullTour)
        // and set its steps so only existing elements are included
        realTour.steps = filteredsteps
        // and build the driverObj with it
        var driverObj = window.driver.js.driver(realTour);
        // Start the tour
        driverObj.drive();
      """)

    // Tour building functions
    module Tour =
      // Build a DriverStep instance from the 3 strings
      let newStep (selector:string)(title:string)(description:string) =
        { element= selector; popover = {title=title; description=description}}

      // Initialise a tour with no step
      let newTour () : DriverTour=
        { steps =
            [| |]
        }

      // Add a step to the tour passed as last parameter
      let addStep (selector:string)(title:string)(description:string) (tour:DriverTour)=
        // Only add the step if the element targetted is present. This simplifies the definitions
        // of tour as there's no attention to be spent on knowing if user is admin or not
        {tour with steps = Array.append tour.steps [|newStep selector title description|]}

      // Takes an array of the form  [| [| selector; title; desc |] ; ....|]
      // and returns a DriverTour with these steps
      let fromTuple (a: (string*string*string) array) =
        a
        |> Array.fold
          // Folder adds the step to the current tour
          (fun tour (selector,title,description) -> tour |> addStep selector title description)
          // Initialise with an empty tour
          (newTour())

      let setPageTourFromTuples (a: (string*string*string) array) =
        a
        |> fromTuple
        |> setPageTour


    // Returns the icon to be shown in the menu.
    // Is a function so that it can possibly be expanded with additional
    // behaviour setup.
    let tourMenuIcon() =
      fontAwesomeIcon "info-circle"

    // Disable the tour, displaying a default tour
    let resetTour() =
      JS.Inline ("""
        var step = $0
        var tour = { steps : [ step ] };
        var driverObj = window.driver.js.driver(tour);
        window.show_tour=driverObj.drive;
      """,
        Tour.newStep "#pageTourIcon" (th.tHelp("Guided tour"))  (th.tHelp("You clicked on the right icon to ask for a guided tour. Unfortunately there is no guided tour around this page.")))



[<JavaScript>]
module JsonHelpers =
    let deserialisedEmptyDataTable = [|new Map<string,string>([||]) |]
    let deserialiseDataTable (json:string) =
        Json.Decode<Map<string,string> array>(Json.Parse json )

[<JavaScript>]
module DeleteConfirmation =
    open Css
    [<Stub>]
    type BSTooltip() =
        [<Name "hide">]
        [<Stub>]
        member this.Hide() = WebSharper.JavaScript.Interop.X<_>
        [<Name "dispose">]
        member this.Dispose() = WebSharper.JavaScript.Interop.X<_>

    // Function displaying a custom element triggering the display of a confirmation
    // request before an action is taken.
    // The first argument is a function taking as argument one Attr (which is the onclick
    // attribute that is defined here and which the caller has to set on the element
    // supposed to trigger the deletion)
    // The second argument is the action to take if the user confirms.
    let customDeleteConfirmation (docFn:Attr -> Doc)(takeAction:unit->Async<bool>)(confirmationQuestion:string) =
        let deleteStep = Var.Create 0
        deleteStep.View
            |> View.Map(fun step ->
                let dustbin =
                  docFn
                    (
                      on.click
                          (fun ev el ->
                              deleteStep.Set 1)

                    )
                match step with
                | 0 ->
                    dustbin
                | 1 ->
                    let tooltipId = string (System.Random().Next())
                    let tooltipVar:Ref<BSTooltip> = ref Unchecked.defaultof<_>
                    span [ Attr.Create "data-bs-toggle" "tooltip"
                           Attr.Create "data-bs-placement" "top"
                           Attr.Create "title" confirmationQuestion
                           attr.id tooltipId
                           on.afterRender (fun el ->
                            tooltipVar.Value <-(
                                JavaScript.JS.Inline("""
                                    var el = document.getElementById($0)
                                    var tt = new bootstrap.Tooltip(el)
                                    return tt
                                """, tooltipId)
                                )
                           )
                         ]
                         [
                          span [classes [BS.``text-danger``; BS.``px-2``]
                                on.click(fun el ev ->
                                    async {
                                        let! result = takeAction()
                                        if result then
                                            deleteStep.Set -1
                                            tooltipVar.Value.Dispose()
                                        else
                                            deleteStep.Set 3
                                            tooltipVar.Value.Dispose()
                                    }|> Async.Start
                                )
                               ]
                               [t.tt("Yes")]
                          span
                            [on.click
                                (fun el ev->
                                    deleteStep.Set 0
                                    tooltipVar.Value.Dispose()
                                )
                            ]
                            [t.tt("No")]
                         ]
                // Error
                | 3 -> span [] [t.tt("An error occured");dustbin]
                | _ -> span [] []
            )

    let deleteConfirmation (target:string)(takeAction:unit->Async<bool>) =
      customDeleteConfirmation
        (
          fun onclick ->
            span [
                classes [ $"myowndb-delete-{target}" ]
                attr.``data-`` "myowndb-action" "delete";
                onclick
             ]
             [
                // Use of class me-5 for display in tables. It makes the yes be under the cursor
                // for easy confirmation in datatable action cells
                li [ classes ["fas"; "fa-trash-alt"; BS.``me-5``] ] []
             ]
        )
        takeAction
        (t.t("Confirm deletion?"))
