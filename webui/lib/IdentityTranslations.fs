namespace web
open System
open Microsoft.AspNetCore.Identity

type LocalizedIdentityErrorDescriber(t:I18n) =
    inherit IdentityErrorDescriber()
    override  self.DefaultError() =
        IdentityError(Code="DefaultError", Description=t.t("An undetermined error has occured."))
    override  self.DuplicateUserName(email: string) =
        IdentityError(Code="DuplicateUserName", Description=t.t("The email {0} is already registered.", email))
    override  self.InvalidEmail(email: string) =
        IdentityError(Code="InvalidEmail", Description=t.t("{0} is not a valid email.", email))
    override  self.InvalidToken() =
        IdentityError(Code="InvalidToken", Description=t.t("Invalid token"))
    override  self.PasswordMismatch() =
        IdentityError(Code="PasswordMismatch", Description=t.t("Password mismatch"))
// The following strings are normally not used in the app
// They are commented with //-- so the xgettext lua parser used sees them as comment and doesn't
//-- We use our custom password validators for utf-8 support
//--    override  self.PasswordRequiresDigit() =
//--        IdentityError(Code="PasswordRequiresDigit", Description=t.t("Password requires a digit"))
//--    override  self.PasswordRequiresLower() =
//--        IdentityError(Code="PasswordRequiresLower", Description=t.t("Password requires a lowercase letter"))
//--    override  self.PasswordRequiresNonAlphanumeric() =
//--        IdentityError(Code="PasswordNonAlphanumeric", Description=t.t("Password requires a non-alphanumeric character"))
//--    override  self.PasswordRequiresUpper() =
//--        IdentityError(Code="PasswordRequiresUpper", Description=t.t("Password requires a uppercase letter"))
//--    override  self.PasswordTooShort(length:int) =
//--        IdentityError(Code="PasswordTooShort", Description=t.t("Password must be at least {0} characters long.",length))
// include their translations in the .pot file.
// -- Following strings are for features we don't use (hopefully)
//--    override  self.UserAlreadyHasPassword() =
//--        IdentityError(Code="UserAlreadyHasPassword", Description=t.t("User already has password"))
//--    override  self.UserAlreadyInRole(role:string) =
//--        IdentityError(Code="UserAlreadyInRole", Description=t.t("User already in role {0}",role))
//--    override  self.UserNotInRole(role:string) =
//--        IdentityError(Code="UserNotInRole", Description=t.t("User not in role {0}",role))
//--    override  self.UserLockoutNotEnabled() =
//--        IdentityError(Code="UserLockoutNotEnabled", Description=t.t("User lockout not enabled"))
//--    override  self.RecoveryCodeRedemptionFailed() =
//--        IdentityError(Code="RecoveryCodeRedemptionFailed", Description=t.t("Recovery code redemption failed"))
//--    override  self.ConcurrencyFailure() =
//--        IdentityError(Code="ConcurrencyFailure", Description=t.t("Optimistic concurrency failure, object has been modified."))
//--    override  self.DuplicateEmail(email: string) =
//--        IdentityError(Code="DuplicateEmail", Description=t.t("The email {0} is already registered.", email))
//--    override  self.DuplicateRoleName(role: string) =
//--        IdentityError(Code="DuplicateRoleName", Description=t.t("Role {0} already exists.", role))
//--    override  self.InvalidRoleName(role: string) =
//--        IdentityError(Code="InvalidRoleName", Description=t.t("Role name {0} is invalid.", role))
//--    override  self.InvalidUserName(name:string) =
//--        IdentityError(Code="InvalidUserName", Description=t.t("{0} is an invalid username",name))
//--    override  self.LoginAlreadyAssociated() =
//--        IdentityError(Code="LoginAlreadyAssociated", Description=t.t("A user with this login already exists."))
//--    override  self.PasswordRequiresUniqueChars(n:int) =
//--        IdentityError(Code="PasswordRequiresUniqueChars", Description=t.t("Password requires {0} distinct characters",n))