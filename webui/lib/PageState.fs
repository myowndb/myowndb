namespace web
// This is the way we save the state of the page in the URL to have the back button working.
// The state of the page is kept in a Var that is created at first render in ClientContent.
// As the state kept differs for each endpoint, we define a module below for each endpoint, in which
// we define the State type and its Empty value used for initialisation.
// From this state Var, we use lenses to derive other Vars to be used in widgets in the page. The widgets
// update thir respective Vars, and this is reflected in the main state Var.
// The stateVar is serialised and put in an optional GET parameter.
// The same serialiasation is done for all pages, wiht the helpers fromStringOption and toStringOption below.
// When the state Var is updated, a new entry is added in the browser's history with pushStateIfNeeded.
//
// The handling of the first back event requires special care. When the client side router is installed,
// WebSharper creates a Var (https://github.com/dotnet-websharper/ui/blob/master/WebSharper.UI/Router.fs#L108)
// holding the EndPoint value of the currently displayed page.
// Thie Var is used to determine if the page needs to be redisplayed. This introduced a subtle bug:
// Imagine this browsing history states:
//   A -> B -> C -> D
// Throughout all this browsing, the client router's var is left untouched because the current EndPoint didn't change.
// Even though the URL displayed in the address bar is updated thanks to the browser's History API, the client
// side router has no view of this.
// Then the user presses back. Now, the endpoint of the displayed page is updated to C, because this is a normal browsing
// to a URL with the state C in the GET parameter.
// If the user now takes an action in the page, like changing the display order in a table, the URL in the address bar
// is updated accordingly (with state E), but the endpoint of the client side router is not updated. And this causes a problem if the
// user now presses back. The client side router will see that the endpoint of the page to display (C) is exactly the same as
// the endpoint it considers as displayed (C) because it is not aware that we went to state D.
// One solution could be to make the client side router of all the state changes, but the problem is that this will trigger
// a full page re-display. This is something we want to avoid.
// The best solution found was to trigger a client side router refresh with `ClientRouting.installedRouter.Update (fun v -> v)`
// Pros and cons:
// + works!
// - the installed router (which is a Var<EndPoint>), is left out of sync. But this was actually already the case, we just didn't reflect the state in the URL
// - we refresh the router at every step, while it's only needed at the first back after a forward change as explained above
//
open WebSharper
open WebSharper.JavaScript
open WebSharper.UI
[<JavaScript;RequireQualifiedAccess>]
module PageState =
  // It is not possible to add a constraint to ensure a record has a field.
  // See https://stackoverflow.com/a/39276278
  // This is the raison d'être of this interface
  type IState<'T> =
    // pushOnHistory has to be set to true when the app changes the state. However,
    // it is set to false before it is pushed on the history. This way, we avoid
    // pushing on history a URL that we visit following a press of the back button.
    abstract member pushOnHistory:bool
    // Return IState<_> so we can chain them
    abstract member withPushOnHistory:bool -> IState<'T>
    // get access to the state itself so it can be serialised
    abstract member toState:unit -> 'T

  // Encodes the state of the page in a string option that can be put in the EndPoint
  [<Inline>]
  let toStringOption<'a>(s:'a) =
    s|>WebSharper.Json.Serialize|> JS.EncodeURIComponent |> Some

  // Function to build the stateVar from the string option retrieved from the EndPoint
  [<Inline>]
  let fromStringOption<'a>(so:string option):Var<'a option>=
    match so with
    | None -> Var.Create None
    | Some s ->
      let parsedState:'a =
        s
        |> JS.DecodeURIComponent
        |> WebSharper.Json.Deserialize
      parsedState
      |> Some
      |> Var.Create
  // Adds an entry in the history if needed, i.e. if state.pushOnHistory=true
  [<Inline>]
  let pushStateIfNeeded<'T>(stateOption:IState<'T> option, buildEndPointWithState: string option-> EndPoint) =
      let h = JS.Window.History
      match stateOption with
      | None ->
        ()
      | Some st ->
          if st.pushOnHistory then
            // The state pushed on history is always false. See IState.pushOnHistory for reason.
            let pushedState:'T = st.withPushOnHistory(false).toState()
            // The link to be pushed on the history
            let endpoint = pushedState|>toStringOption|>buildEndPointWithState
            //ClientRouting.installedRouter.Set endpoint
            let link = Routing.router.Link(endpoint)
            // Add the entry in the history, but only if we push another state. Otherwise, we would
            // get 2 entries for the same page, eg when refreshing a table or refiltering a page with the same criteria.
            if h.State <> pushedState then
              h.PushState(pushedState,"",link)

  // All Vars used to store the individual state information are derived from the stateVar using Lenses.
  // This means that everytime a part of the state if modified, stateVar will be too. So using a Sink on
  // its views enables us to record every change of the state in the brower history.
  [<Inline>]
  let setupPushStateSink (stateVar:Var<_>)(stringOptionToEndPoint) =
      stateVar.View|> View.Sink (fun s ->
        pushStateIfNeeded(s |> Option.map (fun v -> v :>IState<_>), stringOptionToEndPoint)
      )

  // A popstate event is fired when a back event occurs.
  // When this occurs, we need to update the state so it corresponds to the state in the popped URL.
  [<Inline>]
  let setupPopStateListener (stateVar:Var<_>) =
      // This function is meant to fix the first back after a forward change (see above comments).
      // However, it currently is triggered at every `popstate` event as I didn't identify a criteria
      // to limit its action.
      let fixFirstBack(poppedState) =
        ClientRouting.installedRouter.Update (fun v -> v)

      // register listener only on first display of page
      match stateVar.Value with
      | None ->
        JS.Window.AddEventListener ("popstate", System.Action<Dom.Event>(fun e ->
          let state = JS.Inline("""$0.state""",e)

          JavaScript.JS.SetTimeout (fun () ->
            fixFirstBack(state)
          // Zero timer just to put it at end of queue and execute it immediately
          ) 0 |> ignore
          ()
        )
        )
      | _ ->
        ()
  // Helper function to setup the Sink to run when stateVar is updated, and the listener to run
  // when a pop state event occurs.
  [<Inline>]
  let setupStateHandlers  (stateVar:Var<_>) (stringOptionToEndPoint) =
      // This function will continuously update the URL so it contains the current page state
      setupPushStateSink stateVar stringOptionToEndPoint
      // Popstate occurs when a back event occurs, so we need to update state
      setupPopStateListener stateVar

  // Helper function to initialise the State Var and sinks and listeners.
  [<Inline>]
  let setupPageState (state:string option) (stringOptionToEndPoint)=
      let stateVar = state |> fromStringOption
      setupStateHandlers stateVar stringOptionToEndPoint
      stateVar

  // This module defines the state of the pages behind the EndPoint Instances(id,stateString option)
  // If a new information becomes part of the state, add it as a field to the State typei here, define a lens
  // on that field to get a Var, and use it as the Var holding the new state information.
  [<RequireQualifiedAccess>]
  module Datatable =
    open DatatableTypes
    type State =
      {
        sortCriteria : SortCriteria option
        searchCriteria: SearchCriteria option
        paginationState: PaginationState option
        pushOnHistory: bool
      }
      interface IState<State> with
        member self.withPushOnHistory(b) = {self with pushOnHistory = b}
        member self.pushOnHistory = self.pushOnHistory
        member self.toState() = self
    let Empty = { sortCriteria = None; searchCriteria = None; paginationState = None; pushOnHistory = true}
  [<RequireQualifiedAccess>]
  module InstanceDisplay =
    open DatatableTypes
    type State =
      {
        map: Map<string,Datatable.State>
        pushOnHistory: bool
      }
      interface IState<State> with
        member self.withPushOnHistory(b) = {self with pushOnHistory = b}
        member self.pushOnHistory = self.pushOnHistory
        member self.toState() = self
    let Empty = { map = Map.empty<string,Datatable.State>; pushOnHistory = true}
