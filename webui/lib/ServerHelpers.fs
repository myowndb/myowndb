namespace web

module ServerHelpers =

    module DataTable =
        open FsToolkit.ErrorHandling

        let tryGetSearchCriteria
            (client: DB.DBClient)
            (entityId: Ids.EntityId)
            (searchCriteria: DatatableTypes.SearchCriteria)
            =
            if searchCriteria.value = "" then
                async.Return None
            else
                async {
                    // select the detail for this entity. If we find it, the request is valid.
                    let! detailResult =
                        DBAction.querySingleAction
                            "select * from details d
                                                                    join entities2details ed on (d.id=ed.detail_id)
                                                                where ed.entity_id = %d and d.name = %s"
                            (Ids.EntityId.Get entityId)
                            (searchCriteria.column)
                        |> DBAction.run<Detail.Detail> client

                    match detailResult with
                    | Ok [ d ] ->
                        // I had to specify the type here because the record type wasn't correctly inferred (I think,
                        // it complained that the first field name set in the record expression was not defined)
                        let r: Instance.WhereFilter option =
                            Some(
                                Instance.WhereFilter.DetailFilter
                                    { column = d
                                      value = searchCriteria.value }
                            )

                        return r
                    | Ok l ->
                        MyOwnDBLib.Logging.Log.Error(
                            "Got invalid list of Details for search criteria. List should be of length 1, here we got {l}",
                            l
                        )

                        return None
                    | Error e ->
                        MyOwnDBLib.Logging.Log.Error("Error getting Detail for search criteria: {e}", e)
                        return None
                }

        let getSortDirection (sortCriteria: DatatableTypes.SortCriteria) =
            match sortCriteria.direction with
            | DatatableTypes.SortDirection.Asc -> Instance.SortDirection.Asc
            | DatatableTypes.SortDirection.Desc -> Instance.SortDirection.Desc

        let getOrderClause (sortCriteria: DatatableTypes.SortCriteria) : Instance.OrderClause option =
            let sortDirection = getSortDirection sortCriteria

            sortCriteria.column
            |> Option.map (fun c ->
                { column = c
                  direction = sortDirection })

        // FIXME: getFilterClause should not issue a query itself, but return a DBAction
        let getFilterClause
            (client: DB.DBClient)
            (entityIdResult: Result<Ids.EntityId, string>)
            (searchCriteria: DatatableTypes.SearchCriteria)
            (sortCriteria: DatatableTypes.SortCriteria)
            (paginationState: DatatableTypes.PaginationState)
            : Async<Instance.ListClauses> =
            async {
                // Get the where filter
                let! whereFilterOption =
                    entityIdResult
                    |> Result.map (fun entityId -> tryGetSearchCriteria client entityId searchCriteria)
                    // If entityIdResult is an error, return a WhereFilter that ensures we don't return any row
                    |> Result.defaultValue (async.Return(Some(Instance.WhereFilter.False)))
                // build the order clause
                let orderClause = getOrderClause sortCriteria

                // return the complete ListClauses
                return
                    { select = "select *"
                      where = whereFilterOption
                      page = paginationState.currentPage
                      perPage = paginationState.perPage
                      order = orderClause }
            }

        let getDataTableAction
            (tableType: DatatableTypes.TableType)
            (entityId: Ids.EntityId)
            (filterClause: Instance.ListClauses)
            =
            (match tableType with
             | DatatableTypes.Main -> Instance.selectDataTableAction entityId filterClause
             | DatatableTypes.Linked info ->
                 Instance.linkedInstancesAction (info.sourceInstanceId) (info.direction) filterClause
             | DatatableTypes.Linkable info ->
                 Instance.linkableInstancesAction (info.sourceInstanceId) (info.direction) filterClause)

        type DataTableFormat =
            | JSON
            | CSV

        let mapDataTabletoFormat (format: DataTableFormat) =
            match format with
            | JSON -> DBAction.mapDataTableToJSON
            | CSV -> DBAction.mapDataTableToCSV

        // FIXME: do not issue query but return a DBAction
        let queryData
            (client: DB.DBClient)
            (tableType: DatatableTypes.TableType)
            (format: DataTableFormat)
            (entityId: Ids.EntityId)
            (filterClause: Instance.ListClauses)
            =
            getDataTableAction tableType entityId filterClause
            |> mapDataTabletoFormat format
            |> DBAction.run client
            |> Async.map DBResult.toResult
            // When we get the entityIdResult, it is a Result<EntityId,string>. As we call
            // this function in a bind, we need to return the same type, so we convert the errors in a string
            |> Async.map (Result.mapError (fun el -> String.concat "\n" el))

        let unnestListAndLogError (v: Async<Result<_, _>>) =
            v
            // Going from DBResult to Result leaves a Result.Ok [ [r1;r2;...] ]. Get rid of the nesting...
            |> AsyncResult.map (fun l -> List.head l)
            // log the error and return a localised string to show to the user
            |> AsyncResult.mapError (fun e ->
                MyOwnDBLib.Logging.Log.Error("Error retrieving list for entity {id}: error: {el}", id, e)
                e)

        let getData
            (client: DB.DBClient)
            (tableType: DatatableTypes.TableType)
            (format: DataTableFormat)
            (entityIdResult: Result<Ids.EntityId, string>)
            (filterClause: Instance.ListClauses)
            =
            entityIdResult
            |> async.Return
            // issue data query
            |> AsyncResult.bind (fun entityId -> queryData client tableType format entityId filterClause)
            |> unnestListAndLogError

        let getCountAction
            (tableType: DatatableTypes.TableType)
            (entityId: Ids.EntityId)
            (filterClause: Instance.ListClauses)
            =
            (match tableType with
             | DatatableTypes.Main -> Instance.selectCountAction entityId (Instance.countFor filterClause)
             | DatatableTypes.Linked info ->
                 Instance.countLinkedInstancesAction (info.sourceInstanceId) (info.direction) filterClause
             | DatatableTypes.Linkable info ->
                 Instance.countLinkableInstancesAction (info.sourceInstanceId) (info.direction) filterClause)

        let getCountResult
            (client: DB.DBClient)
            (tableType: DatatableTypes.TableType)
            (entityId: Ids.EntityId)
            (filterClause: Instance.ListClauses)
            =
            getCountAction tableType entityId filterClause
            |> DBAction.run client
            |> Async.map DBResult.toResult
            // When we get the entityIdResult, it is a Result<EntityId,string>. As this code is in
            // a bind, it needs to return the same type, so convert the string list to a string
            |> Async.map (Result.mapError (fun el -> String.concat "\n" el))

        let getCount
            (client: DB.DBClient)
            (tableType: DatatableTypes.TableType)
            (entityIdResult: Result<Ids.EntityId, string>)
            (filterClause: Instance.ListClauses)
            =
            entityIdResult
            |> async.Return
            // issue data query
            |> AsyncResult.bind (fun entityId -> getCountResult client tableType entityId filterClause)
            |> unnestListAndLogError

    module Auth =
        open System.Security.Claims

        let getCurrentUserId (httpContext: Microsoft.AspNetCore.Http.HttpContext) =
            let user: ClaimsPrincipal = httpContext.User
            let idClaim = user.FindFirst(ClaimTypes.NameIdentifier)
            int idClaim.Value

        // For public forms, there is no account id available, so we return 0 by convention
        let getCurrentUserAccountId (httpContext: Microsoft.AspNetCore.Http.HttpContext) =
            let user: ClaimsPrincipal = httpContext.User
            let idClaim = user.FindFirst("AccountId")
            if isNull idClaim then 0 else int idClaim.Value

    module Uploads =
        // To access IConfiguration
        open Microsoft.Extensions.Configuration
        open Auth
        // This function handles the upload of files before the RPC call submitting the form. It does this by
        // - assigning a random id to the upload
        // - save the uploaded file to a directory identified by the random id
        // - return the id identifying the upload to the caller
        let copyXhrFirstFileToWaitRoom (ctx: WebSharper.Sitelets.Context<_>) =
            let httpContext: Microsoft.AspNetCore.Http.HttpContext =
                unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])

            let serviceProvider = httpContext.RequestServices

            let config: IConfiguration =
                unbox (serviceProvider.GetService(typeof<IConfiguration>))


            let accountId = getCurrentUserAccountId httpContext
            let baseDir = config.GetValue("myowndb:fileAttachments:waitRoom")
            let uploadId = string (System.Random.Shared.NextInt64())
            let files = httpContext.Request.Form.Files
            let file = files[0]
            let fileName = file.FileName
            let waitDir = System.IO.Path.Combine(baseDir, string accountId, uploadId)
            let _dirInfo = System.IO.Directory.CreateDirectory(waitDir)
            let uploadPath = System.IO.Path.Combine(waitDir, fileName)
            use fileStream = new System.IO.FileStream(uploadPath, System.IO.FileMode.Create)
            file.CopyTo(fileStream)
            uploadId

    module Remoting =
        let getService<'T> () =
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let serviceProvider= httpContext.RequestServices
            let service:'T = unbox (serviceProvider.GetService(typeof<'T>))
            service

        let getServices (services: System.Type list) =
            let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
            let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
            let serviceProvider= httpContext.RequestServices
            services
            |> List.map (fun t -> unbox (serviceProvider.GetService(t)))

    open WebSharper.UI
    open Microsoft.Extensions.Configuration
    open System.IO
    module Utils =
      let humaniseDataTypeName (n:string) =
        n.Replace("madb_","").Replace("s3_","").Replace("_"," ")
      // If found, read all html banner files, place them in an danger box, and concatenate these.
      // Otherwise, return an empty doc
      let BuildBanners (config:Microsoft.Extensions.Configuration.IConfiguration) (subdir:string option)=
        // This key can be set by env var MYOWNDB_myowndb__bannersDirectory
        let bannersDirectoryKey = "myowndb:bannersDirectory"
        let baseBannersDirectory = config.Item(bannersDirectoryKey)
        let bannersDirectory =
          match subdir with
          | None -> baseBannersDirectory
          | Some path ->
            System.IO.Path.Combine(baseBannersDirectory,path)
        if config.GetSection(bannersDirectoryKey).Exists() && baseBannersDirectory<>"-" then
          let bannerFiles = Directory.GetFiles(bannersDirectory,"*.html")
          bannerFiles
          |> Seq.map (fun path -> Templating.DynamicTemplate(File.ReadAllText(path)).Doc())
          |> Seq.map (fun d -> HtmlHelpers.dangerBox d)
          |> Doc.Concat
        else
          Doc.Empty

    module MemoizedData =
      // Cached list, initialised with None
      let mutable cachedDataTypes:DataType.DataType list option= None
      // Get data types found in database, using cached value if available
      let getDataTypesAsync (client:DB.DBClient) = async {
        // Return cached data if available
        if cachedDataTypes.IsSome then
          return Option.get cachedDataTypes
        // else retrieve from database
        else
          let! dataTypesResult =
            DataTypeExtensions.GetAllAction()
            |> DBAction.map (fun (dt:DataType.DataType) ->
              // We clean up the names so that they can be displayed in english. The alternative would be to translate
              // the strings extracted from the database, also for english. But that requires that all translated strings
              // also be translated,and in most cases the translation would be equal to the id. This brings also the potential
              // to see english translations updated without their id updated and so missing the signal to update other locales.
              // This seems to be the best option for now.
              let englishName =
                dt.name
                |> Utils.humaniseDataTypeName
              {dt with name=englishName})
            |> DBAction.run client
          match dataTypesResult with
          |Error es ->
            // The app can't start if we cannot load the datatypes, so rais exception here
            raise (System.Exception($"Problem retrieving data types: {es|>Error.toString}"))
            return Unchecked.defaultof<_>
          |Ok l ->
            // Set cached value and return list
            cachedDataTypes <- Some l
            return l
      }
