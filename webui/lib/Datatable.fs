namespace web

open WebSharper
open WebSharper.UI
open WebSharper.UI.Html
// needed for UI.Html.attr.disableDynPred
open WebSharper.UI.Client
open HtmlHelpers
open Css
open WebSharper.JavaScript
open System
open ClientI18n

[<JavaScript>]
module Datatable =
    open DatatableTypes
    open FormHelpers

    // We set the number of pagination links to an even number. We display half that number of
    // navigation links before the current page "link", and half after.
    // We need to handle the situation near the first and last pages.
    // We look how many links are missing in front (resp. at the end), and
    // add those ot the end (resp; the front) if possible
    // Page numbers are 1 based.
    // We illustrate with N=6 navigations links, ie 3 before and 3 after
    // links marked with n are links that are normal cases, those marked with -
    // are links to non existing pages so not displayed, and links marked + are
    // those added to compensate for missing links at the other side.
    //  .........._____________________
    //  |-2|  |  | 1| 2| 3| 4| 5| 6| 7|   (a)
    //    -  -  -  p  n  n  n  +  +  +
    //     ......._____________________
    //     |  |  | 1| 2| 3| 4| 5| 6| 7|   (b)
    //       -  -  n  p  n  n  n  +  +
    //        ...._____________________
    //        |  | 1| 2| 3| 4| 5| 6| 7|   (c)
    //          -  n  n  p  n  n  n  +
    // When the current page p, we computer the absolute first, ie the page number of the
    // first link without paying attention to the limits to respect
    // We see that the missing links before are -absoluteFirst+1.
    // The normal last link is p + N /2 to which we add the links missing in front.
    // As we need to not go further than the number of pages available, we take the minimum with
    // of both.
    // Similar logic applies to links added in front.
    let paginatedPagesBoundaries numberOfLinks currentPageValue numberOfPagesValue=
        let absoluteFirst = currentPageValue - numberOfLinks/2
        let absoluteLast = currentPageValue + numberOfLinks/2
        let missingBefore = max ( -absoluteFirst + 1 ) 0
        let missingAfter =  max (absoluteLast - numberOfPagesValue ) 0
        let firstLinkNumber = (max (currentPageValue - numberOfLinks/2 - missingAfter) 1)
        let lastLinkNumber = min (currentPageValue + numberOfLinks/2 + missingBefore) numberOfPagesValue
        (firstLinkNumber,lastLinkNumber)
    let computeNumberOfPages totalRows rowsPerPage =
        int ( ceil ((float totalRows) / (float rowsPerPage)))

    // ********************
    // Pagination links
    // returns the list of pagination links
    // includes links to
    // - first
    // - previous
    // - x direct page links
    // - next
    // - last
    // the argument currentPage is actually Lenses from argument paginationState.
    // I pass both as argument rather than re-Lens into paginationState.
    let getNavigationLinks (paginationState:Var<PaginationState>) numberOfPages =
        let currentPage = paginationState.LensAuto (fun v -> v.currentPage)
        // initialise data and Views
        let (<*>) f s = View.Apply f s
        let onNumberClick(n) = (fun (_el:Dom.Element) (e:Dom.MouseEvent) -> e.PreventDefault();currentPage.Set (n))

        let computePaginationLinks currentPageValue numberOfPagesValue =
            let numberOfLinks = 6
            let firstLinkNumber,lastLinkNumber = paginatedPagesBoundaries numberOfLinks currentPageValue numberOfPagesValue
            seq { for i in firstLinkNumber .. lastLinkNumber do
                    yield i
            }
            |> Seq.map (fun i ->
                let isCurrentPage = currentPageValue = i
                let activeClass = if isCurrentPage then "active" else ""
                li [classes [BS.``page-item``; activeClass; "myowndbpaginationnumber"]
                    UI.Html.attr.disabledDynPred (View.Const "disabled") (View.Const isCurrentPage )
                   ]
                   [a [attr.``class`` "page-link" ; attr.href "#"; UI.Html.on.click (onNumberClick i)]
                      // FIXME: double digits page numbers links are winder than single digit page number links
                      [ text $"{i}" ]
                   ]
            )
            |> Doc.Concat
        let paginationLinks = View.Const computePaginationLinks <*> currentPage.View <*> numberOfPages

        // Start of content building
        let first = paginationState.View |> View.MapCached (fun r ->
                let onClick = (fun (_el:Dom.Element) (e:Dom.MouseEvent) -> e.PreventDefault(); if currentPage.Get()<>1 then currentPage.Set 1)
                li [ attr.``class`` "page-item"
                     UI.Html.attr.classDyn (V(if numberOfPages.V = 0 || currentPage.V=1 then "page-item disabled" else "page-item"))
                    // only act if we are not on the first page already.
                    // asiigning the disabled CSS class does not disable the link action
                   ]
                   [ a [classes [BS.``page-link``; "myowndbpaginationfirst"];  attr.href "#"; UI.Html.on.click onClick]
                       [text "|«"]
                   ]
            )
        // previous page button
        let previous = paginationState.View |> View.MapCached (fun r ->
                let onClick = (fun (_el:Dom.Element) (e:Dom.MouseEvent) -> e.PreventDefault(); if currentPage.Get()<>1 then currentPage.Set (currentPage.Value-1))
                li [ attr.``class`` "page-item"
                     UI.Html.attr.classDyn (V(if numberOfPages.V = 0 || currentPage.V=1 then "page-item disabled" else "page-item"))
                    // only act if we are not on the first page already.
                    // asiigning the disabled CSS class does not disable the link action
                                        ]
                   [ a [classes [BS.``page-link``;"myowndbpaginationprevious"] ; attr.href "#"; UI.Html.on.click onClick ]
                       [text "‹"]
                   ]
            )
        // next page button
        let next = paginationState.View |> View.MapCached (fun r ->
                let onClick = (fun (_el:Dom.Element) (e:Dom.MouseEvent) -> e.PreventDefault();numberOfPages |> View.Get (fun n -> if n>0 && currentPage.Get() <> n then currentPage.Set (currentPage.Value+1)))
                li [attr.``class`` "page-item"
                    UI.Html.attr.classDyn (V(if numberOfPages.V = 0 || currentPage.V=numberOfPages.V then "page-item disabled" else "page-item"))
                    // only act if we are not on the last page already.
                    // asiigning the disabled CSS class does not disable the link action
                   ]
                   [a [classes [BS.``page-link``; "myowndbpaginationnext"]; attr.href "#"; UI.Html.on.click onClick]
                     [text "›"] ]
            )
        // last page button
        let last = paginationState.View |> View.MapCached (fun r ->
                let onClick = (fun (_el:Dom.Element) (e:Dom.MouseEvent) -> e.PreventDefault(); numberOfPages |> View.Get (fun n -> if n>0 then currentPage.Set n) )
                li [attr.``class`` "page-item"
                    UI.Html.attr.classDyn (V(if numberOfPages.V = 0 || currentPage.V=numberOfPages.V then "page-item disabled" else "page-item"))
                    // only act if we are not on the last page already.
                    // asiigning the disabled CSS class does not disable the link action
                   ]
                   [a [classes [BS.``page-link``; "myowndbpaginationlast"]; attr.href "#"; UI.Html.on.click onClick]
                     [text "»|"] ]
            )
        [ first; previous ; paginationLinks ; next; last;]

    // ********************
    // Rows per page select
    let getPerPageSelect (paginationState:Var<PaginationState>)  (count:View<int64>)=
        // use another var for handling user choice in a drop down list.
        // This is needed to properly handle the case when increasing the
        // rows per page leads to the currentPage being out of range
        let perPageDdl = Var.Create (paginationState.Value.perPage)

        // function to ensure we display a valid page when the user changes
        // the number of rows displayed per page.
        let constrainCurrentPage (el:JavaScript.Dom.Element) (ev:JavaScript.Dom.Event) =
            // access the value of a view with View.Get (fun currentValue -> ...)
            // and do work in the function passed.
            count
            |> View.Get (fun c ->
                // compute total pages with new perPage selected
                let newTotalPages = (computeNumberOfPages c (perPageDdl.Value))
                // we can now set the pagination state to a consitent value
                paginationState.Set {currentPage = (min (paginationState.Value.currentPage) newTotalPages)
                                     perPage = perPageDdl.Value
                }
            )
        li [attr.``class`` "page-item"] [
            Doc.InputType.Select [ classes [BS.``form-select-sm``  ; BS.``page-link`` ; "myowndbperpageddl"] ;
                            UI.Html.on.change constrainCurrentPage ]
                        string
                        [10; 20;30;50;100;300;500]
                        perPageDdl
        ]

    // *************
    // Search fields
    let getSearchFields (detailsResult:Result<list<Entity.EntityDetail>,string>) (paginationState:Var<PaginationState>) (searchCriteria:Var<SearchCriteria>) =

        let currentPage = paginationState.LensAuto (fun v -> v.currentPage)
        let columnSearched = searchCriteria.LensAuto (fun v -> v.column)
        let valueSearched = searchCriteria.LensAuto (fun v -> v.value)
        let valueTyped = Var.Create valueSearched.Value
        let submitOnEnter () = on.keyUp (fun el ev -> if ev.KeyCode = 13 then
                                                        valueSearched.Set valueTyped.Value
                                                        currentPage.Set 1

                                                        )
        let clearOnClick ()  = on.click (fun el ev -> valueTyped.Set ""; valueSearched.Set "")

        // search form
        let columnSearchedDdl =
          li [classes [BS.``page-item``; ] ] [
                Doc.InputType.Select [ classes [BS.``form-select-sm``  ; BS.``page-link`` ; "myowndbsearchddl"] ]
                           string
                           (detailsResult
                            |> Result.map (List.map (fun d -> d.name ))
                            |> (function
                               | Ok l -> l
                               | Error _ -> [""]
                              )
                           )
                           columnSearched
            ]
        let valueSearchedInput =
            [
                    li [attr.``class`` "page-item"

                       ]
                       [
                            Doc.InputType.Text [
                                        classes [BS.``form-select-sm``  ; BS.``page-link``; "myowndbsearchvalue"  ];
                                        submitOnEnter()
                                        Attr.Create "placeholder" (t.t("Press Enter to search"))
                                    ]
                                    valueTyped
                       ]
                    span [attr.``class`` "input-group-text myowndbclearsearch"; clearOnClick()] [li [attr.``class`` "fas fa-backspace page-item"] []]
            ] |> Doc.Concat

        [ columnSearchedDdl; valueSearchedInput;  ]

    // ************************
    // number of rows indicator
    let rowsCountIndicator (count:View<int64>) =
      li [ classes [BS.``page-item``; "myowndb-table-rows-count"];]
            [ a [ attr.``class`` "page-link" ; attr.href "#"]
                [ textView (count|> View.MapCached (fun c -> t.n("There is %1 row","There are %1 rows",c) )) ]
        ]

    // get information from database
    let queryDatabaseRows (tableType: TableType) (id:int) (searchCriteria:Var<SearchCriteria>) (sortCriteria:Var<SortCriteria>) (stateVar:Var<PageState.Datatable.State option>)=
        let paginationState =
          let defaultValue = {currentPage=1; perPage=10}
          in
          stateVar.Lens
            (fun state ->
              match state with
              | None -> defaultValue
              | Some s ->
                match s.paginationState with
                | Some pagination -> pagination
                | _ ->  defaultValue

            )
            (fun state newPagination ->
                match state with
                | None -> Some {PageState.Datatable.Empty with paginationState = Some newPagination; pushOnHistory= true}
                | Some s -> Some {s with paginationState = Some newPagination; pushOnHistory= true}
            )
        // Request the data and rows count, and collect possible error
        let dt_countViewWithoutSort = View.MapAsync2 (fun paginationState search -> async {
                                                return Server.InstancesDataTableJSON tableType id paginationState search
                                            })
                                        paginationState.View
                                        searchCriteria.View
        let dt_countView = View.MapAsync2 (fun  f sort-> f sort)
                            dt_countViewWithoutSort
                            sortCriteria.View
        let dt =  dt_countView |> View.MapCached (fun v -> match v with |Ok (data,_) -> JsonHelpers.deserialiseDataTable data
                                                                        | Error e -> JsonHelpers.deserialisedEmptyDataTable )
        let numberOfRows =  dt_countView |> View.MapCached (fun v -> match v with | Ok (_,i) -> i
                                                                                  | Error e -> 0L)
        let numberOfPages = View.Map2 (fun totalRows paginationState ->
                                computeNumberOfPages totalRows paginationState.perPage) numberOfRows paginationState.View
        let dtError =  dt_countView |> View.MapCached (fun v -> match v with |Ok _ -> None
                                                                             |Error e -> Some e)

        (dt,paginationState,numberOfRows,numberOfPages,dtError)

    // returns href value to download csv of current table rows
    let downloadCSVLink
        (tableType: TableType)
        (id:int)
        (paginationState:Var<PaginationState>)
        (searchCriteria:Var<SearchCriteria>)
        (sortCriteria:Var<SortCriteria>)=
        View.Map3 (fun paginationStateValue (searchCriteriaValue:SearchCriteria) sortCriteriaValue ->
                    Routing.router.Link (GetTableCsv
                                        (tableType,
                                         id,
                                         paginationStateValue.currentPage,
                                         paginationStateValue.perPage,
                                         searchCriteriaValue.column,
                                         searchCriteriaValue.value,
                                         sortCriteriaValue.column,
                                         sortCriteriaValue.direction
                                         )))
                 paginationState.View searchCriteria.View sortCriteria.View

    let getTHead(detailsResult:Result<Entity.EntityDetail list,string>)(stateVar:Var<PageState.Datatable.State option>) =
        let sortCriteria =
          let defaultSortCriteria = {column= None; direction= Asc}
          in
          stateVar.Lens
            (fun state ->
              match state with
              | None -> defaultSortCriteria
              | Some s ->
                match s.sortCriteria with
                | Some criteria -> criteria
                | _ ->  defaultSortCriteria
            )
            (fun state newSortCriteria ->
                JavaScript.Console.Log($"""second lens function called with {sprintf "%A" state} and {sprintf "%A" newSortCriteria}""")
                match state with
                | None -> Some { PageState.Datatable.Empty with sortCriteria = Some newSortCriteria; pushOnHistory= true}
                | Some s -> Some {s with sortCriteria = Some newSortCriteria; pushOnHistory= true}
            )

        let sortColumn = sortCriteria.LensAuto (fun v -> v.column)
        let sortDirection = sortCriteria.LensAuto (fun v -> v.direction)

        let cssForColumn (column:string) =
            sortCriteria.View
            |> View.MapCached (fun {column = sortColumnOption ; direction = d} ->
                match sortColumnOption with
                | Some sortColumn when sortColumn=column ->  d |> function |Asc -> "desc" |Desc -> "asc"
                | _ -> "both"
            )

        {| sortCriteria = sortCriteria
           markup = thead [] [
                        detailsResult
                        |> Result.map (List.map (fun d ->
                            th []
                                            [ div [ (cssForColumn (d.name))
                                                      |> View.MapCached (fun klass -> $"th-inner sortable {klass}")
                                                      |>  attr.classDyn
                                                    UI.Html.on.click (fun el ev ->
                                                                        if sortCriteria.Value.column = Some d.name  then
                                                                            // switch direction
                                                                            match sortCriteria.Value.direction with
                                                                            | Asc -> sortCriteria.Value <- {column=Some d.name; direction = Desc}
                                                                            | Desc -> sortCriteria.Value <- {column=Some d.name; direction = Asc}
                                                                        else
                                                                            sortCriteria.Value <- {column=Some d.name; direction = Asc}
                                                    )
                                                ]
                                                [ text d.name ]
                                            ]
                        ))
                        |> ( function | Ok d  -> d |> Doc.Concat
                                      | Error e -> Doc.Empty
                        )
                    ]
        |}

    // Returns a refresh button that will execute refreshFunction when clicked
    let refreshButton refreshFunction =
        span
            [
                on.click (fun ev el -> refreshFunction());
                classes [BS.``input-group-text``;"myowndbrefreshtable"]
            ]
            [
                li [attr.``class`` "fas fa-sync page-item"] []
            ]

    let getImportButton (entityId:int) =
        button
            [
                attr.title (t.t("Import CSV"))
                on.click (fun _el _ev -> ClientRouting.installedRouter.Value <- Import entityId)
                classes [BS.btn;BS.``btn-primary``;"myowndbimportcsv"]
            ]
            [
                t.tt("Import CSV")
                fontAwesomeIcon "file-import"
            ]

    let getNotificationsButton (entityId:int) = async {
        let! subscriptionsResult = Server.getInstanceNotifications entityId
        return
          match subscriptionsResult with
          | Error e-> dangerBox (text (e|>Error.toString))
          | Ok subscriptions ->
            let feedbackVar = Var.Create Doc.Empty
            let listModel = ListModel.Create (fun (s:Server.NotificationSubscriptionSpec) -> s.id) (subscriptions|> Seq.ofList)
            // Do not create text nodes here, as such a node can only be used once as a child (which we do here)
            let labelForThisEvent = t.t("For this event")
            let labelNotifyBy = t.t("Notify by")
            let labelRecipient = t.t("Recipient")
            ////////////////////////////////
            // Existing Subscriptions list
            ///////////////////////////////
            let existingSubscriptionsTable =
              listModel.View
              |> View.MapCached (fun l ->
                table
                  [classes [BS.table;BS.``table-bordered``; BS.``table-striped``; BS.``table-hover``; BS.``table-sm``; "myowndb-instance-subscriptions-list"]]
                  [
                    thead
                      []
                      [
                        th
                          [Attr.Create "scope" "col"]
                          [
                            text labelForThisEvent
                          ]
                        th
                          [Attr.Create "scope" "col"]
                          [
                            text labelNotifyBy
                          ]
                        th
                          [Attr.Create "scope" "col"]
                          [
                            text labelRecipient
                          ]
                      ]
                    l
                    |> Seq.map (fun subs ->
                      tr
                        []
                        [
                          td [classes [ BS.``table-sm``]] [text (translateNotificationEvent subs.event)]
                          td [classes [ BS.``table-sm``]] [text (translateNotificationProtocol subs.protocol)]
                          td [classes [ BS.``table-sm``]] [text (sprintf "%s" subs.destination)]
                          td
                            [
                              classes [ "myowndbactioncell" ]
                            ]
                            [(DeleteConfirmation.deleteConfirmation "notification_subscription" (fun () ->
                                async {
                                  let! r = Server.deleteInstanceNotification subs.id
                                  match r with
                                  | Ok _ ->
                                    listModel.Remove subs
                                    return true
                                  | Error es ->
                                    feedbackVar.Set(dangerBox (es|> Error.toString |> text))
                                    return false
                                }
                              )).V]
                        ]
                    )
                    |> Doc.Concat
                  ]
            )
            ////////////////////////////////
            // New subscription form
            ///////////////////////////////
            let newSubscriptionForm =
              let eventVar = Var.Create (NotificationSubscriptionCriteria.Event.AfterCreate)
              let protocolVar = Var.Create (NotificationSubscriptionCriteria.Protocol.Smtp)
              div
                []
                [
                  div
                    [classes [ BS.container; "myowndb-instance-subscription-form"]]
                    [
                      div
                        [classes [ BS.row] ]
                        [
                          div
                            [classes [BS.col; BS.``col-lg-2``]]
                            [
                              text labelForThisEvent
                            ]
                          div
                            [classes [BS.col; BS.``col-lg-2``]]
                            [
                              Doc.InputType.Select [attr.``id`` "notification_event"; classes [BS.``form-select``;BS.``form-select-sm``]] (translateNotificationEvent) [NotificationSubscriptionCriteria.Event.AfterCreate] eventVar
                            ]
                        ]
                      div
                        [classes [ BS.row] ]
                        [
                          div
                            [classes [BS.col; BS.``col-lg-2``]]
                            [
                              text labelNotifyBy
                            ]
                          div
                            [classes [BS.col; BS.``col-lg-2``]]
                            [
                              Doc.InputType.Select [attr.``id`` "notification_protocol"; classes [BS.``form-select``;BS.``form-select-sm``]] (translateNotificationProtocol) [NotificationSubscriptionCriteria.Protocol.Smtp] protocolVar
                            ]
                        ]
                      div
                        [classes [ BS.row; BS.``col-lg-2``] ]
                        [
                          button
                            [
                              classes [BS.btn; BS.``btn-primary``]
                              on.click (fun _ev _el ->
                                async {
                                  let! res = Server.createInstanceNotification entityId eventVar.Value protocolVar.Value
                                  match res with
                                  | Ok [s] ->
                                    feedbackVar.Set Doc.Empty
                                    listModel.Add s
                                  | Error es ->
                                    // The right text has been set server side
                                    feedbackVar.Set (dangerBox (es|> Error.toString |> text ))
                                  | Ok _ ->
                                    // This should never occur as only one subscription is created at a time
                                    feedbackVar.Set (dangerBox (t.tt("An error occurred")))
                                }
                                |> Async.Start
                              )
                            ]
                            [ t.tt("Save")]
                        ]

                    ]

                ]
            span
              [ classes ["myowndbmanagenotifications"] ]
              [
                buttonRevealingContent
                  {|
                    buttonTextWhenVisible = t.tt "Hide notifications"
                    buttonTextWhenHidden =  t.tt "Manage notifications"
                  |}
                  (
                    div
                      [classes [BS.card; "myowndb-instance-subscription-section"]]
                      [
                        div [ classes [BS.``card-body``]]
                            [
                              feedbackVar.V
                              (
                                // Currently we only support one type of notification, so once there is a subscription in the list,
                                // no new subscription can be created as it would be a duplicate, so we hide the form.
                                // When there are more notifications, just put newSubscriptionForm here
                                (listModel.View |> View.MapCached (fun l -> if Seq.length l > 0 then Doc.Empty else newSubscriptionForm)).V
                              )
                              existingSubscriptionsTable.V
                            ]
                      ]
                  )
              ]
    }

    // *********************************
    // function displaying the datatable
    let displayValidListTable (tableType:TableType) (entityId:int) (detailsResult:Result<list<Entity.EntityDetail>,string>) (detailsError:Option<string>) (actionsFunction:Map<string,string>->(unit->unit)->Doc)(stateVar:Var<PageState.Datatable.State option>): Async<TableDocInfo>= async {

        // get the details list for the selection of the
        // searched column
        let initialSearchedColumn = detailsResult
                                    |> function
                                        | Ok ds -> ds |> List.map (fun d -> d.name) |> List.tryHead |> Option.defaultValue ""
                                        | Error _ ->  ""
        // Search criteria Var
        let searchCriteria =
          let defaultValue = { column = initialSearchedColumn; value= ""}
          in
          stateVar.Lens
            (fun state ->
              match state with
              | None -> defaultValue
              | Some s ->
                match s.searchCriteria with
                | Some criteria -> criteria
                | _ ->  defaultValue
            )
            (fun state newCriteria ->
                match state with
                | None -> Some {PageState.Datatable.Empty with searchCriteria = Some newCriteria; pushOnHistory= true}
                | Some s -> Some {s with searchCriteria = Some newCriteria; pushOnHistory= true}
            )
        // We use the searchCriteria Var as a gateway to a table refresh. The function queryDatabaseRows below takes as argument
        // this Var, and maps its view to generate the query of rows from the server. Triggering an update will request the new
        // data from the server.
        let refreshFunction() =
            searchCriteria.Update id

        // table head
        let headInfo = getTHead detailsResult stateVar
        let tableHead = headInfo.markup
        let sortCriteria = headInfo.sortCriteria

        // query database and get views of information to display
        let rows,paginationState,numberOfRows,numberOfPages,dataError = queryDatabaseRows tableType entityId searchCriteria sortCriteria stateVar


        // collect elements displayed before the table
        let navigationLinks = getNavigationLinks paginationState numberOfPages
        let perPageSelect = getPerPageSelect paginationState numberOfRows
        let searchFields = getSearchFields detailsResult paginationState searchCriteria
        let rowsCount = rowsCountIndicator numberOfRows

        // The csv link is only displayed for main tables, not for linked instance datatables,
        // because the generation of the link by websharper is incorrect when this parameter is set as a query parameter.
        // This is needed because our authorization code maps WS endpoints to ASP.Net endpoints. However, the Main and Linked
        // cases generate different numbers of path elements, which seems hard to replicate outside of WebSharper.
        // Putting it as a GET query parameter thus seems required.
        let csvLink =
            match tableType with
            |Main ->
                let csvUrlView = downloadCSVLink tableType entityId paginationState searchCriteria sortCriteria
                csvUrlView
                |> View.MapCached
                    (fun url ->
                        // We wrap the CSV link in a span as an easy fix for tests. Datatable tests compare html captured
                        // at the time the tests were written to what is generated dureing the test. Not wrapping this in
                        // a span breaks the check of number of navigation elements.
                        // App code should not be influenced by tests, but bear with me here: it's much quicker this way
                        // and not a big deal I think.
                        span [] [
                            ul [ classes [BS.pagination] ] [
                                li [classes [BS.``page-item``] ] [
                                    a [attr.href url; classes ["myowndb_csv_download_link"; BS.``page-link``] ]
                                      [ text "CSV"; text " "; i [attr.``class`` "fas fa-file-csv"] []]
                                ]
                            ]
                        ]
                    )
             | Linked _ -> View.Const Doc.Empty
             | Linkable _ -> View.Const Doc.Empty

        // we build and return the table and accompanying widgets
        let doc =
            div [attr.id $"table_{tableType.ToString()}_entity_{entityId}"] [
                Doc.Element "nav" [Attr.Create "aria-label" "data-table-pagination"] [
                    ul [attr.``class`` "pagination pagination-sm"] [
                        navigationLinks
                        |> List.map Doc.EmbedView
                        |> (fun l -> List.append [rowsCount] l)
                        |> (fun l -> List.append l [perPageSelect])
                        |> (fun l -> List.append l searchFields)
                        |> (fun l -> List.append l [ csvLink.V ] )
                        |> (fun l -> List.append l [ refreshButton refreshFunction]  )
                        |> Doc.Concat
                    ]
                ]

                // informational
                ((View.Const detailsError ), dataError)
                ||> View.Map2 (fun detailsErr dtErr ->
                    match detailsErr, dtErr with
                    |None, None ->
                        table [classes [BS.table; BS.``table-bordered``; BS.``table-striped``; BS.``table-hover``; BS.``table-sm``]] [

                            tableHead
                            tbody [] [
                                rows |> View.MapCached ( // get memoized evaluator generating the markup for the detail_value's data type
                                                   // this avoids an evaluator instanciation for each value displayed
                                                   let getEvaluator = DetailvalueFormater.memoizedEvaluatorGetter()
                                                   // then map rows to tr markup
                                                   Array.map (fun (r:Map<string,string>) ->
                                                                    // each data row is mapped to a table row
                                                                    tr [] [
                                                                    // colums are take from the details list
                                                                    detailsResult
                                                                    // the item of the map corresponding to the column (d.name) is mapped to a td
                                                                    |> Result.map (List.map (fun d ->
                                                                                                      let value = r.Item (d.name)
                                                                                                      td [ classes ["myowndbdatacell"; BS.``table-sm``]]
                                                                                                         [ (DetailvalueFormater.displayDetailValue d value)]))
                                                                    // all tds need to be concatenated in one Doc
                                                                    |> Result.map Doc.Concat
                                                                    // finally extract the result
                                                                    |> function |Ok v -> v |Error e -> Doc.Empty
                                                                    // datacell are above, now add action cells
                                                                    actionsFunction r refreshFunction
                                                                    ]
                                                                    )

                                                    // concatenate all rows in one doc
                                                    >> Doc.Concat
                                                // Embed the View<Doc> of all rows in the page
                                                ) |> Doc.EmbedView
                            ]
                        ]
                    | Some e1 , _ -> text (e1)
                    | _ , Some e2 -> text (e2)
                )
                |> Doc.EmbedView
            ]
        return
            {
                doc = doc
                refresh = refreshFunction
                numberOfRows = numberOfRows
            }
    }


    let displayListTable (tableType:TableType) (entityId:int) (actionsFunction:Map<string,string>->(unit->unit)->Doc) (tableState:Var<PageState.Datatable.State option>)= async {
        // request details, and collect the possible error
        let! detailsResult = Server.EntityDetails entityId true
        match detailsResult with
            // FIXME: translate strings
            // We return the Doc and a refreshFunction that can trigger a refresh of the datatable content
            | Ok [] ->
                return
                    TableDocInfo.fromDoc
                        (div [] [text "empty details list"])
            | Ok _ ->
                // FIXME: pass state
                let! tableDocInfo =  displayValidListTable (tableType:TableType) (entityId:int) detailsResult None actionsFunction tableState
                return tableDocInfo
            | Error e ->
                return
                    TableDocInfo.fromDoc
                        (div [] [text "details list error"])
    }
    // This returns the refresh function in addition to the table itself
    let displayRefreshableListTable (tableType:TableType) (entityId:int) (actionsFunction:Map<string,string>->(unit->unit)->Doc)(stateVar:Var<PageState.Datatable.State option>)= async {
        // request details, and collect the possible error
        let! detailsResult = Server.EntityDetails entityId true
        match detailsResult with
            // FIXME: translate strings
            // We return the Doc and a refreshFunction that can trigger a refresh of the datatable content
            | Ok [] -> return TableDocInfo.fromDoc (div [] [text "empty details list"])
            | Ok _ -> return! displayValidListTable (tableType:TableType) (entityId:int) detailsResult None actionsFunction stateVar
            | Error e -> return TableDocInfo.fromDoc (div [] [text "details list error"])
    }

    // Function returning section above a datatable to add new instances.
    let addInstanceSection (entityId:int) (formKind:FormHelpers.FormKind) refreshFunction=
        let afterSaveCallback (formKind) (detailsSpecs)(instanceSpec)=
            refreshFunction()
            FormHelpers.displaySavedMessageCompact formKind detailsSpecs instanceSpec
        div
          [attr.id "add_instance_section"]
          [
              buttonRevealingContent
                {|
                  buttonTextWhenVisible = t.tt "Hide form"
                  buttonTextWhenHidden =  t.tt "Add new"
                |}
                (
                  div
                    [classes [BS.card]]
                    [
                      div [ classes [BS.``card-body``]]
                          [
                              match formKind with
                              | Creation -> InstanceForm.displayCreate  entityId afterSaveCallback |> Doc.Async
                              | Link _ -> InstanceForm.displayLink entityId formKind afterSaveCallback |> Doc.Async
                              | _ -> Doc.Empty

                          ]
                    ]
                )
              // We display the import button only if the from above is a instance creation form. For a
              // linking form, we do not display the import button
              // Same for notification subscriptions button
              (
                match formKind with
                | FormHelpers.Creation ->
                   [
                    getImportButton entityId
                    getNotificationsButton entityId |> Doc.Async
                   ]
                   |> Doc.Concat
                | FormHelpers.Link _ | FormHelpers.Edition | FormHelpers.Public -> Doc.Empty
              )
          ]

    let displayMainListTable (id:int) (pageInfo:PageInfo.Instances.PageInfo) (stateVar:Var<PageState.Datatable.State option>)= async{

        let deleteElement (instanceId:string)(refreshFunction:unit->unit)=
            DeleteConfirmation.deleteConfirmation "instance" (
                (fun () -> async {
                    match! (Server.deleteInstance (int instanceId)) with
                    | Ok _ ->
                        refreshFunction()
                        // signal success
                        return true
                    | Error _ ->
                        // signal failure
                        return false
                })
            )
        let mainActionsFunction (r:Map<string,string>) (refreshFunction:unit->unit) =
            [
                td [attr.``class`` "myowndbactioncell"]
                    [ a [ attr.``data-`` "myowndb-action" "view"; attr.href (Routing.router.Link (Instance(int (r.Item "id"), None) ) )]
                        [li [attr.``class`` "fas fa-search"] []]]
                td [attr.``class`` "myowndbactioncell"]
                    [a  [ attr.``data-`` "myowndb-action" "edit"; attr.href (Routing.router.Link (EditInstance(int (r.Item "id")) ) )]
                        [li [attr.``class`` "fas fa-edit"] []]]
                td
                    [attr.``class`` "myowndbactioncell"]
                    [
                        span
                            [classes [ BS.``btn-link``] ]
                            [(deleteElement (r.Item "id") refreshFunction).V]
                    ]
            ]
            |> Doc.Concat
        let! tableDocInfo =  displayRefreshableListTable Main id mainActionsFunction stateVar
        let additionSection = addInstanceSection id Creation tableDocInfo.refresh
        let content =
            div
                []
                [
                    additionSection
                    tableDocInfo.doc
                ]
        return content

    }
