namespace web

open DB
open System.Security.Claims
open Microsoft.AspNetCore.Authentication
open FsToolkit.ErrorHandling
open Ids
open IdsExtensions

type GetId(signInManager: Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser>) =
    member self.EntityId (client: DBClient) (user: ClaimsPrincipal) (id: int) =
        // This function will issue one query to check if a reload of claims is needed
        // This is needed for example when an entity has bee created after the user logged in
        let addNewClaimIfNeededAsync client (user: ClaimsPrincipal) (id: int) =
            async {
                MyOwnDBLib.Logging.Log.Debug("User does not have claim required for entity {id}", id)
                MyOwnDBLib.Logging.Log.Debug("Will check if claims refresh is needed")
                // get user id from the claims
                let idClaim = user.FindFirst(ClaimTypes.NameIdentifier)
                let userId = int idClaim.Value
                // query to check if the entity is in the account of the user
                let! countResult =
                    DBAction.querySingleAction
                        "select count(*) from entities e
                    join databases d on (e.database_id=d.id)
                    join accounts a on (d.account_id=a.id)
                    join users u on (u.account_id=a.id)
                    where u.id=%d and e.id=%d"
                        userId
                        id
                    |> DBAction.run<int64> client
                // as we cound the entitied with the requested id, only a count of 1 is valid and
                // means the user should be granted access
                match countResult with
                | Ok [ 1L ] ->
                    MyOwnDBLib.Logging.Log.Debug("We found the entity for the user, will re-signin")

                    let! appUser =
                        signInManager.UserManager.GetUserAsync(user)
                        |> Async.AwaitTask
                    // this reloads all claims from the database

                    MyOwnDBLib.Logging.Log.Debug("{claims}", user.Claims)

                    do!
                        // FIXME: can we pass the AuthenticationProperties from the current session?
                        signInManager.SignInWithClaimsAsync(
                            appUser,
                            AuthenticationProperties(IsPersistent = true),
                            additionalClaims = Seq.append user.Claims [| Claim("Entity", string id) |]
                        )
                        |> Async.AwaitTask

                    MyOwnDBLib.Logging.Log.Debug("{claims}", user.Claims)

                    return true
                | Ok l ->
                    MyOwnDBLib.Logging.Log.Debug("will return false as no correct Ok result {r}", l)
                    return false
                | Error e ->
                    MyOwnDBLib.Logging.Log.Debug("will return false as error {e}", e)
                    return false
            }

        let validator (id: int) =
            async {
                MyOwnDBLib.Logging.Log.Debug(
                    "haClaims Entity{id}? {result}\n\tUser's claims:{claims}",
                    id,
                    user.HasClaim("Entity", string id),
                    user.Claims
                )
                |> ignore

                if user.HasClaim("Entity", string id) then
                    return true
                else
                    return! addNewClaimIfNeededAsync client user id
            }

        Ids.EntityId.ValidateAsync(id, validator)

    member self.EntityIdAction (user: ClaimsPrincipal) (id: int) =
        // This function will issue one query to check if a reload of claims is needed
        // This is needed for example when an entity has bee created after the user logged in
        let addNewClaimIfNeededAction (user: ClaimsPrincipal) (id: int) =
            MyOwnDBLib.Logging.Log.Debug("User does not have claim required for entity {id}", id)
            MyOwnDBLib.Logging.Log.Debug("Will check if claims refresh is needed")
            // get user id from the claims
            let idClaim = user.FindFirst(ClaimTypes.NameIdentifier)
            let userId = int idClaim.Value
            // query to check if the entity is in the account of the user
            let countResultAction =
                DBAction.querySingleAction
                    "select count(*) from entities e
                join databases d on (e.database_id=d.id)
                join accounts a on (d.account_id=a.id)
                join users u on (u.account_id=a.id)
                where u.id=%d and e.id=%d"
                    userId
                    id
            countResultAction
            |> DBAction.bind (fun count ->
              match count with
              | 1L ->
                MyOwnDBLib.Logging.Log.Debug("We found the entity for the user, will re-signin")

                let appUserAction =
                    signInManager.UserManager.GetUserAsync(user)
                    |> Async.AwaitTask
                    |> DBAction.fromAsync
                // this reloads all claims from the database
                appUserAction
                |> DBAction.bind (fun appUser ->
                    // FIXME: can we pass the AuthenticationProperties from the current session?
                    signInManager.SignInWithClaimsAsync(
                        appUser,
                        AuthenticationProperties(IsPersistent = true),
                        additionalClaims = Seq.append user.Claims [| Claim("Entity", string id) |]
                    )
                    |> Async.AwaitTask
                    |> DBAction.fromAsync
                )
                |> DBAction.map (fun _ -> true)
              | c ->
                  MyOwnDBLib.Logging.Log.Error("No access to entity id {id} as no correct Ok result {r} for user {u}", id, c, user.Identity.Name)
                  DBAction.retn false
            )
            |> DBAction.mapError (fun e ->
              MyOwnDBLib.Logging.Log.Error("No access to entity id {id} as no error {e} for user {u}", id, e|>Error.toString, user.Identity.Name)
              e
            )

        let validatorAction (id: int) =
            MyOwnDBLib.Logging.Log.Debug(
                "haClaims Entity{id}? {result}\n\tUser's claims:{claims}",
                id,
                user.HasClaim("Entity", string id),
                user.Claims
            )
            |> ignore

            if user.HasClaim("Entity", string id) then
                DBAction.retn true
            else
                addNewClaimIfNeededAction user id

        Ids.EntityId.ValidateAction(id, validatorAction)

    // Currently users have access to an instance if they have access to the corresponding entity
    member self.InstanceId (client: DBClient) (user: ClaimsPrincipal) (id: int) =
        // get entity_id from the instance
        let validator (id:int) =
            // retrieve the entity_id for the instance id we get
            DBAction.querySingleAction "select entity_id from instances where id=%d" id
            // issue query
            |> DBAction.run<int> client
            // map to Result as we only have one value returned
            |> Async.map DBResult.toSingleResult
            // Check user has access to the entity id retrieve in previous steps
            |> AsyncResult.bind (fun entityId -> async {
                return! (self.EntityId client user entityId
                        // map option to result
                        |> Async.map (function
                            |Some v -> Result.Ok v
                            | None -> Error (Error.fromString $"Error getting entityId {entityId} to validate access to instance {id}")) )
               })
            |> AsyncResult.mapError (fun e ->
               MyOwnDBLib.Logging.Log.Debug ("Error getting entity_id for instance {instance_id}, possibly due to authorization failure: {error}", id, e )
               e
            )
            // if we have an EntityId, return true to signal access to the instance is granted
            |> Async.map (fun v ->
               match v with
               |Ok _ -> true
               |Error _ -> false)

        Ids.InstanceId.ValidateAsync(id, validator)

    member self.RelationId (client: DBClient) (user: ClaimsPrincipal) (id: int) =
        // get entity_id from the instance
        let validator (id:int) = async {
            // retrieve the entity ids for child and parent
            let! entityIdsResult =
                // Do a "union all" to keep duplicate rows, needed when a relations self references an entity.
                DBAction.queryAction "select child_id as id from relations where id=%d UNION ALL select parent_id as id from relations where id=%d" id id
                // issue query
                |> DBAction.run<int> client
            match entityIdsResult with
            // If there's an error, log it and refuse access
            | Error es ->
                MyOwnDBLib.Logging.Log.Error($"Error getting entity ids for relation {id}: {es |> Error.toString}")
                return false
            // Otherwise, check the user has access to both entities
            | Ok entityIdList ->
                let childId = entityIdList[0]
                let parentId = entityIdList[1]
                let! childOption = self.EntityId client user childId
                let! parentOption = self.EntityId client user parentId
                return childOption.IsSome && parentOption.IsSome
        }

        Ids.RelationId.ValidateAsync(id, validator)

    member self.DatabaseId (client: DBClient) (claimsPrincipal: ClaimsPrincipal) (id: int) =
        // We cannot go through an EntityId validation as new databases have no entity.
        // We check that the user has the claim to access the account the database is linked to.
        let validator (id:int) = async {
            let! accountIdResult =
                DBAction.queryAction "select account_id from databases where id=%d" id
                // issue query
                |> DBAction.run<int> client
            match accountIdResult with
            // If there's an error, log it and refuse access
            | Error es ->
                MyOwnDBLib.Logging.Log.Error($"Error getting account_id from databases to validated access to database id: {es |> Error.toString}")
                return false
            // Otherwise, check the user has access to both entities
            | Ok [accountIdInt] ->
                return claimsPrincipal.HasClaim("AccountId",string accountIdInt)
            | Ok l ->
                MyOwnDBLib.Logging.Log.Error($"""Error getting database id: multiple values retrieved! {sprintf "%A" l}""")
                return false
        }

        DatabaseId.ValidateAsync(id, validator)
module Authorization =
    open Microsoft.AspNetCore.Http
    let isAdmin (httpContext:HttpContext) =
        httpContext.User.Identity.IsAuthenticated && httpContext.User.HasClaim("UserType","primary")
    let isAuthenticated (httpContext:HttpContext) =
        httpContext.User.Identity.IsAuthenticated
