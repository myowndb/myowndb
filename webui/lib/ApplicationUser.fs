namespace web
open Microsoft.AspNetCore.Identity
open System.Security.Claims
open FSharp.Core
open FSharpPlus


type ApplicationUserSpec= {
    accountId: int
    userTypeId: int
    email: string
    firstname: string option
    lastname: string option
}
// if you want to return null in a Task<ApplicationUser> as needed
// for the user store, you need to add this annotation.
[<AllowNullLiteral>]
type ApplicationUser() =
    // inheriting from IdentityUser, but alternatively defining the fields
    // ourselves seems to work too. As a reference, here's what I implemented
    // when not inherithing from IdentityUser:
    //  member val Id:string = null with get,set
    //  member val NormalizedEmail:string = null with get,set
    //  member val UserName:string = null with get,set
    //  member val NormalizedUserName:string = null with get,set
    //  member val Email:string = null with get,set
    //  member val PasswordHash:string = null with get,set
    //  member val EmailConfirmed:bool = false with get,set
    inherit IdentityUser<int>()

    member val accountId:int = -1 with get,set
    // ordinary user by default
    member val userTypeId:int = 2 with get,set
    member val firstname:string = null with get,set
    member val lastname:string = null with get,set
    member val Claims:List<Claim> = [] with get,set
    member this.isPrimary = this.userTypeId = 1
    // probably not needed:
    //member this.isPrimaryAction =
    //    // intermediate assignment to specify type
    //    let r:DBAction.DBAction<bool> = DBAction.queryAction "select (CASE WHEN ut.name='primary_user' THEN true ELSE false END) from users u join user_types ut on (u.user_type_id=ut.id) where u.id=%d" (int this.Id)
    //    r

    static member Create (u:User.User) =
        let appUser = ApplicationUser()
        let (Ids.UserId uid) = u.id
        appUser.Id <- uid
        let (Ids.AccountId accountId) = u.account_id
        appUser.accountId <- accountId
        appUser.userTypeId <-u.user_type_id
        appUser.UserName <- (u.login)
        appUser.NormalizedUserName <- (u.login|> String.toUpper)
        appUser.Email <- (u.email)
        appUser.NormalizedEmail <- (u.email|> String.toUpper)
        appUser.firstname <- (u.firstname|> (Option.defaultValue null))
        appUser.lastname <- (u.lastname|> (Option.defaultValue null))
        appUser.PasswordHash <- (u.aspnet_pass_hash|> (Option.defaultValue null))
        appUser.EmailConfirmed <- u.verified>0
        appUser

    static member FromSpec (u:ApplicationUserSpec) =
        let appUser = ApplicationUser()
        appUser.accountId <-u.accountId
        appUser.userTypeId <-u.userTypeId
        appUser.NormalizedEmail <- (u.email |> String.toLower)
        appUser.UserName <- (u.email)
        appUser.NormalizedUserName <- (u.email|> String.toLower)
        appUser.Email <- (u.email)
        appUser.UserName <- (u.email)
        if (Option.isSome u.firstname) then
            appUser.firstname <- u.firstname|> Option.get
        if (Option.isSome u.lastname) then
            appUser.lastname <- u.lastname|> Option.get
        appUser
    static member FromUser (u:User.User) =

        let appUser = ApplicationUser()
        let (Ids.UserId userId) = u.id
        appUser.Id <-userId
        let (Ids.AccountId accountId) = u.account_id
        appUser.accountId <- accountId
        appUser.userTypeId <- u.user_type_id
        appUser.NormalizedEmail <- (u.email |> String.toLower)
        appUser.UserName <- (u.email)
        appUser.NormalizedUserName <- (u.email|> String.toLower)
        appUser.Email <- (u.email)
        appUser.UserName <- (u.email)
        if (Option.isSome u.firstname) then
            appUser.firstname <- u.firstname|> Option.get
        if (Option.isSome u.lastname) then
            appUser.lastname <- u.lastname|> Option.get
        appUser.EmailConfirmed <- u.verified>0
        appUser
