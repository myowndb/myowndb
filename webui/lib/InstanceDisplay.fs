namespace web

open WebSharper
open WebSharper.UI
open WebSharper.UI.Html
// needed for UI.Html.attr.disableDynPred
open WebSharper.UI.Client
open HtmlHelpers
open Css
open ClientI18n
open WebSharper.UI.Templating
open WebSharper.JavaScript

type InstanceShowTemplate = Template<"templates/instance/Show.html">

[<JavaScript>]
module InstanceDisplay =
    let t=JStr()

    let instanceField(key:string,value:Doc) =
        InstanceShowTemplate.Field()
            .Key(key)
            .Value(value)
            .Doc()

    let instanceMapToHtml (instanceId:int)(title:string) (details:list<Entity.EntityDetail>) (instance:Map<string,string>) =
        let title = t.t("Record from table %1",title)
        InstanceShowTemplate()
            .CardTitle(string title)
            .EditLink(Routing.router.Link (EndPoint.EditInstance instanceId))
            .Edit(t.t("Edit"))
            .Fields(
                //// This code does not display fields with null values like rails version
                //instance
                //|> Map.filter (fun k v -> v<>null)
                //|> Map.toList
                //|> List.map instanceField
                // This code displays all fields, even those without value
                // I use this version now to possibly add inplace editing of all fields.
                details
                |> List.map (fun d -> match instance.TryGetValue(d.name) with
                                      | true, v -> d.name, (if isNull v then Doc.Empty else DetailvalueFormater.displayDetailValue d v)
                                      | _ -> d.name, Doc.Empty
                )
                |> List.map instanceField
            )
            .Doc()

    open LinkedInstancesTypes
    open DatatableTypes

    let displayLinkableInstances (tableType:DatatableTypes.TableType) (entityId:Ids.EntityId)(actionsFunction: Map<string,string>->(unit->unit) -> Doc) (tableState:Var<PageState.Datatable.State option>)=  async {
        match tableType with
        | Main ->
            return TableDocInfo.fromDoc (div [] [text "error"])
        | Linked info
        | Linkable info ->
            let linkableTableType = Linkable info
            let! tableDocInfo = web.Datatable.displayListTable linkableTableType (entityId|> Ids.EntityId.Get) actionsFunction tableState
            return tableDocInfo
    }

    let displayLinkedInstances (dirAndId:RelatedInDirectionForIntInstanceId) (stateVar:Var<PageState.InstanceDisplay.State option>) =  async {
            let titleForRelation (dirAndId:RelatedInDirectionForIntInstanceId) (relation:Relation.Relation) =
                match dirAndId with
                | ChildrenFor sourceId -> relation.from_parent_to_child_name
                | ParentsFor sourceId -> relation.from_child_to_parent_name

            // Get relation ids from server
            let! relationsToLinkedResult =
                match dirAndId with
                | ChildrenFor sourceId -> Server.getChildrenRelations sourceId
                | ParentsFor sourceId -> Server.getParentsRelations sourceId

            let displayLinkedAndLinkableTables (relation_destination_type:int)(relationId:Ids.RelationId)(entityId:Ids.EntityId)(linked:TableDocInfo)(setupLinkable:unit -> Async<TableDocInfo>) =
                // Initialise these vars outside the View.Map so that they aren't reinitialised each time the View.Map function is run.
                // Var indicating if the linkable list is visible
                let linkableVisible = Var.Create false
                // As initially we don't retrieve the linkable instances table from the server, we store None in this Var.
                // When it is retrieved, it is set to Some, and won't change after that.
                let linkableTableDocInfoOption:Var<Option<TableDocInfo>> = Var.Create None
                linked.numberOfRows
                |> View.Map (fun numberOfLinkedInstances ->
                    if numberOfLinkedInstances = 1 && relation_destination_type = 1 then
                        linked.doc
                    else
                        let linkableVisibilityClass =
                            linkableVisible.View
                            |> View.Map
                                (fun isVisible ->
                                    if isVisible then
                                        String.concat " " [BS.``d-block``; BS.card; BS.``w-75``]
                                    else
                                        String.concat " " [BS.``d-none``; BS.card]
                                )
                        let buttonText =
                            linkableVisible.View
                            |> View.Map (fun visible->
                                if visible then
                                    t.tt "Finish linking existing entry"
                                else
                                    t.tt "Link existing entry"

                            )
                        // View<Doc> to insert in the DOM according to the linkableTableDocInfoOption content
                        let linkableDocView =
                            linkableTableDocInfoOption.View
                            |> View.Map(
                                function
                                | None -> Doc.Empty
                                | Some i -> i.doc
                            )
                        // A view combining both visibility and retrieved status
                        let isVisibleIsRetrieved =
                            View.Map2 (fun visible (doc:Option<TableDocInfo>) -> (visible,doc.IsSome))
                                linkableVisible.View
                                linkableTableDocInfoOption.View
                        //Function that will be called by a click on the button
                        let displayLinkableTable (visible,retrieved) = async {
                            match visible,retrieved with
                            // If visible, just hide it. It must have been retrieved at that point.
                            | true, _ ->
                                linkableVisible.Set (not visible)
                            // if it was alredy retrieved (by a previous click on the button), just make it visible again after refreshing it
                            | false, true ->
                                // Refresh the linkable table to be sure it is up to date
                                linkableTableDocInfoOption.Value |> Option.iter (fun i -> i.refresh())
                                // Make it visible
                                linkableVisible.Set (not visible)
                            | false, false ->
                                    // Call the thunk we got to setup the linkable table, we get a TableDocInfo value
                                    let! linkable = setupLinkable()
                                    // Update the Vars accordingly
                                    linkableTableDocInfoOption.Set (Some linkable)
                                    linkableVisible.Set (not visible)
                        }

                        let additionSection = Datatable.addInstanceSection (entityId|>Ids.EntityId.Get) (FormHelpers.Link {relationId=relationId|>Ids.RelationId.Get; dirAndId=dirAndId}) linked.refresh

                        div []
                            [
                                button
                                    [
                                        classes [BS.btn; BS.``btn-primary``; BS.``m-2``]
                                        on.clickView  isVisibleIsRetrieved (fun _ev _el isVisibleIsRetrieved-> (displayLinkableTable isVisibleIsRetrieved)|> Async.StartImmediate )
                                    ]
                                    [   buttonText.V
                                    ]
                                additionSection
                                div [attr.classDyn linkableVisibilityClass ]
                                    [
                                        div [ classes [BS.``card-body``; BS.``d-flex``; BS.``justify-content-center``]]
                                            [
                                                linkableDocView.V
                                            ]
                                    ]
                                linked.doc
                            ]
                )

            // for each relation display a div with the list of linked instances
            return
                relationsToLinkedResult
                |> DBResult.map (fun (relation,tableType) ->
                        // We create Vars to store the refresh function of each table because each table calls the refresh of the other and
                        // assigning both with a let is not possible I think  (let .. and is not available because it is async, so needs `let!`)
                        let linkedRefreshFunction = Var.Create (fun () -> ())
                        let linkableRefreshFunction = Var.Create (fun () -> ())
                        // Id used to assign a CSS id to the card, and as a key in the page state map
                        let tableId = tableType.ToString()
                        // We define a Var holding this table state by lensing into the page state's map.
                        // This Var will be updated everytime the page state is modified. This means that
                        // everytime another table on the page changes state, all tables of the page have their
                        // rawTableState updated.
                        // To avoid this, we define another Var that will only be updated when the rawTableState
                        // gets a different value.
                        let rawTableState =
                          stateVar.Lens
                            (fun s ->
                              match s with
                              | None ->  None
                              | Some state ->
                                state.map.TryFind tableId
                            )
                            (fun s newTableState ->
                              match s with
                              | None ->
                                let initState = PageState.InstanceDisplay.Empty
                                Some { initState with map = Map.add tableId (newTableState|>Option.defaultValue PageState.Datatable.Empty) initState.map }
                              | Some state ->
                                Some { state with map = Map.add tableId (newTableState|>Option.defaultValue PageState.Datatable.Empty) state.map}
                            )

                        // Define that table state actually passed down to the datatable functions.
                        let tableState:Var<PageState.Datatable.State option> = (Var.Create (rawTableState.Value))
                        // Run a sink to update the tableState when a *different* value is set in the page state.
                        // This is how we avoid to have an redisplay if our state didn't change.
                        rawTableState.View
                              |> View.Sink (fun newTableState ->
                                if tableState.Value <> newTableState then
                                  tableState.Set newTableState
                              )
                        // Run a sink to update the rawTableState as this is used to update the page state that is reflected in the URL.
                        // Without this sink, the page state in the URL would never be updated.
                        tableState.View
                              |> View.Sink (fun newTableState ->
                                if rawTableState.Value <> newTableState then
                                  rawTableState.Set newTableState
                              )
                        // FIXME: we get the int from the entity id only to again build the EntityId inside the function call
                        let table =
                            match dirAndId with
                            // Both branches of the match have very similar code, but differing each time on the argument being the parent of the child id.
                            // It seems more approachable to leave it like this rather than refactor to remove repetition at the prive of making it more
                            // complex to understand.
                            |ChildrenFor parentId ->


                                // Function called on click of the link icon in the linkable instances list
                                // It takes as only argument the childId, which will be specific for each row, and passed to the call of this
                                // function by the click handler, which has access to the row's values map from which the childId will be extracted.
                                let linkableLinkAction childId= async {
                                    let! linkIdResult =  Server.linkInstances relation.id parentId childId
                                    match linkIdResult with
                                    | Ok _ ->
                                        // This is a call to the function stored in the Var, not a call to Value() method!
                                        linkedRefreshFunction.Value()
                                        linkableRefreshFunction.Value()
                                    | Error es -> JavaScript.Console.Log($"Failed linking instances:{es|> Error.toString}")
                                }
                                let linkedUnlinkAction childId= async {
                                    let! linkIdResult =  Server.unlinkInstances relation.id parentId childId
                                    match linkIdResult with
                                    | Ok _ ->
                                        // This is a call to the function stored in the Var, not a call to Value() method!
                                        linkedRefreshFunction.Value()
                                        linkableRefreshFunction.Value()
                                    | Error es -> JavaScript.Console.Log($"Failed unlinking instances:{es|> Error.toString}")
                                }
                                // Function that returns the action cell to unlink an instances. It is thus used in the linked instances table
                                let linkedActionsFunction (r:Map<string,string>) (refreshFunction:unit->unit) =
                                    [
                                        td [attr.``class`` "myowndbactioncell"]
                                            [ a [ attr.``data-`` "myowndb-action" "view"; attr.href (Routing.router.Link (Instance(int (r.Item "id"),None) ) )]
                                                [li [attr.``class`` "fas fa-search"] []]]

                                        td [attr.``class`` "myowndbactioncell";  on.click (fun _el ev -> ev.PreventDefault(); linkedUnlinkAction (int (r.Item "id")) |> Async.StartImmediate )]
                                            [
                                                a
                                                    [attr.``data-`` "myowndb-action" "unlink"; attr.href "#";]
                                                    [li [attr.``class`` "fas fa-unlink"] []]
                                            ]
                                    ]
                                    |> Doc.Concat
                                // Function that returns the action cell to link an instances. It is thus used in the linkable instances table
                                let linkableActionsFunction (r:Map<string,string>) (refreshFunction:unit->unit)=
                                    [
                                        td [attr.``class`` "myowndbactioncell"]
                                            [ a [ attr.``data-`` "myowndb-action" "view"; attr.href (Routing.router.Link (Instance(int (r.Item "id"),None) ) )]
                                                [li [attr.``class`` "fas fa-search"] []]]

                                        td [attr.``class`` "myowndbactioncell"; on.click (fun el ev ->ev.PreventDefault();   linkableLinkAction (int (r.Item "id")) |> Async.StartImmediate)]
                                           [
                                                a
                                                    [attr.``data-`` "myowndb-action" "link"; attr.href "#";]
                                                    [
                                                        li [attr.``class`` "fas fa-link"] []
                                                    ]
                                           ]
                                    ]
                                    |> Doc.Concat

                                async {
                                    // Get the entityId of the instances linked through this relation
                                    let entityId = (Ids.EntityId.Get relation.child_id)
                                    // Get the tableInfo for linked instances and set its refresh function in the right var
                                    let! linkedTableInfo = web.Datatable.displayListTable tableType entityId linkedActionsFunction tableState
                                    linkedRefreshFunction.Set linkedTableInfo.refresh
                                    // Get the tableInfo for linkable instances and set its refresh function in the right var
                                    let setupLinkable() = async {
                                        let! linkableTableInfo = displayLinkableInstances tableType relation.child_id linkableActionsFunction tableState
                                        linkableRefreshFunction.Set linkableTableInfo.refresh
                                        return linkableTableInfo
                                    }
                                    // return the doc
                                    return displayLinkedAndLinkableTables relation.child_side_type_id relation.id relation.child_id linkedTableInfo setupLinkable |> Doc.EmbedView
                                } |> Doc.Async
                            |ParentsFor childId ->
                                // Function called on click of the link icon in the linkable instances list
                                // It takes as only argument the parentId, which will be specific for each row, and passed to the call of this
                                // function by the click handler, which has access to the row's values map from which the childId will be extracted.
                                let linkableLinkAction parentId= async {
                                    let! linkIdResult =  Server.linkInstances relation.id parentId childId
                                    match linkIdResult with
                                    | Ok _ ->
                                        // This is a call to the function stored in the Var, not a call to Value() method!
                                        linkedRefreshFunction.Value()
                                        linkableRefreshFunction.Value()
                                    | Error es -> JavaScript.Console.Log($"Failed linking instances:{es|> Error.toString}")
                                }
                                let linkedUnlinkAction parentId= async {
                                    let! linkIdResult =  Server.unlinkInstances relation.id parentId childId
                                    match linkIdResult with
                                    | Ok _ ->
                                        // This is a call to the function stored in the Var, not a call to Value() method!
                                        linkedRefreshFunction.Value()
                                        linkableRefreshFunction.Value()
                                    | Error es -> JavaScript.Console.Log($"Failed unlinking instances:{es|> Error.toString}")
                                }
                                // Function that returns the action cell to unlink an instances. It is thus used in the linked instances table
                                let linkedActionsFunction (r:Map<string,string>) (refreshFunction:unit->unit) =
                                    [
                                        td [attr.``class`` "myowndbactioncell"]
                                            [ a [ attr.href (Routing.router.Link (Instance(int (r.Item "id"),None) ) )]
                                                [li [attr.``class`` "fas fa-search"] []]]

                                        td [attr.``class`` "myowndbactioncell";  on.click (fun _el ev -> ev.PreventDefault(); linkedUnlinkAction (int (r.Item "id")) |> Async.StartImmediate )]
                                            [
                                                a
                                                    [attr.``data-`` "myowndb-action" "unlink"; attr.href "#";]
                                                    [li [attr.``class`` "fas fa-unlink"] []]
                                            ]
                                    ] |> Doc.Concat

                                // Function that returns the action cell to link an instances. It is thus used in the linkable instances table
                                let linkableActionsFunction (r:Map<string,string>) (refreshFunction:unit->unit) =
                                    [
                                        td [attr.``class`` "myowndbactioncell"]
                                            [ a [ attr.href (Routing.router.Link (Instance(int (r.Item "id"),None) ) )]
                                                [li [attr.``class`` "fas fa-search"] []]]

                                        td [attr.``class`` "myowndbactioncell"; on.click (fun el ev ->ev.PreventDefault();  linkableLinkAction (int (r.Item "id")) |> Async.StartImmediate)]
                                           [
                                            a
                                                [attr.``data-`` "myowndb-action" "link"; attr.href "#"]
                                                [li [attr.``class`` "fas fa-link"] []]
                                           ]
                                    ]
                                    |> Doc.Concat

                                async {
                                    // Get the entityId of the instances linked through this relation
                                    let entityId = (Ids.EntityId.Get relation.parent_id)
                                    // Get the tableInfo for linked instances and set its refresh function in the right var
                                    let! linkedTableInfo = web.Datatable.displayListTable tableType entityId linkedActionsFunction tableState
                                    linkedRefreshFunction.Set linkedTableInfo.refresh
                                    // Get the tableInfo for linkable instances and set its refresh function in the right var
                                    let setupLinkable () =async {
                                        let! linkableTableInfo = displayLinkableInstances tableType relation.parent_id linkableActionsFunction tableState
                                        linkableRefreshFunction.Set linkableTableInfo.refresh
                                        return linkableTableInfo
                                    }
                                    return displayLinkedAndLinkableTables relation.parent_side_type_id relation.id relation.parent_id linkedTableInfo setupLinkable |> Doc.EmbedView
                                }|>Doc.Async
                        wrapInCard $"card_{tableId}" (titleForRelation dirAndId relation) table
                )
                |> (function
                            | DBResult.DBResult.Ok docList ->
                                docList
                            | DBResult.DBResult.Error e ->
                                    // FIXME: report error
                                    [ div [] [text "error"]]
                )

    }
    let displayInstance id (pageInfo:PageInfo.Instance.PageInfo) (stateVar:Var<PageState.InstanceDisplay.State option>)= async {
        try
            let! detailsResult = Server.InstanceDetails id
            let! dtResult = Server.getInstanceDatatable id
            let! children = displayLinkedInstances (ChildrenFor id) stateVar
            let! parents = displayLinkedInstances (ParentsFor id) stateVar

            match detailsResult,dtResult with
            |Ok details,Some data ->
                return JsonHelpers.deserialiseDataTable data
                        |> Array.head
                        // parents.[1] causes trouble, I don't know why
                        |> instanceMapToHtml id pageInfo.entity.name details
                        |> (fun body ->
                            div []
                                (parents
                                 |> List.append children
                                 |> List.append  [ h1 [] [t.tt("Related Records")]]
                                 |> List.append [body]))
            |_,_ -> return div [] [ t.tt("Instance could not be retrieved.") ]
        with
        | e -> return div [] [text e.Message]
    }
