namespace web

open WebSharper

[<JavaScript>]
module FormHelpers =
    open Specs
    // We have different behaviour according to the form type. This type
    // enables to distinguish on which type of form ware are currently working.
    type LinkFormInfo = {
            relationId: int
            dirAndId: LinkedInstancesTypes.RelatedInDirectionForIntInstanceId
    }
    type FormKind = |Creation |Edition |Public |Link of LinkFormInfo
    open WebSharper.UI
    open WebSharper.UI.Html
    open ClientI18n
    open HtmlHelpers
    open Css
    let valueToString (v:Value) =
        match v with
        | ServerValidated sv ->
            match sv with
            | ServerValue.Simple (Some sv) -> sv
            | ServerValue.Simple None -> ""
            | ServerValue.LongText (Some sv) -> sv
            | ServerValue.LongText None -> ""
            | ServerValue.Integer (Some i) -> string i
            | ServerValue.Integer None -> ""
            | ServerValue.Email (Some e) -> e |> Email.toString
            | ServerValue.Email None -> ""
            | ServerValue.WebURL (Some u) -> u |> WebURL.toString
            | ServerValue.WebURL None -> ""
            | ServerValue.DdlValueChoice (Some c) -> c |> DdlChoice.toString
            | ServerValue.DdlValueChoice None -> ""
            | ServerValue.DateAndTime (Some dt) ->
                                            let dateTime = dt |> DateAndTime.get
                                            // javascript translation does not have ToString("format")
                                            (dateTime.get_Year()|> sprintf "%04d")
                                            + "-"
                                            + (dateTime.get_Month()|> sprintf "%02d")
                                            + "-"
                                            + (dateTime.get_Day()|> sprintf "%02d")
                                            + " "
                                            + (dateTime.get_Hour()|> sprintf "%02d")
                                            + ":"
                                            + (dateTime.get_Minute()|> sprintf "%02d")
                                            + ":"
                                            + (dateTime.get_Second()|> sprintf "%02d")
            | ServerValue.DateAndTime None -> ""
            // For file attachment, we serialise the FileAttachment record to transmit all info to the field formatter
            | ServerValue.FileAttachment (Some info) -> Json.Serialize(info)
            | ServerValue.FileAttachment None -> Json.Serialize(ServerFileAttachment.New())
        | ClientProposed cp ->
            match cp with
            | ClientValue.Simple (Some v) -> v
            | ClientValue.Simple None -> ""
            | ClientValue.LongText (Some v) -> v
            | ClientValue.LongText None -> ""
            | ClientValue.Integer (Some i) -> i
            | ClientValue.Integer None -> ""
            | ClientValue.Email (Some e) -> e
            | ClientValue.Email None -> ""
            | ClientValue.WebURL (Some u) -> u
            | ClientValue.WebURL None -> ""
            | ClientValue.DdlValueChoice (Some c) -> c |> string
            | ClientValue.DdlValueChoice None -> ""
            | ClientValue.DateAndTime (Some d) -> d
            | ClientValue.DateAndTime None -> ""
            | ClientValue.FileAttachment None -> Json.Serialize(ServerFileAttachment.New())
            | ClientValue.FileAttachment (Some s) -> s



    let InitialiseNewDetailValue (Ids.DataTypeId dataTypeId) =
        // A new empty value is initialised to a ServerValue so it is not handled by the server
        // if left unmodified by the user
        match dataTypeId with
            | 1 -> ServerValue.Simple None
            | 2 -> ServerValue.LongText None
            | 3 -> ServerValue.DateAndTime None
            | 4 -> ServerValue.Integer None
            | 5 -> ServerValue.DdlValueChoice None
            | 6 -> ServerValue.Email None
            | 7 -> ServerValue.WebURL None
            | 8 -> ServerValue.FileAttachment None
            | _ -> failwith "unknown data type id"
        |> ServerValidated

    // Adds detailValuesSpec to an InstanceSpec for details it has no value for
    let addMissingDetailValues (spec:InstanceSpec) (detailsWithoutValue:list<DetailSpec>)=
            {spec with
                detailValues =
                    // Fold with initial value the spec with existing values, and the
                    // list to traverse being the details without value.
                    // At each step of the fold we add an empty ClientProposed detail value to the list.
                    List.fold
                        (fun dvs (detailSpec:DetailSpec) ->
                            let item =
                                { detailId=(detailSpec.detailId|>Option.get);
                                  values=
                                    [
                                        { detailValueId=None;
                                          value=InitialiseNewDetailValue detailSpec.dataType.dataTypeId
                                        }
                                    ]
                                }
                            item::dvs
                        )
                        spec.detailValues
                        detailsWithoutValue
            }


    // Build the lens Map where the key is the detailId, and the value is the lens to the ValueSpec for this detail id.
    // The specs supports multiple values for one detail id but this is not used yet, so the lens focuses on the first
    // ValueSpec in the list.
    let buildLensMap (fullSpecVar:Var<InstanceSpec>) =
        // Create a map of lenses into the detail values of the instance
        let lensMap = ref Map.empty
        fullSpecVar.Value.detailValues
        // We iterate on the detail values with the index.
        // We cannot use the detail value item passed to our function by iteri to define lenses because
        // the functions defining the lens get as argument the spec, and it is that spec that
        // we must use in the lens definition. That is the reason why we use iteri, so that in the lens definition
        // we can get to the detail value of index i.
        |> List.iteri (fun i dv ->
            // define the lens for detail value i
            let lens =
                fullSpecVar.Lens
                    // Get the string representation of a Value
                    (fun s->
                      s.detailValues[i].values[0])
                    // update function for detail value #i
                    (fun s v ->
                        {
                            s with
                                // only update the detailValues field
                                detailValues =
                                    s.detailValues
                                    |> List.mapi
                                        (fun index dvToMap ->
                                            // only touch detail value with index i (set by iteri)
                                            if index = i then
                                                { dvToMap with
                                                    values = [v]
                                                }
                                            else
                                                dvToMap
                                          )
                        }
                    )
            // add the lens we just created to the map, under the key of the detail value's detailId
            lensMap.Value <- (Map.add dv.detailId lens lensMap.Value)
        )
        lensMap

    // Template used to display updates applied by a form submission
    type UpdatesTemplate    = Templating.Template<"templates/form/UpdatesFeedback.html">
    type UpdatesTemplateInRow    = Templating.Template<"templates/form/UpdatesFeedbackInRow.html">

    let updateFeedbackForDetail (formKind: FormKind) (dspec:DetailSpec) (spec:InstanceSpec) =
        spec.detailValues
        // Extract detailValue for detail dspec
        |> List.filter (fun dv ->
                dspec.detailId
                |> Option.map (fun detailSpecDetailId ->
                    detailSpecDetailId = dv.detailId
                )
                |> Option.defaultValue false
            )
        // Get the detail value's string representation
        |> List.map (fun dv -> valueToString dv.values.Head.value)
        // Extract the file attachment's name from the json string
        |> List.map (fun s ->
            if dspec.dataType.className = "FileAttachmentDetailValue" then
                let info:ServerFileAttachment.FileAttachmentWithOptionalIndb = Json.Deserialize s
                info.filename
            else
                s
        )
        // Filter out empty value for creation as those are not saved. However, keep them
        // for edition as thos have to be displayed as new values.
        |> List.filter (fun s ->
          match formKind with
          |Creation|Public|Link _ -> s.Length>0
          |Edition -> true
        )
        // Map the string to a Doc. We introduce the span so that we have the same hierarchy for
        // fields changed and those unchanged (facilitates testing too)
        |> List.map (fun s -> span [] [text s])
        // If we end up with an empty list, replace it by a list of one Doc indicating no change was done
        // on this field
        |> (fun l ->
          if List.length l > 0 then
              l
          else
              let str =
                  match formKind with
                  |Creation|Public|Link _ -> t.tt("Left Empty")
                  |Edition -> t.tt("No change")
              [ span [attr.``class`` "opacity-25" ] [str] ]
        )
        // As we have a list of Docs, concatenate them to get one Doc
        |> Doc.Concat


    // Generates the list of Docs that are the rows of the "grid" displaying the updates applied by a form submission
    let specInGridFields (formKind:FormKind)(detailsSpecs:DetailSpec list)(spec:InstanceSpec) =
        detailsSpecs
        |> List.map (fun dspec ->
            UpdatesTemplate.Field()
                .Key(dspec.name)
                .Value(
                    updateFeedbackForDetail formKind dspec spec
                )
                .DetailName(dspec.name)
                .Doc()
        )

    // Generates the list of Docs is the table displaying the update summary:
    // - first row is the column headers with the details names
    // - second row is the values
    let specInRow (formKind:FormKind)(detailsSpecs:DetailSpec list)(spec:InstanceSpec) =
        let keys =
            detailsSpecs
            |> List.map (fun dspec ->
                    UpdatesTemplateInRow.KeyCell()
                        .Key(dspec.name)
                        .Doc()
            )
        let values =
            detailsSpecs
            |> List.map (fun dspec ->
                    UpdatesTemplateInRow.ValueCell()
                        .Value(
                            updateFeedbackForDetail formKind dspec spec
                        )
                        .DetailName(dspec.name)
                        .Doc()
            )
        [UpdatesTemplateInRow.FeedbackTable()
            .FieldsRow(keys)
            .ValuesRow(values)
            .Doc()]

    // Displays the infobox of a successful form submission
    let displaySavedMessage(displayer)(formKind:FormKind)(detailsSpecs:DetailSpec list)(spec:InstanceSpec) =
        let title =
            formKind
            |> function
                |Creation|Public|Link _ -> t.t("New entry overview")
                |Edition -> t.t("Updates overview")
        infobox (InfoBoxKind.SuccessBox)
                (div
                    []
                    [div [classes [BS.``pb-3``; BS.``fw-bold``]] [t.tt "Saved successfully"]
                     UpdatesTemplate()
                      .CardTitle(title)
                      .Fields(displayer formKind detailsSpecs spec |> Doc.Concat)
                      .Doc()
                    ]
                )
    let displaySavedMessageInGrid (formKind:FormKind)(detailsSpecs:DetailSpec list)(spec:InstanceSpec) =
        displaySavedMessage specInGridFields formKind detailsSpecs spec

    let displaySavedMessageCompact (formKind:FormKind)(detailsSpecs:DetailSpec list)(spec:InstanceSpec) =
        displaySavedMessage specInRow formKind detailsSpecs spec