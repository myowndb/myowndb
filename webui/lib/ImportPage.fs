namespace web

open WebSharper
open WebSharper.UI
open WebSharper.UI.Html
// needed for *DynPred attributes
open WebSharper.UI.Client
open HtmlHelpers
open Css

open ClientI18n

[<JavaScript>]
module ImportPage =


  type ImportStep =
    |Init
    |Uploaded of Ids.ImportId * Specs.DetailSpec list * string array
    |Imported of DBResult.DBResult<list<option<Ids.InstanceId>>>*Ids.ImportId
    |Undone of int
  let t=JStr()

  let step = Var.Create Init

  // Step 1: upload form
  // *******************
  let uploadForm (entityIdInt:int) =
    let fileVar:Var<JavaScript.File array> = Var.Create [||]
    let textVar:Var<string> = Var.Create ""
    let resultMessage = Var.Create ""

    div [] [
      Client.Doc.InputType.Text [] textVar
      Client.Doc.InputType.File [classes ["myowndbfileselection"]] fileVar
      button
        [ on.click (fun el ev ->
          let f = fileVar.Get()[0]
          async {
            let! buffer = f.ArrayBuffer() |>WebSharper.JavaScript.Promise.AsAsync
            // Get a Uint8Array from ArrayBuffer
            let q = WebSharper.JavaScript.Uint8Array(buffer)
            // Get the bytes for each Uint8Array element
            // This gives us a byte array we can send to the server side
            let d = [| for i in 1..q.Length do q.Get(i-1) |]
            try
              match! Server.handleImportUpload entityIdInt (f.Name) d with
              | Ok [importId,detailsSpecs,columns] ->
                step.Set (Uploaded (importId,detailsSpecs,columns))
              |Ok _
              |DBResult.DBResult.Error _ ->
                resultMessage.Set (t.t("An error occurred"))
            with
            | _e -> resultMessage.Set  (t.t("An error occurred in the upload"))

            return ()
            } |> Async.StartImmediate
          )
          classes [BS.btn; BS.``btn-primary``;"myowndbuploadfile"]
        ]
        [t.tt("Submit")]
      div [] [text resultMessage.V]
      ]

  // Step 2: map csv columns to details
  // **********************************
  let mappingsForm (importId:Ids.ImportId)(columns:string array)(detailsSpecs:Specs.DetailSpec list) =
    let resultMessage = Var.Create ""
    // build form element for each csv column
    let choices =
      detailsSpecs
      // We cannot import a file attachment from a csv
      |> List.filter (fun spec -> spec.dataType.className <> "FileAttachmentDetailValue")
      |> List.map (fun spec ->
        // If a column is found with the same name as the detail, use it as pre-filled mapping
        let defaultMapping =
          columns
          |> Array.tryFind (fun col -> col = spec.name)
        // The Var holding the user's choice of mapping for column col
        let choice:Var<Option<string>> =
          Var.Create defaultMapping
        // The <select> displaying the columns found in the CSV
        let ddl = Doc.InputType.SelectOptional [attr.id spec.name; attr.``data-`` "width" "auto"; classes [BS.``form-control``;BS.``form-select-sm``]] "---" id (columns|>List.ofArray) choice
        (spec,choice,ddl)
      )
    // Function to get current mappings. As we have a list of Vars holding the choices, we call
    // this function to retrieve mappings right before sending the request to the server.
    let getMappings ()=
      choices
      // List.choose only keeps the Some values, and extracts it from the Option.
      |> List.choose (fun (spec,choice,_ddl) ->
        let dest = choice.Get()
        match dest with
        | Some col -> Some (col,spec)
        | _ -> None
      )
      |> List.map (fun (col,detailSpec) ->
        // Option.get is ok here as we work on details retrieved from the database
        let res:ImportMapping.Mapping = {csvColumn=col; detailId = detailSpec.detailId|>Option.get}
        res
      )

    // Each item of the list is a line displayed to the user.
    // Each line corresponds one detail of the entity
    // The drop down lists the columns of the uploaded CSV, so that the user
    // can choose which column to import in the line's corresponding detail
    let mappingsFields =
      choices
      |> List.fold (fun acc (spec,_,ddl) ->
        List.append
          acc
          [
            div
              [ classes [BS.container] ]
              [
                div
                  [classes [BS.row;"myowndbcsvmappingrow"]]
                  [
                    div [classes [BS.``col-md-auto``; "myowndbcsvfieldintro"]] [t.tt("CSV field")]
                    div [classes [BS.``col-md-auto``; "myowndbcsvfieldchoice"]] [ddl]
                    div [classes [BS.``col-md-auto``; "myowndbdestinationdetail"]] [t.tt("goes into %1", spec.name)]
                    hr [] []
                  ]
              ]
        ]
        )
        []
      // Make one Doc of the list of Docs.
      |> Doc.Concat
    // Arrange the choices in a form
    div
      []
      [
        mappingsFields
        // Submit button
        button
          [
            classes [BS.btn; BS.``btn-primary``; "myowndbdoimport"]
            on.click (fun _el _ev ->
              let _r =
                // Send request to server and report result to user
                async {
                  let! res = Server.importFileWithMappings importId (getMappings())
                  step.Set(Imported (res,importId))
                } |> Async.Start
              ()
            )
          ]
          // -- TRANSLATOR: button text to import, this is the verb
          [t.tt("Import")]
        div [] [text resultMessage.V]
      ]

  // Step 3: result of import
  // ************************
  let resultPage (importId:Ids.ImportId)(entityId:int)(res:DBResult.DBResult<list<option<Ids.InstanceId>>>) =
    let feedback = Var.Create Doc.Empty
    match res with
    | Ok [idsList] ->
      feedback.Set
        (
          [
            successBox (text (t.n("Imported one instance","Imported %1 instances",List.length idsList,List.length idsList)))
            t.tt("You can undo the import. Undoing the import will permanently delete all instances created by the import.")
            DeleteConfirmation.customDeleteConfirmation
              // Function returning element triggering the delete. attrs is the on click
              (fun onclick ->
                button [onclick; classes [BS.btn;BS.``btn-danger``;"myowndbundoimport"]] [t.tt("Undo")]
              )
              (fun () -> async {
                let! countResult =Server.undoImport importId
                match countResult with
                | Ok [c] ->
                  step.Set(Undone c)
                  return true
                | _ ->
                  return false

              }
              )
              (t.t("Undo import?"))
            |>Doc.EmbedView
          ]
          |> Doc.Concat
        )
    | DBResult.DBResult.Error es ->
      let lis =
        es
        |>Error.toStringList
        |>List.truncate 6
        |>List.map (fun s -> li [] [text s])
        |> Doc.Concat
      let errorHeader = t.tt("Errors were detected in values to be imported. You can find some info regarding the first errors below.")
      feedback.Set
        (dangerBox
          (div
            []
            [
              errorHeader
              br [] []
              ul [] [lis]
            ]))
    | Ok _ -> feedback.Set(dangerBox(t.tt("An error occured")))
    div
      []
      [
        feedback.V
        br [] []
        a [attr.href (Routing.router.Link (Instances (entityId,None) ) )] [t.tt("Go back to the list")]
      ]

  let undonePage (entityIdInt:int)(count:int) =
    [
      successBox (t.tt("Reverted import, resulting in the deletion of %1 instances",count))
      a [attr.href (Routing.router.Link (Instances (entityIdInt,None) ) )] [t.tt("Go back to the list")]
    ]
    |> Doc.Concat


  // Entrypoint to display successive steps of import
  // ************************************************
  let displayForm (entityIdInt) = async {
    return
      step.View
      |> View.Map (function
        | Init -> uploadForm(entityIdInt)
        | Uploaded (importId,detailsSpecs,columns) ->
            div
              []
              [mappingsForm importId columns detailsSpecs]
        | Imported (idsList,importId) ->
            resultPage importId entityIdInt idsList

        | Undone count ->
            undonePage entityIdInt count
      )
  }



  let display (entityIdInt:int) (pageInfo:PageInfo.Import.PageInfo)= async {
    // Restart from scratch when we first display the page. Not doing this
    // displays the previous import's last page.
    step.Set Init

    let! form = displayForm(entityIdInt)
    return
      div
        []
        [
          h1 [] [t.tt("Import %1",pageInfo.entity.name)]
          form.V
        ]
  }
