namespace web

open WebSharper
open System.Text.RegularExpressions

module PasswordValidators=
    // these functions take the same argument as the method in interface IPasswordValidator
    // this enables eg to validate the password is different than the login
    let hasDigit  _um _u (s:string)= Regex.IsMatch(s,@"[0-9]")
    let hasUpper  _um _u (s:string)= Regex.IsMatch(s,@"\p{Lu}")
    let hasLower  _um _u (s:string)= Regex.IsMatch(s,@"\p{Ll}")
    let hasNonAlphaNum _um _u (s:string) = Regex.IsMatch(s,@"[^\p{L}\p{N}]+")
    let checkLength (length:int) _um _u (s:string) =  String.length(s)>= length
    let hasLength um u p = checkLength 8 um u p

    [<JavaScript>]
    type SetNewPasswordStrings = {
        NewPassword: string
        PasswordConfirmation: string
        Send : string
        PasswordValidationMsg: string
        ConfirmationValidationMsg: string
        PasswordUpdatedMsg: string
        CriteriaHeader: string
        LengthMsg: string
        NonAlphaMsg: string
        UpperMsg: string
        LowerMsg: string
        DigitMsg: string
    }

module AspNetPasswordValidators =
    open Microsoft.AspNetCore.Identity
    open PasswordValidators
    open Microsoft.Extensions.Configuration

    // Define base class from which all validators will inherit.
    // The validators will pass their distinguishing characteristics to the base class constructor:
    // - the checked function returning a bool indicating if password is accepted or not
    // - the code and description of the error returned when the password is not accepted
    type ValidatorGenerator<'TUser when 'TUser : not struct>(code:string, description: string, checker: UserManager<'TUser> -> 'TUser -> string -> bool) =
        // map the checker return value to an IdentityResult
        let f um u s= async {
                                if checker um u s then
                                    return IdentityResult.Success
                                else
                                    return IdentityResult.Failed( IdentityError(Code=code,Description=description))
                          } |> Async.StartAsTask
        // implement the Identity Core interface for password validators, simply calling f
        interface IPasswordValidator<'TUser> with
            member _.ValidateAsync(um: UserManager<'TUser>,user:'TUser,password:string) = f um user password


    // Define our password validation types.
    // We use ASP.Net Core dependency injection to acces the I18n instance so we can translate the description message
    //!!!!!!! IMPORTANT: KEEP THE CODE FORMATTING!!!!!!!!
    // The call to t.t is on a separate line so that the xgettext lua parser we use detects it.
    // Putting all arguments on one line makes that xgettext misses the call to t.t
    type HasUpper<'TUser when 'TUser : not struct>(t:I18n) =
        inherit ValidatorGenerator<'TUser>(code="NoUpper",
                                           description= t.t("Password must have at least one uppercase letter"),
                                           checker= hasUpper)
    type HasLower<'TUser when 'TUser : not struct>(t:I18n) =
        inherit ValidatorGenerator<'TUser>(code="NoLower",
                                           description= t.t("Password must have at least one lowercase letter"),
                                           checker= hasLower)
    type HasDigit<'TUser when 'TUser : not struct>(t:I18n) =
        inherit ValidatorGenerator<'TUser>(code="NoDigit",
                                           description= t.t("Password must have at least one digit"),
                                           checker= hasDigit)
    type HasNonAlphaNum<'TUser when 'TUser : not struct>(t:I18n) =
        inherit ValidatorGenerator<'TUser>(code="NoNonAlphaNum",
                                           description= t.t("Password must have at least one special character"),
                                           checker= hasNonAlphaNum)
    type HasLength<'TUser when 'TUser : not struct>(t:I18n, config:IConfiguration) =
        // not sure the duplicate call to config.GetValue can be avoided as the inherit call must be first
        inherit ValidatorGenerator<'TUser>(code="TooShort",
                                           description= t.t("Password must have at least {0} characters", config.GetValue<int>("myowndb:auth:minimumPasswordLength") ),
                                           checker= (checkLength (config.GetValue("myowndb:auth:minimumPasswordLength"))))
