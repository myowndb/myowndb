namespace web

open WebSharper.UI.Html
open WebSharper
open NGettext
open WebSharper.UI
open WebSharper.UI.Client
open ClientI18n
open HtmlHelpers
open WSExtensions
open Css

[<JavaScript>]
module InstanceForm =
    open Specs
    open FormHelpers
    let t=JStr()

    open FormHelpers

    module private Helpers =
        let buildInstanceForm
            (formKind:FormKind)
            // callback that returns a doc displayed under the form to signal successful save
            (successDocCallback:FormKind ->list<DetailSpec> -> InstanceSpec -> Doc)
            (entityId:Ids.EntityId)
            (detailsSpecs:DetailSpec list)
            (lensMap:ref<Map<Ids.DetailId,Var<ValueSpec>>>)
            (specVar:Var<InstanceSpec>)
            (additionalButtons:Doc)
            =
            // Define form and collect field information in the list of records `infos`
            // Collecting all records in the list `infos` makes it more flexible: we can add a field to the record and use it by iterating
            // over the records.
            let form, infos =
                // work with detailsSpecs because the instance spec does not include enough information
                // regarding the details (eg name, value propositions)
                detailsSpecs
                |> List.map (fun detailSpec ->
                    let fieldVarOption = (Map.tryFind (detailSpec.detailId|>Option.get) (lensMap.Value))
                    let fieldVar =
                        match fieldVarOption with
                        | Some v ->
                            v
                        | None ->
                            // This should never occur as we added empty detail values for details which had none
                            failwith "A detail without value was encountered at a point where this shoud not be possible"
                    let evaluator = DetailvalueFormater.FormFieldMarkupEvaluator detailSpec
                    // explicitly type annotate with the interface so .Apply can be called on it
                    let crate:Entity.IMarkupCrate = DetailvalueFormater.FormFieldCrate(fieldVar)
                    crate.Apply evaluator
                )
                // for each field we get a tuple (doc,fieldIsValidView,fieldIsChangedView,prepareSubmit). Here we split these in lists
                |> (fun l ->
                    List.fold
                        (fun (docs,infos)(doc,info:DetailvalueFormater.FormFieldInfo) ->
                            List.append docs [doc] ,
                            List.append infos [info]
                                )
                        ([],[])
                        l
                    )

            // Define aggregated view from info received from the form fields
            // (sequence transforms a list of views in a view of a list)
            let validityViews = infos |>List.map(fun r -> r.isValid)
            let validityView = View.sequence validityViews
            let awaitingValidationViews = infos |> List.map (fun r -> r.waitingValidation)
            let awaitingValidationView = View.sequence awaitingValidationViews

            // Define helper functions to run callbacks we got from form fields
            // We need to wait for all preparation functions to be done
            let runPrepareFunctions() =  infos
                                        |> List.map (fun r-> r.prepareSubmit())
                                        // Transform list of view to view of list
                                        |> View.Sequence
                                        // Map the sequence of bools to one bool value indicating if
                                        // all preparetion calls are done
                                        |> View.Map (fun s -> s|> Seq.forall id)
            // Beware, this is not a call to the method Value(), but retrieving the function in the ref
            // and then calling it!
            let runCancelCallbacks() = infos |> List.iter (fun r -> r.cancelSubmit.Value())
            let runSuccessCallbacks() = infos |> List.iter (fun r -> r.afterSubmissionCallback(true,formKind))
            let runFailureCallbacks() = infos |> List.iter (fun r -> r.afterSubmissionCallback(false,formKind))
            let resetForm() = infos |> List.iter (fun info -> info.reset())


            // The form is invalid if any field is invalid
            let formIsInvalid =
                validityView
                |> View.Map (List.contains false)

            // The a field was edited but not yet validated
            let formHasFieldAwaitingValidation =
                awaitingValidationView
                |> View.Map (List.contains true)

            // An invalid button has the additional class disabled
            let buttonClasses =
                View.Map2
                    (fun isInvalid awaitsValidation ->
                    if isInvalid || awaitsValidation then
                        // Disable submission if invalid or waiting for validation to occur
                        // (Server validation delayed to not do it at every key press)
                        String.concat " " [BS.btn; BS.``btn-primary``; BS.disabled]
                    else
                        String.concat " " [BS.btn; BS.``btn-primary``]

                    )
                    formIsInvalid
                    formHasFieldAwaitingValidation
            let isDisabled =
                View.Map2
                    (fun isInvalid awaitsValidation -> isInvalid || awaitsValidation )
                    formIsInvalid
                    formHasFieldAwaitingValidation

            // User feedback regarding form submission
            let resultVar = Var.Create Doc.Empty
            // Var indicating if preparation (attachment uploads) is currently occuring. Used to enable canceling uploads
            let isPreparing = Var.Create false
            let t = JStr()
            div [ attr.``data-`` "form-entity-id" (Ids.EntityId.Get entityId |> string) ]
                [
                    div [] form; br [] [];
                    Doc.Button (t.t("Save"))
                            [attr.``class`` buttonClasses.V ; attr.disabledBool isDisabled]
                            (fun () ->
                                async {
                                    // We call the prepareSubmit functions of all fields. These each return a View<bool> indicating if
                                    // form submission can proceed. We combine these by a logical AND to get a global View<bool> indicating
                                    // if all fields are ready for form submission to proceed.
                                    isPreparing.Set true
                                    let prepareSubmitDone = runPrepareFunctions()
                                    // We wait until prepareSubmitDone has value true
                                    let! _ = prepareSubmitDone |> View.AsyncAwait id
                                    isPreparing.Set false
                                    // prepareSubmit is done for all fields (file attachments have been uploaded) and we can now submit
                                    // the instance spec to the server.
                                    try
                                        // Our client side only FormKind has no parameter, but is not known by the servre. The server side FormKind
                                        // required an argument, which we pass here
                                        let entityIdInt = (entityId|>Ids.EntityId.Get)
                                        let serverFormKind =
                                            match formKind with
                                            | Creation -> Server.FormKind.CreationForm entityIdInt
                                            |Edition ->
                                                let instanceIdFromSpec = specVar.Value.instanceId |> Option.get |> Ids.InstanceId.Get
                                                Server.FormKind.EditionForm instanceIdFromSpec
                                            | Public ->Server.FormKind.PublicForm entityIdInt
                                            | Link linkFormInfo -> Server.FormKind.LinkForm {entityId=entityIdInt; relationId = linkFormInfo.relationId; dirAndId=linkFormInfo.dirAndId}
                                        let! res = Server.createOrUdpdateInstance serverFormKind (specVar.Get())
                                        match res with
                                        | Ok [spec] ->
                                            spec.detailValues
                                            // Update the values in the instance spec to enable re-edition of fields
                                            |> List.iter (fun dv ->
                                                (lensMap.Value.[dv.detailId]).Set(dv.values[0])
                                            )
                                            runSuccessCallbacks()
                                            resultVar.Set  (successDocCallback formKind detailsSpecs spec)
                                            // For public form, redirect to a success message replacing the form
                                            if formKind = Public then
                                                ClientRouting.installedRouter.Value <- PublicFormSubmitted entityIdInt
                                        | Ok l ->
                                            runFailureCallbacks()
                                            JavaScript.Console.Log(sprintf "%A" l)
                                            resultVar.Set (infobox (InfoBoxKind.Danger)(div [] [t.tt "Saving resulted in an inconsistent state"]))
                                        | Error e ->
                                            runFailureCallbacks()
                                            resultVar.Set (infobox (InfoBoxKind.Danger)(div [] [text (e|> Error.toString)]))
                                    with
                                    | e ->
                                        resultVar.Set (infobox (InfoBoxKind.Danger)(div [] [t.tt "Error communicating with server."; text (e.Message)]))
                                        runFailureCallbacks()
                                        raise e
                                    // Reset form if needed
                                    match formKind with
                                    // Creation forms are reset so new entries can be created tapidly
                                    | Creation | Public | Link _-> resetForm()
                                    // The edition form is not reset so the instance can immediately be re-edited if needed.
                                    | Edition -> ()
                                } |> Async.Start
                    );
                    Doc.Button (t.t("Cancel"))
                        [attr.classDynPredBoth ("d-inline btn btn-secondary")("d-none btn btn-secondary") isPreparing.View]
                        (fun () ->
                            // uploads are canceled so the form is not preparing submission anymore, and this buttin can be hidden
                            isPreparing.Set false
                            runCancelCallbacks()
                        )
                    additionalButtons
                    span [] [resultVar.V]
                ]
    open Helpers

    let instanceFormForSpecFromServer
        (formKind:FormKind)
        // callback that returns a doc displayed under the form to signal successful save
        (successDocCallback:FormKind ->list<DetailSpec> -> InstanceSpec -> Doc)
        (instanceSpecOption:option<InstanceSpec * list<DetailSpec>>) =
        match instanceSpecOption with
        | Some (spec,detailsSpecs) ->
            let detailIdsWithValue =
                spec.detailValues
                |> List.map ( fun dv -> dv.detailId
                )
            // buidl list of details which have no value
            let detailsWithoutValue =
                detailsSpecs
                |> List.filter (fun (item:DetailSpec) -> not (List.contains (item.detailId|>Option.get) detailIdsWithValue ) )
            // update spec with detailValues for details without values
            // This is necessary so that we can have lenses to details which currently have no value
            let fullSpec = addMissingDetailValues spec detailsWithoutValue

            // Create a Var wrapping the fullSpec so we can define a lens for each detail value.
            let specVar = Var.Create fullSpec
            // To DEBUG, uncomment this and follow the updates to the spec to be sent to the server
            // specVar.View |> View.Sink (fun spec -> printfn "specvar content: %A" spec)

            // Build a map of lenses zooming in the full instance spec
            let lensMap = buildLensMap specVar
            let additionalButton =
              match spec.instanceId with
              | Some instanceId ->
                  a
                    [ classes [BS.btn;BS.``btn-primary``]
                      attr.href (Routing.router.Link (EndPoint.Instance (instanceId|>Ids.InstanceId.Get, None) ))
                    ]
                    [
                      t.tt("View")
                    ]
              | _ ->
                    match formKind with
                    | Public ->
                        a
                            [ attr.target "_blank"
                              attr.href "https://www.myowndb.com"
                              classes [ BS.``fw-lighter``; BS.``p-3`` ] ]
                            [ text "Powered by MyOwndb" ]
                    | _ -> Doc.Empty

            buildInstanceForm formKind successDocCallback (spec.entityId) detailsSpecs lensMap specVar additionalButton
        | None ->
            div [] [text "problem getting spec"]

    // Display the instance creation form
    let displayCreate id (successDocCallback:FormKind ->list<DetailSpec> -> InstanceSpec -> Doc)= async {
    // Display form for edition of an existing instance
        let! instanceSpecOption = Server.getInstanceAndDetailsSpec (Server.CreationForm id)
        return instanceFormForSpecFromServer Creation successDocCallback instanceSpecOption
    }

    // Display the instance edition form
    let displayEdit id (successDocCallback:FormKind ->list<DetailSpec> -> InstanceSpec -> Doc)= async {
        let! instanceSpecOption = Server.getInstanceAndDetailsSpec (Server.EditionForm id)
        return instanceFormForSpecFromServer Edition successDocCallback instanceSpecOption

    }

    // Display the public form
    let displayPublic id (successDocCallback:FormKind ->list<DetailSpec> -> InstanceSpec -> Doc)= async {
        let! instanceSpecOption = Server.getInstanceAndDetailsSpec (Server.PublicForm id)
        return instanceFormForSpecFromServer Public successDocCallback instanceSpecOption
    }

    // Display the linked instance creation form
    let displayLink id formKind (successDocCallback:FormKind ->list<DetailSpec> -> InstanceSpec -> Doc)= async {
    // Display form for edition of an existing instance
        let! instanceSpecOption = Server.getInstanceAndDetailsSpec (Server.CreationForm id)
        return instanceFormForSpecFromServer formKind successDocCallback instanceSpecOption
    }
