namespace web
open System;
open System.Collections.Generic;
open System.Data.SqlClient;
open System.Linq;
open System.Threading;
open System.Threading.Tasks;
open Microsoft.AspNetCore.Identity;
open Microsoft.Extensions.Configuration;
open System.Security.Claims
open User
open DBAction
open FSharpPlus
open Microsoft.Extensions.Logging

// username= email
// normalized username = login
// When the username (i.e. email) is updated, the normalized username (i.e. login) is also updated
// Not sure when SetNormalizedUsername is called/useful, so it only updates the login field.

// Based on example at https://markjohnson.io/articles/asp-net-core-identity-without-entity-framework/
type UserStore(client:DB.DBClient, logger: ILogger<UserStore>)=
    // keep track if the DBClient connection was opened by the caller.
    // If not, we open and cloe the connection as we need, otherwise, we
    // simply use it and don't close it. We might act in a transaction started
    // by the caller
    let validateSingleUpdate(result:DBResult.DBResult<DBTypes.ImpactedRows>) =
        match result with
        | Ok [DBTypes.ImpactedRows 1] -> ()
        | Ok [DBTypes.ImpactedRows 0] -> failwith "user not found for single update"
        | Error es -> failwith (es |> Error.toString)
        | Ok _ -> failwith "multiple users updated?"
    let DBErrorsToIdentityErrors (es) = (Error.toStringList es)
                                                        |> List.map (fun e -> let ie = IdentityError()
                                                                              ie.Description <- e
                                                                              ie )
                                                        |> List.toArray


    // I would have liked to populate the list here, but I need the UserManager instance to
    // set the password, and I didn't find a way to achieve that.
    interface IUserStore<ApplicationUser> with
        member this.GetUserIdAsync(user:ApplicationUser, cancellationToken:CancellationToken) =
            // The user store works with string keys. The easy way to use int keys was even removed:
            // https://github.com/aspnet/Identity/issues/1482
            // I don't seem to be alone in casting to string here: https://dev.to/masanori_msl/net-5-asp-net-core-identity-signin-with-custom-user-56fe
            // Altough it seems possible to work with int key with some efforts: https://docs.microsoft.com/en-us/aspnet/identity/overview/extensibility/change-primary-key-for-users-in-aspnet-identity
            // But I currently prefer to progress on the app and will try this casting
            Task.FromResult(string user.Id)
        member this.GetUserNameAsync(appUser:ApplicationUser, cancellationToken:CancellationToken) =
            Task.FromResult(appUser.UserName)
        member this.SetUserNameAsync(appUser:ApplicationUser, userName:string, cancellationToken:CancellationToken) =
            Task.FromResult(appUser.UserName <- userName; appUser.NormalizedUserName <- (userName.ToUpper())) :> Task
        member this.GetNormalizedUserNameAsync(appUser: ApplicationUser, cancellationToken: CancellationToken) : Task<string> =
            Task.FromResult(appUser.NormalizedUserName)
        member this.SetNormalizedUserNameAsync(appUser: ApplicationUser, normalizedName: string, cancellationToken: CancellationToken) : Task =
            Task.FromResult(appUser.NormalizedUserName <- normalizedName) :> Task
        member this.CreateAsync(appUser: ApplicationUser, cancellationToken: CancellationToken) : Task<IdentityResult> =
            async {
                let! closeIfNeeded = client.OpenIfNeeded()
                if appUser.Id <> 0 then
                    failwithf "User store trying to create a new user from an appUser with ID %d: %A" appUser.Id appUser
                let uuid = System.Guid.NewGuid().ToString()
                // we set the password field to the same value as aspnet_pass_hash because leaving it null causes trouble
                // when loading the user, as the password field is not an Option
                let! result = DBAction.singleInsertAction
                                  "insert into users(account_id,user_type_id,login,email,firstname,lastname,uuid,password,salt,aspnet_pass_hash)
                                    VALUES (%d,%d,%s,%s,%s,%s,%s,%s,'UNUSED_DUMMY_SALT',%s) RETURNING id"
                                  appUser.accountId appUser.userTypeId appUser.NormalizedEmail appUser.Email appUser.firstname appUser.lastname uuid appUser.PasswordHash appUser.PasswordHash
                              |> DBAction.ensureSingleRow
                              |> DBAction.run<Ids.UserId> client
                              |> Async.map DBResult.toSingleResult
                do! closeIfNeeded()
                match result with
                | Ok userId ->
                    // update the id of the ApplicationUser instance, as it has id 0 to indicate a new user not yet inserted in the
                    // database (my convertion, not Identity Core's)
                    // this is absolutely required, otherwise the token for email verification will include user id 0 and
                    // verification will fail with the error Invalid Token.
                    let (Ids.UserId id) = userId
                    appUser.Id <- id
                    return IdentityResult.Success
                | Error es ->   let identityErrors = DBErrorsToIdentityErrors es
                                return (IdentityResult.Failed(identityErrors))
            }
            |> Async.StartAsTask
            // create user in store
        member this.UpdateAsync(appUser: ApplicationUser, cancellationToken: CancellationToken) : Task<IdentityResult> =
            // update user in store
            async {
                let! closeIfNeeded = client.OpenIfNeeded()
                let! result = DBAction.statementAction "update users set user_type_id=%d, login=%s,email=%s,firstname=%s,lastname=%s,aspnet_pass_hash=%s, verified=%d where id=%d"
                                                                 appUser.userTypeId
                                                                 appUser.NormalizedEmail
                                                                 appUser.Email
                                                                 appUser.firstname
                                                                 appUser.lastname
                                                                 appUser.PasswordHash
                                                                 (if appUser.EmailConfirmed then 1 else 0)
                                                                 (int appUser.Id)
                              |> DBAction.run client
                validateSingleUpdate(result)
                do! closeIfNeeded ()
                return IdentityResult.Success
            } |> Async.StartAsTask
        member this.DeleteAsync(appUser: ApplicationUser, cancellationToken: CancellationToken) : Task<IdentityResult> =
            async {
                let! closeIfNeeded = client.OpenIfNeeded()
                let! result = DBAction.immediate client "delete from users where id = %d" (int appUser.Id)
                do! closeIfNeeded ()
                match result with
                | Ok u -> return IdentityResult.Success
                | Error es -> let identityErrors = DBErrorsToIdentityErrors es
                              return (IdentityResult.Failed(identityErrors))
            } |> Async.StartAsTask
        member this.FindByIdAsync(userId: string, cancellationToken: CancellationToken) : Task<ApplicationUser> =
            async {
                let! closeIfNeeded = client.OpenIfNeeded()
                logger.LogDebug("UserStore looking for user with id {0}", userId)
                let! result = DBAction.queryAction "select * from users where id = %d" (int userId)
                            |> DBAction.ensureSingleRow
                            |> DBAction.run<User> client
                            |> Async.map DBResult.toSingleResult
                do! closeIfNeeded ()
                match result with
                | Ok u -> let appUser = ApplicationUser.Create(u)
                          return appUser
                | Error es -> return null
            } |> Async.StartAsTask
        member this.FindByNameAsync(normalizedUserName: string, cancellationToken: CancellationToken) : Task<ApplicationUser> =
            async {
                let! closeIfNeeded = client.OpenIfNeeded()
                logger.LogDebug("UserStore looking for user with email {0}", normalizedUserName)
                let! result = DBAction.queryAction "select * from users where UPPER(email) = %s" normalizedUserName
                            |> DBAction.ensureSingleRow
                            |> DBAction.run<User> client
                            |> Async.map DBResult.toSingleResult
                do! closeIfNeeded ()
                match result with
                | Ok u -> let appUser = ApplicationUser.Create(u)
                          return appUser
                | Error es -> return null
            } |> Async.StartAsTask
        member this.Dispose(): unit =
            ()
    interface IUserPasswordStore<ApplicationUser> with
        member this.SetPasswordHashAsync(appUser: ApplicationUser, passwordHash: string , cancellationToken: CancellationToken):  Task =
            Task.FromResult(appUser.PasswordHash <- passwordHash) :> Task
        member this.GetPasswordHashAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<string> =
            Task.FromResult(user.PasswordHash)
            // FIXME: is the password already present in the user object?
            //Task.FromResult(user.aspnet_pass_hash|>Option.get)
        member this.HasPasswordAsync(user: ApplicationUser, cancellationToken: CancellationToken) : Task<bool> =
            Task.FromResult(not (isNull user.PasswordHash))

    // Claims
    // The Claims we set are:
    // * UserType : normal | primary
    // * AccountId
    // * Database : ids of all databases of the user's account.
    // The user id is available under ClaimTypes.NameIdentifier from the ClaimPrincipal
    // As such, we currently do not explicitly store claims in the database.
    // I think this makes AddClaim, RemoveClaim, ReplaceClaim not relevant for our usecase
    // Get Users for Claim is also not interesting. Eg we won't want to get all normal users across accounts.
    interface IUserClaimStore<ApplicationUser> with
        member this.AddClaimsAsync(appUser:ApplicationUser, claims: IEnumerable<Claim>, cancellationToken:CancellationToken) =
            failwith "AddClaimAsync currently not implemented"
            appUser.Claims <- List.append appUser.Claims (claims |> List.ofSeq)
            Task.CompletedTask
        member this.GetClaimsAsync(appUser: ApplicationUser , cancellationToken: CancellationToken ) : Task<IList<Claim>>=
            async {
                let! closeIfNeeded = client.OpenIfNeeded()
                // Collect claims
                // We only add a minimal set of claims, and claims are added when the access to a resource is requested the first time
                let usertypeClaim = if appUser.isPrimary then
                                            Claim("UserType","primary")
                                    else
                                            Claim("UserType","normal")
                let accountClaim =
                    Claim("AccountId",string appUser.accountId)
                    |> DBResult.retn

                // No need to load UserId claim, it is set by aspnetcore under ClaimTypes.NameIdentifier

                // merge lists and return resulting Claims list
                let claims = DBResult.retn usertypeClaim
                             |>DBResult.merge accountClaim
                                |> function
                                    | Ok claims -> claims
                                    | Error _ -> logger.LogError("Error assigning claims to user {0}", appUser.Email)
                                                 []
                do! closeIfNeeded()
                return ((claims |> List.toArray) :>  System.Collections.Generic.IList<Claim>)
            } |> fun a -> Async.StartAsTask (a,cancellationToken=cancellationToken)
            //return ((user.claims |> Seq.toArray ):> System.Collections.Generic.IList<Claim>)
            //Task.FromResult((user.claims |> Seq.toArray ):> System.Collections.Generic.IList<Claim>)
        member this.ReplaceClaimAsync(appUser: ApplicationUser, claim: Claim, newClaim: Claim, cancellationToken: CancellationToken) : Task =
            failwith "ReplaceClaimAsync currently not implemented"
            appUser.Claims <- (appUser.Claims |> List.map (fun c -> if c = claim then newClaim else claim))
            Task.CompletedTask
        member this.RemoveClaimsAsync(appUser: ApplicationUser, claims: IEnumerable<Claim>, cancellationToken: CancellationToken) : Task =
            failwith "RemoveClaimAsync currently not implemented"
            appUser.Claims <- (appUser.Claims |> List.where (fun c -> not (Seq.contains c claims)))
            Task.CompletedTask
        member this.GetUsersForClaimAsync(claim: Claim, cancellationToken: CancellationToken) : Task<IList<ApplicationUser>> =
            failwith "GetUsersForClaimasync currently not implemented"
            Task.FromResult(([||]|> Seq.toArray ):> System.Collections.Generic.IList<ApplicationUser>)

    interface IUserEmailStore<ApplicationUser> with
        member this.GetEmailAsync(appUser:ApplicationUser, cancellationToken:CancellationToken) =
            Task.FromResult(appUser.Email)
        member this.SetEmailAsync(appUser: ApplicationUser, email: string, cancellationToken: CancellationToken) : Task =
            appUser.Email <- email
            appUser.NormalizedEmail <- (email |> String.toUpper)
            Task.CompletedTask
        member this.GetEmailConfirmedAsync(appUser: ApplicationUser, cancellationToken: CancellationToken) : Task<bool> =
            Task.FromResult(appUser.EmailConfirmed)
        member this.SetEmailConfirmedAsync(appUser: ApplicationUser, confirmed: bool, cancellationToken: CancellationToken) : Task =
            appUser.EmailConfirmed <- confirmed
            Task.CompletedTask

        member this.FindByEmailAsync(normalizedEmail: string, cancellationToken: CancellationToken) : Task<ApplicationUser> =
            async {
                let! closeIfNeeded = client.OpenIfNeeded()
                let! result = DBAction.queryAction "select * from users where UPPER(email) = %s" normalizedEmail
                            |> DBAction.run<User> client
                do! closeIfNeeded ()
                match result with
                | Ok [u] -> return ApplicationUser.Create(u)
                | Ok [] -> return null
                | Ok l ->
                    logger.LogError("EmailStore.FindByemail found multiple users for email {0}: {1}", normalizedEmail, l)
                    return null
                | Error es ->
                    logger.LogError(es |> Error.toString)
                    return null
            } |> Async.StartAsTask

        member this.GetNormalizedEmailAsync(appUser: ApplicationUser, cancellationToken: CancellationToken) : Task<string> =
            Task.FromResult(appUser.NormalizedEmail)

        member this.SetNormalizedEmailAsync(appUser: ApplicationUser, normalizedEmail: string, cancellationToken: CancellationToken) : Task =
            appUser.NormalizedEmail <- normalizedEmail
            Task.CompletedTask


//     IUserEmailStore<User>, IUserPhoneNumberStore<User>,
//        IUserTwoFactorStore<User>, IUserPasswordStore<User>, IUserRoleStore<User>
