namespace web

open WebSharper.UI.Html
open WebSharper
open WebSharper.UI
open WebSharper.UI.Client
open HtmlHelpers
open Css

[<JavaScript>]
module DetailvalueFormater =
    // We need to define the type explicitely as the object expression
    //
    //    let crate = { new Entity.IMarkupCrate with
    //        member _.Apply (v:Entity.IMarkupEvaluator<_>) =
    //            v.Eval<string> s (detail.class_name)
    //    }
    //
    // causes the error
    //
    //    Method not found in JavaScript compilation: (Apply<_> : Entity+MarkupEvaluator`1<'?> -> '?), Candidates: (Apply<_> : Entity+MarkupEvaluator`1<'T0> -> 'T0)
    //
    // Defining the type explicitly and instanciating it like this:
    //    let crate = new DataTableValueCrate(v)
    // works fine though.
    open HtmlHelpers
    open Css
    open WSExtensions
    open FormHelpers
    open Specs
    let stringOfValue (v:obj) =
        if isNull v then
            ""
        else
            string v

    // function to extract values from the yaml of a file attachment
    let extractFieldFromValue(v:obj) (field:string)=
        // We know this is a string
        let s:string = unbox v
        // this is yaml formatted, with one file per line
        s.Split([|'\n'|])
        // extract the (unique) line with the key we're looking for
        |> Array.filter (fun s -> s.StartsWith(":"+field))
        // we split on ':', and take index 2 because the key starts with ':' too
        |> Array.map (fun s -> s.Split(':').[2])
        // trim spaces and quotes, in case the value was quoted (as is the case
        // sometimes for devail_value_id)
        |> Array.map (fun s -> s.Trim([|' ';'"'|]))
        // don't return the array but its (unique) element
        |> Array.head

    // get the detail_value_id. We only have access to the value here, and
    // detail_value_id is stored under the key ":detail_value_id"
    let extractDetailValueIdFromAttachmentValue (v:obj) =
        let s:string = unbox v
        if s.StartsWith("---") then
            extractFieldFromValue v "detail_value_id"
            |> int
        else
            let info:ServerFileAttachment.FileAttachmentWithOptionalIndb = WebSharper.Json.Deserialize(s)
            info.detail_value_id |> Option.get


    // get the filename of the attachment
    let extractFileNameFromAttachmentValue (v:obj) =
        let s:string = unbox v
        if s.StartsWith("---") then
            extractFieldFromValue v "filename"
        else
            let info:ServerFileAttachment.FileAttachmentWithOptionalIndb = WebSharper.Json.Deserialize(s)
            info.filename

    type DataTableValueCrate(s:string) =
        interface Entity.IMarkupCrate with
            member _.Apply (v:Entity.IMarkupEvaluator<_>) =
                v.Eval<string> s
    type DataTableCellMarkupEvaluator(class_name:string) =
         interface Entity.IMarkupEvaluator<Doc> with
            member _.Eval (value): Doc =
                match class_name with
                | "EmailDetailValue" -> a [attr.href $"mailto:{stringOfValue value}"] [text (stringOfValue value)]
                | "WebUrlDetailValue" -> a [attr.href (stringOfValue value)] [text (stringOfValue value)]
                | "FileAttachmentDetailValue" ->
                        // this test of null values was added with file attachment display, when other deta types
                        // were already done for a long time. It could be added before the "match  class_name", but it causes trouble:
                        // some tests check the number of links present in a page, and even if a web url detail value has
                        // a null value, it is displayed as a link with empty text. Not displaying an empty link for a nul weburl detail value
                        // will cause the tests to fail.
                        // The solution would be to update the expected html, but it seems too big an effort for the benefit at this time.
                        if isNull (unbox value) then
                            Doc.Empty
                        else
                            a [attr.href (Routing.router.Link (GetAttachment (extractDetailValueIdFromAttachmentValue value)))] [text (extractFileNameFromAttachmentValue value)]
                | _ -> text (stringOfValue value)
    type FormFieldCrate(s:Var<ValueSpec>) =
        interface Entity.IMarkupCrate with
            member _.Apply (v:Entity.IMarkupEvaluator<'ret>) =
                // box argument otherwise type system requests a type constraint to Var<string>
                v.Eval (box s)
    module private UploadHelpers =
        let t = ClientI18n.t
        let uploadFiles
            (fileVar:Var<JavaScript.File array>)
            (progressVar:Var<int option>)
            (fileUploadId:Var<string option>)
            (isValidVar:Var<bool>)
            (validationTextVar:Var<string>)
            (cancelFunction:ref<unit->unit>) =

            // Function setting the isValidVar to false and the corresponding validationTextVar
            let markInvalid(xhr:WebSharper.JavaScript.XMLHttpRequest) =
                isValidVar.Set false
                let translatedStatus = t.t(xhr.StatusText)
                let message = t.t("An error occured: %1. (Status = %2).", translatedStatus, xhr.Status)
                validationTextVar.Set message

            let files = fileVar.Get()
            match files|>List.ofSeq with
            // if no file to be uploaded, do not send request, but still change the id for that field.
            // An empty string will be detected as no file uploaded. In prod, use DU!
            | [] -> fileUploadId.Set (Some "")
            | _ -> // We need to build a FormData and append the file to be uploaded
                let formData = JavaScript.FormData()
                files
                |> Array.iter (fun f -> formData.Append(f.Name,f))

                // We will send an xhr request to upload the file
                let xhr = new JavaScript.XMLHttpRequest();
                // set the cancel function to abort this xhr
                cancelFunction.Value <- (fun () ->
                    xhr.Abort()
                    // Reset progress to None, as it is used to decide the display of the file selection element
                    progressVar.Set None
                    )
                // Compute progress
                xhr.Upload.Onprogress <-
                     (fun (ev) ->
                        let progressEvent = downcast ev : JavaScript.ProgressEvent
                        progressVar.Set( Some (int ((float progressEvent.Loaded) / (float progressEvent.Total)*100.0)))
                     )
                // This callback is sent when the upload is finished (also when interrupted by server when file too big
                // but whole file was already sent)
                xhr.Onload <-
                    (fun ev ->
                        // Check status as a connection status 413 Payload Too Large can still end up here
                        if xhr.Status<300 then
                            isValidVar.Set true
                            let response = xhr.Response
                            // The server returns a simple JSON. We use anonymous records here, but in prod we could/should
                            // use a proper record type shared by client and server.
                            let fileInfo:{|id:string|} = unbox (WebSharper.JavaScript.JSON.Parse (unbox response))
                            fileUploadId.Set (Some fileInfo.id)
                        else
                            markInvalid xhr

                    )
                // This is called in case of error, eg the file sent it too big
                xhr.Onerror <-
                    (fun ev ->
                        markInvalid xhr
                    )
                let uploadLink = Routing.router.Link HandleUpload
                // Do not set the content type here, it prevents the browser setting
                // data boundaries https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects#sending_files_using_a_formdata_object
                // Now that we have defined all prerequisites like callback and formData, issue the xhr request
                xhr.Open("post",uploadLink)
                xhr.Send(formData)
    open UploadHelpers
    // The form field formatted returns for each fied on instance of this type, which communicates to
    // the caller the state of each field, so it can deduce a global form state (eg is form submittable)
    type FormFieldInfo =
        {
            isValid: View<bool>
            waitingValidation: View<bool>
            // prepareSubmit is used to upload files before other fields are submitted
            // The bool indicates if the call was successful and if form submission can proceed
            // according to this field
            prepareSubmit: unit -> View<bool>
            cancelSubmit: ref<unit->unit>
            // A function called after the form submission was handled on the server, the bool indicates if submission was successful
            // Introduced to reset file selection in forms after submission
            afterSubmissionCallback: bool*FormKind->unit
            reset : unit -> unit
        }
    type FormFieldMarkupEvaluator(detailSpec:Specs.DetailSpec) =
         // The evaluator returns  the Doc of the field, a bool view indicating if the field holds a valid value and
         // a bool indicating if the field is still awaiting validation (due to a change)
         // Returning the validity view enables to prevent form submission if a field has an invalid value.
         interface Entity.IMarkupEvaluator<Doc*FormFieldInfo> with
            member _.Eval (var): Doc*FormFieldInfo =
                // The field's id including a random suffix. The field id is referenced by the label's `for` attribute.
                // It was initially simply the detail name. However, we could end up with a page having multiple forms for one entity
                // and including a rancom string in the id should avoid any problem.
                let fieldId = $"{detailSpec.name}-{ClientHelpers.randomIntString 10000}"

                // Wrap the field to display its label in the firls "column"
                let wrapField (field:Doc) =
                    div [ classes [BS.row; BS.``mb-3``; BS.``align-items-center``] ]
                        [
                          div [classes [BS.``col-sm-4``]] [
                            label [ attr.``for`` fieldId;  classes [BS.``col-sm-4``; BS.``col-form-label``] ] [text detailSpec.name]
                          ]
                          div [classes [BS.``col-sm-8``]] [
                            field
                          ]
                        ]
                // helper to combine the Doc and awaitingValidation view of the debounced field returned by the debouncedInput function
                // with the validity View defined locally
                let buildFieldInfo isValidView (resetFunction) (field,awaitingValidation) =
                    wrapField field
                    |> (fun field ->
                        field,
                        {   isValid= isValidView;
                            waitingValidation = awaitingValidation;
                            prepareSubmit = (fun () -> View.Const true);
                            cancelSubmit = ref (fun () -> ());
                            afterSubmissionCallback = (fun _ -> ())
                            reset = resetFunction
                        })
                // The validation function is async so that both client side and server side validation
                // can be handled by this function.
                let validatedFieldAfterDelay (delay:int)(validationFunction:string->Async<bool>) (inputVar:Var<string>) (resetFunction:unit->unit)=
                    let isValidView =
                        inputVar.View
                        // do a RPC to validate, because WS does not translate Regexps to JavaScript
                        |> View.MapAsync validationFunction
                    let fieldClasses =
                        isValidView
                        |> View.Map (fun valid ->
                            if valid then
                                BS.``form-control``
                            else
                                [BS.``form-control``;BS.``is-invalid``]
                                |> String.concat " "

                        )
                    // The DebouncedInput returns a triplet:
                    // - the doc to display
                    // - the awaitingValidation View
                    // - a thunk to be used to reset the field. This is needed because the DebouncedInput uses an internal var that is not reset
                    //   when inputVar is reset, which is the case when the cretion form was submitted and fields are reset. Not resetting the
                    //   DebouncedInput internal var introduces an inconsistency: the ValueSpec is set to "" but the field still displays the (now obsolete) value
                    //   held by the internal var.
                    Doc.debouncedInput [attr.id fieldId; attr.classDyn fieldClasses] delay inputVar
                    |> (fun (doc,awaitingValidation,resetDebounced) -> buildFieldInfo isValidView (fun () -> resetFunction(); resetDebounced()) (doc,awaitingValidation) )
                // Define functions for client side (no delay) and server side (500ms delay) validations
                let serverSideValidatedField = validatedFieldAfterDelay 500
                let clientSideValidatedField = validatedFieldAfterDelay 0

                // Helper to update the value spec with the new client value
                let setClientProposed (old:ValueSpec) (cstructor:string option -> ClientValue) (value:string option) =
                    // Replace empty string values by None
                    // This makes that detail values updates to an empty value are actually erased from the database
                    let cleanedUpValue = match value with |Some "" -> None | _ -> value
                    {old with value = ClientProposed (cstructor cleanedUpValue)}


                // This uses WebSharper's IntInput for integer values.
                let validatedIntField (valueSpecVar:Var<ValueSpec>)(resetFunction:unit->unit) =
                    let clientVar:Var<string> =
                        valueSpecVar.Lens
                            (fun v -> valueToString (valueSpecVar.Value.value))
                            (fun old v ->
                                setClientProposed old ClientValue.Integer (Some v)
                            )
                    // the validity is based on the IntInput's var value
                    let isValidView =
                        clientVar.View
                        |> View.Map
                            (fun v ->
                                // empty value is ok
                                if v = "" then
                                    true
                                else
                                    // refuse space-only value
                                    if v.Trim() = "" then
                                        false
                                    else
                                        match System.Int64.TryParse v with
                                        | true, _ -> true
                                        | _ -> false
                            )
                    let fieldClasses =
                        isValidView
                        |> View.Map (fun valid ->
                            if valid then
                                BS.``form-control``
                            else
                                [BS.``form-control``;BS.``is-invalid``]
                                |> String.concat " "

                        )
                    Doc.InputType.Text [attr.id fieldId; attr.classDyn fieldClasses] clientVar
                    |> (fun doc -> buildFieldInfo isValidView resetFunction (doc, View.Const false))

                // Helper to get the reset function for the var of the ValueSpec used by a form field.
                // Resetting the valueVar (corresponding most of the time to the string in the field is not sufficient as
                // it would retain the detail_value_id, meaning that the value of the previous detail valu would be overriden)
                let resetValueSpecThunk (var:Var<ValueSpec>) =
                    (fun () -> var.Set (var.Value.Reset()))
                // Helper function to transform he doc of a field to a tuple indicating an always valid field
                // (i.e. not needing validation before submission)
                let makeAlwaysValid (resetFun)(doc:Doc) : Doc * FormFieldInfo=
                    doc,
                    {
                        isValid=View.Const true;
                        waitingValidation = View.Const false;
                        prepareSubmit = (fun () -> View.Const true);
                        cancelSubmit = ref (fun () -> ())
                        afterSubmissionCallback = (fun _ -> () )
                        reset= resetFun
                    }

                match detailSpec.dataType.className with
                // the unbox var gives us a Var<string>
                | "DdlDetailValue" ->
                    let valueIds =
                        detailSpec.propositions
                        |>Option.get
                        |>List.map
                            (fun p ->
                                //p.value
                                let (Ids.DetailValuePropositionId propId) = p.detailValuePropositionId.Value
                                propId
                            )
                    let displayOptionFor (optionId)=
                        detailSpec.propositions
                        |>Option.get
                        |>List.map
                            (fun p ->
                                let (Ids.DetailValuePropositionId propId) = p.detailValuePropositionId.Value
                                (p.value, propId)
                            )
                        |> List.filter (fun (_,id) -> optionId=id)
                        |> List.head
                        |> fst
                    let getpropositionIdFromValue (v:Value) =
                        match v with
                        | ServerValidated sv ->
                            match sv with
                            |ServerValue.DdlValueChoice propOption -> propOption|>Option.map (fun o -> DdlChoice.toId o)
                            | other -> failwithf "unexpected server validated value %A where DdlValueChoice expected" other
                        | ClientProposed cp ->
                            match cp with
                            | ClientValue.DdlValueChoice intOption -> intOption
                            | other -> failwithf "unexpected client proposed value %A where DdlValueChoice expected" other
                    let valueSpecVar:Var<ValueSpec> = unbox var
                    let valueVar = Var.Lens valueSpecVar
                                            (fun v ->
                                                getpropositionIdFromValue v.value
                                            )

                                            (fun old fromDdl ->
                                                {old with value = ClientProposed ((ClientValue.DdlValueChoice) fromDdl)}
                                            )
                    let noneText = t.t("None")
                    Doc.InputType.SelectOptional [attr.id fieldId; classes [BS.``form-control``;BS.``form-select``]] noneText displayOptionFor valueIds valueVar
                    |>wrapField
                    |>makeAlwaysValid (resetValueSpecThunk valueSpecVar)
                | "LongTextDetailValue" ->
                    let valueSpecVar:Var<ValueSpec> = unbox var
                    let valueVar =
                        valueSpecVar.Lens
                            (fun v -> valueToString (valueSpecVar.Value.value))
                            (fun old v -> setClientProposed old ClientValue.LongText (Some v))
                    Doc.InputType.TextArea [attr.id fieldId; classes [BS.``form-control``]] valueVar
                    |> wrapField
                    |> makeAlwaysValid (resetValueSpecThunk valueSpecVar)
                | "SimpleDetailValue" ->
                    let valueSpecVar:Var<ValueSpec> = unbox var
                    let valueVar =
                        valueSpecVar.Lens
                            (fun v -> valueToString (valueSpecVar.Value.value))
                            (fun old v -> setClientProposed old ClientValue.Simple (Some v))
                    Doc.InputType.Text [attr.id fieldId; classes [BS.``form-control``]] valueVar
                    |> wrapField
                    |> makeAlwaysValid (resetValueSpecThunk valueSpecVar)
                | "EmailDetailValue" ->
                    let valueSpecVar:Var<ValueSpec> = unbox var
                    let valueVar =
                        valueSpecVar.Lens
                            (fun v -> valueToString (valueSpecVar.Value.value))
                            (fun old v -> setClientProposed old ClientValue.Email (Some v))
                    serverSideValidatedField Server.isEmailValid valueVar (resetValueSpecThunk valueSpecVar)
                | "IntegerDetailValue" ->
                    let valueSpecVar:Var<ValueSpec> = unbox var
                    validatedIntField valueSpecVar (resetValueSpecThunk valueSpecVar)
                | "WebUrlDetailValue" ->
                    let valueSpecVar:Var<ValueSpec> = unbox var
                    let valueVar =
                        valueSpecVar.Lens
                            (fun v -> valueToString (valueSpecVar.Value.value))
                            (fun old v -> setClientProposed old ClientValue.WebURL (Some v))
                    serverSideValidatedField Server.isWebURLValid valueVar (resetValueSpecThunk valueSpecVar)
                | "DateDetailValue" ->
                    let dateValidationAsync (s:string) = async {
                        return DateAndTime.isValid s
                    }
                    let valueSpecVar:Var<ValueSpec> = unbox var
                    let valueVar =
                        valueSpecVar.Lens
                            (fun v -> valueToString (valueSpecVar.Value.value))
                            (fun old v -> setClientProposed old ClientValue.DateAndTime (Some v))
                    clientSideValidatedField dateValidationAsync valueVar (resetValueSpecThunk valueSpecVar)
                | "FileAttachmentDetailValue" ->
                    let valueSpecVar:Var<ValueSpec> = unbox var
                    // If the file gets deleted, we need to set the detailValueId of the ValueSpec to None. This Var enables this
                    let valueSpecDetailValueIdVar =
                        valueSpecVar.Lens
                            (fun v -> v.detailValueId)
                            (fun old v -> {old with detailValueId = v })
                    // Attention:
                    // As this var is used as a lens source for other vars below, changing one of thess vars below will change this var
                    // to be a ClientProposed value. Vars derived from this one should thus not be changed if this field value is not changed
                    let specValueVar =
                        valueSpecVar.Lens
                            (fun v -> valueToString (valueSpecVar.Value.value))
                            (fun old v ->
                                setClientProposed old ClientValue.FileAttachment (Some v))

                    // Keep the initial value in case the user resets the file selection when a file is already saved on the server
                    // for this detail value
                    let serverValueVar = Var.Create (specValueVar.Get())
                    // Define a lens var holding the FileAttachment record, result of deserialising the specValueVar
                    let fileAttachmentVar:Var<ServerFileAttachment.FileAttachment>=
                        specValueVar.Lens
                            // getter
                            (fun v ->
                                Json.Deserialize(v)
                            )
                            // writer
                            (fun v fromField ->
                                Json.Serialize fromField
                            )
                    // Define a lens var for each field of the FileAttachment
                    let filenameVar =
                        fileAttachmentVar.LensAuto
                            (fun r -> r.filename)
                    let filetypeVar =
                        fileAttachmentVar.LensAuto
                            (fun r -> r.filetype)
                    let s3KeyVar =
                        fileAttachmentVar.LensAuto
                            (fun r -> r.s3_key)
                    let uploadedVar =
                        fileAttachmentVar.LensAuto
                            (fun r -> r.uploaded)
                    let inDbVar =
                        fileAttachmentVar.LensAuto
                            (fun r -> r.indb)
                    let detailValueIdVar =
                        fileAttachmentVar.LensAuto
                            (fun r -> r.detail_value_id)

                    // Validity variables
                    // coommunicate to form if field is valid
                    let isValidVar = Var.Create true
                    // message to be displayed when field is invalid
                    let validationTextVar = Var.Create ""

                    // View of the Doc to be displayed with the error. Empty Doc if no error.
                    let errorBoxView =
                        View.Map2
                            (fun isValid validationText ->
                                match isValid with
                                | false -> infobox Danger  (span [] [text validationText])
                                | true -> Doc.Empty
                            )
                            isValidVar.View
                            validationTextVar.View

                    // Define a lens var (jsFilesVar) holding an array of javascript file objects, to be passed to the Doc.InputType.File.
                    let jsFilesVar:Var<JavaScript.File array> = Var.Create [||]
                    // Define var to hold the upload progress
                    let progressVar:Var<int option> = Var.Create None
                    // Define var to hold the upload id, which is returned by the server.
                    // This identifies the file on the server side and has to be sent when the form is submitted.
                    let uploadIdVar:Var<string option> = Var.Create None
                    // This function is used to cancel an ongoing upload. It is a ref because it needs to be set in the function uploadFiles
                    // which is issuing the xhr.
                    let cancelFunction:ref<unit->unit> = ref (fun () -> ())

                    // Function to reset the file selector.
                    // Beware that changing a var derived from specValueVar will changed this field as a ClientProposed
                    // and thus will be included it in the form processing.
                    let resetFileSelectionField () =
                        // Set uploadIdVar to None, otherwise the upload is considered as uploaded
                        uploadIdVar.Set None
                        // Set progressVar to None so the file selection element is shown
                        progressVar.Set None
                        // Set jsVarFilesVar to an empty array so the file selection element is reset
                        jsFilesVar.Set [||]
                    let resetToServerValue() =
                        resetFileSelectionField()
                        // When the user resets the field, we reset it to the value currently save for this detailvalue on the server
                        specValueVar.Set (serverValueVar.Get())
                    // Reinitialise field, eg after deletion
                    let reinitialiseFileSelectionField()=
                        resetFileSelectionField()
                        // Reset to empty file
                        fileAttachmentVar.Set (ServerFileAttachment.New())
                        // Also reset the value considered as initially on the server
                        serverValueVar.Set(specValueVar.Get())

                    // Resetting a file input element is done by setting its value to ""
                    // This can only be done by working with the element in javascript.
                    // I wanted to reset the file input element only by setting the Var<File array> to an empty array.
                    // To that end, I watch the changes on the Var, and if it is set to an empty array,
                    // the value of the file input is set to "". To identify the file input, it is assigned a randomly generated data-*
                    // attribute getting a random value. These are used with Document.QuerySelector
                    let resettableFileInput attrs (jsFilesVar: Var<JavaScript.File array>) (progressVar:Var<int option>)=
                        let randomAttributeSuffix =  ClientHelpers.randomIntString 10000
                        let randomString = ClientHelpers.randomIntString 10000
                        jsFilesVar.View |> View.Sink
                            (fun v ->
                                if Seq.isEmpty v then
                                    let fileElt = (downcast JavaScript.JS.Document.QuerySelector($"[data-{randomAttributeSuffix}=\"{randomString}\"]"): WebSharper.JavaScript.HTMLInputElement)
                                    if not  (isNull fileElt) then
                                        fileElt.Value <- ""

                            )
                        div []
                            [
                                Client.Doc.InputType.File (attrs |> Seq.append [|Attr.Create $"data-{randomAttributeSuffix}" randomString|] ) jsFilesVar
                                // The reset button is only displayed when a file is selected.
                                jsFilesVar.View
                                |> View.Map (fun files ->
                                    if Array.length files = 0 then
                                        span [] []
                                    else
                                        button
                                            [
                                                attr.``class`` "btn btn-secondary"
                                                on.click (fun ev el->
                                                    // When the user resets the field, we reset it to the value currently saved for this detailvalue on the server
                                                    resetToServerValue ()
                                                )
                                            ]
                                            [t.tt("Reset")]
                                ) |> Doc.EmbedView
                            ]


                    // When the file selection is modified, we update the FileAttachment Var accordingly
                    jsFilesVar.View
                        |> View.Sink
                            (function
                                // If no file is selected, set all values of FileAttachment to empty
                                | [||] ->
                                    // Do not reset vars if the file is uploaded, as it would reset the vars when receiving the
                                    // ServerValidation spec at render of the page
                                    // This should not cause trouble as if the uploaded is true, the file selectionis not displayed
                                    // Also, do not reset if the filename is empty. This happens at first show of the field. Not testing
                                    // This "filename empty" condition makes that we then would update a var derived from valueSpecVar, which
                                    // then triggers a change of the ValueSpec to a ClientProposed, which is not fine at first render of the
                                    // form (the field would be considered as changed as it has become a ClientProposed)
                                    if not uploadedVar.Value && filenameVar.Value <> "" then
                                        filenameVar.Set ""
                                        filetypeVar.Set None
                                        uploadedVar.Set false
                                        isValidVar.Set true
                                        s3KeyVar.Set None
                                // When a file is selected, set its name and type, and reset key and uploaded (needed
                                // as this is a new file that is selected and it is not uploaded)
                                | [|jsFileInfo|] ->
                                    // IMPORTANT: keep the update to inDbVar as first change as it triggers different behaviour for
                                    // later changes (it allows to keep non-modifified file fields as ServerValidated. See updateValue in
                                    // InstanceForm.fs for FileAttachment)
                                    inDbVar.Set false
                                    uploadedVar.Set false
                                    s3KeyVar.Set None
                                    filenameVar.Set jsFileInfo.Name
                                    filetypeVar.Set (Some jsFileInfo.Type)
                                    // When a file is selected, check its size is acceptable
                                    if jsFileInfo.Size > JavaScript.JS.Inline("window.myowndb_fileAttachments_maxBytes") then
                                        isValidVar.Set false
                                        validationTextVar.Set (t.t("File too big"))
                                    else
                                        isValidVar.Set true
                                        validationTextVar.Set ("")

                                | _ ->
                                    raise (System.Exception("multiple files selected but this is not supported"))
                            )
                    // When the upload id is set, the file has been uploaded, and we update the uploaded field accordingly.
                    // This is probably redundant legacy, but left as is for the moment.
                    // We also set the s3key to the upload id, with the plan to clean it later (using a proper field name)
                    uploadIdVar.View
                        |> View.Sink
                            (fun idOption ->
                                match idOption with
                                // If the upload id is empty, this means no file was uploaded because no file was selected.
                                | Some "" -> ()
                                // Only update the Vars if the upload has effectively taken place so that the FileAttachment instance
                                // is updated accordingly
                                | Some uploadId ->
                                    uploadedVar.Set true
                                    s3KeyVar.Set idOption
                                | None -> ()

                            )

                    // We need to reuse the same input element so that selected files are kept when canceling an upload.
                    let fileInputElement =
                        div []
                            [
                                resettableFileInput [attr.id fieldId] jsFilesVar progressVar
                        ]

                    // The View<bool> indicating if the file selector should be displayed
                    let shouldDisplayFileSelector =
                        View.Map2
                            (fun (progress: int option) indb  -> not progress.IsSome && not indb
                            )
                            progressVar.View
                            inDbVar.View


                    div [] [
                        // Here is the UI shown to the user
                        // We keep the file selection element always in the document, otherwise the afterSubmissionCallback might not work
                        // because it is working with havascript's document element selection
                        div [attr.classDynPredBoth "d-block" "d-none" shouldDisplayFileSelector ] [fileInputElement]
                        // Display other elements related to the file selection (progress or uploaded filename). These should be displayed only
                        // when the file selection element is not shown, so make sure the conditions checked are opposite to the previous div's class display.
                        progressVar.View
                        |> View.Map
                            (function
                                |Some progress ->
                                    if  progress < 100 then
                                        div [] [text (sprintf "%d%%" progress)]
                                    else
                                        div [] []
                                |None ->
                                        div [] []
                            )
                        |> Doc.EmbedView
                        View.Map3
                            (fun filename indb progress->
                                match progress with
                                |Some progress ->
                                    if  progress < 100 then
                                        div [] [text (sprintf "%d%%" progress)]
                                    else
                                        div [] []
                                |None ->
                                    let deleteElement ()=
                                        DeleteConfirmation.deleteConfirmation "attachment" (
                                            (fun () -> async {
                                                match! Server.deleteAttachment (fileAttachmentVar.Get()) with
                                                | Ok _ ->
                                                    reinitialiseFileSelectionField()
                                                    valueSpecDetailValueIdVar.Set None
                                                    // signal success
                                                    return true
                                                | Error _ ->
                                                    // signal failure
                                                    return false
                                            })
                                        )

                                    if indb then
                                        detailValueIdVar.Get()
                                        |> Option.map (fun detailValueId ->
                                            div []
                                                [ t.tt "File currently saved in the database"
                                                  text ":"
                                                  a [classes [BS.``pe-1``]; attr.href (Routing.router.Link (GetAttachment (int64 detailValueId)))] [text filename]
                                                  // FIXME: this element causes flickering when the user clicks no
                                                  deleteElement().V
                                                ]
                                        )
                                        // For the creation form the detailValueIdVar value is None, in which case we display an empty doc
                                        |> Option.defaultValue (div [] [])
                                    else
                                        div [] []
                            )
                            filenameVar.View
                            inDbVar.View
                            progressVar.View
                        |> Doc.EmbedView
                        errorBoxView.V
                    ]
                    |> wrapField
                    ,
                    { isValid = isValidVar.View
                      waitingValidation =View.Const false
                      // The prepareSubmit function for a file field uploads the file and returns a View<bool> which will
                      // be true when the file has been uploaded. This View is built from the Var holding the uploadId returned
                      // by the server
                      prepareSubmit = fun () ->
                        uploadFiles jsFilesVar progressVar uploadIdVar isValidVar validationTextVar cancelFunction
                        uploadIdVar.View |> View.Map (fun o -> o.IsSome)
                      cancelSubmit = cancelFunction
                      afterSubmissionCallback =
                        fun (success:bool,formKind:FormKind) ->
                            if formKind = Edition then
                                resetFileSelectionField()
                            else
                                (resetValueSpecThunk valueSpecVar)()
                      reset = resetFileSelectionField
                    }
                | _ ->
                    let valueSpecVar:Var<ValueSpec> = unbox var
                    let valueVar =
                        valueSpecVar.Lens
                            (fun v -> valueToString (valueSpecVar.Value.value))
                            (fun old v -> setClientProposed old ClientValue.Simple (Some v))
                    Doc.InputType.Text [attr.readonly "readonly"; attr.id fieldId; classes [BS.``form-control``]] valueVar
                    |> wrapField
                    , {
                        isValid=View.Const true;
                        waitingValidation = View.Const false;
                        prepareSubmit = (fun () -> View.Const true);
                        cancelSubmit = ref (fun () -> ())
                        afterSubmissionCallback = (fun _ -> ())
                        reset = (resetValueSpecThunk valueSpecVar)
                    }
    let memoizedEvaluatorGetter () =
        let mutable cache= new Map<string,DataTableCellMarkupEvaluator>([||])
        let getEvaluator (class_name:string) =
            match cache.TryFind class_name with
            | Some ev ->
                      ev
            | None -> let ev = DataTableCellMarkupEvaluator(class_name)
                      cache <- cache.Add(class_name,ev)
                      ev
        getEvaluator

    let displayDetailValue (d:Entity.EntityDetail) value  =
        let getEvaluator = memoizedEvaluatorGetter()
        let crate = DataTableValueCrate(value)
        let evaluator = getEvaluator(d.class_name)
        d.display crate evaluator
