namespace web
open WebSharper
open Microsoft.AspNetCore.Authorization;
open WebSharper.UI.Html
open WebSharper.Sitelets
open WebSharper.UI
open WebSharper.UI.Html.Elt
open DatatableTypes

type LoginError = | AuthenticationError

type EndPoint =
    | [<EndPoint "/"; Authorize>] Home
    | [<EndPoint "/instances"; Query("state"); Authorize>] Instances of entityId:int * state:string option
    | [<EndPoint "/instance"; Query("state"); Authorize>] Instance of instanceId:int * state:string option
    | [<EndPoint "/create_instance"; Authorize>] CreateInstance of entityId:int
    | [<EndPoint "/edit_instance"; Authorize>] EditInstance of instanceId:int
    | [<EndPoint "/about">] About
    | [<EndPoint "/login"; Query("ReturnUrl"); Query("errorCode")>] Login of ReturnUrl: string option * errorCode: LoginError option
    | [<EndPoint "/logout"; Authorize>] Logout
    | [<EndPoint "/forgot_password">] ForgotPassword
    | [<EndPoint "/reset_password">] ResetPassword of token:string * email:string
    | [<EndPoint "/signup">] Signup
    | [<EndPoint "/confirm_email">] ConfirmEmail of token:string * email:string
    | [<EndPoint "/get_attachment"; Authorize >] GetAttachment of detailValueId:int64
    | [<EndPoint "/get_table_csv"; Authorize; Query("searchValue","sortColumn","tableType")>] GetTableCsv of tableType:TableType *
                                                   id:int *
                                                   page:int *
                                                   perPage:int *
                                                   searchColumn:string *
                                                   searchValue:string *
                                                   sortColumn:string option*
                                                   sortDirection:SortDirection
    | [<EndPoint "POST /handle_upload";>] HandleUpload
    // The public form endpoint takes an optional GET parameter named "responsive". When present (with any value), the page includes the
    // iframeresizer code to communicate with the parent page and allow responsive resizing (https://github.com/davidjbradshaw/iframe-resizer)
    | [<EndPoint "/public_form";Query("responsive")>] PublicForm of entityId:int*responsive:string option
    | [<EndPoint "/public_form_submitted";>] PublicFormSubmitted of entityId:int
    | [<EndPoint "/import"; Authorize>] Import of entityId:int
    //  We add an endpoint that will actually  be handled by the embedded Sitelet for admin pages.
    //  We put everything in the wildcard, see https://github.com/dotnet-websharper/core/issues/1330
    | [<EndPoint "/admin";Authorize(Policy="Admin");Wildcard>] Admin of args:list<string>


[<JavaScript>]
module Routing =
    let router : WebSharper.Sitelets.Router<EndPoint> =
        WebSharper.Sitelets.InferRouter.Router.Infer ()
