namespace web

open System
open Microsoft.AspNetCore
open Microsoft.AspNetCore.Builder
open Microsoft.AspNetCore.Authorization;
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Identity;
open Microsoft.Extensions.DependencyInjection
open Microsoft.Extensions.Hosting
open WebSharper.AspNetCore
open Microsoft.AspNetCore.Authorization
open Microsoft.AspNetCore.Localization
open System.Globalization
// For ICatalog added as scoped service
open NGettext
open Serilog
open Serilog.Events
open Serilog.Formatting.Compact
open Hangfire
open Hangfire.PostgreSql
open Microsoft.Extensions.Configuration
open Microsoft.Extensions.Configuration.Json
open Serilog.Extensions.Hosting

open Microsoft.AspNetCore.DataProtection;
open Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption;
open Microsoft.AspNetCore.DataProtection.AuthenticatedEncryption.ConfigurationModel;

open System.Security.Cryptography

type Startup(config:IConfiguration, env: IWebHostEnvironment) =

    // There's no dependency injection to ConfigureServices. Add dependencies
    // to the Startup constructor above
    member this.ConfigureServices(services: IServiceCollection) =
        services.AddHttpContextAccessor()|>ignore
        // This was added as a singleton for development of the usertore, but
        // could be changed to scoped?
        services.AddScoped<IUserStore<ApplicationUser>, UserStore>() |> ignore
        // The default AuthorizationService will go over all AuthorizationHandlers and
        // check they succeed: https://docs.microsoft.com/en-us/aspnet/core/security/authorization/policies?view=aspnetcore-5.0#iauthorizationservice
        // It suffices then to inject our authorization handler
        //services.AddSingleton<IAuthorizationHandler, ApplicationAuthorizationHandler>() |> ignore

        services.AddLocalization(fun options -> options.ResourcesPath <- "Resources") |> ignore
        let supportedCultures = [| CultureInfo("en"); CultureInfo("fr") |]
        services.Configure<RequestLocalizationOptions>(fun (options:RequestLocalizationOptions) ->
            options.DefaultRequestCulture <- RequestCulture(supportedCultures.[0])
            // we need to add supported cultures here. Without this, the IRequestCultureFeature will
            // report english as the language.
            options.SupportedCultures <- supportedCultures
            options.SupportedUICultures <- supportedCultures
            // To set cookie used by the middleware, look at https://stackoverflow.com/a/51804296
            // we can change the order of the providers like this:
            //options.RequestCultureProviders <- [| QueryStringRequestCultureProvider(); CookieRequestCultureProvider() |]
        ) |> ignore

        // Add transient translation helper, using the catalog based on the current UICulture
        // This has to be transient. In my tests, making is scoped doesn't change the translation
        // when setting a new culture (eg with the GET parameter):  the http context's culture info is
        // changed, but the catalog's culture info is not....
        // I don't understand why as each request should get its catalog instance based on the UI Culture.
        // Maybe the ui culture is not updated as I think?
        services.AddTransient<web.I18n, web.I18n>(fun _ -> web.I18n(Catalog("myowndb", "./locale"))) |> ignore

        // inject Mailer corresponding to environment we are running in
        // !!! Do not inject a Mailer in test environment, or it breaks the playwright tests. The test mailer is
        // injected by the test code itself.
        // Seems the config item is null if the empty string it its value in the json.
        let noSmtpHost = isNull (config.Item("myowndb:smtp:host"))
        if env.IsDevelopment() then
          if noSmtpHost then
            Log.Information "In development mode and empty smtp host: using ConsoleMailer"
            services.AddTransient<IMailer,ConsoleMailer>() |> ignore
          else
            Log.Information("In development mode and smtp host specified: using MailkitMailer")
            services.AddTransient<IMailer,MailkitMailer>() |> ignore
        else if env.IsProduction() then
          if noSmtpHost then
            // Possibly change that in a fatal error as production without mails will probably not make much sense
            Log.Warning "Smtp host not set in production, mails will not be sent but logged in the console"
            services.AddTransient<IMailer,ConsoleMailer>() |> ignore
          else
            Log.Information("In production mode and smtp host: using MailkitMailer")
            services.AddTransient<IMailer,MailkitMailer>() |> ignore

        // Hangfire DBAction runner
        services.AddTransient<IDBActionRunner,DBActionRunner>() |> ignore

        services.AddTransient<INotifier,InstanceNotifier>() |> ignore
        services.AddTransient<IAttachmentsCleaner,AttachmentsCleaner>() |> ignore
        // Limit token lifetime to 15 minutes (eg for reset password functionality)
        services.Configure<DataProtectionTokenProviderOptions>(fun (opt:DataProtectionTokenProviderOptions) ->
               opt.TokenLifespan <- TimeSpan.FromHours(0.25))
        |> ignore

        // Configure max attachment size from config file. See MaxRequestBodySize in this file for more details.
        services.Configure<Microsoft.AspNetCore.Http.Features.FormOptions>(
            fun (o: Microsoft.AspNetCore.Http.Features.FormOptions) -> o.MultipartBodyLengthLimit <- int64 (config.Item("myowndb:fileAttachments:maxBytes")) )
        |> ignore

        // Log all config keys in development
        if env.IsDevelopment() then
            config.AsEnumerable(false)
            |> Seq.fold (fun acc v -> sprintf "%s\n%A" acc v) "Config values:\n"
            |> Log.Debug

        services.AddIdentityCore<ApplicationUser>( fun options ->
                                                       //options.User.RequireUniqueEmail <- true
                                                       options.SignIn.RequireConfirmedEmail <- true
                                                       // Disable these checks as it only looks as ASCII chars
                                                       // For coherence and control (eg for utf-8 support) we disable all included check
                                                       options.Password.RequireUppercase <- false
                                                       options.Password.RequireLowercase <- false
                                                       options.Password.RequireDigit <- false
                                                       options.Password.RequireNonAlphanumeric <- false
                                                       options.Password.RequiredLength <- 0
                                                       options.Password.RequiredUniqueChars <- 0
                                                       )
            .AddErrorDescriber<LocalizedIdentityErrorDescriber>()
            .AddDefaultTokenProviders()
            // need to add signInManager as AddIdentityCore doesn't do it itself
            .AddSignInManager<SignInManager<ApplicationUser>>()
            // needed to create a user (and setting the pasword hash value)
            .AddUserManager<UserManager<ApplicationUser>>()
            .AddPasswordValidator<AspNetPasswordValidators.HasUpper<ApplicationUser>>()
            .AddPasswordValidator<AspNetPasswordValidators.HasLower<ApplicationUser>>()
            .AddPasswordValidator<AspNetPasswordValidators.HasNonAlphaNum<ApplicationUser>>()
            .AddPasswordValidator<AspNetPasswordValidators.HasDigit<ApplicationUser>>()
            .AddPasswordValidator<AspNetPasswordValidators.HasLength<ApplicationUser>>()
            // this doesn't seem to inject the same way as AddScoped use above, so commented
            //.AddClaimsPrincipalFactory<ApplicationUserClaimsPrincipalFactory<ApplicationUser>>()
            |> ignore

        services.AddAuthorization(
            fun options ->
                                    // options.AddPolicy will make a policy available
                                    options.AddPolicy("Level2",System.Action<AuthorizationPolicyBuilder>(fun policy -> policy.RequireClaim("level","2")|>ignore))
                                    options.AddPolicy("Admin",System.Action<AuthorizationPolicyBuilder>(fun policy -> policy.RequireClaim("UserType","primary")|>ignore))
                                    options.AddPolicy("Authenticate",System.Action<AuthorizationPolicyBuilder>(fun policy -> policy.RequireAuthenticatedUser()|>ignore))
                                    ()
            )
        |> ignore


        // in test we rapidly prune idle connections so we can write a test checking that we don't leave
        // open connections behind
        let pgsqlConnectionStringAppend = if env.EnvironmentName = "Test" then
                                                "Connection Idle Lifetime=1;Connection Pruning Interval=1;"
                                            else
                                                ""
        services.AddHangfire(fun cfg ->
                            cfg.SetDataCompatibilityLevel(CompatibilityLevel.Version_170)
                                    .UseSimpleAssemblyNameTypeSerializer()
                                    .UseRecommendedSerializerSettings()
                                    .UsePostgreSqlStorage(MyOwnDB.WS.Database(config.GetValue("myowndb:database"), pgsqlConnectionStringAppend).ConnectionString() )
                            |> ignore
        ) |> ignore

        // opens connection to the database:
        // 1 connection for the server
        // + 2 connections per worker!
        if env.IsProduction() then
            services.AddHangfireServer() |> ignore
        else
            services.AddHangfireServer(fun cfg -> cfg.WorkerCount <- 1) |> ignore

        // Also see https://medium.com/swlh/how-to-distribute-data-protection-keys-with-an-asp-net-core-web-app-8b2b5d52851b
        // Keys management. These are used for token generation for example.
        let dataProtectionBuilder =
          services.AddDataProtection()
            .SetApplicationName("MyOwnDB")
            // Here are other examples of settings:
            // Those are the default values
            //.UseCryptographicAlgorithms(
            //  AuthenticatedEncryptorConfiguration(EncryptionAlgorithm=EncryptionAlgorithm.AES_256_CBC, ValidationAlgorithm = ValidationAlgorithm.HMACSHA256)
            //)
            // This is the contrary: keys are lost on restart. But I'm not sure as at the application start
            // there's still the message User profile is available. Using '$HOME/.aspnet/DataProtection-Keys' as key repository; keys will not be encrypted at rest.
            //.UseEphemeralDataProtectionProvider()

        // Configure directory where keys are persisted. The default config file has the value "-", effectively ignoring this setting.
        // The value "-" is used as unsetting value to be able to override a value set in a file by the value of the environment variable MYOWNDB_aspnet__dataProtection__keysPersistenceDirectory as
        // an environment variable with an empty value is ignore when the config instance is built.
        // Other values are the directory where to persist the keys.
        let keysPersistenceDirectoryConfigItem = "aspnet:dataprotection:keysPersistenceDirectory"
        let keysPersistenceDirectory = config.Item(keysPersistenceDirectoryConfigItem)
        if config.GetSection(keysPersistenceDirectoryConfigItem).Exists() && keysPersistenceDirectory <>"-" then
          // To save keys on disk, so keys are available across restarts. Will be usefule in production.
          dataProtectionBuilder.PersistKeysToFileSystem(new System.IO.DirectoryInfo(keysPersistenceDirectory)) |> ignore

        services.AddSitelet(Site.Main)
            .AddScoped<DB.DBClient>(fun _ ->
                    let dbClient = MyOwnDB.WS.Database(config.GetValue("myowndb:database"), pgsqlConnectionStringAppend).Get()
                    // Add configMap items to DBClient so it is available in DBAction.
                    // Add attachment paths for DetailValue's createAction.
                    let attachments = "myowndb:fileAttachments:path"
                    let waitRoom = "myowndb:fileAttachments:waitRoom"
                    dbClient.SetConfigItem(attachments,config.GetValue(attachments))
                    dbClient.SetConfigItem(waitRoom,config.GetValue(waitRoom))
                    dbClient
                    )
             .AddAuthentication("Identity.Application")
            .AddCookie("Identity.Application", fun options ->
                                options.LoginPath <- PathString("/login")
                                options.AccessDeniedPath <- PathString("/forbidden/")
            )
            // for signing out with signin manager
            // https://github.com/aspnet/Identity/issues/2082
            // to avoid, we should be able to call signout on httpcontext, but doesn't seem to be available
            .AddCookie("Identity.External", fun options -> ())
            .AddCookie("Identity.TwoFactorUserId", fun options -> ())
            .AddCookie("WebSharper")
            |> ignore

    member this.Configure(app: IApplicationBuilder, env: IWebHostEnvironment,  notifier:INotifier, config:IConfiguration,mailer:IMailer ) =
        if env.IsDevelopment() then
            app.UseDeveloperExceptionPage() |> ignore
            // only run Hangfire dashboard in development for now
            // requires caution if running in prod, regarding auth
            // opens 1 connection to the database
            app.UseHangfireDashboard() |> ignore
        if env.EnvironmentName.ToLower() = "test" then
            app.UseDeveloperExceptionPage() |> ignore

        |> ignore
        // adding log context
        app.UseSerilogRequestLogging(fun options ->
                                        options.EnrichDiagnosticContext <- ( fun (diagnosticContext:IDiagnosticContext) (httpContext:HttpContext) ->
                                                                                diagnosticContext.Set("RequestHost", httpContext.Request.Host.Value)
                                                                                diagnosticContext.Set("RequestScheme", httpContext.Request.Scheme)
                                                                                diagnosticContext.Set("ClientIP", httpContext.Connection.RemoteIpAddress)
                                        )
        ) |> ignore

        app.UseRequestLocalization() |> ignore

        let adminEmail = config.Item("myowndb:adminEmail")
        if env.IsProduction() then
          if isNull adminEmail then
            Log.Logger.Warning("Admin email is not set, no mail notification will be sent in case of uncaught exception. Set it in the file conf/myowndb.$env.json under key myowndb:adminEmail or environment variable MYOWNDB_myowndb__adminEmail")
          else
            app.UseExceptionHandler (fun handler ->
                handler.Run (fun context -> task{
                  let exceptionHandlerPathFeature =
                    context.Features.Get<Microsoft.AspNetCore.Diagnostics.IExceptionHandlerPathFeature>()
                  let req = context.Request
                  let exc = exceptionHandlerPathFeature.Error
                  let body =
                    $"""
Server: {Environment.MachineName}
Server time: {System.DateTime.Now}
Request path: {req.Path}

Request headers: {req.Headers.Keys |> Seq.fold (fun acc key -> sprintf "%s\n%s: %s" acc (key) (String.concat "," req.Headers[key])) ""}

Exception:
  {sprintf "%A" exc}
                    """
                  mailer.Send(adminEmail,"MyOwnDB Error Notification", body)
                  context.Response.StatusCode <- StatusCodes.Status500InternalServerError
                  context.Response.ContentType <- System.Net.Mime.MediaTypeNames.Text.Plain
                  do! context.Response.WriteAsync("An error occured and developers have been notified.");
                })
            ) |> ignore

        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Setup Notifiers
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // We need to retrieve the baseUrl from the config to use it in the notifier functions.
        // Otherwise the notifier cannot know which domain to link to as it doesn't handle a request itself.
        let baseUrl = config.Item("myowndb:baseUrl")
        if baseUrl = "" || isNull baseUrl then
          failwith "Missing config item myowndb::baseUrl in config/myowndb.$env.json or environemnt variable MYOWNDB_myowndb__baseUrl"
        if (baseUrl.EndsWith "/") then
          failwith "Config item myowndb::baseUrl is invalid, must not include trailing /"

        // Initilalise router to build links to endpoints. We do it outside the notifiers function to avoid
        // repeating this initialisation.
        let router = WebSharper.Sitelets.InferRouter.Router.Infer<EndPoint>()
        // Helper to build link to include in the notification.
        let getLinkToResource (sub:NotificationSubscription.NotificationSubscription) (sourceId:int) =
            match sub.source_type with
            | NotificationSubscriptionCriteria.Instance -> baseUrl + (router.Link (Instance(sourceId,None)))

        // Now we can set one notifier for each protocol
        NotificationSubscription.Notifier.setNotifier
          // +++ SMTP +++
          NotificationSubscriptionCriteria.Protocol.Smtp
          (fun (sub:NotificationSubscription.NotificationSubscription) sourceId ->
            // Construct the link to include in the notification
            let link = getLinkToResource sub sourceId
            // Create the Hangfire job to send the notification
            notifier.Send(sub,link)
          )

        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        // Setup authentication
        // ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
        app.UseAuthentication()
           .UseStaticFiles()
            .UseWebSharperEndpointRouting<EndPoint>()
            .Run(fun context ->
                context.Response.StatusCode <- 404
                context.Response.WriteAsync("Page not found"))

open System.IO
open System.Reflection
module Program =

    // We generate the configuration here from json files and environment.
    // We do this so we have the config available to apply Evolve migrations, and then
    // we add the config to the DI system.
    let generateConfig(args:string array) =
      let config:IConfigurationBuilder =
        ConfigurationBuilder()
          .SetBasePath(Directory.GetCurrentDirectory())
      // Take run environment from the variable environment, and use "production" as a default
      let env =
        let fromEnvironment = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")
        if isNull fromEnvironment then
          "production"
        else
          fromEnvironment

      config.AddJsonFile("conf/appsettings.json", optional= true, reloadOnChange= true)
            .AddJsonFile(sprintf "conf/appsettings.%s.json" (env.ToLower()),
                           optional= true, reloadOnChange= true)
            |> ignore

      config.AddJsonFile("conf/myowndb.defaults.json", optional= false, reloadOnChange= true)
            .AddJsonFile(sprintf "conf/myowndb.%s.json" (env.ToLower()),
                           optional= true, reloadOnChange= true)
            |> ignore

      // Look only at environment variables with the prefix MYOWNDB_
      // MYOWNDB_myowndb__fileAttachments__maxBytes will override key `myowndb:fileAttachments:maxBytes`.
      config.AddEnvironmentVariables("MYOWNDB_")  |> ignore

      if not (isNull args) then
          config.AddCommandLine(args) |> ignore
      config.Build()

    // Helper to setup Evolve and apply the migrations
    let applyMigrations(config:IConfigurationRoot) =
        // Prepare Evolve for db migrations
        let dbClient = MyOwnDB.WS.Database(config.GetValue("myowndb:database"), "").Get()
        let evolve = EvolveDb.Evolve(dbClient.Connection(), fun msg -> Log.Logger.Information msg)
        // If we find the directory migrations, use it, othrewise look for embedded resources
        if Directory.Exists "migrations" then
            Log.Logger.Information("Using directory migrations")
            evolve.Locations <- [|"migrations"|]
        else
            Log.Logger.Information("Using embedded migrations")
            evolve.EmbeddedResourceFilters <- [| "webws.migrations" |]
            evolve.EmbeddedResourceAssemblies <- [| Assembly.GetExecutingAssembly() |]
            evolve.IsEraseDisabled <- true

        let cmd = new Npgsql.NpgsqlCommand("select * from information_schema.tables  where table_name='instances'", dbClient.Connection())
        if cmd.Connection.State <> System.Data.ConnectionState.Open then
          cmd.Connection.Open()
        let reader = cmd.ExecuteReader()

        // Apply migrations
        // We start at migration 201 if the table instance already exists as earlier migrations are for creating the database schema
        // and basic data.
        // If the table instances does not exist, meaning we are using a new empty database, we don't set a first migration and
        // the first 2 migrations will also be applied
        if not (isNull reader) && reader.HasRows then
          Log.Logger.Information("Using an existing database")
          evolve.StartVersion <- (EvolveDb.Migration.MigrationVersion("201"))
        else
          Log.Logger.Information("Initialising a new database")

        // Close the database connection, so that evolve can open a new clean connection. It avoids
        // errors like "A command is already progress".
        cmd.Connection.Close()

        // Finally apply migrations
        evolve.Migrate()


    open NotificationSubscriptionCriteria
    let WebHostBuilder env args : IWebHostBuilder=

        // Get our configuration, so we can use it both for the asp net Startup, and for the Evolve migrations
        let appConfiguration = generateConfig(args)

        // Migrations are run by the code setting up the Playwright tests, do not attempt to apply them again
        // as it raises an error "the StartVersion parameter is not allowed when migrations have already been applied."
        // If we remove the StartVersion in applyMigrations, we might remove the check.
        if env <> "test" then
          applyMigrations appConfiguration

        WebHost
            .CreateDefaultBuilder(args)
            .ConfigureAppConfiguration(fun _context config ->
              config.Sources.Clear()
              // Add the configuration we built ourselves earlier
              config.AddConfiguration(appConfiguration) |> ignore
            )
            // Configure max request body size from config file
            // We configure both MultipartBodyLengthLimit (limiting the size of each file sent) and MaxRequestBodySize
            // (limiting the total request body size, incl file attachments)
            // Both are somewhat redundant, as a multipart upload also has a request body size.
            // As a user will have the opportunity to upload attachment one by one with subsequent updates of an instance,
            // it doesn't make much sense to control the MultipartBodyLengthLimit completely separated from the max request body size.
            // If we want to limit the size of an instance, it also requires control of already existing attachments.
            // NOTE: The server closes the connection with a 413 Payload Too Large, but firefox doesn't show it as such in the
            // dev console if it still wanted to send data (if all data was sent, it shows the 413 correctly). It works in Chromium though.
            .ConfigureKestrel(fun context o -> o.Limits.MaxRequestBodySize <- int64 (context.Configuration.Item("myowndb:requests:maxBytes"));() )
            .UseSerilog()
            .UseStartup<Startup>()

    let BuildWebHost env args =
        let builder = WebHostBuilder env args
        builder.Build()

    [<EntryPoint>]
    let main args =
        let env = let envVar = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT") in
                    if isNull envVar || envVar = "" then
                        "production"
                    else
                        envVar.ToLower()
        // Web application logs
        // The situation is that all MyOwnDB code uses the logger configured in MyOwnDBLib configure below, but
        // we still need to configure the Serilog logger for ASP.Net (IIUC).
        let logConfig = ConfigurationBuilder()
                            .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                            .AddJsonFile("conf/logs.defaults.json", optional= false)
                            .AddJsonFile(sprintf "conf/logs.%s.json" env, optional= true)
                            .AddEnvironmentVariables("MYOWNDB_")
                            .Build();

        Log.Logger <- LoggerConfiguration()
                        .ReadFrom.Configuration(logConfig)
                        .CreateLogger()

        // Now that the logger was created, log all log config keys to Debug level
        logConfig.AsEnumerable(false)
            |> Seq.fold (fun acc v -> sprintf "%s\n%A" acc v) "ASP logs Serilog Config values:\n"
            |> Log.Debug

        // Myowndb lib logs
        // lib logs are configured in a specific config file. This means that if we want
        // to change log level of the whole app, we need to do it in 2 locations :-(
        // I didn't find a better way
        let libLogConfig = ConfigurationBuilder()
                            .SetBasePath(System.IO.Directory.GetCurrentDirectory())
                            .AddJsonFile("conf/logs.lib.defaults.json", optional= false)
                            .AddJsonFile(sprintf "conf/logs.lib.%s.json" env, optional= true)
                            .AddEnvironmentVariables("MYOWNDB_")
                            .Build();
        let libLogger =  LoggerConfiguration()
                           .ReadFrom.Configuration(libLogConfig)
                           .CreateLogger()
        MyOwnDBLib.Logging.setLogger libLogger

        // Now that the logger was created, log all log config keys to Debug level
        logConfig.AsEnumerable(false)
            |> Seq.fold (fun acc v -> sprintf "%s\n%A" acc v) "MyOwnDBLib.Log Config values:\n"
            |> MyOwnDBLib.Logging.Log.Debug

        // try..with and try..finalyl are distinct construct, so need to nest
        // 2 calls to have both
        try
            try
                Log.Information("Application starting")
                (BuildWebHost env args).Run()
                0
            with
            | e -> Log.Fatal(e,"Host terminated")
                   1
        finally
            Log.CloseAndFlush()
