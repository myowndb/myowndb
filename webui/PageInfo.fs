namespace web
open WebSharper
open Microsoft.AspNetCore.Identity;
open web.ServerHelpers
open web.ServerHelpers.Remoting

// The PageInfo module defines one RPC function per page of the application. The RPC returns
// general information required to display the page, so that the same information doesn't have to be
// requested multiple times on the page. For example, on an entity page, the name of the entity will
// probably be used multiple times and needs to be retrieved from the server. This kind of info is
// put in the Info record type defined in each page-specific module.
[<JavaScript>]
module PageInfo =
  module Admin =
    module Entity =
      type PageInfo =
        {
          entity : Entity.Entity
          entitiesInDB : Entity.Entity list
        }

      [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
      let pageInfo (entityIdInt:int)= async {
         let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
         let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
         let client:DB.DBClient = getService<DB.DBClient>()
         let t:web.I18n = getService<web.I18n>()
         let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
         let accountId = Auth.getCurrentUserAccountId httpContext
         let! entityIdOption = GetId(signInManager).EntityId client (httpContext.User) entityIdInt
         match entityIdOption with
         // We don't have access to the entityId, stop here
         | None ->
             MyOwnDBLib.Logging.Log.Error($"Problem accessing entity id {entityIdInt} for account {accountId} in Admin.Entity PageInfo retrieval")
             return DBResult.error (t.t("An error occured"))
         | Some entityId ->
           return!
             EntityExtensions.byIdAction entityId
             |> DBAction.bind (fun e ->
               DBAction.queryAction "select * from entities where database_id = %d" (e.database_id)
                |> DBAction.mapError (fun e -> MyOwnDBLib.Logging.Log.Error($"Error retrieving entities in database for admin entity: {e|>Error.toString}"); e)
                |> DBAction.workOnList
                |> DBAction.map (fun (entitiesInDB:Entity.Entity list) ->
                  (e,entitiesInDB)
               )
             )
             |> DBAction.map (fun (e,entitiesInDB) -> {entity = e; entitiesInDB = entitiesInDB} )
             |> DBAction.run client

      }
    module Database =
      open Microsoft.Extensions.Configuration
      type PageInfo =
        {
          database : Database.Database
          publicFormDocumentationUrl : string option
        }

      [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
      let pageInfo (databaseIdInt:int)= async {
         let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
         let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
         let client:DB.DBClient = getService<DB.DBClient>()
         let t:web.I18n = getService<web.I18n>()
         let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
         let accountId = Auth.getCurrentUserAccountId httpContext
         let! o = GetId(signInManager).DatabaseId client (httpContext.User) databaseIdInt

         // Retrieve public form documentation url returned below
         let config = getService<IConfiguration>()
         let url = config.Item("myowndb:urls:publicFormDocumentation")

         return!
           o
           |> DBAction.fromOption
           |> DBAction.mapError (fun e ->
             MyOwnDBLib.Logging.Log.Error($"Problem accessing database id {databaseIdInt} for account {accountId} in Admin.Database PageInfo retrieval: {e}")
             e
           )
           |> DBAction.bind DatabaseExtensions.byIdAction
           |> DBAction.map (fun database ->
               {
                 database = database
                 publicFormDocumentationUrl = url |> Option.ofObj
               }
           )
           |> DBAction.run client
      }
  module Instances =
    type PageInfo =
      {
        entity : Entity.Entity
      }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let pageInfo (entityIdInt:int)= async {
       let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
       let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
       let client:DB.DBClient = getService<DB.DBClient>()
       let t:web.I18n = getService<web.I18n>()
       let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
       let accountId = Auth.getCurrentUserAccountId httpContext
       let! entityIdOption = GetId(signInManager).EntityId client (httpContext.User) entityIdInt
       match entityIdOption with
       // We don't have access to the entityId, stop here
       | None ->
           MyOwnDBLib.Logging.Log.Error($"Problem accessing entity id {entityIdInt} for account {accountId} in Instances PageInfo retrieval")
           return DBResult.error (t.t("An error occured"))
       | Some entityId ->
         return!
           EntityExtensions.byIdAction entityId
           |> DBAction.map (fun (e) -> {entity = e} )
           |> DBAction.run client

    }
  module Instance =
    type PageInfo =
      {
        entity : Entity.Entity
      }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let pageInfo (instanceIdInt:int)= async {
       let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
       let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
       let client:DB.DBClient = getService<DB.DBClient>()
       let t:web.I18n = getService<web.I18n>()
       let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
       let accountId = Auth.getCurrentUserAccountId httpContext
       let! instanceIdOption = GetId(signInManager).InstanceId client (httpContext.User) instanceIdInt
       match instanceIdOption with
       // We don't have access to the entityId, stop here
       | None ->
           MyOwnDBLib.Logging.Log.Error($"Problem accessing entity id {instanceIdInt} for account {accountId} in Instances PageInfo retrieval")
           return DBResult.error (t.t("An error occured"))
       | Some instanceId ->
         return!
           Instance.byIdAction instanceId
           |> DBAction.bind (fun i -> EntityExtensions.byIdAction i.entity_id)
           |> DBAction.map (fun (e) -> {entity = e} )
           |> DBAction.run client

    }
  module Import =
    type PageInfo =
      {
        entity : Entity.Entity
      }

    [<Rpc; RemotingProvider(typeof<WSExtensions.Remoting.RemotingWithActiveCounter>)>]
    let pageInfo (entityIdInt:int)= async {
       let ctx:WebSharper.Web.Context = WebSharper.Web.Remoting.GetContext()
       let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
       let client:DB.DBClient = getService<DB.DBClient>()
       let t:web.I18n = getService<web.I18n>()
       let signInManager:Microsoft.AspNetCore.Identity.SignInManager<ApplicationUser> = getService<SignInManager<ApplicationUser>>()
       let accountId = Auth.getCurrentUserAccountId httpContext
       let! entityIdOption = GetId(signInManager).EntityId client (httpContext.User) entityIdInt
       match entityIdOption with
       // We don't have access to the entityId, stop here
       | None ->
           MyOwnDBLib.Logging.Log.Error($"Problem accessing entity id {entityIdInt} for account {accountId} in Instances PageInfo retrieval")
           return DBResult.error (t.t("An error occured"))
       | Some entityId ->
         return!
           EntityExtensions.byIdAction entityId
           |> DBAction.map (fun (e) -> {entity = e} )
           |> DBAction.run client

    }
