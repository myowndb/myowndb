namespace web
open WebSharper

[<JavaScript>]
// Needed to install the client side routing in this specific module. Doing it in the module Routing led to an obscure error
// with some client side function being called server side. Putting it here avoids that error.
// It was moved here from ClientContent to make it available to the public form client code si it can redirect to an
// endpoint after a successful public form submission.
module ClientRouting =
    let installedRouter =
        Routing.router
        |> Sitelets.Router.Slice
            (fun endpoint ->
                match endpoint with
                | Home -> Some endpoint
                | Instances _ -> Some endpoint
                | Instance _ -> Some endpoint
                | EditInstance _ -> Some endpoint
                | CreateInstance _ -> Some endpoint
                | PublicForm _ -> Some endpoint
                | Import _ -> Some endpoint
                | Admin["users"] -> Some endpoint
                | Admin["databases"] -> Some endpoint
                | Admin["database";_databaseId] -> Some endpoint
                | Admin["entity";_entityId] -> Some endpoint
                | _ -> None
            )
            id
        |> WebSharper.UI.Router.Install Home
