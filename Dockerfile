# Dockerfile building the image to run tests with Gitlab CI/CD
# To build an image with the code copied to /app, run the docker build
# with --build-arg build_env=copy
# This is useful to run tests locally in a docker container
ARG build_env=nocopy

FROM mcr.microsoft.com/playwright:v1.31.1-focal as base
# signal if we are docker or not. Used to handle volume mounts in webui/Makefile
ENV IN_DOCKER=1
RUN mkdir myowndb
COPY deployment myowndb/deployment
RUN cd myowndb/deployment && ./setup_env.sh && ./migrate.sh && ./clean_env.sh
RUN rm -r myowndb
RUN wget https://packages.microsoft.com/config/debian/10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb \
    && sudo dpkg -i packages-microsoft-prod.deb \
    && sudo apt-get update \
    && rm -f packages-microsoft-prod.deb \
    && apt-get install -y dotnet-sdk-6.0 \
    && dotnet tool install --global Microsoft.Playwright.CLI \
    && dotnet tool install --global fantomas-tool --version 4.6.0-alpha-007
RUN curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg \
        && apt-get install -y curl gpg lsb-release \
        && echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" \
            > /etc/apt/sources.list.d/docker.list \
        && apt-get update \
        && apt-get install -y docker-ce docker-ce-cli containerd.io

# this is the image that will be used for the default value of build_env
FROM base AS image_nocopy

# this is the image that will be used when build_env=copy
FROM base AS image_copy
COPY . /app
WORKDIR /app

# Finally build the image by selecting a previous image based on the build_env value
FROM image_${build_env}
