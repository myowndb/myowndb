#!/bin/bash -eu

script_dir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )"
# go up one level to works on the migrations directory
cd "$script_dir/.."

../../../deployment/update_evolve_migrations.sh migrations
