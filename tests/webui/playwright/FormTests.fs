namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open MyOwndbForms
open System.Collections.Generic
open MyOwnDB.Tests.Helpers

// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// IMPORTANT:
// As tests create instances and go the the instances list to check their operations
// are reflected in the list view, and because click on attachments in the table are
// only targetting the filename, it is needed to use distinct attachment file names
// for different tests. Failing to do so can result in a PlayWright error complaining
// that more than one element was returned for the selector
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
[<TestFixture;Parallelizable>]
type DatabaseReadOnlyTests() =
    inherit PageTest()
    [<Test>]
    member self.``creation_form_display`` () = unitTask {
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")

        // view instances for entity id 12
        do! goToPathAsync(page, "/create_instance/100001")

        let formSpec:FieldSpec list = [
            {name="name"; value=""}
            {name="notes"; value=""}
            {name="birthdate"; value=""}
            {name="count"; value=""}
            {name="approved"; value="0"}
            {name="email"; value=""}
            {name="website"; value=""}
            {name="logo"; value=""}
            {name="picture"; value=""}
        ]
        do! checkForm page ("""div[data-form-entity-id="100001"]""") formSpec
        ()
    }
    [<Test>]
    member self.``form_fields_validations`` () = unitTask {
        let page = self.Page

        // Helper to check form field validations for entity 100001
        let checkFormValidations (page:IPage) = task {
            let formSpec:FieldSpec list = [
                {name="name"; value=""}
                {name="notes"; value=""}
                {name="birthdate"; value=""}
                {name="count"; value=""}
                {name="approved"; value="0"}
                {name="email"; value=""}
                {name="website"; value=""}
                {name="logo"; value=""}
                {name="picture"; value=""}
            ]
            do! checkForm page ("""div[data-form-entity-id="100001"]""") formSpec

            let ancestor = """div[data-form-entity-id="100001"]"""
            let xPathAncestor = """//div[@data-form-entity-id="100001"]"""
            let! fields = getFieldsMap page ancestor

            // Some helper defined by partial applications
            let save = page.Locator($"""{ancestor} button:text("Save")""").ClickAsync
            let fill = fillFieldFromMapWith fields
            let typeIn = typeInFieldFromMapWith fields
            let submitIsDisabled() = submitButtonIsDisabled page xPathAncestor
            let submitIsEnabled() = submitButtonIsEnabled page xPathAncestor

            // Type invalid values in subsequent fields
            do! typeIn "birthdate" "invalid date"
            do! fieldIsInvalid page "birthdate"
            do! fieldIsValid page "count"
            do! fieldIsValid page "email"
            do! fieldIsValid page "website"
            do! submitIsDisabled()

            do! typeIn "count" "invalid number"
            do! fieldIsInvalid page "birthdate"
            do! fieldIsInvalid page "count"
            do! fieldIsValid page "email"
            do! fieldIsValid page "website"
            do! submitIsDisabled()

            do! fill "email" "invalid email"
            do! fieldIsInvalid page "birthdate"
            do! fieldIsInvalid page "count"
            do! fieldIsInvalid page "email"
            do! fieldIsValid page "website"
            do! submitIsDisabled()

            do! fill "website" "invalid website"
            do! fieldIsInvalid page "birthdate"
            do! fieldIsInvalid page "count"
            do! fieldIsInvalid page "email"
            do! fieldIsInvalid page "website"
            do! submitIsDisabled()

            // Test files too big are immediately detected and a warning is displayed
            do! selectFileForFormField page "picture" "toobig.iso"
            do! fileFieldHasFileTooBigWarning page "picture"
            do! submitIsDisabled()
            // But selecting a file or an acceptable size removes the warning
            do! selectFileForFormField page "picture" "huge1.iso"
            do! fileFieldHasNoFileTooBigWarning page "picture"

            // Type valid values in subsequent fields
            do! fill "birthdate" ""
            do! typeIn "birthdate" "1973-02-15"
            do! fieldIsValid page "birthdate"
            do! fieldIsInvalid page "count"
            do! fieldIsInvalid page "email"
            do! fieldIsInvalid page "website"
            do! submitIsDisabled()

            do! fill "count" "32"
            do! fieldIsValid page "birthdate"
            do! fieldIsValid page "count"
            do! fieldIsInvalid page "email"
            do! fieldIsInvalid page "website"
            do! submitIsDisabled()

            do! fill "email" "valid@example.com"
            do! fieldIsValid page "birthdate"
            do! fieldIsValid page "count"
            do! fieldIsValid page "email"
            do! fieldIsInvalid page "website"
            do! submitIsDisabled()

            do! fill "website" "http://valid.example.com"
            do! fieldIsValid page "birthdate"
            do! fieldIsValid page "count"
            do! fieldIsValid page "email"
            do! fieldIsValid page "website"
            do! submitIsEnabled()
        }

        // Perform tests
        //--------------
        // Start with the public form without being authenticated
        do! goToPathAsync(page, "/public_form/100001")
        do! checkFormValidations page

        // Authenticate then test instance creation page
        do! authenticate(page,"existingbob@test.com")
        // view instances for entity id 100001
        do! goToPathAsync(page, "/create_instance/100001")
        do! checkFormValidations page
        ()
    }

    [<Test>]
    member self.``form_submit_cancellation`` () = unitTask {
        // This test focuses on the cacellation of update form submission.
        // It does so by uploading huge files to give the time to press cancel.
        // This test uses the huge*.iso and tara-may attachment fixtures

        // Function testing form submission cancellation. It is used for edition and
        // creation forms. The argument nameValue is the value of the field name when
        // first displaying the form. It is empty for the creation form, but not for the
        // edition form as the edition we will edit has a name value
        let testFormCancellation (page:IPage) (nameValue:string) = task {
            let ancestor = """div[data-form-entity-id="100001"]"""
            let xPathAncestor = """//div[@data-form-entity-id="100001"]"""
            // This also waits until the ancestor is visible
            do! elementVisible(page,ancestor)
            let! fields = getFieldsMap page ancestor

            // Some helper defined by partial applications
            let save ()= task {
                do! page.Locator($"""{ancestor} button:text("Save")""").ClickAsync()
            }
            let fill = fillFieldFromMapWith fields
            let setFile = setFileFieldFromMapWith fields
            let setOption = chooseOptionFieldFromMapWith fields
            let fieldHasValue = checkFieldHasValue page

            let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)
            let name = "Tara-May II"
            let notes = "Not a lot to note about Tara"
            let birthdate = "2005-05-23"
            let count = "42"
            let approved = "Yes"
            let email = "tara@cool.example.com"
            let website = "https://tara.tada"

            // Fill form
            do! fill "name" name
            do! fill "notes" notes
            do! fill "birthdate" birthdate
            do! fill "count" count
            let! _ = setOption "approved" approved
            do! fill "email" email
            do! fill "website" website
            do! selectFileForFormField page "logo" "tara-may-small.png"
            do! selectFileForFormField page "picture" "huge1.iso"

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client
                    []
                    DBState.zeroState

            // Save but cancel before files are uploaded
            do! save()
            do! Async.Sleep 300
            // We check there's no error reported regarding a payload too big. This is because this passed earlier with this error
            // reported but not detected. In that case, the cancellation was not actually tested!
            // That error was reported when the test code did not have request maxBytes correctly configured.
            do! elementNotVisible(page,$"""{xPathAncestor}//div[contains(@class,"alert-danger")]""")
            do! pressCancel page xPathAncestor

            // Check field has edited value. For creation form this validates the form
            // was not reset, because the submission was cancelled.
            do! fieldHasValue "name" "Tara-May II"

            // Check no update was done
            do! checkDifferenceNow ()
            do! client.Close()
            do! elementNotVisible(page,"""//div[contains(@class,"alert-success")]""")

            let! _response = page.ReloadAsync()
            do! fieldHasValue "name" nameValue
            do! fieldHasValue "notes" ""
            do! fieldHasValue "birthdate" ""
            do! fieldHasValue "count" ""
            do! fieldHasValue "approved" "0"
            do! fieldHasValue "email" ""
            do! fieldHasValue "website" ""
            do! fileFieldHasNoFileOnServer page "logo"
            do! fileFieldHasNoFileOnServer page "picture"
        }
        let page = self.Page

        // Public form
        do! goToPathAsync(page, "/public_form/100001")
        do! testFormCancellation page ""

        // Authenticate to access other forms
        do! authenticate(page,"existingbob@test.com")
        //--------------------------------------------------------------------------------
        // Edition form
        // Go to instances list
        do! goToPathAsync(page, "/instances/100001")
        // then click edit in the table
        let editLink = getActionCellForRowWithData page "Tara-May" "edit"
        do! editLink.ClickAsync()
        do! testFormCancellation page "Tara-May"

        //--------------------------------------------------------------------------------
        // Creation form
        do! goToPathAsync(page, "/create_instance/100001")
        do! testFormCancellation page ""
    }

    [<Test>]
    member self.``iframeresizer_load`` () = unitTask {
        let page = self.Page

        // Function to check if the resizer script is included for the parameter string
        let testWithParameter (page:IPage)(parameter:string)(resizeScriptIncluded:bool) = task {
          let count = if resizeScriptIncluded then 1 else 0
          let completeGetParams = if parameter.Length=0 then "" else $"?{parameter}"
          do! goToPathAsync(page, $"""/public_form/100001{completeGetParams}""")
          let resizerScript = page.Locator("""//script[contains(@src,"iframeResizer")]""")
          do! Assertions.Expect(resizerScript).ToHaveCountAsync(count)
        }

        // Only if responsive is "0" or "false" will the script not be included
        do! testWithParameter page "" true
        do! testWithParameter page "responsive=1" true
        do! testWithParameter page "responsive=true" true
        do! testWithParameter page "responsive=bla" true
        do! testWithParameter page "responsive=0" false
        do! testWithParameter page "responsive=false" false

        ()
    }

[<TestFixture;>]
type DatabaseAlteringTests() =
    inherit PageTest()
    [<Test>]
    member self.``valid_creation_form_submit`` () = unitTask {
        let page = self.Page


        // Check public form before authentication
        do! goToPathAsync(page, "/public_form/100001")
        let valuesPublic =
            {
                name = "Publica-Jon"
                notes = "Notes about Publica"
                birthdate = "1972-09-27"
                count = "942"
                approved = "Yes"
                email = "pub@pro.example.com"
                website = "http://tim.pro.example.com"
                logo = "pub-logo-small.png"
                picture = "pub-picture-medium.jpg"
            }
        do! fillSubmitAndValidatedFullForm100001 page valuesPublic false

        // authenticate to test in app forms
        do! authenticate(page,"existingbob@test.com")
        do! goToPathAsync(page, "/create_instance/100001")
        let values =
            {
                name = "Time-jon"
                notes = "Notes about Tim, which will take on this project for us."
                birthdate = "1952-01-31"
                count = "542"
                approved = "Yes"
                email = "tim@pro.example.com"
                website = "http://tim.pro.example.com"
                logo = "logo-small.png"
                picture = "picture-medium.jpg"
            }
        do! fillSubmitAndValidatedFullForm100001 page values true
        // Fill form
        // Submit creation form a second time to check a new entry is created
        let values2 = {
            name = "Timmy-jon"
            notes = "Notes about Tim2, brother of the 1."
            birthdate = "1954-01-31"
            count = "5422"
            approved = "No"
            email = "secondtim@pro.example.com"
            website = "http://tim.pro.example.com/2"
            logo = "logo-creation2-medium.png"
            picture = "picture-creation2-small.jpg"
        }
        do! fillSubmitAndValidatedFullForm100001 page values2 true


        do! fillSubmitAndValidatePartial10001 page ("Bruno-Bob")

        // Go to list view and check new instance is present
        do! goToPathAsync(page, "/instances/100001")
        // We have possibly 11 entries later in the test, so display more entries per page to be sure we have all rows
        do! setPerPage page "#table_main_entity_100001" 20


        // Check row inserted by public form
        do! checkFormEntryInTable page values2
        // Check row of name Time-jon
        do! checkFormEntryInTable page values
        // Check row of name Timmy-jon
        do! checkFormEntryInTable page values2

        do! elementVisible(page,$"""//div[@id="table_main_entity_100001"]//td[text()="Bruno-Bob"]""")

        ()
    }

    [<Test>]
    member self.``creation_form_submit_with_empty_fields`` () = unitTask {
        let page = self.Page

        // Go to public form to be able to extract fields
        do! goToPathAsync(page, "/public_form/100001")
        let ancestor = """div[data-form-entity-id="100001"]"""
        let! fields = getFieldsMap page ancestor

        // Some helper defined by partial applications
        let save = page.Locator($"""{ancestor} button:text("Save")""").ClickAsync
        let fill = fillFieldFromMapWith fields
        let setFile = setFileFieldFromMapWith fields
        let setOption = chooseOptionFieldFromMapWith fields
        let fieldHasValue = checkFieldHasValue page


        // Initialise db state checker
        let expectedStateDifference =
            { DBState.zeroState with
                  instances = 1L
                  maxInstanceId = 1
                  detailValues = 1L
                  maxDetailValueId = 1
            }

        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

        // Test public form
        let! preState, checkDifferenceNow =
            DBState.startWithCustomQueries client
                [ 1L, "select count(*) from detail_values where type='SimpleDetailValue'"
                  0L, "select count(*) from detail_values where type='LongTextDetailValue'"
                  0L, "select count(*) from detail_values where type='FileAttachmentDetailValue'"
                  0L, "select count(*) from detail_values where type='EmailDetailValue'"
                  0L, "select count(*) from detail_values where type='WebUrlDetailValue'"
                ]
                expectedStateDifference
        do! goToPathAsync(page, "/public_form/100001")

        let name = "Public-jan"
        // Fill form
        do! fill "name" name

        // Submit it
        do! save()
        do! textIsVisible(page,"Your form has been successfully registered")

        do! checkDifferenceNow ()



        // Authenticate and do in app tests
        do! authenticate(page,"existingbob@test.com")

        let! preState, checkDifferenceNow =
            DBState.startWithCustomQueries client
                [ 1L, "select count(*) from detail_values where type='SimpleDetailValue'"
                  0L, "select count(*) from detail_values where type='LongTextDetailValue'"
                  0L, "select count(*) from detail_values where type='FileAttachmentDetailValue'"
                  0L, "select count(*) from detail_values where type='EmailDetailValue'"
                  0L, "select count(*) from detail_values where type='WebUrlDetailValue'"
                ]
                expectedStateDifference

        // view instances for entity id 12
        do! goToPathAsync(page, "/create_instance/100001")

        let name = "Timmy-jan"

        // Fill form
        do! fill "name" name

        // Submit it
        do! save()
        do! textIsVisible(page,"Saved successfully")

        // Check change overview is displayed
        do! isInOverviewWithValue page "name" name
        do! isMarkedLeftEmptyInOverview page "notes"
        do! isMarkedLeftEmptyInOverview page "birthdate"
        do! isMarkedLeftEmptyInOverview page "count"
        do! isMarkedLeftEmptyInOverview page "approved"
        do! isMarkedLeftEmptyInOverview page "email"
        do! isMarkedLeftEmptyInOverview page "website"
        do! isMarkedLeftEmptyInOverview page "logo"
        do! isMarkedLeftEmptyInOverview page "picture"

        do! checkDifferenceNow ()

        // Submit creation form a second time to check a new entry is created
        let! preState, checkDifferenceNow =
            DBState.startWithCustomQueries client
                [ 1L, "select count(*) from detail_values where type='SimpleDetailValue'"
                  0L, "select count(*) from detail_values where type='LongTextDetailValue'"
                  0L, "select count(*) from detail_values where type='FileAttachmentDetailValue'"
                  0L, "select count(*) from detail_values where type='EmailDetailValue'"
                  0L, "select count(*) from detail_values where type='WebUrlDetailValue'"
                ]
                expectedStateDifference
        // Fill form
        do! fill "name" (name+"2")

        // Submit it
        do! save()
        // Introduce a small sleep so that the feedback from the first save is replaced
        // by the feedback of the second save.
        do! Async.Sleep 100
        do! textIsVisible(page,"Saved successfully")

        do! checkDifferenceNow ()

        do! client.Close()
        ()
    }

    [<Test>]
    member self.``edition_form_for_instance_with_values_for_all_details`` () = unitTask {
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")

        // Go to instances list
        do! goToPathAsync(page, "/instances/100001")
        // then click edit in the table
        let editLink = getActionCellForRowWithData page "Tom-Jim" "edit"
        do! editLink.ClickAsync()

        let ancestor = """div[data-form-entity-id="100001"]"""
        // This also waits until the ancestor is visible
        do! elementVisible(page,ancestor)
        let! fields = getFieldsMap page ancestor

        // Some helper defined by partial applications
        let save ()= task {
            do! page.Locator($"""{ancestor} button:text("Save")""").ClickAsync()
        }
        let fill = fillFieldFromMapWith fields
        let setFile = setFileFieldFromMapWith fields
        let setOption = chooseOptionFieldFromMapWith fields
        let fieldHasValue = checkFieldHasValue page

        // Check the edition form has the expected values
        // ----------------------------------------------
        do! fieldHasValue "name" "Tom-Jim"
        do! fieldHasValue "note" "This is the note about Tom-Jim"
        do! fieldHasValue "birthdate" "1981-01-31 00:00:00"
        do! fieldHasValue "count" "42"
        // 2 is Yes
        do! fieldHasValue "approved" "1"
        do! fieldHasValue "email" "Tom-Jim@email.example.com"
        do! fieldHasValue "website" "https://Tom-Jim.web.example.com"
        do! fileFieldHasFileOnServer page "logo" "tim-jon-logo-ubuntu.jpg"
        do! fileFieldHasFileOnServer page "picture" "tim-jon-picture-dedomenon.jpg"

        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)
        // We don't expect db differences as we only update and don't delete to create
        let! preState, checkDifferenceNow =
            DBState.startWithCustomQueries client [] DBState.zeroState

        // We now update non-file attachment fields one by one
        // ---------------------------------------------------
        // update SimpleText
        do! fill "name" "Tom-Jim Jr"
        do! save()
        do! textIsVisible(page,"Saved successfully")
        do! isInOverviewWithValue page "name" "Tom-Jim Jr"
        do! isUnchangedInOverview page "notes"
        do! isUnchangedInOverview page "birthdate"
        do! isUnchangedInOverview page "count"
        do! isUnchangedInOverview page "approved"
        do! isUnchangedInOverview page "email"
        do! isUnchangedInOverview page "website"
        do! isUnchangedInOverview page "logo"
        do! isUnchangedInOverview page "picture"
        // update LongText
        do! fill "notes" "Updated notes for Tom-Jim Jr"
        do! save()
        do! textIsVisible(page,"Saved successfully")
        do! isInOverviewWithValue page "notes" "Updated notes for Tom-Jim Jr"
        do! isUnchangedInOverview page "name"
        do! isUnchangedInOverview page "birthdate"
        do! isUnchangedInOverview page "count"
        do! isUnchangedInOverview page "approved"
        do! isUnchangedInOverview page "email"
        do! isUnchangedInOverview page "website"
        do! isUnchangedInOverview page "logo"
        do! isUnchangedInOverview page "picture"
        // update Date
        do! fill "birthdate" "1982-03-31"
        do! save()
        do! textIsVisible(page,"Saved successfully")
        do! isInOverviewWithValue page "birthdate" "1982-03-31 00:00:00"
        do! isUnchangedInOverview page "name"
        do! isUnchangedInOverview page "count"
        do! isUnchangedInOverview page "approved"
        do! isUnchangedInOverview page "email"
        do! isUnchangedInOverview page "website"
        do! isUnchangedInOverview page "logo"
        do! isUnchangedInOverview page "picture"
        // update Count
        do! fill "count" "84"
        do! save()
        do! textIsVisible(page,"Saved successfully")
        do! isInOverviewWithValue page "count" "84"
        do! isUnchangedInOverview page "name"
        do! isUnchangedInOverview page "notes"
        do! isUnchangedInOverview page "birthdate"
        do! isUnchangedInOverview page "approved"
        do! isUnchangedInOverview page "email"
        do! isUnchangedInOverview page "website"
        do! isUnchangedInOverview page "logo"
        do! isUnchangedInOverview page "picture"

        // update Approved
        let! _ = setOption "approved" "No"
        do! save()
        do! textIsVisible(page,"Saved successfully")
        do! isInOverviewWithValue page "approved" "No"
        do! isUnchangedInOverview page "name"
        do! isUnchangedInOverview page "notes"
        do! isUnchangedInOverview page "birthdate"
        do! isUnchangedInOverview page "count"
        do! isUnchangedInOverview page "email"
        do! isUnchangedInOverview page "website"
        do! isUnchangedInOverview page "logo"
        do! isUnchangedInOverview page "picture"
        // update email
        do! fill "email" "new@tim.jon.example.com"
        do! save()
        do! textIsVisible(page,"Saved successfully")
        do! isInOverviewWithValue page "email" "new@tim.jon.example.com"
        do! isUnchangedInOverview page "name"
        do! isUnchangedInOverview page "notes"
        do! isUnchangedInOverview page "birthdate"
        do! isUnchangedInOverview page "count"
        do! isUnchangedInOverview page "approved"
        do! isUnchangedInOverview page "website"
        do! isUnchangedInOverview page "logo"
        do! isUnchangedInOverview page "picture"
        // update website
        do! fill "website" "https://example.com/tim-jon"
        do! save()
        do! textIsVisible(page,"Saved successfully")
        do! isInOverviewWithValue page "website" "https://example.com/tim-jon"
        do! isUnchangedInOverview page "name"
        do! isUnchangedInOverview page "notes"
        do! isUnchangedInOverview page "birthdate"
        do! isUnchangedInOverview page "count"
        do! isUnchangedInOverview page "approved"
        do! isUnchangedInOverview page "email"
        do! isUnchangedInOverview page "logo"
        do! isUnchangedInOverview page "picture"
        // update multiple fields
        let! _ = setOption "approved" "Yes"
        do! fill "website" "https://example.com/tim-jon/index.html"
        do! fill "count" "85"
        do! save()
        do! textIsVisible(page,"Saved successfully")
        do! isInOverviewWithValue page "website" "https://example.com/tim-jon/index.html"
        do! isInOverviewWithValue page "count" "85"
        do! isInOverviewWithValue page "approved" "Yes"
        do! isUnchangedInOverview page "name"
        do! isUnchangedInOverview page "notes"
        do! isUnchangedInOverview page "birthdate"
        do! isUnchangedInOverview page "email"
        do! isUnchangedInOverview page "logo"
        do! isUnchangedInOverview page "picture"

        // Let's start to request a deletion but change our mind
        //-------------------------------------------------------
        do! clickDustBinOfAttachment page "picture"
        // Check the other file does not have the delete confirmation
        do! attachmentDeleteConfirmationNotVisible page "logo"
        do! cancelDeletion page "picture"
        do! attachmentDeleteConfirmationNotVisible page "picture"
        do! fileFieldHasFileOnServer page "picture" "tim-jon-picture-dedomenon.jpg"

        // Check not additional entry was created in the database as we only updated
        do! checkDifferenceNow ()

        // This is an helper for each db state validation for detail_values of type FileAttachment
        let startAttachmentDetailValuesDBState (difference:int) (maxIdDifference:int) =
            DBState.startWithCustomQueries
                client
                [ int64 difference , "select count(*) from detail_values where type='FileAttachmentDetailValue'" ]
                { DBState.zeroState with
                    detailValues = difference
                    maxDetailValueId = maxIdDifference
                }

        // Prepare db check for on detail_value deleted when deleting a file attachment
        let! preState, checkDifferenceNow = startAttachmentDetailValuesDBState -1 0

        // Let's delete the logo
        // ---------------------
        do! clickDustBinOfAttachment page "logo"
        // Check the other file field does not display the confirmation
        do! attachmentDeleteConfirmationNotVisible page "picture"
        do! confirmDeletion page "logo"
        do! attachmentDeleteConfirmationNotVisible page "logo"
        do! fileFieldHasNoFileOnServer page "logo"
        // Check other file was left untouched
        do! fileFieldHasFileOnServer page "picture" "tim-jon-picture-dedomenon.jpg"

        // Check we deleted on detail_value
        do! checkDifferenceNow ()

        // Select a file but then reset it
        // -----------------
        let! preState, checkDifferenceNow = startAttachmentDetailValuesDBState 0 0

        do! selectFileForFormField page "logo" "logo-medium.png"
        do! resetChosenFile page "logo"
        do! save()
        do! isEmptyInOverview page "logo"
        do! fileFieldHasNoFileOnServer page "logo"

        do! checkDifferenceNow()


        // Attach a new file
        // -----------------
        let! preState, checkDifferenceNow = startAttachmentDetailValuesDBState 1 1

        do! selectFileForFormField page "logo" "logo-medium.png"
        do! save()
        do! isInOverviewWithValue page "logo" "logo-medium.png"
        do! fileFieldHasFileOnServer page "logo" "logo-medium.png"

        do! checkDifferenceNow()

        do! client.Close()
        ()
    }

    [<Test;Retry(3)>]
    member self.``edition_form_files_updates`` () = unitTask {
        // This test focuses on the behaviour of the file attachment fields. Those fields have a more complex
        // behaviour than others, and required special care during development. This should ensure we don't
        // break its behaviour.
        // This test uses the tana-jane attachment fixtures
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")

        // Go to instances list
        do! goToPathAsync(page, "/instances/100001")
        // then click edit in the table
        let editLink = getActionCellForRowWithData page "Tana-Jane" "edit"
        do! editLink.ClickAsync()

        let ancestor = """div[data-form-entity-id="100001"]"""
        // This also waits until the ancestor is visible
        do! elementVisible(page,ancestor)
        let! fields = getFieldsMap page ancestor

        // Some helper defined by partial applications
        let save ()= task {
            do! page.Locator($"""{ancestor} button:text("Save")""").ClickAsync()
            // Add delay after save. This is needed for multiple subsequent form submissions on one page without
            // reload because the action following the save would work on the element of the previous form, which
            // is then replaced. The following action would then work on the new form elements, introducing incoherency.
            // In this test, the problem is that after the save, the dustbin of a file attachment was clicked, the confirmation
            // dialog was displayed, but at that time the form element is updated after the save. This new element didn't have
            // the confirmation open, and so waiting for the confirmation dialog timed out. Setting the sleep to 200ms make the
            // test pass reliably.
            do! Async.Sleep 200
        }
        let fill = fillFieldFromMapWith fields
        let setFile = setFileFieldFromMapWith fields
        let setOption = chooseOptionFieldFromMapWith fields
        let fieldHasValue = checkFieldHasValue page


        // Check we start from the expected state
        do! fileFieldHasNoFileOnServer page "logo"
        do! fileFieldHasNoFileOnServer page "picture"

        // Selecting a file in one field works and has no impact on the other file attachments
        do! selectFileForFormField page "logo" "logo-tana-jane-small.png"
        do! fileFieldHasSelection page "logo"
        do! fileFieldHasNoSelection page "picture"

        // Selecting a file in the other field works and has no impact on previous file attachments
        do! selectFileForFormField page "picture" "tana-jane-small.jpg"
        do! fileFieldHasSelection page "logo"
        do! fileFieldHasSelection page "picture"

        // Resetting selection of a file field works and has no impact on other fields
        do! resetChosenFile page "picture"
        do! fileFieldHasNoSelection page "picture"
        do! fileFieldHasSelection page "logo"

        do! save()
        do! isInOverviewWithValue page "logo" "logo-tana-jane-small.png"
        do! isEmptyInOverview page "picture"

        let repeatFileUpdates page = task {
            // Delete a file and attach a new one. No change on other attachment fields.
            // We repeat as this caused trouble when initially developing the feature
            // We map to async{} and not task{} because with the latter the FileChooser event
            // never fired
            do! [1..4]
                |> List.map (fun i -> async {
                    try
                        // Alternate suffix so we are sure the overview corresponds to this round
                        let suffix = if i%2 = 0 then "small" else "medium"
                        do! deleteAttachment page ("logo") |> Async.AwaitTask
                        do! selectFileForFormField page "logo" $"logo-tana-jane-{suffix}.png" |> Async.AwaitTask
                        do! save() |> Async.AwaitTask
                        do! isInOverviewWithValue page "logo" $"logo-tana-jane-{suffix}.png" |> Async.AwaitTask
                        // Not action took place on the picture field, so it is correctly displayed as unchanged
                        do! isUnchangedInOverview page "picture" |> Async.AwaitTask
                    with
                    |e ->
                        let newE = new System.Exception($"Error at repetition {i} selecting a logo",e)
                        raise  newE
                })
                |> Async.Sequential
                |> Async.Ignore

            // Delete a file and attach a new one. Other attachment field is selected and reset.
            // We repeat as this caused trouble when initially developing the feature
            do! [1..4]
                |> List.map (fun i -> async {
                    try
                    // Alternate suffix so we are sure the overview corresponds to this round
                        let suffix = if i%2 = 0 then "small" else "medium"
                        do! deleteAttachment page ("logo") |> Async.AwaitTask
                        do! selectFileForFormField page "picture" $"logo-tana-jane-{suffix}.png" |> Async.AwaitTask
                        do! resetChosenFile page "picture" |> Async.AwaitTask
                        do! selectFileForFormField page "logo" $"logo-tana-jane-{suffix}.png" |> Async.AwaitTask
                        do! save() |> Async.AwaitTask
                        do! isInOverviewWithValue page "logo" $"logo-tana-jane-{suffix}.png" |> Async.AwaitTask
                        // As we selected and reset the file for picture, it is displayed as empty
                        do! isEmptyInOverview page "picture" |> Async.AwaitTask
                    with
                    |e -> raise (System.Exception($"Error at repetition {i} selecting a logo",e))
                })
                |> Async.Sequential
                |> Async.Ignore

            // Delete a file and attach a new one. Do it for 2 file fields in form.
            // We repeat as this caused trouble when initially developing the feature
            // Start by attaching a picture so we have an attachment for each file field
            do! selectFileForFormField page "picture" $"tana-jane-small.jpg" |> Async.AwaitTask
            do! save()
            do! [1..4]
                |> List.map (fun i -> async {
                    // Alternate suffix so we are sure the overview corresponds to this round
                    let suffix = if i%2 = 0 then "small" else "medium"
                    do! deleteAttachment page ("logo") |> Async.AwaitTask
                    do! deleteAttachment page ("picture") |> Async.AwaitTask
                    do! selectFileForFormField page "picture" $"tana-jane-{suffix}.jpg" |> Async.AwaitTask
                    do! selectFileForFormField page "logo" $"logo-tana-jane-{suffix}.png" |> Async.AwaitTask
                    do! save() |> Async.AwaitTask
                    do! isInOverviewWithValue page "picture" $"tana-jane-{suffix}.jpg" |> Async.AwaitTask
                    do! isInOverviewWithValue page "logo" $"logo-tana-jane-{suffix}.png" |> Async.AwaitTask
                })
                |> Async.Sequential
                |> Async.Ignore
            // Delete picture so we have againt the same state with a logo attached but no picture
            do! deleteAttachment page ("picture") |> Async.AwaitTask
        }

        // Execute repeated file updates tests
        try
            do! repeatFileUpdates page
        with
        |e ->
          do! screenshotAsync(page,$"""screenshot-{System.DateTime.Now.ToFileTime()}.png""")
          raise (System.Exception($"Error at first call to repeatFileUpdates",e))

        // Refresh the page an re-run the repeated file updates tests.
        // This ensures that it also works when a file attachment was present at first load of the page
        let! _response =  page.ReloadAsync()
        try
            do! repeatFileUpdates page
        with
        |e ->
          do! screenshotAsync(page,$"""screenshot-{System.DateTime.Now.ToFileTime()}.png""")
          raise (System.Exception($"Error at second call to repeatFileUpdates",e))


    }

    [<Test>]
    member self.``datatable_creation_form_submit`` () = unitTask {
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")
        let formAncestor = """div[data-form-entity-id="100001"]"""
        let tableAncestor = """div[id="table_main_entity_100001"]"""
        let tableXPathAncestor = """//div[@id="table_main_entity_100001"]"""

        // view instances for entity id 12
        do! goToPathAsync(page, "/instances/100001")

        do! page.GetByText("Add new").ClickAsync()
        do! elementVisible(page,formAncestor)


        let values =
            {
                name = "Jon-Mark"
                notes = "Notes about Jon-Mark, though there's not so much to say"
                birthdate = "1982-04-24"
                count = "64"
                approved = "Yes"
                email = "jon-mark@pro.example.com"
                website = "http://jon-mark.pro.example.com"
                logo = "logo-jon-mark-small.png"
                picture = "picture-jon-mark-medium.jpg"
            }

        // First insertion
        let! checkRowInserted = getRowsDifferenceChecker page tableXPathAncestor 1
        do! fillSubmitAndValidatedFullForm100001 page values true
        do! checkFormEntryInTable page values
        do! checkRowInserted ()

        // Fill form
        // Submit creation form a second time to check a new entry is created
        let values2 = {
            name = "Jose-Luis"
            notes = "Not many notes about Jose-Luis"
            birthdate = "1994-11-28"
            count = "584"
            approved = "No"
            email = "jose-luis@pro.example.com"
            website = "http://jose-luis.pro.example.com/2"
            logo = "logo-jose-luis-medium.png"
            picture = "picture-jose-luis-small.jpg"
        }
        // Second insertion
        let! checkRowInserted = getRowsDifferenceChecker page tableXPathAncestor 1
        do! fillSubmitAndValidatedFullForm100001 page values2 true
        do! checkFormEntryInTable page values2
        do! checkRowInserted ()

        // Local helper to insert instance with only name filled, and check results
        let insertNameOnlyAndCheckResult (name:string) = task {
            let! checkRowInserted = getRowsDifferenceChecker page tableXPathAncestor 1
            do! fillSubmitAndValidatePartial10001 page (name)
            do! elementVisible(page,$"""{tableXPathAncestor}//td[text()="{name}"]""")
            do! checkRowInserted ()
        }

        // Third insertion, with empty fields
        do! insertNameOnlyAndCheckResult "Maria"

        // Filter datatable and check number of rows
        do! filterTable page tableAncestor "name" "ia"
        let! rows = getNumberOfRowsDisplayed page tableXPathAncestor
        rows |> should equal 1
        // Check filter is kept after update due to an insertion
        do! insertNameOnlyAndCheckResult "Mia"
        let! rows = getNumberOfRowsDisplayed page tableXPathAncestor
        rows |> should equal 2

        // Check an instance filtered out is not displayed. The helper sill checks
        // it was added to the database though.
        do! fillSubmitAndValidatePartial10001 page "Mario"
        let! rows = getNumberOfRowsDisplayed page tableXPathAncestor
        rows |> should equal 2
        ()
    }
