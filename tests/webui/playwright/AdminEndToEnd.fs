namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open System.Collections.Generic
open MyOwndbForms
open FSharpPlus
open MyOwnDB.Tests.Helpers
open AdminHelpers


type AdminEndToEnd() =
    inherit PlaywrightTest()
    // Checking no error occurs where creating a database and its entities from scratch.
    [<Test>]
    member self.``admin_end_to_end`` () = unitTask {
        let pw = self.Playwright
        let! adminBrowser = pw.Chromium.LaunchAsync()
        let! adminPage = adminBrowser.NewPageAsync()
        //let! userBrowser = pw.Chromium.LaunchAsync()
        //let! userPage = userBrowser.NewPageAsync()


        let tableXPath = """//table[@id="admin_databases_table"]"""
        let takeAction =
            clickActionCell adminPage tableXPath
        let formSelector = """//form[@id="admin_databases_form"]"""
        let create() =
            adminPage.Locator("""button:text("Create")""").ClickAsync()
        let save() =
            adminPage.Locator("""button:text("Save")""").ClickAsync()
        // database client for db content checks
        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)



        do! authenticate(adminPage,"admin@test.com")
        do! checkMenusInLanguageAsync(adminPage,adminMenu,"en")
        do! goToPathAsync(adminPage, "/admin/databases")


        // Add new database
        // ------------
        // We create a standard user
        let editedDatabase="New database from scratch"
        do! adminPage.Locator("""button:text("Add new")""").ClickAsync()
        do! textIsNotVisible(adminPage,"Database saved successfully")
        do! elementVisible(adminPage,formSelector)
        do! adminPage.GetByLabel("Name").FillAsync(editedDatabase)
        do! create()
        do! textIsVisible(adminPage,"Database saved successfully")
        do! Async.Sleep 200

        // Check that the admin page for the new database is displayed successfully
        do! clickActionCell adminPage """//table[@id="admin_databases_table"]""" editedDatabase "view"
        do! elementVisible(adminPage, """//table[@id="admin_entities_table"]""")

        // Add new entity to new database
        // ------------------------------
        let formSelector = """//form[@id="admin_entities_form"]"""
        let editedEntity="New entity"
        do! adminPage.GetByRole(AriaRole.Button, PageGetByRoleOptions(Name = "Add new", Exact = true)).ClickAsync()
        do! textIsNotVisible(adminPage,"Entity saved successfully")
        do! elementVisible(adminPage,formSelector)
        do! adminPage.GetByLabel("Name", PageGetByLabelOptions(Exact=true)).FillAsync(editedEntity)
        do! save()
        do! textIsNotVisible(adminPage,"An error occured")
        do! textIsVisible(adminPage,"Entity saved successfully")
        // Check that the admin page for the new entity is displayed successfully
        do! clickActionCell adminPage """//table[@id="admin_entities_table"]""" editedEntity "view"
        do! elementVisible(adminPage, """//h1[text()="Relations to parents"]""")

        // Add a new detail to new entity
        // ------------------------------
        do! adminPage.Locator("""button:text(" Add field based on new detail")""").ClickAsync()
        do! adminPage.GetByLabel("Detail name").FillAsync("Name")
        let! _request = adminPage.RunAndWaitForRequestFinishedAsync(fun () -> task {
                          do! adminPage.ClickAsync("#submitNewEntityDetail")
                        }) |> Async.AwaitTask
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        // Check new detail was added
        do! textIsNotVisible(adminPage,"An error occured")
        do! elementVisible(adminPage,"""//ul[@id="entityDetails"]/li""")
        do! textIsVisible(adminPage,"Name")

        // Add a new Relation to self
        // --------------------------
        // From AdminEntity.fs
        let toParentsFormLocator = """//div[@id="relationFormToParents"]"""
        let toParentField (name:string) =
          $"""{toParentsFormLocator}//*[@id="{name}"]"""
        let submitParentForm () = task {
          do! adminPage.Locator($"""{toParentsFormLocator}//button[text()="Submit"]""").ClickAsync()
        }
        do! adminPage.ClickAsync("""//button[@id="toggleRelationFormToParents"]""")
        // Choose target, set names and leave arities at default values
        do! adminPage.Locator(toParentField "parentToChildName").FillAsync("is father of")
        do! adminPage.Locator(toParentField "childToParentName").FillAsync("is child of")
        do! chooseDdlValue adminPage (toParentField "childSideType") "No"
        do! submitParentForm()
        // Check the relation is displayed in the table
        do! elementVisible(adminPage,"""//td[text()="is father of"]""")

        // Add instances
        // Update sidebar
        do! reload adminPage
        // Open new database entities list in sidebar
        do! adminPage.ClickAsync($"""//a[contains(text(),"{editedDatabase}")]""")
        // Open the new entity page
        do! adminPage.ClickAsync("""//a[contains(text(),"New entity")]""")
        do! adminPage.ClickAsync("""//button[text()="Add new"]""")
        let createInstance (name:string)= task {
          do! adminPage.Locator("""//input[starts-with(@id,"Name-")]""").FillAsync(name)
          do! adminPage.ClickAsync("""//button[text()="Save"]""")
          do! elementVisible(adminPage,$"""//td[contains(@class,"myowndbdatacell") and text()="{name}"]""")
        }
        do! createInstance "Jon"
        do! createInstance "Jon Jr"

        // Visit instance Jo and link "Jo Jr"
        // This was added at preview deployment because a relation self referencing an entity caused trouble.
        do! clickActionCell adminPage """//div[contains(@id,"table_main_entity")]""" "Jon" "view"
        // This matches 2 buttons, and uses the first. We don't care in this case as we just want to see if linking works
        // and we will only check if the linked instances appears in a linked table. More precise tests are done elsewhere.
        do! adminPage.ClickAsync("""//button[contains(text(),"Link existing entry")]""")
        // As a safety measure, check "Jon Jr" is not yet in a table of linked instances.
        do! elementNotVisible(adminPage,"""//div[contains(@id,"table_linked_")]//td[text()="Jon Jr"]""")
        do! clickActionCell adminPage """//div[contains(@id,"table_linkable_")]""" "Jon Jr" "link"
        // If the linking was successful, "Jon Jr" should be in a table of linked instances.
        do! elementVisible(adminPage,"""//div[contains(@id,"table_linked_")]//td[text()="Jon Jr"]""")


    }
