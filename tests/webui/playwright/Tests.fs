namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open System.Collections.Generic
open MyOwndbForms
open FSharpPlus
open MyOwnDB.Tests.Helpers
open System.Text.RegularExpressions


[<TestFixture;Parallelizable>]
type LanguageSelection() =
    inherit PageTest()
    [<Test>]
    member self.``language_selection_in_ddl`` () = unitTask {
        let page = self.Page
        let! result = page.GotoAsync($"{BaseUrl}/about")
        result.Status |> should equal 200
        page.Url |> should equal $"{BaseUrl}/about"
        let! lang = getLanguageAsync(page)
        lang |> should equal "en"
        let! t = getPageTranslatorAsync(page)
        t.t("MyOwnDB") |> should equal "MyOwnDB"
        do! checkMenusInLanguageAsync(page,publicMenu, "en")

        // now we check the menus in french
        let! _ = switchToFrenchAsync(page)
        let! lang = getLanguageAsync(page)
        lang |> should equal "fr"
        do! checkMenusInLanguageAsync(page,publicMenu,"fr")
        ()
    }

[<TestFixture;Parallelizable>]
type LanguageSelectionFromHeaders() =
    inherit BrowserTest()
    [<Test>]
    member self.``language_selection_in_header`` () = unitTask {
        let! page = self.Browser.NewPageAsync(BrowserNewPageOptions(ExtraHTTPHeaders=[| KeyValuePair("Accept-Language","fr")|]))
        let! result = page.GotoAsync($"{BaseUrl}/login")
        result.Status |> should equal 200
        // check we were not redirected to another URL
        page.Url |> should equal $"{BaseUrl}/login"
        // check menus are in french
        do! checkMenusInLanguageAsync(page,publicMenu, "fr")
        // check the language selection ddl has correct value selected
        let! languageDdlValue = page.EvalOnSelectorAsync("#languageSelect", "el => el.value" )
        languageDdlValue.Value.ToString()|> should equal "fr"

        // check the cookie overrides the default value from the header
        let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task {
            let! _list = page.SelectOptionAsync("#languageSelect", "en")
            ()
        })
        do! page.WaitForURLAsync($"{BaseUrl}/login")
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)
        page.Url |> should equal $"{BaseUrl}/login"
        // check the cookie was set correctly
        let! lang = getLanguageAsync(page)
        lang |> should equal "en"
        // check the menus in english
        do! checkMenusInLanguageAsync(page,publicMenu,"en")
        // check the language selection ddl has correct value selected
        let! languageDdlValue = page.EvalOnSelectorAsync("#languageSelect", "el => el.value" )
        languageDdlValue.Value.ToString()|> should equal "en"
        ()
    }


[<TestFixture;Parallelizable>]
type LoginTests() =
    inherit PageTest()
    [<Test>]
    member self.``failed_login`` () = unitTask {
        let page = self.Page
        let submit () = unitTask {
            let! _  = page.RunAndWaitForRequestFinishedAsync(fun () -> page.PressAsync("#passwordInput", "Enter"))
            ()
        }

        do! goToLoginAsync(page)
        do! errorNotVisibleAsync(page)

        do! page.PressAsync("#passwordInput", "Enter")
        do! errorIsVisibleAsync(page)

        do! goToLoginAsync(page)
        do! page.PressAsync("#passwordInput", "Enter")
        do! errorIsVisibleAsync(page)
        do! textIsVisible(page,"Please enter your email address.")
        do! textIsVisible(page,"Please enter your password.")

        do! goToLoginAsync(page)
        do! page.FillAsync("#emailInput","longbob@test.com")
        do! page.PressAsync("#passwordInput", "Enter")
        do! errorIsVisibleAsync(page)
        do! textIsNotVisible(page,"Please enter your email address.")
        do! textIsVisible(page,"Please enter your password.")

        do! goToLoginAsync(page)
        do! page.FillAsync("#emailInput","longbob@test.com")
        do! page.FillAsync("#passwordInput","invalidPassword")
        do! submit()
        do! errorIsVisibleAsync(page)
        do! textIsNotVisible(page,"Please enter your email address.")
        do! textIsNotVisible(page,"Please enter your password.")
        do! textIsVisible(page,"Authentication failed. Check your credentials and try again or reset your password.")

        do! goToLoginAsync(page)
        do! page.FillAsync("#emailInput","longbob@test.com")
        do! page.FillAsync("#passwordInput","My§uperPass01234")
        do! submit()
        do! page.WaitForURLAsync($"{BaseUrl}/")
        do! errorNotVisibleAsync(page)
        do! textIsNotVisible(page,"Please enter your email address.")
        do! textIsNotVisible(page,"Please enter your password.")
        do! textIsNotVisible(page,"Authentication failed. Check your credentials and try again or reset your password.")
        page.Url |> should equal $"{BaseUrl}/"
        do! checkMenusAsync(page,authenticatedMenu)

        ()
    }
    [<Test>]
    member self.``redirect_after_login`` () = unitTask {
        let page = self.Page
        let submit () = unitTask {
            let! _  = page.RunAndWaitForRequestFinishedAsync(fun () -> page.PressAsync("#passwordInput", "Enter"))
            ()
        }
        let email = "existingbob@test.com"
        do! goToPathAsync(page,"/login?ReturnUrl=%2Finstances%2F100")
        do! page.FillAsync("#emailInput",email)
        do! page.FillAsync("#passwordInput",passwords.Item(email))
        do! submit()
        //result.Status |> should equal 200
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)
        do! Assertions.Expect(page).ToHaveURLAsync($"{BaseUrl}/instances/100")
    }

[<TestFixture;Parallelizable>]
type ForgotPasswordTests() =
    inherit PageTest()
    [<Test>]
    member self.``forgot_password_validations`` () = unitTask {
        let page = self.Page
        page.SetDefaultTimeout(10000F)
        let submitClientError () = unitTask {
            do! page.PressAsync("#emailInput", "Enter")
            let _ =  page.WaitForSelectorAsync("#errorBox")
            ()
        }
        let submitServer () = unitTask {
            let! response =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                                    do! page.PressAsync("#emailInput", "Enter")
                                                                    ()
                                                             })
            ()
        }

        let reload () = unitTask {
            let! r = page.ReloadAsync(PageReloadOptions(WaitUntil=WaitUntilState.NetworkIdle))
            r.Status |> should equal 200
            do! errorNotVisibleAsync(page)
            do! elementVisible(page, "#emailInput")
            do! elementVisible(page, "#submitForgotEmail")
        }
        let checkEmailIsRefused() = unitTask{
            do! errorIsVisibleAsync(page)
            do! textIsVisible(page, "Please enter your email address. The current value is not recognised as a valid address.")
        }

        // helper function to reset password of an existing user
        let resetPasswordAndGetMailAsync(page:IPage, email:string) = task{
            // PRE: the email corresponds to an existin user
            // PRE: page is on the /forgot_password page
            // PRE: the user's mailbox is empty, so we can return the unique mail found
            //      in the inbox after password reset request:w
            do! noMailSentTo(email)

            let! t = getPageTranslatorAsync(page)
            // reset password are return the mail sent to user.
            do! page.FillAsync("#emailInput", email)
            do! submitServer()
            do! errorNotVisibleAsync(page)
            do! infoVisible(page,t.t("You will receive a mail shortly with reset instructions if the email address you entered belongs to a MyOwnDB account. If you don't receive a mail, check your spam folder, and check you entered the right email address in this form."))
            let! mails = readMails(email)
            cleanMailsFor(email)
            return mails |> Seq.head
        }

        do! goToLoginAsync(page)
        do! errorNotVisibleAsync(page)

        // Form is submitted by pressing enter in email input
        // and empty email refused
        do! page.ClickAsync("text=Forgot password")
        do! errorNotVisibleAsync(page)
        do! submitClientError()
        do! checkEmailIsRefused()

        // Form is submitted by clicking submit button
        do! reload()
        do! page.ClickAsync("#submitForgotEmail")
        do! checkEmailIsRefused()

        // Invalid email is refused
        do! reload()
        do! page.FillAsync("#emailInput", "novalidemail")
        do! submitClientError()
        do! checkEmailIsRefused()

        // No mail is sent for unknown address
        do! reload()
        do! page.FillAsync("#emailInput", "unknown_email@example.com")
        do! submitServer()
        do! errorNotVisibleAsync(page)
        do! infoVisible(page,"You will receive a mail shortly with reset instructions if the email address you entered belongs to a MyOwnDB account. If you don't receive a mail, check your spam folder, and check you entered the right email address in this form.")
        do! noMailSentTo("unknown_email@example.com")

        // Mail is sent for valid email
        let recipient = "bob@test.com"
        do! reload()
        do! page.FillAsync("#emailInput", recipient)
        do! submitServer()
        do! errorNotVisibleAsync(page)
        do! infoVisible(page,"You will receive a mail shortly with reset instructions if the email address you entered belongs to a MyOwnDB account. If you don't receive a mail, check your spam folder, and check you entered the right email address in this form.")
        let! mails = readMails(recipient)
        (mails |> Array.length) |> should equal 1
        mails.[0].Subject |> should equal "Reset your password at MyOwnDB"
        mails.[0].Body.StartsWith("Dear user,\n\n\
                                 Someone asked a reset of your password at MyOwnDB. If you didn't initiate this request, please ignore this mail.\n\
                                 \n\
                                 You can set a new password for your account by following this link:")
        |> should equal true
        cleanMailsFor(recipient)
        // check that the cleanup was done
        // Ideally should be in a helpers test suite
        let! mails = readMails(recipient)
        (mails |> Array.length) |> should equal 0

        // Mail is sent in correct language for valid email
        let! t = switchToFrenchAsync(page)
        t.t("MyOwnDB") |> should equal "MaDB"
        let! lang = getLanguageAsync(page)
        lang |> should equal "fr"

        let! mail = resetPasswordAndGetMailAsync(page,recipient)
        mail.Subject |> should equal (t.t("Reset your password at MyOwnDB"))
        mail.Subject |> should equal ("Réinitialiser votre mot de passe sur MyOwnDB")
        mail.Body |> should startWith "Cher·e utilisateur·rice,\n\
                                  \n\
                                  Quelqu’un a demandé une réinitialisation de votre mot de passe à MyOwnDB. Si \
                                  vous n’avez pas initié cette demande, veuillez ignorer ce courriel.\n\
                                  \n\
                                  Vous pouvez définir un nouveau mot de passe pour votre compte en suivant ce lien:"

        let! t = switchToEnglishAsync(page)
        let! mail = resetPasswordAndGetMailAsync(page,recipient)
        let link = mail |> firstLinkInMail
        let! _r = goToUrlAsync(page,link)
        do! elementVisible(page,"#passwordRequirements")

        // enter just the password and press enter
        do! page.FillAsync("#passwordInput", "badpass")
        // - button should be disabled
        do! elementDisabled(page,"#submitNewPassword")
        // - pressing enter displays the error box telling the confirmation is not equal
        do! pressEnterClientActionAsync(page,"#passwordInput")
        do! errorIsVisibleAsync(page)
        do! textIsVisible(page,"The confirmation must be equal to the password")
        do! elementNotVisible(page,"#passwordRequirements")

        // fill in confirmation and submit
        // password is all lowercase
        do! page.FillAsync("#confirmationInput", "badpass")
        // - button should be enabled
        let! _ =  submitNewPassword(page)
        //
        do! pressEnterServerActionAsync(page,"#confirmationInput")
        do! errorIsVisibleAsync(page)
        do! textIsNotVisible(page,"Password must have at least one lowercase letter")
        do! textIsVisible(page,"Password must have at least one uppercase letter")
        do! textIsVisible(page,"Password must have at least one special character")
        do! textIsVisible(page,"Password must have at least one digit")
        do! textIsVisible(page,"Password must have at least 10 characters")


        // repeat with password all uppercase and click submit
        do! page.FillAsync("#passwordInput", "BADPASS")
        do! page.FillAsync("#confirmationInput", "BADPASS")
        // - button should be enabled
        let! _ =  submitNewPassword(page)
        do! errorIsVisibleAsync(page)
        do! textIsVisible(page,"Password must have at least one lowercase letter")
        do! textIsNotVisible(page,"Password must have at least one uppercase letter")
        do! textIsVisible(page,"Password must have at least one special character")
        do! textIsVisible(page,"Password must have at least one digit")
        do! textIsVisible(page,"Password must have at least 10 characters")

        // repeat with password with upper and lower cases
        do! page.FillAsync("#passwordInput", "TestTest")
        do! page.FillAsync("#confirmationInput", "TestTest")
        // - button should be enabled
        let! _ =  submitNewPassword(page)
        do! errorIsVisibleAsync(page)
        do! textIsNotVisible(page,"Password must have at least one lowercase letter")
        do! textIsNotVisible(page,"Password must have at least one uppercase letter")
        do! textIsVisible(page,"Password must have at least one special character")
        do! textIsVisible(page,"Password must have at least one digit")
        do! textIsVisible(page,"Password must have at least 10 characters")

        // repeat with password with upper and lower cases, digit
        do! page.FillAsync("#passwordInput", "TestTest1")
        do! page.FillAsync("#confirmationInput", "TestTest1")
        // - button should be enabled
        let! _ =  submitNewPassword(page)
        do! errorIsVisibleAsync(page)
        do! textIsNotVisible(page,"Password must have at least one lowercase letter")
        do! textIsNotVisible(page,"Password must have at least one uppercase letter")
        do! textIsVisible(page,"Password must have at least one special character")
        do! textIsNotVisible(page,"Password must have at least one digit")
        do! textIsVisible(page,"Password must have at least 10 characters")

        // repeat with password with upper and lower cases, digit, special character
        // should be successful
        do! page.FillAsync("#passwordInput", "TestTest1&")
        do! page.FillAsync("#confirmationInput", "TestTest1&")
        // - button should be enabled
        let! _ =  submitNewPassword(page)
        // success, so no error
        do! errorNotVisibleAsync(page)
        do! textIsNotVisible(page,"Password must have at least one lowercase letter")
        do! textIsNotVisible(page,"Password must have at least one uppercase letter")
        do! textIsNotVisible(page,"Password must have at least one special character")
        do! textIsNotVisible(page,"Password must have at least one digit")
        do! textIsNotVisible(page,"Password must have at least 10 characters")
        // success, so info message displayed
        do! infoVisible(page,"Your password was updated, you can now try to log in!")
        // Login form is displayed on the same page
        do! elementVisible(page,"#emailInput")
        do! elementVisible(page,"#passwordInput")
        do! elementVisible(page,"button#submitLogin")
        // Check this form is working
        do! checkLoginThenLogoutOnCurrentPage(page,recipient,"TestTest1&",false)

        // Now check log in from normal login form
        do! checkLoginThenLogoutStandardUser(page,recipient,"TestTest1&")

        // If a user resets the password twice, both links are valid and can
        // be used during their respective validity.
        do! goToPathAsync(page, "/forgot_password")
        let! mail1 = resetPasswordAndGetMailAsync(page,recipient)
        let link1 = firstLinkInMail(mail1)
        do! goToPathAsync(page, "/forgot_password")
        let! mail2 = resetPasswordAndGetMailAsync(page,recipient)
        let link2 = firstLinkInMail(mail2)
        // follow second link and reset password
        do! goToUrlAsync(page,link2)
        do! page.FillAsync("#passwordInput", "TestTest2&")
        do! page.FillAsync("#confirmationInput", "TestTest2&")
        let! _ =  submitNewPassword(page)
        do! checkLoginThenLogoutStandardUser(page,recipient,"TestTest2&")
        // follow first link and reset password too
        do! goToUrlAsync(page,link1)
        do! page.FillAsync("#passwordInput", "TestTest3&")
        do! page.FillAsync("#confirmationInput", "TestTest3&")
        let! _ =  submitNewPassword(page)
        do! checkLoginThenLogoutStandardUser(page,recipient,"TestTest3&")

        // if the email address in the link is modified, the change of pw fails
        do! goToPathAsync(page, "/forgot_password")
        let! mail = resetPasswordAndGetMailAsync(page,recipient)
        let link = firstLinkInMail(mail)
        let tweakedLink = link.Replace("bob~","longbob~")
        do! goToUrlAsync(page,tweakedLink)
        do! page.FillAsync("#passwordInput", "TestTest4&")
        do! page.FillAsync("#confirmationInput", "TestTest4&")
        let! req =  submitNewPassword(page)
        let! _resp = req.ResponseAsync()
        do! errorIsVisibleAsync(page)
        do! textIsVisible(page,"Invalid token")
        // check passwords were not changed
        do! checkLoginThenLogoutStandardUser(page,recipient,"TestTest3&")
        do! checkLoginFails(page,"longbob@test.com","TestTest4&")
    }

[<TestFixture;>]
type SignupTests() =
    inherit PageTest()
    [<Test>]
    member self.``registration`` () = unitTask {
        let page = self.Page
        page.SetDefaultTimeout(10000F)
        let submitClientError () = unitTask {
            do! page.PressAsync("#emailInput", "Enter")
            let _ =  page.WaitForSelectorAsync("#errorBox")
            ()
        }
        let submitServer () = unitTask {
            let! request =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                                    do! page.PressAsync("#emailInput", "Enter")
                                                                    ()
                                                             })
            let! response = request.ResponseAsync()
            response.Status |> should equal 200
            ()
        }

        let reload () = unitTask {
            let! r = page.ReloadAsync(PageReloadOptions(WaitUntil=WaitUntilState.NetworkIdle))
            r.Status |> should equal 200
            do! errorNotVisibleAsync(page)
            do! elementVisible(page, "#emailInput")
            do! elementVisible(page, "#submitSignup")
        }

        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

        let newUser = "newbob@test.com"
        do! goToSignupAsync(page)
        do! errorNotVisibleAsync(page)

        // No user should be created when submission is prevented
        let! preState, checkDifferenceNow =
            DBState.startWithCustomQueries client [] DBState.zeroState
        // Check submission is prevented as expected
        let buttonLocator = page.Locator("#submitSignup")
        // - unrecognised email
        do! page.FillAsync("#emailInput", "invalid_email")
        do! page.FillAsync("#passwordInput", "pass")
        do! page.FillAsync("#confirmationInput", "pass")
        do! page.CheckAsync("#tosInput")
        let subject="#emailInput"
        do! Assertions.Expect(buttonLocator).ToHaveAttributeAsync("disabled","")
        do! Assertions.Expect(page.Locator(subject)).ToHaveClassAsync("form-control is-invalid")
        do! page.FillAsync(subject, newUser)
        do! Assertions.Expect(buttonLocator).Not.ToHaveAttributeAsync("disabled","")
        do! Assertions.Expect(page.Locator(subject)).ToHaveClassAsync("form-control is-valid")
        // - no tos accepted
        let subject="#tosInput"
        do! page.UncheckAsync(subject)
        do! Assertions.Expect(buttonLocator).ToHaveAttributeAsync("disabled","")
        do! Assertions.Expect(page.Locator(subject)).ToHaveClassAsync("form-check-input is-invalid")
        do! page.CheckAsync("#tosInput")
        do! Assertions.Expect(buttonLocator).Not.ToHaveAttributeAsync("disabled","")
        do! Assertions.Expect(page.Locator(subject)).ToHaveClassAsync("form-check-input is-valid")
        // - password confirmation is different
        let subject = "#confirmationInput"
        do! page.FillAsync(subject, "passdifferent")
        do! Assertions.Expect(buttonLocator).ToHaveAttributeAsync("disabled","")
        do! Assertions.Expect(page.Locator(subject)).ToHaveClassAsync("is-invalid form-control")
        do! page.FillAsync(subject, "pass")
        do! Assertions.Expect(page.Locator(subject)).ToHaveClassAsync("is-valid form-control")

        do! checkDifferenceNow()


        // No user should be created when server reports errors
        let! preState, checkDifferenceNow =
            DBState.startWithCustomQueries client [] DBState.zeroState

        do! reload()
        // check password criteria errors are displayed
        do! page.FillAsync("#emailInput", newUser)
        do! page.FillAsync("#passwordInput", "pass")
        do! page.FillAsync("#confirmationInput", "pass")
        do! page.CheckAsync("#tosInput")
        do! submitClientError()
        do! errorIsVisibleAsync(page)
        do! textIsNotVisible(page,"Password must have at least one lowercase letter")
        do! textIsVisible(page,"Password must have at least one uppercase letter")
        do! textIsVisible(page,"Password must have at least one special character")
        do! textIsVisible(page,"Password must have at least one digit")
        do! textIsVisible(page,"Password must have at least 10 characters")

        do! checkDifferenceNow()

        // successful registration
        do! reload()

        let! preState, checkDifferenceNow =
            DBState.startWithOnlyCustomQueries client
                [
                    1L,"select count(*) from users where user_type_id=1"
                    1L,"select count(*) from users where login='NEWBOB@TEST.COM' and user_type_id=1"
                    0L,"select count(*) from users where user_type_id!=1"
                ]
        do! page.FillAsync("#emailInput", newUser)
        do! page.FillAsync("#passwordInput", "TestTest1&")
        do! page.FillAsync("#confirmationInput", "TestTest1&")
        do! page.CheckAsync("#tosInput")
        do! noMailSentTo(newUser)
        do! submitServer()
        do! infoVisible(page, "Your account has been registered, but you need to confirm it by clicking the link we just sent you.")
        do! mailSentTo(newUser)
        let! mail= firstMailTo(newUser)
        let link = firstLinkInMail(mail)
        cleanMailsFor(newUser)
        do! goToUrlAsync(page, link)
        do! infoVisible(page, "Your email was successfully confirmed. You can now log in.")
        // login from form displayed on confirmation page
        do! checkDifferenceNow()

        // Check the logged_in_at field is null, so we can be sure it's been updated by the login below
        do! DBAction.queryAction "select count(*) from users where email = %s and logged_in_at is null" newUser
            |> DBAction.run client
            |> Async.map (checkOkDBResult [1L])

        do! checkLoginThenLogoutOnCurrentPage(page,newUser,"TestTest1&", true)

        // Wait for Hangfire to update logged_in_at
        do! Async.Sleep 300

        do! DBAction.queryAction "select count(*) from users where email = %s and now()-logged_in_at < '1seconds'" newUser
            |> DBAction.run client
            |> Async.map (checkOkDBResult [1L])

        do! client.Close()

        // registering with the same email fails
        do! goToSignupAsync(page)
        do! errorNotVisibleAsync(page)
        do! page.FillAsync("#emailInput", newUser)
        do! page.FillAsync("#passwordInput", "TestTest1&")
        do! page.FillAsync("#confirmationInput", "TestTest1&")
        do! page.CheckAsync("#tosInput")
        do! noMailSentTo(newUser)
        do! submitServer()
        do! noMailSentTo(newUser)
        do! errorIsVisibleAsync(page)
        do! textIsVisible(page, "The email "+newUser+" is already registered.")

        // check Identity Core messages are translated
        do! goToSignupAsync(page)
        let! _ = switchToFrenchAsync(page)
        do! errorNotVisibleAsync(page)
        do! page.FillAsync("#emailInput", newUser)
        do! page.FillAsync("#passwordInput", "TestTest1&")
        do! page.FillAsync("#confirmationInput", "TestTest1&")
        do! page.CheckAsync("#tosInput")
        do! noMailSentTo(newUser)
        do! submitServer()
        do! noMailSentTo(newUser)
        do! errorIsVisibleAsync(page)
        do! textIsVisible(page, "L'adresse "+newUser+" est déjà enregistrée.")

    }

[<TestFixture;Parallelizable>]
type DatatableTest() =
    inherit PageTest()
    [<Test>]
    member self.``datatable_display`` () = unitTask {
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")
        // view instances for entity id 12
        do! goToPathAsync(page, "/instances/12")

        let ancestor = """//div[@id="table_main_entity_12"]"""

        do! elementVisible(page,$"""{ancestor}//span[contains(@class,"myowndbrefreshtable")]""")
    }

    [<Test>]
    member self.``datatable_pagination`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"existingbob@test.com")
        // view instances for entity id 12
        do! goToPathAsync(page, "/instances/12")
        // helpers
        let Check = checkTable page "#table_main_entity_12"
        let ToPage = clickTablePagination page "#table_main_entity_12"
        let ClickHeader = clickTableColumnHeader page "#table_main_entity_12"

        // run tests
        let spec={searchedColumn="nom"; searchedValue=""; perPage=10; columnsCount=5; sorting= None; hasLinks=true; paginationLinks=7}
        do! Check
             "tables/entity_12/p1.html"
             spec
        // specific page number
        do! ToPage "3"
        do! Check
                "tables/entity_12/p3.html"
                spec
        // previous page
        do! ToPage "‹"
        do! Check
                "tables/entity_12/p2.html"
                spec
        // last page
        do! ToPage "»|"
        do! Check
                "tables/entity_12/p12.html"
                spec
        // first page
        do! ToPage "|«"
        do! Check
                "tables/entity_12/p1.html"
                spec
        // next page
        do! ToPage "›"
        do! Check
                "tables/entity_12/p2.html"
                spec


        // This displays the same page again, but is only registered once in the history
        // test sorting
        do! ToPage "2"
        do! Check
                "tables/entity_12/p2.html"
                spec
        do! ClickHeader "prenom"
        do! Check
                "tables/entity_12/p2_sort_prenom.html"
                { spec with sorting=Some Asc }
        do! ClickHeader "prenom"
        do! Check
                "tables/entity_12/p2_sort_prenom_asc.html"
                { spec with sorting=Some Desc }
        // Go back in history
        let! _ = page.GoBackAsync()
        do! Check
                "tables/entity_12/p2_sort_prenom.html"
                { spec with sorting=Some Asc }
        let! _ = page.GoBackAsync()
        do! Check
                "tables/entity_12/p2.html"
                spec
        // The p2 page was displayed twice subsequently, but it was registered
        // only once in the history, as expected.
        let! _ = page.GoBackAsync()
        do! Check
                "tables/entity_12/p1.html"
                spec
    }

    [<Test>]
    member self.``history_back_then_new`` () = unitTask {
        let page = self.Page

        // helpers
        let Check = checkTable page "#table_main_entity_12"
        let ToPage = clickTablePagination page "#table_main_entity_12"
        let ClickHeader = clickTableColumnHeader page "#table_main_entity_12"
        let spec={searchedColumn="nom"; searchedValue=""; perPage=10; columnsCount=5; sorting= None; hasLinks=true; paginationLinks=7}

        do! authenticate(page,"existingbob@test.com")
        // view instances for entity id 12
        do! goToPathAsync(page, "/instances/12")
        do! filterTable page "#table_main_entity_12" "prenom" "en"
        do! Check
                "tables/entity_12/p1_filter_prenom_en.html"
                { spec with searchedColumn="prenom"
                            searchedValue="en"
                            paginationLinks=1
                            }
        do! filterTable page "#table_main_entity_12" "prenom" "a$"
        let spec_adollar = { spec with
                                searchedColumn="prenom"
                                searchedValue="a$"
                                paginationLinks=3
                           }
        do! Check
                "tables/entity_12/p1_filter_prenom_a$.html"
                spec_adollar
        // Go back
        let! _ = page.GoBackAsync()
        do! Check
                "tables/entity_12/p1_filter_prenom_en.html"
                { spec with searchedColumn="prenom"
                            searchedValue="en"
                            paginationLinks=1
                            }
        // Do a new navigation
        do! clearSearch page "#table_main_entity_12"
        do! Check
                "tables/entity_12/p1.html"
                { spec with searchedColumn="prenom"
                            searchedValue=""
                            }
        // Go back and check the page is updated correctly.
        // It was this step that failed during the development of history navigation.
        let! _ = page.GoBackAsync()
        do! Check
                "tables/entity_12/p1_filter_prenom_en.html"
                { spec with searchedColumn="prenom"
                            searchedValue="en"
                            paginationLinks=1
                            }
    }
    [<Test>]
    member self.``datatable_search`` () = unitTask {
        let page = self.Page

        // helpers
        let Check = checkTable page "#table_main_entity_12"
        let ToPage = clickTablePagination page "#table_main_entity_12"
        let ClickHeader = clickTableColumnHeader page "#table_main_entity_12"
        let spec={searchedColumn="nom"; searchedValue=""; perPage=10; columnsCount=5; sorting= None; hasLinks=true; paginationLinks=7}

        do! authenticate(page,"existingbob@test.com")
        // view instances for entity id 12
        do! goToPathAsync(page, "/instances/12")
        do! filterTable page "#table_main_entity_12" "prenom" "en"
        do! Check
                "tables/entity_12/p1_filter_prenom_en.html"
                { spec with searchedColumn="prenom"
                            searchedValue="en"
                            paginationLinks=1
                            }
        //clear search
        do! clearSearch page "#table_main_entity_12"
        do! Check
                "tables/entity_12/p1.html"
                { spec with searchedColumn="prenom"
                            searchedValue=""
                            }

        // redo the search so we can test 2 subsequent searches
        do! filterTable page "#table_main_entity_12" "prenom" "en"
        do! Check
                "tables/entity_12/p1_filter_prenom_en.html"
                { spec with searchedColumn="prenom"
                            searchedValue="en"
                            paginationLinks=1
                            }
        // do another search
        do! filterTable page "#table_main_entity_12" "prenom" "a$"
        // Take url for testing link sharing of a datatable page
        let page1Url = page.Url
        let spec_adollar = { spec with
                                searchedColumn="prenom"
                                searchedValue="a$"
                                paginationLinks=3
                           }
        do! Check
                "tables/entity_12/p1_filter_prenom_a$.html"
                spec_adollar
        do! ToPage "2"
        do! Check
                "tables/entity_12/p2_filter_prenom_a$.html"
                spec_adollar
        do! ToPage "3"
        do! Check
                "tables/entity_12/p3_filter_prenom_a$.html"
                spec_adollar
        do! ClickHeader "nom"
        do! Check
                "tables/entity_12/p3_filter_prenom_a$_sort_nom.html"
                { spec_adollar with sorting = Some Asc }

        // Test history navigation
        let! _ =  page.GoBackAsync()
        do! Check
                "tables/entity_12/p3_filter_prenom_a$.html"
                spec_adollar
        let! _ =  page.GoBackAsync()
        do! Check
                "tables/entity_12/p2_filter_prenom_a$.html"
                spec_adollar
        let! _ =  page.GoBackAsync()
        do! Check
                "tables/entity_12/p1_filter_prenom_a$.html"
                spec_adollar
        let! _ =  page.GoBackAsync()
        do! Check
                "tables/entity_12/p1_filter_prenom_en.html"
                { spec with searchedColumn="prenom"
                            searchedValue="en"
                            paginationLinks=1
                            }
        let! _ =  page.GoBackAsync()
        do! Check
                "tables/entity_12/p1.html"
                { spec with searchedColumn="prenom"
                            searchedValue=""
                            }
        let! _ =  page.GoBackAsync()
        do! Check
                "tables/entity_12/p1_filter_prenom_en.html"
                { spec with searchedColumn="prenom"
                            searchedValue="en"
                            paginationLinks=1
                            }
        let! _ =  page.GoForwardAsync()
        do! Check
                "tables/entity_12/p1.html"
                { spec with searchedColumn="prenom"
                            searchedValue=""
                            }
        let! _ =  page.GoForwardAsync()
        do! Check
                "tables/entity_12/p1_filter_prenom_en.html"
                { spec with searchedColumn="prenom"
                            searchedValue="en"
                            paginationLinks=1
                            }
        let! _ =  page.GoForwardAsync()
        do! Check
                "tables/entity_12/p1_filter_prenom_a$.html"
                spec_adollar
        let! _ =  page.GoForwardAsync()
        do! Check
                "tables/entity_12/p2_filter_prenom_a$.html"
                spec_adollar
        let! _ =  page.GoForwardAsync()
        do! Check
                "tables/entity_12/p3_filter_prenom_a$.html"
                spec_adollar
        let! _ =  page.GoForwardAsync()
        do! Check
                "tables/entity_12/p3_filter_prenom_a$_sort_nom.html"
                { spec_adollar with sorting = Some Asc }
        // Check going to a URL copied from address bar works
        do! goToUrlAsync(page, page1Url)
        do! Check
                "tables/entity_12/p1_filter_prenom_a$.html"
                spec_adollar
    }

    [<Test>]
    member self.``datatable_perpage`` () = unitTask {
        let page = self.Page
        // helpers
        let Check = checkTable page "#table_main_entity_12"
        let ToPage = clickTablePagination page "#table_main_entity_12"
        let ClickHeader = clickTableColumnHeader page "#table_main_entity_12"
        let spec={searchedColumn="nom"; searchedValue=""; perPage=10; columnsCount=5; sorting= None; hasLinks=true; paginationLinks=7}

        do! authenticate(page,"existingbob@test.com")
        // view instances for entity id 12
        do! goToPathAsync(page, "/instances/12")
        // 20 per page
        do! setPerPage page "#table_main_entity_12" 20
        do! Check
                "tables/entity_12/p1_pp20.html"
                { spec with perPage = 20 ; paginationLinks = 6}
        // 500 per page
        do! setPerPage page "#table_main_entity_12" 500
        do! Check
                "tables/entity_12/p1_pp500.html"
                { spec with perPage = 500; paginationLinks = 1 }
        // 50 per page
        let specPp50 = { spec with perPage = 50 ; paginationLinks = 3}
        do! setPerPage page "#table_main_entity_12" 50
        do! Check
                "tables/entity_12/p1_pp50.html"
                specPp50
        // sort with per page
        let specPp50Sorted = {specPp50 with sorting = Some Asc}
        do! ClickHeader "fonction"
        do! Check
                "tables/entity_12/p1_pp50_sort_fonction.html"
                specPp50Sorted
        // go to page 2 of sort with per page
        do! ToPage "2"
        do! Check
                "tables/entity_12/p2_pp50_sort_fonction.html"
                specPp50Sorted
        // go to page 3 of sort with per page
        do! ToPage "3"
        do! Check
                "tables/entity_12/p3_pp50_sort_fonction.html"
                specPp50Sorted
        // search with per page
        do! filterTable page "#table_main_entity_12" "service" "&"
        do! Check
                "tables/entity_12/p1_pp50_sort_fonction_search_service_ampersand.html"
                { specPp50Sorted with searchedColumn="service"; searchedValue="&"; paginationLinks=2}
        // Go back in history
        // We go back two steps because the filterTable call updated both the column
        // used for filtering, and the value searched. Although this is done in one
        // function call, it still is done in two steps, resulting in 2 history entries.
        let! _ = page.GoBackAsync()
        let! _ = page.GoBackAsync()
        do! Check
                "tables/entity_12/p3_pp50_sort_fonction.html"
                specPp50Sorted
        let! _ = page.GoBackAsync()
        do! Check
                "tables/entity_12/p2_pp50_sort_fonction.html"
                specPp50Sorted
    }

    [<Test>]
    // tests the datatable of displaying linked instances
    member self.``linked_instances_datatable`` () = unitTask {
        let page = self.Page
        // helpers
        let tableId = "#table_linked_77_children_relation_7_entity_12"
        let Check = checkTable page tableId
        let ToPage = clickTablePagination page tableId
        let ClickHeader = clickTableColumnHeader page tableId
        let spec={searchedColumn="nom"; searchedValue=""; perPage=10; columnsCount=5; sorting= None; hasLinks=true; paginationLinks=1}

        // Check the other table of linked instance through the other relation
        // This test is not great because it has only one instance linked, but I think if it was
        // impacted by changes in the other linked table it would still detect it
        let CheckOther () =
                checkTable
                    page
                    "#table_linked_77_children_relation_9_entity_12"
                    "tables/linked_instances_from_77/other_relation.html"
                    spec

        do! authenticate(page,"existingbob@test.com")
        // view instances for entity id 12
        do! goToPathAsync(page, "/instance/77")
        do! Check
                "tables/linked_instances_from_77/all_pp10_p1.html"
                spec
        do! CheckOther()

        // Check pagination is ok
        do! ToPage "3"
        do! Check
                "tables/linked_instances_from_77/all_pp10_p3.html"
                spec
        do! CheckOther()

        // check per page setting
        do! setPerPage page tableId 20
        do! Check
                "tables/linked_instances_from_77/all_pp20_p3.html"
                {spec with perPage=20; paginationLinks=3}
        do! CheckOther()
        do! ToPage "1"
        do! Check
                "tables/linked_instances_from_77/all_pp20_p1.html"
                {spec with perPage=20; paginationLinks=3}
        do! CheckOther()

        // Check search is ok
        do! filterTable page tableId "nom" "y"
        do! Check
                "tables/linked_instances_from_77/search_nom_y_pp20_p1.html"
                {spec with perPage=20; searchedValue="y"; paginationLinks=1}
        do! CheckOther()
        let copiedUrl = page.Url

        // Check history navigation back and forward
        let! _ = page.GoBackAsync()
        do! Check
                "tables/linked_instances_from_77/all_pp20_p1.html"
                {spec with perPage=20; paginationLinks=3}
        do! CheckOther()
        let! _ = page.GoBackAsync()
        do! Check
                "tables/linked_instances_from_77/all_pp20_p3.html"
                {spec with perPage=20; paginationLinks=3}
        do! CheckOther()
        let! _ = page.GoBackAsync()
        do! Check
                "tables/linked_instances_from_77/all_pp10_p3.html"
                spec
        do! CheckOther()
        let! _ = page.GoBackAsync()
        do! Check
                "tables/linked_instances_from_77/all_pp10_p1.html"
                spec
        do! CheckOther()
        // forward
        let! _ = page.GoForwardAsync()
        do! Check
                "tables/linked_instances_from_77/all_pp10_p3.html"
                spec
        do! CheckOther()
        let! _ = page.GoForwardAsync()
        do! Check
                "tables/linked_instances_from_77/all_pp20_p3.html"
                {spec with perPage=20; paginationLinks=3}
        do! CheckOther()
        let! _ = page.GoForwardAsync()
        do! Check
                "tables/linked_instances_from_77/all_pp20_p1.html"
                {spec with perPage=20; paginationLinks=3}
        do! CheckOther()
        let! _ = page.GoForwardAsync()
        do! Check
                "tables/linked_instances_from_77/search_nom_y_pp20_p1.html"
                {spec with perPage=20; searchedValue="y"; paginationLinks=1}
        do! CheckOther()
        // Check link copied with state

        do! goToUrlAsync(page, copiedUrl)
        do! Check
                "tables/linked_instances_from_77/search_nom_y_pp20_p1.html"
                {spec with perPage=20; searchedValue="y"; paginationLinks=1}
        do! CheckOther()

    }

    [<Test>]
    member self.``instance_display`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"existingbob@test.com")
        do! goToPathAsync(page, "/instance/100200")
        // view instances for entity id 12
        let! _ = page.WaitForSelectorAsync(".instance-display .instance-field-label")
        let labels = page.Locator(".instance-display .instance-field-label")

        let! labelsText = labels.AllInnerTextsAsync()
        labelsText |> List.ofSeq |> should equal ["nom";"prenom";"fonction";"service";"coordonees_specifiques";"company_email"]


        let values = page.Locator(".instance-display .instance-field-value")
        let! valuesText = values.AllInnerTextsAsync()
        valuesText |> List.ofSeq |> should equal ["Runolfsdottir";"Greg";"Product Brand Coordinator";"Baby & Kids"; String.Empty; String.Empty]
        // check related instances are displayed. Their working is checked in test "linked_instances_datatable"
        do! elementVisible(page,"#table_linked_100200_parents_relation_8_entity_19")
        do! elementVisible(page,"#table_linked_100200_parents_relation_7_entity_11")
        do! elementVisible(page,"#table_linked_100200_parents_relation_9_entity_11")
        // Check we have the link to edit the instance
        do! elementVisible(page,"""a:text("Edit")""")
        do! page.Locator("""a:text("Edit")""").ClickAsync()
        do! Assertions.Expect(page).ToHaveURLAsync($"{BaseUrl}/edit_instance/100200")
        // Check the edit form has a link back to the view
        do! elementVisible(page,"""//a[text()="View"]""")
        do! page.Locator("""//a[text()="View"]""").ClickAsync()
        do! Assertions.Expect(page).ToHaveURLAsync($"{BaseUrl}/instance/100200")
    }



[<TestFixture;Parallelizable>]
type AttachmentsTest() =
    inherit PageTest()
    override _.ContextOptions() =
        new BrowserNewContextOptions(AcceptDownloads=true)

    [<Test>]
    member self.``file_attach_display`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"existingbob@test.com")

        // display of empty value of link in datatable
        // This table has instances with a null value for the file attachment.
        // It caused trouble before, but should work fine now
        do! goToPathAsync(page, "/instances/100")
        let! _ = page.WaitForSelectorAsync("#table_main_entity_100")
        // Simply validate this link is present, which should indicate the table
        // was displayed successfully
        do! elementVisible(page,"""a[href*="http://www.amzon.com"]""")
        // Same check on instance display page
        do! goToPathAsync(page, "/instance/200")
        do! elementVisible(page,"""a[href*="http://www.amzon.com"]""")

        // display of link in datatable
        do! goToPathAsync(page, "/instances/100000")
        let! _ = page.WaitForSelectorAsync("#table_main_entity_100000")
        do! elementVisible(page,"""a[href*="/get_attachment/1216"]""")
        do! elementVisible(page,"""a[href*="/get_attachment/1219"]""")

        // display of link in instance view
        do! goToPathAsync(page, "/instance/203")
        do! elementVisible(page,"""a[href*="/get_attachment/1216"]""")
        do! goToPathAsync(page, "/instance/100000")
        do! elementVisible(page,"""a[href*="/get_attachment/1219"]""")
    }

    [<Test>]
    member self.``file_attach_download`` () = unitTask {
        let page = self.Page

        // Check we are redirected to login page if unauthenticated
        do! isStatus(page,"/get_attachment/1219", 302)
        do! page.WaitForURLAsync(Regex(".*/login\?ReturnUrl=%2Fget_attachment%2F1219"))

        do! authenticate(page,"existingbob@test.com")

        // display of link in datatable
        do! goToPathAsync(page, "/instances/100000")
        let! _ = page.WaitForSelectorAsync("#table_main_entity_100000")

        do! checkDownloadLink
                page
                """a[href*="/get_attachment/1216"]"""
                "fixtures/attachments/1/6/100000/203/1216"
                "logo-Ubuntu.png"
    }

    [<Test>]
    member self.``main_csv_download`` () = unitTask {
        let page = self.Page


        // Check we are redirected to login page if unauthenticated
        do! isStatus(page,"/get_table_csv/19/1/10/date/Asc?searchValue=&tableType=Main", 302)
        do! page.WaitForURLAsync(Regex(".*/login\?ReturnUrl=%2Fget_table_csv%2F.*"))

        do! authenticate(page,"existingbob@test.com")

        // ----------------------------------------------
        // csv with special characters, file attachments
        // display of link in datatable
        do! goToPathAsync(page, "/instances/100000")
        let! _ = page.WaitForSelectorAsync("#table_main_entity_100000")

        do! checkDownloadLink
                page
                """#table_main_entity_100000 a.myowndb_csv_download_link"""
                "expected/entity_100000.csv"
                "entity_100000.csv"

        // ----------------------------------------------
        // csv of all, check pagination is not applied
        // display of link in datatable
        do! goToPathAsync(page, "/instances/12")
        let! _ = page.WaitForSelectorAsync("#table_main_entity_12")

        // get IDownload instance and the downloaded path
        do! checkDownloadLink
                page
                """#table_main_entity_12 a.myowndb_csv_download_link"""
                "expected/entity_12_all.csv"
                "entity_12.csv"

        // ----------------------------------------------
        // csv of all, check filter is applied
        // display of link in datatable
        let tableId = "#table_main_entity_12"
        do! goToPathAsync(page, "/instances/12")
        let! _ = page.WaitForSelectorAsync(tableId)

        // get IDownload instance and the downloaded path
        do! checkDownloadLink
                page
                """#table_main_entity_12 a.myowndb_csv_download_link"""
                "expected/entity_12_all.csv"
                "entity_12.csv"

        // check the page we're at as no influence
        let toPage = clickTablePagination page tableId
        do! toPage "3"
        // get IDownload instance and the downloaded path
        do! checkDownloadLink
                page
                """#table_main_entity_12 a.myowndb_csv_download_link"""
                "expected/entity_12_all.csv"
                "entity_12.csv"

        // check sort order is kept
        let ClickHeader = clickTableColumnHeader page tableId
        do! ClickHeader "fonction"
        // get IDownload instance and the downloaded path
        do! checkDownloadLink
                page
                """#table_main_entity_12 a.myowndb_csv_download_link"""
                "expected/entity_12_all_sort_fonction.csv"
                "entity_12.csv"


        // reset sort column
        do! goToPathAsync(page, "/instances/12")
        let! _ = page.WaitForSelectorAsync(tableId)

        // check filters are applied
        do! filterTable page tableId "nom" "e"
        // get IDownload instance and the downloaded path
        do! checkDownloadLink
                page
                """#table_main_entity_12 a.myowndb_csv_download_link"""
                "expected/entity_12_search_e.csv"
                "entity_12.csv"

        // cleared search is also respected
        do! clearSearch page "#table_main_entity_12"
        // get IDownload instance and the downloaded path
        do! checkDownloadLink
                page
                """#table_main_entity_12 a.myowndb_csv_download_link"""
                "expected/entity_12_all.csv"
                "entity_12.csv"


        // simultaneaous search and sort are applied
        do! filterTable page tableId "nom" "e"
        do! ClickHeader "prenom"
        // Introduce a small delay to ensure the right file gets downloaded
        do! Async.Sleep 300
        // get IDownload instance and the downloaded path
        do! checkDownloadLink
                page
                """#table_main_entity_12 a.myowndb_csv_download_link"""
                "expected/entity_12_search_e_sort_prenom.csv"
                "entity_12.csv"

    }
    [<Test>]
    member self.``linked_csv_download`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"existingbob@test.com")

        // ----------------------------------------------
        // csv with special characters, file attachments
        // display of link in datatable
        do! goToPathAsync(page, "/instance/73")
        let! _ = page.WaitForSelectorAsync("#table_linked_73_children_relation_7_entity_12")
        // Linked instance csv download is disabled now as the link generation
        // is incorrect for the Linked tableType.
        do! elementNotVisible(page,"a.myowndb_csv_download_link")
    }

    [<Test>]
    member self.``datatable_action_cells`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"existingbob@test.com")
        do! goToPathAsync(page, "/instances/12")

        let tableXPath= $"""//div[@id="table_main_entity_12"]"""
        let viewActionXPath = $"""{tableXPath}//tr/td[contains(@class,"myowndbactioncell")][1]/a[@href="/instance/70"]"""
        let editActionXPath = $"""{tableXPath}//tr/td[contains(@class,"myowndbactioncell")][2]/a[@href="/edit_instance/70"]"""
        let deleteActionXPath = $"""{tableXPath}//tr/td[contains(@class,"myowndbactioncell")][3]//span[@data-myowndb-action="delete"]"""

        // Check action cells count
        do! checkXPathCount (page.Locator(tableXPath))  """//tr[1]/td[contains(@class,"myowndbactioncell")]""" (equal 3)

        // Now check the action cells are visible
        do! elementVisible(page,viewActionXPath)
        do! elementVisible(page,editActionXPath)
        do! elementVisible(page,deleteActionXPath)
    }

[<TestFixture>]
type InstanceDeletionTest() =
    inherit PageTest()

    [<Test>]
    member self.``instance_deletion_from_datatable`` () = unitTask {
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")
        let formAncestor = """div[data-form-entity-id="100001"]"""
        let tableXPath = """//div[@id="table_main_entity_100001"]"""

        // view instances for entity id 12
        do! goToPathAsync(page, "/instances/100001")

        // We have possibly 11 entries later in the test, so display more entries per page to be sure we have all rows
        do! setPerPage page "#table_main_entity_100001" 20

        do! page.GetByText("Add new").ClickAsync()
        do! elementVisible(page,formAncestor)


        let values =
            {
                name = "Jon-Peter"
                notes = "Notes about Jon-Peter, though there's not so much to say"
                birthdate = "1992-08-24"
                count = "37"
                approved = "Yes"
                email = "jon-peter@pro.example.com"
                website = "http://jon-peter.pro.example.com"
                logo = "logo-jon-peter-small.png"
                picture = "picture-jon-peter-medium.jpg"
            }

        do! page.PauseAsync()
        // First do insertion
        let! checkRowInserted = getRowsDifferenceChecker page tableXPath 1
        do! fillSubmitAndValidatedFullForm100001 page values true
        do! checkFormEntryInTable page values
        do! checkRowInserted ()

        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)
        // Get the link to the view page of the newly created instance
        let viewLink = getRowElement page tableXPath "Jon-Peter" """a[starts-with(@href,"/instance/")]"""
        let! viewURL = viewLink.GetAttributeAsync("href")
        let lastSlashIndex = viewURL.LastIndexOf("/")
        let instanceId = viewURL.Remove(0,lastSlashIndex+1)
        let! dvIdsResult =
            DBAction.queryAction "select id from detail_values where type='FileAttachmentDetailValue' and instance_id=%d" (int instanceId)
            |> DBAction.run client
        // Extract detailValueIds from database, needed to determine path to attachment on disk
        let attachmentIds =
            match dvIdsResult with
            | Error es ->
                sprintf "should have been ok but got error %s" (es |> Error.toString)
                |> should equal "no error expected here"
                Unchecked.defaultof<_>
            | Ok ids ->
                ids

        // Helper to validate presence or not of attachments on disk
        let checkAttachmentsOnDisk (isPresent:bool) (ids) =
            ids
            |> List.iter(fun id ->
                System.IO.File.Exists($"run/attachments/1/6/100001/{id}")
                |> should equal isPresent
            )
        // Check the file attachments are on disk
        attachmentIds
        |> checkAttachmentsOnDisk true

        // Delete instance but cancel at confirmation
        let! checkNoRowDeleted = getRowsDifferenceChecker page tableXPath 0
        do! clickActionCell page tableXPath "Jon-Peter" "delete"
        do! checkRowHasDeleteConfirmation page tableXPath "Jon-Peter"
        do! cancelRowDeletion page tableXPath "Jon-Peter"
        do! checkRowHasNoDeleteConfirmation page tableXPath "Jon-Peter"
        do! checkNoRowDeleted()
        attachmentIds
        |> checkAttachmentsOnDisk true

        // Delete instance
        let! checkRowDeleted = getRowsDifferenceChecker page tableXPath -1
        do! clickActionCell page tableXPath "Jon-Peter" "delete"
        do! checkRowHasDeleteConfirmation page tableXPath "Jon-Peter"
        let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task {
            do! clickRowDeleteConfirmation page tableXPath "Jon-Peter"
        })
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)
        // This will also wait until the row is not there anymore
        do! checkRowAbsent page tableXPath "Jon-Peter"
        // This checks the number of rows
        do! checkRowDeleted ()
        // Check the file attachments were removed from disk
        attachmentIds
        |> checkAttachmentsOnDisk false

        ()
    }
