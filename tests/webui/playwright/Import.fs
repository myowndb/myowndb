namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open System.Collections.Generic
open MyOwnDB.Tests.Helpers

module Helpers =
    // Helper to check links creation and deletion in database
    let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

    // All fields of the csv, also names of the details of the entity
    let allFields = ["name";"notes";"birthdate";"count";"approved";"email";"website"]

      // Check these fields have the default mapping because the csv
      // column name is the same as a detail name of the entity.
      // Use async for tail recursion
    let rec checkDefaultMappingsFor (page:IPage)(names:string list) = async {
      match names with
      | name:: rest ->
        let nameField = page.Locator($"#{name}")
        let! nameMapping = nameField.EvaluateAsync<string>("el => el.options[el.selectedIndex].label")|> Async.AwaitTask
        nameMapping |> should equal name
        do! checkDefaultMappingsFor page rest
      | [] ->
        return ()
    }
    let rec checkNoMappingsFor (page:IPage)(names:string list) = async {
      match names with
      | name:: rest ->
        let nameField = page.Locator($"#{name}")
        let! nameMapping = nameField.EvaluateAsync<string>("el => el.options[el.selectedIndex].label")|> Async.AwaitTask
        nameMapping |> should equal "---"
        do! checkNoMappingsFor page rest
      | [] ->
        return ()
    }
open Helpers

[<TestFixture;Parallelizable>]
type ImportDatabaseReadOnlyTests() =
    inherit PageTest()


    [<Test>]
    member self.``import_default_mappings`` () = unitTask {
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")

        do! goToPathAsync(page, "/instances/100001")
        // Upload file
        do! page.ClickAsync("""//button[contains(@class,"myowndbimportcsv")]""")
        do! page.WaitForURLAsync("**/import/100001")
        do! selectFileForFieldSelector page """//input[@class="myowndbfileselection"]""" "fixtures/csv/pw_import_Form_tests_invalid_data.csv"
        do! page.ClickAsync("""//button[text()="Submit"]""")
        // Check mappings page
        do! checkDefaultMappingsFor page allFields |> Async.StartAsTask
    }

    [<Test>]
    member self.``import_errors`` () = unitTask {
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")

        do! goToPathAsync(page, "/instances/100001")
        // Take state snapshot
        let expectedChanges =
          {
             DBState.zeroState with
               imports = 1
               maxImportId = 1
          }
        let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client [] expectedChanges
        // Upload file
        do! page.ClickAsync("""//button[contains(@class,"myowndbimportcsv")]""")
        do! page.WaitForURLAsync("**/import/100001")
        do! selectFileForFieldSelector page """//input[@class="myowndbfileselection"]""" "fixtures/csv/pw_import_Form_tests_invalid_data.csv"
        do! page.ClickAsync("""//button[text()="Submit"]""")
        // Check we have the default mappings so we don't have to set them ourselves
        // This feature is covered in another test, but including it here makes this test clearer
        // and ensures mappings are there in the case we run this test individually
        do! checkDefaultMappingsFor page allFields |> Async.StartAsTask
        do! page.ClickAsync("""//button[text()="Import"]""")
        do! elementsVisible
              page
              [
                """//li[text()="DateDetailValue my birthdate invalid for detail birthdate"]"""
                """//li[text()="integer detail value five for count could not be parsed"]"""
                """//li[text()="No proposition found for value True to be mapped to approved"]"""
                """//li[text()="Email joe.com invalid for detail email"]"""
                """//li[text()="WebURL joe's website invalid for detail website"]"""
              ]
            |> Async.StartAsTask
        // Check nothing was created in database
        do! checkDifferenceNow ()
    }

    [<Test>]
    member self.``import_many_errors`` () = unitTask {
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")

        do! goToPathAsync(page, "/instances/100001")
        let expectedChanges =
          {
             DBState.zeroState with
               imports = 1
               maxImportId = 1
          }
        // Take state snapshot
        let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client [] expectedChanges
        // Upload file
        do! page.ClickAsync("""//button[contains(@class,"myowndbimportcsv")]""")
        do! page.WaitForURLAsync("**/import/100001")
        do! selectFileForFieldSelector page """//input[@class="myowndbfileselection"]""" "fixtures/csv/pw_import_Form_tests_many_invalid_data.csv"
        do! page.ClickAsync("""//button[text()="Submit"]""")
        // Check we have the default mappings so we don't have to set them ourselves
        // This feature is covered in another test, but including it here makes this test clearer
        // and ensures mappings are there in the case we run this test individually
        do! checkDefaultMappingsFor page allFields |> Async.StartAsTask
        do! page.ClickAsync("""//button[text()="Import"]""")
        do! elementsVisible
              page
              [
                """//li[text()="DateDetailValue my birthdate invalid for detail birthdate"]"""
                """//li[text()="integer detail value five for count could not be parsed"]"""
                """//li[text()="No proposition found for value True to be mapped to approved"]"""
                """//li[text()="Email joe.com invalid for detail email"]"""
                """//li[text()="WebURL joe's website invalid for detail website"]"""
                """//li[text()="WebURL june.test.com invalid for detail website"]"""
              ]
            |> Async.StartAsTask
        do! Assertions.Expect(page.Locator("""//div[contains(@class,"alert-danger")]//li""")).ToHaveCountAsync(6)
        // Check nothing was created in database
        do! checkDifferenceNow ()
    }
[<TestFixture;>]
type ImportDatabaseAlteringTests() =
    inherit PageTest()

    // Helper to check links creation and deletion in database
    let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

    let undoXPath = """//button[contains(@class,"myowndbundoimport")]"""
    let undoImport (page:IPage) (undoXPath:string) expectedChanges = unitTask {
        let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client [] expectedChanges
        do! page.ClickAsync(undoXPath)
        do! elementVisible(page,$"""//{confirmationYesXPath}""")
        do! elementVisible(page,$"""//{confirmationNoXPath}""")
        let! request =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                                do! page.ClickAsync($"//{confirmationYesXPath}")
                                                                ()
                                                         })
        do! textIsVisible(page, "Reverted import")
        do! checkDifferenceNow ()
    }

    [<Test>]
    member self.``import_full_rows`` () = unitTask {
        // Import a csv with a value for each detail of the entity
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")

        do! goToPathAsync(page, "/instances/100001")
        // Take state snapshot
        let expectedChanges =
          {
             DBState.zeroState with
               instances = 99L
               detailValues = 396L
               dateDetailValues = 99L
               ddlDetailValues = 99L
               integerDetailValues = 99L
               imports = 1L
               maxInstanceId = 99
               maxDetailValueId = 396
               maxDateDetailValueId = 99
               maxDdlDetailValueId = 99
               maxIntegerDetailValueId = 99
               maxImportId = 1
          }
        let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client [] expectedChanges
        // Upload file
        do! page.ClickAsync("""//button[contains(@class,"myowndbimportcsv")]""")
        do! page.WaitForURLAsync("**/import/100001")
        do! selectFileForFieldSelector page """//input[@class="myowndbfileselection"]""" "fixtures/csv/pw_import_Form_tests_valid_data.csv"
        do! page.ClickAsync("""//button[text()="Submit"]""")
        // Check we have the default mappings so we don't have to set them ourselves
        // This feature is covered in another test, but including it here makes this test clearer
        // and ensures mappings are there in the case we run this test individually
        do! checkDefaultMappingsFor page allFields |> Async.StartAsTask
        // Import can take some time, wait for it to be done before further checks
        let! request =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                                do! page.ClickAsync("""//button[text()="Import"]""")
                                                                ()
                                                         })
        // Check all items were created in the database
        do! elementsVisible
              page
              [
                """//div[contains(@class,"alert-success") and text()="Imported 99 instances"]"""
                """//a[@href="/instances/100001" and text() = "Go back to the list"]"""
                undoXPath
              ]
        do! checkDifferenceNow ()

        // Undo import, so as to not impact other tests
        let expectedChanges =
          {
             DBState.zeroState with
               instances = -99L
               detailValues = -396L
               dateDetailValues = -99L
               ddlDetailValues = -99L
               integerDetailValues = -99L
          }
        do! undoImport page undoXPath expectedChanges
    }

    [<Test>]
    member self.``import_partial_rows`` () = unitTask {
        // Import a csv with details not having a column in the csv
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")

        do! goToPathAsync(page, "/instances/100001")
        // Take state snapshot
        let expectedChanges =
          {
             DBState.zeroState with
               instances = 99L
               detailValues = 297L
               dateDetailValues = 99L
               integerDetailValues = 99L
               imports = 1L
               maxInstanceId = 99
               maxDetailValueId = 297
               maxDateDetailValueId = 99
               maxIntegerDetailValueId = 99
               maxImportId = 1
          }
        let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client [] expectedChanges
        // Upload file
        do! page.ClickAsync("""//button[contains(@class,"myowndbimportcsv")]""")
        do! page.WaitForURLAsync("**/import/100001")
        do! selectFileForFieldSelector page """//input[@class="myowndbfileselection"]""" "fixtures/csv/pw_import_Form_tests_valid_data_missing_details.csv"
        do! page.ClickAsync("""//button[text()="Submit"]""")

        // Check we have the default mappings so we don't have to set them ourselves
        do! checkDefaultMappingsFor page (allFields|> List.filter (fun v -> v<>"notes" && v<>"approved")) |> Async.StartAsTask
        do! checkNoMappingsFor page ["notes";"approved"]

        // Import can take some time, wait for it to be done before further checks
        let! request =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                                do! page.ClickAsync("""//button[text()="Import"]""")
                                                                ()
                                                         })
        // Check all items were created in the database
        do! elementsVisible
              page
              [
                """//div[contains(@class,"alert-success") and text()="Imported 99 instances"]"""
                """//a[@href="/instances/100001" and text() = "Go back to the list"]"""
              ]
        do! checkDifferenceNow ()

        let expectedChanges =
          {
             DBState.zeroState with
               instances = -99L
               detailValues = -297L
               dateDetailValues = -99L
               integerDetailValues = -99L
          }
        let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client [] expectedChanges
        do! undoImport page undoXPath expectedChanges
        do! checkDifferenceNow()
    }
