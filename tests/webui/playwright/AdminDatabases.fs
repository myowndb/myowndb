namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open System.Collections.Generic
open MyOwndbForms
open FSharpPlus
open MyOwnDB.Tests.Helpers
open AdminHelpers


[<TestFixture;Parallelizable>]
type AdminDatabasesAccess() =
    inherit PageTest()
    [<Test>]
    member self.``admin_databases_access`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"admin@test.com")
        do! checkMenusInLanguageAsync(page,adminMenu,"en")
        do! goToPathAsync(page, "/admin/databases")
        ()
    }

type AdminDatabases() =
    inherit PlaywrightTest()
    [<Test>]
    member self.``admin_databases_manage`` () = unitTask {
        let pw = self.Playwright
        let! adminBrowser = pw.Chromium.LaunchAsync()
        let! adminPage = adminBrowser.NewPageAsync()
        //let! userBrowser = pw.Chromium.LaunchAsync()
        //let! userPage = userBrowser.NewPageAsync()
        let tableXPath = """//table[@id="admin_databases_table"]"""
        let takeAction =
            clickActionCell adminPage tableXPath
        let formSelector = """//form[@id="admin_databases_form"]"""
        // This submits the creation form, also validating the label of the button is correct
        let save() =
            adminPage.Locator("""button:text("Create")""").ClickAsync()
        // This submits the update form, also validating the label of the button is correct
        let update() =
            adminPage.Locator("""button:text("Update")""").ClickAsync()
        // database client for db content checks
        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

        // Helper to get db changes checking function
        let initDatabasesInDBCheck (changes:{|all:int64;forAccount:int64|}) = task {
            let! preState, checkDifferenceNow =
                DBState.startWithOnlyCustomQueries client
                    [
                        changes.all,"select count(*) from databases "
                        changes.forAccount,"select count(*) from databases where account_id=1"
                    ]
            return checkDifferenceNow
        }


        do! authenticate(adminPage,"admin@test.com")
        do! checkMenusInLanguageAsync(adminPage,adminMenu,"en")
        do! goToPathAsync(adminPage, "/admin/databases")
        do! checkAdminTable adminPage tableXPath "tables/admin/databases/from_fixtures.html"

        let editedDatabase = "database_without_details"

        // Edit database name
        // ----------------------------------------------------
        // We will edit an admin user to be a standard user
        let! checkDifferenceNow = initDatabasesInDBCheck {|all= 0L; forAccount=0L|}
        do! takeAction editedDatabase "edit"
        do! elementVisible(adminPage,formSelector)
        do! adminPage.GetByLabel("Name").FillAsync("database without details")
        do! update()
        // Form is hidden after ssaving
        do! elementNotVisible(adminPage,formSelector)
        // With infirmative feedback displayed
        do! textIsVisible(adminPage,"Database saved successfully")
        // The table is updated
        do! checkAdminTable adminPage tableXPath "tables/admin/databases/after_edit.html"
        do! checkDifferenceNow()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/databases/after_edit.html"

        // Add new database
        // ------------
        // We create a standard user
        let! checkDifferenceNow = initDatabasesInDBCheck {|all= 1L; forAccount=1L|}
        let editedDatabase="New database"
        do! adminPage.Locator("""button:text("Add new")""").ClickAsync()
        do! textIsNotVisible(adminPage,"Database saved successfully")
        do! elementVisible(adminPage,formSelector)
        do! adminPage.GetByLabel("Name").FillAsync(editedDatabase)
        do! save()
        do! textIsVisible(adminPage,"Database saved successfully")
        // Form is hidden after ssaving
        do! elementNotVisible(adminPage,formSelector)
        do! Async.Sleep 200
        // The table is updated
        do! checkAdminTable adminPage tableXPath "tables/admin/databases/after_add.html"
        do! checkDifferenceNow()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/databases/after_add.html"

        // Check that the admin page for the new database is displayed successfully
        do! clickActionCell adminPage """//table[@id="admin_databases_table"]""" editedDatabase "view"
        do! elementVisible(adminPage, """//table[@id="admin_entities_table"]""")

        // This should be added when a constraint on unique database names per account are added.
        // It is not added now because there are accounts with duplicate database names. Although I suspect
        // these are unused accounts, it is a matter for later. I keep the test code here though.
        // // Attempt to add existing database
        // // ----------------------------
        // // Unsuccessful, so no change in database
        // let! checkDifferenceNow = initDatabasesInDBCheck {|all= 0L; forAccount=0L|}
        // let editedUser = "New database"
        // do! adminPage.Locator("""button:text("Add new")""").ClickAsync()
        // do! textIsNotVisible(adminPage,"Database saved successfully")
        // do! elementVisible(adminPage,formSelector)
        // do! adminPage.GetByLabel("Name").FillAsync(editedUser)
        // do! save()
        // do! textIsNotVisible(adminPage,"Database saved successfully")
        // do! textIsVisible(adminPage,"An error occured")
        // do! checkDifferenceNow()
        // do! client.Close()
        // // Reload page to ensure fresh fetch from db and check displayed table
        // do! reload adminPage
        // do! checkAdminTable adminPage tableXPath "tables/admin/databases/after_add.html" 2 5
    }
