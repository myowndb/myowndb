namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open System.Collections.Generic
open MyOwndbForms
open FSharpPlus
open MyOwnDB.Tests.Helpers
open AdminHelpers


[<TestFixture;Parallelizable>]
type AdminUsersAccess() =
    inherit PageTest()
    [<Test>]
    member self.``admin_users_access`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"admin@test.com")
        do! checkMenusInLanguageAsync(page,adminMenu,"en")
        do! goToPathAsync(page, "/admin/users")
        ()
    }

type AdminUsers() =
    inherit PlaywrightTest()
    [<Test>]
    member self.``admin_users_manage`` () = unitTask {
        let pw = self.Playwright
        let! adminBrowser = pw.Chromium.LaunchAsync()
        let! adminPage = adminBrowser.NewPageAsync()
        let! userBrowser = pw.Chromium.LaunchAsync()
        let! userPage = userBrowser.NewPageAsync()
        let tableXPath = """//table[@id="admin_users_table"]"""
        let takeAction =
            clickActionCell adminPage tableXPath
        let formSelector = """//form[@id="admin_users_form"]"""
        let selectUserType s =
            adminPage.Locator($"""{formSelector}//select[@id="userEditionUsertype"]""").SelectOptionAsync(SelectOptionValue(Label=s))
        let save() =
            adminPage.Locator("""button:text("Save")""").ClickAsync()
        let setPasswordAndLoginLogout (userPage:IPage)(editedUser:string)(newPassword:string) = unitTask {
            // Check a mail is sent and extract the password reset link
            do! mailSentTo(editedUser)
            let! mail= firstMailTo(editedUser)
            let link = firstLinkInMail(mail)
            cleanMailsFor(editedUser)
            // Got to URL in mail
            let! _r = goToUrlAsync(userPage,link)
            // Set new password
            do! userPage.FillAsync("#passwordInput", newPassword)
            do! userPage.FillAsync("#confirmationInput", newPassword)
            let! _ =  submitNewPassword(userPage)
            do! errorNotVisibleAsync(userPage)
            do! infoVisible(userPage,"Your password was updated, you can now try to log in!")
            // Check the new password aloows to log in
            do! checkLoginThenLogoutStandardUser(userPage,editedUser,newPassword)
        }
        // database client for db content checks
        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

        // Helper to get db changes checking function
        let initUsersInDBCheck (changes:{|standardUsers:int64;adminUsers:int64|}) = task {
            let! preState, checkDifferenceNow =
                DBState.startWithOnlyCustomQueries client
                    [
                        changes.adminUsers,"select count(*) from users where user_type_id=1"
                        changes.standardUsers,"select count(*) from users where user_type_id=2"
                    ]
            return checkDifferenceNow
        }


        do! authenticate(adminPage,"admin@test.com")
        do! checkMenusInLanguageAsync(adminPage,adminMenu,"en")
        do! goToPathAsync(adminPage, "/admin/users")
        do! checkAdminTable adminPage tableXPath "tables/admin/users/from_fixtures.html"

        let editedUser = "mohsinuser@company.it"
        do! checkLoginThenLogoutAdminUser(userPage,editedUser, passwords.Item(editedUser))

        // Edit user including user type from admin to standard
        // ----------------------------------------------------
        // We will edit an admin user to be a standard user
        let! checkDifferenceNow = initUsersInDBCheck {|adminUsers= -1L; standardUsers=1L|}
        do! takeAction editedUser "edit"
        do! elementVisible(adminPage,formSelector)
        let! _r = selectUserType "Standard"
        do! adminPage.GetByLabel("Firstname").FillAsync("Martin")
        do! adminPage.GetByLabel("Lastname").FillAsync("Under")
        do! save()
        // Form is hidden after ssaving
        do! elementNotVisible(adminPage,formSelector)
        // With infirmative feedback displayed
        do! textIsVisible(adminPage,"User saved successfully")
        // The table is updated
        do! checkAdminTable adminPage tableXPath "tables/admin/users/after_user_edit.html"
        // And the user can login, and sees correct menus
        do! checkLoginThenLogoutStandardUser(userPage,editedUser, passwords.Item(editedUser))
        do! checkDifferenceNow()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/users/after_user_edit.html"

        // Reset password for that same user
        // ---------------------------------
        // A reset of password won't change the types of users
        let! checkDifferenceNow = initUsersInDBCheck {|adminUsers= 0L; standardUsers=0L|}
        // Ensure we do not have mails waiting for that user
        do! noMailSentTo(editedUser)
        let newPassword="N3w_Password!"
        do! takeAction editedUser "resetpw"
        // Mail handling is asynchronous via hangfire, let's sleep a bit
        do! Async.Sleep 100
        do! setPasswordAndLoginLogout userPage editedUser newPassword
        do! checkDifferenceNow()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/users/after_user_edit.html"

        // Add new user
        // ------------
        // We create a standard user
        let! checkDifferenceNow = initUsersInDBCheck {|adminUsers= 0L; standardUsers=1L|}
        let editedUser="gg@example.com"
        do! adminPage.Locator("""button:text("Add new")""").ClickAsync()
        do! textIsNotVisible(adminPage,"User saved successfully")
        do! elementVisible(adminPage,formSelector)
        let! _r = selectUserType "Standard"
        do! adminPage.GetByLabel("Firstname").FillAsync("Gavin")
        do! adminPage.GetByLabel("Lastname").FillAsync("Grubber")
        do! adminPage.GetByLabel("Email").FillAsync(editedUser)
        do! save()
        do! textIsVisible(adminPage,"User saved successfully")
        // Form is hidden after ssaving
        do! elementNotVisible(adminPage,formSelector)
        // With infirmative feedback displayed
        do! textIsVisible(adminPage,"User saved successfully")
        // The table is updated
        do! checkAdminTable adminPage tableXPath "tables/admin/users/after_add.html"
        do! setPasswordAndLoginLogout userPage editedUser newPassword
        do! checkDifferenceNow()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/users/after_add.html"
        // Delete user
        // ----------------------------------------------------
        let! checkDifferenceNow = initUsersInDBCheck {|adminUsers= 0L; standardUsers= -1L|}
        do! takeAction editedUser "delete"
        do! checkRowHasDeleteConfirmation adminPage tableXPath editedUser
        do! clickRowDeleteConfirmation adminPage tableXPath editedUser
        do! Async.Sleep 200
        do! checkAdminTable adminPage tableXPath "tables/admin/users/after_user_edit.html"
        do! checkDifferenceNow()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/users/after_user_edit.html"

        // Attempt to add existing user
        // ----------------------------
        // Unsuccessful, so no user change
        let! checkDifferenceNow = initUsersInDBCheck {|adminUsers= 0L; standardUsers=0L|}
        let editedUser = "mohsinuser@company.it"
        cleanMailsFor editedUser
        do! adminPage.Locator("""button:text("Add new")""").ClickAsync()
        do! textIsNotVisible(adminPage,"User saved successfully")
        do! elementVisible(adminPage,formSelector)
        let! _r = selectUserType "Standard"
        do! adminPage.GetByLabel("Firstname").FillAsync("Martin")
        do! adminPage.GetByLabel("Lastname").FillAsync("Under")
        do! adminPage.GetByLabel("Email").FillAsync(editedUser)
        do! save()
        do! textIsNotVisible(adminPage,"User saved successfully")
        do! textIsVisible(adminPage,"The email mohsinuser@company.it is already registered.")
        do! Async.Sleep 200
        do! noMailSentTo(editedUser)
        do! checkDifferenceNow()
        do! client.Close()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/users/after_user_edit.html"
        ()
    }
