namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open System.Collections.Generic
open MyOwndbForms
open FSharpPlus
open MyOwnDB.Tests.Helpers
open AdminHelpers


[<TestFixture;Parallelizable>]
type AdminentityAccess() =
    inherit PageTest()
    [<Test>]
    member self.``admin_entity_access`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"admin@test.com")
        do! checkMenusInLanguageAsync(page,adminMenu,"en")
        do! goToPathAsync(page, "/admin/entity/52")
        ()
    }

type Adminentity() =
    inherit PlaywrightTest()
    [<Test>]
    member self.``admin_entity_manage`` () = unitTask {
        let entityId = 52
        let pw = self.Playwright
        let! adminBrowser = pw.Chromium.LaunchAsync()
        let! adminPage = adminBrowser.NewPageAsync()
        let! userBrowser = pw.Chromium.LaunchAsync()
        let! userPage = userBrowser.NewPageAsync()
        let entityDetailsXPath = """//ul[@id="entityDetails"]"""
        let nameDivXPath = """li/div[position()=1]"""
        let selectNameDivXPath (s:string) =  $"""{entityDetailsXPath}//div[position()=1 and  contains(.,"{s}")]"""
        let actionsDivXPath = """li/div[position()=2]"""
        let save() =
            adminPage.Locator("""button:text("Save")""").ClickAsync()
        // entity client for db content checks
        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

        do! authenticate(adminPage,"admin@test.com")
        do! checkMenusInLanguageAsync(adminPage,adminMenu,"en")
        do! goToPathAsync(adminPage, $"/admin/entity/{entityId}")

        do! authenticate(userPage,"existingbob@test.com")

        let namesXPath = $"""{entityDetailsXPath}//{nameDivXPath}"""
        // Check number of details
        let checkEntityDetailsNumber (entityId:int)(n:int) = task {
          do! Assertions.Expect(adminPage.Locator(namesXPath)).ToHaveCountAsync(n)
          // Check consistency with database
          do!
              DBAction.querySingleAction "select count(*) from entities2details where entity_id=%d" entityId
              |> DBAction.run client
              |> Async.map (checkOkDBResult [int64 n])
        }
        let checkCount = checkEntityDetailsNumber entityId

        // Check names of entity details, also validating the order
        let checkEntityDetailNames (entityId:int)(names:string seq)= task {
          let! entityDetailNames = adminPage.Locator(namesXPath).AllTextContentsAsync()
          entityDetailNames |> should equal names
          // Check consistency with database
          do!
              DBAction.queryAction "select d.name from entities2details ed join details d on (d.id=ed.detail_id) where ed.entity_id=%d order by ed.display_order" entityId
              |> DBAction.run client
              |> Async.map (checkOkDBResult (names|> List.ofSeq))
        }
        let checkNames = checkEntityDetailNames entityId

        // Check display in list of entity details
        let checkEntityDetailDisplay (entityId:int)(displays:bool seq) = task {
          // Map bool indicators passed in to the text actually displayed on the page
          let displayTexts =
            displays
            |> Seq.map(fun b -> if b then "Displayed in list view" else "Not displayed in list view")
          let actionsXPath =  $"""{entityDetailsXPath}//{actionsDivXPath}"""
          let!  displayedInLists = adminPage.Locator(actionsXPath).AllTextContentsAsync()
          displayedInLists |> should equal displayTexts
          // Check consistency with database
          do!
              DBAction.queryAction "select  displayed_in_list_view from entities2details where entity_id=%d order by display_order" entityId
              |> DBAction.run client
              |> Async.map (checkOkDBResult (displays|> List.ofSeq))
        }
        let checkDisplayInList = checkEntityDetailDisplay entityId

        // Build XPath selector to a detail's action
        let detailActionXPath (name:string)(action:string) =
          $"""{selectNameDivXPath name}//following-sibling::div//div[@data-myowndb-action="{action}"]"""
        // Click a detail's action
        let clickDetailActionAsync name action =
          adminPage.Locator(detailActionXPath name action).ClickAsync()


        do! checkCount 1
        do! checkNames  [|"unique_detail_db_8"|]
        do! checkDisplayInList  [|false|]
        // No detail visible in list view
        do! goToPathAsync(userPage, $"/instances/{entityId}")
        do! textIsVisible(userPage,"empty details list")

        // Make unique detail displayed in list view
        let! _request = adminPage.RunAndWaitForRequestFinishedAsync( fun () ->
              adminPage.ClickAsync(detailActionXPath "unique_detail_db_8" "show-in-list")
            )
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle)
        // Wait for the updated div to be displayed. Not doing so caused trouble here, the following check being done before the display was updated.
        do! elementVisible(adminPage,detailActionXPath "unique_detail_db_8" "hide-in-list")
        do! checkDisplayInList  [|true|]
        do! reload userPage
        // We don't get the empty details list message anymore
        do! textIsNotVisible(userPage,"empty detail list")


        // Make unique detail not displayed in list view as initially
        // We keep a detail not displayed in list view to be able to test such a detail
        // is still displayed in the form
        let! _request = adminPage.RunAndWaitForRequestFinishedAsync(fun () ->
                  adminPage.ClickAsync(detailActionXPath "unique_detail_db_8" "hide-in-list"))
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        // Wait for the updated div to be displayed. Not doing so caused trouble here, the following check being done before the display was updated.
        do! elementVisible(adminPage,detailActionXPath "unique_detail_db_8" "show-in-list")
        do! checkDisplayInList  [|false|]

        // Add a new int detail
        do! adminPage.Locator("""button:text(" Add field based on new detail")""").ClickAsync()
        do! adminPage.GetByLabel("Detail name").FillAsync("Count")
        do! chooseDdlValue adminPage "#newDetailDataType" "integer"
        let! _request = adminPage.RunAndWaitForRequestFinishedAsync(fun () -> task {
                          do! adminPage.ClickAsync("#submitNewEntityDetail")
                        }) |> Async.AwaitTask
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        // Check new detail was added
        do! checkCount 2
        do! checkNames  [|"unique_detail_db_8"; "Count"|]
        do! checkDisplayInList  [|false; true|]

        // Add a new date detail
        do! adminPage.GetByLabel("Detail name").FillAsync("date")
        do! chooseDdlValue adminPage "#newDetailDataType" "date"
        let! _request = adminPage.RunAndWaitForRequestFinishedAsync(fun () -> task {
                          do! adminPage.ClickAsync("#submitNewEntityDetail")
                        }) |> Async.AwaitTask
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        // Check new detail was added
        do! checkCount 3
        do! checkNames  [|"unique_detail_db_8"; "Count"; "date"|]
        do! checkDisplayInList  [|false; true; true|]


        // Helper to check new propositions list
        let checkPropositionsList (l:string seq) = task {
          let lisXPath = """//ul[@id="newPropositionsList"]/li"""
          do! Assertions.Expect(adminPage.Locator(lisXPath)).ToHaveCountAsync(l|>Seq.length)
          let! newPropositions = adminPage.Locator(lisXPath).AllTextContentsAsync()
          newPropositions |> should equal l
        }
        // Add a new drop down list detail
        do! adminPage.GetByLabel("Detail name").FillAsync("alternatives")
        do! chooseDdlValue adminPage "#newDetailDataType" "choose in list"
        do! elementVisible(adminPage,"#newProposition")
        do! adminPage.GetByLabel("Add proposition").FillAsync("Option 1")
        do! adminPage.PressAsync("#newProposition", "Enter")
        do! checkPropositionsList [|"Option 1"|]
        do! adminPage.GetByLabel("Add proposition").FillAsync("Option 2")
        do! adminPage.PressAsync("#newProposition", "Enter")
        do! checkPropositionsList [|"Option 1"; "Option 2"|]
        do! adminPage.GetByLabel("Add proposition").FillAsync("Option 3")
        do! adminPage.PressAsync("#newProposition", "Enter")
        do! checkPropositionsList [|"Option 1"; "Option 2";"Option 3"|]
        let! _request = adminPage.RunAndWaitForRequestFinishedAsync(fun () -> task {
                          do! adminPage.ClickAsync("#submitNewEntityDetail")
                        }) |> Async.AwaitTask
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        // Check new detail was added
        do! checkCount 4
        do! checkNames  [|"unique_detail_db_8"; "Count"; "date"; "alternatives"|]
        do! checkDisplayInList  [|false; true; true; true|]

        // Helper to check form
        let textFieldPresent (name:string) =
          elementVisible(userPage, $"""//input[contains(@id,"{name}-")]""")
        // Check the form is ok
        do! reload userPage
        do! userPage.ClickAsync("""button:text(" Add new")""")
        // Detail not displayed in list, but still present in form
        do! textFieldPresent("unique_detail_db_8")
        do! textFieldPresent("Count")
        do! textFieldPresent("date")
        // Drop down list is displayed as expected
        let! alternativesDisplayedInForm =
          userPage.Locator("""//select[contains(@id,"alternatives-")]/option""").AllTextContentsAsync()
        alternativesDisplayedInForm |> should equal [|"None";"Option 1";"Option 2"; "Option 3"|]

        // Reorder details
        do! clickDetailActionAsync "date" "up"
        do! waitForMutationAsync adminPage entityDetailsXPath
        do! checkCount 4
        do! checkNames  [|"unique_detail_db_8"; "date"; "Count"; "alternatives"|]
        do! checkDisplayInList  [|false; true; true; true|]

        do! clickDetailActionAsync "unique_detail_db_8" "down"
        do! waitForMutationAsync adminPage entityDetailsXPath
        do! checkCount 4
        do! checkNames  [|"date";"unique_detail_db_8"; "Count"; "alternatives"|]
        do! checkDisplayInList  [|true;false; true; true|]

        // First can't go up
        do! clickDetailActionAsync "date" "up"
        // There should be no network request here, so not possible to run RunAndWaitForRequestFinishedAsync.
        // There's also no mutation of the elements, so no change to wait for.
        // We introduce a sleep here just in case a network request would be erroneously made, so that the update
        // of the ui would have the time to be done
        do! Async.Sleep 200
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle)
        do! checkCount 4
        do! checkNames  [|"date";"unique_detail_db_8"; "Count"; "alternatives"|]
        do! checkDisplayInList  [|true;false; true; true|]
        // Last can't go down
        do! clickDetailActionAsync "alternatives" "down"
        // Need to sleep here, see previous sleep for explanation
        do! Async.Sleep 200
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle)
        do! checkCount 4
        do! checkNames  [|"date";"unique_detail_db_8"; "Count"; "alternatives"|]
        do! checkDisplayInList  [|true;false; true; true|]

        // Using existing details
        let usableDetailsXPath = """//table[@id="adminUsableDetails"]"""
        let usableDetailLinkXPath name =
          $"""{usableDetailsXPath}//tr/td[position()=1 and contains(.,"{name}")]//following-sibling::td[@data-myowndb-action="link"]"""
        let linkExistingDetailAsync name =
          adminPage.Locator(usableDetailLinkXPath name).ClickAsync()
        do! adminPage.ClickAsync("""button:text("Define field with existing detail")""")
        do! elementVisible(adminPage,usableDetailsXPath)
        do! linkExistingDetailAsync "unlinked_detail"
        do! waitForMutationAsync adminPage entityDetailsXPath
        do! checkCount 5
        do! checkNames  [|"date"; "unique_detail_db_8"; "Count"; "alternatives";"unlinked_detail"|]
        do! checkDisplayInList  [|true; false; true; true; true;|]

        // Unlink detail
        // Uses the advanced mutation waiter, though it is not required in the end.
        do! runAndWaitForMutationAsync
              adminPage
              entityDetailsXPath
              (fun () -> task {
                do! clickDetailActionAsync "date" "delete";
                do! adminPage.Locator($"""{detailActionXPath "date" "delete"}//span[text()="Yes"]""").ClickAsync(LocatorClickOptions(NoWaitAfter=true))
              })
        // Check the detail is not linked anymore to the entity
        do! checkCount 4
        do! checkNames  [|"unique_detail_db_8"; "Count"; "alternatives";"unlinked_detail"|]
        do! checkDisplayInList  [|false; true; true;true|]
        // The detail is now displayed as available to be added to the entity
        do! elementVisible(adminPage, usableDetailLinkXPath "date")

    }

    [<Test>]
    member self.``admin_entity_relations`` () = unitTask {
        // we're working with entities 15 (formations) and 101 (persons). We will add relations between those
        let adminPersonPath = "/admin/entity/101"
        let adminFormationPath = "/admin/entity/15"
        let pw = self.Playwright
        let! adminBrowser = pw.Chromium.LaunchAsync()
        let! adminPage = adminBrowser.NewPageAsync()
        let! userBrowser = pw.Chromium.LaunchAsync()
        let! userPage = userBrowser.NewPageAsync()
        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

        let toParentsTableLocator = """//table[@id="ToParentsRelations"]"""
        let toChildrenTableLocator = """//table[@id="ToChildrenRelations"]"""
        let toParentsFormLocator = """//div[@id="relationFormToParents"]"""
        let toChildrenFormLocator = """//div[@id="relationFormToChildren"]"""

        let checkRelationsTables (suffix:string) = task {
          do! goToPathAsync(adminPage, adminPersonPath)
          do! checkPlainTable adminPage toParentsTableLocator $"tables/admin_entity/relations/101_persons/to_parents_{suffix}.html"
          do! checkPlainTable adminPage toChildrenTableLocator $"tables/admin_entity/relations/101_persons/to_children_{suffix}.html"
          do! goToPathAsync(adminPage, adminFormationPath)
          do! checkPlainTable adminPage toParentsTableLocator $"tables/admin_entity/relations/15_formation//to_parents_{suffix}.html"
          do! checkPlainTable adminPage toChildrenTableLocator $"tables/admin_entity/relations/15_formation//to_children_{suffix}.html"
        }

        let toParentField (name:string) =
          $"""{toParentsFormLocator}//*[@id="{name}"]"""
        let toChildField (name:string) =
          $"""{toChildrenFormLocator}//*[@id="{name}"]"""

        let submitParentForm () = task {
          do! adminPage.Locator($"""{toParentsFormLocator}//button[text()="Submit"]""").ClickAsync()
        }
        let submitChildForm () = task {
          do! adminPage.Locator($"""{toChildrenFormLocator}//button[text()="Submit"]""").ClickAsync()
        }

        // Click on the edit icon of the row having a cell with text `cellContent` in the table `tableXPath`
        let editRelation (tableXPath:string) (cellContent:string) = task {
          //table[@id="ToChildrenRelations"]//td[text()="participated to"]//following-sibling::td[@data-myowndb-action="edit-relation"]
          do!
            adminPage
              .Locator($"""{tableXPath}//td[text()="{cellContent}"]//following-sibling::td[@data-myowndb-action="edit-relation"]""")
              .ClickAsync()
        }
        do! authenticate(adminPage,"admin@test.com")
        do! checkMenusInLanguageAsync(adminPage,adminMenu,"en")
        do! goToPathAsync(adminPage, adminPersonPath)

        do! authenticate(userPage,"existingbob@test.com")

        do! checkRelationsTables "from_fixtures"

        // Add parent relation
        let! _preState, checkDifferenceNow =
            DBState.startWithCustomQueries client
                [
                    1L,"select count(*) from relations where parent_id = 15 and child_id = 101"
                ]
                {
                  DBState.zeroState with
                    relations = 1L;
                    maxRelationId = 1;
                }
        do! goToPathAsync(adminPage, adminPersonPath)
        do! adminPage.ClickAsync("""//button[@id="toggleRelationFormToParents"]""")
        do! elementVisible(adminPage,toParentsFormLocator)
        do! elementNotVisible(adminPage,toChildrenFormLocator)
        // Choose target, set names and leave arities at default values
        do! chooseDdlValue adminPage (toParentField "target") "formation"
        do! adminPage.Locator(toParentField "parentToChildName").FillAsync("attended by")
        do! adminPage.Locator(toParentField "childToParentName").FillAsync("participated to")
        do! submitParentForm()
        do! checkRelationsTables "0_after_add_attended_by"
        do! checkDifferenceNow()


        // Edit to child relation
        let! _preState, checkDifferenceNow =
            DBState.startWithCustomQueries client
                [
                    0L,"select count(*) from relations where parent_id = 15 and child_id = 101"
                    1L,"select count(*) from relations where from_child_to_parent_name='followed' and parent_id = 15 and child_id = 101"
                    -1L,"select count(*) from relations where from_child_to_parent_name='participated to' and parent_id = 15 and child_id = 101"
                ]
                DBState.zeroState
        do! goToPathAsync(adminPage, adminFormationPath)
        do! editRelation toChildrenTableLocator "attended by"
        do! elementVisible(adminPage,toChildrenFormLocator)
        do! elementNotVisible(adminPage,toParentsFormLocator)
        do! adminPage.Locator(toChildField "childToParentName").FillAsync("followed")
        let! b = adminPage.Locator(toChildField "target").IsDisabledAsync()
        b|> should equal true
        // To many relation, cannot be changed
        let! b = adminPage.Locator(toChildField "parentSideType").IsDisabledAsync()
        b|> should equal true
        // To many relation, cannot be changed
        let! b = adminPage.Locator(toChildField "childSideType").IsDisabledAsync()
        b|> should equal true
        do! submitChildForm()
        // This also makes us wait for the table to be updated
        do! elementNotVisible(adminPage,toChildrenFormLocator)
        do! checkRelationsTables "1_after_edit"
        do! checkDifferenceNow()

        // Check a to one relation can be modified
        // First create a new relation
        do! goToPathAsync(adminPage, adminFormationPath)
        do! adminPage.ClickAsync("""//button[@id="toggleRelationFormToParents"]""")
        do! elementVisible(adminPage,toParentsFormLocator)
        do! elementNotVisible(adminPage,toChildrenFormLocator)
        // Choose target, set names and leave arities at default values
        do! chooseDdlValue adminPage (toParentField "target") "Persons"
        do! adminPage.Locator(toParentField "parentToChildName").FillAsync("authored by")
        do! adminPage.Locator(toParentField "childToParentName").FillAsync("author")
        do! chooseDdlValue adminPage (toParentField "parentSideType") "No"
        do! chooseDdlValue adminPage (toParentField "childSideType") "Yes"
        do! submitParentForm()
        // This also makes us wait for the table to be updated
        do! elementNotVisible(adminPage,toParentsFormLocator)
        // We don't repeat check done earlier on relation creation, we just want to check ddl disabled
        // Now edit relation
        let! _preState, checkDifferenceNow =
            DBState.startWithCustomQueries client
                [
                    0L,"select count(*) from relations where parent_id = 101 and child_id = 15"
                    1L,"select count(*) from relations where child_side_type_id=2 and parent_side_type_id=2 and  parent_id = 101 and child_id = 15"
                    -1L,"select count(*) from relations where child_side_type_id=2 and parent_side_type_id=1 and  parent_id = 101 and child_id = 15"
                ]
                DBState.zeroState
        do! editRelation toParentsTableLocator "author"
        do! elementVisible(adminPage,toParentsFormLocator)
        do! elementNotVisible(adminPage,toChildrenFormLocator)
        // To one relation, can be changed
        let! b = adminPage.Locator(toParentField "parentSideType").IsDisabledAsync()
        b|> should equal false
        // To many relation, cannot be changed
        let! b = adminPage.Locator(toParentField "childSideType").IsDisabledAsync()
        b|> should equal true
        // Change the to one to be to many
        do! chooseDdlValue adminPage (toParentField "parentSideType") "Yes"
        do! submitParentForm()
        // This also makes us wait for the table to be updated
        do! elementNotVisible(adminPage,toParentsFormLocator)
        do! checkDifferenceNow()

        ()
    }
