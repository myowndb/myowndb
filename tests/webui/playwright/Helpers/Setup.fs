namespace Tests

open System
open NUnit
open NUnit.Framework
open FSharp.Control.Tasks
open System.Threading
open Serilog
open Microsoft.Playwright
open System.Text.RegularExpressions
open Npgsql

module Database =
    open Microsoft.Extensions.DependencyInjection

    let client =
        DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

    let getWorkDatabase (connectionString:string) =
        let regex = Regex(".*;Database *= *([^;]+);.*")
        let regexmatch = regex.Match(connectionString)
        regexmatch.Groups.[1].Value

    let getPostgresConnectionString (connectionString:string) =
        Regex.Replace(connectionString, "Database=[^;]+;", "Database=postgres;")

    let connect (connectionString:string) =
        new NpgsqlConnection(connectionString)

    [<SetUpFixture>]
    type DatabaseFixture()=
        let id = Guid.NewGuid().ToString().Replace("-", "");
        let fixtures =
            [ "Account"
              "User"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              "Entities2details"
              "Relation"
              "Link" ]


        let dropAndCreateDatabase (connection:NpgsqlConnection) (db:string) = unitTask {
            do! connection.OpenAsync()
            let drop = new NpgsqlCommand($"drop database if exists {db}", connection)
            let create = new NpgsqlCommand($"create database {db}", connection)
            let! _i = drop.ExecuteNonQueryAsync()
            let! _j = create.ExecuteNonQueryAsync()
            ()
        }

        let reinitialiseClientDatabase (client:DB.DBClient) =  unitTask {
            let connectionString = client.ConnectionString()
            let workDatabase = getWorkDatabase connectionString
            let postgresConnectionString = getPostgresConnectionString connectionString
            let postgresConnection = connect postgresConnectionString
            do! dropAndCreateDatabase postgresConnection workDatabase
        }

        let applyMigrations (client:DB.DBClient) =
            let evolve = EvolveDb.Evolve(client.Connection(), fun msg -> Log.Logger.Information msg)
            evolve.Locations <- [|"migrations"|]
            evolve.Migrate()

        let resetDatabaseForClient(client:DB.DBClient) =  unitTask {
            // We need to reinitialise the database because Evolve does not support
            // down migrations.
            do! reinitialiseClientDatabase client
            applyMigrations client
        }

        let source = new CancellationTokenSource()
        let token = source.Token

        [<OneTimeSetUp>]
        member _.InitializeAsync() = unitTask {
            // We set up logging here as we want to have some logs sent to NUnit too.
            // Complete logs, including app logs, are found at tests/webui/playwright/bin/Debug/net6.0/logs/
            Log.Logger <- LoggerConfiguration()
                            .MinimumLevel.Debug()
                            .WriteTo.File("logs/myowndb.log", rollingInterval = RollingInterval.Day)
                            .WriteTo.NUnitOutput()
                            .CreateLogger()

            do! resetDatabaseForClient client

            let! () = client.Open()
            let! () = client.BeginTransaction()
            // load database records
            let! _ = Fixtures.processRecordsArray client
                                        (fixtures
                                         |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                         |> Array.ofList) |> Async.Sequential
            let! _ = Fixtures.processRecordsArrayInDir client
                                        ( [ "Instance"; "DetailValue";"Link"]
                                         |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                         |> Array.ofList)
                                        "fixtures-webui"
                                    |> Async.Sequential
            do! client.Commit()
            // We will reopen a connection in the tear down
            do! client.Close()
            // First argument is the environment, which is test in our case.
            // This prevents the app to re-run migrations
            let builder = web.Program.WebHostBuilder "test" [||]
            builder.ConfigureServices(fun context services ->
                                services.AddSingleton<TestMailSystem.Sink, TestMailSystem.Sink>(fun _ -> Info.mailSink) |> ignore
                                services.AddSingleton<web.IMailer,TestMailSystem.Mailer>() |> ignore
            ) |> ignore
            return builder.Build().StartAsync(token)
        }

        [<OneTimeTearDown>]
        member __.DisposeAsync() = unitTask {
            // internal helper functions
            let waitForInputIfRequested () =
                // Set env var KEEP_SERVER to 1 to keep the server available after the tests.
                // This allows devs to inspect with the web UI the test data loaded in the database.
                System.Environment.GetEnvironmentVariable("KEEP_SERVER")
                |> Option.ofObj
                |> Option.iter (fun v -> if v = "1" then Log.Logger.Information "Waiting as requested with KEEP_SERVER=1. You can access the ui with test data. Press enter to finish.";  Console.ReadLine() |> ignore )


            waitForInputIfRequested()

            // In the test environment connection pruning is done every second, and
            // connections are closed after being idle for 1s. Waiting 3s here should ensure
            // that every connection that was used for the application has been closed by
            // the time we count the number of open connections.
            do! Async.Sleep 3000
            let! () = client.Open()
            let workDatabase = getWorkDatabase (client.ConnectionString())

            let! (result:DBResult.DBResult<int32>) = DBAction.queryAction "SELECT numbackends FROM pg_stat_database where datname=%s --should be last test" workDatabase
                                                    |> DBAction.ensureSingleRow
                                                    |> DBAction.run client
            match result with
            // only our current connection and the hangfire server connection should be
            // active, all other connections opened by the application to treat
            // requests should have been closed by now
            | Ok [n] when n=2 -> ()
            | Ok [n] -> failwithf "Unexpected number of database connection open to database %s after test run: %d" workDatabase n
            | Ok l -> failwithf "got multiple values for connections count? : %A" l
            | Error es -> (es|> Error.toString ) |> failwith
            client.Close() |> Async.Start


            source.Cancel()
            return ()
        }
