namespace Tests
open System



// This module is only used in test environment. It mocks the sending of mails,
// and keeps track of mails being sent so tests can validate mails supposed to be
// sent by the app
module TestMailSystem =
    // Record type to keep information about mails
    type Mail = { Recipients: string
                  Subject: string
                  Body: string
    }
    // Messages recognised by the actor
    type SinkMsg=  Add of Mail
                  |Exists of Mail * AsyncReplyChannel<bool>
                  |Count of AsyncReplyChannel<int>
                  |Read of recipient:string * AsyncReplyChannel<Mail array>
                  |Clean of recipient:string

    // This mail sink is an actor keeping trace of all mails supposedly sent by the Test Mailer.
    // It also handles requests about the collected mails, so tests can validate and read mails that were 'sent'.
    let private mailSink =
        MailboxProcessor.Start(fun inbox ->
            let rec loop (acc:Mail array) =
                //System.IO.File.AppendAllText("/tmp/debug.txt", sprintf "loop\n******\nacc= %A\n" acc)
                async { let! msg = inbox.Receive()
                        match msg with
                        // add a new mail as sent. This message is sent by the tests implementation of IMailer
                        | Add mail ->
                                      let msg = sprintf "%s [%s] -- %s" mail.Recipients mail.Subject mail.Body
                                      //System.IO.File.AppendAllText("/tmp/debug.txt", sprintf "adding sent mail: %s\n" msg)
                                      return! loop (Array.append acc [|mail|])
                        // Probably only useful for developer writing tests
                        | Count replyChannel -> replyChannel.Reply (Array.length acc)
                        // check if mail was sent
                        // for the mail's body, only pass the beginning so different URLs don't screw up the match
                        | Exists (mail, replyChannel) ->
                                      replyChannel.Reply( Array.exists (fun m -> m.Recipients = mail.Recipients &&
                                                                                 m.Subject = mail.Subject &&
                                                                                 m.Body.StartsWith mail.Body)
                                                                      acc )
                                      return! loop acc
                        // retrieve all mails for recipient
                        | Read (recipient,replyChannel) -> let mailsForRecipient = Array.filter (fun m -> m.Recipients = recipient) acc
                                                           //System.IO.File.AppendAllText("/tmp/debug.txt", $"wit accu = {acc} we got request to read mails for {recipient} and returned {mailsForRecipient}\n" )
                                                           replyChannel.Reply(mailsForRecipient)
                                                           return! loop acc
                        | Clean recipient -> return! loop (Array.filter (fun m -> m.Recipients <> recipient) acc)
                      }
            loop (Array.empty) )

    // We need to wrap the actor in a class so it can be injected as a singleton
    // when the test code starts up the app
    type Sink() =
        member _.Get() = mailSink

    // The test mailer does not enqueue a Hangfire job as this makes it too
    // hard to test (it would required adding a sleep to let hangfire handle the job
    // but how long should that sleep be?)
    type Mailer(mailLogger:Sink) =
        interface web.IMailer with
            member _.Send(recipient, subject, body) =
                mailLogger.Get().Post(Add {Recipients=recipient;Subject=subject; Body=body})
                Serilog.Log.Debug("Sent mail for recipient {recipient}", id, recipient)

module Info=

    open FSharp.Data
    open FSharp.Data.JsonExtensions
    // Take baseUrl from the myowndb config
    let config = JsonValue.Parse(System.IO.File.ReadAllText("conf/myowndb.test.json"))
    let BaseUrl = config?myowndb?baseUrl.AsString()

    // seq of menu links' text with their respective target path
    let publicMenu = [| "Home", "/"
                        "Login", "/login"
                        "Signup", "/signup"
                        "About", "/about"
                     |]
    let authenticatedMenu = [| "Home", "/"
                               "Logout", "/logout"
                               // This is the guided tour icon
                               // It is a link without text, only an icon, and no href
                               "" , null
                            |]
    let adminMenu = [| "Home", "/"
                       "Manage users", "/admin/users"
                       "Logout", "/logout"
                       // It is a link without text, only an icon, and no href
                       "" , null
                    |]
    // passwords for fixtures users
    let passwords =
        Map.empty<string,string>
            .Add("existingbob@test.com","My§uperPass01234")
            .Add("longbob@test.com","My§uperPass01234")
            .Add("admin@test.com","My§uperPass01234")
            .Add("mohsinuser@company.it","My§uperPass01234")
    let visibilityTimeout = if (Environment.GetEnvironmentVariable("PWDEBUG")<>"" && not (isNull(Environment.GetEnvironmentVariable("PWDEBUG")))) then
                              200000.0F
                            else
                              2000.0F
    let mailSink = TestMailSystem.Sink()
