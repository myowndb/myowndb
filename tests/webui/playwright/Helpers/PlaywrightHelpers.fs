namespace Tests
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open NUnit.Framework

open FSharp.Control.Tasks
open FsUnit
open Info
open System.Threading.Tasks
open NGettext
open System.Globalization
open MyOwnDB.Tests.Helpers

module MyowndbTests =

    let confirmationYesXPath = """span[contains(@class,"text-danger") and text()="Yes"]"""
    let confirmationNoXPath = """span[not(contains(@class,"text-danger")) and text()="No"]"""

    // Waits for an update of the element indicated by the selector (incl its children)
    let waitForMutationAsync (page:IPage)(selector:string) = unitTask {
        let! _ =
          page
            .Locator(selector)
            .EvaluateAsync(""" (el) =>{
              return new Promise(resolve => {
                new window.MutationObserver((mutationsList, observer) => {
                    resolve();
                }).observe(el, { childList: true });
              });
        } """)
        ()
    }

    // Should the mutation be too fast to be detected with the simple waitForMutationAsync function,
    // This one can be used. It starts the task waiting for mutation before taking the action
    // that should trigger it.
    let runAndWaitForMutationAsync (page:IPage)(selector:string) (f:unit->Task) = unitTask {
        let t =
          page
            .Locator(selector)
            .EvaluateAsync(""" (el) =>{
              return new Promise(resolve => {
                new window.MutationObserver((mutationsList, observer) => {
                    resolve();
                }).observe(el, { childList: true });
              });
        } """)
        do! f()
        let! _ = t
        ()
    }
    let private checkHeadersLinks (t:Myowndb.I18n.Helpers)(page:IPage) (i:int) (text:string,path:string) = unitTask {
            let! link = page.QuerySelectorAsync($"li.nav-item:nth-child({i+1}) > a:nth-child(1)")
            let! href = link.GetAttributeAsync("href")
            href |> should equal path
            let! inner = link.InnerTextAsync()
            inner |> should equal (t.t(text))
    }

    // Use this helper to identify the error location in the test output. It catches the exception thrown in the
    // task it gets as argument, and wraps it in a new exception with the message passed as argument.
    // Without this, with all async tasks, identifying the location of the error can be cumbersome.
    // If you pass a task builder as argument (`task {}`), be sure to enclose it in parenthesis or you'll
    // get the error `The type 'Builders.TaskBuilder' is not compatible with the type 'Task<unit>'.`
    let withException (message:string)(t:Task<_>) = task {
        try
            do! t
        with
        |e ->
            raise (System.Exception(message,e))
    }

    let getLanguageAsync(page:IPage) = task {
            let! cookies = page.Context.CookiesAsync()
            let lang = cookies
                            |> Seq.filter (fun c -> c.Name =".AspNetCore.Culture")
                            |> Seq.tryExactlyOne
                            |> function
                                | Some c -> ( c.Value.Split("%7C")
                                                // split on "|"
                                                |> Seq.map(fun s -> s.Split("%3D"))
                                                // select ui culture part
                                                |> Seq.filter(fun c -> c.[0]="uic")
                                                // map to the value of uic
                                                |> Seq.map (fun c -> c.[1]))
                                                |> Seq.head
                                | None -> "en"
            return lang
    }
    let getTranslatorAsync(language:string) = task {
            return Myowndb.I18n.Helpers(Catalog("myowndb","./locale", CultureInfo(language)))
    }
    let getPageTranslatorAsync(page:IPage) = task {
            let! lang = getLanguageAsync(page)
            return! getTranslatorAsync(lang)
    }

    let checkMenusInLanguageAsync(page:IPage, menu:(string*string) array, lang)= unitTask {
            let! t = getTranslatorAsync(lang)
            // check brand link
            let! brand = page.InnerTextAsync(":is(.navbar-brand,.sidebar-brand)")
            brand |> should equal (t.t("MyOwnDB"))

            // check correct number of links are present in the menu
            // this is necessary because the public menu items could be a
            // subset of the authenticated menu items.
            let! links = page.QuerySelectorAllAsync($"ul.navbar-nav li.nav-item")
            (links |> Seq.length) |> should equal (menu |> Seq.length)

            // check all other menu links
            // go over all links
            do! menu
                |> Seq.mapi (checkHeadersLinks t page)
                |> Task.WhenAll
            ()
    }
    let checkMenusAsync(page:IPage, menu:(string*string) array)= unitTask {
            let! lang = getLanguageAsync(page)
            do! checkMenusInLanguageAsync(page, menu, lang)
    }


    let goToUrlAsync(page:IPage, url:string) = unitTask {
        let! _response  = page.RunAndWaitForResponseAsync((fun () -> unitTask { let! result = page.GotoAsync(url, PageGotoOptions(WaitUntil = WaitUntilState.NetworkIdle ))
                                                                       ()}),
                                                      (fun response -> response.Url = url)
                                                )
        ()
    }
    let goToPathAsync(page:IPage, path:string) = unitTask {
        do! goToUrlAsync(page,$"{BaseUrl}{path}")
    }

    let isStatus(page:IPage,path:string, status:int) = unitTask {
        let url = $"{BaseUrl}{path}"
        let! response  = page.RunAndWaitForResponseAsync((fun () -> unitTask { let! result = page.GotoAsync(url, PageGotoOptions(WaitUntil = WaitUntilState.NetworkIdle ))
                                                                       ()}),
                                                      (fun response -> response.Url = url)
                                                )
        response.Status |> should equal status
    }

    let goToLoginAsync(page:IPage) = goToPathAsync(page,"/login")
    let goToSignupAsync(page:IPage) = goToPathAsync(page,"/signup")

    let errorIsVisibleAsync(page:IPage) = unitTask {
        let! _ = page.WaitForSelectorAsync("#errorBox", PageWaitForSelectorOptions(Timeout=Info.visibilityTimeout))
        ()
    }

    // There were regular errors in tests checking a selector was not visible.
    // This is a function to be called before. It will wait for an element to be hidden.
    // If it is not present, it will wait for the full timeout.
    let waitForNotVisible(page:IPage)(selector:string)= unitTask {
        try
            let! _ = page.WaitForSelectorAsync(selector, PageWaitForSelectorOptions(Timeout=2000.0F,State=WaitForSelectorState.Hidden))
            ()
        with
        | e -> ()
    }

    let errorNotVisibleAsync(page:IPage) = unitTask {
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)
        let selector="#errorBox"
        do! waitForNotVisible page selector
        let! inError = page.IsVisibleAsync(selector)
        inError |> should equal false
    }

    let screenshotAsync(page:IPage,path:string) = unitTask {
        let! _ =  page.ScreenshotAsync(PageScreenshotOptions(Path=path,FullPage=true))
        return ()
    }

    let textIsVisible(page:IPage, s:string) = unitTask {
        try
            let! _ = page.WaitForSelectorAsync($"text={s}", PageWaitForSelectorOptions(Timeout=Info.visibilityTimeout,State=WaitForSelectorState.Visible))
            ()
        with
        | e ->
            do! screenshotAsync(page,$"""screenshot-{System.DateTime.Now.ToFileTime()}.png""")
            raise e
    }
    let textIsNotVisible(page:IPage, s:string) = unitTask {
        let selector = $"text={s}"
        do! waitForNotVisible page selector
        let! v = page.IsVisibleAsync(selector)
        v |> should equal false
    }

    let elementVisible(page:IPage, selector:string) = unitTask {
        try
            let! v = page.WaitForSelectorAsync(selector, PageWaitForSelectorOptions(Timeout=Info.visibilityTimeout))
            ()
        with
        | e ->
            do! screenshotAsync(page,$"""screenshot-{System.DateTime.Now.ToFileTime()}.png""")
            raise e
    }

    let rec elementsVisible (page:IPage) (selectors: string list) = async {
      match selectors with
      | selector :: rest ->
          do! elementVisible(page,selector) |> Async.AwaitTask
          do! elementsVisible page rest
      | [] -> return ()

    }

    let locatorVisible(page:IPage, locator:ILocator) = unitTask {
        try
            do! locator.WaitForAsync(LocatorWaitForOptions(Timeout=Info.visibilityTimeout))
            let! visible =  locator.IsVisibleAsync()
            visible |> should equal true
            ()
        with
        | e ->
            do! screenshotAsync(page,$"""screenshot-{System.DateTime.Now.ToFileTime()}.png""")
            raise e
    }

    let elementNotVisible(page:IPage, selector:string) = unitTask {
        try
            let! _ = page.WaitForSelectorAsync(selector, PageWaitForSelectorOptions(Timeout=2000.0F,State=WaitForSelectorState.Hidden))
            ()
        with
        | e -> ()
        let! v = page.IsVisibleAsync(selector)
        v |> should equal false
    }

    let elementEnabled(page:IPage, selector:string) = unitTask {
        let! v = page.IsEnabledAsync(selector,PageIsEnabledOptions(Timeout=1000F))
        v |> should equal true
    }

    let elementDisabled(page:IPage, selector:string) = unitTask {
        let! v = page.IsDisabledAsync(selector, PageIsDisabledOptions(Timeout=1000F))
        v |> should equal true
    }

    // Note: match is always on complete text
    let alertVisible(page:IPage)(kind:string)(expectedText:string) =  unitTask {
        do! elementVisible(page, $".alert-{kind}")
        let! info = page.TextContentAsync($".alert-{kind}")
        info |> should equal expectedText
    }

    let infoVisible(page:IPage,expectedText:string) =
        alertVisible page "info" expectedText

    let successVisible(page:IPage)(expectedText:string) =
        alertVisible page "success" expectedText

    let mailWasSent (mail:TestMailSystem.Mail) = task {
        let! wasSent = Info.mailSink.Get().PostAndAsyncReply(fun replyChannel -> TestMailSystem.SinkMsg.Exists(mail,replyChannel))
        return wasSent
    }

    let readMails (recipient:string) = task {
        let! mails = Info.mailSink.Get().PostAndAsyncReply(fun replyChannel -> TestMailSystem.SinkMsg.Read(recipient,replyChannel))
        return mails
    }

    let firstMailTo(email:string) = task {
        let! mails = readMails(email)
        return mails |> Seq.head
    }

    let noMailSentTo(recipient:string) = task {
        let! mails = readMails(recipient)
        (mails|>Array.length) |> should equal 0
    }

    let mailSentTo(recipient:string) = task {
        let! mails = readMails(recipient)
        (mails|>Array.length) |> should not' (equal 0)
    }

    let cleanMailsFor(recipient:string) =
        mailSink.Get().Post( TestMailSystem.SinkMsg.Clean recipient)

    let switchLanguageAsync(page:IPage, lang:string) = task {
        let originalUrl = page.Url
        let! request =  page.RunAndWaitForRequestAsync((fun () -> unitTask {
                                                                let! selected = page.SelectOptionAsync("#languageSelect", lang)
                                                                ()
                                                                })
                                                         ,
                                                         fun request -> request.Url = originalUrl
                                                      )
        let! response = request.ResponseAsync()
        response.Status |> should equal 200
        page.Url |> should equal originalUrl
        // check the cookie was set correctly
        let! pageLang = getLanguageAsync(page)
        pageLang |> should equal lang
        // and that we get the right translator
        // this is more a test of the test infrastructure, but let's keep it here...
        let! t = getPageTranslatorAsync(page)
        return t
    }
    let switchToFrenchAsync(page:IPage) = task {
        let! t = switchLanguageAsync(page,"fr")
        t.t("MyOwnDB") |> should equal "MaDB"
        return t
    }
    let switchToEnglishAsync(page:IPage) = task {
        let! t = switchLanguageAsync(page,"en")
        t.t("About MyOwnDB") |> should equal "About MyOwnDB"
        return t
    }

    let linksInText(text:string) =
        let pattern = @"(http(s?)\:\/\/[0-9a-zA-Z]([-.\w]*[0-9a-zA-Z])*(:(0-9)*)*(\/?)([a-zA-Z0-9\-\.\?\,\'\/\\\+&%\$#_~]*)?)"
        let matches = System.Text.RegularExpressions.Regex.Matches(text, pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase)
        matches |> Seq.map (fun m -> m.Value)

    let linksInMail(mail:TestMailSystem.Mail) =
        linksInText(mail.Body)

    let firstLinkInMail(mail:TestMailSystem.Mail) =
        linksInText(mail.Body) |> Seq.head

    let pressEnterServerActionAsync (page:IPage,selector:string) = unitTask {
        let! response =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                                do! page.PressAsync(selector, "Enter")
                                                                ()
                                                         })
        ()
    }
    let pressEnterClientActionAsync (page:IPage,selector:string) = unitTask {
        do! page.PressAsync(selector, "Enter")
        ()
    }

    // helper to submit new password with click on button
    let submitNewPassword(page:IPage) =  task {
        return! page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                             do! page.ClickAsync("#submitNewPassword")
                                                             ()
                                                   })
    }
    // Reload page and check we get the 200 status
    let reload (page:IPage) = unitTask {
        let! r = page.ReloadAsync(PageReloadOptions(WaitUntil=WaitUntilState.NetworkIdle))
        r.Status |> should equal 200
    }

    // We don't use the fixtures users passwords from Info here because this helper is used to test
    // password changes.
    let checkLoginThenLogoutOnCurrentPage(page:IPage,email:string,password:string, isAdmin:bool) = task {
        do! checkMenusInLanguageAsync(page,publicMenu, "en")
        do! checkMenusAsync(page,publicMenu)
        do! page.FillAsync("#emailInput", email)
        do! page.FillAsync("#passwordInput", password)
        do! page.PauseAsync()
        do! pressEnterServerActionAsync(page, "#passwordInput")
        do! page.WaitForURLAsync($"{BaseUrl}/")
        do! errorNotVisibleAsync(page)
        do! checkMenusInLanguageAsync(page,(if isAdmin then adminMenu else authenticatedMenu), "en")
        do! page.ClickAsync("text=Logout")
    }

    let checkLoginThenLogoutStandardUser(page:IPage,email:string,password:string) = task {
        do! goToPathAsync(page,"/login")
        do! checkLoginThenLogoutOnCurrentPage(page,email,password,false)
    }

    let checkLoginThenLogoutAdminUser(page:IPage,email:string,password:string) = task {
        do! goToPathAsync(page,"/login")
        do! checkLoginThenLogoutOnCurrentPage(page,email,password,true)
    }

    let checkLoginFails(page:IPage,email:string,password:string) = task {
        do! goToPathAsync(page,"/login")
        do! checkMenusInLanguageAsync(page,publicMenu, "en")
        do! checkMenusAsync(page,publicMenu)
        do! page.FillAsync("#emailInput", email)
        do! page.FillAsync("#passwordInput", password)
        do! pressEnterServerActionAsync(page, "#passwordInput")
        do! errorIsVisibleAsync(page)
    }

    // Generic file selection helper
    let selectFileForFieldSelector (page:IPage)(selector:string)(filePath:string) = task {
        let! fileChooser=
            page.RunAndWaitForFileChooserAsync(
                fun () -> task{
                    do! page.ClickAsync(selector)
                }
            )
        do! fileChooser.SetFilesAsync(filePath)
    }

// ********************************************************************************
// Datatable helpers
// ********************************************************************************
    // The column according which it is sorted will be checked when using the control document.
    type TableSortOrder = |Asc |Desc
    type TableSortSpec = TableSortOrder option
    type TableInputSpec =
        {
            searchedColumn:string;  // value selected in dropdown list of details to filter against
            searchedValue:string;   // value used to filter rows displayed
            perPage: int;           // perpage value selected in dropdown
            columnsCount:int;       // number of data columns in the table
            sorting: TableSortSpec; // sorting criteria and direction
            hasLinks: bool          // does this table have values displayed as links. /false if all values of a link column are empty.
            paginationLinks: int    // number of pagination links (to page numbers) displayed
        }
    // checks that the ith element in navItems has text expected
    // does not work on iniput elements
    let checkElementsTextContent (navItem:ILocator) (expected:string)= unitTask {
        let! navText = navItem.TextContentAsync()
        navText |> should equal expected
    }
    let checkInputValue (input:ILocator) (expected:string)= unitTask {
        let! el = input.ElementHandleAsync()
        let! v = el.InputValueAsync()
        v |> should equal expected
    }
    // checks that the ith element in navItems is a select with expected as selected text content
    let checkElementsOptionContent (navItem:ILocator) (expected:string)= unitTask {
        let! navText = navItem.Locator("option:checked").InnerTextAsync()
        navText.ToString() |> should equal expected
    }
    let checkElementsCount (navItems:ILocator) (navControlNodes:HtmlAgilityPack.HtmlNodeCollection) = unitTask{

        // evaluate on all will force to wait for all to be available. I didn't find another way.
        let! _ = navItems.EvaluateAllAsync( "e => e")
        // I tried lots of things and this sleep is what finally makes this test reliable. There's
        // probably a better way, but I don't want to search further if this fixes it for now.
        do! Async.Sleep 100
        let expectedNumber = navControlNodes|> Seq.length
        do! Assertions.Expect(navItems).ToHaveCountAsync(expectedNumber)

    }
    let checkTableNavigationElementsText (navItems:ILocator) (navControlNodes:HtmlAgilityPack.HtmlNodeCollection) (spec:TableInputSpec)= unitTask{
        // helpers
        let checkItem = checkElementsTextContent
        let checkSelect = checkElementsOptionContent
        // check we have sufficient elements before doing the checks
        // we do this because the selector used earlier could be buggy and return an empty list from both the controle doc and the page
        // +7 for:
        //  * 4 additional navigation links (firstpage, previous page ..)
        //  * number of rows indicator
        //  * 2 dropdowns
        (navControlNodes |> Seq.length) |> should be (greaterThanOrEqualTo (spec.paginationLinks+7))
        // check navigation elements
        for i in 0..(spec.paginationLinks+4) do
            do! checkItem (navItems.Nth(i)) (navControlNodes.[i].InnerText)
        do! checkSelect (navItems.Locator(".myowndbperpageddl")) (string spec.perPage)
        do! checkSelect (navItems.Locator(".myowndbsearchddl")) spec.searchedColumn
        do! checkInputValue (navItems.Locator("input.myowndbsearchvalue")) spec.searchedValue

    }
    // helper to elements extracted by a XPath selector from a control document and the page
    let checkXPathInnerText(mainTable:ILocator) (htmlDoc:HtmlAgilityPack.HtmlDocument) (selector:string)= unitTask{
        // extract elements
        let selectedItems = mainTable.Locator(selector)
        let disabledControlNodes = htmlDoc.DocumentNode.SelectNodes(selector)
        // check counts
        do! checkElementsCount selectedItems disabledControlNodes
        // extract and check inner text
        let! actual = selectedItems.AllInnerTextsAsync()
        let expectedRaws = disabledControlNodes |> Seq.map (fun n -> HtmlAgilityPack.HtmlEntity.DeEntitize n.InnerText)
        let expected=
          expectedRaws
          // Remove new lines from strings. It seems that in AllInnerTextsAsync, those are not kept
          |> Seq.map (fun s -> s.Replace(System.Environment.NewLine, "") )
          // Browsers collapse multiple spaces in their output, so InnerText will not return subsequent spaces.
          // We have this in the detial "Doesn't or does?" he said with a  in his eyes"
          |> Seq.map (fun s -> System.Text.RegularExpressions.Regex.Replace(s, "\s+", " ") )
        actual |> should equal expected
    }
    // Check we have the expected number of elements selected by the XPath selection
    let checkXPathCount(mainTable:ILocator) (selector:string) (comparison:NUnit.Framework.Constraints.Constraint)= unitTask{
        // extract elements
        let! itemsCount = mainTable.Locator(selector).CountAsync()
        itemsCount |> should be comparison
    }
    // Check that right navigation elements are active and disabled
    let checkTableNavigationElementsStates(mainTable:ILocator) (controlDoc:HtmlAgilityPack.HtmlDocument) = unitTask{
        let check = checkXPathInnerText mainTable controlDoc
        do! check "//li[contains(@class,'disabled')]/a[contains(@class,'page-link')]"
        do! check "//li[contains(@class,'active')]/a[contains(@class,'page-link')]"
    }
    // check we have the search reset button
    let checkTableNavigationSearchReset(mainTable:ILocator) (htmlDoc:HtmlAgilityPack.HtmlDocument) = unitTask{
        // using the class didn't work
        do! checkXPathCount mainTable "//*[local-name()='svg' and @data-icon='backspace']" (equal 1)
    }
    let checkTableNavigation (mainTable:ILocator) (htmlDoc:HtmlAgilityPack.HtmlDocument) (spec:TableInputSpec)= unitTask {
        // this selector should get all navigation links + search fields + number of rows indicator.
        let navSelector="//nav/ul/li"
        // extract navigation elements
        let navControlNodes = htmlDoc.DocumentNode.SelectNodes(navSelector)
        let navItems = mainTable.Locator(navSelector)

        do! withException
                "Navigation element count check failed"
                (task{
                    do! checkElementsCount navItems navControlNodes
                })
        do! withException
                "Navigation elements checks using spec failed"
                (task{
                    do! checkTableNavigationElementsText navItems navControlNodes spec
                })
        do! withException
                "Navigation elements states and search checks failed"
                (task{
                    do! checkTableNavigationElementsStates mainTable htmlDoc
                    do! checkTableNavigationSearchReset mainTable htmlDoc
                })
    }

    let checkTableColumnHeaders (mainTable:ILocator) (controlDoc:HtmlAgilityPack.HtmlDocument)(spec:TableInputSpec)= unitTask {
        // look at all columns
        let cellsSelector = "//th/div[contains(@class,'sortable')]"
        // check explicitly that we have the expected number of columns to avoid bugs
        // where both control and page would return 0 (eg if selector had to be updated)
        do! withException
                "Table column headers checks using spec failed"
                (task {
                    do! checkXPathCount mainTable cellsSelector (equal spec.columnsCount)
                })
        do! withException
                "Table column headers text checks failed"
                (task {
                    do! checkXPathInnerText mainTable controlDoc cellsSelector
                })

        // Sorting indicator checks
        let countAsc,countDesc =
            match spec.sorting with
            | None -> 0,0
            | Some Asc -> 1,0
            | Some Desc-> 0,1
        let countSorted = countAsc + countDesc
        // look at columns not used for sorting
        let cellsSelector = "//th/div[contains(@class,'sortable') and contains(@class,'both')]"
        do! withException
                "Columns not used for sorting check failed"
                (task {
                    do! checkXPathCount mainTable cellsSelector (equal (spec.columnsCount-countSorted))
                    do! checkXPathInnerText mainTable controlDoc cellsSelector
                })
        do! withException
                "Columns used for sorting check failed"
                (task {
                    // look at columns in ascending order
                    // !!BEWARE!!!
                    // When we display values in ascending order, we display the smallest value on top and the
                    // biggest at the bottom, so the arrow is descending.
                    // The CSS class used is determining the direction of the arrow, so it is opposite of the
                    // value ordering...
                    // If the CSS selectors below look as inverted, (re)read the lines above.
                    let cellsSelector = "//th/div[contains(@class,'sortable') and contains(@class,'desc')]"
                    do! checkXPathCount mainTable cellsSelector (equal countAsc)
                    do! checkXPathInnerText mainTable controlDoc cellsSelector
                    // look at columns in descending order
                    // If the CSS selectors below look as inverted, (re)read the lines above.
                    let cellsSelector = "//th/div[contains(@class,'sortable') and contains(@class,'asc')]"
                    do! checkXPathCount mainTable cellsSelector (equal countDesc)
                    do! checkXPathInnerText mainTable controlDoc cellsSelector
                })
    }
    // Check datacells displayed as hyperlink
    let checkTableDataLinks (mainTable:ILocator) (controlDoc:HtmlAgilityPack.HtmlDocument) (spec:TableInputSpec)= unitTask {
        let selector = "//td[contains(@class,'myowndbdatacell')]/a"
        // Double check to ensure we have at least one link when links should be displayed
        // Added so we avoid future bugs when a buggy selector returns zero links from both the page and the control doc even when links are present.
        let linksPresenceCheck:NUnit.Framework.Constraints.Constraint =
            if spec.hasLinks then
                (greaterThan 0) :> NUnit.Framework.Constraints.Constraint
            else
                (equal 0) :> NUnit.Framework.Constraints.Constraint
        // If you have an exception here while testing linked instances tables, it is often due to passing an erroneous
        // canLinkOnlyOne or hasLinks
        do! withException
                "Checks in checkTableDataLink failed for linksPresenceCheck"
                (task {
                    do! checkXPathCount mainTable selector linksPresenceCheck
                })

        // extract elements
        let selectedItems = mainTable.Locator(selector)
        let selectedControlNodes = controlDoc.DocumentNode.SelectNodes(selector)
        // check count
        do! withException
                "Checks in checkTableDataLink failed for selectedControlNodes"
                (task {
                    do! checkElementsCount selectedItems selectedControlNodes
                })
        // check href
        do! withException
                "Checks in checkTableDataLink failed for href"
                (task {
                    let! count = selectedItems.CountAsync()
                    for i in 0..(count-1) do
                        let item = selectedItems.Nth(i)
                        let controlNode = selectedControlNodes.Item(i)
                        let! href = item.GetAttributeAsync("href")
                        let controlHref = controlNode.GetAttributeValue("href","")
                        href |> should equal controlHref
                })
    }
    let checkTableData (mainTable:ILocator) (controlDoc:HtmlAgilityPack.HtmlDocument)(spec:TableInputSpec)= unitTask {
        do! withException
                "Checks in checkTableData failed for innertext"
                (task {
                    // data cells XPath selector
                    let cellsSelector = "//td[contains(@class,'myowndbdatacell')]"
                    do! checkXPathInnerText mainTable controlDoc cellsSelector
                })

        do! withException
                "Checks in checkTableData failed for for column headers"
                (task {
                    do! checkTableColumnHeaders mainTable controlDoc spec
                })
                    // check data cells displayed as link
        // Detailed exceptions raised inside this function, no need ot tall withException
        do! checkTableDataLinks mainTable controlDoc spec
    }

    let checkTable(page:IPage) (idSelector:string) (validationFile:string) (spec:TableInputSpec)= unitTask {
        // select data from control file
        let controlDoc = HtmlAgilityPack.HtmlDocument()
        controlDoc.OptionEmptyCollection <- true
        controlDoc.Load(validationFile)
        // check table is present
        let mainTable = page.Locator(idSelector)
        // Assertions do wait when testing the expectation
        do! Assertions.Expect(mainTable).ToBeVisibleAsync()
        try
            do! checkTableNavigation mainTable controlDoc spec
            do! checkTableData mainTable controlDoc spec
        with
        e ->
            let! outerHTML = mainTable.EvaluateAsync("node => node.outerHTML")
            System.IO.File.WriteAllText($"{validationFile}.actual.html", outerHTML.ToString() )
            do! screenshotAsync(page,$"{validationFile}.actual.png")
            raise (new System.Exception($"Mismatch in table check, look at inner exception and check actual output at\nbin/Debug/net*/{validationFile}.actual.html\n and screenshot at bin/Debug/net*/{validationFile}.actual.png",e))
    }

    // --------------------------------------------------------------------------------
    // Helpers to check html tables. More general than previous helpers that were geared towards datatable tests
    // --------------------------------------------------------------------------------
    let checkPlainTableData (mainTable:ILocator) (controlDoc:HtmlAgilityPack.HtmlDocument)= unitTask {
        do! withException
                "Checks in checkPlainTableData failed for innertext, both td"
                (task {
                    // data cells XPath selector
                    let cellsSelector = "//td"
                    do! checkXPathInnerText mainTable controlDoc cellsSelector
                }
                )
        do! withException
                "Checks in checkPlainTableData failed for innertext, both th"
                (task {
                    // data cells XPath selector
                    let cellsSelector = "//th"
                    do! checkXPathInnerText mainTable controlDoc cellsSelector
                })
    }
    let checkPlainTable(page:IPage) (idSelector:string) (validationFile:string) = unitTask {
        // select data from control file
        let controlDoc = HtmlAgilityPack.HtmlDocument()
        controlDoc.OptionEmptyCollection <- true
        controlDoc.Load(validationFile)
        // check table is present
        let mainTable = page.Locator(idSelector)
        // Assertions do wait when testing the expectation
        do! Assertions.Expect(mainTable).ToBeVisibleAsync()
        try
            do! checkPlainTableData mainTable controlDoc
        with
        e ->
            let! outerHTML = mainTable.EvaluateAsync("node => node.outerHTML")
            System.IO.File.WriteAllText($"{validationFile}.actual.html", outerHTML.ToString() )
            do! screenshotAsync(page,$"{validationFile}.actual.png")
            raise (new System.Exception($"Mismatch in plain table check, look at inner exception and check actual output at\nbin/Debug/net*/{validationFile}.actual.html\n and screenshot at bin/Debug/net*/{validationFile}.actual.png",e))
    }
    let clickTablePagination(page:IPage) (idSelector:string) (linkText:string) = unitTask{
        // check table is present
        let mainTable = page.Locator(idSelector)
        do! mainTable.WaitForAsync()
        // is table there?
        let! tableVisible = mainTable.IsVisibleAsync()
        tableVisible |> should equal true

        // put text to be matched in single quote for an exact match. otherwise matching with 2 will also match 28...
        let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task { do! mainTable.Locator("nav>ul>li>a.page-link").Locator($"text='{linkText}'").ClickAsync()})
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)

    }

    let clickTableColumnHeader(page:IPage) (idSelector:string) (headerText:string) = unitTask{
        // check table is present
        let mainTable = page.Locator(idSelector)
        do! mainTable.WaitForAsync()
        // is table visible?
        let! tableVisible = mainTable.IsVisibleAsync()
        tableVisible |> should equal true

        let cellsSelector = "//th/div[contains(@class,'sortable')]"
        do! mainTable.Locator(cellsSelector).Locator($"text='{headerText}'").ClickAsync()
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)
    }

    // In row having a cell with text `rowCellText`, click on the action cell having myowndb-action equal to `action`.
    let clickActionCell (page:IPage)(tableXPath:string) (rowCellText:string) (action:string) = task {
        let xPathAction (cellText:string) (action:string) = $"""//td[text()="{cellText}"]//following-sibling::td[contains(@class,"myowndbactioncell")]//*[@data-myowndb-action="{action}"]"""
        do! page.ClickAsync($"""{tableXPath}{xPathAction rowCellText action}""",PageClickOptions(NoWaitAfter=true))
    }

    // Defining the getRowelement function allows to define the inner function xPathElement, building the XPAth for an element in a row,
    // once and be used by all caller of getRowElement.
    let getRowElement (page:IPage)(tableXPath:string) (rowCellText:string) (elementXPath:string) =
        let xPathElement (cellText:string) (element:string) = $"""{tableXPath}//td[text()="{cellText}"]//following-sibling::td//{elementXPath}"""
        page.Locator(xPathElement rowCellText elementXPath)

    let checkRowAbsent (page:IPage)(tableXPath:string) (rowCellText:string)  = task {
        let completeXPath = $"""{tableXPath}//td[text()="{rowCellText}"]"""
        let rowLocator = page.Locator(completeXPath)
        // This waits until the assertions is verified, or the timeout occurs
        do! Assertions.Expect(rowLocator).ToHaveCountAsync(0)
    }

    let checkRowHasElement (page:IPage)(tableXPath:string) (rowCellText:string) (elementXPath:string) = task {
        let element = getRowElement page tableXPath rowCellText elementXPath
        do! locatorVisible(page, element)
    }

    let checkRowHasDeleteConfirmation (page:IPage)(tableXPath:string) (rowCellText:string) = task {
        do! checkRowHasElement page tableXPath rowCellText confirmationYesXPath
        do! checkRowHasElement page tableXPath rowCellText confirmationNoXPath
    }

    // Check the dustbin action cell is displayed, meaning the confirmation is not displayed
    let checkRowHasNoDeleteConfirmation (page:IPage)(tableXPath:string) (rowCellText:string) = task {
        let deleteActionCell = getRowElement page tableXPath rowCellText $"""span[@data-myowndb-action="delete"]"""
        do! locatorVisible(page,deleteActionCell)
    }

    let clickRowDeleteConfirmation (page:IPage)(tableXPath:string) (rowCellText:string) = task {
        let element = getRowElement page tableXPath rowCellText confirmationYesXPath
        let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task {
            do! element.ClickAsync()
        })
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)
    }
    let cancelRowDeletion (page:IPage)(tableXPath:string) (rowCellText:string) = task {
        let element = getRowElement page tableXPath rowCellText confirmationNoXPath
        do! element.ClickAsync()
    }

    let deleteInstanceFromTable (page:IPage)(tableXPath:string)(rowCellText:string) = task {
        do! clickActionCell page tableXPath rowCellText "delete"
        do! checkRowHasDeleteConfirmation page tableXPath rowCellText
        let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task {
            do! clickRowDeleteConfirmation page tableXPath rowCellText
        })
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)
    }

    let chooseDdlValue (page:IPage) (selector:string) (value:string) = unitTask {
        let! _ = page.SelectOptionAsync(selector, SelectOptionValue(Label=value))
        return ()
    }

    let enterSearchValue (page:IPage) (searchFieldSelector:string) (value:string) = unitTask {
        // enter searched value
        do! page.FillAsync(searchFieldSelector, value)
        // submit the search and wait for the triggered request to be done
        let! request =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                                do! page.PressAsync(searchFieldSelector, "Enter")
                                                                // sadly other waits were not reliable, so adding this sleep :-(
                                                                do! Async.Sleep 100
                                                                ()
                                                         })
        let! _response = request.ResponseAsync()
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)

    }

    let filterTable (page:IPage) (idSelector:string) (column:string) (value:string)= unitTask{
        // check table is present
        let mainTable = page.Locator(idSelector)
        do! mainTable.WaitForAsync()
        // is table visible?
        let! tableVisible = mainTable.IsVisibleAsync()
        tableVisible |> should equal true

        let searchDdlSelector = $"{idSelector} select.myowndbsearchddl"
        let searchFieldSelector = $"{idSelector} input.myowndbsearchvalue"

        // choose search column
        do! chooseDdlValue page searchDdlSelector column
        do! enterSearchValue page searchFieldSelector value

    }

    let clearSearch (page:IPage) (idSelector:string) = unitTask{
        // check table is present
        let mainTable = page.Locator(idSelector)
        do! mainTable.WaitForAsync()
        // is table visible?
        let! tableVisible = mainTable.IsVisibleAsync()
        tableVisible |> should equal true

        let clearSearchSelector = $".myowndbclearsearch"

        // choose search column
        let! request =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                                do! mainTable.Locator(clearSearchSelector).ClickAsync()
                                                                ()
                                                         })
        let! _response = request.ResponseAsync()
        ()
    }

    let setPerPage (page:IPage) (idSelector:string) (value:int)= unitTask{
        // check table is present
        let mainTable = page.Locator(idSelector)
        do! mainTable.WaitForAsync()
        // is table visible?
        let! tableVisible = mainTable.IsVisibleAsync()
        tableVisible |> should equal true

        let perPageDdlSelector = $"{idSelector} select.myowndbperpageddl"

        // choose search column
        let! request =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
                                                                do! chooseDdlValue page perPageDdlSelector (string value)
                                                                ()
                                                         })
        let! _response = request.ResponseAsync()
        ()

    }

    // Extract the number of rows announced by the datatable
    let getTableRowsCount (page:IPage)(tableAncestor:string) = task {
        let rowsCountXPath = $"""{tableAncestor}//li[contains(@class,'page-item')]/a[contains(@class,'page-link')]"""
        // I don't know why this regex `Regex(@"There are (\d+) rows?|There is (\d) row")` raises the error
        // `Input string was not in a correct format.`. So I went for:
        let rowsCountRegex = new System.Text.RegularExpressions.Regex(@"There (are|is) (\d+) rows?")
        let! rowsCountText = page.Locator(rowsCountXPath, PageLocatorOptions(HasTextRegex=rowsCountRegex)).InnerTextAsync()
        try
            return int rowsCountRegex.Matches(rowsCountText).[0].Groups[2].Value
        with
        e ->
            printfn "wrong format?--%s--" rowsCountText
            raise e
            return Unchecked.defaultof<_>
    }

    // takes the current number of rows in the table and returns a thunk that will check that the difference of number of rows
    // when it is called is as expected
    let getRowsDifferenceChecker (page:IPage)(tableAncestor:string)(difference:int) = task {
        let! countBefore = getTableRowsCount page tableAncestor
        return (fun () -> task{
            let! countAfter = getTableRowsCount page tableAncestor
            countAfter |> should equal (countBefore + difference)
        })

    }

    // Returns the number of rows currently displayed by a datatable
    let getNumberOfRowsDisplayed (page:IPage)(tableAncestor:string) = task {
        let xPath = $"""{tableAncestor}//tr"""
        return! page.Locator(xPath).CountAsync()
    }
    let authenticate(page:IPage, email:string) =  task{
        let submit () = unitTask {
            let! _  = page.RunAndWaitForRequestFinishedAsync(fun () -> page.PressAsync("#passwordInput", "Enter"))
            ()
        }
        do! goToLoginAsync(page)
        do! page.FillAsync("#emailInput",email)
        do! page.FillAsync("#passwordInput",passwords.Item(email))
        do! submit()
        let! _ =  page.WaitForURLAsync($"{BaseUrl}/")
        page.Url |> should equal $"{BaseUrl}/"
    }

    let downloadWithClickOn(page:IPage, selector: string) = task {
        let waitForDownloadTask = page.WaitForDownloadAsync()
        // trigger the download
        do! page.Locator(selector).ClickAsync()
        let! download = waitForDownloadTask
        return download
    }

    let checkDownloadLink (page:IPage)(linkSelector:string)(expectedFile:string)(suggestedName:string)= task {
        // get IDownload instance and the downloaded path
        let! download = downloadWithClickOn(page, linkSelector)
        let! downloadedFile = download.PathAsync()

        // compare file content with file in fixtures
        do! checkFileHash(downloadedFile,expectedFile)
        // check suggested file name
        download.SuggestedFilename |> should equal suggestedName
    }

    // Locate an action link for the row with data `s`. Useful to locate the edit link for an instance in a table
    let getActionCellForRowWithData (page:IPage)(s:string)(action:string) =
        page.Locator($"""//td[contains(@class,"myowndbdatacell") and text()="{s}"]//following-sibling::td[contains(@class,"myowndbactioncell")]/a[contains(@href,"{action}")]""")

    // Checks that the row having a cell with value `id` also has a cell with value `searchedColumn` in a following column.
    // As a consequence the value passed for `id` should be the value of the row in the first column identifying it, i.e. having
    // a unique value in the displayed table.
    // We cannot match on td because links have the text under `a` and not `td`.
    let rowWithTextHasNextSiblingColumn (page:IPage)(id:string)(searchedColumn:string)= task {
        do! elementVisible(page,$"""//td[text()="{id}"]/following-sibling::*/..//*[text()="{searchedColumn}"]""")
    }

open MyowndbTests

module MyOwndbForms =
    type FieldSpec =
        {
            name: string
            value: string
        }

    // Function returning all form fields labels in order. Can easily be used with page.GetByLabel.
    let getLabelTexts (page:IPage)(ancestorSelector:string)= task{
        let labels = page.Locator($"{ancestorSelector} label.col-form-label")
        return! labels.AllInnerTextsAsync()
    }

    // Builds a map with keys the names of the fields, and the values the Ilocator of the field.
    let getFieldsMap (page:IPage) (ancestorSelector:string) = task {
        let! labelTexts = getLabelTexts page ancestorSelector
        let ancestor = page.Locator(ancestorSelector)
        let r =
            labelTexts
            |> Seq.map (fun l ->
                ancestor.GetByLabel(l,LocatorGetByLabelOptions(Exact=true))
            )
        let pairs = Seq.zip labelTexts r
        return Map pairs
    }

    // Helper to fill a form field as present in the map built by getFieldsMap
    let fillFieldFromMapWith (fields:Map<string,ILocator>)(key:string)(value:string) = task {
        do! fields[key].FillAsync(value)
    }
    // Define this helper because a number field cannot be filled with text, but typing seems ok
    let typeInFieldFromMapWith (fields:Map<string,ILocator>)(key:string)(value:string) = task {
        do! fields[key].TypeAsync(value)
    }


    let setFileFieldFromMapWith (fields:Map<string,ILocator>)(key:string)(filename:string) = task {
        do! fields[key].SetInputFilesAsync($"fixtures/files/attachments/{filename}")
    }

    let chooseOptionFieldFromMapWith (fields:Map<string,ILocator>)(key:string)(value:string) = task {
        return! fields[key].SelectOptionAsync(SelectOptionValue(Label=value))
    }
    // Build a map fieldName -> FieldSpec for easy access
    let specListToMap (spec:FieldSpec list) =
        spec
        |> List.map
            (fun s ->
                (s.name, s)
            )
        |> Map

    // Validates an form according to specs
    let checkForm (page:IPage)(ancestorSelector:string)(specs:FieldSpec list) = unitTask {
        let! labelTexts = getLabelTexts page ancestorSelector
        labelTexts |> should equal (specs |> Seq.map (fun r -> r.name))

        let! values =
            labelTexts
            |> Seq.map (fun l ->
                page.GetByLabel(l).InputValueAsync()
            )
            |> System.Threading.Tasks.Task.WhenAll
        values |> should equal (specs |> Seq.map (fun r -> r.value))

        // Count all buttons. Only the submit should be displayed
        let buttons = page.Locator($"{ancestorSelector} button.btn:visible")
        let! buttonElements = buttons.AllAsync()
        buttonElements|>Seq.length |> should equal 1

        // Count all primary buttons. Only the submit should be displayed
        let buttons = page.Locator($"{ancestorSelector} button.btn-primary:visible")
        let! buttonElements = buttons.AllAsync()
        buttonElements|>Seq.length |> should equal 1

        // Count all secondary buttons. None should be displayed
        let buttons = page.Locator($"{ancestorSelector} button.btn-secondary:visible")
        let! buttonElements = buttons.AllAsync()
        buttonElements|>Seq.length |> should equal 0
    }

    // checks that the value is displayed in bold in the form submission overview, indicating it was updated
    let xPathForOverviewField field = $"""//div[contains(@class,"alert-success")]//*[contains(@class,"instance-field-value") and @data-myowndb-field-name="{field}"]"""
    let isInOverviewWithValue (page:IPage) (field:string) (value:string)= task{
        do! elementVisible(page,$"""{xPathForOverviewField field}/span[text()="{value}"]""")
    }

    let isUnchangedInOverview (page:IPage) (field:string) = task{
        do! isInOverviewWithValue page field "No change"
    }

    let isMarkedLeftEmptyInOverview (page:IPage) (field:string) = task{
        do! isInOverviewWithValue page field "Left Empty"
    }

    let isEmptyInOverview (page:IPage) (field:string) = task{
        // There's no way to validated that text()="" because there is no text node. So we check it is empty with this xpath.
        // See https://stackoverflow.com/a/20997872
        let! v = page.WaitForSelectorAsync($"""{xPathForOverviewField field}/span[1][.=""]""", PageWaitForSelectorOptions(State=WaitForSelectorState.Attached))
        ()
    }

    // Locate input field for detail named `field` via its label
    // Playwright will read the value of the related input field if InputValueAsync is called on a label.
    let inputValueForField (page:IPage)(field:string)=
        // This matches input ou textarea. Both xpaths are separated by |
        page.Locator($"""//label[contains(@class,"col-form-label") and contains(@for,"{field}")]""")

    // Returns the XPath locator of the form field container. It starts by locating its label, then goes to its parent
    // and goes to the next sibling, which wraps the form field.
    // It is used as a base for more precise XPaths identifying elements of the field, eg for file attachments
    let xPathForFormFieldContainerFromLabel (field:string) =
        $"""//label[contains(@class,"col-form-label") and contains(@for,"{field}")]/../following-sibling::*"""

    // Check the file field displays the link to download the current file
    let fileFieldHasFileOnServer (page:IPage)(field:string)(filename:string)= task{
        // This matches input ou textarea. Both xpaths are separated by |
        do! elementVisible(page,$"""{xPathForFormFieldContainerFromLabel field}//a[text()="{filename}"]""")
    }

    // Check the field has no file currently present on the server
    let fileFieldHasNoFileOnServer (page:IPage)(field:string)= task{
        // This matches input ou textarea. Both xpaths are separated by |
        do! elementNotVisible(page,$"""{xPathForFormFieldContainerFromLabel field}//a""")
    }

    let checkFieldHasValue (page:IPage)(field:string)(value:string)= task {
        let input = inputValueForField page field
        let! currentValue = input.InputValueAsync()
        currentValue |> should equal value
    }

    let fieldIsInvalid (page:IPage)(field:string)= task{
        // This matches input ou textarea. Both xpaths are separated by |
        do! elementVisible(page,$"""{xPathForFormFieldContainerFromLabel field}//input[contains(@class,"is-invalid")]""")
    }

    // Check the warning for too big files is displayed
    let fileFieldHasFileTooBigWarning (page:IPage) (field:string) = task {
        do! elementVisible(page,$"""{xPathForFormFieldContainerFromLabel field}//div[contains(@class,"alert-danger")]/span[text()="File too big"]""")
    }

    // Check the warning for too big files is NOT displayed
    let fileFieldHasNoFileTooBigWarning (page:IPage) (field:string) = task {
        do! elementNotVisible(page,$"""{xPathForFormFieldContainerFromLabel field}//div[contains(@class,"alert-danger")]/span[text()="File too big"]""")
    }

    let fieldIsValid (page:IPage)(field:string)= task{
        // This matches input ou textarea. Both xpaths are separated by |
        do! elementVisible(page,$"""{xPathForFormFieldContainerFromLabel field}//input[not(contains(@class,"is-invalid"))]""")
    }

    // Click the dustbin beside a current file attachment in instance edition form
    // This Triggers the display of a confirmation request
    let clickDustBinOfAttachment (page:IPage)(field:string) = task {
        let dustbin = page.Locator($"""{xPathForFormFieldContainerFromLabel field}//span[contains(@class,"myowndb-delete-attachment")]""")
        // Wait for dustbin to be visible. This was required to avoid random failure of the test where the confirmation was not shown.
        // Maybe due to NoWaiAfter? It is required as the clicked element is removed from the DOM, but maybe it didn't wait for the
        // dustbin to be visible and continued right away?
        do! dustbin.WaitForAsync()
        do! dustbin.ClickAsync(LocatorClickOptions(NoWaitAfter=true))
        // Wait for the confirmation request to be displayed before continuing
        do! page.Locator($"""{xPathForFormFieldContainerFromLabel field}//span[contains(@class,"danger") and text()="Yes"]""").WaitForAsync()
        ()
    }

    // XPaths for to confirm and cancel a deletion request when yes/no confirmation is requested
    let xPathToConfirmDeletion field= $"""{xPathForFormFieldContainerFromLabel field}//{confirmationYesXPath}"""
    let xPathToCancelDeletion field= $"""{xPathForFormFieldContainerFromLabel field}//{confirmationNoXPath}"""
    let xPathToResetChosenFile field= $"""{xPathForFormFieldContainerFromLabel field}//button[text()="Reset"]"""
    let xPathToSaveButton (ancestor:string)= $"""{ancestor}//button[contains(@class,"btn-primary") and text()="Save"]"""
    // xPath used to check the disblaed css class is also set
    let xPathToDisabledSaveButton (ancestor:string)= $"""{ancestor}//button[contains(@class,"btn-primary") and contains(@class,"disabled") and text()="Save"]"""

    // Check the field has a new file selected by checking the reset button is present
    let fileFieldHasSelection (page:IPage)(field:string)= task{
        // This matches input ou textarea. Both xpaths are separated by |
        do! elementVisible(page,xPathToResetChosenFile field)
    }
    // Check the field has no new file selected by checking the reset button is present
    let fileFieldHasNoSelection (page:IPage)(field:string)= task{
        // This matches input ou textarea. Both xpaths are separated by |
        do! elementNotVisible(page,xPathToResetChosenFile field)
    }
    // Validates that the deletion confirmation is displayed. To be called after the dustbin icon has been clicked
    let attachmentDeleteConfirmationVisible (page:IPage)(field:string) = task {
        do! elementVisible(page,xPathToConfirmDeletion field)
        do! elementVisible(page,xPathToCancelDeletion field)
        do! page.Locator(xPathToConfirmDeletion field).HoverAsync()
        do! elementVisible(page, $"""{xPathForFormFieldContainerFromLabel field}//span[@data-bs-toggle="tooltip" and @data-bs-original-title="Confirm deletion?"]""")
    }

    // Validates that the deletion confirmation is NOT currently displayed.
    let attachmentDeleteConfirmationNotVisible (page:IPage)(field:string) = task {
        do! elementNotVisible(page,xPathToConfirmDeletion field)
        do! elementNotVisible(page,xPathToCancelDeletion field)
        do! elementNotVisible(page, $"""{xPathForFormFieldContainerFromLabel field}//span[@data-bs-toggle="tooltip" and @data-bs-original-title="Confirm deletion?"]""")
    }

    // Confirm deletion
    let confirmDeletion (page:IPage)(field:string) = task {
        do! attachmentDeleteConfirmationVisible page field
        do! page.Locator(xPathToConfirmDeletion field).ClickAsync()
    }

    // Cancel deletion
    let cancelDeletion (page:IPage)(field:string) = task {
        do! attachmentDeleteConfirmationVisible page field
        do! page.Locator(xPathToCancelDeletion field).ClickAsync()
    }

    // Helper used in form tests
    let selectFileForFormField (page:IPage)(field:string)(filename:string) = task {
        do! selectFileForFieldSelector
              page
              ($"""{xPathForFormFieldContainerFromLabel field}//input""")
              ($"fixtures/files/attachments/{filename}")
    }

    // Reset chosen file in instance form
    let resetChosenFile (page:IPage)(field:string) = task {
        do! page.Locator(xPathToResetChosenFile field).ClickAsync()
        do! elementNotVisible(page,xPathToResetChosenFile field)
    }

    // Delete an attachment, clicking yes in the confirmation dialog
    let deleteAttachment (page:IPage)(field:string) = task {
        do! clickDustBinOfAttachment page field
        do! confirmDeletion page field
    }

    // Press the cancel displayed while form submission can still be cancelled (because form
    // uploads are busy)
    let pressCancel (page:IPage) (xPathAncestor:string)= task {
        let selector = $"""{xPathAncestor}//button[text()="Cancel"]"""
        do! page.Locator(selector).ClickAsync()
    }

    // Function to check is form submission is disabled
    let submitButtonIsDisabled (page:IPage) (ancestor:string)= task {
        do! elementDisabled(page,xPathToSaveButton ancestor)
        // also check the css class disabled is set
        do! elementVisible(page,xPathToDisabledSaveButton ancestor)
    }
    // Function to check is form submission is enabled
    let submitButtonIsEnabled (page:IPage) (ancestor:string)= task {
        do! elementEnabled(page,xPathToSaveButton ancestor)
        // also check the css class disabled is not set
        do! elementNotVisible(page,xPathToDisabledSaveButton ancestor)
    }

    // Helpers and type to fill a form of entity 100001
    type Values100001 =
        {
            name: string
            notes: string
            birthdate: string
            count: string
            approved: string
            email: string
            website: string
            logo: string
            picture: string
        }

    let checkInAppFormAfterSubmit page values = task{
        let fieldHasValue = checkFieldHasValue page
        do! textIsVisible(page,"Saved successfully")

        // Check change overview is displayed
        do! isInOverviewWithValue page "name" values.name
        do! isInOverviewWithValue page "notes" values.notes
        do! isInOverviewWithValue page "birthdate" $"{values.birthdate} 00:00:00"
        do! isInOverviewWithValue page "count" values.count
        do! isInOverviewWithValue page "approved" values.approved
        do! isInOverviewWithValue page "email" values.email
        do! isInOverviewWithValue page "website" values.website
        do! isInOverviewWithValue page "logo" values.logo
        do! isInOverviewWithValue page "picture" values.picture


        // Check fields are reset after submitting the creation form
        do! fieldHasValue "name" ""
        do! fieldHasValue "notes" ""
        do! fieldHasValue "birthdate" ""
        do! fieldHasValue "count" ""
        do! fieldHasValue "approved" "0"
        do! fieldHasValue "email" ""
        do! fieldHasValue "website" ""
        do! fileFieldHasNoFileOnServer page "logo"
        do! fileFieldHasNoSelection page "logo"
        do! fileFieldHasNoSelection page "picture"
    }

    let fillSubmitAndValidatedFullForm100001(page:IPage)(values:Values100001)(inApp:bool)= task {
            let ancestor = """div[data-form-entity-id="100001"]"""
            let! fields = getFieldsMap page ancestor

            // Some helper defined by partial applications
            let save () = task {
                do! page.Locator($"""{ancestor} button:text("Save")""").ClickAsync()
                // As we have multiple saves on the save page, introduce a delay so the feedback of the previous
                // save is removed before the checks on the feedback of this save are done
                do! Async.Sleep 100
            }
            let fill = fillFieldFromMapWith fields
            let setFile = setFileFieldFromMapWith fields
            let setOption = chooseOptionFieldFromMapWith fields


            // Initialise db state checker
            let expectedStateDifference =
                { DBState.zeroState with
                    instances = 1L
                    maxInstanceId = 1
                    detailValues = 6L
                    maxDetailValueId = 6
                    ddlDetailValues = 1L
                    maxDdlDetailValueId = 1
                    dateDetailValues = 1L
                    maxDateDetailValueId = 1
                    integerDetailValues = 1L
                    maxIntegerDetailValueId = 1
                }

            let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client
                    [ 1L, "select count(*) from detail_values where type='SimpleDetailValue'"
                      1L, "select count(*) from detail_values where type='LongTextDetailValue'"
                      2L, "select count(*) from detail_values where type='FileAttachmentDetailValue'"
                      1L, "select count(*) from detail_values where type='EmailDetailValue'"
                      1L, "select count(*) from detail_values where type='WebUrlDetailValue'"
                    ]
                    expectedStateDifference
            do! fill "name" values.name
            do! fill "notes" values.notes
            do! fill "birthdate" values.birthdate
            do! fill "count" values.count
            let! _ = setOption "approved" values.approved
            do! fill "email" values.email
            do! fill "website" values.website
            do! setFile "logo" values.logo
            do! setFile "picture" values.picture

            // Submit it
            do! save()
            if inApp then
                do! checkInAppFormAfterSubmit page values
            else
                do! textIsVisible(page,"Your form has been successfully registered")

            do! checkDifferenceNow ()
            do! client.Close()

    }
    let fillSubmitAndValidatePartial10001 (page:IPage)(name:string)= task {
        // Define some helpers also present in fillSubmitAndValidatedFullForm100001.
        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)
        let ancestor = """div[data-form-entity-id="100001"]"""
        let! fields = getFieldsMap page ancestor
        // Some helper defined by partial applications
        let save () = task {
            do! page.Locator($"""{ancestor} button:text("Save")""").ClickAsync()
            // As we have multiple saves on the save page, introduce a delay so the feedback of the previous
            // save is removed before the checks on the feedback of this save are done
            do! Async.Sleep 100
        }
        let fill = fillFieldFromMapWith fields
        let fieldHasValue = checkFieldHasValue page
        // Now submit form with fields left empty. This checks that the detail_value_id in the spec is
        // reset correctly (which was not the case at a time).
        let expectedStateDifference =
            { DBState.zeroState with
                  instances = 1L
                  maxInstanceId = 1
                  detailValues = 1L
                  maxDetailValueId = 1
            }

        // Only one detail value will be filled in
        let! preState, checkDifferenceNow =
            DBState.startWithCustomQueries client
                [ 1L, "select count(*) from detail_values where type='SimpleDetailValue'"
                  0L, "select count(*) from detail_values where type='LongTextDetailValue'"
                  0L, "select count(*) from detail_values where type='FileAttachmentDetailValue'"
                  0L, "select count(*) from detail_values where type='EmailDetailValue'"
                  0L, "select count(*) from detail_values where type='WebUrlDetailValue'"
                ]
                expectedStateDifference
        // Fill form
        do! fill "name" name

        // Submit it
        // Before correcting debouncedInput to reset its internal variable, the button would be disabled
        // because the internal Var was out of sync with the debounced Var used extenally.
        do! save()
        do! textIsVisible(page,"Saved successfully")

        // Check fields are as expected after submission
        do! fieldHasValue "name" ""
        do! fieldHasValue "notes" ""
        do! fieldHasValue "birthdate" ""
        do! fieldHasValue "count" ""
        do! fieldHasValue "approved" "0"
        do! fieldHasValue "email" ""
        do! fieldHasValue "website" ""
        do! fileFieldHasNoFileOnServer page "logo"
        do! fileFieldHasNoSelection page "logo"
        do! fileFieldHasNoSelection page "picture"

        // Check change overview is displayed
        do! isInOverviewWithValue page "name" name
        do! isMarkedLeftEmptyInOverview page "notes"
        do! isMarkedLeftEmptyInOverview page "birthdate"
        do! isMarkedLeftEmptyInOverview page "count"
        do! isMarkedLeftEmptyInOverview page "approved"
        do! isMarkedLeftEmptyInOverview page "email"
        do! isMarkedLeftEmptyInOverview page "website"
        do! isMarkedLeftEmptyInOverview page "logo"
        do! isMarkedLeftEmptyInOverview page "picture"

        // Check correct database changes are observed
        do! checkDifferenceNow ()

        do! client.Close()

    }
    let checkFormEntryInTable (page:IPage)(values:Values100001) = task {
        let rowHasValue = rowWithTextHasNextSiblingColumn page values.name
        do! textIsVisible(page,values.name)
        do! rowHasValue $"{values.birthdate} 00:00:00"
        do! rowHasValue values.count
        do! rowHasValue values.approved
        do! rowHasValue values.email
        do! rowHasValue values.website
        do! rowHasValue values.logo
        do! rowHasValue values.picture
        do! checkDownloadLink
                page
                $"""//div[@id="table_main_entity_100001"]//a[text()="{values.logo}"]"""
                $"fixtures/files/attachments/{values.logo}"
                values.logo
        do! checkDownloadLink
                page
                $"""//div[@id="table_main_entity_100001"]//a[text()="{values.picture}"]"""
                $"fixtures/files/attachments/{values.picture}"
                values.picture
    }
    // End helpers to fill form for entity

module AdminHelpers =
    let checkAdminTable (page:IPage) (selector:string)(validationFile:string)= unitTask {
        let testedTable = page.Locator(selector)

        let controlDoc = HtmlAgilityPack.HtmlDocument()
        controlDoc.OptionEmptyCollection <- true
        controlDoc.Load(validationFile)

        // look at all columns
        let cellsSelector = "//td"
        try
          // check explicitly that we have the expected number of columns to avoid bugs
          // where both control and page would return 0 (eg if selector had to be updated)
          do! withException
                  $"Admin table content cell count failed for {validationFile}"
                  (task {
                      let! controlCount = testedTable.Locator(selector).CountAsync()
                      let cells = testedTable.Locator(cellsSelector)
                      let controlCells = controlDoc.DocumentNode.SelectNodes(cellsSelector)
                      // check counts
                      do! checkElementsCount cells controlCells
                  })
          do! withException
                  $"Admin table content check failed for {validationFile}"
                  (task {
                      do! checkXPathInnerText testedTable controlDoc cellsSelector
                  })
        with
        | e ->
            let! outerHTML = testedTable.EvaluateAsync("node => node.outerHTML")
            System.IO.File.WriteAllText($"{validationFile}.actual.html", outerHTML.ToString() )
            do! screenshotAsync(page,$"{validationFile}.actual.png")
            raise (new System.Exception($"Mismatch in admin table check, look at inner exception and check actual output at\nbin/Debug/net*/{validationFile}.actual.html\n and screenshot at bin/Debug/net*/{validationFile}.actual.png.\nPossible command to update test:\n\n  cp bin/Debug/net*/{validationFile}.actual.html {validationFile}\n\n",e))
    }
