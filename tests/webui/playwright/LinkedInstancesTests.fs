namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open System.Collections.Generic
open MyOwnDB.Tests.Helpers

[<TestFixture>]
type LinkedInstancesTest() =
    inherit PageTest()


    // Helper to check links creation and deletion in database
    let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)
    let getLinksDifferenceChecker (i:int) = async {
        // Initialise db state checker
        let expectedStateDifference =
                { DBState.zeroState with
                        links = int64 i
                        maxLinkId = if i<0 then 0 else i
                }
        let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client [] expectedStateDifference
        return checkDifferenceNow
    }



    let checkLinkAdditionAndRemoval (page:IPage)(info:{|startsWithNoLinked:bool; fromInstanceId: int; who: string; relationId: int; entityId: int; columnsCount: int; instanceTextSelector:string; searchedColumn:string; hasLinks:bool; canLinkOnlyOne: bool|}) = task {
        let rootId = $"""#card_linked_{info.fromInstanceId}_{info.who}_relation_{info.relationId}"""
        let rootXPath = $"""//div[@id="card_linked_{info.fromInstanceId}_{info.who}_relation_{info.relationId}"]"""
        let linkedXPath = $"""//div[@id="table_linked_{info.fromInstanceId}_{info.who}_relation_{info.relationId}_entity_{info.entityId}"]"""
        let linkedId = $"#table_linked_{info.fromInstanceId}_{info.who}_relation_{info.relationId}_entity_{info.entityId}"
        let linkableXPath = $"""//div[@id="table_linkable_{info.fromInstanceId}_{info.who}_relation_{info.relationId}_entity_{info.entityId}"]"""
        let linkableId = $"#table_linkable_{info.fromInstanceId}_{info.who}_relation_{info.relationId}_entity_{info.entityId}"
        let linkExistingButtonXPath = $"""{rootXPath}//button[text()="Link existing entry"]"""
        let finishLinkExistingButtonXPath = $"""{rootXPath}//button[text()="Finish linking existing entry"]"""
        let linkNewButtonXPath = $"""{rootXPath}//button[text()="Add new"]"""
        let linkNewFormXPath = $"""{rootXPath}//div[@data-form-entity-id="{info.entityId}"]"""
        let linkNewFormLocator = page.Locator(linkNewFormXPath)
        let spec={searchedColumn=info.searchedColumn; searchedValue=""; perPage=10; columnsCount=info.columnsCount; sorting= None; hasLinks=info.hasLinks; paginationLinks=1}
        let Check = checkTable page

        do! elementVisible(page,linkExistingButtonXPath)
        do! elementVisible(page,linkNewButtonXPath)
        do! elementNotVisible(page,linkableXPath)
        do! page.ClickAsync(linkExistingButtonXPath)
        do! elementVisible(page,linkableXPath)


        // **********************
        // Link existing instance
        // **********************
        // check status before linking
        let! checkLinksDifferenceNow = getLinksDifferenceChecker 1
        // set hasLinks to false and paginationLinks 0 for empty table
        do! Check
                linkedId
                $"tables/linked_instances_management/linked_{info.who}_from_{info.fromInstanceId}_rel_{info.relationId}_all_pp10_p1.html"
                (if info.startsWithNoLinked then {spec with paginationLinks=0; hasLinks=false} else spec)
        do! Check
                linkableId
                $"tables/linked_instances_management/linkable_{info.who}_from_{info.fromInstanceId}_rel_{info.relationId}_all_pp10_p1.html"
                spec
        // link instance
        let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task {
                do! clickActionCell page linkableXPath info.instanceTextSelector "link" }) |> Async.AwaitTask
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        // check status after linking
        do! checkLinksDifferenceNow ()
        do! Check
                linkedId
                $"tables/linked_instances_management/linked_{info.who}_from_{info.fromInstanceId}_rel_{info.relationId}_all_pp10_p1_after_add.html"
                {spec with paginationLinks=1}
        if info.canLinkOnlyOne then
                do! Assertions.Expect(page.Locator(linkableId)).ToBeHiddenAsync()
                do! Assertions.Expect(page.Locator(linkExistingButtonXPath)).ToBeHiddenAsync()
        else
                do! Check
                        linkableId
                        $"tables/linked_instances_management/linkable_{info.who}_from_{info.fromInstanceId}_rel_{info.relationId}_all_pp10_p1_after_add.html"
                        spec

        // Take snapshot before unlinking
        let! checkLinksDifferenceNow = getLinksDifferenceChecker -1
        // unlink instance
        let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task {
                do! clickActionCell page linkedXPath info.instanceTextSelector "unlink" }) |> Async.AwaitTask
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        // Check we are back at the previous situation
        do! checkLinksDifferenceNow ()
        if info.canLinkOnlyOne then
                do! Assertions.Expect(page.Locator(linkableId)).ToBeHiddenAsync()
                do! Assertions.Expect(page.Locator(linkExistingButtonXPath)).ToBeHiddenAsync()
        else
                do! Check
                        linkableId
                        $"tables/linked_instances_management/linkable_{info.who}_from_{info.fromInstanceId}_rel_{info.relationId}_all_pp10_p1.html"
                        spec
                // Hide list of existing linkable instances
                do! page.ClickAsync(finishLinkExistingButtonXPath)
        // *****************
        // Link new instance
        // *****************
        do! page.ClickAsync(linkNewButtonXPath)
        do! Assertions.Expect(linkNewFormLocator).ToBeVisibleAsync()
        let! fields = MyOwndbForms.getFieldsMap page rootId
        let fill = MyOwndbForms.fillFieldFromMapWith fields
        let save = page.Locator($"""{rootXPath}//button[text()="Save"]""").ClickAsync
        let expectedLinkNewStateDifference =
            { DBState.zeroState with
                  instances = 1L
                  maxInstanceId = 1
                  detailValues = 1L
                  maxDetailValueId = 1
                  links = 1L
                  maxLinkId = 1
            }
        let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client [] expectedLinkNewStateDifference

        // We don't test the whole form here, so we fill just one field.
        let key,filledText =
                if Map.containsKey "nom" fields then
                        "nom", "JonJon"
                else
                        "titre", "Report for linking new instance"

        do! fill key filledText
        do! save()
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask

        // Wait for updated table to be displayed
        do! page.Locator($"""//td[text()="{filledText}"]""").WaitForAsync()

        // Check new entry is in the table
        do! Check
                linkedId
                $"tables/linked_instances_management/linked_{info.who}_from_{info.fromInstanceId}_rel_{info.relationId}_all_pp10_p1_after_new.html"
                spec
        do! checkDifferenceNow()

        // Go to list of instances to delete what we inserted
        let instanceUrl = page.Url
        let mainTableId = $"""div#table_main_entity_{info.entityId}"""
        let mainTableXPath = $"""//div[@id="table_main_entity_{info.entityId}"]"""
        let expectedLinkNewStateDifference =
            { DBState.zeroState with
                  instances = -1L
                  detailValues = -1L
                  links = -1L
            }
        let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client [] expectedLinkNewStateDifference
        do! goToPathAsync(page, $"/instances/{info.entityId}")
        do! setPerPage page mainTableId 500
        do! deleteInstanceFromTable page mainTableXPath filledText
        do! checkDifferenceNow()
        do! goToUrlAsync(page,instanceUrl)
        // Check we have the initial state again
        do! Check
                linkedId
                $"tables/linked_instances_management/linked_{info.who}_from_{info.fromInstanceId}_rel_{info.relationId}_all_pp10_p1.html"
                (if info.startsWithNoLinked then {spec with paginationLinks=0; hasLinks=false} else spec)
    }


    [<Test>]
    member self.``linked_instances_management_from_child`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"existingbob@test.com")
        do! goToPathAsync(page, "/instance/100200")
        // view instances for entity id 12
        let! _ = page.WaitForSelectorAsync(".instance-display .instance-field-label")
        let labels = page.Locator(".instance-display .instance-field-label")

        let! labelsText = labels.AllInnerTextsAsync()
        labelsText |> List.ofSeq |> should equal ["nom";"prenom";"fonction";"service";"coordonees_specifiques";"company_email"]

        let values = page.Locator(".instance-display .instance-field-value")
        let! valuesText = values.AllInnerTextsAsync()
        valuesText |> List.ofSeq |> should equal ["Runolfsdottir";"Greg";"Product Brand Coordinator";"Baby & Kids"; String.Empty; String.Empty]
        // check related instances are displayed. Their working is checked in test "linked_instances_datatable"
        do! elementVisible(page,"#table_linked_100200_parents_relation_8_entity_19")
        do! elementVisible(page,"#card_linked_100200_parents_relation_8")
        do! elementVisible(page,"#table_linked_100200_parents_relation_7_entity_11")
        do! elementVisible(page,"#card_linked_100200_parents_relation_7")
        do! elementVisible(page,"#table_linked_100200_parents_relation_9_entity_11")
        do! elementVisible(page,"#card_linked_100200_parents_relation_9")


        // Work on link "société de"
        let rootXPath = $"""//div[@id="card_linked_100200_parents_relation_7"]"""
        let linkedId = "#table_linked_100200_parents_relation_7_entity_11"
        let linkedXPath = """//div[@id="table_linked_100200_parents_relation_7_entity_11"]"""
        let linkableId = "#table_linkable_100200_parents_relation_7_entity_11"
        let linkableXPath = """//div[@id="table_linkable_100200_parents_relation_7_entity_11"]"""
        do! elementVisible(page,linkedId)
        do! elementNotVisible(page,linkableId)
        do! elementVisible(page,$"""{rootXPath}//button[text()="Link existing entry"]""")
        do! elementNotVisible(page,$"""{rootXPath}//button[text()="Import CSV"]""")
        do! page.ClickAsync($"""{rootXPath}//button[text()="Link existing entry"]""")
        do! elementVisible(page,linkableId)


        let spec={searchedColumn="nom"; searchedValue=""; perPage=10; columnsCount=5; sorting= None; hasLinks=true; paginationLinks=2}
        let Check = checkTable page
        ()

        do! Check
                linkedId
                "tables/linked_instances_management/linked_parents_from_100200_rel_7_all_pp10_p1.html"
                {spec with hasLinks=false; paginationLinks=0}
        do! Check
                linkableId
                "tables/linked_instances_management/linkable_parents_from_100200_rel_7_all_pp10_p1.html"
                spec

        // Link instances, many to many relation
        // *************************************

        let! checkLinksDifferenceNow = getLinksDifferenceChecker 3
        do! ["hjhjhjk"; "BE 728 842 258";"BE230202020"]
            // Map to async here, and start them sequentially later in the pipeline
            // Using a task here does not work as a task is started immediately! In that case all run in parallel
            // and the order of addition of linked instances is not respected!
            |> List.mapi (fun i s ->  async {
                let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task {
                        do! clickActionCell page linkableXPath s "link" }) |> Async.AwaitTask
                do! page.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask

                do! Check
                        linkedId
                        $"tables/linked_instances_management/linked_parents_from_100200_rel_7_all_pp10_p1_after_add_{i+1}.html"
                        {spec with hasLinks=true; paginationLinks=1} |> Async.AwaitTask
                do! Check
                        linkableId
                        $"tables/linked_instances_management/linkable_parents_from_100200_rel_7_all_pp10_p1_after_add_{i+1}.html"
                        spec |> Async.AwaitTask
            }
            )
            |> Async.Sequential
            |> Async.Ignore

        do! checkLinksDifferenceNow()

        // We unlink this one because it clashes with the _from_parent tests. Removing it leaves the instance 71
        // as loaded from fixtures, and so there"s no impact on the other test which was authored with data from
        // fixtures (with `make run-with-test-data`)
        let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task {
                do! clickActionCell page linkedXPath "BE 728 842 258" "unlink" })
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle)

        // Filter the linkable table and link the instance displayed
        let! checkLinksDifferenceNow = getLinksDifferenceChecker 1
        do! filterTable page linkableId "personnes_occuppees" "65"
        do! Check
                linkableId
                $"tables/linked_instances_management/linkable_parents_from_100200_rel_7_all_pp10_p1_filtered_65.html"
                { spec with paginationLinks=1; searchedColumn="personnes_occuppees";searchedValue="65"}
                |> Async.AwaitTask
        let! _request = page.RunAndWaitForRequestFinishedAsync(fun () -> task {
                do! clickActionCell page linkableXPath "65" "link" }) |> Async.AwaitTask
        do! page.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        do! checkLinksDifferenceNow()
        do! Check
                linkableId
                $"tables/linked_instances_management/linkable_parents_from_100200_rel_7_all_pp10_p1_filtered_65_after_link.html"
                // hasLinks is false as there is no row displayed in the linkable table after linking the only one displayed by the filter
                { spec with paginationLinks=1; searchedColumn="personnes_occuppees";searchedValue="65"; hasLinks=false}
                |> Async.AwaitTask

        // many to one relation from child (contact_de_visite)
        // ***************************************************
        // canOnlyLinkOne is true, but has no importance because hasLinks is false, and it is only used to set hasLinks to false for an empty table....
        do! checkLinkAdditionAndRemoval
                page
                {|startsWithNoLinked=true; fromInstanceId=100200;who="parents"; relationId=8;entityId=19;columnsCount=3;instanceTextSelector="memo instance dvid 473";searchedColumn="date";hasLinks=false; canLinkOnlyOne=true|}
        // one to many relation from child ( is ceo at)
        // ***************************************************
        // hasLinks is true because it has an email column
        do! checkLinkAdditionAndRemoval
                page
                {|startsWithNoLinked=true; fromInstanceId=100200; who="parents"; relationId=10;entityId=11;columnsCount=5;instanceTextSelector="BE230202020";searchedColumn="nom"; hasLinks=true; canLinkOnlyOne=false|}
        // one to one relation from child ( contact prefere de)
        // ***************************************************
        do! checkLinkAdditionAndRemoval
                page
                {|startsWithNoLinked=true; fromInstanceId=100200; who="parents"; relationId=9;entityId=11;columnsCount=5;instanceTextSelector="BE 728 842 258";searchedColumn="nom"; hasLinks=true; canLinkOnlyOne=true|}
    }

    [<Test>]
    member self.``linked_instances_management_from_parent`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"existingbob@test.com")
        do! goToPathAsync(page, "/instance/71")
        // view instances for entity id 12
        let! _ = page.WaitForSelectorAsync(".instance-display .instance-field-label")
        let labels = page.Locator(".instance-display .instance-field-label")

        let! labelsText = labels.AllInnerTextsAsync()
        labelsText |> List.ofSeq |> should equal ["nom";"code_nace";"TVA";"adresse";"personnes_occuppees";"telephone";"fax";"memo";"status";"company_email"]

        let values = page.Locator(".instance-display .instance-field-value")
        let! valuesText = values.AllInnerTextsAsync()
        valuesText |> List.ofSeq |> should equivalent [String.Empty; String.Empty;"BE 728 842 258";"kasteelbaan 45";"1";"+32 499 999 999";String.Empty;String.Empty;"sprl";String.Empty]
        // check related instances are displayed. Their working is checked in test "linked_instances_datatable"
        do! elementVisible(page,"#table_linked_71_children_relation_7_entity_12")
        do! elementVisible(page,"#card_linked_71_children_relation_7")
        do! elementVisible(page,"#table_linked_71_children_relation_9_entity_12")
        do! elementVisible(page,"#card_linked_71_children_relation_9")
        do! elementVisible(page,"#table_linked_71_children_relation_10_entity_12")
        do! elementVisible(page,"#card_linked_71_children_relation_10")


        // many to many relation from parent (contact_de_la_societe)
        // *********************************************************
        // canOnlyLinkOne is false, but has no importance because hasLinks is false, and it is only used to set hasLinks to false for an empty table....
        do! checkLinkAdditionAndRemoval
                page
                {|startsWithNoLinked= false; fromInstanceId=71;who="children"; relationId=7;entityId=12;columnsCount=5;instanceTextSelector="Bernadette";searchedColumn="nom";hasLinks=true; canLinkOnlyOne=false|}
        // one to one relation from parent ( contact prefere)
        // ***************************************************
        do! checkLinkAdditionAndRemoval
                page
                {|startsWithNoLinked=true; fromInstanceId=71; who="children"; relationId=9;entityId=12;columnsCount=5;instanceTextSelector="Kimberly";searchedColumn="nom"; hasLinks=true; canLinkOnlyOne=true|}
        // many to one relation from parent ( has CEO )
        // ***************************************************
        // hasLinks is true because it has an email column
        do! checkLinkAdditionAndRemoval
                page
                {|startsWithNoLinked=true; fromInstanceId=71; who="children"; relationId=10;entityId=12;columnsCount=5;instanceTextSelector="Nadine";searchedColumn="nom"; hasLinks=true; canLinkOnlyOne=true|}

        // one to many from parent
        do! goToPathAsync(page, "/instance/96")
        do! checkLinkAdditionAndRemoval
                page
                {|startsWithNoLinked=true; fromInstanceId=96; who="children"; relationId=8;entityId=12;columnsCount=5;instanceTextSelector="Willie";searchedColumn="nom"; hasLinks=true; canLinkOnlyOne=false|}
    }

    [<Test>]
    member self.``linked_instances_action_cells_from_child`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"existingbob@test.com")
        do! goToPathAsync(page, "/instance/70")

        let rootXPath = $"""//div[@id="card_linked_70_parents_relation_7"]"""
        let linkableId = "#table_linkable_70_parents_relation_7_entity_11"
        let linkableXPath = """//div[@id="table_linkable_70_parents_relation_7_entity_11"]"""
        let linkedXPath = """//div[@id="table_linked_70_parents_relation_7_entity_11"]"""

        // linked action cells
        let linkedViewActionXPath = $"""{linkedXPath}//tr/td[contains(@class,"myowndbactioncell")][1]/a[@href="/instance/69"]"""
        let linkedUnlinkActionXPath = $"""{linkedXPath}//tr/td[contains(@class,"myowndbactioncell")][2]/a[@data-myowndb-action="unlink"]"""
        // linkeable action cells
        let linkableViewActionXPath = $"""{linkableXPath}//tr/td[contains(@class,"myowndbactioncell")][1]/a[@href="/instance/71"]"""
        let linkableLinkActionXPath = $"""{linkableXPath}//tr/td[contains(@class,"myowndbactioncell")][2]/a[@data-myowndb-action="link"]"""

        // Display linkable list
        do! page.ClickAsync($"""{rootXPath}//button[text()="Link existing entry"]""")
        do! elementVisible(page,linkableId)

        // Check action cells count
        do! checkXPathCount (page.Locator(linkedXPath))  """//tr[1]/td[contains(@class,"myowndbactioncell")]""" (equal 2)
        do! checkXPathCount (page.Locator(linkableXPath))  """//tr[1]/td[contains(@class,"myowndbactioncell")]""" (equal 2)

        // Now check the action cells are visible
        do! elementVisible(page,linkedViewActionXPath)
        do! elementVisible(page,linkedUnlinkActionXPath)
        do! elementVisible(page,linkableViewActionXPath)
        do! elementVisible(page,linkableLinkActionXPath)

    }

    [<Test>]
    member self.``linked_instances_action_cells_from_parent`` () = unitTask {
        let page = self.Page

        do! authenticate(page,"existingbob@test.com")
        do! goToPathAsync(page, "/instance/69")

        let rootXPath = $"""//div[@id="card_linked_69_children_relation_7"]"""
        let linkableId = "#table_linkable_69_children_relation_7_entity_12"
        let linkableXPath = """//div[@id="table_linkable_69_children_relation_7_entity_12"]"""
        let linkedXPath = """//div[@id="table_linked_69_children_relation_7_entity_12"]"""

        // linked action cells
        let linkedViewActionXPath = $"""{linkedXPath}//tr/td[contains(@class,"myowndbactioncell")][1]/a[@href="/instance/70"]"""
        let linkedUnlinkActionXPath = $"""{linkedXPath}//tr/td[contains(@class,"myowndbactioncell")][2]/a[@data-myowndb-action="unlink"]"""
        // linkeable action cells
        let linkableViewActionXPath = $"""{linkableXPath}//tr/td[contains(@class,"myowndbactioncell")][1]/a[@href="/instance/72"]"""
        let linkableLinkActionXPath = $"""{linkableXPath}//tr/td[contains(@class,"myowndbactioncell")][2]/a[@data-myowndb-action="link"]"""

        // Display linkable list
        do! page.ClickAsync($"""{rootXPath}//button[text()="Link existing entry"]""")
        do! elementVisible(page,linkableId)

        // Check action cells count
        do! checkXPathCount (page.Locator(linkedXPath))  """//tr[1]/td[contains(@class,"myowndbactioncell")]""" (equal 2)
        do! checkXPathCount (page.Locator(linkableXPath))  """//tr[1]/td[contains(@class,"myowndbactioncell")]""" (equal 2)

        // Now check the action cells are visible
        do! elementVisible(page,linkedViewActionXPath)
        do! elementVisible(page,linkedUnlinkActionXPath)
        do! elementVisible(page,linkableViewActionXPath)
        do! elementVisible(page,linkableLinkActionXPath)

    }
