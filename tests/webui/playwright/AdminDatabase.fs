namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open System.Collections.Generic
open MyOwndbForms
open FSharpPlus
open MyOwnDB.Tests.Helpers
open AdminHelpers


[<TestFixture;Parallelizable>]
type AdminentitiesAccess() =
    inherit PageTest()
    [<Test>]
    member self.``admin_entities_access`` () = unitTask {
        let page = self.Page

        // User admin@test.com has account id 1, and database 3 has account id 2, so this access should be refused
        // and an error reported.
        do! authenticate(page,"admin@test.com")
        do! checkMenusInLanguageAsync(page,adminMenu,"en")
        do! goToPathAsync(page, "/admin/database/3")
        do! textIsVisible(page,"An error occured")
        ()
    }

type Adminentities() =
    inherit PlaywrightTest()
    [<Test>]
    member self.``admin_entities_manage`` () = unitTask {
        let databaseId = 6
        let pw = self.Playwright
        let! adminBrowser = pw.Chromium.LaunchAsync()
        let! adminPage = adminBrowser.NewPageAsync()
        let! userBrowser = pw.Chromium.LaunchAsync()
        let! userPage = userBrowser.NewPageAsync()
        let tableXPath = """//table[@id="admin_entities_table"]"""
        let takeAction =
            clickActionCell adminPage tableXPath
        let formSelector = """//form[@id="admin_entities_form"]"""
        let save() =
            adminPage.Locator("""button:text("Save")""").ClickAsync()
        // entity client for db content checks
        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

        // Helper to get db changes checking function
        let initEntitiesInDBCheck (changes:{|all:int64;forDatabase:int64|}) = task {
            let! preState, checkDifferenceNow =
                DBState.startWithOnlyCustomQueries client
                    [
                        changes.all,"select count(*) from entities "
                        changes.forDatabase,$"select count(*) from entities where database_id={databaseId}"
                    ]
            return checkDifferenceNow
        }


        do! authenticate(adminPage,"admin@test.com")
        do! checkMenusInLanguageAsync(adminPage,adminMenu,"en")
        do! goToPathAsync(adminPage, $"/admin/database/{databaseId}")
        do! checkAdminTable adminPage tableXPath "tables/admin/entities/from_fixtures.html"

        let editedEntity = "engagement"

        // Edit entity name
        // ----------------------------------------------------
        // We will edit an admin user to be a standard user
        let! checkDifferenceNow = initEntitiesInDBCheck {|all= 0L; forDatabase=0L|}
        do! takeAction editedEntity "edit"
        do! elementVisible(adminPage,formSelector)
        do! adminPage.GetByLabel("Name", PageGetByLabelOptions(Exact=true)).FillAsync("Engagements")
        do! adminPage.GetByLabel("Has public form").CheckAsync()
        do! save()
        // Form is hidden after ssaving
        do! elementNotVisible(adminPage,formSelector)
        // With infirmative feedback displayed
        do! textIsVisible(adminPage,"Entity saved successfully")
        // The table is updated
        do! checkAdminTable adminPage tableXPath "tables/admin/entities/after_edit.html"
        do! checkDifferenceNow()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/entities/after_edit.html"

        // Add new entity
        // ------------
        // We create a standard user
        let! checkDifferenceNow = initEntitiesInDBCheck {|all= 1L; forDatabase=1L|}
        let editedentity="New entity"
        do! adminPage.GetByRole(AriaRole.Button, PageGetByRoleOptions(Name = "Add new", Exact = true)).ClickAsync()
        do! textIsNotVisible(adminPage,"Entity saved successfully")
        do! elementVisible(adminPage,formSelector)
        do! adminPage.GetByLabel("Name", PageGetByLabelOptions(Exact=true)).FillAsync(editedentity)
        do! save()
        do! textIsVisible(adminPage,"Entity saved successfully")
        // Form is hidden after ssaving
        do! elementNotVisible(adminPage,formSelector)
        do! Async.Sleep 200
        // The table is updated
        do! checkAdminTable adminPage tableXPath "tables/admin/entities/after_add.html"
        do! checkDifferenceNow()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/entities/after_add.html"

        // Toggle public form
        // ------------------
        // Public form is initially available
        do! goToPathAsync(userPage, "/public_form/16")
        do! elementVisible(userPage,"//input")
        let! checkDifferenceNow = initEntitiesInDBCheck {|all= 0L; forDatabase=0L|}
        let editedEntity="Engagements"
        do! takeAction editedEntity "togglePublicForm"
        // Form is hidden after ssaving
        do! elementNotVisible(adminPage,formSelector)
        do! textIsVisible(adminPage,"Saved successfully")
        // The table is updated
        do! checkAdminTable adminPage tableXPath "tables/admin/entities/after_toggle.html"
        do! checkDifferenceNow()
        // Reload page to ensure fresh fetch from db and check displayed table
        do! reload adminPage
        do! checkAdminTable adminPage tableXPath "tables/admin/entities/after_toggle.html"
        // After toggling the public form, it should not be available anymore
        do! isStatus(userPage, "/public_form/16",404)
        // This should be added when a constraint on unique entity names per database are added.
        // It is not added now because there are databases with duplicate entity names.
        // // Attempt to add existing entity
        // // ----------------------------
        // // Unsuccessful, so no change in entity
        // let! checkDifferenceNow = initentitiesInDBCheck {|all= 0L; forAccount=0L|}
        // let editedUser = "New entity"
        // do! adminPage.Locator("""button:text("Add new")""").ClickAsync()
        // do! textIsNotVisible(adminPage,"entity saved successfully")
        // do! elementVisible(adminPage,formSelector)
        // do! adminPage.GetByLabel("Name", PageGetByLabelOptions(Exact=true)).FillAsync(editedUser)
        // do! save()
        // do! textIsNotVisible(adminPage,"entity saved successfully")
        // do! textIsVisible(adminPage,"An error occured")
        // do! checkDifferenceNow()
        // do! client.Close()
        // // Reload page to ensure fresh fetch from db and check displayed table
        // do! reload adminPage
        // do! checkAdminTable adminPage tableXPath "tables/admin/entities/after_add.html" 2 5
    }

    [<Test>]
    member self.``admin_details`` () = unitTask {
        let databaseId = 6
        let pw = self.Playwright
        let! adminBrowser = pw.Chromium.LaunchAsync()
        let! adminPage = adminBrowser.NewPageAsync()
        let tableXPath = """//table[@id="DetailsTable"]"""
        // Check list of propositions of new detail
        let checkNewPropositionsList (l:string seq) = task {
          let lisXPath = """//ul[@id="newPropositionsList"]/li"""
          do! Assertions.Expect(adminPage.Locator(lisXPath)).ToHaveCountAsync(l|>Seq.length)
          let! newPropositions = adminPage.Locator(lisXPath).AllTextContentsAsync()
          newPropositions |> should equal l
        }
        // client for db content checks
        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)
        // Helper to get db changes checking function
        // Takes int args that are transparently converted to int64 when needed
        let initDetailsInDBCheck (changes:{|detailValuePropositions:int;details:int|})(customQueries:(int64*string) list) = task {
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries client
                    (List.append
                      [
                          // check the changes are done in the expected database
                          changes.details,$"select count(*) from details where database_id={databaseId}"
                          changes.detailValuePropositions, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId}"
                      ]
                      customQueries
                    )
                    { DBState.zeroState with
                        details = changes.details
                        maxDetailId = changes.details
                        detailValuePropositions = changes.detailValuePropositions
                        maxDetailValuePropositionId = changes.detailValuePropositions
                    }
            return checkDifferenceNow
        }

        do! authenticate(adminPage,"admin@test.com")
        do! checkMenusInLanguageAsync(adminPage,adminMenu,"en")
        do! goToPathAsync(adminPage, $"/admin/database/{databaseId}")
        do! checkAdminTable adminPage tableXPath "tables/admin/details/from_fixtures.html"

        // Add a detail
        let! checkDifferenceNow = initDetailsInDBCheck {|detailValuePropositions=0;details=1|} []
        do! adminPage.Locator("""button:text("Add new detail")""").ClickAsync()
        do! adminPage.GetByLabel("Detail name").FillAsync("Abstract")
        do! chooseDdlValue adminPage "#newDetailDataType" "long text"
        let! _request = adminPage.RunAndWaitForRequestFinishedAsync(fun () -> task {
                          do! adminPage.ClickAsync("#submitNewEntityDetail")
                        }) |> Async.AwaitTask
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        do! checkAdminTable adminPage tableXPath "tables/admin/details/1_after_add.html"
        do! checkDifferenceNow ()

        // Add choice in list detail
        let! checkDifferenceNow = initDetailsInDBCheck {|detailValuePropositions=3;details=1|} []
        do! adminPage.GetByLabel("Detail name").FillAsync("Your choices")
        do! chooseDdlValue adminPage "#newDetailDataType" "choose in list"
        do! elementVisible(adminPage,"#newProposition")
        do! adminPage.GetByLabel("Add proposition").FillAsync("Choice 1")
        do! adminPage.PressAsync("#newProposition", "Enter")
        do! checkNewPropositionsList [|"Choice 1"|]
        do! adminPage.GetByLabel("Add proposition").FillAsync("Choice 2")
        do! adminPage.PressAsync("#newProposition", "Enter")
        do! checkNewPropositionsList [|"Choice 1"; "Choice 2"|]
        do! adminPage.GetByLabel("Add proposition").FillAsync("Choice 3")
        do! adminPage.PressAsync("#newProposition", "Enter")
        do! checkNewPropositionsList [|"Choice 1"; "Choice 2";"Choice 3"|]
        let! _request = adminPage.RunAndWaitForRequestFinishedAsync(fun () -> task {
                          do! adminPage.ClickAsync("#submitNewEntityDetail")
                        }) |> Async.AwaitTask
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        do! checkAdminTable adminPage tableXPath "tables/admin/details/2_after_add_ddl.html"
        do! checkDifferenceNow ()

        // Edit detail name
        let! checkDifferenceNow =
              initDetailsInDBCheck
                {|detailValuePropositions=0;details=0|}
                [
                  -1, "select count(*) from details where name='Your choices'"
                  -1, $"select count(*) from details where name='Your choices' and database_id={databaseId}"
                  1, "select count(*) from details where name='Possible choices'"
                  1, $"select count(*) from details where name='Possible choices' and database_id={databaseId}"
        ]
        do! adminPage.Locator($"""{tableXPath}//td[contains(.,"Your choices")]//span[@data-myowndb-action="edit"]""").ClickAsync()
        do! adminPage.Locator($"""{tableXPath}//input[contains(@class,"myowndb-detail-name-edition")]""").FillAsync("Possible choices")
        do! adminPage.Locator($"""{tableXPath}//input[contains(@class,"myowndb-detail-name-edition")]""").PressAsync("Enter")
        do! checkAdminTable adminPage tableXPath "tables/admin/details/3_after_name_edit.html"
        do! checkDifferenceNow ()

        // Edit value propositions
        let! checkDifferenceNow =
              initDetailsInDBCheck
                // No new propositions as we edit those existing
                {|detailValuePropositions=0;details=0|}
                [
                  // Old value propositions are not present anymore
                  -1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='Choice 1'"
                  -1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='Choice 1'"
                  -1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='Choice 2'"
                  -1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='Choice 2'"
                  -1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='Choice 3'"
                  -1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='Choice 3'"
                  // New propositions are added
                  1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='First choice'"
                  1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='First choice'"
                  1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='Second choice'"
                  1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='Second choice'"
                  1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='Third choice'"
                  1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='Third choice'"
                  // detail is not changed
                  0, "select count(*) from details where name='Possible choices'"
                  0, $"select count(*) from details where name='Possible choices' and database_id={databaseId}"
        ]
        do! adminPage.Locator($"""{tableXPath}//td[contains(.,"Choice 1,Choice 2,Choice 3")]//span[@data-myowndb-action="edit"]""").ClickAsync()
        do! adminPage.Locator($"""{tableXPath}//td[contains(@class,"myowndb-detail-propositions")]//li[position()=1]//input""").FillAsync("First choice")
        do! adminPage.Locator($"""{tableXPath}//td[contains(@class,"myowndb-detail-propositions")]//li[position()=2]//input""").FillAsync("Second choice")
        do! adminPage.Locator($"""{tableXPath}//td[contains(@class,"myowndb-detail-propositions")]//li[position()=3]//input""").FillAsync("Third choice")
        let rowSelector = $"""{tableXPath}//td[contains(.,"Possible choices")]/ancestor::tr"""
        do! adminPage.Locator($"""{rowSelector}//span[@data-myowndb-action="apply-propositions-changes"]""").ClickAsync()
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        do! checkAdminTable adminPage tableXPath "tables/admin/details/4_after_propositions_edit.html"
        do! checkDifferenceNow ()

        // Cancel edit
        let! checkDifferenceNow = initDetailsInDBCheck {|detailValuePropositions=0;details=0|} []
        do! adminPage.Locator($"""{tableXPath}//td[contains(.,"First choice,Second choice,Third choice")]//span[@data-myowndb-action="edit"]""").ClickAsync()
        do! adminPage.Locator($"""{tableXPath}//td[contains(@class,"myowndb-detail-propositions")]//li[position()=1]//input""").FillAsync("First choice edited")
        do! adminPage.Locator($"""{rowSelector}//span[@data-myowndb-action="cancel-propositions-changes"]""").ClickAsync()
        // No change to table
        do! checkAdminTable adminPage tableXPath "tables/admin/details/4_after_propositions_edit.html"
        do! checkDifferenceNow ()

        // Add proposition
        let! checkDifferenceNow =
              initDetailsInDBCheck
                // No new propositions as we edit those existing
                {|detailValuePropositions=1;details=0|}
                [
                  // Existing propositions are left as is
                  0, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='First choice'"
                  0, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='First choice'"
                  0, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='Second choice'"
                  0, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='Second choice'"
                  0, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='Third choice'"
                  0, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='Third choice'"
                  // New proposition added
                  1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where dvp.value='Fourth choice'"
                  1, $"select count(*) from detail_value_propositions dvp join details d on (d.id=dvp.detail_id) where d.database_id={databaseId} and dvp.value='Fourth choice'"
                  // detail is not changed
                  0, "select count(*) from details where name='Possible choices'"
                  0, $"select count(*) from details where name='Possible choices' and database_id={databaseId}"
        ]
        do! adminPage.Locator($"""{tableXPath}//td[contains(.,"First choice,Second choice,Third choice")]//span[@data-myowndb-action="edit"]""").ClickAsync()
        do! adminPage.Locator($"""{rowSelector}//span[@data-myowndb-action="add-proposition"]""").ClickAsync()
        do! adminPage.Locator($"""{tableXPath}//td[contains(@class,"myowndb-detail-propositions")]//li[position()=4]//input""").FillAsync("Fourth choice")
        do! adminPage.Locator($"""{rowSelector}//span[@data-myowndb-action="apply-propositions-changes"]""").ClickAsync()
        do! adminPage.WaitForLoadStateAsync(LoadState.NetworkIdle) |> Async.AwaitTask
        do! checkAdminTable adminPage tableXPath "tables/admin/details/5_after_add_choice.html"


    }
