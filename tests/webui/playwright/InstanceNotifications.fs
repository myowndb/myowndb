namespace Tests

open System
open NUnit
open NUnit.Framework
open Microsoft.Playwright
open Microsoft.Playwright.NUnit
open FSharp.Control.Tasks
open FsUnit
open Info
open MyowndbTests
open System.Collections.Generic
open MyOwndbForms
open FSharpPlus
open MyOwnDB.Tests.Helpers
open System.Text.RegularExpressions



[<TestFixture;>]
type InstanceNotifications() =
    inherit PageTest()
    [<Test>]
    member self.``instance_notification`` () = unitTask {
        let page = self.Page
        do! authenticate(page,"existingbob@test.com")
        let! t = getPageTranslatorAsync(page)
        let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)
        // view instances for entity id 100001
        do! goToPathAsync(page, "/instances/100001")

        let ancestor = """//div[contains(@class,"myowndb-instance-subscription-section")]"""
        let manageNotificationsButtonXPath = $"""//div[@id="add_instance_section"]//button[text()="Manage notifications"]"""
        let subscriptionFormXPath = $"""{ancestor}//div[contains(@class,"myowndb-instance-subscription-form")]"""
        let subscriptionsTableXPath = $"""{ancestor}//table[contains(@class,"myowndb-instance-subscriptions-list")]"""

        // The section is not displayed by default.
        do! elementNotVisible(page,$"""{ancestor}""")
        do! page.Locator(manageNotificationsButtonXPath).ClickAsync()
        do! elementVisible(page,$"""{ancestor}""")
        // The form is displayed
        do! elementVisible(page,subscriptionFormXPath)
        // No subscription initially, and the tbody is not displayed
        do! elementNotVisible(page,$"""{subscriptionsTableXPath}/tbody""")

        // Click submit to create new notification subscription
        let! request =  page.RunAndWaitForRequestFinishedAsync(fun () -> unitTask {
              do! page.ClickAsync($"""{ancestor}//div[contains(@class,"myowndb-instance-subscription-form")]//button[text()="Save"]""")
              ()
        })
        // Summary check to see if the subscription is now listed
        do! elementVisible(page,$"""{subscriptionsTableXPath}//td[text()="Creation"]""")
        do! elementVisible(page,$"""{subscriptionsTableXPath}//td[text()="Email"]""")
        do! elementVisible(page,$"""{subscriptionsTableXPath}//td[text()="existingbob@test.com"]""")
        do! page.ClickAsync("text=Logout")

        // Take db state, also to check link sent in the notification
        let! preState=
            DBState.getCurrentState client

        // Submit public form
        do! goToPathAsync(page, "/public_form/100001")
        let valuesPublic =
            {
                name = "Publica-Jon"
                notes = "Notes about Publica"
                birthdate = "1972-09-27"
                count = "942"
                approved = "Yes"
                email = "pub@pro.example.com"
                website = "http://tim.pro.example.com"
                logo = "pub-logo-small.png"
                picture = "pub-picture-medium.jpg"
            }
        let email = "existingbob@test.com"
        do! noMailSentTo(email)
        do! fillSubmitAndValidatedFullForm100001 page valuesPublic false
        // We had some errors of an empty array returned in the gitlab ci.
        // This attempts to read the mails, and if there is no mail,
        // we wait 1s and read the mails again.
        let! mails = task {
          let! firstRead = readMails(email)
          if firstRead |> Array.length > 0 then
            // We have mail, use that
            return firstRead
          else
            // No mail yet, wait 1s and then return the mails we have
            do! Async.Sleep 1000
            return! readMails(email)
        }
        let notification = mails |> Array.head
        cleanMailsFor(email)

        let link = notification |> firstLinkInMail
        notification.Subject |> should equal (t.t("New {0}", "Form tests"))
        link |> should equal $"{BaseUrl}/instance/{preState.maxInstanceId + 1}"

        // Log back in
        do! authenticate(page,"existingbob@test.com")
        // view instances for entity id 100001
        do! goToPathAsync(page, "/instances/100001")
        // Display notifications management
        do! page.Locator(manageNotificationsButtonXPath).ClickAsync()
        do! elementVisible(page,$"""{ancestor}""")
        let! preState, checkDifferenceNow =
            DBState.startWithCustomQueries client
                []
                { DBState.zeroState with notificationSubscriptions = -1L }
        // Delete subscription
        do! clickActionCell page subscriptionsTableXPath"Creation" "delete"
        do! clickRowDeleteConfirmation (page)(subscriptionsTableXPath) ("Creation")
        // Confirm deletion by checking the form is displayed
        do! elementVisible(page,subscriptionFormXPath)
        do! checkDifferenceNow()

    }
