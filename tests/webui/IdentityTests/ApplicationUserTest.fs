module Tests.ApplicationUser

open Xunit
open FsUnit.Xunit

module Tests =
    type ApplicationUserTest() =
        [<Fact>]
        member __.``ApplicationUser_FromSpec`` () = async {
            let firstname= "John"
            let lastname="Bolt"
            let email="J.Bolt@example.com"
            let lowerEmail="j.bolt@example.com"
            let primaryType = 1
            let ordinaryType = 2

            let uSpec:web.ApplicationUserSpec = {
                firstname= Some firstname
                lastname= Some lastname
                accountId= 1
                userTypeId= ordinaryType
                email= email
            }
            let appUser = web.ApplicationUser.FromSpec(uSpec)
            appUser.firstname |> should equal firstname
            appUser.lastname |> should equal lastname
            appUser.Email |> should equal email
            appUser.UserName |> should equal email
            appUser.NormalizedEmail |> should equal lowerEmail
            appUser.NormalizedUserName |> should equal lowerEmail
            appUser.isPrimary |> should equal false

            let uSpec:web.ApplicationUserSpec = {
                firstname= None
                lastname= Some lastname
                accountId= 1
                userTypeId= primaryType
                email= email
            }
            let appUser = web.ApplicationUser.FromSpec(uSpec)
            appUser.firstname |> should equal null
            appUser.lastname |> should equal lastname
            appUser.Email |> should equal email
            appUser.UserName |> should equal email
            appUser.NormalizedEmail |> should equal lowerEmail
            appUser.NormalizedUserName |> should equal lowerEmail
            appUser.isPrimary |> should equal true

            let uSpec:web.ApplicationUserSpec = {
                firstname= None
                lastname= None
                accountId= 1
                userTypeId= ordinaryType
                email= email
            }
            let appUser = web.ApplicationUser.FromSpec(uSpec)
            appUser.firstname |> should equal null
            appUser.lastname |> should equal null
            appUser.Email |> should equal email
            appUser.UserName |> should equal email
            appUser.NormalizedEmail |> should equal lowerEmail
            appUser.NormalizedUserName |> should equal lowerEmail
            appUser.isPrimary |> should equal false
        }
