module Tests.PasswordStore

open System
open Xunit
open FsUnit.Xunit
open System.Threading.Tasks;
// open Microsoft.AspNetCore.Builder.Internal;
// open Microsoft.AspNetCore.Identity.Test;
// open Microsoft.AspNetCore.Testing.xunit;
// open Microsoft.EntityFrameworkCore;
open FSharp.Control.Tasks
open Microsoft.Extensions.Configuration;
open Microsoft.Extensions.DependencyInjection;
open Microsoft.AspNetCore.Authentication
// for IUserStore
open Microsoft.AspNetCore.Identity
// for services.AddIdentityCore
open Microsoft.AspNetCore.Builder.Extensions
// for ApplicationBuilder
open Microsoft.AspNetCore.Builder
open FSharpPlus
open MyOwnDB.Tests.Helpers
open Microsoft.AspNetCore.Identity
open MyOwnDB.Tests.Utils
open web


module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type UserFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client [| "Account+Account,lib"; "User+User,lib" |] |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Tests =
    open Microsoft.Extensions.Logging
    let logger= LoggerFactory.Create(fun builder -> builder.AddConsole().AddDebug() |> ignore).CreateLogger()
    let services = new ServiceCollection()
    // required to avoid "Unable to resolve service for type 'Microsoft.AspNetCore.DataProtection.IDataProtectionProvider' while attempting to activate 'Microsoft.AspNetCore.Identity.DataProtectorTokenProvider`1"
    services.AddAuthentication()
    |> ignore
    services.AddSingleton<IUserStore<web.ApplicationUser>, web.UserStore>( fun s -> new web.UserStore(Client.client,logger))
            .AddSingleton<IUserPasswordStore<web.ApplicationUser>,web.UserStore>( fun s -> new web.UserStore(Client.client,logger))
            .AddIdentityCore<ApplicationUser>( fun options ->
                                                       options.User.RequireUniqueEmail <- true
                                                       options.SignIn.RequireConfirmedEmail <- true)
            // Add default token provider to avoid "No IUserTwoFactorTokenProvider<TUser> named 'Default' is registered."
            .AddDefaultTokenProviders()
            // need to add signInManager as AddIdentityCore doesn't do it itself
            .AddUserManager<UserManager<ApplicationUser>>()
    |> ignore
    let provider = services.BuildServiceProvider()
    let _builder = new ApplicationBuilder(provider);

    let biSequence (inp:Result<Async<'T>, Async<'Error>>) : Async<Result<'T,'Error>> =
            match inp with
            | Ok a -> a |> Async.map Ok
            | Error b ->  b |> Async.map Error

    let userByMailAsync (email:string) = (Email.fromString email)
                                            |> Result.map (fun email -> UserExtensions.byEmail email)
                                            |> Result.map (fun a -> DBAction.run Client.client a)
                                            |> Result.mapError (fun e -> async.Return e )
                                            |> bisequence
                                            |> Async.map Result.flatten


    type IPasswordStoreTest() =
        interface IClassFixture<Fixtures.UserFixture>
        [<Fact>]
        // Nothing is written to the database by these interface methods. Only the AppuUser instance is modified.
        member __.``PasswordStore_ro`` () = async {
            let userStore = _builder.ApplicationServices.GetRequiredService<IUserStore<web.ApplicationUser>>()
            let pwStore = _builder.ApplicationServices.GetRequiredService<IUserPasswordStore<web.ApplicationUser>>()

            let! byName = userStore.FindByNameAsync("BOB@TEST.COM", Async.DefaultCancellationToken) |> Async.AwaitTask

            // HasPasswordAsync
            let! hasPassword = pwStore.HasPasswordAsync(byName,Async.DefaultCancellationToken) |> Async.AwaitTask
            hasPassword |> should equal false
            // SetPasswordHashAsync
            pwStore.SetPasswordHashAsync(byName,"blablabla",Async.DefaultCancellationToken) |> Async.AwaitTask |> ignore
            // HasPasswordAsync
            let! hasPassword = pwStore.HasPasswordAsync(byName,Async.DefaultCancellationToken) |> Async.AwaitTask
            hasPassword |> should equal true
            // GetPasswordHashAsync
            let! retrievedHash = pwStore.GetPasswordHashAsync(byName,Async.DefaultCancellationToken) |> Async.AwaitTask
            retrievedHash |> should equal "blablabla"

        }
