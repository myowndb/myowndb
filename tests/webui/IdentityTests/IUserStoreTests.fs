module Tests.UserStore

open System
open Xunit
open FsUnit.Xunit
open System.Threading.Tasks;
// open Microsoft.AspNetCore.Builder.Internal;
// open Microsoft.AspNetCore.Identity.Test;
// open Microsoft.AspNetCore.Testing.xunit;
// open Microsoft.EntityFrameworkCore;
open FSharp.Control.Tasks
open Microsoft.Extensions.Configuration;
open Microsoft.Extensions.DependencyInjection;
open Microsoft.AspNetCore.Authentication
// for IUserStore
open Microsoft.AspNetCore.Identity
// for ApplicationBuilder
open Microsoft.AspNetCore.Builder
open FSharpPlus
open MyOwnDB.Tests.Helpers
open Microsoft.AspNetCore.Identity
open MyOwnDB.Tests.Utils


module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type UserFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client [| "Account+Account,lib"; "User+User,lib" |] |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Tests =
    open Microsoft.Extensions.Logging
    let logger= LoggerFactory.Create(fun builder -> builder.AddConsole().AddDebug() |> ignore).CreateLogger()
    let services = new ServiceCollection()
    services.AddSingleton<IUserStore<web.ApplicationUser>, web.UserStore>( fun s -> new web.UserStore(Client.client, logger))
    |> ignore
    let provider = services.BuildServiceProvider()
    let _builder = new ApplicationBuilder(provider);

    let biSequence (inp:Result<Async<'T>, Async<'Error>>) : Async<Result<'T,'Error>> =
            match inp with
            | Ok a -> a |> Async.map Ok
            | Error b ->  b |> Async.map Error

    let userByMailAsync (email:string) = (Email.fromString email)
                                            |> Result.map (fun email -> UserExtensions.byEmail email)
                                            |> Result.map (fun a -> DBAction.run Client.client a)
                                            |> Result.mapError (fun e -> async.Return e )
                                            |> bisequence
                                            |> Async.map Result.flatten


    type UserStoreTest() =
        interface IClassFixture<Fixtures.UserFixture>
        [<Fact>]
        member __.``UserStore_ro`` () = async {
            let! bob = userByMailAsync "bob@test.com"
            let userStore = _builder.ApplicationServices.GetRequiredService<IUserStore<web.ApplicationUser>>()

            // retrieving users
            let! byName = userStore.FindByNameAsync("BOB@TEST.COM",Async.DefaultCancellationToken) |> Async.AwaitTask
            byName.Email |> should equal "bob@test.com"
            byName.Id |> should equal 100001
            let! byId = userStore.FindByIdAsync("100001",Async.DefaultCancellationToken) |> Async.AwaitTask
            byId.Email |> should equal "bob@test.com"

            // User id
            let! id = userStore.GetUserIdAsync(byName , Async.DefaultCancellationToken)|> Async.AwaitTask
            id |> should equal "100001"

            // retrieving username and normalized username
            let! userName = userStore.GetUserNameAsync(byId , Async.DefaultCancellationToken)|> Async.AwaitTask
            userName |> should equal "bob@test.com"
            let! normalizedUserName = userStore.GetNormalizedUserNameAsync(byId , Async.DefaultCancellationToken)|> Async.AwaitTask
            normalizedUserName |> should equal "BOB@TEST.COM"

            // When the username is changed, the normalized user name is also updated
            let! () = userStore.SetUserNameAsync(byId , "Rebob@test.com" ,Async.DefaultCancellationToken)|> Async.AwaitTask
            let! newName = userStore.GetUserNameAsync(byId , Async.DefaultCancellationToken)|> Async.AwaitTask
            newName |> should equal "Rebob@test.com"
            let! newNormalizedName = userStore.GetNormalizedUserNameAsync(byId , Async.DefaultCancellationToken)|> Async.AwaitTask
            newNormalizedName |> should equal "REBOB@TEST.COM"
            let! newName = userStore.GetUserNameAsync(byId , Async.DefaultCancellationToken)|> Async.AwaitTask
            newName |> should equal "Rebob@test.com"
        }

        [<Fact>]
        member __.``UserStore_rw`` () = async {
            let firstname= "John"
            let lastname="Bolt"
            let email="J.Bolt@example.com"
            let lowerEmail="j.bolt@example.com"
            let userStore = _builder.ApplicationServices.GetRequiredService<IUserStore<web.ApplicationUser>>()

            // Setup SavePoint and rollback for this function
            let!_rollbackDisposable = setSavePoint Client.client



            let! preState, checkDifferenceNow =
                DBState.startWithOnlyCustomQueries Client.client [ 1L, "select count(*) from users"]


            let uSpec:web.ApplicationUserSpec = {
                firstname= Some firstname
                lastname= Some lastname
                accountId= 1
                userTypeId= 2
                email= email
            }
            let appUser = web.ApplicationUser.FromSpec(uSpec)
            let! result = userStore.CreateAsync(appUser, Async.DefaultCancellationToken) |> Async.AwaitTask
            if not result.Succeeded then
                failwithf "succeeded: %b errors: %A" result.Succeeded (result.Errors|> Seq.map (fun e-> e.Description))
            result.Succeeded |> should equal true
            do! checkDifferenceNow ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
