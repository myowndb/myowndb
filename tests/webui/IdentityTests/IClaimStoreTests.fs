module Tests.ClaimStore

open System
open Xunit
open FsUnit.Xunit
open System.Threading.Tasks;
// open Microsoft.AspNetCore.Builder.Internal;
// open Microsoft.AspNetCore.Identity.Test;
// open Microsoft.AspNetCore.Testing.xunit;
// open Microsoft.EntityFrameworkCore;
open FSharp.Control.Tasks
open Microsoft.Extensions.Configuration;
open Microsoft.Extensions.DependencyInjection;
open Microsoft.AspNetCore.Authentication
// for IUserStore
open Microsoft.AspNetCore.Identity
// for services.AddIdentityCore
open Microsoft.AspNetCore.Builder.Extensions
// for ApplicationBuilder
open Microsoft.AspNetCore.Builder
open FSharpPlus
open System.Security.Claims
open MyOwnDB.Tests.Helpers
open Microsoft.AspNetCore.Identity
open MyOwnDB.Tests.Utils
open web


module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type UserFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client [| "Account+Account,lib"; "User+User,lib"; "Database+Database,lib" |] |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Tests =
    open Microsoft.Extensions.Logging
    let logger= LoggerFactory.Create(fun builder -> builder.AddConsole().AddDebug() |> ignore).CreateLogger()
    let services = new ServiceCollection()
    // required to avoid "Unable to resolve service for type 'Microsoft.AspNetCore.DataProtection.IDataProtectionProvider' while attempting to activate 'Microsoft.AspNetCore.Identity.DataProtectorTokenProvider`1"
    services.AddAuthentication()
    |> ignore
    services.AddSingleton<IUserStore<web.ApplicationUser>, web.UserStore>( fun s -> new web.UserStore(Client.client,logger))
            .AddSingleton<IUserClaimStore<web.ApplicationUser>,web.UserStore>( fun s -> new web.UserStore(Client.client,logger))
            .AddIdentityCore<ApplicationUser>( fun options ->
                                                       options.User.RequireUniqueEmail <- true
                                                       options.SignIn.RequireConfirmedEmail <- true)
            // Add default token provider to avoid "No IUserTwoFactorTokenProvider<TUser> named 'Default' is registered."
            .AddDefaultTokenProviders()
            // need to add signInManager as AddIdentityCore doesn't do it itself
            .AddUserManager<UserManager<ApplicationUser>>()
    |> ignore
    let provider = services.BuildServiceProvider()
    let _builder = new ApplicationBuilder(provider);

    let biSequence (inp:Result<Async<'T>, Async<'Error>>) : Async<Result<'T,'Error>> =
            match inp with
            | Ok a -> a |> Async.map Ok
            | Error b ->  b |> Async.map Error

    let userByMailAsync (email:string) = (Email.fromString email)
                                            |> Result.map (fun email -> UserExtensions.byEmail email)
                                            |> Result.map (fun a -> DBAction.run Client.client a)
                                            |> Result.mapError (fun e -> async.Return e )
                                            |> bisequence
                                            |> Async.map Result.flatten

    let claimString (claim:Claim) = claim.Type+claim.Value
    let toComparableClaimsList (l:Claim list) = l |> List.map claimString |> List.sort

    type IClaimStoreTest() =
        interface IClassFixture<Fixtures.UserFixture>
        [<Fact>]
        // Nothing is written to the database by these interface methods. Only the AppuUser instance is modified.
        member __.``claims_ro`` () = async {
            let userStore = _builder.ApplicationServices.GetRequiredService<IUserStore<web.ApplicationUser>>()
            let claimStore = _builder.ApplicationServices.GetRequiredService<IUserClaimStore<web.ApplicationUser>>()

            let! byName = userStore.FindByNameAsync("BOB@TEST.COM", Async.DefaultCancellationToken) |> Async.AwaitTask

            // GetClaimsAsync
            let! claimsUnsorted = claimStore.GetClaimsAsync(byName,Async.DefaultCancellationToken) |> Async.AwaitTask
            // we build lists, as comparison with "should" fails with a seq
            let claims = claimsUnsorted |> Seq.toList |> toComparableClaimsList
            let expected =  [ Claim("AccountId","1"); Claim("UserType", "normal") ]
                            |> toComparableClaimsList
            claims |> should equal expected

        }


        [<Fact>]
        member __.``UserClaimValue`` () = async {
            let tryFirstClaimValue (claimType:string)(claims:System.Collections.Generic.IList<Claim>) =
                claims
                |> Seq.filter(fun c -> c.Type=claimType)
                |> Seq.tryHead
                |> Option.map (fun c -> c.Value)
            let userStore = _builder.ApplicationServices.GetRequiredService<IUserStore<web.ApplicationUser>>()
            let claimStore = _builder.ApplicationServices.GetRequiredService<IUserClaimStore<web.ApplicationUser>>()

            // retrieving users
            let! byName = userStore.FindByNameAsync("BOB@TEST.COM",Async.DefaultCancellationToken) |> Async.AwaitTask
            byName.Email |> should equal "bob@test.com"
            byName.Id |> should equal 100001

            // Getting its claims
            let! claimsUnsorted = claimStore.GetClaimsAsync(byName,Async.DefaultCancellationToken) |> Async.AwaitTask

            // Check we can get the account id from claims
            // Here we can only test Claims that are loaded by our UserStore, not those set by aspnetcore like ClaimTypes.NameIdentifier
            tryFirstClaimValue "AccountId" claimsUnsorted
                |> should equal (Some "1")

        }