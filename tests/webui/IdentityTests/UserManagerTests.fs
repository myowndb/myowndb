module Tests.UserManager

open System
open Xunit
open FsUnit.Xunit
open System.Threading.Tasks;
// open Microsoft.AspNetCore.Builder.Internal;
// open Microsoft.AspNetCore.Identity.Test;
// open Microsoft.AspNetCore.Testing.xunit;
// open Microsoft.EntityFrameworkCore;
open FSharp.Control.Tasks
open Microsoft.Extensions.Configuration;
open Microsoft.Extensions.DependencyInjection;
open Microsoft.AspNetCore.Authentication
// for IUserStore
open Microsoft.AspNetCore.Identity
// for services.AddIdentityCore
open Microsoft.AspNetCore.Builder.Extensions
// for ApplicationBuilder
open Microsoft.AspNetCore.Builder
open FSharpPlus
open MyOwnDB.Tests.Helpers
open Microsoft.AspNetCore.Identity
open MyOwnDB.Tests.Utils
open web


module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type UserFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client [| "Account+Account,lib"; "User+User,lib" |] |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Tests =
    open Microsoft.Extensions.Logging
    let logger= LoggerFactory.Create(fun builder -> builder.AddConsole().AddDebug() |> ignore).CreateLogger()
    let services = new ServiceCollection()
    // required to avoid "Unable to resolve service for type 'Microsoft.AspNetCore.DataProtection.IDataProtectionProvider' while attempting to activate 'Microsoft.AspNetCore.Identity.DataProtectorTokenProvider`1"
    services.AddAuthentication()
    |> ignore
    services.AddSingleton<IUserStore<web.ApplicationUser>, web.UserStore>( fun s -> new web.UserStore(Client.client,logger))
            .AddIdentityCore<ApplicationUser>( fun options ->
                                                       options.User.RequireUniqueEmail <- true
                                                       options.SignIn.RequireConfirmedEmail <- true)
            // Add default token provider to avoid "No IUserTwoFactorTokenProvider<TUser> named 'Default' is registered."
            .AddDefaultTokenProviders()
            // need to add signInManager as AddIdentityCore doesn't do it itself
            .AddUserManager<UserManager<ApplicationUser>>()
    |> ignore
    let provider = services.BuildServiceProvider()
    let _builder = new ApplicationBuilder(provider);

    let biSequence (inp:Result<Async<'T>, Async<'Error>>) : Async<Result<'T,'Error>> =
            match inp with
            | Ok a -> a |> Async.map Ok
            | Error b ->  b |> Async.map Error

    let userByMailAsync (email:string) = (Email.fromString email)
                                            |> Result.map (fun email -> UserExtensions.byEmail email)
                                            |> Result.map (fun a -> DBAction.run Client.client a)
                                            |> Result.mapError (fun e -> async.Return e )
                                            |> bisequence
                                            |> Async.map Result.flatten


    type IUserManagerTest() =
        interface IClassFixture<Fixtures.UserFixture>
        [<Fact>]
        member __.``usermanager_ro`` () = async {
            let! bob = userByMailAsync "bob@test.com"
            let userManager = _builder.ApplicationServices.GetRequiredService<UserManager<web.ApplicationUser>>()


            // FindByEmail
            let! byName = userManager.FindByEmailAsync("bob@test.com") |> Async.AwaitTask
            byName.Email |> should equal "bob@test.com"
            byName.Id |> should equal 100001

            // FindById
            let! byId = userManager.FindByIdAsync("100001") |> Async.AwaitTask
            byId.Email |> should equal "bob@test.com"
            byId.Id |> should equal 100001

            // HasPasswordAsync
            let! hasPassword = userManager.HasPasswordAsync(byName) |> Async.AwaitTask
            hasPassword |> should equal false

            // IsEmailconfirmedAsync
            let! emailConfirmed = userManager.IsEmailConfirmedAsync(byName) |> Async.AwaitTask
            emailConfirmed |> should equal true

            // ResetPasswordAsync rejects invalid password
            let invalidPass = "1234"
            let! token1 = userManager.GeneratePasswordResetTokenAsync(byName) |> Async.AwaitTask
            let! result = userManager.ResetPasswordAsync(byName,token1,invalidPass) |> Async.AwaitTask
            result.Succeeded |> should equal false
            (result.Errors |> Seq.map (fun e -> e.Code)) |> Seq.toArray
            |> should equal [|"PasswordTooShort";"PasswordRequiresNonAlphanumeric";"PasswordRequiresLower";"PasswordRequiresUpper" |]

        }

        [<Fact>]
        member __.``usermanager_rw`` () = async {

            // Setup SavePoint and rollback for this function
            let!_rollbackDisposable = setSavePoint Client.client


            let! bob = userByMailAsync "bob@test.com"
            let userManager = _builder.ApplicationServices.GetRequiredService<UserManager<web.ApplicationUser>>()


            let! byName = userManager.FindByEmailAsync("bob@test.com") |> Async.AwaitTask
            // Ensure we work on the expected user
            byName.Email |> should equal "bob@test.com"
            byName.Id |> should equal 100001
            let! hasPassword = userManager.HasPasswordAsync(byName) |> Async.AwaitTask
            hasPassword |> should equal false

            // ResetPasswordAsync
            let! token1 = userManager.GeneratePasswordResetTokenAsync(byName) |> Async.AwaitTask
            let! result = userManager.ResetPasswordAsync(byName,token1,"My§uperPass01234") |> Async.AwaitTask
            result.Succeeded |> should equal true
            let! hasPassword = userManager.HasPasswordAsync(byName) |> Async.AwaitTask
            hasPassword |> should equal true

            // Email validation
            let! unverifiedUser = userManager.FindByEmailAsync("fbasic1@example.com") |> Async.AwaitTask
            unverifiedUser.Email |> should equal "fbasic1@example.com"
            unverifiedUser.Id |> should equal 100007
            let! emailConfirmed = userManager.IsEmailConfirmedAsync(unverifiedUser) |> Async.AwaitTask
            emailConfirmed |> should equal false
            // fails with invalid token
            let! resultInvalid = userManager.ConfirmEmailAsync(unverifiedUser,"invalidToken") |> Async.AwaitTask
            resultInvalid.Succeeded |> should equal false
            let codes = resultInvalid.Errors |> Seq.map (fun e -> e.Code) |> Seq.toList
            codes |> should equal ["InvalidToken"]
            let! emailConfirmed = userManager.IsEmailConfirmedAsync(unverifiedUser) |> Async.AwaitTask
            emailConfirmed |> should equal false
            // succeeds with valid token
            let! token = userManager.GenerateEmailConfirmationTokenAsync(unverifiedUser)|> Async.AwaitTask
            let! resultValid = userManager.ConfirmEmailAsync(unverifiedUser, token) |> Async.AwaitTask
            resultValid.Succeeded |> should equal true
            let! emailConfirmed = userManager.IsEmailConfirmedAsync(unverifiedUser) |> Async.AwaitTask
            emailConfirmed |> should equal true
            // retrieve user from db to check it was updated
            let! newlyVerifiedUser = userManager.FindByEmailAsync("fbasic1@example.com") |> Async.AwaitTask
            newlyVerifiedUser.Email |> should equal "fbasic1@example.com"
            newlyVerifiedUser.Id |> should equal 100007
            let! emailConfirmed = userManager.IsEmailConfirmedAsync(newlyVerifiedUser) |> Async.AwaitTask
            emailConfirmed |> should equal true
            // DeleteAsync
            let! preState, checkDifferenceNow =
                DBState.startWithOnlyCustomQueries Client.client [ -1L, "select count(*) from users"
                                                                   -1L, "select count(*) from users where id=100001" ]
            let! delResult = userManager.DeleteAsync(byName) |> Async.AwaitTask
            if not delResult.Succeeded then
                printfn "number of errors: %d" (delResult.Errors |> Seq.length)
                failwithf "deletion unexpectedly failed: %s" (delResult.Errors |> Seq.fold (fun acc e -> acc + e.Description) ""  )
            delResult.Succeeded |> should equal true
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
