module Tests.SignInManager

open System
open Xunit
open FsUnit.Xunit
open System.Threading.Tasks;
// open Microsoft.AspNetCore.Builder.Internal;
// open Microsoft.AspNetCore.Identity.Test;
// open Microsoft.AspNetCore.Testing.xunit;
// open Microsoft.EntityFrameworkCore;
open FSharp.Control.Tasks
open Microsoft.Extensions.Configuration;
open Microsoft.Extensions.DependencyInjection;
open Microsoft.AspNetCore.Authentication
// for IUserStore
open Microsoft.AspNetCore.Identity
// for services.AddIdentityCore
open Microsoft.AspNetCore.Builder.Extensions
// for ApplicationBuilder
open Microsoft.AspNetCore.Builder
open FSharpPlus
open MyOwnDB.Tests.Helpers
open Microsoft.AspNetCore.Identity
open MyOwnDB.Tests.Utils
open web


module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type UserFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client [| "Account+Account,lib"; "User+User,lib" |] |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Tests =
    open Microsoft.Extensions.Logging
    let logger= LoggerFactory.Create(fun builder -> builder.AddConsole().AddDebug() |> ignore).CreateLogger()
    let services = new ServiceCollection()
    // required to avoid "Unable to resolve service for type 'Microsoft.AspNetCore.DataProtection.IDataProtectionProvider' while attempting to activate 'Microsoft.AspNetCore.Identity.DataProtectorTokenProvider`1"
    services.AddAuthentication()
    |> ignore
    services.AddSingleton<IUserStore<web.ApplicationUser>, web.UserStore>( fun s -> new web.UserStore(Client.client,logger))
            .AddIdentityCore<ApplicationUser>( fun options ->
                                                       options.User.RequireUniqueEmail <- true
                                                       options.SignIn.RequireConfirmedEmail <- true)
            // Add default token provider to avoid "No IUserTwoFactorTokenProvider<TUser> named 'Default' is registered."
            .AddDefaultTokenProviders()
            // need to add signInManager as AddIdentityCore doesn't do it itself
            .AddSignInManager<SignInManager<ApplicationUser>>()
    |> ignore
    let provider = services.BuildServiceProvider()
    let _builder = new ApplicationBuilder(provider);

    let biSequence (inp:Result<Async<'T>, Async<'Error>>) : Async<Result<'T,'Error>> =
            match inp with
            | Ok a -> a |> Async.map Ok
            | Error b ->  b |> Async.map Error

    let userByMailAsync (email:string) = (Email.fromString email)
                                            |> Result.map (fun email -> UserExtensions.byEmail email)
                                            |> Result.map (fun a -> DBAction.run Client.client a)
                                            |> Result.mapError (fun e -> async.Return e )
                                            |> bisequence
                                            |> Async.map Result.flatten


    type ISignInManagerTest() =
        interface IClassFixture<Fixtures.UserFixture>
        [<Fact>]
        member __.``signinmanager_ro`` () = async {
            let signInManager = _builder.ApplicationServices.GetRequiredService<SignInManager<web.ApplicationUser>>()
            let userManager = _builder.ApplicationServices.GetRequiredService<UserManager<web.ApplicationUser>>()

            // Retrieve users
            let! byName = userManager.FindByEmailAsync("bob@test.com") |> Async.AwaitTask
            byName.Email |> should equal "bob@test.com"
            byName.Id |> should equal 100001

            // with password but unverified email
            let! byId = userManager.FindByIdAsync("100007") |> Async.AwaitTask
            byId.Email |> should equal "fbasic1@example.com"
            byId.Id |> should equal 100007

            let longbobEmail = "longbob@test.com"
            let password = "My§uperPass01234"
            let! longbob = userManager.FindByEmailAsync(longbobEmail) |> Async.AwaitTask
            longbob.Email |> should equal longbobEmail
            longbob.Id |> should equal 100003


            let! canSignIn = signInManager.CanSignInAsync(longbob) |> Async.AwaitTask
            canSignIn |> should equal true
            // cant sign in even without password set (for external auth methods?)
            let! canSignIn = signInManager.CanSignInAsync(byName) |> Async.AwaitTask
            canSignIn |> should equal true
            // cannot sign in without verified email
            let! canSignIn = signInManager.CanSignInAsync(byId) |> Async.AwaitTask
            canSignIn |> should equal false
            let! result = signInManager.PasswordSignInAsync(longbobEmail, "incorrect password", true, false) |> Async.AwaitTask
            result.Succeeded |> should equal false
            // Cannot immediately test PasswordSignInAsync as it requires mocking an HTTPContext
            //let! result = signInManager.PasswordSignInAsync(longbobEmail, password, true, false) |> Async.AwaitTask
            //result.Succeeded |> should equal true
        }