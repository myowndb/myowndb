module Tests.EmailStore

open System
open Xunit
open FsUnit.Xunit
open System.Threading.Tasks;
// open Microsoft.AspNetCore.Builder.Internal;
// open Microsoft.AspNetCore.Identity.Test;
// open Microsoft.AspNetCore.Testing.xunit;
// open Microsoft.EntityFrameworkCore;
open FSharp.Control.Tasks
open Microsoft.Extensions.Configuration;
open Microsoft.Extensions.DependencyInjection;
open Microsoft.AspNetCore.Authentication
// for IUserStore
open Microsoft.AspNetCore.Identity
// for services.AddIdentityCore
open Microsoft.AspNetCore.Builder.Extensions
// for ApplicationBuilder
open Microsoft.AspNetCore.Builder
open FSharpPlus
open MyOwnDB.Tests.Helpers
open Microsoft.AspNetCore.Identity
open MyOwnDB.Tests.Utils
open web


module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.test.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type UserFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client [| "Account+Account,lib"; "User+User,lib" |] |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Tests =
    open Microsoft.Extensions.Logging
    let logger= LoggerFactory.Create(fun builder -> builder.AddConsole().AddDebug() |> ignore).CreateLogger()
    let services = new ServiceCollection()
    // required to avoid "Unable to resolve service for type 'Microsoft.AspNetCore.DataProtection.IDataProtectionProvider' while attempting to activate 'Microsoft.AspNetCore.Identity.DataProtectorTokenProvider`1"
    services.AddAuthentication()
    |> ignore
    services.AddSingleton<IUserStore<web.ApplicationUser>, web.UserStore>( fun s -> new web.UserStore(Client.client,logger))
            .AddSingleton<IUserEmailStore<web.ApplicationUser>,web.UserStore>( fun s -> new web.UserStore(Client.client,logger))
            .AddIdentityCore<ApplicationUser>( fun options ->
                                                       options.User.RequireUniqueEmail <- true
                                                       options.SignIn.RequireConfirmedEmail <- true)
            // Add default token provider to avoid "No IUserTwoFactorTokenProvider<TUser> named 'Default' is registered."
            .AddDefaultTokenProviders()
            // need to add signInManager as AddIdentityCore doesn't do it itself
            .AddUserManager<UserManager<ApplicationUser>>()
    |> ignore
    let provider = services.BuildServiceProvider()
    let _builder = new ApplicationBuilder(provider);

    let biSequence (inp:Result<Async<'T>, Async<'Error>>) : Async<Result<'T,'Error>> =
            match inp with
            | Ok a -> a |> Async.map Ok
            | Error b ->  b |> Async.map Error

    let userByMailAsync (email:string) = (Email.fromString email)
                                            |> Result.map (fun email -> UserExtensions.byEmail email)
                                            |> Result.map (fun a -> DBAction.run Client.client a)
                                            |> Result.mapError (fun e -> async.Return e )
                                            |> bisequence
                                            |> Async.map Result.flatten


    type IEmailStoreTest() =
        interface IClassFixture<Fixtures.UserFixture>
        [<Fact>]
        // Nothing is written to the database by these interface methods. Only the AppuUser instance is modified.
        member __.``EmailStore_ro`` () = async {
            let userStore = _builder.ApplicationServices.GetRequiredService<IUserStore<web.ApplicationUser>>()
            let emailStore = _builder.ApplicationServices.GetRequiredService<IUserEmailStore<web.ApplicationUser>>()

            // get user through user store
            let! bob = userStore.FindByNameAsync("BOB@TEST.COM", Async.DefaultCancellationToken) |> Async.AwaitTask
            // GetEmailAsync
            let! email = emailStore.GetEmailAsync(bob,Async.DefaultCancellationToken) |> Async.AwaitTask
            email |> should equal "bob@test.com"
            // GetNormalizedEmailAsync
            let! normalizedEmail = emailStore.GetNormalizedEmailAsync(bob,Async.DefaultCancellationToken) |> Async.AwaitTask
            normalizedEmail |> should equal "BOB@TEST.COM"
            // GetEmailConfirmedAsync
            let! confirmed = emailStore.GetEmailConfirmedAsync(bob,Async.DefaultCancellationToken) |> Async.AwaitTask
            confirmed |> should equal true

            // get user through email store
            let! bob2 = emailStore.FindByNameAsync("BOB@TEST.COM", Async.DefaultCancellationToken) |> Async.AwaitTask
            // GetEmailAsync
            let! email = emailStore.GetEmailAsync(bob2,Async.DefaultCancellationToken) |> Async.AwaitTask
            email |> should equal "bob@test.com"
            // GetNormalizedEmailAsync
            let! normalizedEmail = emailStore.GetNormalizedEmailAsync(bob2,Async.DefaultCancellationToken) |> Async.AwaitTask
            normalizedEmail |> should equal "BOB@TEST.COM"
            // GetEmailConfirmedAsync
            let! confirmed = emailStore.GetEmailConfirmedAsync(bob2,Async.DefaultCancellationToken) |> Async.AwaitTask
            confirmed |> should equal true


            // User from spec has email not verified
            let firstname= "John"
            let lastname="Bolt"
            let email="J.Bolt@example.com"
            let uSpec:web.ApplicationUserSpec = {
                firstname= Some firstname
                lastname= Some lastname
                accountId= 1
                userTypeId= 2
                email= email
            }
            let userFromSpec = web.ApplicationUser.FromSpec(uSpec)
            let! unconfirmedFromSpec = emailStore.GetEmailConfirmedAsync(userFromSpec,Async.DefaultCancellationToken) |> Async.AwaitTask
            unconfirmedFromSpec |> should equal false
            // SetEmailConfirmedAsync
            emailStore.SetEmailConfirmedAsync(userFromSpec,true, Async.DefaultCancellationToken)|> Async.AwaitTask |> ignore
            // check ApplicationUser instance is updated
            userFromSpec.EmailConfirmed |> should equal true
            // and retrieving its value via the interface method works as expected
            let! confirmedFromSpec = emailStore.GetEmailConfirmedAsync(userFromSpec,Async.DefaultCancellationToken) |> Async.AwaitTask
            confirmedFromSpec |> should equal true

            // SetEmailAsync
            emailStore.SetEmailAsync(userFromSpec, "John.Bolt@new.example.com", Async.DefaultCancellationToken) |> Async.AwaitTask |> ignore
            // check the ApplicationUser is updated
            userFromSpec.Email |> should equal "John.Bolt@new.example.com"
            // also check that retrieving via the interface method gives the expected result
            let! newEmail = emailStore.GetEmailAsync(userFromSpec,Async.DefaultCancellationToken) |> Async.AwaitTask
            newEmail |> should equal "John.Bolt@new.example.com"
            // and also check that the normalized value retrieved is also updated
            let! newNormalizedEmail = emailStore.GetNormalizedEmailAsync(userFromSpec,Async.DefaultCancellationToken) |> Async.AwaitTask
            newNormalizedEmail |> should equal "JOHN.BOLT@NEW.EXAMPLE.COM"

            // SetNormalizedEmailAsync
            emailStore.SetNormalizedEmailAsync(userFromSpec,"JOHN.BOLT@NEW.EXAMPLE.COM_NORM",Async.DefaultCancellationToken) |> Async.AwaitTask |> ignore
            let! updatedNormalizedEmail = emailStore.GetNormalizedEmailAsync(userFromSpec,Async.DefaultCancellationToken) |> Async.AwaitTask
            updatedNormalizedEmail |> should equal "JOHN.BOLT@NEW.EXAMPLE.COM_NORM"
            // check the email field was not changed
            let! normalEmail = emailStore.GetEmailAsync(userFromSpec,Async.DefaultCancellationToken) |> Async.AwaitTask
            normalEmail |> should equal  "John.Bolt@new.example.com"
        }
