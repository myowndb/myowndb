module libtests

open NUnit.Framework
open FsUnit

[<Parallelizable>]
type Pagination() =
    [<Test>]
    member self.``pagination_links`` () =
        // test displaying 3 links before and 3 after, if possible
        web.Datatable.paginatedPagesBoundaries 6 1 10
        |> should equal  (1,7)
        web.Datatable.paginatedPagesBoundaries 6 2 10
        |> should equal  (1,7)
        web.Datatable.paginatedPagesBoundaries 6 3 10
        |> should equal  (1,7)
        // on page 4 we are in the middle, next will change the links
        web.Datatable.paginatedPagesBoundaries 6 4 10
        |> should equal  (1,7)
        web.Datatable.paginatedPagesBoundaries 6 5 10
        |> should equal  (2,8)
        web.Datatable.paginatedPagesBoundaries 6 6 10
        |> should equal  (3,9)
        // here we get on the last 3 links possible, next will not change links
        web.Datatable.paginatedPagesBoundaries 6 7 10
        |> should equal  (4,10)
        web.Datatable.paginatedPagesBoundaries 6 8 10
        |> should equal  (4,10)
        web.Datatable.paginatedPagesBoundaries 6 9 10
        |> should equal  (4,10)
        web.Datatable.paginatedPagesBoundaries 6 10 10
        |> should equal  (4,10)
        // edge cases
        // no 6 links possible
        web.Datatable.paginatedPagesBoundaries 6 1 1
        |> should equal  (1,1)
        web.Datatable.paginatedPagesBoundaries 6 3 3
        |> should equal  (1,3)
        // no page, second page number is 0
        web.Datatable.paginatedPagesBoundaries 6 0 0
        |> should equal  (1,0)

    [<Test>]
    member self.``number_of_pages`` () =
        web.Datatable.computeNumberOfPages 29L 10 |> should equal 3
        web.Datatable.computeNumberOfPages 30L 10 |> should equal 3
        web.Datatable.computeNumberOfPages 31L 10 |> should equal 4
        web.Datatable.computeNumberOfPages 31L 7 |> should equal 5
        web.Datatable.computeNumberOfPages 30L 1 |> should equal 30
        web.Datatable.computeNumberOfPages 3L 10 |> should equal 1
        web.Datatable.computeNumberOfPages 0L 10 |> should equal 0