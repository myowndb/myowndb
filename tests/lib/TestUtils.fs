module MyOwnDB.Tests.Utils
// Utilities functions used in tests but unrelated to XUnit

open System.Data
open System.IO

let getScalarOption (client: DB.DBClient) query =
    let dt: DataTable =
        DBAction.staticDataTableAction query
        |> DBAction.run client
        |> Async.RunSynchronously
        |> DBResult.get
        |> List.head

    if dt.Rows.Count > 0 then Some(dt.Rows.[0].[0]) else None

let getScalar (client: DB.DBClient) query =
    match getScalarOption client query with
    | Some v ->
        if ((unbox v).GetType()) = typeof<System.DBNull>
        then failwith "scalar request returned NULL result"
        else (unbox v)
    | None -> failwith "no rows returned for scalar request. Check if you want to use getScalarDefaultZero"

let getScalarDefaultZero (client: DB.DBClient) query =
    match getScalarOption client query with
    | Some v ->
        if ((unbox v).GetType()) = typeof<System.DBNull>
        then 0
        else (unbox v)
    | None -> 0

let expectedJSON path =
    Utils.reformatJSON (File.ReadAllText(path))

// function to be called at the start of a test like
//     _rollbackDisposable = setSavePoint Client.client
// It sets a savepoint and returns a disposable that will roll back
// to that savepoint. As it gets out of scope at the end of the test,
// this ensures that the next test runs on a clean database.
let setSavePoint (client:DB.DBClient) = async {
    let savePoint = "SAVEPOINT_StartOfTest"
    let! v = client.TrySetSavePoint savePoint
    v
    |> function
       | Ok [savePoint] -> ()
       | _ -> failwith "savepoint not set"


    let backToSavepoint () = async {
        let! v=client.TryToSavePoint savePoint
        return v
            |> function
               | Ok [()] -> ()
               | _ -> failwith "return to savepoint failed"
    }

    return Defer.deferAsync backToSavepoint
}
