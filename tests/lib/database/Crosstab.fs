namespace MyOwnDB.Tests.Crosstab

open System
open Xunit
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus
// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
                [| "Account+Account,lib"
                   "Database+Database,lib"
                   "Entity+Entity,lib"
                   "Detail+Detail,lib"
                   "Entities2details+Entities2details,lib"
                   "Instance+Instance,lib"
                   "DetailValue+DetailValue,lib"
                   "IntegerDetailValue+IntegerDetailValue,lib"
                   "DateDetailValue+DateDetailValue,lib"
                   "DetailValueProposition+DetailValueProposition,lib"
                   "DdlDetailValue+DdlDetailValue,lib" |]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client fixtures |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module Crosstab =
    // to issue DBAction sql queries to database
    let issuer<'T> action = DBAction.run<'T> Client.client action

    [<Trait("Category", "database")>]
    [<Trait("Category", "crosstab")>]
    type CrosstabTests() =

        [<Fact>]
        member __.``Retrieving entity details``() = async {
            let! infoResult = Crosstab.getCrosstabInfoAction (newEntityId 12) |> issuer

            // check entity details
            infoResult
                |> DBResult.map (fun info -> info.entityDetails) // extract details list to work on
                |> DBResult.map (List.map (fun d -> d.name)) // get details names
                |> checkOkResult [ [ "nom"; "prenom"; "fonction"; "service"; "coordonees_specifiques"; "company_email" ] ]

            // check entity details for list view
            infoResult
                |> DBResult.map (fun info -> info.entityDetailsInList) // extract details list to work on
                |> DBResult.map (List.map (fun d -> d.name)) // get details names
                |> checkOkResult [ [ "nom"; "prenom"; "fonction"; "service"; "company_email" ] ]

            //---------------------
            // select all instances
            let allForEntity12 = Crosstab.getCrosstabInfoAction (newEntityId 12)

            // check json
            do! allForEntity12
                |> DBAction.map (fun (info: Crosstab.Info) -> sprintf "select * from %s" info.crosstabListQueryBody)
                |> DBAction.bind DBAction.staticDataTableAction
                |> DBAction.mapDataTableToJSON
                |> issuer
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_12.json"])


            //------------------------------
            // filter on instance ids passed
            let filteredInstances = Crosstab.getFilteredCrosstabInfoAction (newEntityId 12) (Some([ 74; 82; 86 ] |> List.map newInstanceId))
            // check rows
            do! filteredInstances
                |> DBAction.map (fun (info: Crosstab.Info) -> sprintf "select * from %s" info.crosstabListQueryBody)
                |> DBAction.bind DBAction.staticDataTableAction
                |> DBAction.mapDataTableToJSON
                |> issuer
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_12_74_82_86.json"])

            //-------------------------------------
            // second filter on instance ids passed
            let filteredInstances2 = Crosstab.getFilteredCrosstabInfoAction (newEntityId 12) (Some ([ 70; 75; 81; 82; 83 ] |> List.map newInstanceId))
            // check rows
            do! filteredInstances2
                |> DBAction.map (fun (info: Crosstab.Info) -> sprintf "select * from %s" info.crosstabListQueryBody)
                |> DBAction.bind DBAction.staticDataTableAction
                |> DBAction.mapDataTableToJSON
                |> issuer
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_12_70_75_81_82_83.json"])

            //-------------------------------------
            // no filter on instance ids passed
            let filteredInstances = Crosstab.getFilteredCrosstabInfoAction (newEntityId 12) None
            // check rows
            do! filteredInstances
                |> DBAction.map (fun (info: Crosstab.Info) -> sprintf "select * from %s" info.crosstabListQueryBody)
                |> DBAction.bind DBAction.staticDataTableAction
                |> DBAction.mapDataTableToJSON
                |> issuer
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_12.json"])
            let filteredInstances = Crosstab.getFilteredCrosstabInfoAction (newEntityId 100000) None
            // check rows
            do! filteredInstances
                |> DBAction.map (fun (info: Crosstab.Info) -> sprintf "select * from %s" info.crosstabListQueryBody)
                |> DBAction.bind DBAction.staticDataTableAction
                |> DBAction.mapDataTableToJSON
                |> issuer
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_100000.json"])
        }

        interface IClassFixture<Fixtures.DatabaseFixture>
