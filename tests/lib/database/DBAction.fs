namespace MyOwnDB.Tests.DBAction

open Xunit
open MyOwnDB.Tests.Helpers
open System
open Ids
open DBAction
open DBAction.StaticArguments
open DBTypes
open FSharp.Control.Tasks
open FSharpPlus
open FsUnit.Xunit

module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
           [ "Account"
             "User"
             "Database"
             "Entity"
             "Instance"
             "Detail"
             "DateDetailValue"
             "IntegerDetailValue"
             "DetailValue"
             "DetailValueProposition"
             "DdlDetailValue"
             // not loading entities2details will results in an error
             // query-specified return tuple and crosstab function are not compatible
             "Entities2details"
             "Relation"
             "Link" ]
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }
module DBAction =
    // to issue DBAction sql queries to database
    let issuer<'T> action = DBAction.run<'T> Client.client action

    [<Trait("Category", "database")>]
    [<Trait("Category", "dbaction")>]
    type DBActionTests() =

        interface IClassFixture<Fixtures.DatabaseFixture>

        //************************************************************
        // First are the tests not using the database
        [<Fact>]
        member __.``dbaction_wrapList``() = async {
            do! wrapList [1;2;3]
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1;2;3])

            do! wrapList ["un";"dos";"tres"]
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult ["un";"dos";"tres"])
        }

        [<Fact>]
        member __.``dbaction_flatten``() = async {
            do! wrapList [[1];[2;3]]
                |> flatten
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1;2;3])

            do! wrapList [["un"];["dos"];["tres"]]
                |> flatten
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult ["un";"dos";"tres"])

            do! error "my error"
                |> flatten
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["my error"])
        }
        [<Fact>]
        member __.``dbaction_merge``() = async {

            // merge DBAction<int>
            do! merge (wrapList [1;2;3]) (wrapList [4;5;6])
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1;2;3;4;5;6])

            // merge DBAction<string>
            do! merge (wrapList ["un";"dos"]) (wrapList ["tres"])
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult ["un";"dos";"tres"])

            // merge DBAction<string> with empty list
            do! merge (wrapList ["un";"dos"]) (wrapList [])
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult ["un";"dos"])
            do! merge (wrapList []) (wrapList ["un";"dos"])
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult ["un";"dos"])

            // second DBAction is in error
            do! merge (wrapList ["un";"dos"]) (error "my error")
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["my error"])

            // two DBActions are in error
            do! merge (error "first error") (error "my error")
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult  ["first error";"my error"])

            // first merge create a DBAction with 2 errors,
            // which is then merge with an action in error
            do! merge (error "first error") (error "my error")
                |> merge (error "third error")
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult  ["third error";"first error";"my error"])
        }

        [<Fact>]
        member __.``dbaction_bind2``() = async {
            let a1 = wrapList [1;2;3]
            let a2 = wrapList [2;4]
            let op1 = (fun v1 v2 -> DBAction.retn (v1+v2))
            do! bind2 op1 a1 a2
                |> run Client.client
                |> Async.map (checkOkDBResult  [3;5;4;6;5;7])

            let b1 = wrapList ["un";"dos"]
            let b2 = wrapList ["tres"]
            let op2 = (fun v1 v2 -> DBAction.retn (v1+v2))

            do! bind2 op2 b1 b2
                |> run Client.client
                |> Async.map (checkOkDBResult  ["untres";"dostres"])

            let a1 = wrapList [1;2;3]
            let a2 = wrapList [2;4]
            let op1 = (fun v1 v2 -> DBAction.retn (v2-v1))
            do! bind2 op1 a1 a2
                |> run Client.client
                |> Async.map (checkOkDBResult  [1;3;0;2;-1;1])


            let a1 = wrapList [1;2;3]
            let a2 = DBAction.error "my error"
            let op1 = (fun v1 v2 -> DBAction.retn (v2-v1))
            do! bind2 op1 a1 a2
                |> run Client.client
                |> Async.map (checkErrorResult  ["my error"])

            let a1 = DBAction.error "first error"
            let a2 = DBAction.error "my error"
            let op1 = (fun v1 v2 -> DBAction.retn (v2-v1))
            do! bind2 op1 a1 a2
                |> run Client.client
                |> Async.map (checkErrorResult  ["first error";"my error"])
        }

        [<Fact>]
        member __.``dbaction_workOnList``() = async {
            do! wrapList [1;2;3]
                |> workOnList
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [[1;2;3]])

            do! wrapList [["un"];["dos"];["tres"]]
                |> workOnList
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [[["un"];["dos"];["tres"]]])
        }

        [<Fact>]
        member __.``dbaction_workOnItems``() = async {
            do! wrapList [[]]
                |> workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [])

            do! wrapList [[1;2;3]]
                |> workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1;2;3])

            do! wrapList [[1;2;3]; [4;5;6]]
                |> workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["wrong call to DBAction.workOnItems, [[1; 2; 3]; [4; 5; 6]] should have been list of one item being a list"])
        }

        [<Fact>]
        member __.``dbaction_workOnListAndBack``() = async {
            let jane = {| name="Doe"; surname="Jane"|}
            let john = {| name="Doe"; surname="John"|}
            let sam = {| name="Smith"; surname="Sam"|}
            let tom = {| name="Smith"; surname="Tom"|}
            let vale = {| name="Smith"; surname="Val"|}
            let ben = {| name="West"; surname="Ben"|}

            do! wrapList [
                          jane
                          john
                          sam
                          tom
                          vale
                          ben
                         ]
                |> workOnList
                |> DBAction.map (List.groupBy (fun i -> i.name ))
                |> DBAction.workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ ("Doe", [ jane;john] )
                                                ("Smith", [sam;tom;vale])
                                                ("West", [ben])
                 ])
        }


        [<Fact>]
        member __.``dbaction_fromResult``() = async {
            do! Ok 1
                |> DBAction.fromResult
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1])

            do! Ok "a string"
                |> DBAction.fromResult
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult ["a string"])

            do! Ok ([1;2;3])
                |> DBAction.fromResult
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [[1;2;3]])

            do! Ok (1,2)
                |> DBAction.fromResult
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1,2])

            do! Error "my error message"
                |> DBAction.fromResult
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["my error message"])

            let exc = new Exception ("my exception")
            do! Error exc
                |> DBAction.fromResult
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult [string exc])
        }

        [<Fact>]
        member __.``dbaction_fromOption``() = async {
            do! Some 1
                |> DBAction.fromOption
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1])
            do! None
                |> DBAction.fromOption
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Option was None"])
        }

        [<Fact>]
        member __.``dbaction_fromAsync``() = async {
            do! async { return 1 }
                |> DBAction.fromAsync
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1])

            do! async { return "a string" }
                |> DBAction.fromAsync
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult ["a string"])

            do! async { return [1;2;3] }
                |> DBAction.fromAsync
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [[1;2;3]])

            do! async { raise (System.Exception("my exception message"))}
                |> DBAction.fromAsync
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["my exception message"])

            let myFunctionAsync () = async {
                do! Async.Sleep 200
                return 42
            }
            do! DBAction.fromAsync (myFunctionAsync())
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [42])

            let myFailingFunctionAsync () = async {
                do! Async.Sleep 200
                raise (System.Exception("function failed"))
            }
            do! DBAction.fromAsync (myFailingFunctionAsync())
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["function failed"])
        }
        [<Fact>]
        member __.``dbaction_bindError``() = async {
            let a1 = wrapList [1;2;3]
            let a2 = DBAction.error "my error"
            // bind error on a success should have no impact
            do! a1
                |> DBAction.bindError (fun e -> DBAction.error "new error text")
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1;2;3])

            // bind error on an error, we can change the error
            do! a2
                |> DBAction.bindError (fun e -> DBAction.error "new error text")
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["new error text"])

            // bind error on an error, we can change it to a success
            do! a2
                |> DBAction.bindError (fun e -> DBAction.retn false )
                |> DBAction.run Client.client
                |> Async.map (checkOkResult [false])

            // check that similarly to mapping error to false in the previous test
            // we can map the success case to true
            do! a1
                // work on list or we end up with a list of bools
                |> DBAction.workOnList
                |> DBAction.map (fun _ -> true )
                |> DBAction.run Client.client
                |> Async.map (checkOkResult [true])

            // Check that combining both map and bindError works as expected
            do! a1
                |> DBAction.workOnList
                |> DBAction.map (fun _ -> true )
                |> DBAction.bindError (fun e -> DBAction.retn false )
                |> DBAction.run Client.client
                |> Async.map (checkOkResult [true])
            do! a2
                |> DBAction.workOnList
                |> DBAction.map (fun _ -> true )
                |> DBAction.bindError (fun e -> DBAction.retn false )
                |> DBAction.run Client.client
                |> Async.map (checkOkResult [false])

            // Check that if bindError results in a DBAction, it is executed as expected
            // Define a mutable variable, of which a change in value is a witness it works
            // fine
            let mutable witness = 0
            do! a2
                |> DBAction.bindError (fun e ->
                  // This is a dummy DBAction, so that we can call map on it.
                  // In reality this should be a db altering action
                  DBAction.retn "dummy"
                  // Call map on the DBAction, and set the witness to 1 so we
                  // can validate it ran succesfully
                  |> DBAction.map (fun s ->
                    witness <- 1
                    s
                  )
                  // We don't want to modify the error return so we bind and wrap the
                  // original error.
                  // So the bindError returns an DBAction that makes a modification but
                  // still returns the original error.
                  |> DBAction.bind (fun _v ->
                    DBAction.wrapError e
                  )
                )
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["my error"])
            // Check the DBAction returned by bindError is executed as expected
            witness |> should equal 1
        }


        //************************************************************
        // Below are tests using using the database
        [<Fact>]
        member __.``queryAction_tests``() = async {
            // these tests won't change the database, define the expected difference
            // once for al ltests
            let expectedStateDifference = DBState.zeroState

            // selecting a scalar, with parameterised query
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.queryAction "select id from instances where id=%d" 82
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 82 ])
            do! checkDifferenceNow ()

            // selecting a scalar
            // the type to be returned is inferred from the call to checkOkDBResult
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.queryAction "select id from instances where id=82"
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 82 ])
            do! checkDifferenceNow ()

            // selecting a scalar
            // the parameterless query is passed in a variables, and hence
            // requires the static variant to be used
            let query = "select id from instances where id=82"
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.staticQueryAction query
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 82 ])
            do! checkDifferenceNow ()

            // data can be retrieved in an anonymous record
            // the type to be returned is inferred from the call to checkOkDBResult
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.queryAction "select id,entity_id from instances where id=82"
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ {| id = 82; entity_id = 12 |} ])
            do! checkDifferenceNow ()

            // data can be retrieved in an model record
            // the type to be returned is passed explicitely to the call DBAction.run
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.queryAction "select * from instances where id=82"
                |> DBAction.run<Instance.Instance> Client.client
                |> Async.map (checkOkDBResult [ { ``id``=newInstanceId 82; entity_id=newEntityId 12; created_at= None; lock_version=0; updated_at=None}])
            do! checkDifferenceNow ()

            // with static arguments, that need to be replaced before passing the query to the database
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.queryAction "select %A from %A where id=%d" (Static "id") (Static "instances") 82
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 82 ])
            do! checkDifferenceNow ()

            // with static arguments not at the start, that need to be replaced before passing the query to the database
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.queryAction "select %A from %A where id in (%d,%d) %A" (Static "id") (Static "instances") 82 83 (Static "order by id asc")
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 82; 83 ])
            do! checkDifferenceNow ()
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.queryAction "select %A from %A where id in (%d,%d) %A" (Static "id") (Static "instances") 82 83 (Static "order by id desc")
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 83; 82 ])
            do! checkDifferenceNow ()

            // single row action
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.querySingleAction "select id from instances where id=82"
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 82 ])
            do! checkDifferenceNow ()

            // single row action error when multiple rows returned
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.querySingleAction "select id from instances where id <=100000"
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["One row expected in result but got 39"])
            do! checkDifferenceNow ()

            // selecting a scalar, error if multiple columns are selected
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.queryAction "select id,entity_id from instances where id=%d" 82
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Multiple columns returned by query when returning scalar, use a record to store result instead.\nIf your type is a record, be sure it is reachable (not private, present in the .fsi).\nCommand was: select id,entity_id from instances where id=@p0"])
            do! checkDifferenceNow ()
        }

        [<Fact>]
        member __.``dataTableAction``() = async {
            // these tests won't change the database, define the expected difference
            // once for al ltests
            let expectedStateDifference = DBState.zeroState

            // just one field
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.dataTableAction "select id from instances where id=%d" 82
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """
                                        [ {id: 82} ]
                """) ])
            do! checkDifferenceNow ()

            // multiple fields, of which 1 is null
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.dataTableAction "select id,entity_id,created_at from instances where id=%d" 82
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """
                                        [ {id: 82, entity_id: 12,created_at:null} ]
                """) ])
            do! checkDifferenceNow ()

            // multiple fields, of which 1 is null
            // parameterless query
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.dataTableAction "select id,entity_id,created_at from instances where id=82"
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """
                                        [ {id: 82, entity_id: 12,created_at:null} ]
                """) ])
            do! checkDifferenceNow ()

            // multiple fields, of which 1 is null
            // parameterless query in a variable
            let query = "select id,entity_id,created_at from instances where id=82"
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.staticDataTableAction query
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """
                                        [ {id: 82, entity_id: 12,created_at:null} ]
                """) ])
            do! checkDifferenceNow ()
        }

        [<Fact>]
        member __.``statementAction``() = async {
            let _rollbackDisposable = MyOwnDB.Tests.Utils.setSavePoint Client.client

            // delete 1 instance, so 1 impacted row, but foreign key constraints cascade
            // deletion in other tables
            let expectedStateDifference =
                { DBState.zeroState with
                      instances = -1L
                      links = -1L
                      detailValues = -6L }
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.statementAction "delete from instances where id=%d" 75
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ ImpactedRows 1 ])
            do! checkDifferenceNow ()

            // zero rows impacted because user with id 75 does not exist
            let expectedStateDifference = DBState.zeroState
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.statementAction "update users set user_type_id=2 where id=%d" 75
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ ImpactedRows 0 ]) // No error, but zero row impacted
            do! checkDifferenceNow ()

            // 1 row impacted by update, but not deletion insertion
            let expectedStateDifference = DBState.zeroState
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.statementAction "update users set user_type_id=1 where id=%d" 100001
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ ImpactedRows 1 ]) // 1 row impacted
            do! checkDifferenceNow ()

            // 3 row impacted by delete
            let expectedStateDifference = DBState.zeroState
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.statementAction "delete from users where user_type_id=%d" 2
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ ImpactedRows 3 ]) // 1 row impacted
            do! checkDifferenceNow ()
        }

        [<Fact>]
        member __.``dataTableToCSV``() = async {
            // these tests won't change the database, define the expected difference
            // once for al ltests
            let expectedStateDifference = DBState.zeroState


            // WARNING WARNING WARNING !!!
            // Error reports duplicate the first and last double quotes. Eg when replace 82 by 83 in the first
            // test, the error is
            //         Expected: Equals [""id"
            //         "83""]
            //         Actual:   was ["id"
            //         "82"]
            // But the error is not with the quotes, the error is with the id
            // and putting the right expected value 82 makes the test pass.
            // WARNING WARNING WARNING !!!

            // just one field
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.dataTableAction "select id from instances where id=%d" 82
                |> DBAction.mapDataTableToCSV
                |> DBAction.run Client.client
                //|> Async.map (checkOkDBResult [ (String.Concat [|"\"id\"";"\n";"\"82\""|]) ])
                |> Async.map (checkOkDBResult [ "\"id\"\n\"82\"" ])
            do! checkDifferenceNow ()

            // multiple fields, of which 1 is null
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.dataTableAction "select id,entity_id,created_at from instances where id=%d" 82
                |> DBAction.mapDataTableToCSV
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ "\"id\",\"entity_id\",\"created_at\"\n\"82\",\"12\",\"\"" ])
            do! checkDifferenceNow ()

            // multiple rows
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference
            do! DBAction.dataTableAction "select id,entity_id,created_at from instances order by id asc limit 3"
                |> DBAction.mapDataTableToCSV
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ "\"id\",\"entity_id\",\"created_at\"\n\"25\",\"7\",\"\"\n\"69\",\"11\",\"\"\n\"70\",\"12\",\"\"" ])
            do! checkDifferenceNow ()
        }

        [<Fact>]
        member __.``dbaction_mapError``() = async {

            // merge DBAction<int>
            do! mapError (fun e -> Error.fromString $"my new error, from :{Error.toString e}")(wrapList [1;2;3])
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1;2;3;])

            do! mapError (fun e -> Error.fromString $"my new error, from: {Error.toString e}")(error "initial error")
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["my new error, from: initial error"])
        }

        [<Fact>]
        member __.``dbaction_configMap``() = async {

            // Check empty config map is initially available
            do! DBAction.config
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [Map.empty<string,string>])

            // Add a config item
            Client.client.SetConfigItem("item1","value1")
            // Check the map is as expected
            do! DBAction.config
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [Map [ ("item1", "value1")]])

            // Check we can use DBAction functions
            do! DBAction.config
                |> DBAction.map (Map.mapValues String.toUpper)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [Map [ ("item1", "VALUE1")]])

            // Check the map is still as expected
            do! DBAction.config
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [Map [ ("item1", "value1")]])
        }

        [<Fact>]
        member __.``dbaction_trigger``() = async {
            let a1 = wrapList [1;2;3]
            let a2 = wrapList ["two";"four"]
            let empty = wrapList []
            let err = error "action failed"

            // We will Enqueue strings here to check we call everything that's needed
            // and in the right order
            let queue = System.Collections.Concurrent.ConcurrentQueue()

            let triggeredFunction (res:'a list) =
              queue.Enqueue $"""We ran after an action generating {res}"""
              DBAction.retn ()

            // Single action
            do! a1
                |> trigger triggeredFunction
                |> run Client.client
                |> Async.map (checkOkDBResult  [1;2;3])

            queue.ToArray() |> should equal [|"We ran after an action generating [1; 2; 3]"|]
            queue.Clear()

            // Single action, with another type, just to be sure
            do! a2
                |> trigger triggeredFunction
                |> run Client.client
                |> Async.map (checkOkDBResult  ["two";"four"])

            queue.ToArray() |> should equal [|"""We ran after an action generating [two; four]"""|]
            queue.Clear()

            // Triggered even if empty result
            do! empty
                |> trigger triggeredFunction
                |> run Client.client
                |> Async.map (checkOkDBResult  [])

            queue.ToArray() |> should equal [|"We ran after an action generating []"|]
            queue.Clear()

            // Multiple triggers in a pipe
            do! a1
                |> trigger triggeredFunction
                |> DBAction.map ( fun i -> i * 2)
                |> trigger triggeredFunction
                |> run Client.client
                |> Async.map (checkOkDBResult  [2;4;6])

            queue.ToArray()
                  |> should equal
                    [|
                      "We ran after an action generating [1; 2; 3]";
                      "We ran after an action generating [2; 4; 6]"
                    |]
            queue.Clear()

            // Check order of actions executions
            let loggingAction =
              // We initialise an action returning unit, then map it so our code is called.
              // Don't do `DBAction.retn (queue.Enqueue ...)` as the enqueue would be done
              // when the DBAction is created, not when it is run.
              DBAction.retn ()
                |> DBAction.map (fun () -> queue.Enqueue "This should run first")
                |> DBAction.bind
                  (fun () ->
                    queue.Enqueue "This should run second"
                    wrapList [9;8;7]
                  )
            do! loggingAction
                |> DBAction.trigger
                  (fun l ->
                    queue.Enqueue $"trigger runs third getting result {l}"
                    // The function must return a DBAction<unit>
                    DBAction.retn ()
                  )
                |> run Client.client
                |> Async.map (checkOkDBResult  [9;8;7])

            queue.ToArray()
                  |> should equal
                    [|
                      "This should run first";
                      "This should run second";
                      "trigger runs third getting result [9; 8; 7]"
                    |]
            queue.Clear()

            // Do not trigger if error
            do! err
                |> trigger triggeredFunction
                |> DBAction.map ( fun i -> i * 2)
                |> trigger triggeredFunction
                |> run Client.client
                |> Async.map (checkErrorResult  ["action failed"])

            queue.ToArray() |> should equal [| |]
            queue.Clear()


            // Do trigger before but not after error
            do! loggingAction
                |> DBAction.trigger
                  (fun l ->
                    queue.Enqueue $"trigger runs third getting result {l}"
                    // The function must return a DBAction<unit>
                    DBAction.retn ()
                  )
                |> DBAction.bind ( fun _ -> DBAction.error "failed bind")
                |> trigger triggeredFunction
                |> run Client.client
                |> Async.map (checkErrorResult  ["failed bind";"failed bind";"failed bind";])

            queue.ToArray()
                  |> should equal
                    [|
                      "This should run first";
                      "This should run second";
                      "trigger runs third getting result [9; 8; 7]"
                    |]
            queue.Clear()
        }
