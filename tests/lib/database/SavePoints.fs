namespace MyOwnDB.Tests.SavePoints

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open DBTypes
open FSharp.Control.Tasks
open FSharpPlus

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =
        let fixtures =
            [ "Account"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details" ]
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module SavePoints =
    [<Trait("Category", "database")>]
    [<Trait("Category", "savepoints")>]
    type InstanceTests() =

        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``DB.SavePoints``() = async {
            // get state before any change. We will then check that after we rollback
            // no change is present in the database
            let! preState, checkDifferenceFromInitialState =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // at start, no savepoint is present
            Client.client.SavePoints()
            |> should equal (List<string>.Empty)

            // set first savepoint before any change
            do! Client.client.TrySetSavePoint "first"
                |> checkOkDBResultAsync [ "first" ]
            Client.client.SavePoints()
            |> should equal [ "first" ]


            // make first changes
            do! DBAction.recordStatementAction "delete from instances where id > 200 and id <= 100000" {|  |}
                |> DBAction.run Client.client
                |> checkOkDBResultAsync [ ImpactedRows 5 ]

            // check the delete instances are not there
            do! DBAction.immediate Client.client "select count(*) from instances where id <= 100000"
                |> checkOkDBResultAsync [ 34L ]

            // take db state snapshot
            let! _, checkDifferenceWithSecondSavePoint =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // set second savepoint
            do! Client.client.TrySetSavePoint "second"
                |> checkOkDBResultAsync [ "second" ]
            Client.client.SavePoints()
            |> should equal [ "second"; "first" ]

            // delete remaining instances
            do! DBAction.recordStatementAction "delete from instances where id <= 100000" {|  |}
                |> DBAction.run Client.client
                |> checkOkDBResultAsync [ ImpactedRows 34 ]

            // check the delete instances are not there
            do! DBAction.immediate Client.client "select count(*) from instances where id<= 100000"
                |> checkOkDBResultAsync [ 0L ]

            // go back one savepoint,
            do! Client.client.TryToSavePoint "second"
                |> checkOkDBResultAsync [ () ]
            Client.client.SavePoints()
            |> should equal [ "first" ]

            // check that compared to the db state at second savepoint, there's no difference
            do! checkDifferenceWithSecondSavePoint ()

            // got back to first savepoint
            do! Client.client.TryToSavePoint "first"
                |> checkOkDBResultAsync [ () ]
            // check there's no savepoint left
            Client.client.SavePoints()
            |> should equal (List<string>.Empty)

            // check we are back at the initial state
            do! DBAction.immediate Client.client "select count(*) from instances where id <=  100000"
                |> checkOkDBResultAsync [ 39L ]
            do! checkDifferenceFromInitialState ()
        }
