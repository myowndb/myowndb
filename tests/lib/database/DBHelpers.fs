namespace MyOwnDB.Tests.DBHelpers

open System
open Xunit
open FsUnit.Xunit
open MyOwnDB.Tests.Helpers
open DBTypes
open MyOwnDB.Extensions
open FSharp.Control.Tasks
open FSharpPlus
open Ids
// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)


module Fixtures =
    type DBHelperFixture() =

        let fixtures =
                [| "Account+Account,lib"
                   "Database+Database,lib"
                |]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client fixtures |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

open MyOwnDB.Tests.IdsHelpers
module Database =
    // to issue DBAction sql queries to database
    let issuer<'T> action = DBAction.run<'T> Client.client action
    // to issue DBAction returned by parameterless function f
    let issuerf f = DBAction.execute Client.client f

    // Define types that we will use in the test below to validate
    // mappings to types
    type Protocol = |Smtp |Http
    type AddressId = AddressId of int
    type Address =
      {
        id: AddressId option
        protocol: Protocol
        protocolOption: Protocol option
        protocolOption2: Protocol option
        protocolString : string
        protocolString2 : string
      }


    [<Trait("Category", "dbhelpers")>]
    type DatabaseTests() =
        interface IClassFixture<Fixtures.DBHelperFixture>
        [<Fact>]
        member __.``helper_mapping_records``() = async {
            do! DBAction.queryAction "select id as DatabaseId,account_id as AccountId, name from databases where id=6"
                |> issuer<Specs.DatabaseSpec>
                |> Async.map (checkOkResult [{databaseId=Some (newDatabaseId 6); accountId = AccountId 1; name = "DemoForTests"}])
        }

        [<Fact>]
        member __.``helper_mapping_records_with_option_ids``() = async {
            do! DBAction.queryAction "select null as DatabaseId,account_id as AccountId, name from databases where id=6"
                |> issuer<Specs.DatabaseSpec>
                |> Async.map (checkOkResult [{databaseId=None; accountId = AccountId 1; name = "DemoForTests"}])
        }

        [<Fact>]
        member __.``mapping_valueless_cases``() = async {
            do! DBAction.queryAction "select 34 as id,'smtp' as protocol, 'smtp' as protocolOption, null as protocolOption2, 'smtp' as protocolString, '' as protocolString2"
                |> issuer<Address>
                |> Async.map (checkOkResult [{id=Some (AddressId 34); protocol = Smtp; protocolOption = Some Smtp; protocolOption2 = None; protocolString = "smtp"; protocolString2 = ""}])
        }
