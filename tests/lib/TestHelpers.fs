// Helper functions related to XUnit

module MyOwnDB.Tests.Helpers

open Xunit
open FsUnit.Xunit
open Ids
open FSharpPlus
open DBAction.StaticArguments
open System.IO
open System.Security.Cryptography

// helper function to results
// we translate the error messages to the "system" locale.
// benefit: we can change the translation without impacting these tests
// downside: translation parameters are not validated (madb_tr email %s is invalid" is not becoming "email youremail.com is invalid"
//           and so we could not detect we got the error "email your@email.com is invalid", which is obviously wrong...
let checkOkResult (expected:'a) (actual:Result<'a,_>) =
    match actual with
    | Ok l -> l |> should equal expected
    | Error es ->
        (Error.toStringList es)
        |> sprintf "Got an error when OK expected: %A"
        |> should equal true
// The additional type annotations relieves the caller from adding type annotations on the actual value passed to this function
// For tests returning DBResult, use of both CheckOkResult and CheckOkDBResult is equivalent. It was needed to add CheckDBResult though
// as Validations tests return a Result.
let checkOkDBResult (expected: 'T list) (actual:DBResult.DBResult<'T>) =
    match actual with
    | Ok l -> l |> should equal expected
    | Error es ->
        (Error.toStringList es)
        |> sprintf "Got an error when OK expected: %A"
        |> should equal true

let checkOkDBResultAsync (expected: 'T list) (actual:Async<DBResult.DBResult<'T>>) = async {
    let! actualValue = actual
    match actualValue with
    | Ok l -> l |> should equal expected
    | Error es ->
        (Error.toStringList es)
        |> sprintf "Got an error when OK expected: %A"
        |> should equal true
}

let checkErrorResult expected actual =
    match actual with
    | Ok res ->
        res
        |> sprintf "This result (%A) should have been an error"
        |> should equal false
    | Error es ->
        es
        |> Error.toShortStringList
        |> should equal expected

let checkRowsCount (client: DB.DBClient) (table: string) (filter: string) expected =
    DBAction.immediate client "select count(*) from %A %A" (Static table) (Static filter)
    |> checkOkDBResultAsync [ expected ]

let checkDetailValuetype (client: DB.DBClient) (t: string) (id: DetailValueIdType option) =
    id
    |> Option.get
    |> DetailValue.byId
    |> DBAction.run client
    |> Async.map (DBResult.map (fun dv -> dv.``type``))
    |> checkOkDBResultAsync [ t ]

// module for getting and checking the database state (counts of rows, max ids, )
// Way to use these functions from the tests is like:
//  // Define a record instance listing the differences expected bewteen the 2
//  // state we will look at
//  let expectedStateDifference = {DBState.zeroState with
//                                    instances=1L
//                                    dateDetailValues=1L
//                                 }
//  // Define custom queries and with their difference of values between first and second state.
//  // Difference of values is the first eslement of the pairs
//  let customQueries = [ 1L, "select count(*) from date_detail_values where detail_id=76" ;
//                      ]
//  // take a first snapshot and get a function to compare against the expected differences
//  let preState, checkDifferenceNow = DBState.startWithCustomQueries
//                                                  Client.client
//                                                  customQueries
//                                                  expectedStateDifference
// // Issue database changing queries/statements
// ....
// // Check that the database was changed as expected, with the new state having
// // the expected changes in values
// checkDifferenceNow()
module DBState =
    // The record type holding the values extracted from the database.
    // This is mainly used with the difference function, returning the deltas for
    // each field between 2 states.
    // Looking at the number of entries (the fields of type count, eg instances,
    // entities,...), we can check the number of additional (or removed) entries
    // in the table. But this is not sufficient, as we don't detect if we insert
    // 2 rows and delete one pre existing... That's why the max ids fields are
    // also collected: it gives the maximum value found in the id column of the
    // table. If we insert 2 rows, the max id will be too high.As we use the
    // ids' sequence value, we also detect the scenario where we insert 2 rows and
    // delete the second one.
    // The firld otherIntegers is to collect values returnes by custom queries
    // passed by the caller.
    type State =
        { instances: int64 //count
          entities: int64 //count
          detailValues: int64 //count
          dateDetailValues: int64 //count
          ddlDetailValues: int64
          integerDetailValues: int64
          links: int64
          details: int64
          detailValuePropositions: int64
          entities2Details: int64
          databases: int64
          relations: int64
          imports: int64
          notificationSubscriptions: int64
          maxInstanceId: int // max
          maxEntityId: int
          maxDetailValueId: int
          maxDateDetailValueId: int
          maxDdlDetailValueId: int
          maxIntegerDetailValueId: int
          maxEntities2DetailsId: int
          maxLinkId: int
          maxDetailId: int
          maxDetailValuePropositionId: int
          maxDatabaseId: int
          maxRelationId: int
          maxImportId: int
          maxNotificationSubscriptionId: int
          otherIntegers: int64 list
          }
    // A state with all fields 0.
    // Useful for specifying expected changes when only few fields changed
    let zeroState =
        { instances = 0L
          entities = 0L
          detailValues = 0L
          dateDetailValues = 0L
          ddlDetailValues = 0L
          integerDetailValues = 0L
          links = 0L
          details = 0L
          detailValuePropositions = 0L
          entities2Details = 0L
          databases = 0L
          relations = 0L
          imports = 0L
          notificationSubscriptions = 0L
          maxInstanceId = 0
          maxEntityId = 0
          maxDetailValueId = 0
          maxDateDetailValueId = 0
          maxDdlDetailValueId = 0
          maxIntegerDetailValueId = 0
          maxLinkId = 0
          maxDetailId = 0
          maxEntities2DetailsId = 0
          maxDetailValuePropositionId = 0
          maxDatabaseId = 0
          maxRelationId = 0
          maxImportId = 0
          maxNotificationSubscriptionId = 0
          otherIntegers = [] }

    // Returns an action that fills the non-custom-query fields of the State;, i.e all fields
    // except otherIntegers.
    let getCurrentStateAction () =
        DBAction.retn zeroState
        |> DBAction.combine (fun i state -> { state with entities = i })
               (DBAction.querySingleAction "select count(*) from entities")
        |> DBAction.combine (fun i state -> { state with instances = i })
               (DBAction.querySingleAction "select count(*) from instances")
        |> DBAction.combine (fun i state -> { state with detailValues = i })
               (DBAction.querySingleAction "select count(*) from detail_values")
        |> DBAction.combine (fun i state -> { state with dateDetailValues = i })
               (DBAction.querySingleAction "select count(*) from date_detail_values")
        |> DBAction.combine (fun i state -> { state with ddlDetailValues = i })
               (DBAction.querySingleAction "select count(*) from ddl_detail_values")
        |> DBAction.combine (fun i state -> { state with integerDetailValues = i })
               (DBAction.querySingleAction "select count(*) from integer_detail_values")
        |> DBAction.combine (fun i state -> { state with links = i })
               (DBAction.querySingleAction "select count(*) from links")
        |> DBAction.combine (fun i state -> { state with details = i })
               (DBAction.querySingleAction "select count(*) from details")
        |> DBAction.combine (fun i state -> { state with entities2Details = i })
               (DBAction.querySingleAction "select count(*) from entities2details")
        |> DBAction.combine (fun i state -> { state with detailValuePropositions = i })
               (DBAction.querySingleAction "select count(*) from detail_value_propositions")
        |> DBAction.combine (fun i state -> { state with databases = i })
               (DBAction.querySingleAction "select count(*) from databases")
        |> DBAction.combine (fun i state -> { state with relations = i })
               (DBAction.querySingleAction "select count(*) from relations")
        |> DBAction.combine (fun i state -> { state with imports = i })
               (DBAction.querySingleAction "select count(*) from imports")
        |> DBAction.combine (fun i state -> { state with notificationSubscriptions = i })
               (DBAction.querySingleAction "select count(*) from notification_subscriptions")
        |> DBAction.combine (fun i state -> { state with maxInstanceId = i })
                // The time a sequence is created, its last_value is 1, and it has the flag is_called set to false. At the first
                // use, its last_value is not updated but the is_called flag is set to true.
                // See https://stackoverflow.com/a/49688819
                // This only caused trouble when working on an empty imports table, but the logic was fixed for every case anyway.
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from instances_id_seq")
        |> DBAction.combine (fun i state -> { state with maxEntityId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from entities_id_seq")
        |> DBAction.combine (fun i state -> { state with maxDetailValueId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from detail_values_id_seq")
        |> DBAction.combine (fun i state -> { state with maxDateDetailValueId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from date_detail_values_id_seq")
        |> DBAction.combine (fun i state -> { state with maxDdlDetailValueId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from ddl_detail_values_id_seq")
        |> DBAction.combine (fun i state -> { state with maxDatabaseId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from databases_id_seq")
        |> DBAction.combine (fun i state ->
            { state with
                  maxIntegerDetailValueId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from integer_detail_values_id_seq")
        |> DBAction.combine (fun i state -> { state with maxLinkId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from links_id_seq")
        |> DBAction.combine (fun i state -> { state with maxDetailId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from details_id_seq")
        |> DBAction.combine (fun i state -> { state with maxEntities2DetailsId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from entities2details_id_seq")
        |> DBAction.combine (fun i state -> { state with maxDetailValuePropositionId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from detail_value_propositions_id_seq")
        |> DBAction.combine (fun i state -> { state with maxRelationId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from relations_id_seq")
        |> DBAction.combine (fun i state -> { state with maxImportId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from imports_id_seq")
        |> DBAction.combine (fun i state -> { state with maxNotificationSubscriptionId = i })
               (DBAction.querySingleAction "select CASE when is_called then last_value::int ELSE 0 END from notification_subscriptions_id_seq")

    // Function to retrieve the current state of the database. Useful in tests where we need to extract data before
    // the db action, but where we don't want to check the changes applied. Eg in notification tests, we want to know
    // the id of the instance created, without repeating all validations that are already done elsewhere.
    let getCurrentState (client:DB.DBClient) = async {
        let! preStateResult =
            getCurrentStateAction ()
            |> DBAction.run client
        return
          preStateResult
          |> DBResult.get
          |> List.head
    }
    // Add results of the custom queries to the state
    let getCurrentStateWithCustomQueriesAction (queries: string list) =
        let standardAction = getCurrentStateAction ()
        // we append to the list, though not efficient, we don't work on big lists here
        let folder a query =
            a
            |> (DBAction.combine (fun i state ->
                    { state with
                          otherIntegers = List.append state.otherIntegers [ i ] }) (DBAction.staticQueryAction query))

        queries |> List.fold folder standardAction

    let getCurrentStateWithOnlyCustomQueriesAction (queries: string list) =
        // builds a DBAction yielding a DBResult whose values are of type 'a list
        let folder a query =
            a
            |> (DBAction.merge  (DBAction.staticQueryAction query))
        // reverse because folding reverts it also
        queries |> List.rev |> List.fold folder DBAction.zero

    // Gives the differences of values between 2 states.
    // It also handles the list of results of custom queries
    let difference (stateA: DBResult.DBResult<State>) (stateB: DBResult.DBResult<State>) =
        let diff (before: State) (after: State) =
            { instances = after.instances - before.instances
              entities = after.entities - before.entities
              detailValues = after.detailValues - before.detailValues
              dateDetailValues = after.dateDetailValues - before.dateDetailValues
              ddlDetailValues = after.ddlDetailValues - before.ddlDetailValues
              integerDetailValues =
                  after.integerDetailValues
                  - before.integerDetailValues
              links = after.links - before.links
              details = after.details - before.details
              entities2Details = after.entities2Details - before.entities2Details
              detailValuePropositions = after.detailValuePropositions - before.detailValuePropositions
              databases = after.databases - before.databases
              relations = after.relations - before.relations
              imports = after.imports - before.imports
              notificationSubscriptions = after.notificationSubscriptions - before.notificationSubscriptions
              maxInstanceId = after.maxInstanceId - before.maxInstanceId
              maxEntityId = after.maxEntityId - before.maxEntityId
              maxDetailValueId = after.maxDetailValueId - before.maxDetailValueId
              maxDateDetailValueId =
                  after.maxDateDetailValueId
                  - before.maxDateDetailValueId
              maxDdlDetailValueId =
                  after.maxDdlDetailValueId
                  - before.maxDdlDetailValueId
              maxIntegerDetailValueId =
                  after.maxIntegerDetailValueId
                  - before.maxIntegerDetailValueId
              maxLinkId = after.maxLinkId - before.maxLinkId
              maxDetailId = after.maxDetailId - before.maxDetailId
              maxEntities2DetailsId = after.maxEntities2DetailsId - before.maxEntities2DetailsId
              maxDetailValuePropositionId = after.maxDetailValuePropositionId - before.maxDetailValuePropositionId
              maxDatabaseId = after.maxDatabaseId - before.maxDatabaseId
              maxRelationId = after.maxRelationId - before.maxRelationId
              maxImportId = after.maxImportId - before.maxImportId
              maxNotificationSubscriptionId = after.maxNotificationSubscriptionId - before.maxNotificationSubscriptionId
              otherIntegers =
                  List.zip after.otherIntegers before.otherIntegers
                  |> List.map (fun (a, b) -> a - b) }

        let (<*>) = DBResult.apply
        DBResult.retn diff <*> stateA <*> stateB

    // take the first State snapshot, and return it in a pair with a function to
    // get the difference against that snapshot.
    // intQueries is a list of pairs, the second element is an integer-returning
    // sql query, and the first element is the expected difference of the
    // returned value between the first and second state
    let startWithCustomQueries (client: DB.DBClient) (intQueries: (int64 * string) list) (expectedDifference: State) = async {
        // extract the expected differences and the queries
        let (expected, queries) = List.unzip intQueries
        // the the first state snapshot
        let action =
            getCurrentStateWithCustomQueriesAction queries

        let! firstState = action |> DBAction.run client
        // build the function to be called to take a second snapshot and compare
        // against the first
        let step2Function () = async {
            let! secondState = action |> DBAction.run client
            let diff = difference firstState secondState

            let expected =
                { expectedDifference with
                      otherIntegers = expected }

            diff |> checkOkDBResult [ expected ]
        }
        // return a pair with the first state, and the just defined function
        return (firstState |> DBResult.get |> List.head, step2Function)
    }

    let start(client: DB.DBClient) (expectedDifference: State) = async {
      return! startWithCustomQueries client [] expectedDifference
    }

    let startWithOnlyCustomQueries (client: DB.DBClient) (intQueries: (int64 * string) list) = async {
        // extract the expected differences and the queries
        let (expected, queries) = List.unzip intQueries
        // take the first state snapshot
        let action = getCurrentStateWithOnlyCustomQueriesAction queries
        let! firstState = action |> DBAction.run client

        // build the function to be called to take a second snapshot and compare
        // against the first
        let step2Function () = async {
            let! secondState = action |> DBAction.run client
            // flatten the states' list so that we don't have nested lists in the DBResult
            // then zip it to make it easy to substract the first from the second to get the
            // difference
            let diff = DBResult.zip firstState secondState
                        |> DBResult.map (fun (f,s) -> s-f)
            diff |> checkOkDBResult expected
        }
        // return a pair with the first state, and the just defined function
        return (firstState |> DBResult.get |> List.head, step2Function)
    }

let newInstanceId = MyOwnDB.Tests.IdsHelpers.newInstanceId
let newEntityId = MyOwnDB.Tests.IdsHelpers.newEntityId
let newDatabaseId = MyOwnDB.Tests.IdsHelpers.newDatabaseId
let newRelationId = MyOwnDB.Tests.IdsHelpers.newRelationId

// helper to enable logging in tests
let logAllToTmp() =
    MyOwnDBLib.Logging.setDefaultLoggerLevel(MyOwnDBLib.Logging.Verbose)
    MyOwnDBLib.Logging.setDefaultLoggerPath("/tmp/tests.log")
    MyOwnDBLib.Logging.useDefaultLogger()

let checkFileHash(path,expectedFile:string) = task {
    // compute on downloaded file
    let bytes = File.ReadAllBytes(path)
    let sha256 = HashAlgorithm.Create("SHA256").ComputeHash bytes
    // compute on fixtures file
    let expectedBytes = File.ReadAllBytes(expectedFile)
    let expectedSha256 = HashAlgorithm.Create("SHA256").ComputeHash expectedBytes

    sha256 |> should equal expectedSha256
}

// Helper so setup notifiers to log to a concurrent queue initialised in the tests.
// Each notification is enqueued as a tuple.
let notifyToQueue (notificationsQueue:System.Collections.Concurrent.ConcurrentQueue<_>)=
    // Setup notifiers for tests. This needs to be done in the apps using this lib.
    NotificationSubscription.Notifier.setNotifier
      NotificationSubscriptionCriteria.Protocol.Smtp
      (fun sub sourceId ->
        notificationsQueue.Enqueue(sub.event,sub.source_type,sourceId,sub.destination)
      )
