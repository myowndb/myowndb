open Ids
open IdsExtensions

// for ConsoleColor
open System
open FSharpPlus

// for arguments parsing
open Argu

// for ksprintf
open Printf

type OutputFormat =
    | JSON
    | ASCII

type CmdArgs =
    | [<AltCommandLine("-d"); Unique>] Database of db: string
    | [<AltCommandLine("-j")>] JSON
    | [<AltCommandLine("-e")>] Existing
    | [<AltCommandLine("-k")>] Keep
    | [<AltCommandLine("-q")>] Quiet
    | [<CliPrefix(CliPrefix.None)>] Instances of id: int
    | [<CliPrefix(CliPrefix.None)>] Entities
    interface IArgParserTemplate with
        member this.Usage =
            match this with
            | Database _ -> "Database to use, will look for config file 'conf/database-${db}.json'"
            | Existing _ -> "Do not load fixture but work with existing data"
            | JSON -> "Print JSON output"
            | Keep ->
                "Dot not clean database before exit. Enables you to look at the data in the database, but requires that you reinitialises it before the next run."
            | Quiet -> "Do not output informational messages on stderr"
            | Instances _ -> "Display instances for an entity id"
            | Entities -> "Display entities available"

let fixtures =
    [ "Account"
      "User"
      "Database"
      "Entity"
      "Instance"
      "Detail"
      "DateDetailValue"
      "DetailValue"
      "DetailValueProposition"
      "DdlDetailValue"
      "IntegerDetailValue"
      // not loading entities2details will results in an error
      // query-specified return tuple and crosstab function are not compatible
      "Entities2details"
      "Relation"
      "Link"
    ]


// functions to help build an Id values. We don't check anything here.
let AlwaysValid _ = true

let newEntityId (id:int) =
    match EntityId.Validate(id,AlwaysValid) with
    | Some v -> v
    | None -> failwith "unexpected error in building always valid entity id"


let printInfo (args: ParseResults<CmdArgs>) (s: string) =
    if args.Contains(Quiet) then () else eprintfn "%s" s
// Flags handling functions
let databaseConfig (args: ParseResults<CmdArgs>) =
    let db = args.GetResult(Database, defaultValue = "dev")
    sprintf "conf/database-%s.json" db

let outputFormat (args: ParseResults<CmdArgs>): OutputFormat =
    if args.Contains(JSON) then OutputFormat.JSON else OutputFormat.ASCII

let keepData (args: ParseResults<CmdArgs>) = args.Contains(Keep)

let getEntityId (args: ParseResults<CmdArgs>) = args.GetResult(Instances, defaultValue = 12)

let useExistingData (args: ParseResults<CmdArgs>) = args.Contains(Existing)

// Validate the request is consistent with the database state
let validateRequest client args =
    let check =
        // check against int64 as that's what's returned by npgsql
        if useExistingData args then (<) 0L else (=) 0L
    // check database state
    fixtures
    |> List.forall (fun fixture ->
        (Fixtures.fixtureTableName fixture)
        |> DBAction.StaticArguments.Static
        |> DBAction.immediate client "select count(*) from %A"
        |> Async.map (DBResult.forall check)
        |> Async.RunSynchronously)

// Top level functions for maintaining database state
let loadDataIfNeeded (client: DB.DBClient) args =
    if not (useExistingData args) then
        client.BeginTransaction() |> Async.RunSynchronously
        // load database records
        ksprintf (printInfo args) "will load data"
        Fixtures.processRecordsArray client
            (fixtures
             |> List.map (fun f -> sprintf "%s+%s,lib" f f)
             |> Array.ofList)
        |> Array.iter (fun v -> v |> Async.RunSynchronously |> ignore)

let commitOrRollbackIfNeeded (client: DB.DBClient) args =
    // commit or rollback if we loaded fixtures
    if not (useExistingData args) then
        // commit or rollback according to user input
        match keepData args with
        | true ->
            ksprintf (printInfo args) "Commiting data"
            client.Commit() |> Async.RunSynchronously
        | false ->
            ksprintf (printInfo args) "Rollback DB transaction"
            client.TryRollback|>ignore

let runAndSprintDataTable client action =
    action
    |>DBAction.run client
    |> Async.RunSynchronously
    |> DBResult.toSingleResult
    |> Result.map Cli.sprintDataTable
    |> (function
    | Ok s -> s
    | Error es -> Error.toString  es)
    |> sprintf "%s"

// Data display functions
let displayInstances client args =
    let entityId = getEntityId args

    // print output
    match outputFormat args with
    | OutputFormat.ASCII ->
        printf "%s" (Cli.tableString (Locale.fromString "en-US") client (newEntityId entityId)|> Async.RunSynchronously)
        let info: Crosstab.Info =
            Crosstab.getCrosstabInfoAction (newEntityId entityId)
            |> DBAction.run client
            |> Async.RunSynchronously
            |> DBResult.get
            |> List.head

        let detailsInfo =
            info.entityDetails
            |> List.map
                (fun d ->
                    sprintf "%d\t%10s%s\t%10s" (d.detail_id|> DetailId.Get) d.name (if d.displayed_in_list_view then "*" else "")
                        d.data_type)
        let relationsToChildrenInfo =
                let EntityId entityId = entityId
                DBAction.dataTableAction "select r.child_id,r.from_parent_to_child_name as to_child, rt.name as cardinality from relations r join relation_side_types rt on (rt.id=r.child_side_type_id) where r.parent_id=%d" entityId
                |> runAndSprintDataTable client
        let relationsToParentsInfo =
                let EntityId entityId = entityId
                DBAction.dataTableAction "select r.parent_id,r.from_child_to_parent_name as to_parent, rt.name as cardinality from relations r join relation_side_types rt on (rt.id=r.parent_side_type_id) where r.child_id=%d" entityId
                |> runAndSprintDataTable client
        printfn "\nDetails info (*=in list view):\n%s" (String.concat "\n" detailsInfo)
        printfn "\nRelations to children:\n%s" relationsToChildrenInfo
        printfn "\nRelations to parents:\n%s" relationsToParentsInfo
    | OutputFormat.JSON -> printf "%s" (Cli.tableJson (Locale.fromString "en-US") client (newEntityId entityId)|> Async.RunSynchronously)

let displayEntities client args =
    DBAction.staticDataTableAction "select * from entities"
    |> runAndSprintDataTable client
    |> printfn "%s"

let displayData client (args: ParseResults<CmdArgs>) =
    if (args.Contains Instances) then displayInstances client args
    else if (args.Contains Entities) then displayEntities client args



[<EntryPoint>]
let main argv =

    let errorHandler =
        ProcessExiter
            (colorizer =
                function
                | ErrorCode.HelpText -> None
                | _ -> Some ConsoleColor.Red)

    let parser = ArgumentParser.Create<CmdArgs>(programName = "fixtinspect", errorHandler = errorHandler)
    let args = parser.ParseCommandLine argv

    let config = AppConfig.load (databaseConfig args)
    let client = DB.DBClient(config)
    client.Open()

    ksprintf (printInfo args) "Using database %s as user %s" (AppConfig.database config) (AppConfig.user config)

    if not (validateRequest client args) then
        eprintfn "Request not valid: data present when loading fixtures, or not data when using existing date"
        exit 1

    loadDataIfNeeded client args

    displayData client args
    commitOrRollbackIfNeeded client args

    client.Close()
    0
