namespace MyOwnDB.Tests.Localisation.Translations

open Xunit
open FsUnit.Xunit


// Adding a language:
// *****************
// lang = "en_US"
//  msginit --input=po/system/tests.pot --locale=${lang} --output po/${lang}/tests.po
// CHECK THAT THE CHARSET IN THE HEADER OF THE GENERATED FILE IS UTF-8 !!!

// Adding a translated string:
// ***************************
// - Add it in po/system/tests.pot
// - Update po files for every language:
//    -  for d in $(find . -maxdepth 1 -name '*_*'); do lang=${d#./}; msgmerge --update ${lang}/tests.po system/tests.pot;  done
// - Edit the tests.po files for each language to translate the new string. Note that in the translation, arguments are positional!
//   This means that
//      msgid "madb_tr integer:%d float:%f"
//   is translated to
//      msgstr "The integet is {0} and the float {1}"
//   or
//      msgstr "The float is {1} and the int is {0}"
// - Update the .mo files for every language
//    -  for d in $(find . -maxdepth 1 -name '*_*'); do lang=${d#./}; mkdir -p ${lang}/LC_MESSAGES; msgfmt ${lang}/tests.po --output-file ${lang}/LC_MESSAGES/tests.mo;  done

// PLural translations
// *******************
// msgid and msgstr are the same. For languages having one plural form (like en and fr), msgstr[0] is the singular form, msgstr[1] is the plural form.
// in the system/tests.pot file, you just add msgid and msgid_plural:
//    msgid "madb_tr Plural translation: there are %d errors"
//    msgid_plural "madb_tr Plural translation: there are %d errors"
//    msgstr[0] ""
//    msgstr[1] ""
// and in the language's po file, you set the values for the translations:
//    msgstr[0] "There is {0} error."
//    msgstr[1] "There are {0} errors."
//


// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture


module Translation =
    let translationConfig: Translation.TranslationConfig =
        { directory = "localisation/po"
          file = "tests" }

    let en_us = Translation.translateWithConfig translationConfig (Locale.fromString "en_US")
    let fr_fr = Translation.translateWithConfig translationConfig (Locale.fromString "fr_FR")


    [<Trait("Category", "l10n")>]
    type TranslationTests() =

        [<Fact>]
        member __.``Translations without parameters``() =
            let translation = Translation.create "madb_tr Translation without parameter"
            en_us translation |> should equal "This is the translation without parameter in en_US"
            fr_fr translation |> should equal "La traduction sans paramètre en fr_FR"

        [<Fact>]
        member __.``Translations with one parameter``() =
            let translation =
                Translation.create "madb_tr Translation with one string parameter \"%s\" substituted" "¯\_(ツ)_/¯"
            en_us translation
            |> should equal "This is the translation with one string parameter \"¯\_(ツ)_/¯\" substituted in en_US"
            fr_fr translation
            |> should equal "La traduction avec une chaîne de caractères \"¯\_(ツ)_/¯\" en paramètre."

            let translationInt =
                Translation.create "madb_tr Translation with one integer parameter \"%d\" substituted" 42
            en_us translationInt
            |> should equal "This is the translation with one integer parameter \"42\" substituted in en_US"

        [<Fact>]
        member __.``Translations with multiple parameters and mixing their usage order``() =
            let translation =
                Translation.plural 3L "madb_tr The email %s has %d error at the temperature of %f" "info@example.com" 3
                    32.5
            en_us translation
            |> should equal "At the temperature of 32.5 degrees, the email info@example.com had 3 errors."
            fr_fr translation
            |> should equal "L'adresse info@example.com a eu 3 erreurs à la température de 32,5 degrés."

            let translationSingular =
                Translation.plural 1L "madb_tr The email %s has %d error at the temperature of %f" "admin@example.com"
                    1 42.5
            en_us translationSingular
            |> should equal "At the temperature of 42.5 degrees, 1 error was found for email admin@example.com."
            fr_fr translationSingular
            |> should equal "1 erreur a été détectée à la température de 42,5 degrés pour l'adresse admin@example.com."

        [<Fact>]
        member __.``Translations with plural form``() =
            let translationPlural = Translation.plural (int64 5) "madb_tr Plural translation: there are %d errors" 5
            en_us translationPlural |> should equal "There are 5 errors."
            fr_fr translationPlural |> should equal "Il y a 5 erreurs."
            let translationSingular =
                Translation.plural (int64 1) "madb_tr Plural translation: there are %d errors" 1
            en_us translationSingular |> should equal "There is one (1) error."
            fr_fr translationSingular |> should equal "Il y a une (1) erreur."
