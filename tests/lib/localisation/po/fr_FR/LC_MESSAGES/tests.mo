��          L      |       �   _   �   u   	  ?     >   �  %   �  1  $  ,   V  �   �  1     B   K  &   �                                         madb_tr Plural translation: there are %d errors madb_tr Plural translation: there are %d errors madb_tr The email %s has %d error at the temperature of %f madb_tr The email %s has %d error at the temperature of %f madb_tr Translation with one integer parameter "%d" substituted madb_tr Translation with one string parameter "%s" substituted madb_tr Translation without parameter Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-03-11 09:07+0100
Last-Translator: rb <info@myowndb.com>
Language-Team: French
Language: fr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
 Il y a une ({0}) erreur. Il y a {0} erreurs. {1} erreur a été détectée à la température de {2} degrés pour l'adresse {0}. L'adresse {0} a eu {1} erreurs à la température de {2} degrés. La traduction avec un entier "{0}" en paramètre. La traduction avec une chaîne de caractères "{0}" en paramètre. La traduction sans paramètre en fr_FR 