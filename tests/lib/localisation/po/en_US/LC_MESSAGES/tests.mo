��          L      |       �   _   �   u   	  ?     >   �  %   �  6  $  /   [  �   �  M     L   `  2   �                                         madb_tr Plural translation: there are %d errors madb_tr Plural translation: there are %d errors madb_tr The email %s has %d error at the temperature of %f madb_tr The email %s has %d error at the temperature of %f madb_tr Translation with one integer parameter "%d" substituted madb_tr Translation with one string parameter "%s" substituted madb_tr Translation without parameter Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2019-03-11 09:06+0100
Last-Translator: rb <info@myowndb.com>
Language-Team: English
Language: en_US
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 There is one ({0}) error. There are {0} errors. At the temperature of {2} degrees, {1} error was found for email {0}. At the temperature of {2} degrees, the email {0} had {1} errors. This is the translation with one integer parameter "{0}" substituted in en_US This is the translation with one string parameter "{0}" substituted in en_US This is the translation without parameter in en_US 