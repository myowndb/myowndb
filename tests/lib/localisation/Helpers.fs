namespace MyOwnDB.Tests.Localisation.Helpers

open Xunit
open FsUnit.Xunit
open NGettext
open System.Globalization

module Translation =

    // FIXME: add a Makefile that will compile the po before running the test
    // if you update the po file, you need to compile it with
    //   msgfmt myowndb.po --output-file myowndb.mo
    let catalogFr = Catalog("myowndb", "localisation/locale", CultureInfo("fr"))
    let tfr = Myowndb.I18n.Helpers(catalogFr)

    [<Trait("Category", "l10n")>]
    type TranslationTests() =

        [<Fact>]
        member __.``i18n_helpers``() =
            tfr.t("Please login to access the app")
            |> should equal "Veuillez vous authentifier pour accéder à l'application."
            tfr.t("Translation with placeholder: {0}","value")
            |> should equal "La valeur value est placée dans la traduction"
            tfr.n("You have {0} message","You have {0} messages", 0L,0L)
            |> should equal "Vous avez 0 message"
            tfr.n("You have {0} message","You have {0} messages", 1L,1L)
            |> should equal "Vous avez 1 message"
            tfr.n("You have {0} message","You have {0} messages", 2L,2L)
            |> should equal "Vous avez 2 messages"
            tfr.c("weekday", "Sun")
            |> should equal "Dim"
            tfr.c("planet", "Sun")
            |> should equal "Soleil"
            tfr.cn("weekday", "{0} Sun","{0} Sundays",1L,1L)
            |> should equal "1 Dim"
            tfr.cn("weekday", "{0} Sun","{0} Sundays",2L,2L)
            |> should equal "2 Dimanches"
            tfr.cn("planet", "{0} Sun","{0} Suns",1L,1L)
            |> should equal "1 Soleil"
            tfr.cn("planet", "{0} Sun","{0} Suns",2L,2L)
            |> should equal "2 Soleils"