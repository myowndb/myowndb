namespace MyOwnDB.Tests.Cli

open System
// for File
open System.IO
open Xunit
open FsUnit.Xunit
open FSharp.Control.Tasks
open Ids
open IdsExtensions

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    let fixtures =
        [ "Account"
          "Database"
          "Entity"
          "Instance"
          "Detail"
          "DetailValue"
          // not loading entities2details will results in an error
          // query-specified return tuple and crosstab function are not compatible
          "Entities2details" ]

    type UserFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module Cli =
    type OutputFormat =
        | JSON
        | ASCII

    let validate (format: OutputFormat) (_id: int) =async {
        let tableFun =
            match format with
            | ASCII -> Cli.tableString
            | JSON -> Cli.tableJson
        let reformatFun =
            match format with
            | ASCII -> id
            | JSON -> Utils.reformatJSON

        let! output = (tableFun (Locale.fromString "en-US") Client.client (EntityId.Validate(12, fun _ -> true ).Value))
        let! expectedRaw = File.ReadAllTextAsync(sprintf "cli/expected/%A_table_12" format)|> Async.AwaitTask
        let expected = expectedRaw |> reformatFun
        output |> should equal expected
    }

    [<Trait("Category", "cli")>]
    type CliTests() =

        [<Fact>]
        member __.ASCII_table_12() =
            validate ASCII 12

        [<Fact>]
        member __.JSON_table_12() =
            validate JSON 12

        interface IClassFixture<Fixtures.UserFixture>
