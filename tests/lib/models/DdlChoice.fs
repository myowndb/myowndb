namespace MyOwnDB.Tests.DdlChoice

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus


// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
           [ "Account"
             "Database"
             "Entity"
             "Instance"
             "Detail"
             "DateDetailValue"
             "IntegerDetailValue"
             "DetailValue"
             "DetailValueProposition"
             "DdlDetailValue"
             // not loading entities2details will results in an error
             // query-specified return tuple and crosstab function are not compatible
             "Entities2details"
             "Relation"
             "Link" ]
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module DdlChoice =
    [<Trait("Category", "models")>]
    [<Trait("Category", "ddlchoice")>]
    type LinkTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``ddl_choice_fns``() = async {
            do! DdlChoice.fromId Client.client (DetailId 62) (DetailValuePropositionId 12)
                |> Async.map (
                    DBResult.map (fun prop ->
                        (DdlChoice.toId prop,DdlChoice.toDetailId prop,DdlChoice.toString prop)
                    )
                )
                |> Async.map (checkOkDBResult [(12,62,"sa")])
        }