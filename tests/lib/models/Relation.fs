namespace MyOwnDB.Tests.Relation

open Xunit
open Ids
open MyOwnDB.Tests.Helpers
open FSharp.Control.Tasks
open FSharpPlus
open System

module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
            [ "Account"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module Relation =
    // to issue DBAction sql queries to database
    let issuer<'T> action = DBAction.run<'T> Client.client action

    [<Trait("Category", "database")>]
    [<Trait("Category", "relation")>]
    type RelationTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``entity_for_relation_test``() = async {

            // entity of child
            do! RelationExtensions.entityIdForRelationAction (ChildrenVia (newRelationId 5))
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (newEntityId 8)])

            // entity of parent
            do! RelationExtensions.entityIdForRelationAction (ParentsVia (newRelationId 5))
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (newEntityId 7)])

            // Error for non-existing relation
            do! RelationExtensions.entityIdForRelationAction (ParentsVia (newRelationId 29))
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])
        }

        [<Fact>]
        member __.``entity_relations``() = async {

            let entityCompany = newEntityId 7
            do! RelationExtensions.toChildrenForEntityAction entityCompany
                |> DBAction.mapToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [
                    (Utils.reformatJSON """
                        [
                        {
                            "id": {
                                "Case": "RelationId",
                                "Fields": [
                                    5
                                ]
                            },
                            "parent_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    7
                                ]
                            },
                            "child_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    8
                                ]
                            },
                            "parent_side_type_id": 1,
                            "child_side_type_id": 2,
                            "from_parent_to_child_name": "company_contacts",
                            "from_child_to_parent_name": "contact of",
                            "lock_version": 0
                        },
                        {
                            "id": {
                                "Case": "RelationId",
                                "Fields": [
                                    6
                                ]
                            },
                            "parent_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    7
                                ]
                            },
                            "child_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    9
                                ]
                            },
                            "parent_side_type_id": 1,
                            "child_side_type_id": 2,
                            "from_parent_to_child_name": "company_other_addresses",
                            "from_child_to_parent_name": "address of",
                            "lock_version": 0
                        }
                        ]
                    """)

                ])

            do! RelationExtensions.toChildrenForEntityAction (newEntityId 8)
                |> DBAction.mapToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [
                    (Utils.reformatJSON """
                        [
                        ]
                    """)

                ])

            do! RelationExtensions.toParentsForEntityAction (newEntityId 12)
                |> DBAction.mapToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [
                    (Utils.reformatJSON """
                        [
                            {
                                "id": {
                                "Case": "RelationId",
                                "Fields": [
                                    8
                                ]
                            },
                                "parent_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    19
                                ]
                            },
                                "child_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    12
                                ]
                            },
                                "parent_side_type_id": 1,
                                "child_side_type_id": 2,
                                "from_parent_to_child_name": "contact_de_visite",
                                "from_child_to_parent_name": "contact_de_visite",
                                "lock_version": 0
                            },
                            {
                                "id": {
                                "Case": "RelationId",
                                "Fields": [
                                    7
                                ]
                            },
                                "parent_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    11
                                ]
                            },
                                "child_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    12
                                ]
                            },
                                "parent_side_type_id": 2,
                                "child_side_type_id": 2,
                                "from_parent_to_child_name": "contact_de_la_societe",
                                "from_child_to_parent_name": "société de",
                                "lock_version": 0
                            },
                            {
                                "id": {
                                "Case": "RelationId",
                                "Fields": [
                                    9
                                ]
                            },
                                "parent_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    11
                                ]
                            },
                                "child_id": {
                                "Case": "EntityId",
                                "Fields": [
                                    12
                                ]
                            },
                                "parent_side_type_id": 1,
                                "child_side_type_id": 1,
                                "from_parent_to_child_name": "contact_prefere",
                                "from_child_to_parent_name": "contact prefere de",
                                "lock_version": 0
                            },
                            {
                              "id": {
                                "Case": "RelationId",
                                "Fields": [
                                  10
                                ]
                              },
                              "parent_id": {
                                "Case": "EntityId",
                                "Fields": [
                                  11
                                ]
                              },
                              "child_id": {
                                "Case": "EntityId",
                                "Fields": [
                                  12
                                ]
                              },
                              "parent_side_type_id": 2,
                              "child_side_type_id": 1,
                              "from_parent_to_child_name": "has CEO",
                              "from_child_to_parent_name": "is CEO at",
                              "lock_version": 0
                            }

                        ]
                    """)

                ])
            do! RelationExtensions.toParentsForEntityAction entityCompany
                |> DBAction.mapToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [
                    (Utils.reformatJSON """
                        [

                        ]
                    """)

                ])
        }
