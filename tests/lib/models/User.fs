namespace MyOwnDB.Tests.User

open System
open Xunit
open FsUnit.Xunit
open FSharp.Control.Tasks
open MyOwnDB.Tests.Helpers
open FSharpPlus
open Ids

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type UserFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client [| "Account+Account,lib"; "User+User,lib" |] |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module User =
    // to issue DBAction sql queries to database
    let issuer = DBAction.run Client.client
    // Get some records from database
    // these are functions, as this code is evaluated immediately, before the fixtures get loaded
    let longbob() = async {return! UserExtensions.byId 100003 |> issuer}

    let bob() = async {
        let! u = UserExtensions.byId 100001
                |> issuer
        return u |> DBResult.get
    }

    [<Trait("Category", "models")>]
    [<Trait("Model", "user")>]
    type UserTests() =

        [<Fact>]
        member __.``User authentication``() = async {
            let! bob = bob()
            let! r = UserExtensions.authenticateEmailAction "bob@test.com" "atest"
                    |> issuer
            r |> checkOkResult (bob)

            UserExtensions.authenticateEmailAction "nonbob@test.com" "atest"
            |> issuer
            |> Async.map (checkErrorResult [ "madb_tr Not found" ])
            |> ignore
        }

        [<Fact>]
        member __.``Call Email.isValid``() = Email.isValid "jon@example.com" |> should equal true

        interface IClassFixture<Fixtures.UserFixture>

        [<Fact>]
        member __.``user_get_all``() = async {
            let! r = UserExtensions.getAllForAccountAction 4
                    |> DBAction.map (fun u -> u.id )
                    |> DBAction.run Client.client
            r |> checkOkResult ( [UserId 100008; UserId 100009])

        }