namespace MyOwnDB.Tests.Entity

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
            [ "Account"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Entity =

    [<Trait("Category", "models")>]
    [<Trait("Category", "entity")>]
    type InstanceTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>


        [<Fact>]
        member __.``entity_byId``() = async{
            let entity17:Entity.Entity = { ``id`` = newEntityId 17;
                                            database_id = 6;
                                            name = "documentation";
                                            lock_version = None;
                                            has_public_form = false;
                                            has_public_data= None;
                                            public_to_all= None
            }

            do! EntityExtensions.byIdAction (newEntityId 17)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [entity17])

            // inexisting instance
            do! EntityExtensions.byIdAction (newEntityId 23982)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])
        }