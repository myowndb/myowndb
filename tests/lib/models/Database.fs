namespace MyOwnDB.Tests.DatabaseModel

open System
open Xunit
open FsUnit.Xunit
open FSharp.Control.Tasks
open MyOwnDB.Tests.Helpers
open FSharpPlus
open Ids

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client [| "Account+Account,lib"; "Database+Database,lib" |] |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }
        [<Fact>]
        member __.``database_byId``() = async{
            let database:Database.Database = { ``id`` = DatabaseId 3;
                                               account_id= AccountId 2
                                               lock_version = 0;
                                               name = "CRM1"; }

            do! DatabaseExtensions.byIdAction (DatabaseId 3)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [database])

            // inexisting database
            do! DatabaseExtensions.byIdAction (DatabaseId 2398)
                |> DBAction.map (fun db -> printfn "%A" db; db)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])
        }
