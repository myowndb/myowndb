namespace MyOwnDB.Tests.DetailValue

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus
open ServerFileAttachmentExtensions

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
            [ "Account"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module DetailValue =

    [<Trait("Category", "models")>]
    [<Trait("Category", "detail_value")>]
    type DetailTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>


        [<Fact>]
        member __.``FileAttachment_getJSON``() = async{
            let expected:ServerFileAttachment.FileAttachment =
              { detail_value_id= Some 1215
                s3_key= Some "1/6/101/202/1215"
                filetype= Some "image/png"
                filename= "logo-Ubuntu.png"
                uploaded= true
                indb= true
              }

            do! DBAction.querySingleAction "select * from detail_values where id=1215"
                |> DBAction.map (fun (dv:DetailValue.FileAttachmentDetailValue) -> dv.attachmentInfo())
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [expected])
        }

        [<Fact>]
        member __.``PathElementsFromRailsKeyInDB``() = async{
            let expected:PathElements =
              { account_id = 1
                database_id = 6
                entity_id = 101
                detail_value_id = 1215
              }


            do! DBAction.querySingleAction "select * from detail_values where id=1215"
                |> DBAction.map (fun (dv:DetailValue.FileAttachmentDetailValue) -> dv.attachmentInfo())
                |> DBAction.map (fun (info: ServerFileAttachment.FileAttachment) -> PathElements.FromKey (info.s3_key|>Option.get))
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [expected])
        }

        [<Fact>]
        member __.``PathElementsFromKey``() = async{
            let expected:PathElements =
              { account_id = 1
                database_id = 6
                entity_id = 101
                detail_value_id = 1215
              }
            // With instance id included, as in rails app version
            PathElements.FromKey "1/6/101/202/1215"
            |> should equal expected
            // Without instance id included, as in Fsharp version
            PathElements.FromKey "1/6/101/1215"
            |> should equal expected

            // Unexpected number of path elements should raise an exception
            (fun () -> PathElements.FromKey "1/6/101/202/1215/43"|>ignore)
            |> should (throwWithMessage (sprintf "failed parsing key in json for file attachment")) typeof<System.Exception>
            (fun () -> PathElements.FromKey "1/6/101"|>ignore)
            |> should (throwWithMessage (sprintf "failed parsing key in json for file attachment")) typeof<System.Exception>

        }