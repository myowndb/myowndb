namespace MyOwnDB.Tests.Detail

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes,DetailId see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
            [ "Account"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Detail =

    [<Trait("Category", "models")>]
    [<Trait("Category", "detail")>]
    type DetailTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>


        [<Fact>]
        member __.``Detail_getSpecAction``() = async{
            let detail84:DetailSpec = {
                detailId = Some (DetailId 84)
                name = "些 世 咹 水 晶"
                dataType =
                  { dataTypeId  = DataTypeId 2
                    name = "madb_long_text"
                    className = "LongTextDetailValue"
                  }

                status = DetailStatus.Active
                databaseId = newDatabaseId 6
                propositions = None
            }
            let detail36:DetailSpec = {
                detailId = Some (DetailId 36)
                name = "legal_status"
                dataType =
                  { dataTypeId  = DataTypeId 5
                    name = "madb_choose_in_list"
                    className = "DdlDetailValue"
                  }
                status = DetailStatus.Active
                databaseId = newDatabaseId 3
                propositions =
                    ( Some  [
                        { detailValuePropositionId = Some (DetailValuePropositionId 7)
                          value = "SPRL"
                        }
                        { detailValuePropositionId = Some (DetailValuePropositionId 8)
                          value = "SA"
                        }
                        { detailValuePropositionId = Some (DetailValuePropositionId 9)
                          value = "ASBL"
                        }
                        { detailValuePropositionId = Some (DetailValuePropositionId 10)
                          value = "Other"
                        }
                    ]
                )
            }
            do! DetailExtensions.getSpecAction (DetailId 84)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [detail84])

            do! DetailExtensions.getSpecAction (DetailId 36)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [detail36])

        }

        [<Fact>]
        member __.``Detail_get_linkable``() = async{
          let detail73:Detail.Detail= {
                id = DetailId 73
                name = "unlinked_detail"
                data_type_id  = DataTypeId 1
                database_id = newDatabaseId 8
                status_id = Some 1
                lock_version = 0
               }
          do!
             EntityExtensions.byIdAction (newEntityId 52)
             |>DBAction.bind DetailExtensions.linkableDetailsForEntityAction
             |> DBAction.run Client.client
             |> Async.map (checkOkDBResult [detail73])

          do!
            EntityExtensions.byIdAction (newEntityId 100)
            |> DBAction.bind (DetailExtensions.linkableDetailsForEntityAction )
            |> DBAction.map (fun detail -> detail.id)
            |> DBAction.run Client.client
            |> Async.map (checkOkDBResult [DetailId 48;DetailId 49;DetailId 50;DetailId 51;DetailId 52;DetailId 53;DetailId 54;DetailId 55;DetailId 56;DetailId 57;DetailId 58;DetailId 59;DetailId 60;DetailId 61;DetailId 62;DetailId 63;DetailId 81;DetailId 83;DetailId 84;DetailId 85;DetailId 87;DetailId 100000;DetailId 100001;DetailId 100002;DetailId 100003;DetailId 100004;DetailId 100005;DetailId 100006;DetailId 100007;DetailId 100008;DetailId 100009])

        }

        [<Fact>]
        member __.``Detail_get_specs_in_db``() = async{
          do!
             DetailExtensions.getDetailSpecsForDatabaseAction (AccountId 100) (newDatabaseId 5)
             |> DBAction.map (fun detailSpec -> detailSpec.detailId |> Option.get)
             |> DBAction.run Client.client
             |> Async.map (checkOkDBResult [DetailId 64; DetailId 65; DetailId 66; DetailId 67; DetailId 68; DetailId 69; DetailId 70; DetailId 71; DetailId 72])


        }
