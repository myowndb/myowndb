namespace MyOwnDB.Tests.Link

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus


// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
           [ "Account"
             "Database"
             "Entity"
             "Instance"
             "Detail"
             "DateDetailValue"
             "IntegerDetailValue"
             "DetailValue"
             "DetailValueProposition"
             "DdlDetailValue"
             // not loading entities2details will results in an error
             // query-specified return tuple and crosstab function are not compatible
             "Entities2details"
             "Relation"
             "Link" ]
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module Link =
    [<Trait("Category", "models")>]
    [<Trait("Category", "link")>]
    type LinkTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``Link 2 instances``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      links = 1L
                      // a,d it was newly inserted
                      maxLinkId = 1
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                                                [
                                                    // link inserted is for relation 5
                                                    1L, "select count(*) from links where relation_id=7"
                                                    // the link inserted is as we expect
                                                    1L, "select count(*) from links where relation_id=7 and parent_id=73 and child_id=87"
                                                ]
                                                expectedStateDifference
            // relation 7 links entities 11 >---< 12 (many-to-many)
            do! Link.linkInstancesAction (newRelationId 7) (newInstanceId 73) (newInstanceId 87)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ LinkId (preState.maxLinkId + 1) ])

            do! checkDifferenceNow ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``1 to 1 relation both exist``() = async {
            // relation 9, which is 1 to 1 in both directions  has 2 links defined:
            // id | parent_id | child_id | relation_id
            //----+-----------+----------+-------------
            // 52 |       77 |       82 |           9
            // 53 |       89 |       90 |           9

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            do! Link.linkInstancesAction (newRelationId 9) (newInstanceId 77) (newInstanceId 90)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                                List.length exns |> should equal 1
                                let e = List.head exns
                                let inner = e.InnerException
                                true |> should equal true
                                inner.Message |> should equal "One or more errors occurred. (P0001: child_side_type (one) not respected, parent has already a child);parent_side_type (one) not respected, child has already a parent);)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``1 to 1 relation parent exists``() = async {
            // relation 9, which is 1 to 1 in both directions  has 2 links defined:
            // id | parent_id | child_id | relation_id
            //----+-----------+----------+-------------
            // 52 |       77 |       82 |           9
            // 53 |       89 |       90 |           9

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            do! Link.linkInstancesAction (newRelationId 9) (newInstanceId 77) (newInstanceId 87)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                    List.length exns |> should equal 1
                    let e = List.head exns
                    let inner = e.InnerException
                    true |> should equal true
                    inner.Message |> should equal "One or more errors occurred. (P0001: child_side_type (one) not respected, parent has already a child);)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``many_to_1_relation_both_linked``() = async {
            // relation 10, which is many to 1
            // Relation named "has CEO" and "is CEO at"
            // Having these links:
            //  id | parent_id | child_id | relation_id
            // ----+-----------+----------+-------------
            //  42 |        93 |       92 |          10

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            do! Link.linkInstancesAction (newRelationId 10) (newInstanceId 93) (newInstanceId 92)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                    List.length exns |> should equal 1
                    let e = List.head exns
                    let inner = e.InnerException
                    true |> should equal true
                    inner.Message |> should equal "One or more errors occurred. (P0001: child_side_type (one) not respected, parent has already a child);)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``many_to_1_relation_parent_linked``() = async {
            // relation 10, which is many to 1
            // Relation named "has CEO" and "is CEO at"
            // Having these links:
            //  id | parent_id | child_id | relation_id
            // ----+-----------+----------+-------------
            //  42 |        93 |       92 |          10

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            do! Link.linkInstancesAction (newRelationId 10) (newInstanceId 93) (newInstanceId 70)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                    List.length exns |> should equal 1
                    let e = List.head exns
                    let inner = e.InnerException
                    true |> should equal true
                    inner.Message |> should equal "One or more errors occurred. (P0001: child_side_type (one) not respected, parent has already a child);)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``many_to_1_relation_child_linked``() = async {
            // relation 10, which is many to 1
            // Relation named "has CEO" and "is CEO at"
            // Having these links:
            //  id | parent_id | child_id | relation_id
            // ----+-----------+----------+-------------
            //  42 |        93 |       92 |          10

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      links = 1L
                      // a,d it was newly inserted
                      maxLinkId = 1
                }
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                                                [
                                                    // link inserted is for relation 5
                                                    1L, "select count(*) from links where relation_id=10"
                                                    // the link inserted is as we expect
                                                    1L, "select count(*) from links where relation_id=10 and parent_id=91 and child_id=92"
                                                ]
                                                expectedStateDifference


            do! Link.linkInstancesAction (newRelationId 10) (newInstanceId 91) (newInstanceId 92)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ LinkId (preState.maxLinkId + 1) ])

            do! checkDifferenceNow ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``one_to_many_relation_both_linked``() = async {
            // relation 8, which is 1 to many
            // Having these links:
            //  id | parent_id | child_id | relation_id
            // ----+-----------+----------+-------------
            //  51 |        95 |       81 |           8

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            do! Link.linkInstancesAction (newRelationId 8) (newInstanceId 95) (newInstanceId 81)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                    List.length exns |> should equal 1
                    let e = List.head exns
                    let inner = e.InnerException
                    true |> should equal true
                    inner.Message |> should equal "One or more errors occurred. (P0001: parent_side_type (one) not respected, child has already a parent);)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``one_to_many_relation_child_linked``() = async {
            // relation 8, which is 1 to many
            // Having these links:
            //  id | parent_id | child_id | relation_id
            // ----+-----------+----------+-------------
            //  51 |        95 |       81 |           8

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            do! Link.linkInstancesAction (newRelationId 8) (newInstanceId 99) (newInstanceId 81)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                    List.length exns |> should equal 1
                    let e = List.head exns
                    let inner = e.InnerException
                    true |> should equal true
                    inner.Message |> should equal "One or more errors occurred. (P0001: parent_side_type (one) not respected, child has already a parent);)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``one_to_many_relation_parent_linked``() = async {
          // relation 8, which is 1 to many
          // Having these links:
          //  id | parent_id | child_id | relation_id
          // ----+-----------+----------+-------------
          //  51 |        95 |       81 |           8

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      links = 1L
                      // a,d it was newly inserted
                      maxLinkId = 1
                }
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                                                [
                                                    // link inserted is for relation 5
                                                    1L, "select count(*) from links where relation_id=8"
                                                    // the link inserted is as we expect
                                                    1L, "select count(*) from links where relation_id=8 and child_id=70 and parent_id=95"
                                                ]
                                                expectedStateDifference


            do! Link.linkInstancesAction (newRelationId 8) (newInstanceId 95) (newInstanceId 70)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ LinkId (preState.maxLinkId + 1) ])

            do! checkDifferenceNow ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``1 to 1 relation child exists``() = async {
            // relation 9, which is 1 to 1 in both directions  has 2 links defined:
            // id | parent_id | child_id | relation_id
            //----+-----------+----------+-------------
            // 52 |       77 |       82 |           9
            // 53 |       89 |       90 |           9

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            do! Link.linkInstancesAction (newRelationId 9) (newInstanceId 88) (newInstanceId 82)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                                List.length exns |> should equal 1
                                let e = List.head exns
                                let inner = e.InnerException
                                true |> should equal true
                                inner.Message |> should equal "One or more errors occurred. (P0001: parent_side_type (one) not respected, child has already a parent);)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``link_child_id_of_bad_entity``() = async {
            // Relation 7, is many to many from parent entity 11 to child entity 12
            // Try to link a child that is not of entity 12

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            // Instance 95 is of entity 19, which is in the same database
            do! Link.linkInstancesAction (newRelationId 7) (newInstanceId 77) (newInstanceId 95)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                                List.length exns |> should equal 1
                                let e = List.head exns
                                let inner = e.InnerException
                                true |> should equal true
                                inner.Message |> should equal "One or more errors occurred. (P0001: child entity_id is not correct according to relation;)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``link_parent_id_of_bad_entity``() = async {
            // Relation 7, is many to many from parent entity 11 to child entity 12
            // Try to link a child that is not of entity 12

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            // Instance 100 is of entity 51
            do! Link.linkInstancesAction (newRelationId 7) (newInstanceId 100) (newInstanceId 86)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                                List.length exns |> should equal 1
                                let e = List.head exns
                                let inner = e.InnerException
                                true |> should equal true
                                inner.Message |> should equal "One or more errors occurred. (P0001: parent entity_id is not correct according to relation;)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``link_both_of_bad_entity``() = async {
            // Relation 7, is many to many from parent entity 11 to child entity 12
            // Try to link a child that is not of entity 12

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Do not use DBState functions: as the insertion will fail with an exception,
            // the transaction will be aborted, causing error when trying to get the DBState
            // for the second step of the comparison.

            // Instance 100 is of entity 51
            do! Link.linkInstancesAction (newRelationId 7) (newInstanceId 100) (newInstanceId 95)
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                |> Async.map (fun exns ->
                                List.length exns |> should equal 1
                                let e = List.head exns
                                let inner = e.InnerException
                                true |> should equal true
                                inner.Message |> should equal "One or more errors occurred. (P0001: parent entity_id is not correct according to relation;child entity_id is not correct according to relation;)"
                )
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``unlink_ok``() = async {
            // Relation 7, is many to many from parent entity 11 to child entity 12

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      links = -1L
                }
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                                                [
                                                    // link inserted is for relation 5
                                                    -1L, "select count(*) from links where relation_id=7"
                                                    // the link inserted is as we expect
                                                    -1L, "select count(*) from links where relation_id=7 and parent_id=69 and child_id=70"
                                                ]
                                                expectedStateDifference

            do! Link.unlinkInstancesAction (newRelationId 7) (newInstanceId 69) (newInstanceId 70)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [DBTypes.ImpactedRows 1])

            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``unlink_inexisting``() = async {
            // Relation 7, is many to many from parent entity 11 to child entity 12

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                                                [
                                                    // link inserted is for relation 5
                                                    0L, "select count(*) from links where relation_id=7"
                                                    0L, "select count(*) from links"
                                                ]
                                                DBState.zeroState

            do! Link.unlinkInstancesAction (newRelationId 7) (newInstanceId 69) (newInstanceId 344370)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [])

            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }