namespace MyOwnDB.Tests.DataTypeModel

open System
open Xunit
open FsUnit.Xunit
open FSharp.Control.Tasks
open MyOwnDB.Tests.Helpers
open FSharpPlus
open Ids

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module Client =
    let client = DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DataTypeFixture() =
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client [| |] |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }
module Datatype =
    [<Trait("Category", "models")>]
    [<Trait("Category", "datatype")>]
    type DataTypeTests() =
        interface IClassFixture<Fixtures.DataTypeFixture>

        [<Fact>]
        member __.``get_all_datatypes``() = async {
            do! DataTypeExtensions.GetAllAction()
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [
                  { id = DataTypeId 1
                    name = "madb_short_text"
                    class_name = "SimpleDetailValue" };
                  { id = DataTypeId 2
                    name = "madb_long_text"
                    class_name = "LongTextDetailValue" };

                  { id = DataTypeId 3
                    name = "madb_date"
                    class_name = "DateDetailValue" };
                  { id = DataTypeId 4
                    name = "madb_integer"
                    class_name = "IntegerDetailValue" };
                  { id = DataTypeId 5
                    name = "madb_choose_in_list"
                    class_name = "DdlDetailValue" };
                  { id = DataTypeId 6
                    name = "madb_email"
                    class_name = "EmailDetailValue" };
                  { id = DataTypeId 7
                    name = "madb_web_url"
                    class_name = "WebUrlDetailValue" };
                  { id = DataTypeId 8
                    name = "madb_file_attachment"
                    class_name = "FileAttachmentDetailValue" }
        ])
        }
