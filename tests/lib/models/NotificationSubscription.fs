namespace MyOwnDB.Tests.NotificationSubscription

open Xunit
open FsUnit.Xunit
open Specs
open MyOwnDB.Tests.Helpers
open FSharp.Control.Tasks
open FSharpPlus
open MyOwnDB.Tests.Utils
open NotificationSubscription
open NotificationSubscriptionCriteria

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
      DB.DBClient((AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString) + " Include Error Detail=true;")

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
            [ "Account"
              "User"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module NotificationSubscription =
    open FSharp.Data
    let societeEntityId = newEntityId 11
    let contactEntityId = newEntityId 12

    [<Trait("Category", "models")>]
    [<Trait("Category", "notification")>]
    type NotificationTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``notification_add_and_remove``() = async {
            // Setup SavePoint and rollback for this function
            let! rollbackDisposable = setSavePoint Client.client
            // Take state snapshot once for all tests not creating data
            let! preState, checkCreation =
                DBState.start
                  Client.client
                  {
                    DBState.zeroState with
                      notificationSubscriptions = 1L
                      maxNotificationSubscriptionId = 1
                  }
            let newSubscriptionId = (preState.maxNotificationSubscriptionId+1)

            do! NotificationSubscription.SubscribeInstanceCreationMailAction societeEntityId (Ids.UserId 100002)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [
                  {
                    id= Ids.NotificationSubscriptionId newSubscriptionId
                    event = AfterCreate
                    source_type= Instance
                    source_filter = JsonValue.Parse """{"entity_id": 11}"""
                    destination_type= User
                    destination= "100002"
                    protocol= Smtp
                  }

            ])
            do! checkCreation()

            let! _preState, checkdeletion =
                DBState.startWithCustomQueries
                  Client.client
                  [ -1L, "select count(*) from notification_subscriptions where destination='100002'"
                    0L , "select count(*) from notification_subscriptions where destination!='100002'"
                  ]
                  {
                    DBState.zeroState with
                      notificationSubscriptions = -1L
                  }
            do! NotificationSubscription.RemoveAction (Ids.NotificationSubscriptionId newSubscriptionId)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ DBTypes.ImpactedRows 1 ])
            do! checkdeletion()
            do! rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``notification_get_for_user``() = async {
            // Setup SavePoint and rollback for this function
            let! rollbackDisposable = setSavePoint Client.client
            // Take state snapshot once for all tests not creating data
            let! preState, checkCreation =
                DBState.start
                  Client.client
                  {
                    DBState.zeroState with
                      notificationSubscriptions = 1L
                      maxNotificationSubscriptionId = 1
                  }

            let userId=(Ids.UserId 100002)
            do! NotificationSubscription.SubscribeInstanceCreationMailAction societeEntityId userId
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1])
            do! checkCreation()

            // Check we retrieve it correctly
            do! GetUserSubscriptionsFor userId Instance (Entity societeEntityId)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [{
                      id = Ids.NotificationSubscriptionId (preState.maxNotificationSubscriptionId + 1)
                      event = AfterCreate
                      source_type = Instance
                      source_filter = JsonValue.Parse """{"entity_id": 11}"""
                      destination_type = User
                      destination = "100002"
                      protocol = Smtp }])

            // and check an empty result if no subscription
            do! GetUserSubscriptionsFor userId Instance (Entity contactEntityId)
                |> DBAction.run<NotificationSubscription.NotificationSubscription> Client.client
                |> Async.map (checkOkDBResult [])

            do! rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
        [<Fact>]
        member __.``notification_get``() = async {
            let! rollbackDisposable = setSavePoint Client.client

            // Take state snapshot once for all tests not creating data
            let! preState, checkCreation =
                DBState.start
                  Client.client
                  {
                    DBState.zeroState with
                      notificationSubscriptions = 3L
                      maxNotificationSubscriptionId = 3
                  }

            // At the time this is written, jsonb columns cannot be imported by fixture, so
            // we need to create the subscription programmatically.
            // On subscriber for societe creation
            do! NotificationSubscription.SubscribeInstanceCreationMailAction societeEntityId (Ids.UserId 100002)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1])

            // Two subscribers for contact creation
            do! NotificationSubscription.SubscribeInstanceCreationMailAction contactEntityId (Ids.UserId 100001)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1])
            do! NotificationSubscription.SubscribeInstanceCreationMailAction contactEntityId (Ids.UserId 100002)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1])
            do! checkCreation()

            // Check we retrieve the societe creation subscribers
            do!
              NotificationSubscription.GetAll AfterCreate Instance (Entity societeEntityId)
              |> DBAction.run Client.client
              |> Async.map
                (
                  checkOkDBResult
                    [{
                      id = Ids.NotificationSubscriptionId (preState.maxNotificationSubscriptionId + 1)
                      event = AfterCreate
                      source_type = Instance
                      source_filter = JsonValue.Parse """{"entity_id": 11}"""
                      destination_type = User
                      destination = "100002"
                      protocol = Smtp }
                    ])
            // Check we retrieve the contact creation subscribers
            do!
              NotificationSubscription.GetAll AfterCreate Instance (Entity contactEntityId)
              |> DBAction.run Client.client
              |> Async.map
                (
                  checkOkDBResult
                    [{
                      id = Ids.NotificationSubscriptionId (preState.maxNotificationSubscriptionId + 2)
                      event = AfterCreate
                      source_type = Instance
                      source_filter = JsonValue.Parse """{"entity_id": 12}"""
                      destination_type = User
                      destination = "100001"
                      protocol = Smtp };
                     {
                      id = Ids.NotificationSubscriptionId (preState.maxNotificationSubscriptionId + 3)
                      event = AfterCreate
                      source_type = Instance
                      source_filter = JsonValue.Parse """{"entity_id": 12}"""
                      destination_type = User
                      destination = "100002"
                      protocol = Smtp }
                    ])
            do! rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
