namespace MyOwnDB.Tests.Instance

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus
open Instance
open Specs

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
            [ "Account"
              "User"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Instance =
    let getDetail (id:int) = async{
        let! detailResult  = DBAction.querySingleAction "select * from details where id = %d" id
                                    |> DBAction.run<Detail.Detail> Client.client
        return detailResult |> DBResult.getFirst
    }

    [<Trait("Category", "models")>]
    [<Trait("Category", "instance")>]
    type InstanceTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``Creating instances with simple values details``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 12)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 48 // nom
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "John")) } ] }
                        { detailId = DetailId 58 // service
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "Finance")) } ] } ] }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      instances = 1L
                      // a,d it was newly inserted
                      maxInstanceId = 1
                      // 2 detail values inserted?
                      detailValues = 2L
                      maxDetailValueId = 2 }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from detail_values where detail_id=48"
                      1L, "select count(*) from detail_values where detail_id=58" ] expectedStateDifference

            // easier names to use in specResult building
            let instanceId = preState.maxInstanceId
            let detailValueId = preState.maxDetailValueId

            // spec we expect as result
            let specResult: Specs.InstanceSpec =
                Lens.With <@ spec.instanceId @> (Some(newInstanceId (instanceId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 2)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[1].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 1)))

            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                |> Async.map (checkOkResult [ specResult ])

            // check the right value was inserted in the database
            do! specResult.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                |> Async.map (checkOkDBResult [ "John" ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // Check detail_values are inserted correctly, incl type
            do! specResult.detailValues.[0].values.[0].detailValueId
                |> checkDetailValuetype Client.client "SimpleDetailValue"

            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // Fiction
            let newName = "Jean"

            let specUpdate =
                Lens.With <@ specResult.detailValues.[0].values.[0].value @> (ServerValidated (ServerValue.Simple (Some newName)))

            // create instance for spec
            let instanceUpdateResult =
                Instance.createOrUpdateAction specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceUpdateResult
                        |> Async.map(checkOkResult [ specUpdate ])

            // check the right value was updated in the database
            let! () = specUpdate.detailValues.[0].values.[0].detailValueId
                        |> Option.get
                        |> DetailValueIdType.get
                        |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                        |> Async.map (checkOkDBResult [ newName ])

            do! checkUpdateDifference ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``Creating_instances_with_long_detail_values``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 100001)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 100002 // nom
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.LongText (Some "This is a long text note added to the new instance")) } ] }
                         ] }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      instances = 1L
                      // a,d it was newly inserted
                      maxInstanceId = 1
                      // 2 detail values inserted?
                      detailValues = 1L
                      maxDetailValueId = 1 }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from detail_values where detail_id=100002 and type='LongTextDetailValue'"
                    ]
                    expectedStateDifference

            // easier names to use in specResult building
            let instanceId = preState.maxInstanceId
            let detailValueId = preState.maxDetailValueId

            // spec we expect as result
            let specResult: Specs.InstanceSpec =
                Lens.With <@ spec.instanceId @> (Some(newInstanceId (instanceId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 1)))

            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                |> Async.map (checkOkResult [ specResult ])

            // check the right value was inserted in the database
            do! specResult.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                |> Async.map (checkOkDBResult [ "This is a long text note added to the new instance" ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // Check detail_values are inserted correctly, incl type
            do! specResult.detailValues.[0].values.[0].detailValueId
                |> checkDetailValuetype Client.client "LongTextDetailValue"

            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // Fiction
            let newNote = "New Note"

            let specUpdate =
                Lens.With <@ specResult.detailValues.[0].values.[0].value @> (ServerValidated (ServerValue.LongText (Some newNote)))

            // create instance for spec
            let instanceUpdateResult =
                Instance.createOrUpdateAction specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceUpdateResult
                        |> Async.map(checkOkResult [ specUpdate ])

            // check the right value was updated in the database
            let! () = specUpdate.detailValues.[0].values.[0].detailValueId
                        |> Option.get
                        |> DetailValueIdType.get
                        |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                        |> Async.map (checkOkDBResult [ newNote ])

            do! checkUpdateDifference ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``Creating_instances_with_file_attachments``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Configuration variables
            // Store our runtime files under "run/"
            let baseDir = "run"
            // We will test with entity belonging to this account
            let accountId = 1
            // Config keys to pass to DBClient so it is available inside DBActions
            let attachmentsKey = "myowndb:fileAttachments:path"
            let waitRoomKey = "myowndb:fileAttachments:waitRoom"
            // Subdirs of basedir where we wtore files
            let waitRoom = Path.Combine(baseDir,"waitRoom")
            let attachments =  Path.Combine(baseDir,"attachments")
            Client.client.SetConfigItem(attachmentsKey,attachments)
            Client.client.SetConfigItem(waitRoomKey,waitRoom)

            // Define the upload id of the files that were supposedly uploaded
            let logoId = "65134962349313232"
            let pictureId = "56644782123984252"
            // Define uploaded files' respectove paths in the waitroom
            let logoWaitRoom = Path.Combine(waitRoom,string accountId,logoId)
            let pictureWaitRoom = Path.Combine(waitRoom,string accountId,pictureId)
            // Delete possibly existing waiting files and attachments. Useful for local runs of tests
            [logoWaitRoom;pictureWaitRoom; Path.Combine(attachments,"1/6/100001");]
            |> List.iter (fun dir ->
                if Directory.Exists dir then Directory.Delete(dir,true) |> ignore
            )

            // Create the waitRoom directory if needed
            Directory.CreateDirectory(waitRoom) |> ignore

            // Location of files that are supposed to have been uploaded
            let logoSource = "fixtures/files/logo-Ubuntu.png"
            let pictureSource = "fixtures/files/file.bin"
            let picture2Source = "fixtures/files/file2.bin"

            // Copy files to the waitRoom, where file are placed when they are uploaded
            // in the first step of form submission.
            Directory.CreateDirectory(Path.Combine(waitRoom,string accountId, logoId))|> ignore
            File.Copy(logoSource, Path.Combine(waitRoom,string accountId, $"{logoId}/logo-Ubuntu.png"),true)
            Directory.CreateDirectory(Path.Combine(waitRoom,string accountId, pictureId))|>ignore
            File.Copy(pictureSource, Path.Combine(waitRoom,string accountId, $"{pictureId}/file.bin"),true)

            // These are the location where we expect the attachments to be saved
            let logoDestination = Path.Combine(attachments,string accountId, "6/100001", string 200002)
            let pictureDestination = Path.Combine(attachments,string accountId, "6/100001", string 200001)

            // spec for the instance to be inserted
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 100001)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 100008 // logo
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.FileAttachment
                                                            (Some {
                                                                    detail_value_id= None
                                                                    s3_key= Some logoId
                                                                    filetype= Some "image/png"
                                                                    filename= "logo-Ubuntu.png"
                                                                    uploaded= true
                                                                    indb= false
                                                                }
                                                            )
                                                          )
                                }
                              ]
                        }
                        { detailId = DetailId 100009 // picture
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.FileAttachment
                                                            (Some {
                                                                    detail_value_id= None
                                                                    s3_key= Some pictureId
                                                                    filetype= Some "application/octet-stream"
                                                                    filename= "file.bin"
                                                                    uploaded= true
                                                                    indb= false
                                                                }
                                                            )
                                                          )
                                }
                              ]
                        }



                        ] }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      instances = 1L
                      // a,d it was newly inserted
                      maxInstanceId = 1
                      // 2 detail values inserted?
                      detailValues = 2L
                      maxDetailValueId = 2 }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from detail_values where detail_id=100008"
                      1L, "select count(*) from detail_values where detail_id=100009" ] expectedStateDifference

            // easier names to use in specResult building
            let instanceId = preState.maxInstanceId
            let detailValueId = preState.maxDetailValueId

            // spec we expect as result
            let specResult: Specs.InstanceSpec =
                Lens.With <@ spec.instanceId @> (Some(newInstanceId (instanceId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 2)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].value @>)
                    (ServerValidated
                        (ServerValue.FileAttachment
                                        (Some { detail_value_id = Some 200002
                                                s3_key = Some "1/6/100001/200002"
                                                filetype = Some "image/png"
                                                filename = "logo-Ubuntu.png"
                                                uploaded = true
                                                indb = true }))
                    )
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[1].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[1].values.[0].value @>)
                    (ServerValidated
                        (ServerValue.FileAttachment
                                        (Some { detail_value_id = Some 200001
                                                s3_key = Some "1/6/100001/200001"
                                                filetype = Some "application/octet-stream"
                                                filename = "file.bin"
                                                uploaded = true
                                                indb = true }))
                    )
            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                |> Async.map (checkOkResult [ specResult ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // Check detail_values are inserted correctly, incl type
            do! specResult.detailValues.[0].values.[0].detailValueId
                |> checkDetailValuetype Client.client "FileAttachmentDetailValue"

            // Check the right files are copied at the right place
            do! checkFileHash(logoDestination, logoSource)|> Async.AwaitTask
            do! checkFileHash(pictureDestination, pictureSource)|> Async.AwaitTask


            // --------------------------------------------------------------------------------
            // Updating a detail value with an existing file should not work. We should never overwrite
            // an existing attached file. The user first has to delete the existing attachment (and its related
            // detail_value) before a new one can be uploaded

            // Define a new upload id for picture
            let pictureId = string (Random().NextInt64())
            // Copy file to waitRoom
            Directory.CreateDirectory(Path.Combine(waitRoom,string accountId, pictureId))|>ignore
            File.Copy(pictureSource, Path.Combine(waitRoom,string accountId, $"{pictureId}/file2.bin"),true)

            // Define the spec for the update of a detail value with a file attached.
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 100001)
                  instanceId = (Some(newInstanceId (instanceId + 1)))
                  detailValues =
                      [ { detailId = DetailId 100009 // picture
                          values =
                              [ { detailValueId = Some (DetailValueId 200001)
                                  value = ServerValidated (ServerValue.FileAttachment
                                                            (Some {
                                                                    detail_value_id= Some 200001
                                                                    s3_key= Some pictureId
                                                                    filetype= Some "application/octet-stream"
                                                                    filename= "file2.bin"
                                                                    uploaded= true
                                                                    indb= false
                                                                }
                                                            )
                                                          )
                                }
                              ]
                        }
                      ]
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 0L, "select count(*) from detail_values where detail_id=100008"
                      0L, "select count(*) from detail_values where detail_id=100009" ] DBState.zeroState

            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            try
                do! instanceActionResult |> Async.Ignore
            with
            | e ->
                // We can't match a regex, so we match start and end because in the middle we have
                // the path dependent on the run environment.
                e.Message |> should  startWith "The file '"
                e.Message |> should  endWith "/run/attachments/1/6/100001/200001' already exists."

            // Now check nothing was added in the database
            do! checkDifferenceNow ()

            // Check the existing file hashes are still the same, confirming no file was overwritten.
            do! checkFileHash(logoDestination, logoSource)|> Async.AwaitTask
            do! checkFileHash(pictureDestination, pictureSource)|> Async.AwaitTask

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }


        [<Fact>]
        member __.``Creating instances with simple details having multiple values``() = async {

            // Setup SavePoint and rollback for this function
            let!_rollbackDisposable = setSavePoint Client.client

            // spec for instance to be inserted
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 12)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 48 // nom
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "Francis") )}
                                { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "Frank")) } ] }
                        { detailId = DetailId 58 // service
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "IT")) }
                                { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "Board")) } ] } ] }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      instances = 1L
                      // a,d it was newly inserted
                      maxInstanceId = 1
                      // 2 detail values inserted?
                      detailValues = 4L
                      maxDetailValueId = 4 }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 2L, "select count(*) from detail_values where detail_id=48"
                      2L, "select count(*) from detail_values where detail_id=58" ] expectedStateDifference
            // easier names to use in specResult building
            let instanceId = preState.maxInstanceId
            let detailValueId = preState.maxDetailValueId

            // spec expected as result
            let specResult: Specs.InstanceSpec =
                Lens.With <@ spec.instanceId @> (Some(newInstanceId(instanceId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 4)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[1].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 3)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[1].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 2)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[1].values.[1].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 1)))

            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                   |> Async.map (checkOkResult [ specResult ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``Handle entities with no detail in list view``() = async {
            do! Instance.selectDataTableAction (newEntityId 51) Instance.defaultListClause
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (DBResult.toSingleResult)
                |> Async.map (checkOkResult "[]")
        }



        [<Fact>]
        member __.``Creating instances with date_detail_value``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let date =
                DateAndTime.fromString "2020-05-17"
                |> function
                | Ok url -> url
                | Error es ->
                    failwithf "unexpected error parsing valid url: %s" (es|>Error.toString)

            // spec for instance to be inserted
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 100)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 76 // published
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.DateAndTime (Some date)) } ] } ] }

            let expectedStateDifference =
                { DBState.zeroState with
                      instances = 1L
                      maxInstanceId = 1
                      maxDateDetailValueId = 1
                      dateDetailValues = 1L }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from date_detail_values where detail_id=76" ] expectedStateDifference
            // easier names to use in specResult building
            let instanceId = preState.maxInstanceId
            let dateDetailValueId = preState.maxDateDetailValueId

            // spec expected as result
            let specResult =
                Lens.With <@ spec.instanceId @> (Some(newInstanceId(instanceId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       (Some(DateDetailValueId(dateDetailValueId + 1)))
            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                |> Async.map (checkOkResult [ specResult ])

            // check the right value was inserted in the database
            do! specResult.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from date_detail_values where id = %d"
                |> Async.map (checkOkDBResult [ (DateTime.Parse "2020-05-17") ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // Fiction
            let newDate =
                DateAndTime.fromString "2020-06-19"
                |> function
                | Ok url -> url
                | Error es ->
                    failwithf "unexpected error parsing valid url: %s" (es|>Error.toString)

            let specUpdate =
                Lens.With <@ specResult.detailValues.[0].values.[0].value @> (ServerValidated (ServerValue.DateAndTime (Some newDate)))

            // create instance for spec
            let instanceUpdateResult =
                Instance.createOrUpdateAction specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceUpdateResult
                |> Async.map (checkOkResult [ specUpdate ])

            // check the right value was updated in the database
            do! specUpdate.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from date_detail_values where id = %d"
                |> Async.map (checkOkDBResult [ (DateAndTime.get newDate) ])

            do! checkUpdateDifference ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``Creating instances with email detail_value``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for instance to be inserted
            let jimDoeEmail =
                (Email.fromString "jim.doe@example.com")
                |> function
                | Ok email -> email
                | Error es ->
                    failwithf "unexpected error parsing valid email address: %s"
                        (Error.toString es)

            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 12)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 48 // name
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "Jim")) } ] }
                        { detailId = DetailId 56 // first name
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "Doe")) } ] }
                        { detailId = DetailId 63 // company_email
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Email (Some jimDoeEmail)) } ] } ] }

            let expectedStateDifference =
                { DBState.zeroState with
                      instances = 1L
                      maxInstanceId = 1
                      detailValues = 3L
                      maxDetailValueId = 3 }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from detail_values where detail_id=48"
                      1L, "select count(*) from detail_values where detail_id=56"
                      1L, "select count(*) from detail_values where detail_id=63"
                      1L, "select count(*) from detail_values where type='EmailDetailValue'"
                      0L, "select count(*) from detail_values where detail_id not in (48,56,63)" ]
                    expectedStateDifference
            // easier names to use in specResult building
            let instanceId = preState.maxInstanceId
            let detailValueId = preState.maxDetailValueId

            // spec expected as result
            let specResult =
                Lens.With <@ spec.instanceId @> (Some(newInstanceId(instanceId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 3)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[1].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 2)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[2].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 1)))
            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                |> Async.map (checkOkResult [ specResult ])

            // check the right value was inserted in the database
            do! specResult.detailValues.[2].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                |> Async.map (checkOkDBResult [ (jimDoeEmail |> Email.toString) ])
            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // Fiction
            let newEmail =
                Email.fromString "jimmy@example.net"
                |> function
                | Ok url -> url
                | Error es ->
                    failwithf "unexpected error parsing valid url: %s" (Error.toString es)

            let specUpdate =
                Lens.With <@ specResult.detailValues.[2].values.[0].value @> (ServerValidated (ServerValue.Email (Some newEmail)))

            // create instance for spec
            let instanceUpdateResult =
                Instance.createOrUpdateAction specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceUpdateResult
                |> Async.map (checkOkResult [ specUpdate ])

            // check the right value was updated in the database
            do! specUpdate.detailValues.[2].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                |> Async.map (checkOkDBResult [ (newEmail |> Email.toString) ])

            do! checkUpdateDifference ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.Creating_instances_with_weburl_detail_value() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for instance to be inserted
            let myOwnDBURL =
                (WebURL.fromString "http://www.myowndb.com")
                |> function
                | Ok url -> url
                | Error es ->
                    failwithf "unexpected error parsing valid url: %s" (Error.toString es)

            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 30)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 71 // short_text
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "Short text value")) } ] }
                        { detailId = DetailId 69 // web_url
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.WebURL (Some myOwnDBURL)) } ] } ] }

            let expectedStateDifference =
                { DBState.zeroState with
                      instances = 1L
                      maxInstanceId = 1
                      detailValues = 2L
                      maxDetailValueId = 2 }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from detail_values where detail_id=69"
                      1L, "select count(*) from detail_values where detail_id=71"
                      1L, "select count(*) from detail_values where type='WebUrlDetailValue'"
                      0L, "select count(*) from detail_values where detail_id not in (69,71)" ] expectedStateDifference
            // easier names to use in specResult building
            let instanceId = preState.maxInstanceId
            let detailValueId = preState.maxDetailValueId

            // spec expected as result
            let specResult =
                Lens.With <@ spec.instanceId @> (Some(newInstanceId(instanceId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 2)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[1].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 1)))
            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                |> Async.map (checkOkResult [ specResult ])

            // check the right value was inserted in the database
            do! specResult.detailValues.[1].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                |> Async.map (checkOkDBResult [ (myOwnDBURL |> WebURL.toString) ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // Fiction
            let newUrl =
                WebURL.fromString "http://www.madb.net"
                |> function
                | Ok url -> url
                | Error es ->
                    failwithf "unexpected error parsing valid url: %s" (Error.toString es)

            let specUpdate =
                Lens.With <@ specResult.detailValues.[0].values.[0].value @> (ServerValidated (ServerValue.WebURL (Some newUrl)))

            // create instance for spec
            let instanceUpdateResult =
                Instance.createOrUpdateAction specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceUpdateResult
                |> Async.map (checkOkResult [ specUpdate ])

            // check the right value was updated in the database
            do! specUpdate.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                |> Async.map (checkOkDBResult [ (newUrl |> WebURL.toString) ])

            do! checkUpdateDifference ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.Creating_and_updating_instances_with_ddl_value() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for instance to be inserted
            let! choice =
                DdlChoice.fromId Client.client (DetailId 78) (DetailValuePropositionId 1005)
                |> Async.map (DBResult.getFirst)

            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 100)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 78 // ddl Category
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.DdlValueChoice (Some choice)) } ] }
                        { detailId = DetailId 74 // name
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "John")) } ] } ] }

            let expectedStateDifference =
                { DBState.zeroState with
                      instances = 1L
                      maxInstanceId = 1
                      detailValues = 1L
                      ddlDetailValues = 1L
                      maxDetailValueId = 1
                      maxDdlDetailValueId = 1 }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from detail_values where detail_id=74"
                      1L, "select count(*) from ddl_detail_values where detail_id=78"
                      0L, "select count(*) from detail_values where detail_id not in (74)"
                      0L, "select count(*) from ddl_detail_values where detail_id not in (78)" ] expectedStateDifference
            // easier names to use in specResult building
            let instanceId = preState.maxInstanceId
            let detailValueId = preState.maxDetailValueId
            let ddlDetailValueId = preState.maxDdlDetailValueId

            // spec expected as result
            let specResult =
                Lens.With <@ spec.instanceId @> (Some(newInstanceId(instanceId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       (Some(DdlDetailValueId(ddlDetailValueId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[1].values.[0].detailValueId @>)
                       (Some(DetailValueId(detailValueId + 1)))
            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                |> Async.map (checkOkResult [ specResult ])

            // check inserted values in db
            do! specResult.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client
                       "select detail_value_proposition_id from ddl_detail_values where id = %d"
                |> Async.map (checkOkDBResult [ 1005 ])

            do! specResult.detailValues.[1].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                |> Async.map (checkOkDBResult [ "John" ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // Fiction
            let! newChoice =
                DdlChoice.fromId Client.client (DetailId 78) (DetailValuePropositionId 1003)
                |> Async.map (DBResult.getFirst)

            let specUpdate =
                Lens.With <@ specResult.detailValues.[0].values.[0].value @> (ServerValidated (ServerValue.DdlValueChoice (Some newChoice)))

            // create instance for spec
            let instanceUpdateResult =
                Instance.createOrUpdateAction specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceUpdateResult
                |> Async.map (checkOkResult [ specUpdate ])

            // check the right value was updated in the database
            do! specUpdate.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client
                       "select detail_value_proposition_id from ddl_detail_values where id = %d"
                |> Async.map (checkOkDBResult [ (newChoice |> DdlChoice.toId) ])

            do! checkUpdateDifference ()


            // update with no choice for ddl
            //---------------------------------------------------------------------------------

            let expectedStateDifference =
                { DBState.zeroState with
                      ddlDetailValues = -1L
                }
            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] expectedStateDifference

            // This is the spec requesting the update.
            // the value of the ddl detail is set to None, but the DetailValueId is left as is for the delete to be possible
            let specUpdate =
                Lens.With <@ specResult.detailValues.[0].values.[0].value @> (ServerValidated (ServerValue.DdlValueChoice None))
            // This is the spec that we get after the deletion occured, the DetailValueId is set to None
            // It is important to start from specResult and do the Lens.BInd for the second change!
            // Doing only one Lens.With applied on specUpdate makes the comparison below failn even though both values are equal....
            let specAfterUpdate =
                Lens.With <@ specResult.detailValues.[0].values.[0].value @> (ServerValidated (ServerValue.DdlValueChoice None))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       None

            // create instance for spec
            let instanceUpdateResult =
                Instance.createOrUpdateAction specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceUpdateResult
                |> Async.map (checkOkResult [ specAfterUpdate ])

            do! Async.Sleep 1000
            do! checkUpdateDifference ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``Creating instances with integer_detail_value``() = async{

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 100)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 77 // pages
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Integer (Some 354L)) } ] } ] }

            let expectedStateDifference =
                { DBState.zeroState with
                      instances = 1L
                      maxInstanceId = 1
                      maxIntegerDetailValueId = 1
                      integerDetailValues = 1L }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from integer_detail_values where detail_id=77" ] expectedStateDifference
            // easier names to use in specResult building
            let instanceId = preState.maxInstanceId
            let integerDetailValueId = preState.maxIntegerDetailValueId

            // spec expected as result
            let specResult =
                Lens.With <@ spec.instanceId @> (Some(newInstanceId(instanceId + 1)))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[0].values.[0].detailValueId @>)
                       (Some(IntegerDetailValueId(integerDetailValueId + 1)))
            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                |> Async.map (checkOkResult [ specResult ])

            // check the right value was inserted in the database
            do! specResult.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from integer_detail_values where id = %d"
                |> Async.map (checkOkDBResult [ 354L ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            let newValue = 374L

            let specUpdate =
                Lens.With <@ specResult.detailValues.[0].values.[0].value @> (ServerValidated (ServerValue.Integer (Some newValue)))

            // create instance for spec
            let instanceUpdateResult =
                Instance.createOrUpdateAction specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceUpdateResult
                |> Async.map (checkOkResult [ specUpdate ])

            // check the right value was inserted in the database
            do! specUpdate.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from integer_detail_values where id = %d"
                |> Async.map (checkOkDBResult [ newValue ])

            do! checkUpdateDifference ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
        [<Fact>]
        member __.Update_instances_detail_value_for_wrong_detail_id() = async{

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let! asciiTablePre =
                Cli.tableString (Locale.fromString "en-US") Client.client (newEntityId 12)

            // spec for the instance to be inserted
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 12)
                  instanceId = Some(newInstanceId 92)
                  detailValues =
                      [ { detailId = DetailId 56 // nom
                          values =
                              [ { detailValueId = Some(DetailValueId 100080)
                                  value = ServerValidated (ServerValue.Simple (Some "Griet")) }
                                // this detail_value is not for detail id 56, but 48!
                                // this update should thus be rejected!
                                { detailValueId = Some(DetailValueId 100079)
                                  value = ServerValidated (ServerValue.Simple (Some "Boz")) } ] }
                        { detailId = DetailId 63
                          values =
                              [ { detailValueId = Some(DetailValueId 100084)
                                  value = ServerValidated (ServerValue.Simple (Some "Griet.Boz@bozzer-enterprise.com")) } ] } ] }

            let expectedStateDifference = DBState.zeroState

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 0L, "select count(*) from detail_values where detail_id=56"
                      0L, "select count(*) from detail_values where detail_id!=56" ] expectedStateDifference

            // spec we expect as result
            let specResult: Specs.InstanceSpec = spec

            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult
                |> Async.map (checkErrorResult
                    [ "empty result, but required (failed update? forgot to include 'RETURNING id' in query?), for query update detail_values set value=%s where detail_id = %d and id= %d RETURNING id with parameters [\"Boz\"; 56; 100079]" ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            let! asciiTable =
                Cli.tableString (Locale.fromString "en-US") Client.client (newEntityId 12)

            asciiTable |> should equal asciiTablePre
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.Update_instances_with_simple_and_email_values_details() = async{

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be Updated. We update the firstname and the email.
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 12)
                  instanceId = Some(newInstanceId 92)
                  detailValues =
                      [ { detailId = DetailId 56 // nom
                          values =
                              [ { detailValueId = Some(DetailValueId 100080)
                                  value = ServerValidated (ServerValue.Simple (Some "Griet")) } ] }
                        { detailId = DetailId 63
                          values =
                              [ { detailValueId = Some(DetailValueId 100084)
                                  value = ServerValidated (ServerValue.Simple (Some "Griet58@gmail.com")) } ] } ] }

            let expectedStateDifference = DBState.zeroState

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client [ 0L, "select count(*) from detail_values " ]
                    expectedStateDifference

            // spec we expect as result
            let specResult: Specs.InstanceSpec = spec

            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            do! instanceActionResult |> Async.map (checkOkDBResult [ spec ])

            // check the right value was inserted in the database
            do! specResult.detailValues.[0].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                |> Async.map (checkOkDBResult [ "Griet" ])

            do! specResult.detailValues.[1].values.[0].detailValueId
                |> Option.get
                |> DetailValueIdType.get
                |> DBAction.immediate Client.client "select value from detail_values where id = %d"
                |> Async.map (checkOkDBResult [ "Griet58@gmail.com" ])

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            let! asciiTable =
                Cli.tableString (Locale.fromString "en-US") Client.client (newEntityId 12)

            let! expectedAsciiTable = File.ReadAllTextAsync("models/expected/InstanceWithCrosstabValidation1.txt") |> Async.AwaitTask

            asciiTable |> should equal expectedAsciiTable
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.delete_simple_and_email_values_details() = async{

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be Updated. We delete the firstname and the email.
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 12)
                  instanceId = Some(newInstanceId 92)
                  detailValues =
                      [ { detailId = DetailId 56 // nom
                          values =
                              [ { detailValueId = Some(DetailValueId 100080)
                                  value = ServerValidated (ServerValue.Simple None) } ] }
                        { detailId = DetailId 63
                          values =
                              [ { detailValueId = Some(DetailValueId 100084)
                                  value = ServerValidated (ServerValue.Simple None) } ] } ] }

            let expectedStateDifference =
                { DBState.zeroState with
                    detailValues = -2L
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ -1L, "select count(*) from detail_values where id = 100080"
                      -1L, "select count(*) from detail_values where id = 100084" ]
                    expectedStateDifference

            // spec we expect as result
            let specResult: Specs.InstanceSpec =
                { entityId = (newEntityId 12)
                  instanceId = Some(newInstanceId 92)
                  detailValues =
                      [ { detailId = DetailId 56 // nom
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple None) } ] }
                        { detailId = DetailId 63
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple None) } ] } ] }


            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // We get the updated spec as expected
            do! instanceActionResult |> Async.map (checkOkDBResult [ specResult ])

            // Now check what was removed from the database
            // get counters after insert
            do! checkDifferenceNow ()

            let! asciiTable =
                Cli.tableString (Locale.fromString "en-US") Client.client (newEntityId 12)

            let! expectedAsciiTable = File.ReadAllTextAsync("models/expected/InstanceWithCrosstabValidation1AfterFirstnameDelete.txt") |> Async.AwaitTask

            asciiTable |> should equal expectedAsciiTable
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.delete_integer_and_ddl_values_details() = async{

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be Updated. We delete the firstname and the email.
            let spec: Specs.InstanceSpec =
                {  entityId = newEntityId 11
                   instanceId = Some (newInstanceId 69)
                   detailValues =
                        // no value for nom, company_email
                        // status sprl
                        [{ detailId = DetailId 62
                           values = [{ detailValueId = Some (DdlDetailValueId 15)
                                       value = ServerValidated (ServerValue.DdlValueChoice None) }] };
                         // personnes_occupees
                         { detailId = DetailId 51
                           values = [{ detailValueId = Some (IntegerDetailValueId 5)
                                       value = ServerValidated (ServerValue.Integer None) }] };
                        ]
                }

            let expectedStateDifference =
                { DBState.zeroState with
                    integerDetailValues = -1L
                    ddlDetailValues = -1l
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ -1L, "select count(*) from integer_detail_values where id = 5"
                      -1L, "select count(*) from ddl_detail_values where id = 15" ]
                    expectedStateDifference

            // spec we expect as result
            let specResult: Specs.InstanceSpec =
               {  entityId = newEntityId 11
                  instanceId = Some (newInstanceId 69)
                  detailValues =
                        // no value for nom, company_email
                        // status sprl
                        [{ detailId = DetailId 62
                           values = [{ detailValueId = None
                                       value = ServerValidated (ServerValue.DdlValueChoice None) }] };
                         // personnes_occupees
                         { detailId = DetailId 51
                           values = [{ detailValueId = None
                                       value = ServerValidated (ServerValue.Integer None) }] };
                        ]
                }


            // create instance for spec
            let instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // We get the updated spec as expected
            do! instanceActionResult |> Async.map (checkOkDBResult [ specResult ])

            // Now check what was removed from the database
            // get counters after insert
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``Instance_links``() = async{

            // Children via one relation
            do! Instance.linksAction (newInstanceId 77) (ChildrenVia (newRelationId 7))
                |> DBAction.map (fun r -> r.child_id)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [81;82;83;84])

            // Children via another relation
            do! Instance.linksAction (newInstanceId 77) (ChildrenVia (newRelationId 9))
                |> DBAction.map (fun r -> r.child_id)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 82 ])

            // Parents via a relation
            do! Instance.linksAction (newInstanceId 81) (ParentsVia (newRelationId 8))
                |> DBAction.map (fun r -> r.parent_id)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 95 ])

            // Parents via another relation
            do! Instance.linksAction (newInstanceId 81) (ParentsVia (newRelationId 7))
                |> DBAction.map (fun r -> r.parent_id)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 77 ])
        }

        [<Fact>]
        member __.``select_datatable_action``() = async{
            do! Instance.selectDataTableAction (newEntityId 12) Instance.defaultListClause
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_12.json"])

            // Entity with no detail in list view
            // This entity 50 has no instance, but this test is still relevant to check this doesn't fail when
            // no detail is displayed in the list view.
            do! Instance.selectDataTableAction (newEntityId 50) Instance.defaultListClause
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [(Utils.reformatJSON " [] ") ])

            // entity with all detail types displayed in list
            do! Instance.selectDataTableAction (newEntityId 100) Instance.defaultListClause
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/datatable_100_all.json"])

        }

        [<Fact>]
        member __.``select_filtered_action_single``() = async{
            do! Instance.getEntryDataTableAction (newInstanceId 90)
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [
                    (Utils.reformatJSON """
                        [{
                            "id": 90,
                            "nom": "Steuber",
                            "prenom": "Toni",
                            "fonction": "Chief Division Administrator",
                            "service": "Tools",
                            "coordonees_specifiques": "533",
                            "company_email": "Toni82@hotmail.com"
                        }]
                    """)

                ])
        }

        [<Fact>]
        member __.``select_datatable_action_filtered``() = async{
            let! nom = getDetail 48
            let! prenom = getDetail 56
            let! utf8DetailName = getDetail 84
            // simple search
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter {column = nom ; value= "Miller"})}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_nom_miller.json"])

            // Ensure that with a False filter we don't get any row
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some False}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult ["[]"])

            // accented chars
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = prenom ; value= "Raphaël"})}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_prenom_raphael.json"])

            // UTF-8 characters, and strange detail name
            do! Instance.selectDataTableAction (newEntityId 100000) {Instance.defaultListClause with where = Some (DetailFilter{column = utf8DetailName ; value= "游"})}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_100000_filtered.json"])

            // empty results
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = prenom ; value= "Jonnyh"})}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ "[]" ])

            // The % sign can still be used as wildcard
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = prenom ; value= "e$"})}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [  expectedJSON "database/expected/crosstab_filtered_prenom_e_dollar.json" ])

            // check we escape sql chars
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = prenom ; value= "'; delete * from instances;"})}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ "[]" ])
            // we check the instance where not deleted
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = prenom ; value= "e$"})}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [  expectedJSON "database/expected/crosstab_filtered_prenom_e_dollar.json" ])
        }

        [<Fact>]
        member __.``select_datatable_action_filtered_paginated``() = async{
            let getDetail (id:int) = async{
                let! detailResult  = DBAction.querySingleAction "select * from details where id = %d" id
                                            |> DBAction.run<Detail.Detail> Client.client
                return detailResult |> DBResult.getFirst
            }
            let! nom = getDetail 48
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = nom ; value= "e"}); page=1; perPage=2}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_nom_e_pp2_p1.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = nom ; value= "e"}); page=2; perPage=2}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_nom_e_pp2_p2.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = nom ; value= "e"}); page=3; perPage=2}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_nom_e_pp2_p3.json"])


            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = nom ; value= "e"}); page=1; perPage=4}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_nom_e_pp4_p1.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = nom ; value= "e"}); page=2; perPage=4}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_nom_e_pp4_p2.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = nom ; value= "e"}); page=3; perPage=4}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_nom_e_pp4_p3.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with where = Some (DetailFilter{column = nom ; value= "e"}); page=4; perPage=4}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_nom_e_pp4_p4.json"])
        }

        [<Fact>]
        member __.``select_datatable_action_paginated``() = async{
            // all full length pages
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with page=1; perPage=5}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_paginated_pp5p1.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with page=2; perPage=5}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_paginated_pp5p2.json"])

            do! Instance.selectDataTableAction (newEntityId 12)  {Instance.defaultListClause with page=3; perPage=5}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_paginated_pp5p3.json"])

            // last page is not full
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with page=1; perPage=6}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_paginated_pp6p1.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with page=2; perPage=6}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_paginated_pp6p2.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with page=3; perPage=6}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_paginated_pp6p3.json"])

            // last page has only 1 item
            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with page=1; perPage=7}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_paginated_pp7p1.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with page=2; perPage=7}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_paginated_pp7p2.json"])

            do! Instance.selectDataTableAction (newEntityId 12) {Instance.defaultListClause with page=3; perPage=7}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_paginated_pp7p3.json"])
        }

        [<Fact>]
        member __.``select_filtered_datatable_action``() = async{
            do! Instance.selectFilteredDataTableAction (newEntityId 12) (Some ([ 74; 82; 86 ] |> List.map newInstanceId)) Instance.defaultListClause
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_filtered_12_74_82_86.json"])

            do! Instance.selectFilteredDataTableAction (newEntityId 12) (Some ([ 74; 82; 86 ] |> List.map newInstanceId)) {defaultListClause with where = Some False}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult ["[]"])

            do! Instance.selectFilteredDataTableAction (newEntityId 12) (Some([ 74; 82; 86 ] |> List.map newInstanceId)) {Instance.defaultListClause with select = "select id,nom,prenom"; perPage=Instance.maxPerPage}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[{ id: 74, nom: "Padberg",prenom:"Willie"}, {id: 82,nom:"Denesik",prenom:"Raphaël"}, {id:86,nom:"Bahringer",prenom:"Dewey"} ]""") ])

            // verify that with no filtering we get all data
            do! Instance.selectFilteredDataTableAction (newEntityId 12) None {Instance.defaultListClause with select = "select *"; perPage=Instance.maxPerPage}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_12.json"])

            // This is an entity with UTF-8 characters in data and details names
            do! Instance.selectFilteredDataTableAction (newEntityId 100000) None {Instance.defaultListClause with select = "select *"; perPage=Instance.maxPerPage}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/crosstab_all_100000.json"])

            // Search on integer detail
            let! personnes_occuppees = getDetail 51
            do! Instance.selectFilteredDataTableAction (newEntityId 11) (Some ([69 ;71 ;73 ;77 ;78 ;79; 89] |> List.map newInstanceId)) {Instance.defaultListClause with where = Some (DetailFilter{column=personnes_occuppees;value="30"}); order = Some {column="personnes_occuppees"; direction=Asc}}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[{ "id": 89, "nom": null, "code_nace": "", "TVA": "", "personnes_occuppees": 30, "company_email": null }]""") ])
            // If the matching instance is not in the instance id filter, returns an empty list
            do! Instance.selectFilteredDataTableAction (newEntityId 11) (Some ([69 ;71 ;73 ;77 ;78 ;79] |> List.map newInstanceId)) {Instance.defaultListClause with where = Some (DetailFilter{column=personnes_occuppees;value="30"}); order = Some {column="personnes_occuppees"; direction=Asc}}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[]""") ])
        }

        [<Fact>]
        member __.``select_filtered_count_action``() = async{
            do! Instance.selectFilteredCountAction (newEntityId 12) (Some ([ 74; 82; 86 ] |> List.map newInstanceId)) {where = None}
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [3])

            do! Instance.selectFilteredCountAction (newEntityId 12) (Some ([ 74; 82; 86 ] |> List.map newInstanceId)) {where = Some False}
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [0])

            do! Instance.selectFilteredCountAction (newEntityId 12) (Some([ 74; 82; 86 ] |> List.map newInstanceId)) {where = None}
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [3])

            // verify that with no filtering we get all data
            do! Instance.selectFilteredCountAction (newEntityId 12) None {where = None}
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [15])

            // This is an entity with UTF-8 characters in data and details names
            do! Instance.selectFilteredCountAction (newEntityId 100000) None {where= None}
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [2])
        }
        [<Fact>]
        member __.``byId_tests``() = async{
            let instance82:Instance.Instance = { ``id`` = newInstanceId 82;
                                                  entity_id = newEntityId 12;
                                                  created_at = None;
                                                  lock_version = 0;
                                                  updated_at = None; }

            do! Instance.byIdAction (newInstanceId 82)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [instance82])

            // inexisting instance
            do! Instance.byIdAction (newInstanceId 23982)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])
        }

        [<Fact>]
        member __.``instance_getEntity``() = async{

            let entity12:Entity.Entity = { ``id`` = newEntityId 12;
                                            database_id = 6;
                                            name = "contacts";
                                            lock_version = None;
                                            has_public_form = false;
                                            has_public_data= None;
                                            public_to_all= None
            }
            do! Instance.byIdAction (newInstanceId 82)
                |> DBAction.bind Instance.getEntityAction
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [entity12])

        }


        [<Fact>]
        member __.``linked_instances``() = async{
            do! Instance.linkedInstancesAction (newInstanceId 77) (ChildrenVia (newRelationId 9)) Instance.defaultListClause
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[{
                                        "id": 82,
                                        "nom": "Denesik",
                                        "prenom": "Raphaël",
                                        "fonction": "Human Program Planner",
                                        "service": "Jewelery",
                                        "company_email": "Raphael45@hotmail.com"
                                  }]""") ])

            do! Instance.linkedInstancesAction (newInstanceId 89) (ChildrenVia (newRelationId 9)) Instance.defaultListClause
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[
                                      {
                                        "id": 90,
                                        "nom": "Steuber",
                                        "prenom": "Toni",
                                        "fonction": "Chief Division Administrator",
                                        "service": "Tools",
                                        "company_email": "Toni82@hotmail.com"
                                      }
                ]""") ])

            do! Instance.linkedInstancesAction (newInstanceId 73) (ChildrenVia (newRelationId 7)) Instance.defaultListClause
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[
                                    {
                                        "id": 74,
                                        "nom": "Padberg",
                                        "prenom": "Willie",
                                        "fonction": "District Metrics Facilitator",
                                        "service": "Music",
                                        "company_email": "Willie_Padberg@yahoo.com"
                                    },
                                    {
                                        "id": 75,
                                        "nom": "Miller",
                                        "prenom": "Nadine",
                                        "fonction": "Future Accountability Manager",
                                        "service": "Tools & Music",
                                        "company_email": "Nadine_Miller@yahoo.com"
                                    },
                                    {
                                        "id": 76,
                                        "nom": "Hammes",
                                        "prenom": "Marcos",
                                        "fonction": "International Accounts Consultant",
                                        "service": "Industrial & Health",
                                        "company_email": "Marcos.Hammes76@gmail.com"
                                    }
                ]""") ])
            // ------------------------------
            // Test filtered linked instances
            // ------------------------------
            let getDetail (id:int) = async{
                let! detailResult  = DBAction.querySingleAction "select * from details where id = %d" id
                                            |> DBAction.run<Detail.Detail> Client.client
                return detailResult |> DBResult.getFirst
            }
            let! nom = getDetail 48
            let! prenom = getDetail 56
            // filter on nom
            do! Instance.linkedInstancesAction (newInstanceId 73)
                                                (ChildrenVia (newRelationId 7))
                                                {Instance.defaultListClause with where = Some (DetailFilter{column=nom;value="iller"}) }
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[
                                    {
                                        "id": 75,
                                        "nom": "Miller",
                                        "prenom": "Nadine",
                                        "fonction": "Future Accountability Manager",
                                        "service": "Tools & Music",
                                        "company_email": "Nadine_Miller@yahoo.com"
                                    }
                ]""") ])

            do! Instance.linkedInstancesAction (newInstanceId 73)
                                                (ChildrenVia (newRelationId 7))
                                                {Instance.defaultListClause with where = Some (DetailFilter{column=nom;value="a"}) }
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[
                                    {
                                        "id": 74,
                                        "nom": "Padberg",
                                        "prenom": "Willie",
                                        "fonction": "District Metrics Facilitator",
                                        "service": "Music",
                                        "company_email": "Willie_Padberg@yahoo.com"
                                    },
                                    {
                                        "id": 76,
                                        "nom": "Hammes",
                                        "prenom": "Marcos",
                                        "fonction": "International Accounts Consultant",
                                        "service": "Industrial & Health",
                                        "company_email": "Marcos.Hammes76@gmail.com"
                                    }
                ]""") ])
            //-----------
            // order list
            //-----------
            do! Instance.linkedInstancesAction (newInstanceId 73)
                                                (ChildrenVia (newRelationId 7))
                                                { Instance.defaultListClause with order = Some {column=nom.name; direction=Instance.Asc}}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[
                                    {
                                        "id": 76,
                                        "nom": "Hammes",
                                        "prenom": "Marcos",
                                        "fonction": "International Accounts Consultant",
                                        "service": "Industrial & Health",
                                        "company_email": "Marcos.Hammes76@gmail.com"
                                    },
                                    {
                                        "id": 75,
                                        "nom": "Miller",
                                        "prenom": "Nadine",
                                        "fonction": "Future Accountability Manager",
                                        "service": "Tools & Music",
                                        "company_email": "Nadine_Miller@yahoo.com"
                                    },
                                    {
                                        "id": 74,
                                        "nom": "Padberg",
                                        "prenom": "Willie",
                                        "fonction": "District Metrics Facilitator",
                                        "service": "Music",
                                        "company_email": "Willie_Padberg@yahoo.com"
                                    }
                ]""") ])
            //-----------
            // pagination
            //-----------
            do! Instance.linkedInstancesAction (newInstanceId 73)
                                                (ChildrenVia (newRelationId 7))
                                                { Instance.defaultListClause with order = Some {column=nom.name; direction=Instance.Asc}; perPage=2; page=1}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[
                                    {
                                        "id": 76,
                                        "nom": "Hammes",
                                        "prenom": "Marcos",
                                        "fonction": "International Accounts Consultant",
                                        "service": "Industrial & Health",
                                        "company_email": "Marcos.Hammes76@gmail.com"
                                    },
                                    {
                                        "id": 75,
                                        "nom": "Miller",
                                        "prenom": "Nadine",
                                        "fonction": "Future Accountability Manager",
                                        "service": "Tools & Music",
                                        "company_email": "Nadine_Miller@yahoo.com"
                                    }               ]""") ])
            do! Instance.linkedInstancesAction (newInstanceId 73)
                                                (ChildrenVia (newRelationId 7))
                                                { Instance.defaultListClause with order = Some {column=nom.name; direction=Instance.Asc}; perPage=2; page=2}
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ (Utils.reformatJSON """[
                                    {
                                        "id": 74,
                                        "nom": "Padberg",
                                        "prenom": "Willie",
                                        "fonction": "District Metrics Facilitator",
                                        "service": "Music",
                                        "company_email": "Willie_Padberg@yahoo.com"
                                    }
                       ]""") ])
        }

        [<Fact>]
        member __.``countAction``() = async {
            // no instance for this
            do! Instance.selectCountAction (newEntityId 81) (Instance.countFor Instance.defaultListClause)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 0L ])

            do! Instance.selectCountAction (newEntityId 12) (Instance.countFor Instance.defaultListClause)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 15L ])

            do! Instance.selectCountAction (newEntityId 11) (Instance.countFor Instance.defaultListClause)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 11L ])
        }

        [<Fact>]
        member __.``countLinkedInstances``() = async {
            // no instance for this
            do! Instance.countLinkedInstancesAction (newInstanceId 77) (ChildrenVia (newRelationId 7)) (Instance.defaultListClause)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 4L ])


            let! nom = getDetail 48
            do! Instance.countLinkedInstancesAction (newInstanceId 77) (ChildrenVia (newRelationId 7)) ({Instance.defaultListClause with where = Some (DetailFilter{ column=nom ; value="e" }) })
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 3L ])
            do! Instance.countLinkedInstancesAction (newInstanceId 77) (ChildrenVia (newRelationId 7)) ({Instance.defaultListClause with where = Some (DetailFilter{ column=nom ; value="ye" }) })
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 1L ])

        }

        [<Fact>]
        member __.``delete_instance``() = async{
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // instance with detail values and parent instance
            let deletedId = 70
            let expectedStateDifference = { DBState.zeroState with
                                              instances = -1L
                                              detailValues = -6L
                                              links = -1L
                                          }
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client [ -1L, "select count(*) from detail_values where type='EmailDetailValue'"
                                                               -5L, "select count(*) from detail_values where type='SimpleDetailValue'"
                                                               0L, "select count(*) from detail_values where type='SimpleDetailValue' and value=''"
                                                               -1L, sprintf "select count(*) from links where child_id=%d" deletedId
                                                             ]
                    expectedStateDifference
            do! Instance.deleteAction (newInstanceId deletedId)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [DBTypes.ImpactedRows 1])

            do! checkDifferenceNow ()

            // instance with ddl, integer and child instance
            let deletedId = 71
            let expectedStateDifference = { DBState.zeroState with
                                              instances = -1L
                                              detailValues = -6L
                                              links = -1L
                                              ddlDetailValues = -1L
                                              integerDetailValues = -1L
                                          }


            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client [ -0L, "select count(*) from detail_values where type='EmailDetailValue'"
                                                               -6L, "select count(*) from detail_values"
                                                               -5L, "select count(*) from detail_values where type='SimpleDetailValue'"
                                                               -2L, "select count(*) from detail_values where type='SimpleDetailValue' and value=''"
                                                               -1L, "select count(*) from detail_values where type='LongTextDetailValue' and value=''"
                                                               -1L, sprintf "select count(*) from links where parent_id=%d" deletedId
                                                             ]
                    expectedStateDifference
            do! Instance.deleteAction (newInstanceId deletedId)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [DBTypes.ImpactedRows 1])

            do! checkDifferenceNow ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``instance_getSpecAction``() = async{
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let! instance69Spec =
                Instance.getSpecAction  (newInstanceId 69)
                |> DBAction.run Client.client
            // The ChoiceId is a private type that we can only build by querying the database, which we do here
            let! choiceForDetail62Result = DdlChoice.fromId Client.client (DetailId 62) (DetailValuePropositionId 11)
            let choiceForDetail62 = Result.get choiceForDetail62Result |> List.head

            instance69Spec
            |> checkOkDBResult
                [{  entityId = newEntityId 11
                    instanceId = Some (newInstanceId 69)
                    detailValues =
                        // no value for nom, company_email
                        // status sprl
                        [{ detailId = DetailId 62
                           values = [{ detailValueId = Some (DdlDetailValueId 15)
                                       value = ServerValidated (ServerValue.DdlValueChoice (Some choiceForDetail62)) }] };
                         // personnes_occupees
                         { detailId = DetailId 51
                           values = [{ detailValueId = Some (IntegerDetailValueId 5)
                                       value = ServerValidated (ServerValue.Integer (Some 2L)) }] };
                        // memo
                         { detailId = DetailId 55
                           values = [{ detailValueId = Some (DetailValueId 299)
                                       value = ServerValidated (ServerValue.LongText (Some "") )}] };
                         // code_nace
                         { detailId = DetailId 49
                           values = [{ detailValueId = Some (DetailValueId 294)
                                       value = ServerValidated (ServerValue.Simple (Some "hjhjhjk")) }] };
                         // TVA
                         { detailId = DetailId 50
                           values = [{ detailValueId = Some (DetailValueId 295)
                                       value = ServerValidated (ServerValue.Simple (Some "")) }] };
                         // adresse
                         { detailId = DetailId 52
                           values = [{ detailValueId = Some (DetailValueId 296)
                                       value = ServerValidated (ServerValue.Simple (Some "rue de perck")) }] }
                         // telephone
                         { detailId = DetailId 53
                           values = [{ detailValueId = Some (DetailValueId 297)
                                       value = ServerValidated (ServerValue.Simple (Some "")) }] };
                         // fax
                         { detailId = DetailId 54
                           values = [{ detailValueId = Some (DetailValueId 298)
                                       value = ServerValidated (ServerValue.Simple (Some "")) }] };
                        ]
                }]


            // Delete 2 detail values and check the new spec if correct
            let! _ = DBAction.immediate Client.client "delete from detail_values where id in (297,298)"

            let! instance69SpecAfterDelete =
                Instance.getSpecAction  (newInstanceId 69)
                |> DBAction.run Client.client

            instance69SpecAfterDelete
            |> checkOkDBResult
                [{  entityId = newEntityId 11
                    instanceId = Some (newInstanceId 69)
                    detailValues =
                        // no value for nom, company_email
                        // status sprl
                        [{ detailId = DetailId 62
                           values = [{ detailValueId = Some (DdlDetailValueId 15)
                                       value = ServerValidated (ServerValue.DdlValueChoice (Some choiceForDetail62)) }] };
                         // personnes_occupees
                         { detailId = DetailId 51
                           values = [{ detailValueId = Some (IntegerDetailValueId 5)
                                       value = ServerValidated (ServerValue.Integer (Some 2L)) }] };
                        // memo
                         { detailId = DetailId 55
                           values = [{ detailValueId = Some (DetailValueId 299)
                                       value = ServerValidated (ServerValue.LongText (Some "") )}] };
                         // code_nace
                         { detailId = DetailId 49
                           values = [{ detailValueId = Some (DetailValueId 294)
                                       value = ServerValidated (ServerValue.Simple (Some "hjhjhjk")) }] };
                         // TVA
                         { detailId = DetailId 50
                           values = [{ detailValueId = Some (DetailValueId 295)
                                       value = ServerValidated (ServerValue.Simple (Some "")) }] };
                         // adresse
                         { detailId = DetailId 52
                           values = [{ detailValueId = Some (DetailValueId 296)
                                       value = ServerValidated (ServerValue.Simple (Some "rue de perck")) }] }
                                       ] }]

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``instance_getSpecActionWithFileAttachment``() = async{
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let! instance202Spec =
                Instance.getSpecAction  (newInstanceId 202)
                |> DBAction.run Client.client

            instance202Spec
            |> checkOkDBResult
                [{  entityId = newEntityId 101
                    instanceId = Some (newInstanceId 202)
                    detailValues = [

                        {  detailId = DetailId 82
                           values =
                            [{ detailValueId = Some (DetailValueId 1215)
                               value =
                                ServerValidated
                                  (ServerValue.FileAttachment (Some { detail_value_id = Some 1215
                                                                      s3_key = Some "1/6/101/202/1215"
                                                                      filetype = Some "image/png"
                                                                      filename = "logo-Ubuntu.png"
                                                                      uploaded = true
                                                                      indb = true })) }] };
                         { detailId = DetailId 74
                           values = [{ detailValueId = Some (DetailValueId 1214)
                                       value = ServerValidated (ServerValue.Simple (Some "Mohsin Hijazee")) }] }

                        ]
                }]


            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``linkable_instances``() = async{
            // #########################
            // Many to many relations
            // #########################
            // From parent to child
            // There are 15 instances, of which 4 are already linked, which leaves us 11 available to link
            // We check the number of available and linked instances to ensure the validity of the test
            // From parent instance 77 (entity 11) to instances of entity 12 through relation 7
            let! (totalInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from instances where entity_id = 12"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            totalInstances.count |> should equal 15L
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where parent_id=77 and relation_id=7;"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 4L
            let expectedCount = totalInstances.count-linkedInstances.count
            do! Instance.linkableInstanceIdsAction (newInstanceId 77) (ChildrenVia (newRelationId 7))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 11 ])
            do! Instance.linkableInstanceIdsAction (newInstanceId 77) (ChildrenVia (newRelationId 7))
                |> DBAction.map (fun id -> InstanceId.Get id)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.sort l)
                |> DBAction.workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 70 ;72 ;74 ;75 ;76 ;85 ;86 ;87 ;90 ;92 ;94 ])
            // From child to parent
            //-----------------------
            // From child instance 81 (entity 12) to instances of entity 11 through relation 7
            let! (totalInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from instances where entity_id = 11"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            totalInstances.count |> should equal 11L
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where child_id=81 and relation_id=7;"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 1L
            do! Instance.linkableInstanceIdsAction (newInstanceId 81) (ParentsVia (newRelationId 7))
                |> DBAction.map (fun id -> InstanceId.Get id)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.sort l)
                |> DBAction.workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 69 ;71 ;73 ;78 ;79 ;80 ;88 ;89 ;91 ;93])

            // instance without linked instances, should list all existing instances of the right entity id
            do! Instance.linkableInstanceIdsAction (newInstanceId 80) (ParentsVia (newRelationId 7))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l |> int64)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 11 ])
            do! Instance.linkableInstanceIdsAction (newInstanceId 80) (ParentsVia (newRelationId 7))
                |> DBAction.map (fun id -> InstanceId.Get id)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.sort l)
                |> DBAction.workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 69 ;71 ;73 ;77; 78 ;79 ;80 ;88 ;89 ;91 ;93])

            // #########################
            // One to one relations
            // #########################
            // Working with relation 9 having these links:
            //  id | parent_id | child_id | relation_id
            // ----+-----------+----------+-------------
            //  52 |        77 |       82 |           9
            //  53 |        89 |       90 |           9
            // From parent to child
            // There are 15 instances, of which 4 are already linked, which leaves us 11 available to link
            // We check the number of available and linked instances to ensure the validity of the test
            // Instance with linked instance
            // From parent instance 77 (entity 11) to instances of entity 12 through relation 9
            // No linkable because the parent alraedy has a linked entry
            let sourceInstanceIdWithLinked = 77
            let! (totalInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from instances where entity_id = 12"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            totalInstances.count |> should equal 15L
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where parent_id=%d and relation_id=9;" sourceInstanceIdWithLinked
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 1L
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithLinked) (ChildrenVia (newRelationId 9))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 0 ])

            // This source instance does not have a link yet for relation 9
            let sourceInstanceIdWithLinked = 88
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where parent_id=%d and relation_id=9;" sourceInstanceIdWithLinked
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 0L
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithLinked) (ChildrenVia (newRelationId 9))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 13 ])

            //// From child to parent
            ////-----------------------
            // source already linked, should result in empty list to choose from
            /// from child instance 82 (entity 12) to instances of entity 11 through relation 9
            let sourceInstanceIdWithLinked = 82
            let! (totalInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from instances where entity_id = 11"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            totalInstances.count |> should equal 11L
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where child_id=%d and relation_id=9;" sourceInstanceIdWithLinked
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 1L
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithLinked) (ParentsVia (newRelationId 9))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 0 ])
            // source not yet linked
            /// from child instance 86 (entity 12) to instances of entity 11 through relation 9
            let sourceInstanceIdWithoutLinked = 86
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where child_id=%d and relation_id=9;" sourceInstanceIdWithoutLinked
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 0L
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithoutLinked) (ParentsVia (newRelationId 9))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 9 ])
            // Also check the ids are those expected
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithoutLinked) (ParentsVia (newRelationId 9))
                |> DBAction.map (fun id -> InstanceId.Get id)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.sort l)
                |> DBAction.workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [69; 71; 73; 78; 79; 80; 88; 91; 93])

            // #########################
            // One to many relations
            // #########################
            // Working with relation 8 from parent entity 19 to child entity 12
            // Having these links:
            //  id | parent_id | child_id | relation_id
            // ----+-----------+----------+-------------
            //  51 |        95 |       81 |           8
            // From parent to child
            // From parent instance 77 (entity 19) to instances of entity 12 through relation 8
            // No linkable because the parent alraedy has a linked entry
            let sourceInstanceIdWithLinked = 95
            let relationId = 8
            let! (totalInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from instances where entity_id = 12"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            totalInstances.count |> should equal 15L
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where parent_id=%d and relation_id=%d;" sourceInstanceIdWithLinked relationId
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 1L
            // 14 because 1 of the 15 is already linked to us (and additionally, it is a to one relation from their side)
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithLinked) (ChildrenVia (newRelationId relationId))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 14 ])

            // This source instance does not have a link yet for relation 9
            let sourceInstanceIdWithLinked = 96
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where parent_id=%d and relation_id=%d;" sourceInstanceIdWithLinked relationId
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 0L
            // 14 because 1 of the 15 is already linked to an entity, and from their side it is a to one relation
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithLinked) (ChildrenVia (newRelationId relationId))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 14 ])

            //// From child to parent
            ////-----------------------
            // source already linked, should result in empty list to choose from
            /// from child instance 82 (entity 12) to instances of entity 11 through relation 9
            let sourceInstanceIdWithLinked = 81
            let! (totalInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from instances where entity_id = 19"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            totalInstances.count |> should equal 5L
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where child_id=%d and relation_id=%d;" sourceInstanceIdWithLinked relationId
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 1L
            // we are looking at the source of an existing to one link, so no additional link can be created
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithLinked) (ParentsVia (newRelationId relationId))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 0 ])
            // source not yet linked
            /// from child instance 86 (entity 12) to instances of entity 19 through relation 8
            let sourceInstanceIdWithoutLinked = 86
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where child_id=%d and relation_id=%d;" sourceInstanceIdWithoutLinked relationId
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 0L
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithoutLinked) (ParentsVia (newRelationId relationId))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 5 ])
            // Also check the ids are those expected
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithoutLinked) (ParentsVia (newRelationId relationId))
                |> DBAction.map (fun id -> InstanceId.Get id)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.sort l)
                |> DBAction.workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [95;96;97;98;99])

            // #########################
            // Many to one relations
            // #########################
            // Working with relation 10 from parent entity 11 (company) to child entity 12 (contact)
            // Relation named "has CEO" and "is CEO at"
            // Having these links:
            //  id | parent_id | child_id | relation_id
            // ----+-----------+----------+-------------
            //  42 |        93 |       92 |          10
            // From parent to child
            // No linkable because the parent already has a linked entry and we are in a to one relation
            let sourceInstanceIdWithLinked = 93
            let relationId = 10
            let! (totalInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from instances where entity_id = 12"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            totalInstances.count |> should equal 15L
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where parent_id=%d and relation_id=%d;" sourceInstanceIdWithLinked relationId
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 1L
            // 14 because 1 of the 15 is already linked to us (and additionally, it is a to one relation from their side)
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithLinked) (ChildrenVia (newRelationId relationId))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 0 ])

            // This source instance does not have a link yet for relation 10
            let sourceInstanceIdWithLinked = 69
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where parent_id=%d and relation_id=%d;" sourceInstanceIdWithLinked relationId
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 0L
            // 15, because from their side it is a to many relation so all of them are elligible
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithLinked) (ChildrenVia (newRelationId relationId))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 15 ])

            //// From child to parent
            ////-----------------------
            // source already linked
            let sourceInstanceIdWithLinked = 92
            let! (totalInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from instances where entity_id = 11"
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            totalInstances.count |> should equal 11L
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where child_id=%d and relation_id=%d;" sourceInstanceIdWithLinked relationId
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 1L
            // we are looking at the source of an existing to many link, but a to one from their side, so all not yet linked are elligible
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithLinked) (ParentsVia (newRelationId relationId))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 10 ])
            // source not yet linked
            let sourceInstanceIdWithoutLinked = 83
            let! (linkedInstances:{|count:int64|}) =
                DBAction.queryAction "select count(*) from links where child_id=%d and relation_id=%d;" sourceInstanceIdWithoutLinked relationId
                |> DBAction.run Client.client
                |> Async.map (DBResult.get)
                |> Async.map List.head
            linkedInstances.count |> should equal 0L
            // From their side this is a to 1 relation, so those already linked should not appear
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithoutLinked) (ParentsVia (newRelationId relationId))
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ 10 ])
            // Also check the ids are those expected
            do! Instance.linkableInstanceIdsAction (newInstanceId sourceInstanceIdWithoutLinked) (ParentsVia (newRelationId relationId))
                |> DBAction.map (fun id -> InstanceId.Get id)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.sort l)
                |> DBAction.workOnItems
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [69 ; 71 ; 73 ; 77 ; 78 ; 79 ; 80 ; 88 ; 89 ; 91])
        }

        [<Fact>]
        member __.``instanceIdsFilteredSelect``() = async{
            let relationAndDirection = (ParentsVia (newRelationId 7))
            let sourceInstanceId = newInstanceId 87
            // select a datatable
            do! linkableInstanceIdsAction sourceInstanceId relationAndDirection
                |> instanceIdsFilteredSelect  relationAndDirection (Instance.defaultListClause) (selectFilteredDataTableAction)
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/instanceIdsFilteredSelect_from_through_7.json"])

            // select a datatable with a sort
            do! linkableInstanceIdsAction sourceInstanceId relationAndDirection
                |> instanceIdsFilteredSelect  relationAndDirection {Instance.defaultListClause with order = Some {column="personnes_occuppees"; direction=Asc}} (selectFilteredDataTableAction)
                |> DBAction.mapDataTableToJSON
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ expectedJSON "database/expected/instanceIdsFilteredSelect_from_through_7_sort_by_personnes_occuppees.json"])

            // a count
            do! linkableInstanceIdsAction sourceInstanceId relationAndDirection
                |> instanceIdsFilteredSelect  relationAndDirection (countFor Instance.defaultListClause) (selectFilteredCountAction)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [10])

            // a count with a sort
            do! linkableInstanceIdsAction sourceInstanceId relationAndDirection
                |> instanceIdsFilteredSelect  relationAndDirection (countFor {Instance.defaultListClause with order = Some {column="personnes_occuppees"; direction=Asc}}) (selectFilteredCountAction)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [10L])

            // helper
            let getDetail (id:int) = async{
                let! detailResult  = DBAction.querySingleAction "select * from details where id = %d" id
                                            |> DBAction.run<Detail.Detail> Client.client
                return detailResult |> DBResult.getFirst
            }
            // A count with a filter
            let! personnes_occuppees = getDetail 51
            do! linkableInstanceIdsAction sourceInstanceId relationAndDirection
                |> instanceIdsFilteredSelect  relationAndDirection (countFor {Instance.defaultListClause with where = Some (DetailFilter{column=personnes_occuppees;value="30"}); order = Some {column="personnes_occuppees"; direction=Asc}}) (selectFilteredCountAction)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1L])


        }

        [<Fact>]
        member __.``instance_creation_notification``() = async {

            // We collect notifications in this queue. Remember to clear it when needed.
            let notificationsQueue = System.Collections.Concurrent.ConcurrentQueue<NotificationSubscriptionCriteria.Event*NotificationSubscriptionCriteria.Source*int*string>()
            notifyToQueue notificationsQueue

            // spec for the instance to be inserted
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 12)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 48 // nom
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "John")) } ] }
                        { detailId = DetailId 58 // service
                          values =
                              [ { detailValueId = None
                                  value = ServerValidated (ServerValue.Simple (Some "Finance")) } ] } ] }
            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      instances = 1L
                      // a,d it was newly inserted
                      maxInstanceId = 1
                      // 2 detail values inserted?
                      detailValues = 2L
                      maxDetailValueId = 2 }


            // Helper function to easily repeat creation with and without subscriber to instance creation notifications
            // ---------------
            let testCreationNotification (withSubscriber:bool) = async {
              // Ensure we start with an empty queue
              notificationsQueue.Clear()
              // Setup SavePoint and rollback for this function
              let! _rollbackDisposable = setSavePoint Client.client
              // Add a subscriber if needed
              if withSubscriber then
                // Create a notification subscription for societe creation by user 100002
                do! NotificationSubscription.SubscribeInstanceCreationMailAction (newEntityId 12)(Ids.UserId 100002)
                    |> DBAction.workOnList
                    |> DBAction.map (fun l -> List.length l)
                    |> DBAction.run Client.client
                    |> Async.map (checkOkDBResult [1])

              // Check we have the expected number of subscribers as possible debugging help
              do! DBAction.queryAction "select count(*) from notification_subscriptions"
                  |> DBAction.run Client.client
                  |> Async.map (checkOkDBResult [if withSubscriber then 1L else 0L])

              let! preState, checkDifferenceNow =
                  DBState.startWithCustomQueries Client.client
                      [ 1L, "select count(*) from detail_values where detail_id=48"
                        1L, "select count(*) from detail_values where detail_id=58" ] expectedStateDifference

              // create instance for spec
              let! instanceActionResult =
                  Instance.createOrUpdateAction spec
                  |> DBAction.run Client.client

              let specResult =
                match instanceActionResult with
                | Ok [spec] -> spec
                | Ok l -> failwithf "Unexpected spec list length in instance creation %A" l
                | Error es -> failwithf "Unexpected error in instance creation %A" (es|>Error.toString)

              // Now check what was added to the database
              // get counters after insert
              do! checkDifferenceNow ()

              if withSubscriber then
                notificationsQueue.ToArray() |> should equal [| NotificationSubscriptionCriteria.Event.AfterCreate,NotificationSubscriptionCriteria.Source.Instance,preState.maxInstanceId + 1,"100002"|]
              else
                notificationsQueue.ToArray() |> should equal [||]


              // Update
              notificationsQueue.Clear()
              let! _, checkUpdateDifference =
                  DBState.startWithCustomQueries Client.client [] DBState.zeroState

              // Fiction
              let newName = "Jean"

              let specUpdate =
                  Lens.With <@ specResult.detailValues.[0].values.[0].value @> (ServerValidated (ServerValue.Simple (Some newName)))

              // create instance for spec
              let instanceUpdateResult =
                  Instance.createOrUpdateAction specUpdate
                  |> DBAction.run Client.client

              // We get the updated spec as expected
              do! instanceUpdateResult
                          |> Async.map(checkOkResult [ specUpdate ])

              // No subscription for instance update, so no notification should be sent
              notificationsQueue.ToArray() |> should equal [||]

              do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
            }

            do! testCreationNotification true
            do! testCreationNotification false
        }
