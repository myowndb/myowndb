namespace MyOwnDB.Tests.Import

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus
open Instance
open Specs
open FSharp.Data
open FSharp.Data.JsonExtensions
open ImportMapping

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
      DB.DBClient((AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString) + " Include Error Detail=true;")

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
            [ "Account"
              "User"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Import =
    let getDetail (id:int) = async{
        let! detailResult  = DBAction.querySingleAction "select * from details where id = %d" id
                                    |> DBAction.run<Detail.Detail> Client.client
        return detailResult |> DBResult.getFirst
    }

    let accountId = AccountId 1
    let userEmail = "existingbob@test.com"

    let visiteEntityId = newEntityId 19
    let societeEntityId = newEntityId 11
    let inexistingEntityId = newEntityId 5555

    [<Trait("Category", "models")>]
    [<Trait("Category", "import")>]
    type ImportTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``failed_initialise_import``() = async {
            // Take state snapshot once for all tests not creating data
            let! _preState, checkNoCreation =
                DBState.start Client.client DBState.zeroState

            // *********************** Test not creating imports ************************************
            // inexisting email
            do! ImportExtensions.initialiseAction visiteEntityId "nouser@myowndb.com"
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult [ "Not found" ])
            do! checkNoCreation()

            // email from another account
            do! ImportExtensions.initialiseAction visiteEntityId "admin2@test.com"
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult [ "Not found" ])
            do! checkNoCreation()

            // inexisting entity
            do! ImportExtensions.initialiseAction inexistingEntityId "admin2@test.com"
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult [ "Not found" ])
            do! checkNoCreation()

        }

        [<Fact>]
        member __.``import_creation``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // We collect notifications in this queue. Remember to clear it when needed.
            let notificationsQueue = System.Collections.Concurrent.ConcurrentQueue<NotificationSubscriptionCriteria.Event*NotificationSubscriptionCriteria.Source*int*string>()
            notifyToQueue notificationsQueue

            // Add subscriber
            do! NotificationSubscription.SubscribeInstanceCreationMailAction visiteEntityId (Ids.UserId 100002)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1])

            let expectedStateDifference =
                { DBState.zeroState with
                      imports = 1L
                      maxImportId = 1
                }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference
            let thisImportId = ImportId (preState.maxImportId + 1)
            // Create import
            do! ImportExtensions.initialiseAction visiteEntityId userEmail
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [thisImportId])
            do! checkDifferenceNow ()

            // After initialisation, the import entry is not yet usable for importing data
            do! ImportExtensions.byIdForImportAction thisImportId accountId userEmail
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])

            // Check afterFileUploadAction
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.afterFileUploadAction
                    import
                    "fixtures/csv/import_visites.csv"
                    "visits_list.csv"
                    "text/csv"
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ [|"Visit date";"mémo";"title"|]])

            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.map (fun importRecord ->
                  // Collect step from the record, and file information from a json field
                  importRecord.step,
                  importRecord.user_email,
                  importRecord.import_info?file_name.AsString(),
                  importRecord.import_info?file_type.AsString(),
                  importRecord.import_info?file_path.AsString()
                  )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1, userEmail, "visits_list.csv", "text/csv", "fixtures/csv/import_visites.csv" ])

            // After the upload, we find the file for import
            do! ImportExtensions.byIdForImportAction thisImportId accountId userEmail
                |> DBAction.map (fun import -> import.id)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [thisImportId])

            // Aply mappings and import data

            let mappings : Mapping list =
              [
                { csvColumn = "Visit date"; detailId = DetailId 60 }
                { csvColumn = "mémo";       detailId = DetailId 55 }
                { csvColumn = "title";      detailId = DetailId 61 }

              ]

            let expectedStateDifference =
                { DBState.zeroState with
                      instances  = 3
                      maxInstanceId = 3
                      detailValues = 6
                      maxDetailValueId = 6
                      dateDetailValues = 3
                      maxDateDetailValueId = 3
                }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference


            // We do the import then do 2 checks on the outcome:
            // - it returns the instance ids inserted
            // - the json of the inserted instances is as expected
            // As we need to collect data from different sources (1 is the result of the operation, the other is the
            // database from which we extract instances), we collect both data in an anonymous record inside one DBAction.
            // Doing otherwise seems to be cumbersome as we then would have both DBResults and DBActions mixed.
            do! ImportExtensions.byIdForImportAction (thisImportId)(accountId)(userEmail)
                |> DBAction.bind (fun import ->
                  ImportExtensions.importFileAction import mappings
                )
                // Collect InstaceIds
                |> DBAction.map (fun instanceSpecs ->
                  {|
                     instanceIds = instanceSpecs |> List.map (fun s -> s.instanceId) |> List.map Option.get
                  |}
                )
                // Collect the json of inserted instances
                |> DBAction.bind(fun inputRec ->
                  Crosstab.getFilteredCrosstabInfoAction visiteEntityId (Some inputRec.instanceIds)
                  |> DBAction.map (fun (info: Crosstab.Info) ->  sprintf "select * from %s" info.crosstabListQueryBody)
                  |> DBAction.bind DBAction.staticDataTableAction
                  |> DBAction.mapDataTableToJSON
                  |> DBAction.map (fun json ->
                    {|
                      intInstanceIds = inputRec.instanceIds |> List.map InstanceId.Get
                      json = json
                    |}
                  )
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [
                    {|
                      intInstanceIds =  [preState.maxInstanceId + 1 ;preState.maxInstanceId + 2;preState.maxInstanceId + 3]
                      // This uses hard-coded ids, see comment in import_with_ddl to see why this can be problematic
                      json =  expectedJSON "import/expected/visite_import.json"
                    |}
                    ])
            do! checkDifferenceNow ()

            // For imports, we do not send notifications
            notificationsQueue.ToArray() |> should equal [||]

            // Check Import was updated
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.map (fun import ->
                    {|
                      step = import.step
                      userEmail = import.user_email
                      importedInstances = import.import_info?imported_instances.AsArray() |> Array.map(fun jsonValue -> jsonValue.AsInteger())
                    |}
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ {| step = 2; userEmail= userEmail; importedInstances = [|preState.maxInstanceId + 1;preState.maxInstanceId + 2;preState.maxInstanceId + 3|] |}])
                //
            // After importing data, the import entry is not usable anymore for importing data
            do! ImportExtensions.byIdForImportAction thisImportId accountId userEmail
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])


            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``import_with_ddl``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
                { DBState.zeroState with
                      imports = 1L
                      maxImportId = 1
                }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference
            let thisImportId = ImportId (preState.maxImportId + 1)
            // Create import
            do! ImportExtensions.initialiseAction societeEntityId "existingbob@test.com"
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [thisImportId])
            do! checkDifferenceNow ()

            // Validate that this import is not yet return for import
            do! ImportExtensions.byIdForImportAction thisImportId accountId userEmail
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])
            // Validate that this import is not yet return for undo
            do! ImportExtensions.byIdForUndoAction thisImportId accountId userEmail
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])

            // Check afterFileUploadAction
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.afterFileUploadAction
                    import
                    "fixtures/csv/import_societe_ddl.csv"
                    "societe_list.csv"
                    "text/csv"
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ [|"nom";"status"|]])

            // Validate that this import is not yet return for undo
            do! ImportExtensions.byIdForUndoAction thisImportId accountId userEmail
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])

            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.map (fun importRecord ->
                  // Collect step from the record, and file information from a json field
                  importRecord.step,
                  importRecord.user_email,
                  importRecord.import_info?file_name.AsString(),
                  importRecord.import_info?file_type.AsString(),
                  importRecord.import_info?file_path.AsString()
                  )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1, "existingbob@test.com", "societe_list.csv", "text/csv", "fixtures/csv/import_societe_ddl.csv" ])

            // Aply mappings and import data

            let mappings : Mapping list =
              [
                { csvColumn = "nom"; detailId = DetailId 48 }
                { csvColumn = "status"; detailId = DetailId 62 }

              ]

            let expectedStateDifference =
                { DBState.zeroState with
                      instances  = 3
                      maxInstanceId = 3
                      detailValues = 3
                      maxDetailValueId = 3
                      ddlDetailValues = 3
                      maxDdlDetailValueId = 3
                }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference


            // We do the import then do 2 checks on the outcome:
            // - it returns the instance ids inserted
            // - the json of the inserted instances is as expected
            // As we need to collect data from different sources (1 is the result of the operation, the other is the
            // database from which we extract instances), we collect both data in an anonymous record inside one DBAction.
            // Doing otherwise seems to be cumbersome as we then would have both DBResults and DBActions mixed.
            do! ImportExtensions.byIdForImportAction (thisImportId) accountId userEmail
                |> DBAction.bind (fun import ->
                  ImportExtensions.importFileAction import mappings
                )
                // Collect InstaceIds
                |> DBAction.map (fun instanceSpecs ->
                  {|
                     instanceIds = instanceSpecs |> List.map (fun s -> s.instanceId) |> List.map Option.get
                  |}
                )
                // Collect the json of inserted instances
                |> DBAction.bind(fun inputRec ->
                  Crosstab.getFilteredCrosstabInfoAction societeEntityId (Some inputRec.instanceIds)
                  |> DBAction.map (fun (info: Crosstab.Info) ->  sprintf "select * from %s" info.crosstabListQueryBody)
                  |> DBAction.bind DBAction.staticDataTableAction
                  |> DBAction.mapDataTableToJSON
                  |> DBAction.map (fun json ->
                    {|
                      intInstanceIds = inputRec.instanceIds |> List.map InstanceId.Get
                      json = json
                    |}
                  )
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [
                    {|
                      intInstanceIds =  [preState.maxInstanceId + 1 ;preState.maxInstanceId + 2;preState.maxInstanceId + 3]
                      // We use a string literal here because the instance ids assigned to the created instances are not deterministic.
                      // This is due to the fact that a rollback of a transaction will rollback the data created/modified, but not
                      // the state of the sequences. So the test 'import_creation' impacts the ids used here. We cannot hard-code it either
                      // as we still want to be able to run this tests indivudually.
                      json =  (Utils.reformatJSON
                                              $"""
                                                    [
                                                                {{
                                                                  "id": { preState.maxInstanceId + 1 },
                                                                  "nom": "first",
                                                                  "code_nace": null,
                                                                  "TVA": null,
                                                                  "personnes_occuppees": null,
                                                                  "company_email": null
                                                                }},
                                                                {{
                                                                  "id": { preState.maxInstanceId + 2},
                                                                  "nom": "second",
                                                                  "code_nace": null,
                                                                  "TVA": null,
                                                                  "personnes_occuppees": null,
                                                                  "company_email": null
                                                                }},
                                                                {{
                                                                  "id": {preState.maxInstanceId + 3},
                                                                  "nom": "third",
                                                                  "code_nace": null,
                                                                  "TVA": null,
                                                                  "personnes_occuppees": null,
                                                                  "company_email": null
                                                                }}
                                                    ]
                                              """)
                    |}
                    ])
            do! checkDifferenceNow ()

            // Validate that this import is not returned for import anymore
            do! ImportExtensions.byIdForImportAction thisImportId accountId userEmail
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Not found"])
            // Validate that this import is now returned for undo
            do! ImportExtensions.byIdForUndoAction thisImportId accountId userEmail
                |> DBAction.map (fun import -> import.id)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [thisImportId])

            // Check Import was updated
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.map (fun import ->
                    {|
                      step = import.step
                      userEmail = import.user_email
                      importedInstances = import.import_info?imported_instances.AsArray() |> Array.map(fun jsonValue -> jsonValue.AsInteger())
                    |}
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ {| step = 2; userEmail= "existingbob@test.com"; importedInstances = [|preState.maxInstanceId + 1;preState.maxInstanceId + 2;preState.maxInstanceId + 3|] |}])


            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }


        [<Fact>]
        member __.``import_with_empty``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
                { DBState.zeroState with
                      imports = 1L
                      maxImportId = 1
                }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference
            let thisImportId = ImportId (preState.maxImportId + 1)
            // Create import
            do! ImportExtensions.initialiseAction (newEntityId 100001) "existingbob@test.com"
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [thisImportId])
            do! checkDifferenceNow ()

            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.afterFileUploadAction
                    import
                    "fixtures/csv/import_Form_tests_valid_data_empty_columns.csv"
                    "formtests_list.csv"
                    "text/csv"
                )
                |> DBAction.run Client.client
                |> Async.Ignore

            // Aply mappings and import data
            let mappings : Mapping list =
              [
                { csvColumn = "name"; detailId = DetailId 100001 }
                { csvColumn = "notes"; detailId = DetailId 100002 }
                { csvColumn = "birthdate"; detailId = DetailId 100003 }
                { csvColumn = "count"; detailId = DetailId 100004 }
                { csvColumn = "approved"; detailId = DetailId 100005 }
                { csvColumn = "email"; detailId = DetailId 100006 }
                { csvColumn = "website"; detailId = DetailId 100007 }

              ]

            let expectedStateDifference =
                {
                  DBState.zeroState with
                   instances = 99L
                   // 3 non-empty columns * 99 lines do in detail_values
                   detailValues = 297L
                   dateDetailValues = 99L
                   integerDetailValues = 99L
                   // count goes to integer_detail_values but is empty, so no change
                   maxInstanceId = 99
                   maxDetailValueId = 297
                   maxDateDetailValueId = 99
                   maxIntegerDetailValueId = 99
                }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference

            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.importFileAction import mappings
                )
                |> DBAction.run Client.client
                |> Async.Ignore

            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }




        [<Fact>]
        member __.``import_with_error``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
              {
                DBState.zeroState with
                  imports = 1
                  maxImportId = 1
              }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference
            let thisImportId = ImportId (preState.maxImportId + 1)

            // Create import
            do! ImportExtensions.initialiseAction societeEntityId "existingbob@test.com"
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [thisImportId])
            do! checkDifferenceNow ()


            // Check afterFileUploadAction
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.afterFileUploadAction
                    import
                    "fixtures/csv/import_societe_error1.csv"
                    "societe_list.csv"
                    "text/csv"
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ [|"nom";"company_email";"personnes_occuppees"|]])

            // Apply mappings and import data
            let mappings : Mapping list =
              [
                { csvColumn = "nom"; detailId = DetailId 48 }
                { csvColumn = "company_email";       detailId = DetailId 63 }
                { csvColumn = "personnes_occuppees";      detailId = DetailId 51 }

              ]

            // Initialise a new DBState checker
            let expectedStateDifference = DBState.zeroState
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference

            // Check we get the errors reported
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.importFileAction import mappings
                )
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult [
                     "Email Megaenterpris.com invalid for detail company_email";
                     "Email gigaflop.com invalid for detail company_email";
                     "integer detail value zero for personnes_occuppees could not be parsed"
                ])

            // Check the import entry is marked as failed by setting its status to -1
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.map (fun import -> import.step)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ -1 ])

            // Check no insert took place
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``import_ddl_error``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
              {
                DBState.zeroState with
                  imports = 1
                  maxImportId = 1
              }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference
            let thisImportId = ImportId (preState.maxImportId + 1)

            // Create import
            do! ImportExtensions.initialiseAction societeEntityId "existingbob@test.com"
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [thisImportId])
            do! checkDifferenceNow ()


            // Check afterFileUploadAction
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.afterFileUploadAction
                    import
                    "fixtures/csv/import_societe_error_ddl.csv"
                    "societe_list.csv"
                    "text/csv"
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ [|"nom";"status";"employees"|]])

            // Apply mappings and import data
            let mappings : Mapping list =
              [
                { csvColumn = "nom"; detailId = DetailId 48 }
                { csvColumn = "status";       detailId = DetailId 62 }
                { csvColumn = "employees";      detailId = DetailId 51 }

              ]

            // Initialise a new DBState checker
            let expectedStateDifference = DBState.zeroState
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference

            // Check we get the errors reported
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.importFileAction import mappings
                )
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult [
                  "integer detail value unknown for personnes_occuppees could not be parsed";
                   "No proposition found for value bla to be mapped to status";
                   "integer detail value unknown for personnes_occuppees could not be parsed";
                   "integer detail value unknown for personnes_occuppees could not be parsed";
                   "No proposition found for value blo to be mapped to status";
                   "integer detail value unknown for personnes_occuppees could not be parsed";
                   "No proposition found for value bli to be mapped to status";
                   "integer detail value unknown for personnes_occuppees could not be parsed";
                   "integer detail value unknown for personnes_occuppees could not be parsed";
                   "No proposition found for value ble to be mapped to status";
                   "integer detail value unknown for personnes_occuppees could not be parsed"
              ])

            // Check the import entry is marked as failed by setting its status to -1
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.map (fun import -> import.step)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ -1 ])

            // Check no insert took place
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``import_malformed``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
              {
                DBState.zeroState with
                  imports = 1
                  maxImportId = 1
              }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference
            let thisImportId = ImportId (preState.maxImportId + 1)

            // Create import
            do! ImportExtensions.initialiseAction societeEntityId "existingbob@test.com"
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [thisImportId])
            do! checkDifferenceNow ()


            // Check afterFileUploadAction
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.afterFileUploadAction
                    import
                    "fixtures/csv/import_malformed.csv"
                    "societe_list.csv"
                    "text/csv"
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ [|"nom";"status";"employees"|]])

            // Apply mappings and import data
            let mappings : Mapping list =
              [
                { csvColumn = "nom"; detailId = DetailId 48 }
                { csvColumn = "status";       detailId = DetailId 62 }
                { csvColumn = "employees";      detailId = DetailId 51 }

              ]

            // Initialise a new DBState checker
            let expectedStateDifference = DBState.zeroState
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference

            // Check we get the errors reported
            do! ImportExtensions.byIdForImportAction (thisImportId) accountId userEmail
                |> DBAction.bind (fun import ->
                  ImportExtensions.importFileAction import mappings
                )
                |> DBAction.run Client.client
                // In this first version we don't check the errors reported, as
                // they are too many, and will need changes in the next step
                |> Async.Ignore

            // Check the import entry is marked as failed by setting its status to -1
            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.map (fun import -> import.step)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ -1 ])

            // Check no insert took place
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }



        [<Fact>]
        member __.``import_and_undo``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let expectedStateDifference =
                { DBState.zeroState with
                      imports = 1L
                      maxImportId = 1
                }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference
            let thisImportId = ImportId (preState.maxImportId + 1)
            // Create import
            do! ImportExtensions.initialiseAction (newEntityId 100001) "existingbob@test.com"
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [thisImportId])
            do! checkDifferenceNow ()

            do! ImportExtensions.byIdAction (thisImportId)
                |> DBAction.bind (fun import ->
                  ImportExtensions.afterFileUploadAction
                    import
                    "fixtures/csv/import_Form_tests_valid_data_empty_columns.csv"
                    "formtests_list.csv"
                    "text/csv"
                )
                |> DBAction.run Client.client
                |> Async.Ignore

            // Aply mappings and import data
            let mappings : Mapping list =
              [
                { csvColumn = "name"; detailId = DetailId 100001 }
                { csvColumn = "notes"; detailId = DetailId 100002 }
                { csvColumn = "birthdate"; detailId = DetailId 100003 }
                { csvColumn = "count"; detailId = DetailId 100004 }
                { csvColumn = "approved"; detailId = DetailId 100005 }
                { csvColumn = "email"; detailId = DetailId 100006 }
                { csvColumn = "website"; detailId = DetailId 100007 }

              ]

            let expectedStateDifference =
                {
                  DBState.zeroState with
                   instances = 99L
                   // 3 non-empty columns * 99 lines do in detail_values
                   detailValues = 297L
                   dateDetailValues = 99L
                   integerDetailValues = 99L
                   // count goes to integer_detail_values but is empty, so no change
                   maxInstanceId = 99
                   maxDetailValueId = 297
                   maxDateDetailValueId = 99
                   maxIntegerDetailValueId = 99
                }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference

            do! ImportExtensions.byIdForImportAction (thisImportId) accountId userEmail
                |> DBAction.bind (fun import ->
                  ImportExtensions.importFileAction import mappings
                )
                |> DBAction.run Client.client
                |> Async.Ignore

            do! checkDifferenceNow ()

            // Take state snaptshot before undo
            let expectedStateDifference =
                {
                  DBState.zeroState with
                   instances = -99L
                   // 3 non-empty columns * 99 lines do in detail_values
                   detailValues = -297L
                   dateDetailValues = -99L
                   integerDetailValues = -99L
                }
            let! preState, checkDifferenceNow =
                DBState.start Client.client expectedStateDifference

            do! ImportExtensions.byIdForUndoAction (thisImportId) accountId userEmail
                |> DBAction.bind (fun import ->
                  ImportExtensions.undoImportAction import (newEntityId 100001)
                )
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [ DBTypes.ImpactedRows 99 ])

            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
