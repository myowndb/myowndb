namespace MyOwnDB.Tests.InstanceSpecValidation

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus
open Instance
open Specs

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
            [ "Account"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module InstanceSpecValidation =
    let getDetail (id:int) = async{
        let! detailResult  = DBAction.querySingleAction "select * from details where id = %d" id
                                    |> DBAction.run<Detail.Detail> Client.client
        return detailResult |> DBResult.getFirst
    }

    // return a value of DdlChoice.t
    // As the type is opaque, we need this function to be able to assign a value of that type in our tests
    let getChoiceId (n:int) = async {
        let! choiceForDetail62Result = DdlChoice.fromId Client.client (DetailId 62) (DetailValuePropositionId n)
        return Result.get choiceForDetail62Result |> List.head
    }

    // We want the instance 69 to be usable in multiple tests.
    // We put this in a function because the id of the choice an only be retrieved with an async function, and
    // using let! here is not possible
    // The alternative is to use Async.RunSynchronously like this (but this is to be generally avoided)
    //     let choiceForDetail62Result = DdlChoice.fromId Client.client (DetailId 62) (DetailValuePropositionId 11) |> Async.RunSynchronously
    let getServerInstance69 () = async {
        // The ChoiceId is a private type that we can only build by querying the database, which we do here
        let! choiceForDetail62Result = DdlChoice.fromId Client.client (DetailId 62) (DetailValuePropositionId 11)
        let choiceForDetail62 = Result.get choiceForDetail62Result |> List.head
        let instance69 =
                     {  entityId = newEntityId 11
                        instanceId = Some (newInstanceId 69)
                        detailValues =
                            // no value for nom, company_email
                            // status sprl
                            [{ detailId = DetailId 62
                               values = [{ detailValueId = Some (DdlDetailValueId 15)
                                           value = ServerValidated (ServerValue.DdlValueChoice (Some choiceForDetail62)) }] };
                             // personnes_occupees
                             { detailId = DetailId 51
                               values = [{ detailValueId = Some (IntegerDetailValueId 5)
                                           value = ServerValidated (ServerValue.Integer (Some 2L)) }] };
                            // memo
                             { detailId = DetailId 55
                               values = [{ detailValueId = Some (DetailValueId 299)
                                           value = ServerValidated (ServerValue.LongText (Some "") )}] };
                             // code_nace
                             { detailId = DetailId 49
                               values = [{ detailValueId = Some (DetailValueId 294)
                                           value = ServerValidated (ServerValue.Simple (Some "hjhjhjk")) }] };
                             // TVA
                             { detailId = DetailId 50
                               values = [{ detailValueId = Some (DetailValueId 295)
                                           value = ServerValidated (ServerValue.Simple (Some "")) }] };
                             // adresse
                             { detailId = DetailId 52
                               values = [{ detailValueId = Some (DetailValueId 296)
                                           value = ServerValidated (ServerValue.Simple (Some "rue de perck")) }] }
                             // telephone
                             { detailId = DetailId 53
                               values = [{ detailValueId = Some (DetailValueId 297)
                                           value = ServerValidated (ServerValue.Simple (Some "")) }] };
                             // fax
                             { detailId = DetailId 54
                               values = [{ detailValueId = Some (DetailValueId 298)
                                           value = ServerValidated (ServerValue.Simple (Some "")) }] };
                                           ] }
        return instance69
    }
    let validUpdatedInstance69 =
                     {  entityId = newEntityId 11
                        instanceId = Some (newInstanceId 69)
                        detailValues =
                            // no value for nom, company_email
                            // status sprl
                            [{ detailId = DetailId 62
                               values = [{ detailValueId = Some (DdlDetailValueId 15)
                                           value = ClientProposed (ClientValue.DdlValueChoice (Some 12)) }] };
                             // personnes_occupees
                             { detailId = DetailId 51
                               values = [{ detailValueId = Some (IntegerDetailValueId 5)
                                           value = ClientProposed (ClientValue.Integer (Some "4")) }] };
                            // memo
                             { detailId = DetailId 55
                               values = [{ detailValueId = Some (DetailValueId 299)
                                           value = ClientProposed (ClientValue.LongText (Some "new memo") )}] };
                             // code_nace
                             { detailId = DetailId 49
                               values = [{ detailValueId = Some (DetailValueId 294)
                                           value = ClientProposed (ClientValue.Simple (Some "UNKNOWN")) }] };
                             // TVA
                             { detailId = DetailId 50
                               values = [{ detailValueId = Some (DetailValueId 295)
                                           value = ClientProposed (ClientValue.Simple (Some "0777666444")) }] };
                             // adresse
                             { detailId = DetailId 52
                               values = [{ detailValueId = Some (DetailValueId 296)
                                           value = ClientProposed (ClientValue.Simple (Some "new street name")) }] }
                             // telephone
                             { detailId = DetailId 53
                               values = [{ detailValueId = Some (DetailValueId 297)
                                           value = ClientProposed (ClientValue.Simple (Some "+32000000000")) }] };
                             // fax
                             { detailId = DetailId 54
                               values = [{ detailValueId = Some (DetailValueId 298)
                                           value = ClientProposed (ClientValue.Simple (Some "+32000000001")) }] };
                                           ] }
    let getValidatedUpdateDetailValues ()= async {
      let! choiceId = getChoiceId 12
      return
        [{ detailId = DetailId 62
           values =
           [{ detailValueId = Some (DdlDetailValueId 15)
              value = ServerValidated (ServerValue.DdlValueChoice (Some choiceId )) }] };
         { detailId = DetailId 51
           values = [{ detailValueId = Some (IntegerDetailValueId 5)
                       value = ServerValidated (ServerValue.Integer (Some 4L)) }] };
         { detailId = DetailId 55
           values = [{ detailValueId = Some (DetailValueId 299)
                       value = ServerValidated (ServerValue.LongText (Some "new memo")) }] };
         { detailId = DetailId 49
           values = [{ detailValueId = Some (DetailValueId 294)
                       value = ServerValidated (ServerValue.Simple (Some "UNKNOWN")) }] };
         { detailId = DetailId 50
           values = [{ detailValueId = Some (DetailValueId 295)
                       value = ServerValidated (ServerValue.Simple (Some "0777666444")) }] };
         { detailId = DetailId 52
           values = [{ detailValueId = Some (DetailValueId 296)
                       value = ServerValidated (ServerValue.Simple (Some "new street name")) }] };
         { detailId = DetailId 53
           values = [{ detailValueId = Some (DetailValueId 297)
                       value = ServerValidated (ServerValue.Simple (Some "+32000000000")) }] };
         { detailId = DetailId 54
           values = [{ detailValueId = Some (DetailValueId 298)
                       value = ServerValidated (ServerValue.Simple (Some "+32000000001")) }] }]
    }
    let validNewInstance =
                     {  entityId = newEntityId 11
                        instanceId = None
                        detailValues =
                            // no value for nom, company_email
                            // status sprl
                            [{ detailId = DetailId 62
                               values = [{ detailValueId = None
                                           value = ClientProposed (ClientValue.DdlValueChoice (Some 12)) }] };
                             // personnes_occupees
                             { detailId = DetailId 51
                               values = [{ detailValueId = None
                                           value = ClientProposed (ClientValue.Integer (Some "4")) }] };
                            // memo
                             { detailId = DetailId 55
                               values = [{ detailValueId = None
                                           value = ClientProposed (ClientValue.LongText (Some "new memo") )}] };
                             // code_nace
                             { detailId = DetailId 49
                               values = [{ detailValueId = None
                                           value = ClientProposed (ClientValue.Simple (Some "UNKNOWN")) }] };
                             // TVA
                             { detailId = DetailId 50
                               values = [{ detailValueId = None
                                           value = ClientProposed (ClientValue.Simple (Some "0777666444")) }] };
                             // adresse
                             { detailId = DetailId 52
                               values = [{ detailValueId = None
                                           value = ClientProposed (ClientValue.Simple (Some "new street name")) }] }
                             // telephone
                             { detailId = DetailId 53
                               values = [{ detailValueId = None
                                           value = ClientProposed (ClientValue.Simple (Some "+32000000000")) }] };
                             // fax
                             { detailId = DetailId 54
                               values = [{ detailValueId = None
                                           value = ClientProposed (ClientValue.Simple (Some "+32000000001")) }] };
                                           ] }
    let getValidatedDetailValues ()= async {
      let! choiceId11 = getChoiceId 12
      return
        [{ detailId = DetailId 62
           values =
           [{ detailValueId = None
              value = ServerValidated (ServerValue.DdlValueChoice (Some choiceId11 )) }] };
         { detailId = DetailId 51
           values = [{ detailValueId = None
                       value = ServerValidated (ServerValue.Integer (Some 4L)) }] };
         { detailId = DetailId 55
           values = [{ detailValueId = None
                       value = ServerValidated (ServerValue.LongText (Some "new memo")) }] };
         { detailId = DetailId 49
           values = [{ detailValueId = None
                       value = ServerValidated (ServerValue.Simple (Some "UNKNOWN")) }] };
         { detailId = DetailId 50
           values = [{ detailValueId = None
                       value = ServerValidated (ServerValue.Simple (Some "0777666444")) }] };
         { detailId = DetailId 52
           values = [{ detailValueId = None
                       value = ServerValidated (ServerValue.Simple (Some "new street name")) }] };
         { detailId = DetailId 53
           values = [{ detailValueId = None
                       value = ServerValidated (ServerValue.Simple (Some "+32000000000")) }] };
         { detailId = DetailId 54
           values = [{ detailValueId = None
                       value = ServerValidated (ServerValue.Simple (Some "+32000000001")) }] }]
    }

    [<Trait("Category", "models")>]
    [<Trait("Category", "instance")>]
    type InstanceSpecValidationTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``instance_spec_validation_without_db``() = async {
          // *****************************************************
          // check count is one
          do! MyOwnDBLib.Instance.checkCountIsOne "dummy log message" 1L
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult  [1L])

          do! MyOwnDBLib.Instance.checkCountIsOne "log message" 0L
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult  ["Ids inconsistency count is 0"])

          // *****************************************************
          // ddlValuesUpdateAdditionalJoin


          // a value of type DdlChoice.t
          let! choiceId = getChoiceId 11
          // Value instances
          let simpleValue = ClientProposed (ClientValue.Simple (Some "text"))
          let longValue = ClientProposed (ClientValue.LongText (Some "text"))
          let emailValue = ClientProposed (ClientValue.Email (Some "info@myowndb.com"))
          let urlValue = ClientProposed (ClientValue.WebURL (Some "http://www.myowndb.com"))
          let dateValue = ClientProposed (ClientValue.DateAndTime (Some "20221105"))
          let ddlSomeValue = ClientProposed (ClientValue.DdlValueChoice (Some 55))
          let ddlNoneValue = ClientProposed (ClientValue.DdlValueChoice None)
          let integerValue = ClientProposed (ClientValue.Integer (Some "55"))
          // a server validated value
          let serverValue = ServerValidated (ServerValue.DdlValueChoice (Some choiceId))
          // valueSpecs
          let simpleValueSpec = {detailValueId=Some (DetailValueId 56000); value=simpleValue}
          let longValueSpec = {detailValueId=Some (DetailValueId 56000); value=longValue}
          let emailValueSpec = {detailValueId=Some (DetailValueId 56000); value=emailValue}
          let urlValueSpec = {detailValueId=Some (DetailValueId 56000); value=urlValue}
          let dateValueSpec = {detailValueId=Some (DateDetailValueId 56000); value=dateValue}
          let integerValueSpec = {detailValueId=Some (IntegerDetailValueId 56000); value=integerValue}
          let ddlValueSpec = { detailValueId = Some (DdlDetailValueId 56000); value = ddlSomeValue }
          // ids
          let entityId = newEntityId 11
          let instanceId = newInstanceId 69
          let detailId = DetailId 48

          // needs an additional join
          MyOwnDBLib.Instance.ddlValuesUpdateAdditionalJoin ddlValueSpec entityId instanceId detailId
          |> should equal "join detail_value_propositions dvp on (dvp.id=55 and dvp.detail_id=48)"

          // no additional join expected
          MyOwnDBLib.Instance.ddlValuesUpdateAdditionalJoin {ddlValueSpec with value=ddlNoneValue} entityId instanceId detailId
          |> should equal ""

          // check that values which should never happen throw an exception
          [| serverValue; simpleValue; longValue ;emailValue; urlValue; dateValue; integerValue|]
          |> Array.iter
              (fun value ->
                let testedValue = {ddlValueSpec with value=simpleValue}
                (fun () -> MyOwnDBLib.Instance.ddlValuesUpdateAdditionalJoin testedValue entityId instanceId detailId |> ignore)
                |> should (throwWithMessage (sprintf "should never happen to have this value here %A" testedValue)) typeof<System.Exception>
              )
          |> ignore


          // ********************************************************************************
          // getUpdateQueryDetails

          // Simple and LongText values are in table detail_values and require no additional join for ids validation
          MyOwnDBLib.Instance.getUpdateQueryDetails  simpleValueSpec entityId instanceId detailId
          |> should equal ("detail_values",56000,"")
          MyOwnDBLib.Instance.getUpdateQueryDetails longValueSpec entityId instanceId detailId
          |> should equal ("detail_values",56000,"")
          MyOwnDBLib.Instance.getUpdateQueryDetails dateValueSpec entityId instanceId detailId
          |> should equal ("date_detail_values",56000,"")
          MyOwnDBLib.Instance.getUpdateQueryDetails integerValueSpec entityId instanceId detailId
          |> should equal ("integer_detail_values",56000,"")
          MyOwnDBLib.Instance.getUpdateQueryDetails ddlValueSpec entityId instanceId detailId
          |> should equal ("ddl_detail_values",56000,"join detail_value_propositions dvp on (dvp.id=55 and dvp.detail_id=48)")


          // ********************************************************************************
          // getNewDetailValueQueryDetails
          MyOwnDBLib.Instance.getNewDetailValueQueryDetails  simpleValue detailId
          |> should equal ""
          MyOwnDBLib.Instance.getNewDetailValueQueryDetails longValue detailId
          |> should equal ""
          MyOwnDBLib.Instance.getNewDetailValueQueryDetails dateValue detailId
          |> should equal ""
          MyOwnDBLib.Instance.getNewDetailValueQueryDetails integerValue detailId
          |> should equal ""
          MyOwnDBLib.Instance.getNewDetailValueQueryDetails ddlSomeValue detailId
          |> should equal "join detail_value_propositions dvp on (dvp.id=55 and dvp.detail_id=48)"
          MyOwnDBLib.Instance.getNewDetailValueQueryDetails ddlNoneValue detailId
          |> should equal ""

          // ********************************************************************************
          // isDetailValueIdDataTypeConsistent
          // We don't tests all combination. First test that all value specs except date detect the inconsistency when
          // holding a DateDetailValueId, then test dateValueSpec separately.
          [| simpleValueSpec; longValueSpec; emailValueSpec; urlValueSpec; integerValueSpec |]
          |> Array.iter (fun vSpec ->
            MyOwnDBLib.Instance.isDetailValueIdDataTypeConsistent vSpec
            |> should equal true
            MyOwnDBLib.Instance.isDetailValueIdDataTypeConsistent {vSpec with detailValueId = None}
            |> should equal true
            MyOwnDBLib.Instance.isDetailValueIdDataTypeConsistent {vSpec with detailValueId = Some (DateDetailValueId 44)}
            |> should equal false
          )
          MyOwnDBLib.Instance.isDetailValueIdDataTypeConsistent dateValueSpec
          |> should equal true
          MyOwnDBLib.Instance.isDetailValueIdDataTypeConsistent {dateValueSpec with detailValueId = None}
          |> should equal true
          MyOwnDBLib.Instance.isDetailValueIdDataTypeConsistent {dateValueSpec with detailValueId = Some (DetailValueId 44)}
          |> should equal false

          // ********************************************************************************
          // checkDetailValueIdDataTypeConsistency
          // spec is returned as is if valid
          do! DBAction.retn validNewInstance
              |> DBAction.bind MyOwnDBLib.Instance.checkDetailValueIdDataTypeConsistency
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [validNewInstance])

          // copy the spec from the module level so it can be edited with out Lenses
          let validNewInstance = validNewInstance
          let validUpdatedInstance69 = validUpdatedInstance69

          // spec with one invalid value
          let invalidSpec: Specs.InstanceSpec =
              Lens.With <@ validNewInstance.detailValues.[0].values.[0].detailValueId @> (Some(DateDetailValueId 55))
          do! DBAction.retn invalidSpec
              |> DBAction.bind MyOwnDBLib.Instance.checkDetailValueIdDataTypeConsistency
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult [sprintf "detail value id type and value inconsistency detected in %A" [invalidSpec.detailValues[0]]])

          // spec with multiple invalid values
          let invalidSpec: Specs.InstanceSpec =
              Lens.With <@ validNewInstance.detailValues.[1].values.[0].detailValueId @> (Some(DateDetailValueId 55))
                |> Lens.Bind (fun (r: Specs.InstanceSpec) -> <@ r.detailValues.[3].values.[0].detailValueId @>) (Some(IntegerDetailValueId 56))
          do! DBAction.retn invalidSpec
              |> DBAction.bind MyOwnDBLib.Instance.checkDetailValueIdDataTypeConsistency
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult [sprintf "detail value id type and value inconsistency detected in %A" [invalidSpec.detailValues[1];invalidSpec.detailValues[3]]])


          // ********************************************************************************
          // filterDetailValuesWithClientProposed

          // when we have only client proposed values, the instance spec is not modified
          MyOwnDBLib.Instance.filterDetailValuesWithClientProposed validNewInstance
          |> should equal validNewInstance
          // same for an update
          MyOwnDBLib.Instance.filterDetailValuesWithClientProposed validUpdatedInstance69
          |> should equal validUpdatedInstance69

          // Extract the list for easier manipulation
          let baseDetailValuesList =  validNewInstance.detailValues

          // get the choice id value wrapped by a ServerValue, which has a private constructor
          let! choiceId = getChoiceId 11
          // define the detailValues list we will work with.
          // We replace ClientProposed values by ServerValidated values to check these are removed by the function under test
          let testedDetailValuesList =
            baseDetailValuesList
            |> List.updateAt
              0
              { detailId = DetailId 62
                values = [{ detailValueId = None
                            value = ServerValidated (ServerValue.DdlValueChoice (Some choiceId)) }] }
            |> List.updateAt
                2
                { detailId = DetailId 55
                  values = [{ detailValueId = None
                              value = ServerValidated (ServerValue.LongText (Some "new memo") )}] }

          // remove the elements that are expected to be filtered out
          let expectedFilteredList =
            testedDetailValuesList
            // start by the highest index to avoid chging indexes of other elements to be deleted
            |> List.deleteAt 2
            |> List.deleteAt 0

          // build specs with our updated lists
          let testedSpec =
            Lens.With <@ validNewInstance.detailValues @> testedDetailValuesList
          let expectedSpec =
            Lens.With <@ validNewInstance.detailValues @> expectedFilteredList

          MyOwnDBLib.Instance.filterDetailValuesWithClientProposed testedSpec
          |> should equal expectedSpec


          // ********************************************************************************
          // filterKeepOnlyClientProposedValues

          // when we have only client proposed values, the instance spec is not modified
          MyOwnDBLib.Instance.filterKeepOnlyClientProposedValues validNewInstance
          |> should equal validNewInstance
          // same for an update
          MyOwnDBLib.Instance.filterKeepOnlyClientProposedValues validUpdatedInstance69
          |> should equal validUpdatedInstance69

          // extract the list for easier manipulation
          let baseDetailValuesList =  validNewInstance.detailValues

          // get the choice id value wrapped by a ServerValue, which has a private constructor
          let! choiceId = getChoiceId 11
          // define the detailValues list we will work with.
          // We replace ClientProposed values by ServerValidated values to check these are removed by the function under test
          let testedDetailValuesList =
            baseDetailValuesList
            |> List.updateAt
              0
              { detailId = DetailId 62
                values = [{ detailValueId = None
                            value = ServerValidated (ServerValue.DdlValueChoice (Some choiceId)) }] }
            |> List.updateAt
                2
                { detailId = DetailId 55
                  values = [{ detailValueId = None
                              value = ServerValidated (ServerValue.LongText (Some "new memo") )}
                            { detailValueId = None
                              value = ClientProposed (ClientValue.LongText (Some "updated memo") )}
                           ] }

          // remove the elements that are expected to be filtered out
          // and place the expected update list of values as expected at index 3
          let expectedFilteredList =
            testedDetailValuesList
            // start by the highest index to avoid chging indexes of other elements to be deleted
            |> List.updateAt 2
                       { detailId = DetailId 55
                         values = [
                                  // only the ClientProposed value is expected to be left in place
                                  { detailValueId = None
                                    value = ClientProposed (ClientValue.LongText (Some "updated memo") )}
                                  ] }
            |> List.deleteAt 0

          // build specs with our updated lists
          let testedSpec =
            Lens.With <@ validNewInstance.detailValues @> testedDetailValuesList
          let expectedSpec =
            Lens.With <@ validNewInstance.detailValues @> expectedFilteredList

          MyOwnDBLib.Instance.filterKeepOnlyClientProposedValues testedSpec
          |> should equal expectedSpec


          // ********************************************************************************
          // validateValuesinDetailValueAction

          // Valid ddl choice
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.DdlValueChoice (Some 11)) }
            ]
          let expectedValueSpecs =
            [{ detailValueId = None
              // choiceId was built earlier for this DdlValueChoice
               value = ServerValidated (ServerValue.DdlValueChoice (Some choiceId)) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 62)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [expectedValueSpecs])

          // Invalid choice id
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.DdlValueChoice (Some 55)) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 62)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult ["DdlDetailValue 55 invalid for detail status"])

          // valid integer
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.Integer (Some "4")) }
            ]
          let expectedValueSpecs =
            [{ detailValueId = None
               value = ServerValidated (ServerValue.Integer (Some 4)) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 51)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [expectedValueSpecs])

          // invalid integer
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.Integer (Some "four")) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 51)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult ["integer detail value four for personnes_occuppees could not be parsed"])

          // valid date
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.DateAndTime (Some "2022-11-09")) }
            ]
          let expectedValueSpecs =
            [{ detailValueId = None
               value = ServerValidated (ServerValue.DateAndTime (Some (DateAndTime.fromString "2022-11-09" |> Result.get))) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 76)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [expectedValueSpecs])

          // invalid date
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.DateAndTime (Some "four")) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 76)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult ["DateDetailValue four invalid for detail Published"])

          // valid email
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.Email (Some "info@myowndb.com")) }
            ]
          let expectedValueSpecs =
            [{ detailValueId = None
               value = ServerValidated (ServerValue.Email (Some (Email.fromString "info@myowndb.com" |> Result.get))) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 63)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [expectedValueSpecs])

          // invalid email
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.Email (Some "info")) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 63)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult ["Email info invalid for detail company_email"])

          // valid url
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.WebURL (Some "http://www.myowndb.com")) }
            ]
          let expectedValueSpecs =
            [{ detailValueId = None
               value = ServerValidated (ServerValue.WebURL (Some (WebURL.fromString "http://www.myowndb.com" |> Result.get))) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 24)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [expectedValueSpecs])

          // invalid email
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.WebURL (Some "info")) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 24)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult ["WebURL info invalid for detail url_detail"])

          // valid simple value, no invalid values
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.Simple (Some "short text")) }
            ]
          let expectedValueSpecs =
            [{ detailValueId = None
               value = ServerValidated (ServerValue.Simple (Some "short text")) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 49)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [expectedValueSpecs])

          // valid long value, no invalid values
          let valueSpecs =
            [{ detailValueId = None
               value = ClientProposed (ClientValue.LongText (Some "long text")) }
            ]
          let expectedValueSpecs =
            [{ detailValueId = None
               value = ServerValidated (ServerValue.LongText (Some "long text")) }
            ]
          do! MyOwnDBLib.Instance.validateValuesinDetailValueAction
                (DetailId 55)
                valueSpecs
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [expectedValueSpecs])
        }

        [<Fact>]
        member __.``instance_spec_validation_without_db_getValidatedDetailValuesAction``() = async {
          let validUpdatedInstance69 = validUpdatedInstance69
          let validNewInstance = validNewInstance
          let! choiceId11 = getChoiceId 12
          let detailValuesToValidate = validNewInstance.detailValues
          let! validatedDetailValues = getValidatedDetailValues()

          // All ok
          do! MyOwnDBLib.Instance.getValidatedDetailValuesAction detailValuesToValidate
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [validatedDetailValues])

          // One error
          let detailValuesToValidate =
            validNewInstance.detailValues
            |> List.updateAt 1
              { detailId = DetailId 51
                values = [{ detailValueId = None
                            value = ClientProposed (ClientValue.Integer (Some "four")) }] }
          do! MyOwnDBLib.Instance.getValidatedDetailValuesAction detailValuesToValidate
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult ["integer detail value four for personnes_occuppees could not be parsed"])

          // two error
          let detailValuesToValidate =
            validNewInstance.detailValues
            |> List.updateAt 0
              { detailId = DetailId 62
                values = [{ detailValueId = None
                            value = ClientProposed (ClientValue.DdlValueChoice (Some 55)) }] }
            |> List.updateAt 1
              { detailId = DetailId 51
                values = [{ detailValueId = None
                            value = ClientProposed (ClientValue.Integer (Some "four")) }] }
            |> List.append
              [ { detailId = DetailId 63
                  values = [{ detailValueId = None
                              value = ClientProposed (ClientValue.Integer (Some "invalidemail")) }] }
              ]
          do! MyOwnDBLib.Instance.getValidatedDetailValuesAction detailValuesToValidate
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult
                             [ "Email invalidemail invalid for detail company_email";
                               "DdlDetailValue 55 invalid for detail status";
                               "integer detail value four for personnes_occuppees could not be parsed"])
        }


        [<Fact>]
        member __.``instance_spec_validation_checkIdsConsistency_and_sanitise``() = async {
          let validNewInstance = validNewInstance
          let! validatedDetailValues = getValidatedDetailValues()
          let! validatedUpdateDetailValues = getValidatedUpdateDetailValues()
          let validUpdatedInstance69 = validUpdatedInstance69

          // build update specs
          let updateDetailValuesWithError =
            validUpdatedInstance69.detailValues
            |> List.updateAt 0
              { detailId = DetailId 62
                values = [{ detailValueId = None
                            value = ClientProposed (ClientValue.DdlValueChoice (Some 55)) }] }
            |> List.updateAt 1
              { detailId = DetailId 51
                values = [{ detailValueId = None
                            value = ClientProposed (ClientValue.Integer (Some "four")) }] }
          let updateDetailValuesWithValidationError =
            validNewInstance.detailValues
            |> List.updateAt 1
              { detailId = DetailId 51
                values = [{ detailValueId = None
                            value = ClientProposed (ClientValue.Integer (Some "four")) }] }

          let specUpdateWithValidationError =
            Lens.With <@ validNewInstance.detailValues @> updateDetailValuesWithValidationError
          let specUpdateWithError =
            Lens.With <@ validNewInstance.detailValues @> updateDetailValuesWithError
          let specValidatedUpdateInstance =
            Lens.With <@ validUpdatedInstance69.detailValues @> validatedUpdateDetailValues

          // build creation specs
          let creationDetailValuesWithError =
            validNewInstance.detailValues
            |> List.updateAt 0
              { detailId = DetailId 62
                values = [{ detailValueId = None
                            value = ClientProposed (ClientValue.DdlValueChoice (Some 55)) }] }
            |> List.updateAt 1
              { detailId = DetailId 51
                values = [{ detailValueId = None
                            value = ClientProposed (ClientValue.Integer (Some "four")) }] }

          let creationDetailValuesWithValidationError =
            validNewInstance.detailValues
            |> List.updateAt 1
              { detailId = DetailId 51
                values = [{ detailValueId = None
                            value = ClientProposed (ClientValue.Integer (Some "four")) }] }

          let specNewWithValidationError =
            Lens.With <@ validNewInstance.detailValues @> creationDetailValuesWithValidationError
          let specNewWithError =
            Lens.With <@ validNewInstance.detailValues @> creationDetailValuesWithError
          let specValidatedNewInstance =
            Lens.With <@ validNewInstance.detailValues @> validatedDetailValues


          // ********************************************************************************
          // checkIdsConsistency

          // no error
          do! MyOwnDBLib.Instance.checkIdsConsistency validNewInstance
              |> DBAction.run Client.client
              |> Async.map ( checkOkDBResult [validNewInstance] )
          do! MyOwnDBLib.Instance.checkIdsConsistency validUpdatedInstance69
              |> DBAction.run Client.client
              |> Async.map ( checkOkDBResult [validUpdatedInstance69] )

          // -------
          // creation
          // with errors ddl error, fails id consistency check
          do! MyOwnDBLib.Instance.checkIdsConsistency specNewWithError
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult
                             [ "Ids inconsistency count is 0" ])

          // with validation errors, no id consistency should be raised
          do! MyOwnDBLib.Instance.checkIdsConsistency specNewWithValidationError
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [specNewWithValidationError])

          // -------
          // update
          // with errors ddl error, fails id consistency check
          do! MyOwnDBLib.Instance.checkIdsConsistency specUpdateWithError
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult
                             [ "Ids inconsistency count is 0" ])

          // with validation errors, no id consistency should be raised
          do! MyOwnDBLib.Instance.checkIdsConsistency specUpdateWithValidationError
              |> DBAction.run Client.client
              |> Async.map (checkOkDBResult [specUpdateWithValidationError])

          // ********************************************************************************
          // sanitiseSpecAction

          // no error
          do! MyOwnDBLib.Instance.sanitiseSpecAction validNewInstance
              |> DBAction.run Client.client
              |> Async.map ( checkOkDBResult [specValidatedNewInstance] )
          do! MyOwnDBLib.Instance.sanitiseSpecAction validUpdatedInstance69
              |> DBAction.run Client.client
              |> Async.map ( checkOkDBResult [specValidatedUpdateInstance] )

          // -------
          // creation
          // with errors ddl error, fails id consistency check
          do! MyOwnDBLib.Instance.sanitiseSpecAction specNewWithError
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult
                             [ "Ids inconsistency count is 0" ])

          // with validation errors, no id consistency should be raised
          do! MyOwnDBLib.Instance.sanitiseSpecAction specNewWithValidationError
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult ["integer detail value four for personnes_occuppees could not be parsed"])

          // -------
          // update
          // with errors ddl error, fails id consistency check
          do! MyOwnDBLib.Instance.sanitiseSpecAction specUpdateWithError
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult
                             [ "Ids inconsistency count is 0" ])

          // with validation errors, no id consistency should be raised
          do! MyOwnDBLib.Instance.sanitiseSpecAction specUpdateWithValidationError
              |> DBAction.run Client.client
              |> Async.map (checkErrorResult ["integer detail value four for personnes_occuppees could not be parsed"])
        }

        [<Fact>]
        member __.``instance_spec_checkDetailValueIdsConsistencyUpdateAction``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let validUpdateSpec = validUpdatedInstance69

            let expectedStateDifference = DBState.zeroState
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 0L, "select count(*) from instances where entity_id=11"
                      0L, "select count(*) from instances where id=69" ] expectedStateDifference


            // define values for easier access
            let entityId = validUpdateSpec.entityId
            let instanceId = validUpdateSpec.instanceId |> Option.get
            let detailValues = validUpdateSpec.detailValues

            // Check a valid value is accepted
            // detailID 62, proposition id 12, ddl detail value id 15
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[0].values[0]
                  entityId
                  instanceId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1L])

            //--------------------------------
            // test a ddl_detail_value
            //--------------------------------
            // tests the join with the value propositions table

            // ddl value proposition id is incorrect, it is for detail id 18
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  { detailValues[0].values[0] with value = ClientProposed (ClientValue.DdlValueChoice (Some 1))}
                  entityId
                  instanceId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // detailId is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[0].values[0]
                  entityId
                  instanceId
                  (DetailId 18)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // proposition id and detailId are consistent, but incorrect in relation to the detail value being updated
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  { detailValues[0].values[0] with value = ClientProposed (ClientValue.DdlValueChoice (Some 1))}
                  entityId
                  instanceId
                  (DetailId 18)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // detailValueId does not exist
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  { detailValues[0].values[0] with detailValueId = Some (DdlDetailValueId 99389)}
                  entityId
                  instanceId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // detailValueId exists but for another detail id (i.e. detail id 78)
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  { detailValues[0].values[0] with detailValueId = Some (DdlDetailValueId 70)}
                  entityId
                  instanceId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // instance_id is incorrect (but same entity id)
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[0].values[0]
                  entityId
                  (newInstanceId 71)
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // instance_id is incorrect (and for another entity id)
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[0].values[0]
                  entityId
                  (newInstanceId 75)
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // entity_id is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[0].values[0]
                  (newEntityId 12)
                  instanceId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // instance_id and entity_id are consistent, but incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[0].values[0]
                  (newEntityId 12)
                  (newInstanceId 75)
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            //--------------------------------
            // test a integer_detail_value
            //--------------------------------

            // detailId is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[1].values[0]
                  entityId
                  instanceId
                  (DetailId 18)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // detailValueId does not exist
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  { detailValues[1].values[0] with detailValueId = Some (IntegerDetailValueId 99389)}
                  entityId
                  instanceId
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // detailValueId exists but for another detail id (i.e. detail id 78)
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  { detailValues[1].values[0] with detailValueId = Some (IntegerDetailValueId 51)}
                  entityId
                  instanceId
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // instance_id is incorrect (but same entity id)
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[1].values[0]
                  entityId
                  (newInstanceId 71)
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // instance_id is incorrect (and for another entity id)
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[1].values[0]
                  entityId
                  (newInstanceId 75)
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // entity_id is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[1].values[0]
                  (newEntityId 12)
                  instanceId
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // instance_id and entity_id are consistent, but incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyUpdateAction
                  detailValues[1].values[0]
                  (newEntityId 12)
                  (newInstanceId 75)
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // Now check nothing was added to the database
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }


        [<Fact>]
        member __.``instance_spec_checkDetailValueIdsConsistencyNewDetailValueAction``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // We test the validation of new detail values for an existing instance
            // So we use the update spec in which we will set detail value ids to None
            // spec for the instance to be inserted
            let validUpdateSpec = validUpdatedInstance69

            let expectedStateDifference = DBState.zeroState
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 0L, "select count(*) from instances where entity_id=11"
                      0L, "select count(*) from instances where id=69" ] expectedStateDifference


            // define values for easier access
            let entityId = validUpdateSpec.entityId
            let instanceId = validUpdateSpec.instanceId |> Option.get
            let detailValues = validUpdateSpec.detailValues

            let ddlValueSpec = {detailValues[0].values[0] with detailValueId = None}
            // Check a valid value is accepted
            // detailID 62, proposition id 12
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  ddlValueSpec
                  entityId
                  instanceId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1L])

            //--------------------------------
            // test a ddl_detail_value
            //--------------------------------
            // tests the join with the value propositions table

            // ddl value proposition id is incorrect, it is for detail id 18
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  { ddlValueSpec with value = ClientProposed (ClientValue.DdlValueChoice (Some 1))}
                  entityId
                  instanceId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // detailId is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  ddlValueSpec
                  entityId
                  instanceId
                  (DetailId 18)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // proposition id and detailId are consistent, but incorrect in relation to the detail value being updated
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  { ddlValueSpec with value = ClientProposed (ClientValue.DdlValueChoice (Some 1))}
                  entityId
                  instanceId
                  (DetailId 18)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])


            // instance_id is incorrect (and for another entity id)
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  ddlValueSpec
                  entityId
                  (newInstanceId 75)
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // entity_id is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  ddlValueSpec
                  (newEntityId 12)
                  instanceId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // instance_id and entity_id are consistent, but incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  ddlValueSpec
                  (newEntityId 12)
                  (newInstanceId 75)
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            //--------------------------------
            // test a integer_detail_value
            //--------------------------------

            let intValueSpec = {detailValues[1].values[0] with detailValueId = None}
            // detailId is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  intValueSpec
                  entityId
                  instanceId
                  (DetailId 18)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])



            // instance_id is incorrect (and for another entity id)
            // Testing with another instanceId for the same entity will not trigger an error
            // because we look at the creation of a new detail value.
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  intValueSpec
                  entityId
                  (newInstanceId 75)
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // entity_id is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  intValueSpec
                  (newEntityId 12)
                  instanceId
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // instance_id and entity_id are consistent, but incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewDetailValueAction
                  intValueSpec
                  (newEntityId 12)
                  (newInstanceId 75)
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // Now check nothing was added to the database
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``instance_spec_checkDetailValueIdsConsistencyNewInstanceAction``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // We test the validation of new detail values for an existing instance
            // So we use the update spec in which we will set detail value ids to None
            // spec for the instance to be inserted
            let validNewSpec = validNewInstance

            let expectedStateDifference = DBState.zeroState
            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 0L, "select count(*) from instances where entity_id=11"
                    ] expectedStateDifference


            // define values for easier access
            let entityId = validNewSpec.entityId
            let detailValues = validNewSpec.detailValues
            let ddlValueSpec = detailValues[0].values[0]

            // Check a valid value is accepted
            // detailID 62, proposition id 12
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewInstanceAction
                  ddlValueSpec
                  entityId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1L])

            //--------------------------------
            // test a ddl_detail_value
            //--------------------------------
            // tests the join with the value propositions table

            // ddl value proposition id is incorrect, it is for detail id 18
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewInstanceAction
                  { ddlValueSpec with value = ClientProposed (ClientValue.DdlValueChoice (Some 1))}
                  entityId
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // detailId is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewInstanceAction
                  ddlValueSpec
                  entityId
                  (DetailId 18)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            // proposition id and detailId are consistent, but incorrect in relation to the detail value being updated
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewInstanceAction
                  { ddlValueSpec with value = ClientProposed (ClientValue.DdlValueChoice (Some 1))}
                  entityId
                  (DetailId 18)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])


            // entity_id is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewInstanceAction
                  ddlValueSpec
                  (newEntityId 12)
                  detailValues[0].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])

            //--------------------------------
            // test a integer_detail_value
            //--------------------------------

            let intValueSpec = detailValues[1].values[0]

            // All is correct
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewInstanceAction
                  intValueSpec
                  entityId
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1L])

            // detailId is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewInstanceAction
                  intValueSpec
                  entityId
                  (DetailId 18)
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])


            // entity_id is incorrect
            do! MyOwnDBLib.Instance.checkDetailValueIdsConsistencyNewInstanceAction
                  ddlValueSpec
                  (newEntityId 12)
                  detailValues[1].detailId
                |> DBAction.run Client.client
                |> Async.map (checkErrorResult ["Ids inconsistency count is 0"])


            // Now check nothing was added to the database
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
