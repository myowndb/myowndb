namespace MyOwnDB.Tests.FileAttachment

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus
open ServerFileAttachmentExtensions

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module FileAttachment =

    [<Trait("Category", "models")>]
    [<Trait("Category", "file_attachment")>]
    type DetailTests() =

        [<Fact>]
        member __.``FileAttachment_ParseYaml``() = async{
            // Test with all keys present and uploaded true
            let yaml = """
                 ---
                 :uploaded: true
                 :filename: logo-Ubuntu.png
                 :s3_key: 1/6/101/202/1215
                 :valueid: 1215
                 :detail_value_id: 1215
                 :filetype: image/png
                 """
            let expected:ServerFileAttachment.FileAttachment =
              { detail_value_id= Some 1215
                s3_key= Some "1/6/101/202/1215"
                filetype= Some "image/png"
                filename= "logo-Ubuntu.png"
                uploaded= true
                indb= true
              }

            let res = ServerFileAttachment.FileAttachment.FromString (yaml.Trim())
            res |> should equal expected



            // With missing keys and not uploaded
            let yaml = """
                 ---
                 :uploaded: false
                 :filename: logo-Ubuntu.png
                 """
            let expected:ServerFileAttachment.FileAttachment =
              { detail_value_id= None
                s3_key= None
                filetype= None
                filename= "logo-Ubuntu.png"
                uploaded= false
                indb= true
              }

            let res = ServerFileAttachment.FileAttachment.FromString (yaml.Trim())
            res |> should equal expected
        }
