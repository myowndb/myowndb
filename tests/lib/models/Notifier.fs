namespace MyOwnDB.Tests.Notifier

open Xunit
open FsUnit.Xunit
open Specs
open MyOwnDB.Tests.Helpers
open FSharp.Control.Tasks
open FSharpPlus
open MyOwnDB.Tests.Utils
open NotificationSubscription

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
      DB.DBClient((AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString) + " Include Error Detail=true;")

module Fixtures =
    type DatabaseFixture() =

        let fixtures =
            [ "Account"
              "User"
              "Database"
              "Entity"
              "Instance"
              "Detail"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading entities2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Notifier =
    let societeEntityId = newEntityId 11
    let contactEntityId = newEntityId 12

    open NotificationSubscriptionCriteria
    [<Trait("Category", "models")>]
    [<Trait("Category", "notification")>]
    type NotifierTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``notification_emit``() = async {
            // Setup SavePoint and rollback for this function
            let! rollbackDisposable = setSavePoint Client.client
            // Setup queue collecting notifications
            let notificationsQueue = System.Collections.Concurrent.ConcurrentQueue<NotificationSubscriptionCriteria.Event*NotificationSubscriptionCriteria.Source*int*string>()
            // Setup notifiers. This needs to be done in the apps using this lib.
            notifyToQueue notificationsQueue

            //
            // Take state snapshot
            let! _preState, checkCreation =
                DBState.start
                  Client.client
                  {
                    DBState.zeroState with
                      notificationSubscriptions = 1L
                      maxNotificationSubscriptionId = 1
                  }

            // Create a notification subscription for societe creation by user 100002
            do! NotificationSubscription.SubscribeInstanceCreationMailAction societeEntityId (Ids.UserId 100002)
                |> DBAction.workOnList
                |> DBAction.map (fun l -> List.length l)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [1])

            // Validate the notification subscription was added
            do! checkCreation()

            // Simulate notification emission for societe creation
            // In the app this code will be run by a Hangfire job
            do! NotificationSubscription.NotifyAction AfterCreate Instance (Entity societeEntityId) (1234)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [()])

            // Check the subscribed use got the notification
            notificationsQueue.ToArray() |> should equal [| AfterCreate,Instance,1234,"100002"|]

            // Clean up the notifications queue for next test
            notificationsQueue.Clear()
            // Simulate notification emission for contact creation
            do! NotificationSubscription.NotifyAction AfterCreate Instance (Entity contactEntityId) (1234)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [])

            // Check no notification sent as nobody subscribed
            notificationsQueue.ToArray() |> should equal [||]

            do! rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
