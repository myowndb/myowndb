namespace MyOwnDB.Tests.TestUtils

open System
open Xunit
open FsUnit.Xunit
open Ids
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open System.Threading.Tasks

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                let fixtures =
                    [ "Account"
                      "Database"
                      "Entity"
                      "Instance"
                      "Detail"
                      "DateDetailValue"
                      "DetailValue"
                      "DetailValueProposition"
                      "DdlDetailValue"
                      "IntegerDetailValue"
                      // not loading entities2details will results in an error
                      // query-specified return tuple and crosstab function are not compatible
                      "Entities2details" ]

                // this needs to be sequential because we use the same client....
                let! result = Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
                return result
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module RecordTypes =
    type SimpleRecord =
        { int: int
          float: float
          string: string }

    type SimpleRecordWithList<'a> =
        { int: int
          float: float
          string: string
          list: 'a list }

    type WrappingRecord<'a> =
        { simple: SimpleRecord
          withList: SimpleRecordWithList<'a> }

    type OuterRecord<'a> = { inner: WrappingRecord<'a> }

module TestUtils =
    open RecordTypes

    [<Trait("Category", "testcode")>]
    type TestUtilsTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``2 DBStates from same db should not have difference``() =
            async {
                let! countsA =
                    DBState.getCurrentStateAction ()
                    |> DBAction.run Client.client

                let! countsB =
                    DBState.getCurrentStateAction ()
                    |> DBAction.run Client.client

                DBState.difference countsA countsB
                |> checkOkResult [ DBState.zeroState ]

            }

        [<Fact>]
        member __.``state_with_only_custom``() = async {
                let! custom1 =
                    DBState.getCurrentStateWithOnlyCustomQueriesAction [ "select 1::bigint as count"; "select 2::bigint as count";"select 3::bigint as count"]
                    |> DBAction.run Client.client
                let! custom2 =
                    DBState.getCurrentStateWithOnlyCustomQueriesAction [ "select 1::bigint as count"; "select 3::bigint as count"; "select 0::bigint as count"]
                    |> DBAction.run Client.client

                let diff = DBResult.zip custom1 custom2
                            |> DBResult.map (fun (f,s) -> s-f)
                diff |> checkOkDBResult [0L;1L;-3L]
        }

        [<Fact>]
        member __.``DBState handling functions``() = async {
            // This is code that was copied from an instance test, but
            // that focuses on the date returned by the DBState
            // functions
            // spec for the instance to be inserted
            let spec: Specs.InstanceSpec =
                { entityId = (newEntityId 12)
                  instanceId = None
                  detailValues =
                      [ { detailId = DetailId 48 // nom
                          values =
                              [ { detailValueId = None
                                  value = Specs.ServerValidated (Specs.ServerValue.Simple (Some "John")) } ] }
                        { detailId = DetailId 58 // service
                          values =
                              [ { detailValueId = None
                                  value = Specs.ServerValidated (Specs.ServerValue.Simple (Some "Finance")) } ] } ] }
            // Here we specify the counts and max ids that will change due to the instance insertion
            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      instances = 1L
                      // a,d it was newly inserted
                      maxInstanceId = 1
                      // 2 detail values inserted?
                      detailValues = 2L
                      maxDetailValueId = 2 }
            // we take the initial state to compare with, and get the
            // function that will give us the difference overview
            // compared to the first state
            let! firstState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    // first element is the expected difference in values ifor that query between
                    // the initial state and the second state we will obtain by calling the function
                    // `difference`.
                    // we set non-palyndromic differences, to check the list is not reversed
                    [ 1L, "select count(*) from detail_values where detail_id=48"
                      1L, "select count(*) from detail_values where detail_id=58"
                      0L, "select count(*) from detail_values where detail_id=9999958" ] expectedStateDifference

            // create instance for spec
            let! instanceActionResult =
                Instance.createOrUpdateAction spec
                |> DBAction.run Client.client

            // Now check what was added to the database
            // get counters after insert
            return checkDifferenceNow ()
        }


        [<Fact>]
        member __.``Lens tests``() =

            let simpleRecord =
                { int = 1
                  float = 1.0
                  string = "Hello World" }

            let simpleRecordWithIntList =
                { int = 1
                  float = 1.0
                  string = "Hello World"
                  list = [ 0; 1; 2; 3 ] }

            let simpleRecordWithFloatList =
                { int = 1
                  float = 1.0
                  string = "Hello World"
                  list = [ 0.; 1.; 2.; 3. ] }

            let simpleRecordWithStringList =
                { int = 1
                  float = 1.0
                  string = "Hello World"
                  list = [ "0"; "1"; "2"; "3" ] }

            let outerWithInts =
                { inner =
                      { simple = simpleRecord
                        withList = simpleRecordWithIntList } }

            let outerWithFloats =
                { inner =
                      { simple = simpleRecord
                        withList = simpleRecordWithFloatList } }

            let outerWithStrings =
                { inner =
                      { simple = simpleRecord
                        withList = simpleRecordWithStringList } }
            // Simple records
            Lens.With <@ simpleRecord.int @> 2
            |> should equal
                   { int = 2
                     float = 1.0
                     string = "Hello World" }

            Lens.With <@ simpleRecord.float @> 2.
            |> should equal
                   { int = 1
                     float = 2.0
                     string = "Hello World" }

            Lens.With <@ simpleRecord.string @> "Hi!"
            |> should equal { int = 1; float = 1.0; string = "Hi!" }

            // Lists in records
            // replace first element in list
            Lens.With <@ simpleRecordWithIntList.list.[0] @> 1
            |> (fun (r: SimpleRecordWithList<int>) ->
                r.list |> should equal [ 1; 1; 2; 3 ]
                r.int |> should equal 1
                r.float |> should equal 1.0
                r.string |> should equal "Hello World")
            Lens.With <@ simpleRecordWithStringList.list.[0] @> "Replacement!"
            |> (fun (r: SimpleRecordWithList<string>) ->
                r.list
                |> should equal [ "Replacement!"; "1"; "2"; "3" ]
                r.int |> should equal 1
                r.float |> should equal 1.0
                r.string |> should equal "Hello World")
            Lens.With <@ simpleRecordWithFloatList.list.[0] @> 1.0
            |> (fun (r: SimpleRecordWithList<float>) ->
                r.list |> should equal [ 1.0; 1.; 2.; 3. ]
                r.int |> should equal 1
                r.float |> should equal 1.0
                r.string |> should equal "Hello World")

            // replace a middle element in list
            Lens.With <@ simpleRecordWithIntList.list.[1] @> 2
            |> (fun (r: SimpleRecordWithList<int>) ->
                r.list |> should equal [ 0; 2; 2; 3 ]
                r.int |> should equal 1
                r.float |> should equal 1.0
                r.string |> should equal "Hello World")
            Lens.With <@ simpleRecordWithStringList.list.[2] @> "Replacement!"
            |> (fun (r: SimpleRecordWithList<string>) ->
                r.list
                |> should equal [ "0"; "1"; "Replacement!"; "3" ]
                r.int |> should equal 1
                r.float |> should equal 1.0
                r.string |> should equal "Hello World")
            Lens.With <@ simpleRecordWithFloatList.list.[1] @> 2.0
            |> (fun (r: SimpleRecordWithList<float>) ->
                r.list |> should equal [ 0.; 2.; 2.; 3. ]
                r.int |> should equal 1
                r.float |> should equal 1.0
                r.string |> should equal "Hello World")

            // replace last element in list
            Lens.With <@ simpleRecordWithIntList.list.[3] @> 8
            |> (fun (r: SimpleRecordWithList<int>) ->
                r.list |> should equal [ 0; 1; 2; 8 ]
                r.int |> should equal 1
                r.float |> should equal 1.0
                r.string |> should equal "Hello World")
            Lens.With <@ simpleRecordWithStringList.list.[3] @> "Replacement!"
            |> (fun (r: SimpleRecordWithList<string>) ->
                r.list
                |> should equal [ "0"; "1"; "2"; "Replacement!" ]
                r.int |> should equal 1
                r.float |> should equal 1.0
                r.string |> should equal "Hello World")
            Lens.With <@ simpleRecordWithFloatList.list.[3] @> 8.0
            |> (fun (r: SimpleRecordWithList<float>) ->
                r.list |> should equal [ 0.; 1.; 2.; 8. ]
                r.int |> should equal 1
                r.float |> should equal 1.0
                r.string |> should equal "Hello World")

            // nested records
            // simple field
            Lens.With <@ outerWithInts.inner.simple.int @> 8
            |> (fun (r: OuterRecord<int>) ->
                r.inner.simple.int |> should equal 8 // <-modification
                r.inner.simple.float |> should equal 1.0
                r.inner.simple.string
                |> should equal "Hello World"
                r.inner.withList.int |> should equal 1
                r.inner.withList.float |> should equal 1.0
                r.inner.withList.string
                |> should equal "Hello World"
                r.inner.withList.list
                |> should equal [ 0; 1; 2; 3 ])
            // inner list
            // set third element of list to 8.0
            Lens.With <@ outerWithFloats.inner.withList.list.[2] @> 8.0
            |> (fun (r: OuterRecord<float>) ->
                r.inner.simple.int |> should equal 1
                r.inner.simple.float |> should equal 1.0
                r.inner.simple.string
                |> should equal "Hello World"
                r.inner.withList.int |> should equal 1
                r.inner.withList.float |> should equal 1.0
                r.inner.withList.string
                |> should equal "Hello World"
                r.inner.withList.list
                |> should equal [ 0.; 1.; 8.; 3. ]) // <- modification

            // multiple changes with Bind
            Lens.With <@ outerWithInts.inner.withList.list.[2] @> 28
            |> Lens.Bind (fun (r: OuterRecord<int>) -> <@ r.inner.withList.list.[0] @>) 8
            |> Lens.Bind (fun (r: OuterRecord<int>) -> <@ r.inner.withList.list.[1] @>) 18
            |> Lens.Bind (fun (r: OuterRecord<int>) -> <@ r.inner.simple.string @>) "Welcome!"
            |> Lens.Bind (fun (r: OuterRecord<int>) -> <@ r.inner.simple.int @>) 8
            |> (fun (r: OuterRecord<int>) ->
                r.inner.simple.int |> should equal 8 // <-modification
                r.inner.simple.float |> should equal 1.0
                r.inner.simple.string |> should equal "Welcome!"
                r.inner.withList.int |> should equal 1
                r.inner.withList.float |> should equal 1.0
                r.inner.withList.string
                |> should equal "Hello World"
                r.inner.withList.list
                |> should equal [ 8; 18; 28; 3 ])
