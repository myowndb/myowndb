namespace MyOwnDB.Tests.Datatypes

open Xunit
open MyOwnDB.Tests.Helpers

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module DateAndTime =
    [<Trait("Category", "date")>]
    type DateTests() =

        [<Fact>]
        member __.Successful_date_parsing() =

            let dates =
                [ "2022-03-02"
                  "2022-03-01 22:35:12" ]

            dates
            |> List.iter (fun date ->
                date
                |> DateAndTime.fromString
                |> Result.bind (DateAndTime.toString >> Ok)
                |> checkOkResult date)


            // EMpty string is valid for empty values in the form
            dates @ [ "" ]
            |> List.map (DateAndTime.isValid)
            |> List.forall id

        [<Fact>]
        member __.Rejected_dates() =

            // Empty string cannot be parsed as date, though it is still accepted as valid for empty form fields
            [ "-"
              ""
              " "
              "2022-31-12"
              "2022-12-01 25:43:12"
              "2022-12-01 15:63:12"
              "2022-12-01 15:43:62"
            ]
            |> List.iter (fun date ->
                date
                |> DateAndTime.fromString
                |> checkErrorResult [ sprintf "Date could not be parsed"])
