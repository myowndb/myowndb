namespace MyOwnDB.Tests.Datatypes

open Xunit
open MyOwnDB.Tests.Helpers

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module Email =
    [<Trait("Category", "email")>]
    type EmailTests() =

        [<Fact>]
        member __.Successful_parsing() =

            let emails =
                [ "charles@example.com"
                  "Jean-Francois@this-company.com" ]

            emails
            |> List.iter (fun email ->
                email
                |> Email.fromString
                |> Result.bind (Email.toString >> Ok)
                |> checkOkResult email)

            emails
            |> List.iter (Email.validate >> checkOkResult ())

            emails
            |> List.map (Email.isValid)
            |> List.forall id

        [<Fact>]
        member __.Rejected_emails() =

            [ "char les@example.com"
              "Jean-Fr@nçois@this-company.com"
              "name@this-company.i" ]
            |> List.iter (fun email ->
                email
                |> Email.fromString
                |> checkErrorResult [ sprintf "email %s is invalid" email ])
