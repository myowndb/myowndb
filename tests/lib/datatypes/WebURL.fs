namespace MyOwnDB.Tests.Datatypes

open Xunit
open MyOwnDB.Tests.Helpers

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module WebURL =
    [<Trait("Category", "weburl")>]
    type Tests() =

        [<Fact>]
        member __.Successful_parsing() =

            let urls =
                [ "http://myowndb.com"
                  "http://www.myowndb.com"
                  "https://www.myowndb.com"
                  "http://www.other-mood.com" ]

            urls
            |> List.iter (fun url ->
                url
                |> WebURL.fromString
                |> Result.bind (WebURL.toString >> Ok)
                |> checkOkResult url)

            urls
            |> List.iter (WebURL.validate >> checkOkResult ())

            urls
            |> List.map (WebURL.isValid)
            |> List.forall id

        [<Fact>]
        member __.Rejected_emails() =

            [ "http://ma db.net"
              "httpc://myowndb.com"
              "http:/myowndb.com"
              "htp://myowndb.com" ]
            |> List.iter (fun url ->
                url
                |> WebURL.fromString
                |> checkErrorResult [ sprintf "email %s is invalid" url])
