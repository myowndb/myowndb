namespace MyOwnDB.Tests.Admin.Entity

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =
        let fixtures =
                [ "Account"
                  "Database"
                  "Entity"
                  "Instance"
                  "Detail"
                  "DateDetailValue"
                  "IntegerDetailValue"
                  "DetailValue"
                  "DetailValueProposition"
                  "DdlDetailValue"
                  // not loading entities2details will results in an error
                  // query-specified return tuple and crosstab function are not compatible
                  "Entities2details"
                  "Relation"
                  "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module Entity =
    [<Trait("Category", "admin")>]
    [<Trait("Category", "entity")>]
    type EntityTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``Creating entity error``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.EntitySpec =
                { entityId = None
                  databaseId = newDatabaseId 12 // Inexisting database Id!
                  name = "Person"
                  hasPublicForm = false
                }

            // create instance for spec
            let! entityActionResult =
                Admin.Entity.createActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            entityActionResult
            |> checkErrorResult [ """Error executing query insert into entities (database_id,name,has_public_form) VALUES (@p0,@p1,@p2) RETURNING id with mappings map [("p0", 12); ("p1", "Person"); ("p2", false)]. Look at inner exception for details""" ]
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask

        }
        [<Fact>]
        member __.``Creating entity``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.EntitySpec =
                { entityId = None
                  databaseId = newDatabaseId 3
                  name = "Person"
                  hasPublicForm = false
                }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      entities = 1L
                      // a,d it was newly inserted
                      maxEntityId = 1
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference

            // easier names to use in specResult building
            let entityId = preState.maxEntityId

            // spec we expect as result
            let specResult: Specs.EntitySpec =
                Lens.With <@ spec.entityId @> (Some(newEntityId (entityId + 1)))

            // create instance for spec
            let! entityActionResult =
                Admin.Entity.createActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            entityActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()


            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // Fiction
            let newName = "People"

            let specUpdate =
                Lens.With <@ specResult.name @> newName

            // create instance for spec
            let! entityUpdateResult =
                Admin.Entity.createOrUpdateActionFromSpec specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            entityUpdateResult
            |> checkOkResult [ specUpdate ]

            do! checkUpdateDifference ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask

        }
        [<Fact>]
        member __.``Updating entity error``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.EntitySpec =
                { entityId = Some (newEntityId 1234)
                  databaseId = newDatabaseId 3
                  name = "Person"
                  hasPublicForm = false
                }

            // create instance for spec
            let! entityActionResult =
                Admin.Entity.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            entityActionResult
            |> checkErrorResult [ """Update failed""" ]

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask

        }
        [<Fact>]
        member __.``Adding detail to entity``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let e2dInfo = { entities2detailId = None
                            entityId= newEntityId 19
                            displayedInList = true
                            maximumNumberOfValues = 1
                            displayOrder = 80
                          }
            let spec: Specs.Entity2DetailSpec =
                Specs.Entity2ExistingDetailSpec {   info = e2dInfo
                                                    detailId= DetailId 87
                                                }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      entities2Details = 1L
                      // a,d it was newly inserted
                      maxEntities2DetailsId = 1
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from entities2details where entity_id=19 and detail_id=87" ]
                    expectedStateDifference

            // easier names to use in specResult building
            let e2dId = preState.maxEntities2DetailsId

            // spec we expect as result
            let infoResult: Specs.Entity2DetailInfoSpec =
                Lens.With <@ e2dInfo.entities2detailId @> (Some(Entities2detailId (e2dId + 1)))
            let specResult : Specs.Entity2ExistingDetailSpec =
               { detailId = DetailId 87
                 info = infoResult
                }

            // create instance for spec
            let! entityActionResult =
                Admin.Entity.addDetailActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            entityActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // new detail to be created
            //---------------------------------------------------------------------------------
            // spec for the instance to be inserted
            let e2dInfo = { entities2detailId = None
                            entityId= newEntityId 19
                            displayedInList = true
                            maximumNumberOfValues = 1
                            displayOrder = 80
                          }
            let spec: Specs.Entity2DetailSpec =
                Specs.Entity2NewDetailSpec {   info = e2dInfo
                                               detailSpec= { detailId= None
                                                             name = "fonction"
                                                             dataType =
                                                               { dataTypeId  = DataTypeId 1
                                                                 name = "madb_short_text"
                                                                 className = "SimpleDetailValue"
                                                               }
                                                             propositions = None
                                                             status = DetailStatus.Active
                                                           }
                                           }

            let expectedStateDifference =
                { DBState.zeroState with
                      // one entities2details added
                      entities2Details = 1L
                      maxEntities2DetailsId = 1
                      // one new detail created
                      details = 1L
                      maxDetailId = 1
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from entities2details where entity_id=19 and display_order=80"
                      1L, "select count(*) from details where name='fonction'"
                    ]
                    expectedStateDifference

            // easier names to use in specResult building
            let e2dId = preState.maxEntities2DetailsId
            let dId = preState.maxDetailId

            // spec we expect as result
            let infoResult: Specs.Entity2DetailInfoSpec =
                Lens.With <@ e2dInfo.entities2detailId @> (Some(Entities2detailId (e2dId + 1)))
            let specResult : Specs.Entity2ExistingDetailSpec =
               { detailId = DetailId(dId+1)
                 info = infoResult
                }

            // create instance for spec
            let! entityActionResult =
                Admin.Entity.addDetailActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            entityActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()


            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client
                                                [
                                                    -1L, "select count(*) from entities2details where displayed_in_list_view = true"
                                                    1L, "select count(*) from entities2details where displayed_in_list_view = false"
                                                ] DBState.zeroState

            let infoUpdate =
                Lens.With <@ infoResult.displayedInList @> false
            let specUpdate : Specs.Entity2ExistingDetailSpec = { detailId = DetailId 87
                                                                 info = infoUpdate
                                                               }

            // create instance for spec
            let! entityUpdateResult =
                Admin.Entity.updateDetailActionFromSpec specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            entityUpdateResult
            |> checkOkResult [ specUpdate ]

            do! checkUpdateDifference ()

            // update of the linked detail itself
            //---------------------------------------------------------------------------

            // We update the info as it was at first step (displayInList = true)
            let e2dInfo = Lens.With <@ infoUpdate.displayedInList @> true
            // and we update the detail itself
            let spec: Specs.Entity2NewOrUpdatedDetailSpec =
                                           {   info = e2dInfo
                                               detailSpec= { detailId= Some specUpdate.detailId
                                                             name = "subtitle"
                                                             dataType =
                                                               { dataTypeId  = DataTypeId 1
                                                                 name = "madb_short_text"
                                                                 className = "SimpleDetailValue"
                                                               }
                                                             propositions = None
                                                             status = DetailStatus.Active
                                                           }
                                           }

            let expectedStateDifference = DBState.zeroState

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, "select count(*) from entities2details where entity_id=19 and displayed_in_list_view=true"
                      1L, "select count(*) from details where name='subtitle'"
                      -1L, "select count(*) from details where name='sous-titre'"
                    ]
                    expectedStateDifference

            // easier names to use in specResult building
            let e2dId = preState.maxEntities2DetailsId
            let dId = preState.maxDetailId

            // spec we expect as result
            let infoResult: Specs.Entity2DetailInfoSpec = e2dInfo
            let specResult : Specs.Entity2ExistingDetailSpec =
               { detailId = spec.detailSpec.detailId |> Option.get
                 info = infoResult
                }

            // create instance for spec
            let! entityActionResult =
                Admin.Entity.addDetailActionFromSpec (Specs.Entity2NewDetailSpec spec)
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            entityActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()


            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }



        [<Fact>]
        // Put exception raising query in a specific test because it rolls back transaction. This also explains
        // why we cannot check the database state with our helper functions as these run in one transaction.
        member __.``link_across_dbs``() = async{
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client
            // linking a detail in another database is not accepted
            //---------------------------------------------------------------------------------
            // spec for the instance to be inserted
            let e2dInfo = { entities2detailId = None
                            entityId= newEntityId 19
                            displayedInList = true
                            maximumNumberOfValues = 1
                            displayOrder = 80
                          }
            let spec: Specs.Entity2DetailSpec =
                Specs.Entity2ExistingDetailSpec {   info = e2dInfo
                                                    // This details is in database with id 3, not 6 as the entity
                                                    detailId= DetailId 28
                                                }

            // attempt linking entity and detail
            do!
                Admin.Entity.addDetailActionFromSpec spec
                |> DBAction.run Client.client
                |> Async.map (function
                                | Ok v -> sprintf "This result (%A) should have been an error" v |> should equal true
                                          List<exn>.Empty
                                | Error es -> Error.getExceptions es)
                // Validate we get an updated spec with expected Ids added
                // We get the updated spec as expected
                |> Async.map (fun exns ->
                                List.length exns |> should equal 1
                                let e = List.head exns
                                let inner = e.InnerException
                                true |> should equal true
                                inner.Message |> should equal "One or more errors occurred. (P0001: entities2details cannot link across databases, 6 for entity 19, 3 for detail 28)"
                )



            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }




        [<Fact>]
        member __.``entities_of_db``() = async{

            // Retrieve list for db and correct account
            do! Admin.Entity.getAllForAccountAndDatabaseAction 1 6
                |> DBAction.map (fun (e: Specs.EntitySpec) -> e.entityId)
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult [Some(newEntityId 11); Some(newEntityId 12); Some(newEntityId 13); Some(newEntityId 14); Some(newEntityId 15); Some(newEntityId 16);
           Some(newEntityId 17); Some(newEntityId 18); Some(newEntityId 19); Some(newEntityId 20); Some(newEntityId 21); Some(newEntityId 51);Some(newEntityId 100);
           Some(newEntityId 101); Some(newEntityId 100000); Some(newEntityId 100001)])

            // When the account and db are not linked, an empty list is returned
            do! Admin.Entity.getAllForAccountAndDatabaseAction 2 6
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult List<Specs.EntitySpec>.Empty)
        }

        [<Fact>]
        member __.``entity_details_of_entity``() = async{

            let expected:List<Entity.EntityDetail> =
                [{  id = 40
                    entity_id = newEntityId 8
                    detail_id = DetailId 40
                    displayed_in_list_view = true
                    maximum_number_of_values = 1
                    display_order = 1
                    name = "contact_name"
                    lock_version = 0
                    status = "active"
                    data_type = "madb_short_text"
                    class_name = "SimpleDetailValue"
                    };
                 {  id = 41
                    entity_id = newEntityId 8
                    detail_id = DetailId 41
                    displayed_in_list_view = true
                    maximum_number_of_values = 1
                    display_order = 2
                    name = "contact_first_name"
                    lock_version = 0
                    status = "active"
                    data_type = "madb_short_text"
                    class_name = "SimpleDetailValue"
                    };
                   { id = 43
                     entity_id = newEntityId 8
                     detail_id = DetailId 43
                     displayed_in_list_view = true
                     maximum_number_of_values = 1
                     display_order = 5
                     name = "phone"
                     lock_version = 0
                     status = "active"
                     data_type = "madb_short_text"
                     class_name = "SimpleDetailValue"
                     };
                   { id = 44
                     entity_id = newEntityId 8
                     detail_id = DetailId 44
                     displayed_in_list_view = true
                     maximum_number_of_values = 1
                     display_order = 6
                     name = "email"
                     lock_version = 0
                     status = "active"
                     data_type = "madb_short_text"
                     class_name = "SimpleDetailValue"
                     };
                   { id = 42
                     entity_id = newEntityId 8
                     detail_id = DetailId 42
                     displayed_in_list_view = true
                     maximum_number_of_values = 1
                     display_order = 20
                     name = "birthday"
                     lock_version = 0
                     status = "active"
                     data_type = "madb_date"
                     class_name = "DateDetailValue"
                     }
                ]



            // Retrieve list for db and correct account
            do! Admin.Entity.getEntityDetailsAction 2 8
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult expected)

            // When the account and db are not linked, an empty list is returned
            do! Admin.Entity.getEntityDetailsAction 1 8
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult List<Entity.EntityDetail>.Empty)
        }

        [<Fact>]
        member __.``get_one_entity_detail``() = async{

            let expected:List<Entity.EntityDetail> =
                [{  id = 40
                    entity_id = newEntityId 8
                    detail_id = DetailId 40
                    displayed_in_list_view = true
                    maximum_number_of_values = 1
                    display_order = 1
                    name = "contact_name"
                    lock_version = 0
                    status = "active"
                    data_type = "madb_short_text"
                    class_name = "SimpleDetailValue"
                    };
                ]



            // Retrieve list for db and correct account
            do! Admin.Entity.getEntityDetailAction 2 40
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult expected)

            // When the account and db are not linked, an empty list is returned
            do! Admin.Entity.getEntityDetailsAction 1 40
                |> DBAction.run Client.client
                |> Async.map (checkOkDBResult List<Entity.EntityDetail>.Empty)
        }
