namespace MyOwnDB.Tests.Admin.Detail

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =
        let fixtures =
            [ "Account"
              "Database"
              "Entity"
              "Detail"
              "Instance"
              "DateDetailValue"
              "IntegerDetailValue"
              "DetailValue"
              "DetailValueProposition"
              "DdlDetailValue"
              // not loading details2details will results in an error
              // query-specified return tuple and crosstab function are not compatible
              "Entities2details"
              "Relation"
              "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }


module Detail =
    [<Trait("Category", "admin")>]
    [<Trait("Category", "detail")>]
    type DetailTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``Creating detail error``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.DetailSpec =
                { detailId = None
                  name = "Firstname"
                  dataType =
                    { dataTypeId  = DataTypeId 1234 // Inexisting Id!
                      name = "madb_short_text"
                      className = "SimpleDetailValue"
                    }
                  propositions = None
                  status = DetailStatus.Active
                  databaseId = newDatabaseId 12   // Inexisting Id!
                }

            // create instance for spec
            let! detailActionResult =
                Admin.Detail.createActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            detailActionResult
            |> checkErrorResult [ """Error executing query insert into details (name,data_type_id,status_id,database_id) VALUES (@p0,@p1,@p2,@p3) RETURNING id with mappings map [("p0", "Firstname"); ("p1", 1234); ("p2", 1); ("p3", 12)]. Look at inner exception for details""" ]

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
        [<Fact>]
        member __.``Creating detail``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Insert of short text without propositions
            // spec for the instance to be inserted
            let spec: Specs.DetailSpec =
                { detailId = None
                  name = "Firstname"
                  dataType =
                    { dataTypeId  = DataTypeId 1
                      name = "madb_short_text"
                      className = "SimpleDetailValue"
                    }
                  propositions = None
                  status = DetailStatus.Active
                  databaseId = newDatabaseId 6
                }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      details = 1L
                      // a,d it was newly inserted
                      maxDetailId = 1
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference

            // easier names to use in specResult building
            let detailId = preState.maxDetailId

            // spec we expect as result
            let specResult: Specs.DetailSpec =
                Lens.With <@ spec.detailId @> (Some(DetailId (detailId + 1)))

            // create instance for spec
            let! detailActionResult =
                Admin.Detail.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            detailActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // Insert detail of type short text with propositions
            // spec for the instance to be inserted
            let spec: Specs.DetailSpec =
                { detailId = None
                  name = "Middlename"
                  dataType =
                    { dataTypeId  = DataTypeId 1
                      name = "madb_short_text"
                      className = "SimpleDetailValue"
                    }
                  propositions = Some [ { detailValuePropositionId= None
                                          value= "John"
                                        }
                                        { detailValuePropositionId= None
                                          value= "Jim"
                                        }
                                 ]
                  status = DetailStatus.Active
                  databaseId = newDatabaseId 6
                }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      details = 1L
                      // a,d it was newly inserted
                      maxDetailId = 1
                      detailValuePropositions =  2L
                      maxDetailValuePropositionId = 2
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference

            // easier names to use in specResult building
            let detailId = preState.maxDetailId

            // spec we expect as result
            let specResult: Specs.DetailSpec =
                Lens.With <@ spec.detailId @> (Some(DetailId (detailId + 1)))
                |> Lens.Bind (fun (s:Specs.DetailSpec) -> <@ s.propositions @>)
                        (Some [ { detailValuePropositionId= Some ( DetailValuePropositionId (preState.maxDetailValuePropositionId+2))
                                  value= "John"
                                }
                                { detailValuePropositionId= Some ( DetailValuePropositionId (preState.maxDetailValuePropositionId+1))
                                  value= "Jim"
                                }
                             ])
            // create instance for spec
            let! detailActionResult =
                Admin.Detail.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            detailActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // Fiction
            let newName = "Lastname"

            let specUpdate =
                Lens.With <@ specResult.name @> newName

            // create instance for spec
            let! detailUpdateResult =
                Admin.Detail.createOrUpdateActionFromSpec specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            detailUpdateResult
            |> checkOkResult [ specUpdate ]

            do! checkUpdateDifference ()

            // Update of existing detail with update of its existing propositions
            // spec for the instance to be inserted
            let spec: Specs.DetailSpec =
                { detailId = Some (DetailId 62)
                  name = "CompanyType"
                  dataType =
                    { dataTypeId  = DataTypeId 5
                      name = "madb_choose_in_list"
                      className = "DdlDetailValue"
                    }
                  // 2 updates and one new proposal
                  propositions = Some [{detailValuePropositionId = Some (DetailValuePropositionId 11)
                                        value = "SRL"
                                       }
                                       {detailValuePropositionId = Some (DetailValuePropositionId 12)
                                        value = "SA"
                                       }
                                       // ne proposal
                                       {detailValuePropositionId = None
                                        value = "SCR"
                                       }
                                       ]
                  status = DetailStatus.Active
                  databaseId = newDatabaseId 6
                }

            let expectedStateDifference = { DBState.zeroState with
                                                detailValuePropositions = 1L
                                                maxDetailValuePropositionId = 1
                                          }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [
                        1L, "select count(*) from detail_value_propositions where value='SRL' and detail_id=62"
                        -1L, "select count(*) from detail_value_propositions where value='sprl' and detail_id=62"
                        1L, "select count(*) from detail_value_propositions where value='SA' and detail_id=62"
                        -1L, "select count(*) from detail_value_propositions where value='sa' and detail_id=62"
                        -1L, "select count(*) from detail_value_propositions where value='sa' and detail_id=62"
                        1L, "select count(*) from detail_value_propositions where value='SCR' and detail_id=62"
                        1L, "select count(*) from details where name='CompanyType' and id=62"
                        -1L, "select count(*) from details where name='status' and id=62"
                    ] expectedStateDifference

            // easier names to use in specResult building
            let dvpId = preState.maxDetailValuePropositionId

            let specUpdate: Specs.DetailSpec =
                { detailId = Some (DetailId 62)
                  name = "CompanyType"
                  dataType =
                    { dataTypeId  = DataTypeId 5
                      name = "madb_choose_in_list"
                      className = "DdlDetailValue"
                    }
                  // 2 updates and one new proposal
                  propositions = Some [{detailValuePropositionId = Some (DetailValuePropositionId 11)
                                        value = "SRL"
                                       }
                                       {detailValuePropositionId = Some (DetailValuePropositionId 12)
                                        value = "SA"
                                       }
                                       // ne proposal
                                       {detailValuePropositionId = Some (DetailValuePropositionId (dvpId+1))
                                        value = "SCR"
                                       }
                                       ]
                  status = DetailStatus.Active
                  databaseId = newDatabaseId 6
                }
            // create instance for spec
            let! detailActionResult =
                Admin.Detail.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            detailActionResult
            |> checkOkResult [ specUpdate ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
        [<Fact>]
        member __.``Updating detail error``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.DetailSpec =
                { detailId = Some (DetailId 56)
                  name = "Firstname"
                  dataType =
                    { dataTypeId  = DataTypeId 1
                      name = "madb_short_text"
                      className = "SimpleDetailValue"
                    }
                  propositions = None
                  status = DetailStatus.Active
                  databaseId = newDatabaseId 12   // Wrong Id!
                }

            // create instance for spec
            let! detailActionResult =
                Admin.Detail.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client

            // row to update not found
            detailActionResult
            |> checkErrorResult [ """Update failed""" ]

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
