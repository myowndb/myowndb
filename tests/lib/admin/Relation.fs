namespace MyOwnDB.Tests.Admin.Relation

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =
        let fixtures =
                [ "Account"
                  "Database"
                  "Entity"
                  "Detail"
                  "Instance"
                  "DateDetailValue"
                  "IntegerDetailValue"
                  "DetailValue"
                  "DetailValueProposition"
                  "DdlDetailValue"
                  // not loading details2details will results in an error
                  // query-specified return tuple and crosstab function are not compatible
                  "Entities2details"
                  "Relation"
                  "Link" ]
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module Relation =
    [<Trait("Category", "admin")>]
    [<Trait("Category", "relation")>]
    type RelationTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``Creating relation error``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.RelationSpec =
                { relationId = None
                  parentId = newEntityId 7 // in database 3
                  childId = newEntityId 17 // in database 6, so not possible to link
                  parentToChildName = "teacher of"
                  childToParentName = "pupil of"
                  parentSideType = Specs.RelationSideType.One
                  childSideType =Specs.RelationSideType.Many
                }

            // create instance for spec
            let! relationActionResult =
                Admin.Relation.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            relationActionResult
            |> checkErrorResult [ """inconsistent data""" ]

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``create_and_update_relation``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // Insert of short text without propositions
            // spec for the instance to be inserted
            let spec: Specs.RelationSpec =
               { relationId = None
                 parentId = newEntityId 7
                 childId = newEntityId 7
                 parentToChildName = "teacher of"
                 childToParentName = "pupil of"
                 parentSideType = Specs.RelationSideType.One
                 childSideType =Specs.RelationSideType.Many
                }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      relations = 1L
                      // a,d it was newly inserted
                      maxRelationId = 1
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [] expectedStateDifference

            // easier names to use in specResult building
            let relationId = preState.maxRelationId

            // spec we expect as result
            let specResult: Specs.RelationSpec =
                Lens.With <@ spec.relationId @> (Some(newRelationId (relationId + 1)))

            // create instance for spec
            let! relationActionResult =
                Admin.Relation.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            relationActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // update
            //---------------------------------------------------------------------------------

            let newName = "In class of"

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [
                        1L, sprintf "select count(*) from relations where from_child_to_parent_name='%s'" newName
                        -1L, "select count(*) from relations where from_child_to_parent_name='pupil of'"
                ] DBState.zeroState

            let specUpdate =
                Lens.With <@ specResult.childToParentName @> newName

            // create instance for spec
            let! relationUpdateResult =
                Admin.Relation.createOrUpdateActionFromSpec specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            relationUpdateResult
            |> checkOkResult [ specUpdate ]

            do! checkUpdateDifference ()

            // Update of relation side types
            //
            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [ ] DBState.zeroState

            // We try to update sides in both directions
            let specSidesUpdate =
                Lens.With <@ specUpdate.parentSideType @> Specs.RelationSideType.Many
                |> Lens.Bind (fun (r: Specs.RelationSpec) -> <@ r.childSideType @>)
                       Specs.RelationSideType.One
            // But only changing a 'to one' to a 'to many' is possible, the other direction is not accepted
            let specExpectedResult =
                Lens.With <@ specUpdate.parentSideType @> Specs.RelationSideType.Many

            // create instance for spec
            let! sidesUpdateResult =
                Admin.Relation.createOrUpdateActionFromSpec specSidesUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            sidesUpdateResult
            |> checkOkResult [ specExpectedResult ]

            do! checkUpdateDifference ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
