namespace MyOwnDB.Tests.Admin.DetailValueProposition

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =
        let fixtures =
                [ "Account"
                  "Database"
                  "Entity"
                  "Detail"
                  "Instance"
                  "DateDetailValue"
                  "IntegerDetailValue"
                  "DetailValue"
                  "DetailValueProposition"
                  "DdlDetailValue"
                  // not loading detailValuePropositions2detailValuePropositions will results in an error
                  // query-specified return tuple and crosstab function are not compatible
                  "Entities2details"
                  "Relation"
                  "Link" ]

        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

module DetailValueProposition =
    [<Trait("Category", "admin")>]
    [<Trait("Category", "detailValueProposition")>]
    type DetailValuePropositionTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``Creating detailValueProposition error``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.DetailValuePropositionSpec =
                { detailId = DetailId 1234  // inexisting
                  propositions = [
                      { detailValuePropositionId = None
                        value = "srl"
                      }
                  ]
                }

            // create instance for spec
            let! detailValuePropositionActionResult =
                Admin.DetailValueProposition.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            detailValuePropositionActionResult
            |> checkErrorResult [ """Error executing query insert into detail_value_propositions (detail_id,value) VALUES (@p0,@p1) RETURNING id with mappings map [("p0", 1234); ("p1", "srl")]. Look at inner exception for details"""]

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
        [<Fact>]
        member __.``Creating detailValueProposition``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.DetailValuePropositionSpec =
                { detailId = DetailId 62
                  propositions = [
                      { detailValuePropositionId = None
                        value = "srl"
                      }
                  ]
                }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      detailValuePropositions = 1L
                      // a,d it was newly inserted
                      maxDetailValuePropositionId = 1
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [
                        1L, "select count(*) from detail_value_propositions where detail_id=62"
                    ] expectedStateDifference

            // easier names to use in specResult building
            let detailValuePropositionId = preState.maxDetailValuePropositionId

            // spec we expect as result
            let specResult: Specs.DetailValuePropositionSpec =
                Lens.With <@ spec.propositions.[0].detailValuePropositionId @> (Some (DetailValuePropositionId (detailValuePropositionId + 1 )))

            // create instance for spec
            let! detailValuePropositionActionResult =
                Admin.DetailValueProposition.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            detailValuePropositionActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()


            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [
                    1L, "select count(*) from detail_value_propositions where detail_id=62 and value='SRL'"
                    -1L, "select count(*) from detail_value_propositions where detail_id=62 and value='srl'"
                ] DBState.zeroState

            let newValue = "SRL"

            let specUpdate =
                Lens.With <@ specResult.propositions.[0].value @> newValue

            // create instance for spec
            let! detailValuePropositionUpdateResult =
                Admin.DetailValueProposition.createOrUpdateActionFromSpec specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            detailValuePropositionUpdateResult
            |> checkOkResult [ specUpdate ]

            do! checkUpdateDifference ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
        [<Fact>]
        member __.``Creating/updating multiple detailValueProposition``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.DetailValuePropositionSpec =
                { detailId = DetailId 62
                  propositions = [
                      { detailValuePropositionId = None
                        value = "srl"
                      }
                      { detailValuePropositionId = None
                        value = "scrl"
                      }
                      { detailValuePropositionId = Some (DetailValuePropositionId 11)
                        value = "SPRL"
                      }
                      { detailValuePropositionId = Some (DetailValuePropositionId 12)
                        value = "SA"
                      }
                  ]
                }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      detailValuePropositions = 2L
                      // a,d it was newly inserted
                      maxDetailValuePropositionId = 2
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [
                        2L, "select count(*) from detail_value_propositions where detail_id=62"
                        1L, "select count(*) from detail_value_propositions where detail_id=62 and value='SPRL' and id=11"
                        -1L, "select count(*) from detail_value_propositions where detail_id=62 and value='sprl' and id=11"
                        1L, "select count(*) from detail_value_propositions where detail_id=62 and value='SA' and id=12"
                        -1L, "select count(*) from detail_value_propositions where detail_id=62 and value='sa' and id=12"
                    ] expectedStateDifference

            // easier names to use in specResult building
            let detailValuePropositionId = preState.maxDetailValuePropositionId

            // spec we expect as result
            let specResult: Specs.DetailValuePropositionSpec =
                Lens.With <@ spec.propositions.[0].detailValuePropositionId @> (Some (DetailValuePropositionId (detailValuePropositionId + 2 )))
                |> Lens.Bind (fun (spec:Specs.DetailValuePropositionSpec) -> <@ spec.propositions.[1].detailValuePropositionId @>) (Some (DetailValuePropositionId (detailValuePropositionId + 1 )))

            // create instance for spec
            let! detailValuePropositionActionResult =
                Admin.DetailValueProposition.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            detailValuePropositionActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``Updating detailValueProposition error``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // spec for the instance to be inserted
            let spec: Specs.DetailValuePropositionSpec =
                { detailId = DetailId 62
                  propositions = [
                      { detailValuePropositionId = Some (DetailValuePropositionId 1234)
                        value = "srl"
                      }
                  ]
                }

            // create instance for spec
            let! detailValuePropositionActionResult =
                Admin.DetailValueProposition.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client

            // row to update not found
            detailValuePropositionActionResult
            |> checkErrorResult [ """Update failed""" ]

            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }
