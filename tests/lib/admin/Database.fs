namespace MyOwnDB.Tests.Admin.Database

open System
open Xunit
open System.IO
open FsUnit.Xunit
open Ids
open Specs
open MyOwnDB.Tests.Helpers
open MyOwnDB.Tests.Utils
open FSharp.Control.Tasks
open FSharpPlus
open Specs

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture
module Client =
    let client =
        DB.DBClient(AppConfig.load "conf/database.json"|>AppConfig.pgConnectionString)

module Fixtures =
    type DatabaseFixture() =
        let fixtures =
                [ "Account"
                  "Database"
                  "Entity"
                  "Instance"
                  "Detail"
                  "DateDetailValue"
                  "IntegerDetailValue"
                  "DetailValue"
                  "DetailValueProposition"
                  "DdlDetailValue"
                  // not loading entities2details will results in an error
                  // query-specified return tuple and crosstab function are not compatible
                  "Entities2details"
                  "Relation"
                  "Link" ]
        interface IAsyncLifetime with
            member _.InitializeAsync() = unitTask {
                let! () = Client.client.Open()
                let! () = Client.client.BeginTransaction()
                // load database records
                return! Fixtures.processRecordsArray Client.client
                                (fixtures
                                 |> List.map (fun f -> sprintf "%s+%s,lib" f f)
                                 |> Array.ofList) |> Async.Sequential
            }

            member __.DisposeAsync() = unitTask {
                let! _ = Client.client.TryRollback()
                let! () = Client.client.Close()
                return ()
            }

open MyOwnDB.Tests.IdsHelpers
module Database =
    [<Trait("Category", "admin")>]
    [<Trait("Category", "database")>]
    type DatabaseTests() =
        interface IClassFixture<Fixtures.DatabaseFixture>

        [<Fact>]
        member __.``Creating_database_error``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            // This account id does not exist
            let accountId = 10
            // spec for the instance to be inserted
            let spec: Specs.DatabaseSpec =
                { databaseId= None
                  accountId= AccountId accountId
                  name= "Test-created database"
                }

            // create instance for spec
            let! databaseActionResult =
                Admin.Database.createActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            databaseActionResult
            |> checkErrorResult [ """Error executing query insert into databases (account_id,name) VALUES (@p0,@p1) RETURNING id with mappings map [("p0", 10); ("p1", "Test-created database")]. Look at inner exception for details""" ]

            // don't check differnce as transaction is closed
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``Creating database successfully``() = async {

            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let accountId = 1
            // spec for the instance to be inserted
            let spec: Specs.DatabaseSpec =
                { databaseId= None
                  accountId= AccountId accountId
                  name= "Test-created database"
                }

            let expectedStateDifference =
                { DBState.zeroState with
                      // only more instance present in database?
                      databases = 1L
                      // a,d it was newly inserted
                      maxDatabaseId = 1
                }

            let! preState, checkDifferenceNow =
                DBState.startWithCustomQueries Client.client
                    [ 1L, sprintf "select count(*) from databases where account_id=%d" accountId
                    ] expectedStateDifference

            // easier names to use in specResult building
            let databaseId = preState.maxDatabaseId

            // spec we expect as result
            let specResult: Specs.DatabaseSpec =
                Lens.With <@ spec.databaseId @> (Some(newDatabaseId(databaseId + 1)))

            // create instance for spec
            let! databaseActionResult =
                Admin.Database.createActionFromSpec spec
                |> DBAction.run Client.client


            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            databaseActionResult
            |> checkOkResult [ specResult ]

            // Now check what was added to the database
            // get counters after insert
            do! checkDifferenceNow ()

            // update
            //---------------------------------------------------------------------------------

            // take db state before update
            // we expect no differences in rows and ids with the update
            let! _, checkUpdateDifference =
                DBState.startWithCustomQueries Client.client [] DBState.zeroState

            // Fiction
            let newName = "People"

            let specUpdate =
                Lens.With <@ specResult.name @> newName

            // create instance for spec
            let! entityUpdateResult =
                Admin.Database.createOrUpdateActionFromSpec specUpdate
                |> DBAction.run Client.client

            // Validate we get an updated spec with expected Ids added
            // We get the updated spec as expected
            entityUpdateResult
            |> checkOkResult [ specUpdate ]

            do! checkUpdateDifference ()
            do! _rollbackDisposable.DisposeAsync().AsTask()  |> Async.AwaitTask
        }

        [<Fact>]
        member __.``Updating database error``() = async {
            // Setup SavePoint and rollback for this function
            let! _rollbackDisposable = setSavePoint Client.client

            let accountId = 1
            // spec for the instance to be inserted
            let spec: Specs.DatabaseSpec =
                { databaseId= Some (newDatabaseId 1234) // Inexisting database
                  accountId= AccountId accountId
                  name= "Test-created database"
                }

            // run update action
            let! entityActionResult =
                Admin.Database.createOrUpdateActionFromSpec spec
                |> DBAction.run Client.client

            // check we get the error
            entityActionResult
            |> checkErrorResult [ """Update failed""" ]
        }


        [<Fact>]
        member __.``database_get_all_for_account``() = async {
            let! r = Admin.Database.getAllForAccountAction 1
                    |> DBAction.run Client.client
            r |> checkOkResult (
                      [{ databaseId = Some (newDatabaseId 1)
                         accountId = AccountId 1
                         name = "Tasks"
                         };
                       { databaseId = Some (newDatabaseId 6)
                         accountId = AccountId 1
                         name = "DemoForTests"
                         };
                       { databaseId = Some (newDatabaseId 7)
                         accountId = AccountId 1
                         name = "database_without_details"
                         };
                       { databaseId = Some (newDatabaseId 8)
                         accountId = AccountId 1
                         name = "database_with_one_entity_with_all_details"
                         }]
            )

        }
