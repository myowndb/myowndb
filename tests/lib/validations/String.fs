namespace MyOwnDB.Tests.Validations

open Xunit
open MyOwnDB.Tests.Helpers

// Found at https://marnee.silvrback.com/fsharp-and-xunit-classfixture
// For using fixtures used in multiple test classes, see this:
// https://xunit.github.io/docs/shared-context.html#collection-fixture

module String =
    [<Trait("Category", "validations")>]
    type DatabaseTests() =

        [<Fact>]
        member __.``Successful validations``() =
            "My string"
            |> Validations.Base.run [ Validations.String.minimumLength 5 ]
            |> checkOkResult()

            "My string"
            |> Validations.Base.run [ Validations.String.maximumLength 10 ]
            |> checkOkResult()

            "My string"
            |> Validations.Base.run [ Validations.String.exactLength 9 ]
            |> checkOkResult()

            "My string"
            |> Validations.Base.run [ Validations.String.regexp "My\s[A-Za-z]*" ]
            |> checkOkResult()

        [<Fact>]
        member __.``Single failed validation``() =
            "My string"
            |> Validations.Base.run [ Validations.String.minimumLength 10 ]
            |> checkErrorResult [ "String is too short, needed length >= 10, got 9" ]

            "My string"
            |> Validations.Base.run [ Validations.String.maximumLength 5 ]
            |> checkErrorResult [ "String is too long, needed length <= 5, got 9" ]

            "My string"
            |> Validations.Base.run [ Validations.String.exactLength 11 ]
            |> checkErrorResult [ "String length incorrect, needed exact length of 11, got 9" ]

            "My string"
            |> Validations.Base.run [ Validations.String.regexp "My\s\d" ]
            |> checkErrorResult [ sprintf "String does not match pattern %s" "My\s\d"]

        [<Fact>]
        member __.``Multiple failed validations error message``() =
            "My string"
            |> Validations.Base.run
                [ Validations.String.minimumLength 10
                  Validations.String.maximumLength 5
                  Validations.String.exactLength 11
                  Validations.String.regexp "My\s\d" ]
            |> checkErrorResult
                [
                  sprintf "String does not match pattern %s" "My\s\d"
                  sprintf "String length incorrect, needed exact length of %d, got %d" 11 9
                  sprintf "String is too long, needed length <= %d, got %d" 5 9
                  sprintf "String is too short, needed length >= %d, got %d" 10 9
                ]

        [<Fact>]
        member __.``Mix successful and failed validations error message``() =
            // first validation ok, rejections later
            "My string"
            |> Validations.Base.run
                [ Validations.String.minimumLength 2
                  Validations.String.maximumLength 5 // failing validation
                  Validations.String.exactLength 9
                  Validations.String.regexp "My\s\d" ] // failing validation
            |> checkErrorResult
                [ sprintf "String does not match pattern %s" "My\s\d";"String is too long, needed length <= 5, got 9"  ]

            // first validation fails
            "My string"
            |> Validations.Base.run
                [ Validations.String.minimumLength 20 // failing validation
                  Validations.String.maximumLength 30
                  Validations.String.exactLength 21 // failing validation
                  Validations.String.regexp "My\s.*" ]
            |> checkErrorResult
                [ "String length incorrect, needed exact length of 21, got 9"
                  "String is too short, needed length >= 20, got 9"]

        [<Fact>]
        member __.``Custom validation functions``() =
            // Email.validate is a function having the right signature for use as a validation function.
            "My string"
            |> Validations.Base.run
                [ Validations.String.minimumLength 2
                  Validations.String.maximumLength 10
                  Email.validate
                  Validations.String.exactLength 9
                  Validations.String.regexp "My\s\d" ] // failing validation
            |> checkErrorResult [ sprintf "String does not match pattern %s" "My\s\d"; sprintf "email %s is invalid" "My string" ]

            "dummy@myowndb.com"
            |> Validations.Base.run
                [ Validations.String.minimumLength 2
                  Validations.String.maximumLength 40
                  Email.validate
                  Validations.String.exactLength 17 ]
            |> checkOkResult()
