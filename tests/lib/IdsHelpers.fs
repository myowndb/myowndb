// Helper functions related to XUnit

module MyOwnDB.Tests.IdsHelpers
open Ids
open IdsExtensions

// functions to help build an Id values in our tests
let AlwaysValid _ = true
let newInstanceId (id:int) =
    match InstanceId.Validate(id,AlwaysValid) with
    | Some v -> v
    | None -> failwith "unexpected error in building always valid instance id"

let newEntityId (id:int) =
    match EntityId.Validate(id,AlwaysValid) with
    | Some v -> v
    | None -> failwith "unexpected error in building always valid entity id"

let newDatabaseId (id:int) =
    match DatabaseId.Validate(id,AlwaysValid) with
    | Some v -> v
    | None -> failwith "unexpected error in building always valid entity id"

let newRelationId (id:int) =
    match RelationId.Validate(id,AlwaysValid) with
    | Some v -> v
    | None -> failwith "unexpected error in building always valid entity id"
