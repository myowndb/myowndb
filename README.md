# MyOwnDB

This is the code powering [MyOwnDB](https://www.myowndb.com). It is shared under the [AGPL v3](https://www.gnu.org/licenses/agpl-3.0.en.html) license.
It is developed with the [F#](https://fsharp.org/) language.

It is composed by major components:

* the library under `lib/`, which provides all database operations
* the web interface under `webui/`, which provides a web interface built with [WebSharper](https://websharper.com/)

Other interesting directories are:

* `deployments/`, where you can find scripts used in deploying the app or its tests setup
* `fixtgen/`, which has been used to generated fixtures used in tests. This part is not actively used currently as all needed fixtures have already been generated.
* `tests/`, where you find extensive tests, for both
  * `lib/`
  * `webui`, using [Playwright](https://playwright.dev/dotnet/)
* `wsproxies/` is a project defining Javascript proxies allowing the use in `webui` of types defined in `lib/`

The priority of the project has been to deploy the new code, and documentation is missing.

## Contact

You can send a mail to [devinfo-3573@myowndb.com](mailto:devinfo-3573@myowndb.com).

The room `#myowndb:matrix.org` is available to chat on matrix. If you don't use Matrix yet, you can [preview and join the room](https://app.element.io/#/room/#myowndb:matrix.org). As the room is new, activity is very low.
