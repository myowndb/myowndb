FROM gitpod/workspace-postgres

# install dotnet, from

USER gitpod

# Install .NET SDK
# Source: https://docs.microsoft.com/dotnet/core/install/linux-scripted-manual#scripted-install
RUN mkdir -p /home/gitpod/dotnet && curl -fsSL https://dot.net/v1/dotnet-install.sh | bash /dev/stdin --channel 6.0 --version 6.0.401 --install-dir /home/gitpod/dotnet
ENV DOTNET_ROOT=/home/gitpod/dotnet
ENV PATH=$PATH:/home/gitpod/dotnet

RUN bash \
    && { echo 'if [ ! -z $GITPOD_REPO_ROOT ]; then'; \
        echo '\tCONTAINER_DIR=$(awk '\''{ print $6 }'\'' /proc/self/maps | grep ^\/run\/containerd | head -n 1 | cut -d '\''/'\'' -f 1-6)'; \
        echo '\tif [ ! -z $CONTAINER_DIR ]; then'; \
        echo '\t\t[[ ! -d $CONTAINER_DIR ]] && sudo mkdir -p $CONTAINER_DIR && sudo ln -s / $CONTAINER_DIR/rootfs'; \
        echo '\tfi'; \
        echo 'fi'; } >> /home/gitpod/.bashrc.d/110-dotnet

RUN chmod +x /home/gitpod/.bashrc.d/110-dotnet


# End of dotnet install ##################################################

# Install playwright (https://github.com/microsoft/playwright/blob/main/utils/docker/Dockerfile.focal)
#

# === INSTALL Node.js ===

USER root
RUN apt-get update && \
    apt-get upgrade -y && \
    apt-get install -y curl wget
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash - && \
    apt-get install -y nodejs
RUN apt-get install -y --no-install-recommends git openssh-client
RUN npm install -g yarn
RUN apt-get install -y python3 python3-pip
RUN update-alternatives --install /usr/bin/pip pip /usr/bin/pip3 1
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3 1
RUN rm -rf /var/lib/apt/lists/*  && \
    adduser --gecos "" --disabled-password pwuser

# playwright is installed in gitpod init

# End of playwright install ##################################################

# Install go migrate
SHELL ["/bin/bash", "-c"]
RUN url="https://github.com/golang-migrate/migrate/releases/download/v4.8.0/migrate.linux-amd64.tar.gz" \
      && sha256="c92ff8b5085b0de4c027c8c3069529c5e097b02e45effc7c21c46d5952bbf509" \
      && filename=$(basename "$url") \
      && curl -O -L "$url" \
      && sha256sum -c <(echo "$sha256 $filename") \
      && tar xvf "$filename" \
      && sha256sum -c <(echo "d1068435cabcf47932a5a7d59a97fc3b2de5361f077a5f6f483735241d0e0404  migrate.linux-amd64") \
      && mv migrate.linux-amd64 /usr/bin/migrate
# End of go migrate install ##################################################

# Install lazygit
SHELL ["/bin/bash", "-c"]
RUN url="https://github.com/jesseduffield/lazygit/releases/download/v0.31.4/lazygit_0.31.4_Linux_x86_64.tar.gz" \
      && sha256="62dbec07df0f72c6a5f1e868fff96bef17cc06d2aa0fb8684c8e598e98c9700b" \
      && filename=$(basename "$url") \
      && curl -O -L "$url" \
      && sha256sum -c <(echo "$sha256 $filename") \
      && tar xvf "$filename" lazygit \
      && mv lazygit /usr/bin/
# Install rtx
SHELL ["/bin/bash", "-c"]
RUN url="https://github.com/jdxcode/rtx/releases/download/v1.34.1/rtx-v1.34.1-linux-x64.tar.gz" \
      && sha256="fecdfa278d4d0dfcdcbb04c4a04b9ad7aecadb07301f8361d1c6b59574a1587e" \
      && filename=$(basename "$url") \
      && curl -O -L "$url" \
      && sha256sum -c <(echo "$sha256 $filename") \
      && tar xvf "$filename" rtx/bin/rtx \
      && mv rtx/bin/rtx /usr/bin/
# End of rtx install ##################################################
USER gitpod

RUN sudo apt-get update \
        && sudo apt-get install -y gettext

RUN dotnet tool install -g dotnet-ws && echo 'PATH=$PATH:~/.dotnet/tools/' >> ~/.bashrc
