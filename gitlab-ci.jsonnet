// Tests for commits bringing no change to the test image are run in the image
// tagged latest. Commits modifying the docker image are run in the image built
// based on this very commit.
// Both jobs do exactly the same, but just use a different image and an inverted
// condition.
{
  local run_tests_common = {
    stage: 'test',
    before_script: [
      './deployment/prepare_container_for_tests.sh',
      'cd .',
    ],
    script: [
      'make test',
    ],
    services: [
      'docker:dind',
    ],
    variables: {
      DOCKER_HOST: 'tcp://docker:2375/',
    },
    artifacts: {
      when: 'on_failure',
      paths: [
        'tests/webui/playwright/bin/Debug/net*/logs/myowndb*.log',
        'tests/webui/playwright/bin/Debug/net*/logs/lib*.json',
        'tests/webui/playwright/bin/Debug/net*/screenshot*.png',
      ],
    },
  },
  local test_image_triggering_changes = {
    compare_to: 'refs/heads/master',
    paths: [
      'deployment/*',
      'deployment/**/*',
      'Dockerfile',
      '.gitlab-ci.yml',
      'gitlab-ci.jsonnet',
    ],
  },
  stages: [
    'prepare',
    'test',
    'publish_images',
  ],
  build_image: {
    stage: 'prepare',
    image: 'docker:stable',
    services: [
      'docker:dind',
    ],
    script: [
      'docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY',
      'docker build -t $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA .',
      'docker push $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA',
    ],
    rules: [
      { changes: test_image_triggering_changes },
      // when on master, do not compare to master or it would never trigger....
      {
        changes: std.prune(test_image_triggering_changes { compare_to: null }),
        'if': '$CI_COMMIT_BRANCH == "master"',
      },
    ],
  },
  run_tests_new_image: run_tests_common {
    image: '$CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA',
    rules: [
      {
        changes: test_image_triggering_changes,
        // Do not run tests on master as they were already run to validate the merge request
        'if': '$CI_COMMIT_BRANCH != "master"',
      },
    ],
  },
  run_tests_master_image: run_tests_common {
    image: '$CI_REGISTRY_IMAGE:latest',
    rules: [
      {
        changes: test_image_triggering_changes,
        // Do not run tests on master as they were already run to validate the merge request
        'if': '$CI_COMMIT_BRANCH != "master"',
        when: 'never',
      },
      // If image triggering changes were done, the previous rule will match and prevent this to run.
      // If the previous didn't match, we need to run it thanks to the next rule
      {
        'if': '$CI_COMMIT_BRANCH != "master"',
      },
    ],
  },

  publish_tests_image: {
    stage: 'publish_images',
    image: 'docker:stable',
    services: [
      'docker:dind',
    ],
    script: [
      'docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY',
      'docker pull $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA',
      'docker tag $CI_REGISTRY_IMAGE:$CI_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE:latest',
      'docker push $CI_REGISTRY_IMAGE:latest',
    ],
    rules: [
      {
        // when on master, do not compare to master or it would never trigger....
        changes: std.prune(test_image_triggering_changes { compare_to: null }),
        'if': '$CI_COMMIT_BRANCH == "master"',
      },
    ],
  },
  publish_prod_image: {
    stage: 'publish_images',
    image: 'docker:stable',
    services: [
      'docker:dind',
    ],
    script: [
      'docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY',
      'docker build -t $CI_REGISTRY_IMAGE/prod:$CI_COMMIT_SHORT_SHA -f Dockerfile.prod .',
      'docker push $CI_REGISTRY_IMAGE/prod:$CI_COMMIT_SHORT_SHA',
      'docker tag $CI_REGISTRY_IMAGE/prod:$CI_COMMIT_SHORT_SHA $CI_REGISTRY_IMAGE/prod:latest',
      'docker push $CI_REGISTRY_IMAGE/prod:latest',
    ],
    rules: [
      { 'if': '$CI_COMMIT_BRANCH == "master"' },
    ],
  },
}
